<?php

namespace Faktury\Controller\Factory;

use Faktury\Controller\FakturyController;

class Factory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

	protected function newController() {
		return new FakturyController();
	}

}
