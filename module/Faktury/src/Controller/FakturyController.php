<?php

namespace Faktury\Controller;

use \Zend\View\Model\ViewModel;

class FakturyController extends \Wspolne\Controller\WspolneController {

    public function indexAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$faktury = $this->fakturyMenager->fetchAll();

	return new ViewModel(['faktury' => $faktury]);
    }

    public function dodaj() {

    }

    public function wystawAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);
	$klientId = $this->params()->fromRoute('id', null);
	if ($klientId === null) {
	    return $this->redirect();
	}

	$klient = $this->klienciTable->getRekord($klientId);

	$model = new ViewModel(array('klient' => $klient));
	return $model;
    }

    public function generujAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);
	$tryb = $this->params()->fromRoute('tryb', 'normal');

	$post = $this->getPost();

	if ($post) {
	    $doZapłaty = $post->do_zaplaty;
	    $termin = $post->termin_platnosci;
	    $dataWykonania = $post->data_wykonania;
	    $vat = $post->vat;
	    $klientId = $post->klient_id;
	    $slownie = $post->slownie;

	    $faktura = $this->fakturyMenager->zrobSzybkaFakture($klientId, $doZapłaty, $vat, $termin, $dataWykonania,
		    $slownie);

	    $parametry = array('faktura' => $faktura);

	    if ($tryb === 'test') {
		$layout = $this->layout();
		$layout->setTemplate('faktury/layout/pusty');
		$model = new ViewModel($parametry);
		return $model;
	    } else {
		$wynik = $this->pdfMenager->zrobPdf('faktury/faktury/generuj', $parametry, $faktura->getNazwePliku());
//		$zasob = $this->zasobyFasada->zapiszPlik($faktura->getNazwePliku(), $wynik);//TODO: Zapisać fakturę do pliku i powiązać zasób z tabelą faktury

		return $wynik;
	    }
	}
    }

    public function wyswietlAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$idFaktury = $this->params()->fromRoute('id', null);

	if ($idFaktury) {
	    $faktura = $this->fakturyMenager->getRekord($idFaktury);
	    $parametry = array('faktura' => $faktura);


	    $wynik = $this->pdfMenager->zrobPdf('faktury/faktury/generuj', $parametry, $faktura->getNazwePliku());

	    return $wynik;
	}

	return $this->redirect('faktury');
    }

}
