<?php

namespace Faktury;

return array(
	'controllers' => array(
		'factories' => array(
			'Faktury\Controller\Faktury' => Controller\Factory\Factory::class,
		),
	),
	'router' => array(
		'routes' => array(
			'faktury' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/faktury[/:action][/:id][.:tryb]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'tryb' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id' => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Faktury\Controller\Faktury',
						'action' => 'index',
						'tryb' => 'normal',
					),
				),
			),

		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Faktury' => __DIR__ . '/../view',
		),
	),
);
