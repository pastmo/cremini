<?php
return array(
     'controllers' => array(
         'factories' => array(
             'Pulpit\Controller\Pulpit' => 'Pulpit\Controller\Factory\PulpitControllerFactory',
		),
     ),
	  'router' => array(
         'routes' => array(
             'pulpit' => array(
				'type'    => 'segment',
                 'options' => array(
                     'route' => '/pulpit[/:action][/:id]',
					'constraints' => array(
                         'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                         'id'     => '[0-9]+',
                     ),
                     'defaults' => array(
                         'controller' => 'Pulpit\Controller\Pulpit',
						'action'     => 'index',
                     ),
                 ),
             ),
         ),
     ),

     'view_manager' => array(
         'template_path_stack' => array(
             'Pulpit' => __DIR__ . '/../view',
		),
     ),
 );