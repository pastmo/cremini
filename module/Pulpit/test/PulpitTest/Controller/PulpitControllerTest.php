<?php

namespace PulpitTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class PulpitControllerTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

	public function setUp() {
		parent::setUp();
	}

	public function testIndex() {
		$this->ustawZalogowanegoUsera();
		parent::testIndex();
	}

	public function getBigName() {
		return 'Pulpit';
	}

	public function getSmallName() {
		return 'pulpit';
	}

}
