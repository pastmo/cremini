<?php

namespace Pulpit\Controller\Factory;

use Pulpit\Controller\PulpitController;

class PulpitControllerFactory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

	protected function newController() {
		return new PulpitController();
	}

}
