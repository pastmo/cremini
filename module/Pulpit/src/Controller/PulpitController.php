<?php

namespace Pulpit\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class PulpitController extends \Wspolne\Controller\WspolneController {

    public function indexAction() {
	if (!$this->getUzytkownicyMenager()->getZalogowanegoUsera()) {
	    return $this->redirect()->toRoute('logowanie', array('action' => 'index'));
	}

	$zlecenia = $this->zleceniaTable->pobierzResultSetZWherem('data_rozpoczecia is not null',
		\Zlecenia\Model\ZleceniaTable::DOMYSLNY_ORDER, false);
	$zleceniaDoWyswietlenia = \Kalendarz\Helper\KonwerterZlecen::przetworzZlecenia($zlecenia);

	$konczaceSieZlecenia = $this->zleceniaTable->pobierzZWherem("zakonczone='0'", 'data_zakonczenia DESC', 6);
	$emaile = $this->maileMenager->pobierzZWherem("1", \Email\Menager\EmaileMenager::DOMYSLNY_ORDER, 8);

	$liczbaZlecenTydzien = $this->zleceniaTable->liczbaZlecenZakonczonychOstatniTydzien();
	$liczbaZlecenMiesiac = $this->zleceniaTable->liczbaZlecenZakonczonychOstatniMiesiac();
	$liczbaZlecenRok = $this->zleceniaTable->liczbaZlecenZakonczonychOstatniRok();
	$liczbaZlecen = $this->zleceniaTable->liczbaZlecenZakonczonych();

	return new ViewModel(array(
		'zlecenia' => $zleceniaDoWyswietlenia,
		'konczaceSieZlecenia' => $konczaceSieZlecenia,
		'emaile' => $emaile,
		'liczbaZlecenTydzien' => $liczbaZlecenTydzien,
		'liczbaZlecenMiesiac' => $liczbaZlecenMiesiac,
		'liczbaZlecenRok' => $liczbaZlecenRok,
		'liczbaZlecen' => $liczbaZlecen,
	));
    }

}
