<?php

namespace Firmy\Fasada;

class FirmyFasada extends \Wspolne\Fasada\WspolneFasada {

    private $firmyMenager;
    private $uzytkownicyFirmyMenager;
    private $adresyMenager;

    public function __construct(\Zend\ServiceManager\ServiceManager $serviceMenager) {
	parent::__construct($serviceMenager);

	$this->firmyMenager = $this->serviceMenager->get(\Firmy\Menager\FirmyMenager::class);
	$this->uzytkownicyFirmyMenager = $this->serviceMenager->get(\Firmy\Menager\UzytkownicyFirmyMenager::class);
	$this->adresyMenager = $this->serviceMenager->get(\Firmy\Menager\AdresyMenager::class);
    }

    public function pobierzLubUtworzFirme($nazwa) {
	return $this->firmyMenager->pobierzLubUtworzFirme($nazwa);
    }

    public function dodajFirme($post) {
	return $this->firmyMenager->dodajFirme($post);
    }

    public function polaczUzytkownikaZFirma($uzytkownikId, $firmaId) {
	$this->firmyMenager->polaczUzytkownikaZFirma($uzytkownikId, $firmaId);
    }

    public function polaczUzytkownikaZFirmaZBuildera($firmaId,
	    \Wspolne\Interfejs\UzytkownikBuilderInterfejs $builder) {
	$this->uzytkownicyFirmyMenager->polaczUzytkownikaZFirmaZBuildera($firmaId, $builder);
    }

    public function zmienKategorieLojalnosciowa($firmaId, $kategoriaId) {
	$this->firmyMenager->zmienKategorieLojalnosciowa($firmaId, $kategoriaId);
    }

    public function zmienDomyslnegoSprzedawca($firmaId, $sprzedawcaId) {
	$this->firmyMenager->zmienDomyslnegoSprzedawca($firmaId, $sprzedawcaId);
    }

    public function usunPowiazanieUzytkownikaZFirma($uzytkownikId, $firmaId) {
	$this->uzytkownicyFirmyMenager->usunPowiazanieUzytkownikaZFirma($uzytkownikId, $firmaId);
    }

    public function pobierzAdresyDanegoTypu($typ = \Firmy\Enumy\AdresTyp::KSIAZKA_ADRESOWA) {
	return $this->adresyMenager->pobierzAdresyDanegoTypu($typ);
    }

    public function dodajNowyAdresPost($post) {
	$this->adresyMenager->dodajNowyAdresPost($post);
    }

    public function pobierzJedenAdres($id) {
	return $this->adresyMenager->pobierzJedenAdres($id);
    }

    public function usunAdres($id) {
	return $this->adresyMenager->usunAdres($id);
    }

    public function obsluzAdresyNowejPrzesylki($nadawca, $odbiorca) {
	return $this->adresyMenager->obsluzAdresyNowejPrzesylki($nadawca, $odbiorca);
    }

    public function pobierzFirmy($where) {
	return $this->firmyMenager->pobierzZWherem($where);
    }

    public function utworzFirmeZNazwy($nazwa) {
	return $this->firmyMenager->utworzFirmeZNazwy($nazwa);
    }

}
