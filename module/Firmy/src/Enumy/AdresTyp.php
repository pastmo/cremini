<?php

namespace Firmy\Enumy;

class AdresTyp {

    const KSIAZKA_ADRESOWA = 'książka adresowa';
    const POMOCNICZY = 'pomocniczy';
    const ADRES_NADAWCY = 'adres nadawcy';

}
