<?php

namespace Firmy\Menager;

use Zend\ServiceManager\ServiceManager;

class UzytkownicyFirmyMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, \Firmy\Module::UZYTKOWNICY_FIRMY_GATEWAY);
    }

    public function polaczUzytkownikaZFirmaZBuildera($firmaId,
	    \Wspolne\Interfejs\UzytkownikBuilderInterfejs $builder) {
	$uzytkownik = $this->getLogowanieFasada()->zarejestrujLubUaktualnijZBuildera($builder);

	$this->polaczUzytkownikaZFirma($uzytkownik->id, $firmaId);
    }

    public function polaczUzytkownikaZFirma($uzytkownikId, $firmaId) {
	if (!$this->czyUzytkownikPolaczonyZFirma($uzytkownikId, $firmaId)) {
	    $uzytkownikFirma = new \Firmy\Entity\UzytkownikFirma();
	    $uzytkownikFirma->uzytkownik = $uzytkownikId;
	    $uzytkownikFirma->firma = $firmaId;
	    $this->zapisz($uzytkownikFirma);
	}
    }

    public function usunPowiazanieUzytkownikaZFirma($uzytkownikId, $firmaId) {
	$this->usun("uzytkownik_id=$uzytkownikId AND firma_id=$firmaId");
    }

    public function czyUzytkownikPolaczonyZFirma($uzytkownikId, $firmaId) {
	$poprzednich = $this->pobierzZWheremCount("uzytkownik_id=$uzytkownikId AND firma_id=$firmaId");
	return $poprzednich > 0;
    }

}
