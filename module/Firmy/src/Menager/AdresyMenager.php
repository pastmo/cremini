<?php

namespace Firmy\Menager;

use Zend\ServiceManager\ServiceManager;

class AdresyMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public function __construct(ServiceManager $sm) {
	$this->sm = $sm;
	$this->tableGateway = $sm->get(\Firmy\Module::ADRESY_GATEWAY);
    }

    public function pobierzAdresyDanegoTypu($typ = \Firmy\Enumy\AdresTyp::KSIAZKA_ADRESOWA) {
	return $this->pobierzZWherem("typ = '" . $typ . "'");
    }

    public function sprawdzPoprawTypAdresu($adres, $typ) {
	if ($adres->id && $adres->typ !== $typ) {
	    $this->get(AdresyMenager::class)->ustawTypAdresu($adres->id, $typ);
	}
    }

    public function ustawTypAdresu($idAdresu, $typ) {
	$this->update($idAdresu, array('typ' => $typ));
    }

    public function pobierzJedenAdres($id) {
	return $this->getRekord($id);
    }

    public function dodajNowyAdresPost($post) {
	$adres = new \Firmy\Entity\Adres();
	$adres->exchangeArray($post);
	$this->zapisz($adres);
    }

    public function usunAdres($id) {
	try {
	    $this->usunPoId($id);
	} catch (\Exception $ex) {
	    return $ex->getMessage();
	}
    }

    public function obsluzAdresyNowejPrzesylki($nadawca, $odbiorca) {

	$returnArray['nadawca'] = $this->obsluzAdres($nadawca);
	$returnArray['odbiorca'] = $this->obsluzAdres($odbiorca);
	return $returnArray;
    }

    private function obsluzAdres($adres) {
	if (!empty($adres['id'])) {
	    $rekord = $this->getRekord($adres['id']);
	    $nowy = new \Firmy\Entity\Adres();
	    $nowy->exchangeArray($adres);
	    return $this->porownajIZwrocAktualny($rekord, $nowy);
	} else {
	    $this->dodajNowyAdresPost($adres);
	    return $this->getPoprzednioDodany();
	}
    }

    private function porownajIZwrocAktualny($rekord, $nowy) {

	if (!$this->czyTakiSamAdres($rekord, $nowy)) {
	    $nowy->id = 0;
	    $this->zapisz($nowy);
	    $ostatni = $this->getPoprzednioDodany();
	    return $ostatni;
	} else {
	    return $rekord;
	}
    }

    private function czyTakiSamAdres($rekord, $nowy) {
	$odbiorca = $rekord->nazwa === $nowy->nazwa;
	$kod = $rekord->kod === $nowy->kod;
	$miasto = $rekord->miasto === $nowy->miasto;
	$ulica = $rekord->ulica === $nowy->ulica;
	$nrDomu = $rekord->nr_domu === $nowy->nr_domu;
	$nrLokalu = $rekord->nr_lokalu === $nowy->nr_lokalu;
	$kontaktowa = $rekord->osoba_kontaktowa === $nowy->osoba_kontaktowa;
	$telefon = $rekord->telefon === $nowy->telefon;
	$email = $rekord->email === $nowy->email;
	$kraj = $rekord->kraj === $nowy->kraj;
	$typ = $rekord->typ === \Firmy\Enumy\AdresTyp::ADRES_NADAWCY;

	$wynik = ($kontaktowa && $telefon && $email && ($kraj || $typ) && $odbiorca && $kod && $miasto && $ulica && $nrDomu
		&& $nrLokalu);
	return $wynik;
    }

}
