<?php

namespace Firmy\Menager;

use Zend\ServiceManager\ServiceManager;
use Pastmo\Wspolne\Utils\SearchPole;

class FirmyMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy implements \Pastmo\Wspolne\Interfejs\PodpowiadanieInterface {

    private $adresyMenager;

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, \Firmy\Module::FIRMY_GATEWAY);
	$this->adresyMenager = $this->get(AdresyMenager::class);
    }

    protected function pobierzPolaWyszukiwania() {
	return array(
		SearchPole::create()->setDataType(SearchPole::STRING)->setName("nazwa"),
	);
    }

    protected function konwertujNaRekordWyszuiwania($encja) {
	$wynik = new \Pastmo\Wspolne\Entity\RekordWyszukiwania();

	$wynik->value = $encja->nazwa . "";
	$wynik->tokens = array($encja->nazwa);
	$wynik->url = $this->generujUrl('firmy', array('action' => 'szczegoly', 'id' => $encja->id));
	$wynik->type = "Firmy";
	$wynik->id = $encja->id;
	$wynik->menager = \Firmy\Menager\FirmyMenager::class;

	return $wynik;
    }

    public function zapisz(\Pastmo\Wspolne\Entity\WspolnyModel $model) {
	parent::zapisz($model);

	$this->ustawPrawidloweTypyAdresow($model);
    }

    public function usunPoId($idFirmy) {
	$firma = $this->getRekord($idFirmy);

	$this->adresyMenager->sprawdzPoprawTypAdresu($firma->adres_wysylki, \Firmy\Enumy\AdresTyp::POMOCNICZY);
	$this->adresyMenager->sprawdzPoprawTypAdresu($firma->adres_faktury, \Firmy\Enumy\AdresTyp::POMOCNICZY);

	parent::usunPoId($idFirmy);
    }

    public function pobierzLubUtworzFirme($nazwa) {
	$firmy = $this->pobierzZWherem("nazwa='$nazwa'");

	if (count($firmy) === 0) {
	    $firma = new \Firmy\Entity\Firma();
	    $firma->nazwa = $nazwa;
	    $this->zapisz($firma);
	    $wynik = $this->getPoprzednioDodany();
	    return $wynik;
	} else {
	    return $firmy[0];
	}
    }

    public function dodajFirme($post) {

	$parametry = $this->pobierzParametry($post);
	$projektId = $post['project_id'];

	$adres = $this->utworzAdres($parametry);
	$dodanyAdres = $this->zapiszAdres($adres);

	$firma = $this->utworzFirmeZAdresu($dodanyAdres);
	$firma->nip = $parametry['nip'];

	$this->zapisz($firma);
	$dodanaFirma = $this->getPoprzednioDodany();

	$this->getProjektyFasada()->dodajLubUsunFirmeDoProjektu($projektId, $dodanaFirma->id);
    }

    public function utworzFirmeZNazwy($nazwa) {
	$firma = new \Firmy\Entity\Firma();
	$firma->nazwa = $nazwa;
	$this->zapisz($firma);
	$wynik = $this->getPoprzednioDodany();
	return $wynik;
    }

    public function aktualizujNipIAdresFaktury($idFirmy, $post) {

	$nip = isset($post['nip']) ? $post['nip'] : false;

	if ($nip) {
	    $this->update($idFirmy, array('nip' => $nip));
	}

	$adres = new \Firmy\Entity\Adres();
	$adres->exchangeArray($post);

	$this->aktualizujAdresFaktury($idFirmy, $adres);
    }

    public function aktualizujAdresFaktury($idFirmy, \Firmy\Entity\Adres $adres) {
	return $this->aktualizujAdresFirmy($idFirmy, $adres, 'adres_faktury');
    }

    public function aktualizujAdresWysylki($idFirmy, \Firmy\Entity\Adres $adres) {
	return $this->aktualizujAdresFirmy($idFirmy, $adres, 'adres_wysylki');
    }

    public function pobierzUzytkownikowFirmy($firmaId) {
	$uzytkownicy = $this->get(\Logowanie\Menager\UzytkownicyMenager::class)->pobierzZWherem("id IN (select uzytkownik_id from uzytkownicy_firmy where firma_id=$firmaId)");
	return $uzytkownicy;
    }

    public function pobierzFirmyUzytkownika($uzytkownikId) {
	$firmy = $this->pobierzZWherem("id IN (select firma_id from uzytkownicy_firmy where uzytkownik_id=$uzytkownikId)");
	return $firmy;
    }

    public function polaczUzytkownikaZFirma($uzytkownikId, $firmaId) {
	$this->get(UzytkownicyFirmyMenager::class)->polaczUzytkownikaZFirma($uzytkownikId, $firmaId);
    }

    public function zmienKategorieLojalnosciowa($firmaId, $kategoriaId) {
	$this->update($firmaId, array('kategoria_lojalnosciowa_id' => $kategoriaId));
    }

    public function zmienDomyslnegoSprzedawca($firmaId, $sprzedawcaId) {
	$this->update($firmaId, array('domyslny_sprzedawca_id' => $sprzedawcaId));
    }

    public function zmienObrazek($idFirmy) {
	$pliki = $this->getZasobyFasada()->zaladujPlik();

	foreach ($pliki as $plik) {
	    $this->update($idFirmy, array('img_id' => $plik->id));
	}
    }

    public function zmienNazwe($idFirmy, $nowaNazwa) {
	$this->update($idFirmy, array('nazwa' => $nowaNazwa));
    }

    private function aktualizujAdresFirmy($idFirmy, $adres, $kolumnaAdresu) {
	$firma = $this->aktualizujPowiazanaTabeleZObiektu($idFirmy, $kolumnaAdresu, AdresyMenager::class, $adres);
	$this->ustawPrawidloweTypyAdresow($firma);
	return $firma;
    }

    private function ustawPrawidloweTypyAdresow($firma) {
	if ($firma->adres_wysylki) {
	    $this->adresyMenager->sprawdzPoprawTypAdresu($firma->adres_wysylki,
		    \Firmy\Enumy\AdresTyp::KSIAZKA_ADRESOWA);
	}
	if ($firma->adres_faktury) {
	    $this->adresyMenager->sprawdzPoprawTypAdresu($firma->adres_faktury, \Firmy\Enumy\AdresTyp::POMOCNICZY);
	}
    }

    private function pobierzParametry($postZDodawaniaFirmy) {
	$adress = $postZDodawaniaFirmy['address'];
	$adresArray = json_decode($adress);

	$parametry = array();
	$parametry['nazwa'] = $adresArray->name;
	$parametry['kraj'] = $adresArray->country;
	$parametry['miasto'] = $adresArray->city;
	$parametry['kod'] = $adresArray->zip;
	$parametry['ulica'] = $adresArray->street;
	$parametry['nr_domu'] = $adresArray->number;
	$parametry['nr_lokalu'] = $adresArray->flat;
	$parametry['osoba_kontaktowa'] = $adresArray->contact_person;
	$parametry['telefon'] = $adresArray->phone;
	$parametry['nip'] = $postZDodawaniaFirmy['nip'];

	return $parametry;
    }

    private function utworzAdres($parametry) {
	$adres = new \Firmy\Entity\Adres();
	$adres->exchangeArray($parametry);
	return $adres;
    }

    private function zapiszAdres($adres) {
	$adresyMenager = $this->get(AdresyMenager::class);
	$adresyMenager->zapisz($adres);
	$dodany = $adresyMenager->getPoprzednioDodany();
	return $dodany;
    }

    private function utworzFirmeZAdresu($adres) {
	$firma = new \Firmy\Entity\Firma();
	$firma->adres_wysylki = $adres;
	$firma->adres_faktury = $this->kopiujEncje($adres, AdresyMenager::class);
	$firma->nazwa = $adres->nazwa;

	return $firma;
    }

    private function modyfikujAdresWysylki($adres) {
	$adres->typ = \Firmy\Enumy\AdresTyp::KSIAZKA_ADRESOWA;
	return $adres;
    }

    private function modyfikujAdresFaktury($adres) {
	$adres->typ = \Firmy\Enumy\AdresTyp::POMOCNICZY;
	return $adres;
    }

}
