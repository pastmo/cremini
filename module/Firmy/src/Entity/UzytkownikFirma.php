<?php

namespace Firmy\Entity;

class UzytkownikFirma extends \Wspolne\Model\WspolneModel {

    public $id;
    public $uzytkownik;
    public $firma;

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);

	if ($this->czyTabeleDostepne()) {
	    $this->data = $data;
	    $this->firma = $this->pobierzTabeleObca('firma_id', \Firmy\Menager\FirmyMenager::class, Firma::create());
	    $this->uzytkownik = $this->pobierzTabeleObca('uzytkownik_id', \Logowanie\Menager\UzytkownicyMenager::class,
		    \Logowanie\Model\Uzytkownik::create());
	}
    }

    public function dowiazListyTabelObcych() {

    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'uzytkownik':
		return 'uzytkownik_id';
	    case 'firma':
		return 'firma_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public function wyswietlUliceNr() {
	return $this->ulica . ' ' .
		$this->nr_domu . '/'
		. $this->nr_lokalu;
    }

}
