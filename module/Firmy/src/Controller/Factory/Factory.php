<?php

namespace Firmy\Controller\Factory;

use Firmy\Controller\FirmyController;

class Factory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

    protected function newController() {
	return new FirmyController();
    }

}
