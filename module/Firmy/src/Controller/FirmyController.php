<?php

namespace Firmy\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use \Logowanie\Enumy\KodyUprawnien;
use Wspolne\Utils\SearchPole;

class FirmyController extends \Wspolne\Controller\WspolneController {

    protected $zasobyTable;

    public function indexAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::MENU_FIRMY, true);

	$pola = array(
		SearchPole::create()->setDataType(SearchPole::STRING)->setName('nazwa'),
	);

	$dodatkowyWhere = $this->zrobDodatkowyWhere();

	$firmy = $this->pobierzZGeta($this->firmyMenager, $dodatkowyWhere, $pola);
	$kluczOrder = $this->pobierzKluczOrder();
	$typOrder = $this->pobierzTypOrder();

	return new ViewModel(array(
		'firmy' => $firmy,
		'sort_key' => $kluczOrder,
		'sort_typ' => $typOrder,
	));
    }

    public function zrobDodatkowyWhere() {
	$wynik = '1';
	$search = new \Wspolne\Utils\Search();
	$domyslny_sprzedawca = $this->params()->fromQuery('domyslny_sprzedawca', false);
	$aktywne_projekty = $this->params()->fromQuery('aktywne_projekty', false);

	if ($domyslny_sprzedawca) {
	    $imie = $search->zrobPojedynczyWhere('imie', $domyslny_sprzedawca);
	    $nazwisko = $search->zrobPojedynczyWhere('nazwisko', $domyslny_sprzedawca);

	    $uzytkownicy = "select id from uzytkownicy where $imie OR $nazwisko";
	    $wynik.= " AND domyslny_sprzedawca_id IN($uzytkownicy)";
	}
	if ($aktywne_projekty) {
	    $nazwa = $search->zrobPojedynczyWhere('nazwa', $aktywne_projekty);

	    $projekty = "select firma_id from projekty where $nazwa";
	    $wynik.= " AND id IN($projekty)";
	}

	return $wynik;
    }

    public function szczegolyAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::MENU_FIRMY, true);

	$id = $this->params()->fromRoute('id', 0);

	$firma = $this->firmyMenager->getRekord($id);
	$projektyAktywne = $this->projektyMenager->pobierzProjektyAktywneDlaFirmy($firma->id);
	$projektyZamkniete = $this->projektyMenager->pobierzProjektyZamknieteDlaFirmy($firma->id);
	$kategorieLojalnosciowe = $this->produktyFasada->pobierzKategorieLojalnosciowe();
	$sprzedawcy = $this->logowanieFasada->pobierzUzytkownikowZUprawnieniami(KodyUprawnien::UPRAWNIENIA_SPRZEDAWCY);
	$firmy = $this->firmyMenager->pobierzZWherem("1", "nazwa asc");
	$uzytkownicy = $this->uzytkownikTable->pobierzZWherem("1", "nazwisko ASC");

	return new ViewModel(array(
		'firma' => $firma,
		'projektyAktywne' => $projektyAktywne,
		'projektyZamkniete' => $projektyZamkniete,
		'kategorieLojalnosciowe' => $kategorieLojalnosciowe,
		'sprzedawcy' => $sprzedawcy,
		'firmy' => $firmy,
		'uzytkownicy' => $uzytkownicy
	));
    }

    public function dodajAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::SZCZEGOLY_FIRMY_DODAWANIE_EDYCJA, true);

	$nazwa = $this->params()->fromPost('nazwa', false);
	if ($nazwa) {
	    $firma = $this->firmyMenager->pobierzLubUtworzFirme($nazwa);
	    return $this->redirect()->toRoute('firmy', array('action' => 'szczegoly', 'id' => $firma->id));
	}

	return $this->redirect()->toRoute('firmy');
    }

    public function aktualizujAdresWysylkiAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::SZCZEGOLY_FIRMY_DODAWANIE_EDYCJA, true);
	$id = $this->params()->fromRoute('id', 0);

	$post = $this->getPost();
	$adres = new \Firmy\Entity\Adres();
	$adres->exchangeArray($post);

	$this->firmyMenager->aktualizujAdresWysylki($id, $adres);

	return $this->redirect()->toRoute('firmy', array('action' => 'szczegoly', 'id' => $id));
    }

    public function aktualizujAdresFakturyAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::SZCZEGOLY_FIRMY_DODAWANIE_EDYCJA, true);
	$id = $this->params()->fromRoute('id', 0);

	$post = $this->getPost();

	$this->firmyMenager->aktualizujNipIAdresFaktury($id, $post);

	return $this->redirect()->toRoute('firmy', array('action' => 'szczegoly', 'id' => $id));
    }

    public function usunAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::SZCZEGOLY_FIRMY_DODAWANIE_EDYCJA, true);

	$id = $this->params()->fromRoute('id', 0);

	if ($id > 0) {
	    $this->firmyMenager->usunPoId($id);
	}

	return $this->redirect()->toRoute('firmy');
    }

    public function dodajProjektAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::SZCZEGOLY_FIRMY_DODAWANIE_EDYCJA, true);
	$id = $this->params()->fromRoute('id');

	$nazwaProjektu = $this->params()->fromPost('nazwa');
	$firma = $this->firmyMenager->getRekord($id);

	$builder = \Projekty\TworzenieProjektu\ProjektBuilder::create()
		->setNazwaFirmy($firma->nazwa)
		->setNazwa($nazwaProjektu);

	$this->projektyFasada->utworzProjekt($builder);

	return $this->redirect()->toRoute('firmy', array('action' => 'szczegoly', 'id' => $id));
    }

    public function zmienObrazekAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::SZCZEGOLY_FIRMY_DODAWANIE_EDYCJA, true);
	$id = $this->params()->fromRoute('id', 0);

	$this->firmyMenager->zmienObrazek($id);

	return $this->returnSuccess();
    }

    public function zmienNazweAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::SZCZEGOLY_FIRMY_DODAWANIE_EDYCJA, true);

	$id = $this->params()->fromRoute('id', 0);
	$nazwa = $this->params()->fromPost('nazwa');

	$this->firmyMenager->zmienNazwe($id, $nazwa);

	return $this->returnSuccess();
    }

    public function usunUzytkownikaAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::SZCZEGOLY_FIRMY_DODAWANIE_EDYCJA, true);

	$id = $this->params()->fromRoute('id', 0);
	$uzytkownikId = $this->params()->fromPost('user_id');

	$this->firmyFasada->usunPowiazanieUzytkownikaZFirma($uzytkownikId, $id);

	return $this->returnSuccess();
    }

    public function dodajUzytkownikaAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::SZCZEGOLY_FIRMY_DODAWANIE_EDYCJA, true);

	$id = $this->params()->fromRoute('id', 0);
	$email = $this->params()->fromPost('email');
	$imieNazwisko = $this->params()->fromPost('imie_nazwisko');
	$telefon = $this->params()->fromPost('telefon');

	$builder = \Logowanie\Builder\UzytkownikBuilder::create()
		->setEmail($email)
		->setImieINazwisko($imieNazwisko)
		->setTelefon($telefon);

	$this->firmyFasada->polaczUzytkownikaZFirmaZBuildera($id, $builder);

	return $this->redirect()->toRoute('firmy', array('action' => 'szczegoly', 'id' => $id));
    }

    public function przypiszUzytkownikaAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::SZCZEGOLY_FIRMY_DODAWANIE_EDYCJA, true);
	$id = $this->params()->fromRoute('id', 0);
	$post = $this->getPost();

	if ($post && !empty($post->uzytkownik_id)) {
	    $this->firmyFasada->polaczUzytkownikaZFirma($post->uzytkownik_id, $id);
	}

	return $this->redirect()->toRoute('firmy', array('action' => 'szczegoly', 'id' => $id));
    }

    public function pdfAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::MENU_FIRMY, true);

	$id = $this->params()->fromRoute('id', 0);
	$firma = $this->firmyMenager->getRekord($id);

	return $this->pdfMenager->zrobPdfZSzablonem('firmy/firmy/part/pdf', array('firma' => $firma), $firma->nazwa);
    }

}
