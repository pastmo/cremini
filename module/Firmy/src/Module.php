<?php

namespace Firmy;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Firmy\Entity\Firma;

class Module implements ConfigProviderInterface {

    const FIRMY_GATEWAY = 'FirmyGateway';
    const ADRESY_GATEWAY = 'AdresyGateway';
    const UZYTKOWNICY_FIRMY_GATEWAY = 'UzytkownicyFirmyGateway';

    public function getConfig() {
	return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig() {
	return array(
		'factories' => array(
			Fasada\FirmyFasada::class => function($sm) {

			    $table = new Fasada\FirmyFasada($sm);
			    return $table;
			},
			\Firmy\Menager\FirmyMenager::class => function($sm) {
			    $table = new Menager\FirmyMenager($sm);
			    return $table;
			},
			\Firmy\Menager\AdresyMenager::class => function($sm) {
			    $table = new Menager\AdresyMenager($sm);
			    return $table;
			},
			\Firmy\Menager\UzytkownicyFirmyMenager::class => function($sm) {
			    $table = new Menager\UzytkownicyFirmyMenager($sm);
			    return $table;
			},
			self::FIRMY_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new Firma($sm));
			    return new TableGateway('firmy', $dbAdapter, null, $resultSetPrototype);
			},
			self::ADRESY_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new Entity\Adres($sm));
			    return new TableGateway('adresy', $dbAdapter, null, $resultSetPrototype);
			},
			self::UZYTKOWNICY_FIRMY_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new Entity\UzytkownikFirma($sm));
			    return new TableGateway('uzytkownicy_firmy', $dbAdapter, null, $resultSetPrototype);
			},
		),
	);
    }

}
