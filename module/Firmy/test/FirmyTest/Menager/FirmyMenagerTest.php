<?php

namespace FirmyTest\Menager;



class FirmyMenagerTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

	private $post = array(
		"company_id" => " new ",
		"address_id" => " new ",
		"name" => "Nowa firma",
		"address" => '{"name":"Nazwa firmy","country":"Polska","city":"Miasto","zip":"33","street":"dasdf","number":"333","flat":"43","contact_person":"asdfasdf","phone":"234543"}',
		"project_id" => "3",
		"nip"=>'34543'
	);

	public function setUp() {
		parent::setUp();
		$this->ustawMockaGateway(\Firmy\Module::FIRMY_GATEWAY);

		$this->firmyMenager = new \Firmy\Menager\FirmyMenager($this->sm);
	}

	public function test_pobierzLubUtworzFirme() {
		$this->obslugaTestowanejKlasy->ustawMockiDoPobierzZWherem(array());
		$this->gateway->expects($this->once())->method('insert');
		$this->firmyMenager->pobierzLubUtworzFirme('nazwa');
	}

	public function test_dodajFirme() {
		$this->ustawPoprzednioDodanaFirmeIAdres();

		$this->sprawdzZapisAdresuIFirmy();
		$this->sprawdzLaczenieFirmyZProjektem();

		$this->firmyMenager->dodajFirme($this->post);
	}

	private function ustawPoprzednioDodanaFirmeIAdres() {
		$this->adresyMenager->expects($this->exactly(2))->method('getPoprzednioDodany')
				->willReturn(\FabrykaEncjiMockowych::utworzEncje(\Firmy\Entity\Adres::class));

		$firma = \FabrykaEncjiMockowych::utworzEncje(\Firmy\Entity\Firma::class);
		$this->obslugaTestowanejKlasy->ustawGetLastInsertValue($firma->id);
		$this->obslugaTestowanejKlasy->ustawWynikGetRekord($firma, $this->gateway);
	}

	private function sprawdzZapisAdresuIFirmy() {
		$this->adresyMenager->expects($this->exactly(2))->method('zapisz');
		$this->gateway->expects($this->once())->method('insert');
	}

	private function sprawdzLaczenieFirmyZProjektem() {
		$this->projektyMenager->expects($this->once())->method('aktualizujFirmeWProjekcie');
	}

	private function sprawdzCzyUzytkownikJestZapisywany() {

	}

}
