<?php

namespace FirmyTest\Integracyjne;



class AdresyMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

	private $adresyMenager;

	public function setUp() {
		parent::setUp();

		$this->adresyMenager = $this->sm->get(\Firmy\Menager\AdresyMenager::class);
	}

	public function test_dodawanie() {
		$parametry = array();

		$firma = \FabrykaRekordow::utworzEncje($this->sm,
						\Firmy\Entity\Adres::class, $parametry, false);

		$this->adresyMenager->zapisz($firma);

		$wynik = $this->adresyMenager->getPoprzednioDodany();

		$this->assertNotNull($wynik->id);
		$this->assertNotNull($wynik->ulica);
		$this->assertNotNull($wynik->nr_domu);
		$this->assertNotNull($wynik->nr_lokalu);
		$this->assertEquals(1,$wynik->akceptacja);
		//TODO: To jest błąd- wartości logiczne pobinny być reprezentowane przez zmienne boolowskie (true/false).
		//Niestety WspolneModel na razie tego nie obsługuje.
	}

}
