<?php

namespace FirmyTest\Integracyjne;

class UzytkownicyFirmyMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $uzytkownicyFirmyMenager;

    public function setUp() {
	parent::setUp();

	$this->uzytkownicyFirmyMenager = $this->sm->get(\Firmy\Menager\UzytkownicyFirmyMenager::class);
    }

    public function test_dodawanie() {

	$uzytkownikFirma = \FabrykaRekordow::makeEncje(
			\Firmy\Entity\UzytkownikFirma::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneBrakzapisuPowiazane::create($this->sm));


	$this->uzytkownicyFirmyMenager->zapisz($uzytkownikFirma);

	$wynik = $this->uzytkownicyFirmyMenager->getPoprzednioDodany();

	$this->assertNotNull($wynik->id);
	$this->assertNotNull($wynik->firma->id);
	$this->assertNotNull($wynik->uzytkownik->id);
    }

    public function testpolaczUzytkownikaZFirma() {
	$firma = \FabrykaRekordow::makeEncje(\Firmy\Entity\Firma::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane::create($this->sm));

	$uzytkownik = \FabrykaRekordow::makeEncje(\Logowanie\Model\Uzytkownik::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane::create($this->sm));

	$this->uzytkownicyFirmyMenager->polaczUzytkownikaZFirma($uzytkownik->id, $firma->id);
	$this->uzytkownicyFirmyMenager->polaczUzytkownikaZFirma($uzytkownik->id, $firma->id);

	$wynik = $this->sm->get(\Firmy\Menager\UzytkownicyFirmyMenager::class)->pobierzZWheremCount("firma_id={$firma->id} AND uzytkownik_id={$uzytkownik->id}");

	$this->assertEquals(1, $wynik);
    }

    public function test_usunPowiazanie() {

	$uzytkownikFirma = \FabrykaRekordow::makeEncje(
			\Firmy\Entity\UzytkownikFirma::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane::create($this->sm));


	$this->uzytkownicyFirmyMenager->usunPowiazanieUzytkownikaZFirma($uzytkownikFirma->uzytkownik->id,$uzytkownikFirma->firma->id);

	$wynik=$this->uzytkownicyFirmyMenager->pobierzZWheremCount("id={$uzytkownikFirma->id}");

	$this->assertEquals(0,$wynik);
    }

}
