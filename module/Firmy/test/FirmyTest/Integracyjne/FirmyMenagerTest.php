<?php

namespace FirmyTest\Integracyjne;

use \Firmy\Enumy\AdresTyp;
use Firmy\Menager\AdresyMenager;

class FirmyMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $firmyMenager;
    private $post = array(
	"company_id" => " new ",
	"address_id" => " new ",
	"name" => "Nowa firma",
	"address" => '{"name":"Nazwa firmy","country":"Polska","city":"Miasto","zip":"33","street":"dasdf","number":"333","flat":"43","contact_person":"asdfasdf","phone":"234543"}',
	"project_id" => "3",
	"nip" => '34543'
    );

    public function setUp() {
	parent::setUp();

	$this->firmyMenager = $this->sm->get(\Firmy\Menager\FirmyMenager::class);
    }

    public function test_dodawanie() {

	$firma = \FabrykaRekordow::utworzEncje(null, \Firmy\Entity\Firma::class, null,
			null,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneBrakzapisuNiepowiazane::create($this->sm));


	$this->firmyMenager->zapisz($firma);

	$wynik = $this->firmyMenager->getPoprzednioDodany();

	$this->assertNotNull($wynik->id);
	$this->assertNull($wynik->adres_wysylki->id);
	$this->assertNull($wynik->adres_wysylki->id);
    }

    public function test_dodawanie_powiazanie_z_adresami() {

	$firma = \FabrykaRekordow::utworzEncje(null, \Firmy\Entity\Firma::class, null,
			null,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneBrakzapisuPowiazane::create($this->sm));

	$this->firmyMenager->zapisz($firma);

	$wynik = $this->firmyMenager->getPoprzednioDodany();

	$this->assertNotNull($wynik->id);
	$this->assertNotNull($wynik->adres_faktury->id);
	$this->assertNotNull($wynik->adres_wysylki->id);

	$this->sprawdzPrawidloweTypyAdresowFirmy($wynik);
    }

    public function test_aktualizujAdresFaktury() {
	$firma = \FabrykaRekordow::makeEncje(\Firmy\Entity\Firma::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane::create($this->sm));
	$adres = \FabrykaRekordow::makeEncje(\Firmy\Entity\Adres::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm)
				->setparametry(array('ulica' => 'nowa_ulica', 'nazwa' => 'nazwa z testu')));

	$this->firmyMenager->aktualizujAdresFaktury($firma->id, $adres);

	$zapisanaFirma = $this->firmyMenager->getRekord($firma->id);
	$this->assertEquals('nowa_ulica', $zapisanaFirma->adres_faktury->ulica);
	$this->assertEquals('nazwa z testu', $zapisanaFirma->adres_faktury->nazwa);
	$this->assertNotEquals($adres->id, $zapisanaFirma->adres_faktury->id);
	$this->sprawdzPrawidloweTypyAdresowFirmy($zapisanaFirma);
    }

    public function test_dodajFirme() {
	$projekt = \FabrykaRekordow::makeEncje(\Projekty\Entity\Projekt::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm));
	$this->post['project_id'] = $projekt->id;

	$this->firmyMenager->dodajFirme($this->post);

	$zaktualizowanyProjekt = $this->sm->get(\Projekty\Menager\ProjektyMenager::class)->getRekord($projekt->id);

	$this->assertNotNull($zaktualizowanyProjekt->firma->id);
	$this->assertEquals($zaktualizowanyProjekt->firma->nip,
		$zaktualizowanyProjekt->dane_do_faktury->nip);

	$this->sprawdzPrawidloweTypyAdresowFirmy($zaktualizowanyProjekt->firma);
    }

    public function test_usun() {
	$firma = \FabrykaRekordow::makeEncje(\Firmy\Entity\Firma::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane::create($this->sm));
	$adresWysylki = $firma->adres_wysylki;
	$adresFaktury = $firma->adres_faktury;

	$this->firmyMenager->usunPoId($firma->id);

	$wynik = $this->firmyMenager->pobierzZWheremCount("id={$firma->id}");

	$this->assertEquals(0, $wynik);

	$adresyManager = $this->sm->get(AdresyMenager::class);
	$adrWysylki = $adresyManager->getRekord($adresWysylki->id);
	$adrFaktury = $adresyManager->getRekord($adresFaktury->id);

	$this->assertEquals(AdresTyp::POMOCNICZY, $adrWysylki->typ);
	$this->assertEquals(AdresTyp::POMOCNICZY, $adrFaktury->typ);
    }

    public function test_usun_powiazane_z_uzytkownikiem() {
	$uzytkownikFirma = \FabrykaRekordow::makeEncje(
			\Firmy\Entity\UzytkownikFirma::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane::create($this->sm));


	$this->firmyMenager->usunPoId($uzytkownikFirma->firma->id);

	$wynik = $this->firmyMenager->pobierzZWheremCount("id={$uzytkownikFirma->firma->id}");

	$this->assertEquals(0, $wynik);
    }

    public function test_usun_powiazane_z_projektem() {
	$firma = \FabrykaRekordow::makeEncje(\Firmy\Entity\Firma::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane::create($this->sm));

	\FabrykaRekordow::makeEncje(\Projekty\Entity\Projekt::class,
		\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm)
			->setparametry(array('firma' => $firma->id)));

	$this->firmyMenager->usunPoId($firma->id);

	$wynik = $this->firmyMenager->pobierzZWheremCount("id={$firma->id}");

	$this->assertEquals(0, $wynik);
    }

    public function test_pobierzUzytkownikowFirmy() {
	$uzytkownikFirma = \FabrykaRekordow::makeEncje(
			\Firmy\Entity\UzytkownikFirma::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane::create($this->sm));

	$uzytkownicy = $this->firmyMenager->pobierzUzytkownikowFirmy($uzytkownikFirma->firma->id);

	$this->assertEquals(1, count($uzytkownicy));
	$this->assertTrue($uzytkownicy[0] instanceof \Logowanie\Model\Uzytkownik);
    }

    public function test_pobierzFirmyUzytkownika() {
	$uzytkownikFirma = \FabrykaRekordow::makeEncje(
			\Firmy\Entity\UzytkownikFirma::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane::create($this->sm));

	$firmy = $this->firmyMenager->pobierzFirmyUzytkownika($uzytkownikFirma->uzytkownik->id);

	$this->assertEquals(1, count($firmy));
	$this->assertTrue($firmy[0] instanceof \Firmy\Entity\Firma);
    }

    public function test_aktualizujNipIAdresFaktury() {
	$firma = \FabrykaRekordow::makeEncje(\Firmy\Entity\Firma::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm));
	$post = array('miasto' => 'Białystok', 'nip' => '77777777777');

	$this->firmyMenager->aktualizujNipIAdresFaktury($firma->id, $post);

	$zapisana = $this->firmyMenager->getRekord($firma->id);

	$this->assertEquals('77777777777', $zapisana->nip);
	$this->assertEquals('Białystok', $zapisana->adres_faktury->miasto);
	$this->assertEquals(AdresTyp::POMOCNICZY, $zapisana->adres_faktury->typ);
    }

    private function sprawdzPrawidloweTypyAdresowFirmy($firma) {
	$this->assertEquals(AdresTyp::KSIAZKA_ADRESOWA, $firma->adres_wysylki->typ);
	$this->assertEquals(AdresTyp::POMOCNICZY, $firma->adres_faktury->typ);
    }

}
