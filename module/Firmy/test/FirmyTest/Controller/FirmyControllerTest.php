<?php

namespace FirmyTest\Controller;

use \Projekty\TworzenieProjektu\ProjektBuilder;
use \Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane;

class FirmyControllerTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

    protected $traceError = true;

    public function setUp() {
	parent::setUp();

	$this->obslugaKlasObcych->ustawZalogowanegoUsera();
    }

    public function testIndex() {

    }

    public function testzrobDodatkowyWhere() {
	$this->firmyMenager->expects($this->once())->method('pobierzResultSetZWherem')
		->with($this->equalTo('1 AND 1 '));
	$this->dispatch('/firmy?domyslny_sprzedawca=&aktywne_projekty=&wyszukaj=');
    }

    public function testzrobDodatkowyWhere_domyslny_sprzedawca() {
	$this->firmyMenager->expects($this->once())->method('pobierzResultSetZWherem')
		->with($this->equalTo("1 AND 1 AND domyslny_sprzedawca_id IN(select id from uzytkownicy where imie  REGEXP '[\w]*(Gr[zźż][eę])[\w]*'  OR nazwisko  REGEXP '[\w]*(Gr[zźż][eę])[\w]*' ) "));
	$this->dispatch('/firmy?domyslny_sprzedawca=Grze&aktywne_projekty=&wyszukaj=');
    }

    public function testzrobDodatkowyWhere_aktywne_projekty() {
	$this->firmyMenager->expects($this->once())->method('pobierzResultSetZWherem')
		->with($this->equalTo("1 AND 1 AND id IN(select firma_id from projekty where nazwa  REGEXP '[\w]*(Pr[oó]j[eę]kt)[\w]*' ) "));
	$this->dispatch('/firmy?domyslny_sprzedawca=&aktywne_projekty=Projekt&wyszukaj=');
    }

    public function testZwyklyWhere() {
	$this->firmyMenager->expects($this->once())->method('pobierzResultSetZWherem')
		->with($this->equalTo("1 AND 1  AND (nazwa  REGEXP '[\w]*(w)[\w]*' )"));
	$this->dispatch('/firmy?wyszukaj=w');
    }

    public function testaktualizujAdresWysylkiAction() {
	$this->ustawParametryPosta(array('miasto' => 'Wrocław'));

	$this->firmyMenager->expects($this->once())->method('aktualizujAdresWysylki');

	$this->dispatch('/firmy/aktualizuj_adres_wysylki/42');
	$this->sprawdzCzyPrzekierowanie();
    }

    public function testaktualizujAdresFakturyAction() {
	$this->ustawParametryPosta(array('miasto' => 'Wrocław'));

	$this->firmyMenager->expects($this->once())->method('aktualizujNipIAdresFaktury');

	$this->dispatch('/firmy/aktualizuj_adres_faktury/42');
	$this->sprawdzCzyPrzekierowanie();
    }

    public function test_zmienObrazekAction() {
	$this->ustawParametryPosta();

	$this->firmyMenager->expects($this->once())->method('zmienObrazek')
		->with($this->equalTo(42));

	$this->dispatch('/firmy/zmien_obrazek/42');
	$this->sprawdzCzySukces();
    }

    public function testdodajProjektAction() {
	$this->ustawParametryPosta(array('nazwa' => 'Nowy projekt'));

	$firma = \FabrykaEncjiMockowych::makeEncje(\Firmy\Entity\Firma::class,
			PFMockoweNiepowiazane::create()->setparametry(array('nazwa' => "Pastmo")));
	$this->obslugaKlasObcych->ustawGetRecord(\Firmy\Menager\FirmyMenager::class,
		$firma);

	$this->tworzenieProjektuMenager->expects($this->once())->method('utworzProjekt')
		->with($this->callback(function(ProjektBuilder $builder) {
			    $this->assertEquals('Pastmo', $builder->getNazwaFirmy());
			    $this->assertEquals('Nowy projekt', $builder->getNazwa());
			    return true;
			}));
	$this->dispatch('/firmy/dodaj_projekt/42');
	$this->sprawdzCzyPrzekierowanie();
    }

    public function test_zmienNazweAction() {
	$this->ustawParametryPosta(array('nazwa' => 'Nowa nazwa'));

	$this->firmyMenager->expects($this->once())->method('zmienNazwe')
		->with($this->equalTo(42), $this->equalTo('Nowa nazwa'));

	$this->dispatch('/firmy/zmien_nazwe/42');
	$this->sprawdzCzySukces();
    }

    public function test_usunUzytkownikaAction() {
	$this->ustawParametryPosta(array('user_id' => '1500'));

	$this->uzytkownicyFirmyMenager->expects($this->once())->method('usunPowiazanieUzytkownikaZFirma')
		->with($this->equalTo('1500'),$this->equalTo(42));

	$this->dispatch('/firmy/usun_uzytkownika/42');
	$this->sprawdzCzySukces();
    }

    public function test_dodajUzytkownikaAction() {
	$this->ustawParametryPosta(array('imie_nazwisko' => 'Jan Nowak','email'=>'asd@ss.s','telefon'=>'9999'));

	$this->uzytkownicyFirmyMenager->expects($this->once())->method('polaczUzytkownikaZFirmaZBuildera')
		->with($this->equalTo(42),$this->callback(function($builder){
		    return $builder instanceof \Wspolne\Interfejs\UzytkownikBuilderInterfejs;
		}));

	$this->dispatch('/firmy/dodaj_uzytkownika/42');
	$this->sprawdzCzyPrzekierowanie();
    }

    public function getBigName() {
	return 'Firmy';
    }

    public function getSmallName() {
	return 'firmy';
    }

}
