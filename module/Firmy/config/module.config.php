<?php

namespace Firmy;

return array(
	'controllers' => array(
		'factories' => array(
			'Firmy\Controller\Firmy' => Controller\Factory\Factory::class,
		),
	),
	'router' => array(
		'routes' => array(
			'firmy' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/firmy[/:action][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id' => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Firmy\Controller\Firmy',
						'action' => 'index',
					),
				),
			),
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Firmy' => __DIR__ . '/../view',
		),
	),
);
