<?php

namespace Pastmo\Wiadomosci\Enumy;

class SkrzynkiWiadomosci {

    const ODBIORCZA = 'odbiorcza';
    const WYSLANE = 'wyslane';
    const SPAM = 'spam';
    const WAZNE = 'wazne';
    const KOSZ = 'kosz';

}
