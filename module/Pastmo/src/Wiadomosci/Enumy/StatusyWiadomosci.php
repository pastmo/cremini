<?php

namespace Pastmo\Wiadomosci\Enumy;

class StatusyWiadomosci {

    const NIEPRZECZYTANA = 'nieprzeczytana';
    const PRZECZYTANA = 'przeczytana';
    const USUNIETA = 'usunieta';
    const UKRYTA = 'ukryta';

}
