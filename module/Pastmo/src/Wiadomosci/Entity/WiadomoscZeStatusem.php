<?php

namespace Pastmo\Wiadomosci\Entity;

use Pastmo\Wiadomosci\Enumy\StatusyWiadomosci;
use Pastmo\Wiadomosci\Enumy\SkrzynkiWiadomosci;

abstract class WiadomoscZeStatusem extends \Wspolne\Model\WspolneModel {

    public $status;
    public $skrzynka;

    public function exchangeArray($data) {
	$this->skrzynka = $this->pobierzLubNull($data, 'skrzynka', StatusyWiadomosci::NIEPRZECZYTANA);
	$this->status = $this->pobierzLubNull($data, 'status', SkrzynkiWiadomosci::ODBIORCZA);
    }

}
