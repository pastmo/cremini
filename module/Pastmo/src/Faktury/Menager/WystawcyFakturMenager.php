<?php

namespace Pastmo\Faktury\Menager;

class WystawcyFakturMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm, \Pastmo\Faktury\KonfiguracjaModulu::WYSTAWCA_FAKTURY_GATEWAY);
    }

    public function pobierzUtworzWystawce() {
	$wystawcy = $this->pobierzZWherem("1");

	if (count($wystawcy) === 0) {
	    $wystawca = new \Pastmo\Faktury\Entity\WystawcaFaktur();
	    $wystawca->nazwa = "nazwa";
	    $this->zapisz($wystawca);
	    $wynik = $this->getPoprzednioDodany();
	    return $wynik;
	} else {
	    return $wystawcy[0];
	}
    }

    public function zapiszZPosta($post) {
	$wystawca = $this->pobierzUtworzWystawce();
	$wystawca->aktualizujArray($post);

	$adres = $wystawca->adres;
	if (!$adres) {
	    $adres = new \Pastmo\Faktury\Entity\Adres();
	}

	$adres->aktualizujArray($post);
	$wystawca->adres = $this->zapiszWInnymTable($adres, AdresyMenager::class);

	$this->zapisz($wystawca);
    }

}
