<?php

namespace Pastmo\Faktury\Menager;

class FakturyMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm, \Pastmo\Faktury\KonfiguracjaModulu::FAKTURY_GATEWAY);
    }

    public function zrobSzybkaFakture($klientId, $kwota_netto, $vat, $termin, $data_wykonania, $slownie) {

	$today = $this->pobierzDzisiejszaDate();

	$faktura = new \Pastmo\Faktury\Entity\Faktura();
	$faktura->termin_platnosci = $termin;
	$faktura->klient = $klientId;
	$faktura->kwota_netto = $kwota_netto;
	$faktura->vat = $vat;

	$faktura->data_wykonania = $data_wykonania;
	$faktura->data_wystawienia = $today;
	$faktura->wystawca = $this->get(WystawcyFakturMenager::class)->pobierzUtworzWystawce();
	$faktura->nr_faktury = $this->utworzNumerNowejFaktury();
	$faktura->rok = $this->pobierzAktualnyRok();
	$faktura->slownie = $slownie;

	$this->zapisz($faktura);

	return $this->getPoprzednioDodany();
    }

    public function utworzNumerNowejFaktury() {
	$aktualnyRok = $this->pobierzAktualnyRok();

	$fakturWTymRoku = $this->pobierzZWheremCount("rok='$aktualnyRok'");
	$fakturWTymRoku++;

	$wynik = "FVS/$fakturWTymRoku/$aktualnyRok";
	return $wynik;
    }

    private function pobierzAktualnyRok() {
	return substr($this->pobierzDzisiejszaDate(), 0, 4);
    }

    private function pobierzDzisiejszaDate() {
	$todayDatetime = new \DateTime();
	$today = $todayDatetime->format("Y-m-d");
	return $today;
    }

}
