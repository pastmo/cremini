<?php

namespace Pastmo\Faktury;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class KonfiguracjaModulu extends \Pastmo\KonfiguracjaModulu {

    const WYSTAWCA_FAKTURY_GATEWAY = "wystawca_faktury_gateway";
    const FAKTURY_GATEWAY = "faktury_gateway";
    const ADRESY_GATEWAY = "adresy_gateway";
    const KLIENCI_GATEWAY = "KlienciGateway";

    public function getFactories() {
	return array(
		Menager\WystawcyFakturMenager::class => function($sm) {

		    $table = new Menager\WystawcyFakturMenager($sm);
		    return $table;
		},
		self::WYSTAWCA_FAKTURY_GATEWAY => function ($sm) {
		    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
		    $resultSetPrototype = new ResultSet();
		    $resultSetPrototype->setArrayObjectPrototype(new Entity\WystawcaFaktur($sm));
		    return new TableGateway('wystawca_faktur', $dbAdapter, null, $resultSetPrototype);
		},
		Menager\FakturyMenager::class => function($sm) {

		    $table = new Menager\FakturyMenager($sm);
		    return $table;
		},
		self::FAKTURY_GATEWAY => function ($sm) {
		    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
		    $resultSetPrototype = new ResultSet();
		    $resultSetPrototype->setArrayObjectPrototype(new Entity\Faktura($sm));
		    return new TableGateway('faktury', $dbAdapter, null, $resultSetPrototype);
		},
		Menager\AdresyMenager::class => function($sm) {

		    $table = new Menager\AdresyMenager($sm);
		    return $table;
		},
		self::ADRESY_GATEWAY => function ($sm) {
		    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
		    $resultSetPrototype = new ResultSet();
		    $resultSetPrototype->setArrayObjectPrototype(new Entity\Adres($sm));
		    return new TableGateway('adresy', $dbAdapter, null, $resultSetPrototype);
		},
		Menager\KlienciMenager::class => function($sm) {

		    $table = new Menager\KlienciMenager($sm);
		    return $table;
		},
		self::KLIENCI_GATEWAY => function ($sm) {
		    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
		    $resultSetPrototype = new ResultSet();
		    $resultSetPrototype->setArrayObjectPrototype(new Entity\Klient($sm));
		    return new TableGateway('klienci', $dbAdapter, null, $resultSetPrototype);
		},
	);
    }

}
