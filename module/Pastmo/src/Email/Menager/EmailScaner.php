<?php

namespace Pastmo\Email\Menager;

use Zend\Mail\Storage\Imap;
use Zend\Mail\Storage;
use Pastmo\Email\Entity\MailKomunikatZeSkanera;

class EmailScaner {

    use \Pastmo\Wspolne\Utils\TranslateTrait;

    public $errors;
    private $mail;
    private $host;
    private $user;
    private $password;
    private $port_imap;
    private $sm;
    private $kontoMailowe;
    private $zasobyTable;
    private $emaileMenager;

    /* $host = 'serwer1578643.home.pl',
      $user = 'testowe1@grupaglasso.pl', $password = 'Testowe1', */

    public function __construct($sm = null) {
	$this->sm = $sm;
	$this->zasobyTable = $this->sm->get(\Zasoby\Menager\ZasobyUploadMenager::getClass());
	$this->translator = $this->sm->get('MvcTranslator');
    }

    public function init(\Pastmo\Email\Entity\KontoMailowe $kontoMailowe) {
	$this->host = $kontoMailowe->host;
	$this->user = $kontoMailowe->user;
	$this->password = $kontoMailowe->getHasloJawne();
	$this->port_imap = $kontoMailowe->port_imap;
	$this->kontoMailowe = $kontoMailowe;
    }

    public function connect() {

	try {
	    $params = array('host' => $this->host,
		    'user' => $this->user,
		    'password' => $this->password,
	    );

	    if ($this->port_imap) {
		$params['port'] = $this->port_imap;
	    }

	    $this->mail = new \Zend\Mail\Storage\Imap($params);
	} catch (\Exception $e) {
	    if ($e->getMessage() === 'cannot login, user or password wrong') {
		throw new \Pastmo\Wspolne\Exception\PastmoException($this->translate('Błędny login lub hasło'));
	    }
	}
    }

    public function getAllMessages() {
	$this->connect();
	$result = array();
	$ilosc = $this->mail->countMessages();

	for ($i = $ilosc; $i > 0; $i--) {
	    $message = $this->mail->getMessage($i);

	    $mail = new MailKomunikatZeSkanera($this->zasobyTable);
	    $mail->init($message, $this->getRawMessageFromImap($this->mail, $i));
	    $result[] = $mail;
	}

	return $result;
    }

    public function getUnhandledMessages($limit = false) {
	$this->connect();
	$odpowiedz = \Pastmo\Email\Entity\OdpowiedzOdswiezania::create();
	$this->emaileMenager = $this->sm->get(\Pastmo\Email\Menager\EmaileMenager::class);

//	$folders=$this->mail->getFolders();
//	$foldery = new \RecursiveIteratorIterator($folders, \RecursiveIteratorIterator::SELF_FIRST);
//
//	foreach ($foldery as $localName => $folder) {
//	    $localName = str_pad('', $foldery->getDepth(), '-', STR_PAD_LEFT) .
//		    $localName;
//	    if ($folder->isSelectable()) {
//		$this->mail->selectFolder($folder);


		$ilosc = $this->mail->countMessages();

		for ($i = $ilosc; $i > 0; $i--) {
		    try {
			if ($limit && $odpowiedz->ilePobranychMaili >= KontaMailoweMenager::LIMIT_WIADOMOSCI_W_CZESCIOWYM_ODSWIEZANIU) {
			    break;
			}
			$message = $this->mail->getMessage($i);

			$messageId = MailKomunikatZeSkanera::generujMessageId($message);
			$czyIstnieje = $this->emaileMenager->sprawdzCzyMailJuzIstniejeZMailId($messageId, $this->kontoMailowe->id);
			if ($czyIstnieje) {
			    continue;
			}

			$mail = new MailKomunikatZeSkanera($this->zasobyTable);
			$mail->init($message, $this->getRawMessageFromImap($this->mail, $i));

			$odpowiedz->addToListaPobranychMaili($mail);
			$odpowiedz->ilePobranychMaili++;
		    } catch (\Exception $e) {
			$raw = $this->mail->getRawHeader($i) . $this->mail->getRawContent($i);
			$msg = $e->getMessage();
			$this->errors[] = $msg;

			$zasob = $this->zasobyTable->zapiszPlik('error' . $i . '.txt', $raw);

			$odpowiedz->addErrors($msg, $zasob->id);
		    }
		}

		$odpowiedz->setIleMailiZostaloDoPobrania($i);
//	    }
//	}
	return $odpowiedz;
    }

    private function getRawMessageFromImap($imap, $index) {
	return $imap->getRawHeader($index) . $imap->getRawContent($index);
    }

    public static function getClass() {
	return __CLASS__;
    }

}
