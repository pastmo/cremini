<?php

namespace Pastmo\Email\Menager;

use Zend\ServiceManager\ServiceManager;
use Pastmo\Wspolne\Utils\EntityUtil;

class KontaMailoweMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public $emailScaner;
    public $emaileMenager;

    const LIMIT_WIADOMOSCI_W_CZESCIOWYM_ODSWIEZANIU = 10;

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, \Pastmo\Module::KONTA_MAILOWE_GATEWAY);
	$this->emaileMenager = $this->sm->get(\Email\Menager\EmaileMenager::class);
    }

    public function pobierzKontoMailoweDlaUzytkownika($host, $userSerwera, $userAplikacjiId) {
	$kontaMailowe = $this->pobierzZWherem("host='$host' AND user='$userSerwera' AND uzytkownik_id=$userAplikacjiId");

	if (count($kontaMailowe) > 0) {
	    return $kontaMailowe[0];
	} else {
	    return null;
	}
    }

    public function pobierzKontoMailowe($host, $userSerwera) {
	$kontaMailowe = $this->pobierzZWherem("host='$host' AND user='$userSerwera'");

	if (count($kontaMailowe) > 0) {
	    return $kontaMailowe[0];
	} else {
	    return null;
	}
    }

    public function pobierzKontaMailoweUzytkownika($userAplikacjiId) {
	$kontaMailowe = $this->pobierzZWherem("uzytkownik_id=$userAplikacjiId", "id ASC");

	return $kontaMailowe;
    }

    public function odswiezMaileZWszystkichSkrzynek() {
	try {
	    $zalogowany = $this->getLogowanieFasada()->getZalogowanegoUsera();
	    $konta = $zalogowany->getKontaMailowe();
	    foreach ($konta as $konto) {
		if ($konto->id !== 0) {
		    $this->odsiwezMaile($konto->id);
		}
	    }
	} catch (\Exception $e) {
	    $this->emailScaner->errors[] = $e->getMessage();
	}
    }

    public function odswiezMaileZWszystkichSkrzynekCzesciowo(array $ignorowaneSkrzynkiId = []) {
	$wynik = [];
	try {
	    $konta = $this->getLogowanieFasada()->pobierzKontaMailoweDlaZalogowanegoUzytkownika();

	    foreach ($konta as $uzytkownikKontoMailowe) {
		$kontoId = EntityUtil::wydobadzId($uzytkownikKontoMailowe->konto_mailowe);
		if ($kontoId !== 0 && !in_array($kontoId, $ignorowaneSkrzynkiId)) {
		    $wynik[] = $this->odsiwezMaileCzesciowo($kontoId);
		}
	    }
	} catch (\Exception $e) {
	    $this->emailScaner->errors[] = $e->getMessage(); //TODO: Dodać to do wyniku
	}

	return $wynik;
    }

    public function odsiwezMaileCzesciowo($kontoId) {
	return $this->odswiezMaileWspolne($kontoId, true);
    }

    public function odsiwezMaile($kontoId) {
	return $this->odswiezMaileWspolne($kontoId, false);
    }

    public function odswiezMaileWspolne($kontoId, $czesioweOdswiezanie) {
	$kontoMailowe = $this->getRekord($kontoId);
	$this->emailScaner = $kontoMailowe->pobierzEmailScanera();
	$odpowiedzOdswiezania = $this->emailScaner->getUnhandledMessages($czesioweOdswiezanie);
	$odpowiedzOdswiezania->setIdKonta($kontoId);

	for ($i = $odpowiedzOdswiezania->ilePobranychMaili - 1; $i >= 0; $i--) {
	    $mailSkanera = $odpowiedzOdswiezania->listaPobranychMaili[$i];
	    $this->zapiszEmailZeSkanera($mailSkanera, $kontoMailowe);
	    
	    $email = $this->emaileMenager->getPoprzednioDodany();
	    $odpowiedzOdswiezania->addPrzetworzoneMaile($email->id, $email->tytul);
	}

	return $odpowiedzOdswiezania;
    }

    public function zapiszEmailZeSkanera($mailSkanera, $kontoMailowe) {
	$mail = \Pastmo\Email\Entity\Email::konwertujZMailSkanera($mailSkanera, $kontoMailowe, $this->sm);

	$this->emaileMenager->zapisz($mail);
    }

    public static function getClass() {
	return __CLASS__;
    }

}
