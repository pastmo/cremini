<?php

namespace Pastmo\Email\Controller;

use Zend\View\Model\ViewModel;

class EmailController extends \Wspolne\Controller\WspolneController {

    public function indexAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::MENU_EMAIL, true);
	$uzytkownik = $this->uzytkownikTable->getZalogowanegoUsera();

	$id = (int) $this->params()->fromRoute('id', 0);
	$skrzynka = $this->params()->fromRoute('skrzynka', \Pastmo\Wiadomosci\Enumy\SkrzynkiWiadomosci::ODBIORCZA);

	$skrzynka = str_replace("folder", "", $skrzynka);

	$konta = $uzytkownik->getKontaMailowe();

	if (count($konta) > 0) {
	    $aktywneKonto = $konta[$id];

	    $emails = $this->maileMenager->pobierzEmaileDlaKontaISkrzynki($aktywneKonto->id, $skrzynka);

	    return new ViewModel($this->dodajParametry(array(
			    'wybraneKonto' => $aktywneKonto,
			    'emails' => $emails,
			    'konta' => $konta,
			    'skrzynka' => $this->maileMenager->pobierzSkrzynke($skrzynka)
	    )));
	} else {
	    return new ViewModel(array(
		    'wybraneKonto' => new \Pastmo\Email\Entity\KontoMailowe(),
		    'emails' => array(),
		    'konta' => array(),
		    'skrzynka' => $skrzynka
	    ));
	}
    }

    public function szczegolyAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::MENU_EMAIL, true);

	$id = $this->params()->fromRoute('id', 0);

	$this->maileMenager->ustawJakoPrzeczytane($id);
	$email = $this->maileMenager->getRekord($id);

	return new ViewModel($this->dodajParametry(array(
			'email' => $email
	)));
    }

    protected function dodajParametry(array $parametry) {
	return $parametry;
    }

    public function wysylanieAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::MENU_EMAIL, true);

	$id = (int) $this->params()->fromRoute('id', 0);

	return new ViewModel(array(
		'id' => $id
	));
    }

    public function wyslijAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::MENU_EMAIL, true);
	$id = (int) $this->params()->fromRoute('id', 0);

	$post = $this->getPost();
	if ($post) {

	    $emailOdbiorcy = $post->odbiorca;
	    $tresc = $post->tresc;
	    $temat = $post->temat;
	    $konta = $this->logowanieFasada->pobierzKontaMailoweDlaZalogowanegoUzytkownika();


	    if (count($konta) === 0) {
		throw new \Exception("Brak kont mailowych");
	    }

	    $emailDoWysylki = \Pastmo\Email\Entity\EmailDoWysylki::create();
	    $emailDoWysylki->odbiorcaEmail = $emailOdbiorcy;
	    $emailDoWysylki->odbiorcaImieNazwisko = "";
	    $emailDoWysylki->temat = $temat;
	    $emailDoWysylki->tresc = $tresc;
	    $emailDoWysylki->uzytkownikKonto = $konta[$id];

	    $zasobyId = $this->zasobyFasada->pobierzIdZasobow($post, 'pliki');
	    $emailDoWysylki->idZasobow = $zasobyId;

	    $this->emailFasada->wyslijEmail($emailDoWysylki);
	}

	return $this->redirect()->toRoute('email');
    }

    public function emailAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::MENU_EMAIL, true);
	$layout = $this->layout();
	$layout->setTemplate('layout/bialy_layout');
	$id = $this->params()->fromRoute('id');

	$email = $this->emailFasada->pobierzEmailaPoId($id);

	return new ViewModel(array('email' => $email));
    }

}
