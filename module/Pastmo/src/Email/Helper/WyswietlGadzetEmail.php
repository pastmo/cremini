<?php

namespace Pastmo\Email\Helper;

use Logowanie\Enumy\KodyUprawnien;

class WyswietlGadzetEmail extends \Pastmo\Wspolne\Helper\TranslateHelper {

    protected $view;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm) {
	parent::__construct($sm);
    }

    public function __invoke($view) {
	$this->view = $view;
	if (!$this->view->uprawnienia(KodyUprawnien::MENU_EMAIL)) {
	    return;
	}
	$this->dolaczSkrypty();
	$this->wyswietlHtml();
    }

    public function dolaczSkrypty() {
	\Application\Controller\LadowaczSkryptow::add($this->view->basePath('js/wiadomosci/timer.js'));
	\Application\Controller\LadowaczSkryptow::add($this->view->basePath('js/pastmo/email/odswiezacz_email.js'));
    }

    public function wyswietlHtml() {
	?>
	<li class="">
	    <a id="gadzet_email" class="btn" href="<?php
	    echo $this->view->url('email', array('action' => 'index'));
	    ?>" title="Nowe wiadomości email">
		<i class="fa fa-at">
		    <span class="item-quantity" style="display: none;">0</span>
		</i>
	    </a>
	</li>
	<?php
    }

}
