<?php

namespace Pastmo\Lokalizacje;

class KonfiguracjaModulu extends \Pastmo\KonfiguracjaModulu {

    public function onBootstrap(\Zend\Mvc\MvcEvent $e) {
	parent::onBootstrap($e);

	$viewHelperMenager = $e->getApplication()->getServiceManager()->get('ViewHelperManager');
	$viewHelperMenager->setFactory('ustawLokalizacjeJs',
		function($sm) use ($e) {
	    $viewHelper = new Helper\UstawLokalizacjeJs($sm);
	    return $viewHelper;
	});
    }

}
