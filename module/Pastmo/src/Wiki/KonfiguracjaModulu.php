<?php

namespace Pastmo\Wiki;


class KonfiguracjaModulu extends \Pastmo\KonfiguracjaModulu {

    const WIKI_GATEWAY = "WIKI_GATEWAY";
    const WIKI_GATEWAY_HISTORIA = "WIKI_GATEWAY_HISTORIA";

    public function getFactories() {
	return array(

	    Menager\WikiArtykulyMenager::class => function($sm) {
		return new Menager\WikiArtykulyMenager($sm);
	    },

	    Menager\WikiArtykulyHistoriaMenager::class => function($sm) {
		return new Menager\WikiArtykulyHistoriaMenager($sm);
	    },

	    self::WIKI_GATEWAY => function ($sm) {
		return $this->ustawGateway($sm, Entity\WikiArtykul::class, 'wiki_artykuly');
	    },

	    self::WIKI_GATEWAY_HISTORIA => function ($sm) {
		return $this->ustawGateway($sm, Entity\WikiArtykul::class, 'wiki_artykuly_historia');
	    },

	);
    }

}
