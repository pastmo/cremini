<?php

namespace Pastmo\Wiki\Entity;

use Zasoby\Model\Zasob;

class WikiArtykul extends \Wspolne\Model\WspolneModel {

    public $id;
    public $id_tematu;
    public $jezyk_kod;
    public $nadrzedny_id_tematu;
    public $avatar;
    public $autor;
    public $uprawnienie_kod;
    public $tytul;
    public $tresc;

    public function dowiazListyTabelObcych() {

    }

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->id_tematu = $this->pobierzLubNull($data, 'id_tematu');
	$this->jezyk_kod = $this->pobierzLubNull($data, 'jezyk_kod');
	$this->nadrzedny_id_tematu = $this->pobierzLubNull($data, 'nadrzedny_id_tematu');

	$this->data = $data;
	$this->avatar = $this->pobierzTabeleObca('avatar_id',
		\Zasoby\Menager\ZasobyUploadMenager::class,
		Zasob::zrobDomyslnyZasobAvatara($this->sm));

	$this->autor = $this->pobierzTabeleObca('autor_id',
		\Logowanie\Menager\UzytkownicyMenager::class,
		new \Logowanie\Model\Uzytkownik());

	$this->uprawnienie_kod = $this->pobierzLubNull($data, 'uprawnienie_kod');
	$this->tytul = $this->pobierzLubNull($data, 'tytul');
	$this->tresc = $this->pobierzLubNull($data, 'tresc');
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'autor':
		return 'autor_id';
	    case 'avatar':
		return 'avatar_id';
	    default:
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public static function create() {
	$artykul= new WikiArtykul();
	$artykul->autor = \Logowanie\Model\Uzytkownik::create();
	return $artykul;
    }

}
