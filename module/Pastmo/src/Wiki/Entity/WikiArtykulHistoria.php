<?php

namespace Pastmo\Wiki\Entity;

use Zasoby\Model\Zasob;

class WikiArtykulHistoria extends WikiArtykul {

    public $id;
    public $wiki_artykul;
    public $id_tematu;
    public $jezyk_kod;
    public $nadrzedny_id_tematu;
    public $avatar;
    public $autor;
    public $uprawnienie_kod;
    public $tytul;
    public $tresc;

    public function dowiazListyTabelObcych() {

    }

    public static function fromWikiArtykul($artykulZBazy) {

	$artykul = new WikiArtykulHistoria();
	$artykul->id = 0;
	$artykul->wiki_artykul = $artykulZBazy->id;
	$artykul->id_tematu = $artykulZBazy->id_tematu;
	$artykul->jezyk_kod = $artykulZBazy->jezyk_kod;
	$artykul->nadrzedny_id_tematu = $artykulZBazy->nadrzedny_id_tematu;
	$artykul->avatar = $artykulZBazy->avatar;
	$artykul->autor = $artykulZBazy->autor;
	$artykul->uprawnienie_kod = $artykulZBazy->uprawnienie_kod;
	$artykul->tytul = $artykulZBazy->tytul;
	$artykul->tresc = $artykulZBazy->tresc;
	return $artykul;
    }

    public function exchangeArray($data) {

	$artykul = new WikiArtykulHistoria();
	$artykul->id = $this->pobierzLubNull($data, self::ID);

	$artykul->wiki_artykul = $this->pobierzTabeleObca('wiki_artykul',
		\Pastmo\Wiki\Entity\WikiArtykul::class,
		new WikiArtykul());

	$artykul->id_tematu = $this->pobierzLubNull($data, 'id_tematu');
	$artykul->jezyk_kod = $this->pobierzLubNull($data, 'jezyk_kod');
	$artykul->nadrzedny_id_tematu = $this->pobierzLubNull($data, 'nadrzedny_id_tematu');

	$artykul->avatar = $this->pobierzTabeleObca('avatar_id',
		\Zasoby\Menager\ZasobyUploadMenager::class,
		Zasob::zrobDomyslnyZasobAvatara($this->sm));

	$artykul->autor = $this->pobierzTabeleObca('uzytkownik_id',
		\Logowanie\Menager\UzytkownicyMenager::class,
		new \Logowanie\Model\Uzytkownik());

	$artykul->uprawnienie_kod = $this->pobierzLubNull($data, 'uprawnienie_kod');
	$artykul->tytul = $this->pobierzLubNull($data, 'tytul');
	$artykul->tresc = $this->pobierzLubNull($data, 'tresc');
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'wiki_artykul':
		return 'wiki_artykul_id';
	    case 'autor':
		return 'autor_id';
	    case 'avatar':
		return 'avatar_id';
	    default:
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
