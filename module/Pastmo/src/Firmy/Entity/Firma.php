<?php

namespace Pastmo\Firmy\Entity;

use Zasoby\Model\Zasob;


class Firma extends \Wspolne\Model\WspolneModel {

    public $id;
    public $adres_wysylki;
    public $adres_faktury;
    public $nazwa;
    public $data_dodania;
    public $img;
    public $nip;
    private $uzytkownicy;

    public function dowiazListyTabelObcych() {

	if ($this->czyTabeleDostepne()) {
	    $firmyMenager = $this->sm->get(\Firmy\Menager\FirmyMenager::class);

	    $this->uzytkownicy = $firmyMenager->pobierzUzytkownikowFirmy($this->id);
	} else {
	    $this->uzytkownicy = array();
	}
    }

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->nazwa = $this->pobierzLubNull($data, 'nazwa');
	$this->data_dodania = $this->pobierzLubNull($data, 'data_dodania');
	$this->nip = $this->pobierzLubNull($data, 'nip');

	$this->data = $data;
	$this->adres_faktury = $this->pobierzTabeleObca('adres_faktury_id', \Firmy\Menager\AdresyMenager::class,
		new Adres());
	$this->adres_wysylki = $this->pobierzTabeleObca('adres_wysylki_id', \Firmy\Menager\AdresyMenager::class,
		new Adres());
	$this->img = $this->pobierzTabeleObca('img_id', \Zasoby\Menager\ZasobyUploadMenager::class,
		Zasob::zrobDomyslnyZasobAvatara());
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'adres_faktury':
		return 'adres_faktury_id';
	    case 'adres_wysylki':
		return 'adres_wysylki_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function getUzytkownicy() {
	$this->dowiazListyTabelObcych();
	return $this->uzytkownicy;
    }

    public function czySaPrzypisaniUzytkownicy() {
	$this->dowiazListyTabelObcych();
	return count($this->uzytkownicy) > 0;
    }



    public function pobierzKlase() {
	return __CLASS__;
    }

    public function __toString() {
	if ($this->nazwa) {
	    return $this->nazwa;
	} else {
	    return "";
	}
    }

    public static function create($sm = null) {
	$encja = new Firma($sm);
	$encja->img = Zasob::zrobDomyslnyZasobAvatara();
	$encja->adres_wysylki = new Adres();
	$encja->adres_faktury = new Adres();
	$encja->domyslny_sprzedawca = \Logowanie\Model\Uzytkownik::create($sm);
	return $encja;
    }

}
