<?php

namespace Pastmo\Wspolne\Utils;

trait TranslateTrait {

    public $translator;
    public $constantTranslator;

    public function translate($tekst) {
	if (!$this->translator) {
	    return $tekst;
	}
	return $this->translator->translate($tekst);
    }

    public function translateConst($tekst) {
	$this->sprwadzIInicjujConstantTraslator();
	if (!$this->constantTranslatorDostepny()) {
	    return $tekst;
	}
	return $this->constantTranslator->translateConst($tekst);
    }

    public function initConstantTranslation() {
	throw new \Wspolne\Exception\GgerpException("Metoda powinna być przesłonięta w klasach używających traita");
    }

    private function sprwadzIInicjujConstantTraslator() {
	if (!$this->constantTranslatorDostepny()) {
	    $this->initConstantTranslation();
	}
    }

    private function constantTranslatorDostepny() {
	return $this->constantTranslator !== null;
    }

}
