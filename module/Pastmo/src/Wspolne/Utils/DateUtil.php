<?php

namespace Pastmo\Wspolne\Utils;

class DateUtil extends DateCommon {

    public static function obliczInterwal($czasWMinutach) {
	$minuty = round($czasWMinutach * 60);
	$param = "PT{$minuty}S";
	$interval = new \DateInterval($param);
	return $interval;
    }

    public static function obliczMinutyInterwalu($dataOd, $dataDo) {
	$sekundy = self::obliczSekundyInterwalu($dataOd, $dataDo);

	return self::sekundyNaMinuty($sekundy);
    }

    public static function obliczMinutyInterwaluD(\DateTime $dataOd, \DateTime $dataDo) {

	return self::obliczMinutyInterwalu($dataOd->format(self::FORMAT_TIMESTAMP),
			$dataDo->format(self::FORMAT_TIMESTAMP));
    }

    public static function obliczSekundyInterwalu($dataOd, $dataDo) {
	$date1 = strtotime($dataOd);
	$date2 = strtotime($dataDo);

	$diff = $date2 - $date1;

	return $diff;
    }

    public static function sekundyNaMinuty($sekundy) {
	return floor($sekundy / 60);
    }

    public static function dodajOdejmijInterwal($dataString, $czasPrzesunieciaWMinutach) {
	if ($czasPrzesunieciaWMinutach > 0) {
	    return self::dodajInterwal($dataString, $czasPrzesunieciaWMinutach);
	} else {
	    return self::odejmijInterwal($dataString, abs($czasPrzesunieciaWMinutach));
	}
    }

    public static function dodajInterwal($dataString, $czasPrzesunieciaWMinutach) {
	return self::wykonajOdejmijInterwal($dataString, $czasPrzesunieciaWMinutach, 'add');
    }

    public static function odejmijInterwal($dataString, $czasPrzesunieciaWMinutach) {
	return self::wykonajOdejmijInterwal($dataString, $czasPrzesunieciaWMinutach, 'sub');
    }

    public static function pobierzNajblizyPoczatekMiesiaca($nrMiesiaca, $dataBazowa = null) {

	$data = new \DateTime($dataBazowa);

	$rok = $data->format('Y');
	$miesiac = $data->format('m');

	if ($nrMiesiaca <= $miesiac) {
	    $rok++;
	}

	$data->setDate($rok, $nrMiesiaca, 1);
	$data->setTime(0, 0);


	return $data->format(self::FORMAT_TIMESTAMP);
    }

    public static function zwiekszDateODni($ileDni, $date = null) {
	$data = self::dateAdapter($date);

	$data->add(new \DateInterval("P{$ileDni}D"));

	return $data;
    }

    public static function zmniejszDateODni($ileDni, $date = null) {
	$data = self::dateAdapter($date);

	$data->sub(new \DateInterval("P{$ileDni}D"));

	return $data;
    }

    public static function formatujDateNaTimestamp($strData) {
	return (new \DateTime($strData))->format(self::FORMAT_TIMESTAMP);
    }

    public static function pobierzDzienTygodnia(\DateTime $data) {
	return $data->format("w");
    }

    public static function poczatekDnia($dataLubString) {
	$data = self::dateAdapter($dataLubString);
	$data->setTime(0, 0, 0);

	return $data;
    }

    public static function koniecDnia($dataLubString) {
	$data = self::dateAdapter($dataLubString);
	$data->setTime(23, 59, 59);

	return $data;
    }

    private static function wykonajOdejmijInterwal($dataString, $czasPrzesunieciaWMinutach, $metoda) {
	$interval = \Pastmo\Wspolne\Utils\DateUtil::obliczInterwal($czasPrzesunieciaWMinutach);

	$data = self::dateAdapter($dataString);
	$data->$metoda($interval);
	return $data->format("Y-m-d H:i:s");
    }

}
