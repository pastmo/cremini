<?php

namespace Pastmo\Wspolne\Utils;

class EntityUtil {

    public static function czyEncjaZId($modelLubId) {
	$id = self::wydobadzId($modelLubId);
	return $id !== null;
    }

    public static function wydobadzId($modelLubId) {
	return self::wydobadzPole($modelLubId, 'id');
    }

    public static function wydobadzPole($modelLubWartosc, $nazwaPola) {
	if (is_object($modelLubWartosc)) {
	    return $modelLubWartosc->$nazwaPola;
	}
	return $modelLubWartosc;
    }

}
