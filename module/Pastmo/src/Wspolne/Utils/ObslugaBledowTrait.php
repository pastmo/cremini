<?php

namespace Pastmo\Wspolne\Utils;

trait ObslugaBledowTrait {

    protected $czyWyjatek;

    protected function zwrocBlad(\Exception $e) {
	if ($this->czyWyjatek) {
	    throw $e;
	}
	return false;
    }

}
