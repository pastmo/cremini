<?php

namespace Pastmo\Wspolne\Utils;

class DomUtil {

    public static function konwertujNaWiadomoscTekstowa($tresc) {
	$doc = new \DOMDocument();
	$doc->encoding = 'UTF-8';
	if (StrUtil::czyNiepusty($tresc) && @$doc->loadHTML($tresc)) {
	    self::removeElementsByTagName('script', $doc);
	    self::removeElementsByTagName('style', $doc);
	    self::removeElementsByTagName('link', $doc);
	    self::removeElementsByTagName('blockquote', $doc);

	    $tresc = $doc->saveHtml();
	    $tresc = strip_tags($tresc);
	}

	return $tresc;
    }

    public static function removeElementsByTagName($tagName, \DOMDocument $document) {
	$nodeList = $document->getElementsByTagName($tagName);
	for ($nodeIdx = $nodeList->length; --$nodeIdx >= 0;) {
	    $node = $nodeList->item($nodeIdx);
	    $node->parentNode->removeChild($node);
	}
    }

    public static function decode_qprint($str) {
	$str = preg_replace("/\=([A-F][A-F0-9])/", "%$1", $str);
	$str = urldecode($str);
	$str = iconv("ISO-8859-2", "UTF-8", $str);
	return $str;
    }

}
