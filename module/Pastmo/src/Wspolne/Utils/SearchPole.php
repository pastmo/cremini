<?php

namespace Pastmo\Wspolne\Utils;

class SearchPole {

    const QUERY = "query";
    const STRING = "string";
    const INT = "int";
    const FLOAT = "float";

    private $name;
    private $dataType;
    private $query;

    public function setName($name) {
	$this->name = $name;
	return $this;
    }

    public function getName() {
	return $this->name;
    }

    public function setDataType($dataType) {
	$this->dataType = $dataType;
	return $this;
    }

    public function getDataType() {
	return $this->dataType;
    }

    public function setQuery($query) {
	$this->query = $query;
	return $this;
    }

    public function getQuery() {
	return $this->query;
    }

    public static function create() {
	$pole = new SearchPole();
	return $pole;
    }

    public static function createTypuString($nazwa) {
	$pole = new SearchPole();
	$pole->setName($nazwa)->setDataType(self::STRING);
	return $pole;
    }

}
