<?php

namespace Pastmo\Wspolne\Utils;

trait GeneratorWidokow {

    protected $viewRender;
    protected $children = array();

    public function initGeneratorWidokow() {
	$this->viewRender = $this->sm->get('ViewRenderer');
	$this->children = [];
    }

    public function generujHtml($template_phtml, $params) {

	$viewRender = $this->sm->get('ViewRenderer');

	foreach ($this->children as $key => $child) {
	    $nazwa = "strona$key";
	    $html = $viewRender->render($child);

	    $params[$nazwa] = $html;
	}

	$model = new \Zend\View\Model\ViewModel($params);
	$model->setTemplate($template_phtml);

	return $this->viewRender->render($model);
    }

}
