<?php

namespace Pastmo\Wspolne\Utils;

class AdresUtil {

    const POLSKA = 'Polska';
    const CZECHY = 'Czechy';
    const ESTONIA = 'Estonia';
    const HISZPANIA = 'Hiszpania';
    const NIEMCY = 'Niemcy';
    const NORWEGIA = 'Norwegia';
    const USA = 'Stany Zjednoczone Ameryki';
    const WIELKABRYTANIA = 'Wielka Brytania';


    public static function zwrocTablicePanstw() {
	return [
	    self::POLSKA, self::CZECHY, self::ESTONIA, self::HISZPANIA,
	    self::NIEMCY, self::NORWEGIA, self::USA
	];
    }

    public static function pobierzKodPanstwa($nazwa) {
	switch ($nazwa) {
	    case "Polska":
		return "PL";
	    case "Czechy":
		return "CZ";
	    case "Estonia":
		return "EST";
	    case "Hiszpania":
		return "ESP";
	    case "Niemcy":
		return "DE";
	    case "Norwegia":
		return "NO";
	    case "Stany Zjednoczone Ameryki":
		return "USA";
	}
    }

}
