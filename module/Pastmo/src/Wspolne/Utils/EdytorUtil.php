<?php

namespace Pastmo\Wspolne\Utils;

class EdytorUtil {

    public static function pobierzKodyZasobow($tekst) {
	preg_match_all('/\!\(\w+\.?\w+\)\[\d+\]/', $tekst, $matches);
	return ArrayUtil::pobierzPierwszyElement($matches, []);
    }

    public static function opakujImg($url) {
	return '<img src="'.$url.'">';
    }

}
