<?php

namespace Pastmo\Wspolne\Helper;

use Zend\View\Helper\AbstractHelper;

class PobierzUstawienie extends AbstractHelper {

    private $ustawieniaSystemuMenager;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm) {
	$this->translator = $sm->get('MvcTranslator');
	$this->ustawieniaSystemuMenager = $sm->get(\Pastmo\Wspolne\Menager\UstawieniaSystemuMenager::class);
    }

    public function __invoke($kod, $domyslnie) {
	$ustawienie = $this->ustawieniaSystemuMenager->pobierzUstawieniePoKodzie($kod, $domyslnie);
	return $ustawienie->wartosc;
    }

}
