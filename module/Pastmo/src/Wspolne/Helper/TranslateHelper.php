<?php

namespace Pastmo\Wspolne\Helper;

use Zend\View\Helper\AbstractHelper;

class TranslateHelper extends AbstractHelper {

    use \Pastmo\Wspolne\Utils\TranslateTrait;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm) {
	$this->translator = $sm->get('MvcTranslator');
	$this->constantTranslator = $sm->get(\Lokalizacje\Menager\ConstantTranslatorMenager::class);
    }

}
