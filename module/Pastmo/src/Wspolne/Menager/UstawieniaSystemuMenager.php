<?php

namespace Pastmo\Wspolne\Menager;

use Pastmo\Wspolne\Entity\UstawienieSystemu;

class UstawieniaSystemuMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    protected $wyszukiwanieZKontem = false;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm, \Pastmo\Wspolne\KonfiguracjaModulu::USTAWIENIA_SYSTEMU_GATEWAY);
    }

    public function pobierzUstawieniePoKodzie($kod, $domyslnaWartosc = false) {
	$wynik = $this->pobierzZWherem("kod='$kod'");

	if (count($wynik) > 0) {
	    return $wynik[0];
	}

	return UstawienieSystemu::create($domyslnaWartosc);
    }

}
