<?php

namespace Pastmo\Wspolne\Menager;

use Pastmo\Wspolne\Utils\StrUtil;

class PobieraczAkcjiZKlasyKontrolera extends WspolnyMenager {

    private $testowanaKlasa;
    private $adnotacjeModuleMenager;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm);
	$this->adnotacjeModuleMenager = new \Pastmo\Wspolne\Menager\AdnotacjeModeluMenager();
    }

    public function pobierzZwyklaAkcjeZKontrolera($klasaKontrolera) {
	$this->testowanaKlasa = $klasaKontrolera;

	$refleksja = new \Zend\Code\Reflection\ClassReflection($klasaKontrolera);
	$metody = $refleksja->getMethods();
	$wynik = array();

	foreach ($metody as $metoda) {
	    $shortName = $metoda->getShortName();

	    if ($this->czyIgnorowanaMetoda($shortName)) {
		continue;
	    }

	    $helper = $this->sm->get('ViewHelperManager')->get('url');

	    $route = StrUtil::klasaNaRoute($klasaKontrolera);
	    $link = $helper->__invoke($route, array('action' => StrUtil::nazwaAkcjiNaRoute($shortName)));
	    $wynik[] = $link;
	}
	return $wynik;
    }

    private function czyIgnorowanaMetoda($shortName) {
	if (strpos($shortName, "Action") == false) {
	    return true;
	}
	if (in_array($shortName, array('notFoundAction', 'getMethodFromAction'))) {
	    return true;
	}
	if ($this->adnotacjeModuleMenager->sprawdzCzyMetodaMaUstawionyJsonModel($this->testowanaKlasa, $shortName)) {
	    return true;
	}

	return false;
    }

}
