<?php

namespace Pastmo\Wspolne\Menager;

use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;

abstract class WspolnyMenagerBazodanowyEventowy extends \Wspolne\Menager\BazowyMenagerBazodanowy implements EventManagerAwareInterface {

    const ZAPISZ = 'zapisz';
    const UPDATE = 'update';

    protected $events;

    public function zapisz(\Pastmo\Wspolne\Entity\WspolnyModel $model) {
	parent::zapisz($model);

	if ($model->id === NULL) {
	    $model = $this->getPoprzednioDodany();
	}
	$this->getEventManager()->trigger(__FUNCTION__, $this, $model);
    }

    public function update($id, $set) {
	parent::update($id, $set);

	$model = $this->getRekord($id);
	$model->updateSet = $set;
	$this->getEventManager()->trigger(__FUNCTION__, $this, $model);
    }

    public function updateWhere($set, $whereArray) {
	parent::updateWhere($set, $whereArray);

	$table = $this->tableGateway->getTable();

	$this->getEventManager()->trigger(__FUNCTION__, $this,
		['set' => $set, 'table' => $table, 'where' => $whereArray]);
    }

    public function setEventManager(EventManagerInterface $events) {
	$events->setIdentifiers([
		__CLASS__,
		get_class($this)
	]);
	$this->events = $events;
    }

    public function getEventManager() {
	if (!$this->events) {
	    $this->setEventManager(new EventManager());
	}
	return $this->events;
    }

    public function obsluzPowiadomienia($encja) {

    }

}
