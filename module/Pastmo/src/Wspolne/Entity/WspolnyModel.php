<?php

namespace Pastmo\Wspolne\Entity;

abstract class WspolnyModel {

    const _ID = "_id";
    const ID = "id";
    const EXTRA_ID = 'exta_id';
    const EXTRA = 'exta_';
    const NO_DATABASE = 'NO_DATABASE';

    protected $sm;
    protected $data;
    public $id;
    public $wyswietlanie_js = '';
    protected $aktualizuj = false;

    public function __construct($sm = null) {
	$this->sm = $sm;
    }

    public function zrobArray() {
	$class_vars = $this->pobierzSkladnikiEncji();
	$result = array();
	foreach ($class_vars as $name => $value) {

	    $kolumnaWTabeli = $this->konwertujNaKolumneDBWKlasachBazowych($name);
	    $kolumnaWTabeli = $this->konwertujNaKolumneDB($kolumnaWTabeli);


	    if ($this->$name === null || $this->czyIgnorowane($name)) {
		continue;
	    } else if (is_object($this->$name) && property_exists(get_class($this->$name), 'id') && $this->$name->id) {
		$result[$kolumnaWTabeli] = $this->pobierzKluczGlownyEncji($name);
	    } else if (!is_array($this->$name) && !is_object($this->$name)) {
		$result[$kolumnaWTabeli] = $this->$name;
	    }
	}
	return $result;
    }

    protected function pobierzKluczGlownyEncji($name) {
	return $this->$name->id;
    }

    public function pobierzSkladnikiEncji() {
	return get_class_vars($this->pobierzKlase());
    }

    public function czyIgnorowane($name) {
	$ignorowane = array_merge(array("aktualizuj", "wyswietlanie_js", "sm", "data"), $this->dodatkowoIgnorowane());
	return in_array($name, $ignorowane);
    }

    protected function dodatkowoIgnorowane() {
	return array();
    }

    public function pobierzLubNull($data, $key, $wartoscDomyslna = null) {

	if ($this->aktualizuj && isset($this->$key)) {
	    $wartoscDomyslna = $this->$key;
	}

	if($this->aktualizuj){
	    $wartoscDomyslna = null;
	}

	return ($this->kluczIstnieje($data, $key)) ? $data[$this->pobierzPrzedrostek() . $key] : $wartoscDomyslna;
    }

    private function kluczIstnieje($data, $key) {
	if($this->aktualizuj){
	    return isset($data[$this->pobierzPrzedrostek() . $key]);
	}
	return !empty($data[$this->pobierzPrzedrostek() . $key]);
    }

    public function aktualizujArray($data) {
	$this->aktualizuj = true;

	$this->exchangeArray($data);
    }

    abstract function exchangeArray($data);

    abstract function pobierzKlase();

    protected function konwertujNaKolumneDBWKlasachBazowych($nazwaWKodzie) {
	return $nazwaWKodzie;
    }

    abstract function konwertujNaKolumneDB($nazwaWKodzie);

    protected function ustawExtraId($data) {
	if (!empty($data[self::EXTRA_ID])) {
	    $this->id = $data[self::EXTRA_ID];
	}
    }

    protected function ustawExtra($data, $klucz) {
	$extraKlucz = self::getExtraKey($klucz);
	if (!empty($data[$extraKlucz])) {
	    $this->$klucz = $data[$extraKlucz];
	}
    }

    public static function getExtraKey($klucz) {
	return self::EXTRA . $klucz;
    }

    public function pobierzPrzedrostek() {
	return "";
    }

    public function ustawPrzedrostek($przedrostek) {
	$this->przedrostekZFormularza = $przedrostek;
    }

    protected function czyTabeleDostepne() {
	return $this->sm !== null;
    }

    protected function pobierzTabeleObca($klucz, $table, $domyslnie, $kluczWEncji = false) {

	if ($this->aktualizuj && !$this->czyDataZawieraKlucz($klucz)) {
	    if (!$kluczWEncji) {
		$kluczWEncji = str_replace('_id', '', $klucz);
	    }
	    return $this->$kluczWEncji;
	}

	if (!$this->czyTabeleDostepne()) {
	    return $domyslnie;
	}

	$id = $this->czyDataZawieraKlucz($klucz) ? $this->data[$klucz] : null;
	if ($id) {
	    $obiektTable = $this->sm->get($table);
	    return $obiektTable->getRekord($id);
	} else {
	    return $domyslnie;
	}
    }

    protected function czyDataZawieraKlucz($klucz) {
	$wynik = isset($this->data[$klucz]);
	return $wynik;
    }

    protected function pobierzListeZTabeliObcej($menagerClass, $metodaMenagera, $parametr1, $parametr2,
	    $poleWyniku, $wymusZaladowanie = false) {
	if (!$this->czyTabeleDostepne()) {
	    $this->$poleWyniku = array();
	} else {
	    $menager = $this->sm->get($menagerClass);
	    $wynik = $menager->$metodaMenagera($parametr1, $parametr2);
	    $this->$poleWyniku = $wynik;
	}
    }

    protected function pobierzTabeleObcaZKluczemObcym($klucz, $table, $domyslnie, $kluczObcy) {

	$wartosc = isset($this->data[$klucz]) ? $this->data[$klucz] : null;

	if ($wartosc && $this->czyTabeleDostepne()) {
	    $obiektTable = $this->sm->get($table);
	    $wynikArr = $obiektTable->pobierzZWherem("$kluczObcy='$wartosc'", "$kluczObcy ASC");
	    if (count($wynikArr) > 0) {
		return $wynikArr[0];
	    }
	}
	return $domyslnie;
    }

    protected function getZalogowanego() {
	if ($this->czyTabeleDostepne()) {
	    $uzytkownikTable = $this->sm->get(\Logowanie\Menager\UzytkownicyMenager::getClass());
	    return $uzytkownikTable->getZalogowanegoUsera();
	}
	return false;
    }

    protected function pobierzSamaDate($timestamp) {
	return date('Y-m-d', strtotime($timestamp));
    }

    public function setSm($sm) {
	$this->sm = $sm;
    }

}
