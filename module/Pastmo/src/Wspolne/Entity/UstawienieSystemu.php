<?php

namespace Pastmo\Wspolne\Entity;

class UstawienieSystemu extends WspolnyModel {

    public $id;
    public $kod;
    public $wartosc;

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->kod = $this->pobierzLubNull($data, 'kod');
	$this->wartosc = $this->pobierzLubNull($data, 'wartosc');
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	return $nazwaWKodzie;
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public static function create($wartosc) {
	$ustawienie = new UstawienieSystemu();
	$ustawienie->wartosc = $wartosc;
	return $ustawienie;
    }

}
