<?php

namespace Pastmo\Wspolne\Enumy;

class KluczeGet {

    const WYSZUKAJ = 'wyszukaj';
    const SORT_KLUCZ = 'sort_klucz';
    const SORT_TYP = 'sort_typ';
    const STRONA = 'strona';
    const LICZBA_WIERSZY = 'liczba_wierszy';

}
