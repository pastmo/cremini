<?php

namespace Pastmo\Testy\FabrykiKonkretnychEncji;

class JezykFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Pastmo\Uzytkownicy\Entity\Jezyk();
	$encja->kod = $this->getUnikalyNapis("JF");
	$encja->nazwa = __CLASS__;
	$encja->skrot = "sk";
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Pastmo\Uzytkownicy\Menager\JezykiMenager::class;
    }

}
