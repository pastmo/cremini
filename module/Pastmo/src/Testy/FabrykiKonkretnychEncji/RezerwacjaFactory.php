<?php

namespace Pastmo\Testy\FabrykiKonkretnychEncji;

class RezerwacjaFactory extends \Pastmo\Testy\Fabryki\FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Magazyn\Entity\Rezerwacja();

	$encja->ilosc = 1;
	$encja->data_wygasniecia = date("Y-m-d", strtotime("+1 week"));
	$encja->typ = \Magazyn\Enumy\RezerwacjeTyp::TECHNICZNA;

	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Magazyn\Menager\RezerwacjeMenager::class;
    }

    public function dowiazInneTabele() {
	parent::dowiazInneTabele();

	$this->encja->produkt_magazyn = $this->uzyjWlasciwejFabryki(\BazaProduktow\Entity\ProduktMagazyn::class);
	$this->encja->uzytkownik = $this->uzyjWlasciwejFabryki(\Logowanie\Model\Uzytkownik::class);
	$this->encja->produkt = $this->encja->produkt_magazyn->produkt_wariacja->produkt;
    }

    public function uzyjWlasciwejFabryki($klasa, $parametry = array()) {

	return \FabrykaRekordow::makeEncje($klasa,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane::create($this->parametryFabryki->sm)->setparametry($parametry));
    }

}
