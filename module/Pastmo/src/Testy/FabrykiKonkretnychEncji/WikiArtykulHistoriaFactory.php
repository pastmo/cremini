<?php

namespace Pastmo\Testy\FabrykiKonkretnychEncji;

class WikiArtykulHistoriaFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Pastmo\Wiki\Entity\WikiArtykulHistoria();
	$encja->wartosc = __CLASS__;
	$encja->kod = __CLASS__;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Pastmo\Wiki\Menager\WikiArtykulyHistoriaMenager::class;
    }

    public function dowiazInneTabele() {
	$this->encja->wiki_artykul = $this->uzyjWlasciwejFabryki(\Pastmo\Wiki\Entity\WikiArtykul::class);
	$this->encja->autor = $this->uzyjWlasciwejFabryki(\Logowanie\Model\Uzytkownik::class);
	$this->encja->avatar = $this->uzyjWlasciwejFabryki(\Pastmo\Zasoby\Entity\Zasob::class);
    }
}
