<?php

namespace Pastmo\Testy\FabrykiKonkretnychEncji;

class EmailFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Pastmo\Email\Entity\Email();
	$encja->nadawca = "nadawca <email@nadawcy.pl>";
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Pastmo\Email\Menager\EmaileMenager::class;
    }

    public function dowiazInneTabele() {
	$this->encja->konto_mailowe = $this->uzyjWlasciwejFabryki(\Pastmo\Email\Entity\KontoMailowe::class);
    }

}
