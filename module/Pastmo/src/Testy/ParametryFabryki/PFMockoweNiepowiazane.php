<?php

namespace Pastmo\Testy\ParametryFabryki;

class PFMockoweNiepowiazane extends ParametryFabryki {

    public function __construct($sm = null) {
	$this->sm = $sm;
	$this->czyFabrykaMockow = true;
	$this->zapiszDoBazy = false;
	$this->czyGenerowaniePowiazanychEncji = false;
	$this->parametry = array();
    }

    public static function create($sm = null) {
	return new PFMockoweNiepowiazane($sm);
    }

}
