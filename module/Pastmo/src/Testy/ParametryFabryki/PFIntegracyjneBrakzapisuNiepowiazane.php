<?php

namespace Pastmo\Testy\ParametryFabryki;

class PFIntegracyjneBrakzapisuNiepowiazane extends ParametryFabryki {

    public function __construct() {
	$this->czyFabrykaMockow = false;
	$this->zapiszDoBazy = false;
	$this->czyGenerowaniePowiazanychEncji = false;
	$this->parametry = array();
    }

    public static function create() {
	return new PFIntegracyjneBrakzapisuNiepowiazane();
    }

}
