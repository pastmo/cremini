<?php

namespace Pastmo\Testy\TraityPomocnicze;

trait UstawianieParametrowWTestach {

    public function arrayToParameters(array $post) {
	$parameters = new \Zend\Stdlib\Parameters($post);
	return $parameters;
    }

}
