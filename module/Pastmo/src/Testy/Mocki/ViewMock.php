<?php

namespace Pastmo\Testy\Mocki;

class ViewMock {

    public function url() {

    }

    public function basePath($url = "") {
	return $url;
    }

    public function dodajWysiwyg() {

    }

    public function selected() {

    }

    public function wyswietlOknoParametrowWizualizacji() {

    }
    public function uprawnienia() {
	return true;
    }

}
