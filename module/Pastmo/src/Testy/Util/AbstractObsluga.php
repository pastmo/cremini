<?php
namespace Pastmo\Testy\Util;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class AbstractObsluga extends AbstractHttpControllerTestCase {

	protected $abstractMoctTest;

	public function init($abstractMoctTest) {
		$this->abstractMoctTest = $abstractMoctTest;
	}

	public function ustawMetode($menagerClass, $metoda, $wynik, $expects = false) {
		$mock = $this->abstractMoctTest->sm
				->get($menagerClass);

		$metodaExpects = null;
		if (!$expects) {
			$metodaExpects = $this->any();
		} else {
			$metodaExpects = $this->exactly($expects);
		}

		$mockBuilder = $mock->expects($metodaExpects)
				->method($metoda)
				->will($this->returnValue($wynik))
				;

		return $mockBuilder;
	}

}
