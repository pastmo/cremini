<?php
namespace Pastmo\Testy\Util;

class ObslugaEncji  extends AbstractObsluga {

	public function exchangeArrayPorownanieKluczy($klucz, $klucz2) {
		$value = "wartosc:" . $klucz;
		$data = array($klucz => $value);

		$opinia = $this->nowyObiekt();
		$opinia->exchangeArray($data);

		$this->assertEquals($opinia->$klucz2, $value);
	}

}
