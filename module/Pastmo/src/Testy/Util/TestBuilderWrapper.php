<?php

namespace Pastmo\Testy\Util;

trait TestBuilderWrapper {

    public $defaultFactoryParameters;
    public $lastEntity;

    public function makeEntityBuilder($class) {
	$builder = new TB();
	$builder->setDefaultPf($this->defaultFactoryParameters);
	$builder->setEntityClass($class);
	$builder->runer = $this;

	return $builder;
    }

    public function setDefaultFactoryParameters(\Pastmo\Testy\ParametryFabryki\ParametryFabryki $pf) {
	$this->defaultFactoryParameters = $pf;
    }

}
