<?php

namespace Pastmo\Testy\BazoweKlasyTestow;

use Pastmo\Testy\Util\TB;

class IntegracyjnyTestController extends WspolnyIntegracyjnyTest {

    protected $zalogowany;

    public function setUp() {
	parent::setUp();

	$this->zalogujAdmina();
	$this->ustawPustyLayout();
    }

    private function zalogujAdmina() {
	$this->zalogowany = TB::create(\Logowanie\Model\Uzytkownik::class, $this)
		->setParameters(array('rola' => 1))
		->make();
	$this->ustawZalogowanegoUsera($this->zalogowany->id);
    }

    private function ustawPustyLayout() {
	\Pastmo\Controller\WspolnyController::$PUSTY_LAYOUT = true;
    }

}
