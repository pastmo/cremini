<?php

namespace Pastmo\Testy\GeneratoryTestow;

class MapaUprawnien {

    public function uprawnienia() {
	return array();
    }

    public $nieznalezioneUprawnienia = array();
    public $klasyKontrolerow = array();
    public $akcjeBezSprawdzanychUprawnien = array();

    public function pobierzUprawnienie($akcja, $czyAjax) {
	if (isset($this->uprawnienia()[$akcja])) {
	    return $this->uprawnienia()[$akcja];
	} else {
	    $tu = TestUprawnienie::domyslneUprawnienie($akcja, $czyAjax);
	    $this->nieznalezioneUprawnienia[$akcja] = $tu;
	    return $tu;
	}
    }

    public function czyIgnorowanaAkcja($akcja) {
	return in_array($akcja, $this->akcjeBezSprawdzanychUprawnien);
    }

}
