<?php

namespace Pastmo\Testy\GeneratoryTestow;

abstract class AbstrakcyjnyGeneratorTestow extends \Pastmo\Wspolne\Menager\WspolnyMenager {

    protected $class;
    protected $metody = array();

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm);
    }

    public function generuj() {

	$this->utworzMetodeSetUp();
	$this->utworzPozostaleWymaganeMetody();
	$this->utworzClass();
	$this->generujWlasciweMetody();
	$this->dodajWszystkieMetodyDoKlasy();
	$this->zapiszKlaseDoPliku();
	$this->wyswietlPodsumowanie();
    }

    protected function utworzMetodeSetUp() {
	$this->dodajMetode('setUp', 'parent::setUp();');
    }

    protected function utworzPozostaleWymaganeMetody() {
	$this->dodajMetode('testIndex', '');
    }

    protected function dodajMetode($nazwa, $body, $index = 0) {
	$biezacaNazwa=$nazwa . ($index > 0 ? $index : "");

	$method = new \Zend\Code\Generator\MethodGenerator();
	$method->setName($biezacaNazwa)
		->setBody($body);
	if (isset($this->metody[$biezacaNazwa])) {
	    $index++;
	    $this->dodajMetode($nazwa, $body, $index);
	} else {
	    $this->metody[$biezacaNazwa] = $method;
	}
    }

    protected function utworzClass() {
	$this->class = new \Zend\Code\Generator\ClassGenerator();
	$this->class->setName($this->getNazwaKlasy())
		->setNamespaceName($this->getNamespace())
		->setExtendedClass(\Testy\BazoweKlasyTestow\WspolnyControllerCommonTest::class)
	;
    }

    protected function dodajWszystkieMetodyDoKlasy() {
	foreach ($this->metody as $method) {
	    $this->class->addMethodFromGenerator($method);
	}
    }

    protected function zapiszKlaseDoPliku() {
	$file = new \Zend\Code\Generator\FileGenerator();
	$file->setClass($this->class);
	file_put_contents($this->getPathPlikuWynikowego(), $file->generate());
    }

    protected function zrobNazweMetodyTestowejZAkcji($akcja) {
	$result = str_replace("\\", "_", $akcja);
	$result = str_replace("/", "_", $result);
	$result = str_replace("-", "_", $result);
	return 'test_' . $result;
    }

    abstract function generujWlasciweMetody();

    abstract function getNazwaKlasy();

    abstract function getNamespace();

    abstract function getPathPlikuWynikowego();

    abstract function wyswietlPodsumowanie();
}
