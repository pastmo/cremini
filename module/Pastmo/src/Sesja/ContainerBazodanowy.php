<?php

namespace Pastmo\Sesja;

class ContainerBazodanowy extends \Zend\Session\Container {

    private $podreczneDaneMenager;
    public $sesjaMenager;


    public function __construct(string $namespace, \Zend\ServiceManager\ServiceManager $sm = null) {   
        $this->podreczneDaneMenager = $sm->get(Menager\PodreczneDaneMenager::class);  
        $this->podreczneDaneMenager->setNamespace($namespace);
    }
     
    public function offsetGet($key) {       
        $wynik = $this->podreczneDaneMenager->zwrocDaneZBazy($key);
        $json_decode = json_decode($wynik->wartosc, true);
        return $json_decode;
    }
    
    public function offsetSet($key, $value) {        
        $this->podreczneDaneMenager->dodajNoweDane($key, $value);       
    }       
    
    public function offsetExists($key) {
        $wynik = $this->podreczneDaneMenager->zwrocDaneZBazy($key);
        return $wynik;
    }
}