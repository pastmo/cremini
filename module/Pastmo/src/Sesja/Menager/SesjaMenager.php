<?php

namespace Pastmo\Sesja\Menager;

use Zend\Session\Container;
use Zend\Session\SessionManager;

class SesjaMenager extends \Pastmo\Wspolne\Menager\WspolnyMenager {

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm);
    }

    public function createContainer(string $namespace) {
	return new Container($namespace);
    }
    
    public function createContainerBazodanowy(string $namespace, \Zend\ServiceManager\ServiceManager $sm) {
	return new \Pastmo\Sesja\ContainerBazodanowy($namespace, $sm);
    }
    
    public function ustawZawartoscKlucza(Container $container, string $key, $value) {
	$container->offsetSet($key, $value);
    }

    public function sprawdzCzyIstniejeKlucz(Container $container, string $key) {
	return $container->offsetExists($key);
    }

    public function usunZawartoscKlucza(Container $container, string $key) {
	if ($container->offsetExists($key)) {
	    $container->offsetUnset($key);
	}
    }

    public function zwrocZawartoscKluczaLubFalsz(Container $container, string $key) {
	if ($container->offsetExists($key)) {
	    return $container->offsetGet($key);
	} else {
	    return false;
	}
    }

    public function ustawKluczBooleanLubUsun(Container $container, string $key, bool $value) {
	if ($value) {
	    $this->ustawZawartoscKlucza($container, $key, $value);
	} else {
	    $this->usunZawartoscKlucza($container, $key);
	}
    }

    public function ustawKluczLubUsun(Container $container, string $key, $value) {
	if (!empty($value)) {
	    $this->ustawZawartoscKlucza($container, $key, $value);
	} else {
	    $this->usunZawartoscKlucza($container, $key);
	}
    }

    public function zakonczSesje() {
	$container = $this->createContainer('destroy');
	$sessionManager = $container->getManager();
	$sessionManager->destroy();
    }

}
