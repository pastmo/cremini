<?php

namespace Pastmo\Sesja;

class KonfiguracjaModulu extends \Pastmo\KonfiguracjaModulu {

    const PODRECZNE_DANE_GATEWAY = "PODRECZNE_DANE_GATEWAY";
    
    public function getFactories() {
	return array(
		Menager\SesjaMenager::class => function($sm) {
		    $menager = new Menager\SesjaMenager($sm);
		    return $menager;
		},
		\Wspolne\Menager\SesjaMenager::class => function($sm) {
		    $menager = new \Wspolne\Menager\SesjaMenager($sm);
		    return $menager;
		},
                Menager\PodreczneDaneMenager::class => function($sm) {
                    return new Menager\PodreczneDaneMenager ($sm);
                },
                self::PODRECZNE_DANE_GATEWAY => function ($sm) {
                    return $this->ustawGateway($sm, new \Pastmo\Sesja\Entity\PodreczneDane($sm), 'podreczne_dane');
                },                    
                
	);
    }

}
