<?php

namespace Pastmo;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

trait FunkcjeKonfiguracyjne {

    public function ustawGateway($sm, $kasaEncji, $table) {
	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
	$resultSetPrototype = new ResultSet();
	$resultSetPrototype->setArrayObjectPrototype(new $kasaEncji($sm));
	return new TableGateway($table, $dbAdapter, null, $resultSetPrototype);
    }

}
