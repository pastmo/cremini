<?php

namespace Pastmo\Uzytkownicy\Enumy;

class KodyJezykow {

    const POLSKI = "pl_PL";
    const ANGIELSKI = "en_US";
    const DOMYSLNY = self::ANGIELSKI;

}
