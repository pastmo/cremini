<?php

namespace Pastmo\Uzytkownicy\Menager;

class JezykiMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    protected $wyszukiwanieZKontem = false;
    
    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm, \Pastmo\Uzytkownicy\KonfiguracjaModulu::JEZYKI_GATEWAY);
    }

    public function pobierzDostepneJezyki() {
	return $this->fetchAll('skrot ASC');
    }

}
