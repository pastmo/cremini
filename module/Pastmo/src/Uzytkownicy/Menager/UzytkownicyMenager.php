<?php

namespace Pastmo\Uzytkownicy\Menager;

use Zend\ServiceManager\ServiceManager;
use Pastmo\Wspolne\Utils\SearchPole;
use Logowanie\Model\Uzytkownik;
use Wspolne\Menager\SesjaMenager;
use Pastmo\Wspolne\Exception\PastmoException;

class UzytkownicyMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    private $uzytkownik;
    private $sesjaMenager;

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, 'UzytkownikTableGateway');

	$this->sesjaMenager = $this->sm->get(SesjaMenager::class);
    }

    public function pobierzUzytkownikowZUprawnieniamiBezZalogowanego($kodUprawnienia) {
	$zalogowany = $this->getZalogowanegoUsera();
	return $this->pobierzZWherem($this->whereDoUzytkownikowZUprawnieniami($kodUprawnienia) . " AND id!={$zalogowany->id}");
    }

    public function pobierzUzytkownikowZUprawnieniami($kodUprawnienia, $order = self::DOMYSLNY_ORDER,
	    $limit = false) {
	return $this->pobierzZWherem($this->whereDoUzytkownikowZUprawnieniami($kodUprawnienia), $order, $limit);
    }

    public function pobierzLiczbeUzytkownikowZUprawnieniami($kodUprawnienia) {
	return $this->pobierzZWheremCount($this->whereDoUzytkownikowZUprawnieniami($kodUprawnienia));
    }

    protected function whereDoUzytkownikowZUprawnieniami($kodUprawnienia) {
	return "rola_id IN (
											SELECT rola_id
											FROM uprawnienia_role
											WHERE uprawnienie_kod='$kodUprawnienia')";
    }

    public function zapisz(\Pastmo\Wspolne\Entity\WspolnyModel $model) {
	$model = $this->zaktualizujTyp($model);

	if (!\Pastmo\Wspolne\Utils\EntityUtil::wydobadzId($model->rola)) {
	    $model->rola = $this->pobierzRolePoKodzie(\Logowanie\Entity\KodyRol::Pusta);
	}
	parent::zapisz($model);
    }

    private function zaktualizujTyp($model) {
	$rola = $model->rola;
	if ($rola) {
	    if (!$rola instanceof \Logowanie\Entity\Rola) {
		$rola = $this->get(\Logowanie\Menager\RoleMenager::class)->getRekord($rola);
	    }
	    $model->typ = $rola->typ;
	}

	return $model;
    }

    protected function pobierzPolaWyszukiwania() {
	return array(
		SearchPole::create()->setDataType(SearchPole::STRING)->setName("login"),
		SearchPole::create()->setDataType(SearchPole::STRING)->setName("imie"),
		SearchPole::create()->setDataType(SearchPole::STRING)->setName("nazwisko"),
	);
    }

    protected function konwertujNaRekordWyszuiwania($encja) {
	$wynik = new \Pastmo\Wspolne\Entity\RekordWyszukiwania();

	$wynik->value = $encja . "";
	$wynik->tokens = array($encja->imie, $encja->nazwisko, $encja->login);
	$wynik->url = $this->generujUrl('logowanie', array('action' => 'profil', 'id' => $encja->id));
	$wynik->type = "Użytkownicy";
	$wynik->id = $encja->id;
	$wynik->menager = \Logowanie\Menager\UzytkownicyMenager::class;

	return $wynik;
    }

    public function zaloguj($mail, $haslo, $post = null) {
	try {
	    $this->zalogujTry($mail, $haslo, $post);
	    return $this->uzytkownik;
	} catch (\Exception $e) {
	    \Wspolne\Controller\ElementyWidokuController::dodajWiadomosc($e->getMessage());
	    $this->sesjaMenager->ustawIdUzytkownika();
	    return false;
	}
    }

    private function zalogujTry($mail, $haslo, $post = null) {
	$this->sprawdzCaptcha($post);
	$this->pobierzUzytkownika($mail, $haslo);
	$this->sprawdzCzyIstnieje();
	$this->sprawdzCzyAktywny();
	$this->sesjaMenager->ustawIdUzytkownika($this->uzytkownik->id);
    }

    private function sprawdzCaptcha($post) {
	if (isset($post['g-recaptcha-response'])) {
	    $reCaptchaService = $this->sm->get('ReCaptchaService');
	    if (!$reCaptchaService->isValid($post['g-recaptcha-response'], $post)) {
		throw new PastmoException("Nie powiodła się walidacja ReCaptcha.");
	    }
	}
    }

    private function pobierzUzytkownika($mail, $haslo) {
	$md5Haslo = (new Uzytkownik())->kodujHaslo($haslo);
	$rowset = $this->tableGateway->select(array('mail' => $mail, 'haslo' => $md5Haslo));
	$this->uzytkownik = $rowset->current();
    }

    private function sprawdzCzyIstnieje() {
	if (!$this->uzytkownik) {
	    throw new PastmoException("Błędny login lub hasło");
	}
    }

    private function sprawdzCzyAktywny() {
	if (!$this->uzytkownik->czy_aktywny) {
	    throw new PastmoException("Użytkownik nieaktywny");
	}
    }

    public function wyloguj() {
	$this->sesjaMenager->zakonczSesje();
    }

    public function getZalogowanegoUsera() {
	$userId = $this->sesjaMenager->pobierzIdUzytkownika();
	if (!empty($userId)) {
	    return $this->getRekord($userId);
	}
	return false;
    }

    public function zapiszZPosta($post) {

	if (is_object($post)) {
	    $post = (array) $post;
	}

	$rejestracjaMenager = $this->get(\Logowanie\Menager\RejestracjaMenager::class);
	$post = $rejestracjaMenager->konwertujPost($post);

	$uzytkownik = null;
	if (isset($post['id'])) {
	    $uzytkownik = $this->getRekord($post['id']);
	} else {
	    $uzytkownik = new Uzytkownik($this->sm);
	}

	$uzytkownik->aktualizujArray($post);
	$uzytkownik->zmienHaslo($post);

	$zasobyTable = $this->sm->get(\Zasoby\Menager\ZasobyUploadMenager::class);
	$zasoby = $zasobyTable->zaladujPlik();

	foreach ($zasoby as $zasob) {
	    $uzytkownik->avatar = $zasob;

	    break;
	}

	$this->zapisz($uzytkownik);
    }

    public function pobierzPracownikow() {
	$klientRola = $this->pobierzRoleKLient();

	$wynik = $this->pobierzZWherem("rola_id!='" . $klientRola->id . "'");

	return $wynik;
    }

    private function pobierzRoleKLient() {
	$roleMenager = $this->sm->get(\Logowanie\Menager\RoleMenager::class);
	return $roleMenager->pobierzRolePoNazwie(\Logowanie\Entity\Rola::KLIENT);
    }

    protected function pobierzRolePoKodzie($kod) {
	$roleMenager = $this->sm->get(\Logowanie\Menager\RoleMenager::class);
	return $roleMenager->pobierzRolePoKodzie($kod);
    }

    public function pobierzUzytkownikowZProjektowBezZalogowanego() {
	$zalogowany = $this->getZalogowanegoUsera();
	return $this->pobierzZWherem("id IN (select uzytkownik_id from uzytkownicy_projekty where projekt_id IN(select projekt_id from uzytkownicy_projekty where uzytkownik_id={$zalogowany->id}) ) AND id!={$zalogowany->id}");
    }

    public function pobierzUzytkownikaPoMailu($mail) {
	$uzytkownicy = $this->pobierzZWherem("mail='$mail'");
	if (count($uzytkownicy) > 0) {
	    return $uzytkownicy[0];
	} else {
	    return $this->createUzytkownik();
	}
    }

    public function createUzytkownik() {
	$uzytkownik = new Uzytkownik();
	$uzytkownik->typ = \Logowanie\Model\TypyUzytkownikow::KLIENT;

	return $uzytkownik;
    }

    public function zarejestrujLubUaktualnijZBuildera(\Wspolne\Interfejs\UzytkownikBuilderInterfejs $builder) {
	$uzytkownik = $this->pobierzUzytkownikaPoMailu($builder->getEmail());

	$uzytkownik->mail = $builder->getEmail();
	$uzytkownik->setImieINazwisko($builder->getImieINazwisko());
	$uzytkownik->telefon = $builder->getTelefon();

	$this->zarejestrujLubUaktualnij($uzytkownik);

	if (!$uzytkownik->id) {
	    $uzytkownik = $this->getPoprzednioDodany();
	}

	return $uzytkownik;
    }

    public function zarejestrujLubUaktualnij(Uzytkownik $uzytkownik) {
	if ($uzytkownik->id) {
	    $this->zapisz($uzytkownik);
	} else {
	    $this->getLogowanieFasada()->zarejestrujUzytkownika($uzytkownik);
	}
    }

    public function pobierzUzytkownikowZRola($rolaId) {
	return $this->pobierzZWherem("rola_id =$rolaId", 'imie ASC');
    }

    public function zmienJezykUzytkownika($uzytkownikId, $jezykKod) {
	$this->update($uzytkownikId, array('jezyk_kod' => $jezykKod));
    }

}
