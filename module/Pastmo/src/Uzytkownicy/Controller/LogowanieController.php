<?php

namespace Pastmo\Uzytkownicy\Controller;

use Logowanie\Enumy\KodyUprawnien;
use Zend\View\Model\ViewModel;

abstract class LogowanieController extends \Wspolne\Controller\WspolneController {

    public function logowanieAction() {

	$dane = $this->getPost();
	if (isset($dane['uzytkownik'])) {
	    $user = $this->uzytkownikTable->zaloguj($dane['uzytkownik']['login'], $dane['uzytkownik']['haslo']);

	    if ($user) {
		return $this->przekieruj($dane);
	    } else {
		return $this->domyslnePrzekierowanieNiezalogowanego();
	    }
	} else {
	    return $this->domyslnePrzekierowanieNiezalogowanego();
	}
    }

    public function rejestracjaAction() {
	$wynikPrzetwarzanie = null;
	$post = $this->getPost();

	if ($post) {

	    $wynikPrzetwarzanie = $this->logowanieFasada->zarejestruj($post);
	    if ($wynikPrzetwarzanie->isSuccess) {
		$this->redirect()->toRoute('logowanie', ['action' => 'po_rejestracji']);
	    }
	}

	return $this->wyswietlFormularzRejestracji($wynikPrzetwarzanie, $post);
    }

    public function ustawieniaAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$zalogowany = $this->uzytkownikTable->getZalogowanegoUsera();
	$dostepne_jezyki = $this->logowanieFasada->pobierzDostepneJezyki();
	$uzytkownicy_konta = $this->logowanieFasada->pobierzKontaMailoweDlaZalogowanegoUzytkownika();

	if (count($uzytkownicy_konta) == 0) {
	    $uzytkownicy_konta[] = \Logowanie\Entity\UzytkownikKontoMailowe::create();
	}

	return new ViewModel(array(
		'uzytkownik' => $zalogowany,
		'uzytkownicy_konta' => $uzytkownicy_konta,
		'dostepne_jezyki' => $dostepne_jezyki,
	));
    }

    public function zapiszKontaMailoweAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$post = $this->getPost();
	if ($post) {
	    $this->logowanieFasada->zapiszKontaMailowe($post);
	}

	return $this->redirect()->toRoute('email', array('action' => 'index'));
    }

    public function zapiszJezykUzytkownikaAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);


	$request = $this->getRequest();
	if ($request->isPost()) {
	    $uzytkownikId = $this->params()->fromPost('id');
	    $kod = $this->params()->fromPost('jezyk_kod');
	    $this->uzytkownikTable->zmienJezykUzytkownika($uzytkownikId, $kod);
	}
	return $this->redirect()->toRoute('logowanie', array('action' => 'ustawienia'));
    }

    abstract protected function wyswietlFormularzRejestracji($wynikPrzetwarzanie = null, $post = []);

    public function poRejestracjiAction() {
	$layout = $this->layout();
	$layout->setTemplate('logowanie/layout/pusty_layout');
    }

    protected function przekieruj($post) {
	$przekierowanie = isset($post['redirect']) ? $post['redirect'] : "";
	if ($przekierowanie !== '') {
	    return $this->redirect()->toRoute($przekierowanie);
	} else {
	    return $this->domyslnePrzekierowanieZalogowanego();
	}
    }

    protected function domyslnePrzekierowanieNiezalogowanego() {
	return $this->redirect()->toRoute('home');
    }

    protected function domyslnePrzekierowanieZalogowanego() {
	return $this->redirect()->toRoute('home');
    }

}
