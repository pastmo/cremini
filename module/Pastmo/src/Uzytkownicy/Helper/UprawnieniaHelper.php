<?php

namespace Pastmo\Uzytkownicy\Helper;

use Zend\View\Helper\AbstractHelper;

class UprawnieniaHelper extends AbstractHelper {

    protected $count = 0;
    protected $uprawnieniaMenager;

    public function __construct($uprawnieniaMenager) {
	$this->uprawnieniaMenager = $uprawnieniaMenager;
    }

    public function __invoke($kod, $czyWyjatek = false) {
	$result = $this->uprawnieniaMenager->sprawdzUprawnienie($kod, $czyWyjatek);
	return $result;
    }

}
