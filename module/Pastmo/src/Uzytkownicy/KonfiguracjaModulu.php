<?php

namespace Pastmo\Uzytkownicy;

use Zend\Mvc\MvcEvent;
use Pastmo\Uzytkownicy\Helper\UprawnieniaHelper;
use Pastmo\Uzytkownicy\Helper\DisabledHelper;
use Pastmo\Uzytkownicy\Helper\ZalogowanyUserHelper;

class KonfiguracjaModulu extends \Pastmo\KonfiguracjaModulu {

    const UZYTKOWNIK_TABLE_GATEWAY = 'UzytkownikTableGateway';

    public function onBootstrap(MvcEvent $e) {
	$viewHelperMenager = $e->getApplication()->getServiceManager()->get('ViewHelperManager');

	$viewHelperMenager->setFactory('disabled',
		function($sm) use ($e) {
	    $uprawnieniaMenager = $sm->get(\Logowanie\Menager\UprawnieniaMenager::getClass());
	    $viewHelper = new DisabledHelper($uprawnieniaMenager);
	    return $viewHelper;
	});
	$viewHelperMenager->setFactory('uprawnienia',
		function($sm) use ($e) {
	    $uprawnieniaMenager = $sm->get(\Logowanie\Menager\UprawnieniaMenager::getClass());
	    $viewHelper = new UprawnieniaHelper($uprawnieniaMenager);
	    return $viewHelper;
	});
	$viewHelperMenager->setFactory('zalogowanyUser',
		function($sm) use ($e) {
	    $uzytkownikMenager = $sm->get(\Logowanie\Menager\UzytkownicyMenager::getClass());
	    $viewHelper = new ZalogowanyUserHelper($uzytkownikMenager);
	    return $viewHelper;
	})
	;
	$viewHelperMenager->setFactory('pobierzUprawnieniaRoli',
		function($sm) use ($e) {
	    $viewHelper = new Helper\PobierzUprawnieniaRoliHelper($sm);
	    return $viewHelper;
	})
	;
    }

    public function getFactories() {
	return [
		self::UZYTKOWNIK_TABLE_GATEWAY => function ($sm) {
		    return $this->ustawGateway($sm, new \Logowanie\Model\Uzytkownik($sm), 'uzytkownicy');
		},
	];
    }

}
