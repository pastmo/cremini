<?php

namespace Pastmo;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class KonfiguracjaModulu {

    use FunkcjeKonfiguracyjne;

    const JEZYKI_GATEWAY = "JEZYKI_GATEWAY";

    public function onBootstrap(\Zend\Mvc\MvcEvent $e) {

    }

    public function getFactories() {
	return array(
		Uzytkownicy\Menager\JezykiMenager::class => function($sm) {
		    $menager = new Uzytkownicy\Menager\JezykiMenager($sm);
		    return $menager;
		},
		Uzytkownicy\Menager\UzytkownicyMenager::class => function($sm) {
		    $menager = $sm->get(\Logowanie\Menager\UzytkownicyMenager::class);
		    return $menager;
		},
		self::JEZYKI_GATEWAY => function($sm) {
		    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
		    $resultSetPrototype = new ResultSet();
		    $resultSetPrototype->setArrayObjectPrototype(new Uzytkownicy\Entity\Jezyk($sm));
		    return new TableGateway('jezyki', $dbAdapter, null, $resultSetPrototype);
		},
	);
    }

}
