<?php

namespace Pastmo\Logi\Entity;

class Log extends \Wspolne\Model\WspolneModel {

    public $id;
    public $tresc;
    public $data_dodania;
    public $kategoria;
    public $uzytkownik;

    public function dowiazListyTabelObcych() {

    }

    public function exchangeArray($data) {
	$this->data = $data;
	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->tresc = $this->pobierzLubNull($data, 'tresc');
	$this->data_dodania = $this->pobierzLubNull($data, 'data_dodania');
	$this->kategoria = $this->pobierzTabeleObcaZKluczemObcym('kod_kategorii',
		\Pastmo\Logi\Menager\LogiKategorieMenager::class, new LogKategoria(), 'kod');
	$this->uzytkownik = $this->pobierzTabeleObca('uzytkownik_id', \Logowanie\Menager\UzytkownicyMenager::class,
		\Pastmo\Uzytkownicy\Entity\Uzytkownik::create());
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'uzytkownik':
		return 'uzytkownik_id';
	    case 'kategoria':
		return 'kod_kategorii';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function getTresc($klucz) {
	$tablica = json_decode($this->tresc);
	$tabl = (array) $tablica;
	return $this->pobierzLubNull($tabl, $klucz, '');
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
