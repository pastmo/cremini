<?php

namespace Pastmo\Logi\Builder;

class LogiBuilder {

    public $kodKategorii;
    public $tresc = array();

    public static function getBuilder() {
	return new LogiBuilder();
    }

    public function setKodKategorii($kodKategorii) {
	$this->kodKategorii = $kodKategorii;
	return $this;
    }

    public function addTresc($klucz, $tresc) {
	$this->tresc[$klucz] = $tresc;
	return $this;
    }

}
