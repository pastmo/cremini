<?php

namespace Pastmo\Logi\Menager;

use \Pastmo\Logi\Enumy\KodyKategoriiLogow;
use Pastmo\Logi\Builder\LogiBuilder;
use Pastmo\Wspolne\Utils\EntityUtil;

class LogiMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public $czasPracy;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm, \Pastmo\Logi\KonfiguracjaModulu::LOGI_GATEWAY);
    }

    public static function getBuilder() {
	return LogiBuilder::getBuilder();
    }

    public function dodajLog(LogiBuilder $builder) {
	$log = new \Pastmo\Logi\Entity\Log();

	$zalogowany = $this->getLogowanieFasada()->getZalogowanegoUsera();

	$log->uzytkownik = $zalogowany ? $zalogowany : null;
	$log->kategoria = $builder->kodKategorii;
	$log->tresc = json_encode($builder->tresc);
	$this->zapisz($log);
    }

    public function getLogiDlaKategori($kodKategori, $order = self::DOMYSLNY_ORDER) {
	return $this->pobierzZWherem("kod_kategorii='$kodKategori'", $order);
    }

    public function attach(\Zend\EventManager\EventManagerInterface $events, $priority = 1) {
	$this->listeners[] = $events->attach('zapisz', [$this, 'obsluzEventLogow']);
	$this->listeners[] = $events->attach('update', [$this, 'obsluzEventLogow']);
    }

    public function obsluzEventLogow(\Zend\EventManager\EventInterface $e) {
	$event = $e->getName();
	$params = $e->getParams();

	if ($params instanceof \BazaProduktow\Entity\ProduktMagazyn) {
	    if ($event === 'update') {
		$this->dodajLogEventuUpdate($params);
	    } elseif (!empty($params->produkt_wariacja->id) && !empty($params->magazyn->id)) {
		$this->dodajLogEventuUpdate($params);
	    } elseif (!empty($params->produkt_wariacja) && !empty($params->magazyn)) {
		$this->dodajLogEventuZapisz($params);
	    }
	}
    }

    private function dodajLogEventuUpdate($params) {
	if (empty($params->ilosc)) {
	    $params->ilosc = 0;
	}
	$builder = LogiBuilder::getBuilder()
		->setKodKategorii(KodyKategoriiLogow::EDYCJA_STANOW_MAGAZYNOWYCH)
		->addTresc('kod', $params->produkt_wariacja->produkt . '')
		->addTresc('wariacja', $params->produkt_wariacja . '')
		->addTresc('magazyn', $params->magazyn->nazwa)
		->addTresc('ilosc', $params->ilosc);
	$this->dodajLog($builder);
    }

    private function dodajLogEventuZapisz($params) {
	$produktyWariacjeMenager = $this->sm->get(\BazaProduktow\Menager\ProduktyWariacjeMenager::class);
	$magazynyMenager = $this->sm->get(\Magazyn\Menager\MagazynyMenager::class);
	$produktyWariacja = $produktyWariacjeMenager->pobierzZWherem("id = $params->produkt_wariacja");
	if (!empty($produktyWariacja)) {
	    $produktWariacja = $produktyWariacja[0];
	    if (empty($params->ilosc)) {
		$params->ilosc = 0;
	    }
	    $builder = LogiBuilder::getBuilder()
		    ->setKodKategorii(KodyKategoriiLogow::EDYCJA_STANOW_MAGAZYNOWYCH)
		    ->addTresc('kod', $produktWariacja->produkt . '')
		    ->addTresc('wariacja', $produktWariacja . '')
		    ->addTresc('ilosc', $params->ilosc);
	    $magazyny = $magazynyMenager->pobierzZWherem("id = $params->magazyn");
	    if (!empty($magazyny)) {
		$builder->addTresc('magazyn', $magazyny[0]->nazwa);
	    }
	    $this->dodajLog($builder);
	}
    }

    public function dodajLogEdycjaParametryProdukcji($dodany) {
	$builder = LogiBuilder::getBuilder()
		->setKodKategorii(KodyKategoriiLogow::EDYCJA_PARAMETROW_PRODUKCJI)
		->addTresc('nazwa_projektu', EntityUtil::wydobadzPole(EntityUtil::wydobadzPole($dodany->projekt_produkt_wariacja,'projekt'), 'nazwa') . '')
		->addTresc('czas_pracy', $dodany->czas_pracy . '')
		->addTresc('urzadzenie_nazwa', EntityUtil::wydobadzPole($dodany->urzadzenie,'nazwa'));
	$this->dodajLog($builder);
    }

    public function dodajLogPoPrzesunieciuCzasu($post) {

        $wynik = new \Produkcja\Entity\ParametryProdukcjiUrzadzenie();
        $projektyMenager = new \Projekty\Menager\ProjektyMenager($this->sm);
        $wynik->projekt_produkt_wariacja = new \Projekty\Menager\ProjektyProduktyWariacjeMenager($this->sm);
        $projekt = $projektyMenager->getRekord($post['project_id']);
        $wynik->projekt_produkt_wariacja->projekt = $projekt;
        $wynik->czas_pracy = \Pastmo\Wspolne\Utils\DateUtil::obliczMinutyInterwalu($post['data_od'], $post['data_do']);
        $czasPracyMenager = new \Wizualizacje\Menager\CzasyPracyMenager($this->sm);
        $this->czasPracy = $czasPracyMenager->getRekord($post['id']);
        $this->typ = \Wizualizacje\Enumy\TypyCzasowPracy::URZADZENIA;
        $urzadzenieMenager = new \Produkcja\Menager\UrzadzeniaMenager($this->sm);
        $wlasciciel = $this->czasPracy->getWlasciciela();
        $wynik->urzadzenie = $urzadzenieMenager->getRekord($wlasciciel->id);

        $this->dodajLogEdycjaParametryProdukcji($wynik);
    }


}
