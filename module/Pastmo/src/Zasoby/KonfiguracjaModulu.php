<?php

namespace Pastmo\Zasoby;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zasoby\Model\Zasob;

class KonfiguracjaModulu extends \Pastmo\KonfiguracjaModulu {

    const ZASOBY_GATEWAY = 'ZasobyTableGateway';

    public function getFactories() {
	return array(
		Menager\ZasobyKonwerterNapisowMenager::class => function($sm) {
		    return new Menager\ZasobyKonwerterNapisowMenager($sm);
		},
		\Zasoby\Menager\ZasobyUploadMenager::class => function($sm) {
		    $table = new \Zasoby\Menager\ZasobyUploadMenager($sm);
		    return $table;
		},
		\Zasoby\Menager\ZasobyDownloadMenager::class => function($sm) {
		    $table = new \Zasoby\Menager\ZasobyDownloadMenager($sm);
		    return $table;
		},
		self::ZASOBY_GATEWAY => function ($sm) {
		    return $this->ustawGateway($sm, new Zasob($sm), 'zasoby');
		}
	);
    }

}
