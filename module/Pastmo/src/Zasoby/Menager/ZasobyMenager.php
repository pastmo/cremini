<?php

namespace Pastmo\Zasoby\Menager;

use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;
use Zasoby\Model\FileOperator;
use Pastmo\Zasoby\Entity\Zasob;
use Zasoby\Model\TypZasobu;
use Zasoby\Model\KategoriaZasobu;

class ZasobyMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    const domyslnyObrazek = "default.png";
    const domyslnyObrazekTypu = "default_%s.png";

    static $uploadDir = "./public_html/upload/";
    static $tmpDir = "./public_html/tmp/";

    const uploadToShow = "upload/";

    public $fileOperator;
    protected $id = 0;
    private $finalName;
    private $extension;
    private $pelnaSciezkaZapisu;

    public function __construct(ServiceManager $sm = null) {
	parent::__construct($sm, 'ZasobyTableGateway');
	$this->fileOperator = new FileOperator();
    }

    public function zaladujPlik() {

	$this->wynik = array();

	$this->target_dir = $this->getUploadDir();

	while (list($klucz, $wartosc) = each($_FILES)):
	    $this->przeniesPliki($wartosc);
	endwhile;

	return $this->wynik;
    }

    public function zapiszPlik($oryginalnaNazwa, $tresc) {
	$this->target_dir = $this->getUploadDir();

	$this->zrobSciezkeDoZapisu($oryginalnaNazwa);

	$fh = fopen($this->pelnaSciezkaZapisu, 'w');
	fwrite($fh, $tresc);
	fclose($fh);

	return $this->wydobadzInformacjeIZrobZasob($oryginalnaNazwa);
    }

    private function zrobSciezkeDoZapisu($oryginalnaNazwa) {
	$this->extention = pathinfo($oryginalnaNazwa, PATHINFO_EXTENSION);

	$this->finalName = $this->kodujNazwe($oryginalnaNazwa, "." . $this->extention);

	$this->pelnaSciezkaZapisu = $this->target_dir . $this->finalName;
    }

    private function wydobadzInformacjeIZrobZasob($oryginalnaNazwa) {
	$finfo = finfo_open(FILEINFO_MIME_TYPE);

	$type = finfo_file($finfo, $this->pelnaSciezkaZapisu);

	$param = array();
	$param['size'] = filesize($this->pelnaSciezkaZapisu);
	$param['type'] = $type;

	$zasob = $this->zrobZasob($this->finalName, $oryginalnaNazwa, $param);
	return $zasob;
    }

    public function downloadZasobResponse($id) {

	$zasob = $this->getRekord($id);

	$fileName = $this->zasobRealPath($zasob);

	return $this->zrobDownloadLink($fileName, $zasob->nazwa);
    }

    public function downloadZip(array $zasobyId, $nazwaZipa) {
	$zip = new \ZipArchive;

	$nazwa = $nazwaZipa . ".zip";
	$fullPath = $this->realPathTmp($nazwa);
	$res = $zip->open($fullPath, \ZipArchive::CREATE);
	if ($res === TRUE) {

	    foreach ($zasobyId as $zasobId) {
		$zasob = $this->getRekord($zasobId);

		$zip->addFile($this->zasobRealPath($zasob), $zasob->nazwa);
	    }
	    $zip->close();

	    return $this->zrobDownloadLink($fullPath, $nazwa);
	} else {
	    throw new \Wspolne\Exception\GgerpException("Nie można utworzyć pliku $fullPath");
	}
    }

    public function zrobZasobZLinka($link) {
	$this->target_dir = $this->getUploadDir();

	$oryginalnaNazwa = basename($link);

	$this->zrobSciezkeDoZapisu($oryginalnaNazwa);

	copy($link, $this->pelnaSciezkaZapisu);

	return $this->wydobadzInformacjeIZrobZasob($oryginalnaNazwa);
    }

    public function wyswietlMiniaturke($zasob) {
        
	$result = self::uploadToShow;
	if ($zasob->url !== null && $zasob->typ !== TypZasobu::PLIK) {
	    $result.=$zasob->url;
	} else {        
            $nowyZasob = $this->sprawdzCzyPodgladIstnieje($zasob);
	    $result.=$nowyZasob->url;
	}
	return $result;
    }
    
    private function sprawdzCzyPodgladIstnieje($zasob) {
        $miniaturka_id = $zasob->miniaturka_id;
        if(isset($miniaturka_id)){
            $podgladWygenerowanyWczesniej = $this->getRekord($miniaturka_id);
            return $podgladWygenerowanyWczesniej;
        }
        $nowyZasob = $this->zrobPodgladPdfIZapiszWBazie($zasob);
        return $nowyZasob;
    }
    
    
    private function zrobPodgladPdfIZapiszWBazie($zasob) {
        if ($this->sprawdzCzyPdf($zasob->typ_fizyczny)) {
            $this->makePngFromPdfFolderIfDontExist();
            $converted = $this->konwertujPdfDoObrazka($zasob);                       
            $link = self::$tmpDir.'pngFromPdf/'.$converted.'/page-1.png';
            $nowyZasob = $this->zrobZasobZLinka($link);
            $zasob->miniaturka_id = $nowyZasob->id;
            $this->zapisz($zasob);
            return $nowyZasob;
        }
        $obrazek = self::ustawDomyslnyObrazek($zasob->typ, $zasob->url);        
        return $obrazek;
    }
    
    private function konwertujPdfDoObrazka($zasob) {
        $pdflib = new \ImalH\PDFLib\PDFLib();
        $realPdfPath = $this->zasobRealPathZeStringa($zasob->url);
        $pdflib->setPdfPath($realPdfPath);
        $tmpRandom = $this->kodujNazwe($zasob->nazwa, $zasob->typ_fizyczny);
        $nazwaFolderu = 'pngFromPdf/'.$tmpRandom;
        if (!file_exists(self::$tmpDir.$nazwaFolderu)) {
            $this->zrobFolder($nazwaFolderu, true);
        }
        $outputPath = self::$tmpDir.'pngFromPdf/'.$tmpRandom;
        $pdflib->setOutputPath($outputPath);
        $pdflib->setImageFormat(\ImalH\PDFLib\PDFLib::$IMAGE_FORMAT_PNG);
        $pdflib->setDPI(300);
        $pdflib->setPageRange(1,1); //tylko pierwsza strona, bo to podgląd
        
        $pdflib->convert();
        return $tmpRandom;
    }
    
    private function makePngFromPdfFolderIfDontExist() {
        if (!file_exists(self::$tmpDir.'pngFromPdf')) {
            $this->zrobFolder('pngFromPdf', true);
        }
    }

    public function zrobFolder($sciezka, $tmp = false) {
	$path = $this->zasobRealPathZeStringa($sciezka);
        if ($tmp) {
            $path = $this->realPathTmp($sciezka);
        }
	mkdir($path);
    }

    public function zasobRealPath($zasob) {
	return $this->zasobRealPathZeStringa($zasob->url);
    }

    public function getUploadDir() {
	return self::$uploadDir;
    }

    public function zasobRealPathZeStringa($url) {//TODO: Czasmi używane do zapisu a czasami do odczytu
	return $this->getUploadDir() . $url;
    }

    public function realPathTmp($url) {
	return self::$tmpDir . $url;
    }

    private function przeniesPliki($wartosc) {
	if (is_array($wartosc["name"])) {
	    for ($i = 0; $i < count($wartosc['name']); $i++) {
		$komunikat = array(
			'name' => $wartosc['name'][$i],
			'tmp_name' => $wartosc['tmp_name'][$i],
			'type' => $wartosc['type'][$i],
			'size' => $wartosc['size'][$i]);
		$this->przeniesPliki($komunikat);
	    }
	} else {

	    $nazwa = basename($wartosc["name"]);

	    $finalName = $this->przeniesPlik($wartosc["tmp_name"], $nazwa);


	    if ($finalName) {

		$this->wynik[] = $this->zrobZasob($finalName, $nazwa, $wartosc);
		$this->id++;
	    } else {
		//TODO: Wymyślić co zrobić w takim przypadku. Wyrzuca błąd jeśli jest multifile.
		//throw new Exception("Sorry, there was an error uploading your file.");
	    }
	}
    }

    private function przeniesPlik($zrodlo, $nazwa) {
	$rozszerzenie = $this->pobierzRozszerzenie($nazwa);

	$finalName = $this->kodujNazwe($nazwa, $rozszerzenie);
	$target_file = $this->target_dir . $finalName;

	if ($this->fileOperator->move_uploaded_file($zrodlo, $target_file)) {
	    return $finalName;
	} else {
	    return false;
	}
    }

    private function zrobDownloadLink($fullPath, $nazwa) {
	$response = new \Zend\Http\Response\Stream();
	$response->setStream(fopen($fullPath, 'r'));
	$response->setStatusCode(200);

	$headers = new \Zend\Http\Headers();
	$headers->addHeaderLine('Content-Type', 'whatever your content type is')
		->addHeaderLine('Content-Disposition', 'attachment; filename="' . $nazwa . '"')
		->addHeaderLine('Content-Length', filesize($fullPath));

	$response->setHeaders($headers);
	return $response;
    }

    private function pobierzRozszerzenie($nazwa) {
	$kropka = strpos($nazwa, ".");
	$rozszerzenie = substr($nazwa, $kropka);
	return $rozszerzenie;
    }

    private function zrobZasob($finalName, $nazwa, $rowArray) {
	$zasob = new Zasob();
	$zasob->url = $finalName;
	$zasob->typ = $this->wyliczTyp($rowArray['type']);
	$zasob->rozmiar = $rowArray['size'];
	$zasob->kategoria = $this->wyliczKategorie();
	$zasob->nazwa = $nazwa;
	$zasob->typ_fizyczny = $rowArray['type'];
	$this->zapisz($zasob);
	$zapisany = $this->getPoprzednioDodany();
	return $zapisany;
    }

    private function wyliczTyp($typ) {
	if (substr($typ, 0, 5) === 'image') {
	    return TypZasobu::OBRAZEK;
	}
	return TypZasobu::PLIK;
    }
    
    private function sprawdzCzyPdf($typ_fizyczny) {
        if (strpos($typ_fizyczny, 'pdf') !== false) {
            return true;
        }
        return false;
    }

    private function wyliczKategorie() {
	if (isset($_POST['kategoria'])) {
	    return $_POST['kategoria'];
	}
	return KategoriaZasobu::ZWYKLY_PLIK;
    }

    protected function kodujNazwe($nazwa, $rozszerzenie) {
        $this->id = $this->pobierz_ilosc_zasobow() + 1;
	return $this->id . md5($nazwa) . \Pastmo\Wspolne\Utils\StrUtil::usunZnakiNienadajaceSieNaRozszerzeniePliku($rozszerzenie);
    }

    public function wyswietlUrlObrazkaZBasePathPoId($id) {
	$zasob = $this->getRekord($id);
	$wynik = $this->wyswietlUrlObrazkaZBasePath($zasob);
	return $wynik;
    }

    public function wyswietlUrlObrazkaZBasePath($zasob) {
	$wynik = $this->wyswietlUrlObrazka($zasob);
	return $this->opakujBasePath($wynik);
    }

    public function wyswietlUrlPlikuZBasePath($zasob) {
	$result = self::uploadToShow;
	if (!empty($zasob->url)) {
	    $result .= $zasob->url;
	}
	return $this->opakujBasePath($result);
    }

    public function wyswietlUrlObrazka($zasob) {
	$result = self::uploadToShow;
	if ($zasob->url !== null && $zasob->typ !== TypZasobu::PLIK) {
	    $result.=$zasob->url;
	} else {
	    $result.=self::ustawDomyslnyObrazek($zasob->typ, $zasob->url);
	}
	return $result;
    }

    public function pobierz_ilosc_zasobow() {
	$sql = "select count(*) as suma from zasoby;";

	$statement = $this->dbAdapter->query($sql);

	$results = $statement->execute();


	$result = $results->current();
	$suma = $result['suma'];
	if ($suma != null) {
	    return $suma;
	} else {
	    return "0";
	}
    }

    private static function ustawDomyslnyObrazek($typ, $url) {
	if ($typ === TypZasobu::PLIK) {
	    $poUrl = self::sprawdzRozszerzenie($url);
	    if ($poUrl) {
		return $poUrl;
	    }
	}
	if ($typ !== NULL) {
	    $result = sprintf(self::domyslnyObrazekTypu, $typ);
	    return $result;
	}

	return self::domyslnyObrazek;
    }

    private static function sprawdzRozszerzenie($url) {
	$last = substr($url, -3);
	switch ($last) {
	    case 'pdf':
	    case 'zip':
	    case 'doc':
		return sprintf(self::domyslnyObrazekTypu, $last);
	}
	return false;
    }

}
