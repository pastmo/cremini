<?php

namespace Pastmo\Zasoby\PrzeszukiwaniePlikow;

class PrzeszukiwaczPlikow {

    const WYSLIJ_POST = "Pastmo.wyslijPost(";
    const WYSLIJ_GET = "Pastmo.wyslijGet(";

    private $basePath;
    private $searched;
    private $znalezionePliki = [];
    private $wynik = [];

    const IGNOROWANE_PLIKI = array(
	    '.',
	    '..',
	    'test');

    public function przeszukaj() {
	$this->wynik = [];
	$this->pobierzTablicePlikow($this->basePath);
	$this->wydobadzZawartosc();

	return $this->wynik;
    }

    public function pobierzTablicePlikow($dir) {
	if (is_dir($dir)) {

	    $pliki = scandir($dir);
	    foreach ($pliki as $plik) {
		if (!in_array($plik, self::IGNOROWANE_PLIKI)) {

		    $this->pobierzTablicePlikow($this->nazwaPliku($dir, $plik));
		}
	    }
	} else {
	    $this->znalezionePliki[$dir] = $dir;
	}

	return $this->znalezionePliki;
    }

    private function wydobadzZawartosc() {
	foreach ($this->znalezionePliki as $plik) {
	    $tresc = file_get_contents($plik);
	    $tablicaTresci = explode($this->searched, $tresc);

	    for ($i = 1; $i < count($tablicaTresci); $i++) {
		$tr = $tablicaTresci[$i];
		$apostrof = $tr[0];

		$pos = strpos($tr, "$apostrof", 1);

		$this->wynik[] = str_replace('-', '_', substr($tr, 1, $pos - 1));
	    }
	}
    }

    private function nazwaPliku($dir, $filename) {
	return "$dir\\$filename";
    }

    public function setBasePath($basePath) {
	$this->basePath = $basePath;
	return $this;
    }

    public function setSearched($searched) {
	$this->searched = $searched;
	return $this;
    }

    public static function create() {
	return new PrzeszukiwaczPlikow();
    }

}
