<?php

namespace Pastmo\Zasoby\Entity;

use Zasoby\Model\TypZasobu;

class Zasob extends \Wspolne\Model\WspolneModel {

    public $id;
    public $url;
    public $nazwa;
    public $typ;
    public $typ_fizyczny;
    public $rozmiar;
    public $data_dodania;
    public $miniaturka_id;

    public function __construct($sm = null) {
	parent::__construct($sm);
    }

    public function exchangeArray($data) {
	$this->id = (!empty($data['id'])) ? $data['id'] : null;
	$this->url = (!empty($data['url'])) ? $data['url'] : null;
	$this->nazwa = $this->pobierzLubNull($data, 'nazwa');
	$this->typ = (!empty($data['typ'])) ? $data['typ'] : TypZasobu::OBRAZEK;
	$this->typ_fizyczny = $this->pobierzLubNull($data, 'typ_fizyczny');
	$this->rozmiar = $this->pobierzLubNull($data, 'rozmiar');
	$this->data_dodania = $this->pobierzLubNull($data, 'data_dodania');
        $this->miniaturka_id = $this->pobierzLubNull($data, 'miniaturka_id');

	if ($this->czyTabeleDostepne()) {
	    $this->wyswietlanie_js = $this->sm->get(\Zasoby\Fasada\ZasobyFasada::class)
		    ->wyswietlUrlObrazkaZBasePath($this);
	}
    }

    public function getRozmiar() {
	$zwrotnyString = "0";
	$kb = $this->rozmiar / 1024;
	$zwrotnyString = round($kb, 2) . ' KB';
	if ($kb >= 1024) {
	    $mb = $kb / 1024;
	    $zwrotnyString = round($mb, 2) . ' MB';
	    if ($mb >= 1024) {
		$gb = $mb / 1024;
		$zwrotnyString = round($gb, 2) . ' GB';
	    }
	}

	return $zwrotnyString;
    }

    public function wyswietl() {
	if ($this->czyTabeleDostepne()) {
	    $zasobyTable = $this->sm->get(\Zasoby\Menager\ZasobyUploadMenager::class);
	} else {
	    $zasobyTable = new \Zasoby\Menager\ZasobyUploadMenager();
	}
	return $zasobyTable->wyswietlUrlObrazka($this);
    }
    
    public function wyswietlImgLubPdf() {
        $zasobyTable = $this->sm->get(\Zasoby\Menager\ZasobyUploadMenager::class);
        return $zasobyTable->wyswietlMiniaturke($this);
    }

    public function wyswietlUrlBasePath() {
	$zasobyFasada = $this->sm->get(\Zasoby\Fasada\ZasobyFasada::class);
	return $zasobyFasada->wyswietlUrlPlikuZBasePath($this);
    }

    public function dowiazListyTabelObcych() {

    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	return $nazwaWKodzie;
    }

    public static function zrobDomyslnyZasobAvatara($sm = null) {
	$avatar = new Zasob($sm);
	$avatar->exchangeArray(array('typ' => \Zasoby\Model\TypZasobu::AVATAR));
	return $avatar;
    }
    public static function zrobAvataraSystemowego($sm = null) {
	$avatar = new Zasob($sm);
	$avatar->exchangeArray(array('typ' => \Zasoby\Model\TypZasobu::AVATAR));
	return $avatar;
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
