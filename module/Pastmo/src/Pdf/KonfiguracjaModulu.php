<?php

namespace Pastmo\Pdf;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Pastmo\Email\Entity\KontoMailowe;
use Pastmo\Email\Entity\Email;
use Pastmo\Email\Entity\EmailZasob;

class KonfiguracjaModulu extends \Pastmo\KonfiguracjaModulu {

    public function getFactories() {
	return array(
		Menager\PdfMenager::class => function($sm) {
		    $menager = new Menager\PdfMenager($sm);
		    return $menager;
		},
	);
    }

}
