<?php

namespace Pastmo\Pdf\Menager;

use Dompdf\Dompdf;

class PdfMenager extends \Pastmo\Wspolne\Menager\WspolnyMenager {
use \Pastmo\Wspolne\Utils\GeneratorWidokow;

    private $dompdf;
    private $zasob;
    
    public static $test = false;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm);
	$this->initGeneratorWidokow();
    }

    public function zrobPdfZSzablonem($template_phtml, array $params, $tytul = "dokument") {
	$this->children = array();

	$model = new \Zend\View\Model\ViewModel($params);
	$model->setTemplate($template_phtml);
	$this->children[] = $model;

	return $this->zrobPdf("pdf\pdf\szablon", $params, $tytul);
    }

    public function zrobPdf($template_phtml, array $params, $tytul = "dokument") {

	$this->wspolnaCzescTworzeniaPdf($template_phtml, $params);
	return $this->zwrocWynikDownload($tytul);
    }

    public function zrobZasobPdf($template_phtml, array $params, $tytul = "dokument") {

	$this->wspolnaCzescTworzeniaPdf($template_phtml, $params);
	$this->utworzZasob($tytul);
	return $this->zasob;
    }
    
    public function zrobZasobPdfZSzablonem($template_phtml, array $params, $tytul = "dokument") {
        $this->children = array();

	$model = new \Zend\View\Model\ViewModel($params);
	$model->setTemplate($template_phtml);
	$this->children[] = $model;

        $this->wspolnaCzescTworzeniaPdf("pdf\pdf\szablon", $params);
        $this->utworzZasob($tytul);
	return $this->zasob;
    }

    private function wspolnaCzescTworzeniaPdf($template_phtml, $params) {
	$this->zwiekszNestingLevel();
	$html = $this->generujHtml($template_phtml, $params);
	$this->utworzPdf($html);
    }

    public function zwiekszNestingLevel() {
	ini_set('xdebug.max_nesting_level', 1000);
    }

    private function utworzPdf($html, $tytul = "dokument") {
	$this->dompdf = new Dompdf();
	$this->dompdf->loadHtml($html, 'UTF-8');

	$this->dompdf->setPaper('A4', 'portrait');

	$this->dompdf->render();
	return $this->dompdf;
    }

    private function zwrocWynikDownload($tytul) {
	if (!self::$test) {
	    return $this->dompdf->stream($tytul);
	} else {
	    return $this->dompdf->output();
	}
    }

    private function utworzZasob($tytul) {
	$this->zasob = $this->get(\Zasoby\Menager\ZasobyUploadMenager::class)->zapiszPlik($tytul . '.pdf',
		$this->dompdf->output());
    }

}
