<?php

namespace PastmoTest\Uzytkownicy\Integracyjne;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Pastmo\Uzytkownicy\Menager\UprawnieniaGenerator;
use Logowanie\Entity\Uprawnienie;

class GeneratorKodyUprawnienTest extends AbstractHttpControllerTestCase {

    private $sm;
    private $uprawnienia;
    private $uprawnieniaGenerator;

    public function setUp() {

	parent::setUp();
	$this->setApplicationConfig(
		include \Application\Stale::configPath
	);
	$this->sm = $this->getApplicationServiceLocator();

	$this->uprawnieniaGenerator = new UprawnieniaGenerator($this->sm);
    }

    public function testGenerujUprawnieniaEnum() {

	$this->uprawnienia = $this->uprawnieniaGenerator->pobierzZWherem('1', "opis ASC");

	$this->utworzJedenWynik('KodyUprawnien.php', true);
	$this->utworzJedenWynik('kody_uprawnien.js', false);
    }

    private function utworzJedenWynik($tytul, $czyPHP) {
	$wynik = $this->uprawnieniaGenerator->generuj($this->uprawnienia, 'opis',$czyPHP);

	echo PHP_EOL . PHP_EOL . "****$tytul*******" . PHP_EOL;

	foreach ($wynik as $element) {
	    echo $element;
	}
	echo PHP_EOL . PHP_EOL . "***********************" . PHP_EOL;
    }

    public function testGenerujUprawnienia_pojedynczyPrzypadek() {
	$opis = "Testowe uprawnienie ąę";
	$kod = "kod_test";
	$expected = 'const TESTOWE_UPRAWNIENIE_AE="kod_test";' . PHP_EOL;

	$this->uruchomPojedynczePrzypadki($kod, $opis, $expected);
    }

    public function testGenerujUprawnienia_kropka() {
	$opis = "Testowe.";
	$kod = "kod_test";
	$expected = 'const TESTOWE_="kod_test";' . PHP_EOL;

	$this->uruchomPojedynczePrzypadki($kod, $opis, $expected);
    }

    private function uruchomPojedynczePrzypadki($kod, $opis, $expected) {
	$uprawnienie = new Uprawnienie();
	$uprawnienie->kod = $kod;
	$uprawnienie->opis = $opis;

	$uprawnieniaGenerator = new UprawnieniaGenerator($this->sm);
	$wynik = $uprawnieniaGenerator->generuj(array($uprawnienie), 'opis');

	$this->assertEquals($expected, $wynik[0]);
    }

}
