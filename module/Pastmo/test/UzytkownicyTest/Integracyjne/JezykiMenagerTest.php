<?php

namespace PastmoTest\Uzytkownicy\Integracyjne;

use Logowanie\Model\Uzytkownik;
use Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane;
use Pastmo\Testy\Util\TB;

class JezykiMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    protected $traceError = true;
    private $jezykiMenager;

    public function setUp() {
	parent::setUp();

	$this->jezykiMenager = $this->sm->get(\Pastmo\Uzytkownicy\Menager\JezykiMenager::class);
    }

    public function test_dodajRekord() {

	$jezyk = TB::create(\Pastmo\Uzytkownicy\Entity\Jezyk::class, $this)
		->setPf(\Pastmo\Testy\ParametryFabryki\PFIntegracyjneBrakzapisuNiepowiazane::create($this->sm))
		->setParameters(array('nazwa' => __CLASS__))
		->make();
	$this->jezykiMenager->zapisz($jezyk);

	$ostatniRekord = $this->jezykiMenager->pobierzZWherem("kod='{$jezyk->kod}'",'skrot ASC');

	$this->assertEquals(1, count($ostatniRekord));
    }

}
