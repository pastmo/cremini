<?php

namespace ZasobyTest\Integracyjne;

use Zasoby\Menager\ZasobyUploadMenager;

class PdfMenager_zapisywanie_plikuTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $zasobyTable;
    private $pdfMenager;

    public function setUp() {
	parent::setUp();
	$this->zasobyTable = new ZasobyUploadMenager($this->sm);
	$this->pdfMenager = $this->sm->get(\Pastmo\Pdf\Menager\PdfMenager::class);
    }

    public function testZapisywaniePdfaDoPliku() {
	$nazwaDlaKlienta='nazwa dla klienta';

	$zasob = $this->pdfMenager->zrobZasobPdf('pdf/pdf/test',
		array(
	    'tytul' => 'tytuł'
	),$nazwaDlaKlienta);

	$fullPath = $this->zasobyTable->zasobRealPathZeStringa($zasob->url);

	$this->assertTrue(file_exists($fullPath));
	$this->assertNotNull($zasob);
	$this->assertEquals($nazwaDlaKlienta.'.pdf',$zasob->nazwa);
    }

}
