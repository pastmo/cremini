<?php

namespace PastmoTest\Wspolne\Util;

class EntityUtilTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    private $adres;

    public function setUp() {
	parent::setUp();

	$this->adres = \FabrykaEncjiMockowych::makeEncje(\Pastmo\Wspolne\Entity\Adres::class,
			\Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane::create()
				->setparametry(array('ulica' => 'Ulica')));
    }

    public function test_wydobadzIdZObiektu() {
	$wynik = \Pastmo\Wspolne\Utils\EntityUtil::wydobadzId($this->adres);

	$this->assertEquals($this->adres->id, $wynik);
    }

    public function test_wydobadzIdZObiektu_null() {
	$adres = \FabrykaRekordow::makeEncje(\Pastmo\Wspolne\Entity\Adres::class,
			\Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane::create());

	$wynik = \Pastmo\Wspolne\Utils\EntityUtil::wydobadzId($adres);

	$this->assertEquals(null, $wynik);
    }

    public function test_wydobadzId_zInta() {
	$adres = 5;

	$wynik = \Pastmo\Wspolne\Utils\EntityUtil::wydobadzId($adres);

	$this->assertEquals($adres, $wynik);
    }

    public function test_wydobadzId_nulla() {
	$adres = null;

	$wynik = \Pastmo\Wspolne\Utils\EntityUtil::wydobadzId($adres);

	$this->assertEquals($adres, $wynik);
    }

    public function test_wydobadzPole() {

	$wynik = \Pastmo\Wspolne\Utils\EntityUtil::wydobadzPole($this->adres, 'ulica');

	$this->assertEquals($this->adres->ulica, $wynik);
    }

    public function test_czyEncjaZId() {

	$wynik = \Pastmo\Wspolne\Utils\EntityUtil::czyEncjaZId($this->adres);

	$this->assertEquals(true, $wynik);
    }

    public function test_czyEncjaZId_false() {

	$wynik = \Pastmo\Wspolne\Utils\EntityUtil::czyEncjaZId(null);

	$this->assertEquals(false, $wynik);
    }

}
