<?php

namespace PastmoTest\Wspolne\Util;

use Pastmo\Wspolne\Utils\DateUtil;
use Pastmo\Wspolne\Utils\GodzinyPracyUtil;

class GodzinyPracyUtilTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    public function setUp() {
	parent::setUp();
    }

    public function test_podzielNaDniRobocze() {
	$wynik = GodzinyPracyUtil::podzielNaDniRobocze("2016-10-10 9:00",
			"2016-10-10 15:00");

	$this->assertEquals(1, count($wynik));
	$this->assertEquals('2016-10-10 09:00:00', $wynik[0]['od']);
	$this->assertEquals('2016-10-10 15:00:00', $wynik[0]['do']);
    }

    public function test_podzielNaDniRobocze_warunki_brzegowe() {
	$wynik = GodzinyPracyUtil::podzielNaDniRobocze("2016-10-10 9:00",
			"2016-10-10 17:00");

	$this->assertEquals(1, count($wynik));
	$this->assertEquals('2016-10-10 09:00:00', $wynik[0]['od']);
	$this->assertEquals('2016-10-10 17:00:00', $wynik[0]['do']);
    }

    public function test_podzielNaDniRobocze_warunki_brzegowe_początek_pracy_za_pozno() {
	$wynik = GodzinyPracyUtil::podzielNaDniRobocze("2016-10-10 17:00",
			"2016-10-11 15:00");

	$this->assertEquals(1, count($wynik));
	$this->assertEquals('2016-10-11 09:00:00', $wynik[0]['od']);
	$this->assertEquals('2016-10-11 15:00:00', $wynik[0]['do']);
    }

    public function test_podzielNaDniRobocze_nadgodziny() {
	$wynik = GodzinyPracyUtil::podzielNaDniRobocze("2016-10-10 9:00",
			"2016-10-10 19:00");

	$this->assertEquals(1, count($wynik));
	$this->assertEquals('2016-10-10 09:00:00', $wynik[0]['od']);
	$this->assertEquals('2016-10-10 17:00:00', $wynik[0]['do']);
    }

    public function test_podzielNaDniRobocze_kilkaDni() {
	$wynik = GodzinyPracyUtil::podzielNaDniRobocze("2016-10-10 13:00",
			"2016-10-12 15:00");

	$this->assertEquals(3, count($wynik));
	$this->assertEquals('2016-10-10 13:00:00', $wynik[0]['od']);
	$this->assertEquals('2016-10-10 17:00:00', $wynik[0]['do']);
	$this->assertEquals('2016-10-11 09:00:00', $wynik[1]['od']);
	$this->assertEquals('2016-10-11 17:00:00', $wynik[1]['do']);
	$this->assertEquals('2016-10-12 09:00:00', $wynik[2]['od']);
	$this->assertEquals('2016-10-12 15:00:00', $wynik[2]['do']);
    }

    public function test_podzielNaDniRobocze_z_weekendem() {
	$wynik = GodzinyPracyUtil::podzielNaDniRobocze("2016-10-14 13:00",
			"2016-10-17 15:00");

	$this->assertEquals(2, count($wynik));
	$this->assertEquals('2016-10-14 13:00:00', $wynik[0]['od']);
	$this->assertEquals('2016-10-14 17:00:00', $wynik[0]['do']);
	$this->assertEquals('2016-10-17 09:00:00', $wynik[1]['od']);
	$this->assertEquals('2016-10-17 15:00:00', $wynik[1]['do']);
    }

    public function test_podzielNaDniRobocze_warunek_graniczny_weekendu() {
	$wynik = GodzinyPracyUtil::podzielNaDniRobocze("2016-12-30 17:00",
			"2016-12-31 17:00");

	$this->assertEquals(1, count($wynik));
	$this->assertEquals('2016-12-30 17:00:00', $wynik[0]['od']);
	$this->assertEquals('2016-12-30 17:00:00', $wynik[0]['do']);
    }

    public function test_obliczCzasTrwaniaWMinutachRoboczych() {
	$start = '2016-10-14 13:00:00';
	$stop = '2016-10-14 17:00:00';

	$wynik = GodzinyPracyUtil::obliczCzasTrwaniaWMinutachRoboczych($start, $stop);

	$this->assertEquals(4 * 60, $wynik);
    }

    public function test_obliczCzasTrwaniaWMinutachRoboczych_dwa_dni_dwie_godziny() {

	$wynik = GodzinyPracyUtil::obliczCzasTrwaniaWMinutachRoboczych('2016-12-06 16:11:22', '2016-12-07 10:11:22');

	$this->assertEquals(2 * 60, $wynik);
    }

    public function test_obliczCzasTrwaniaWMinutachRoboczych_dwa_dni() {
	$start = '2016-10-14 13:00:00';
	$stop = '2016-10-17 15:00:00';

	$wynik = GodzinyPracyUtil::obliczCzasTrwaniaWMinutachRoboczych($start, $stop);

	$this->assertEquals((17 - 13) * 60 + (15 - 9) * 60, $wynik);
    }

    public function test_obliczCzasTrwaniaWMinutachRoboczych_dwa_dni_ujemna_wartosc() {
	$start = '2016-10-17 15:00:00';
	$stop = '2016-10-14 13:00:00';

	$wynik = GodzinyPracyUtil::obliczCzasTrwaniaWMinutachRoboczych($start, $stop);

	$this->assertEquals(-((17 - 13) * 60 + (15 - 9) * 60), $wynik);
    }

    public function test_obliczCzasTrwaniaWMinutachRoboczych_dwa_dni_godziny_poza_przedzialem() {
	$start = '2016-10-14 06:00:00'; //piątek
	$stop = '2016-10-17 20:00:00'; //poniedziałek

	$wynik = GodzinyPracyUtil::obliczCzasTrwaniaWMinutachRoboczych($start, $stop);

	$this->assertEquals((8) * 60 + (8) * 60, $wynik);
    }

    public function test_obliczDateKoncaWDniachRoboczych() {
	$start = '2016-10-14 13:00:00';
	$stop = '2016-10-14 17:00:00';

	$wynik = GodzinyPracyUtil::obliczDateKoncaWDniachRoboczych($start,
			(17 - 13) * 60);

	$this->assertEquals($stop, $wynik);
    }

    public function test_obliczDateKoncaWDniachRoboczych_weekend() {
	$start = '2016-10-14 13:00:00';
	$stop = '2016-10-17 15:03:00';

	$wynik = GodzinyPracyUtil::obliczDateKoncaWDniachRoboczych($start,
			(17 - 13) * 60 + (15 - 9) * 60 + 3);

	$this->assertEquals($stop, $wynik);
    }

    public function test_obliczDateKoncaWDniachRoboczych__() {
	$start = '2016-09-21 16:00:00';
	$stop = '2016-09-23 16:00:00';

	$wynik = GodzinyPracyUtil::obliczDateKoncaWDniachRoboczych($start, 2 * 8 * 60);

	$this->assertEquals($stop, $wynik);
    }

    public function test_obliczDateKoncaWDniachRoboczych__ujemne_przesuniecie() {
	$start = '2016-09-21 16:00:00';
	$expected = '2016-09-21 14:30:00';

	$wynik = GodzinyPracyUtil::obliczDateKoncaWDniachRoboczych($start, -90);

	$this->assertEquals($expected, $wynik);
    }
    public function test_obliczDateKoncaWDniachRoboczych__ujemne_przesuniecie_powyzejDnia() {
	$start = '2016-12-27 12:00:00';
	$expected = '2016-12-26 13:30:00';

	$wynik = GodzinyPracyUtil::obliczDateKoncaWDniachRoboczych($start, -390);

	$this->assertEquals($expected, $wynik);
    }

    public function test_normalizujDate_przed() {
	$start = '2016-10-14 06:00:00';

	$wynik = GodzinyPracyUtil::normalizujDate(new \DateTime($start));

	$this->assertEquals('2016-10-14 09:00:00',
		$wynik->format(DateUtil::FORMAT_TIMESTAMP));
    }

    public function test_normalizujDate_godzinaOk() {
	$start = '2016-10-14 12:12:00';

	$wynik = GodzinyPracyUtil::normalizujDate(new \DateTime($start));

	$this->assertEquals('2016-10-14 12:12:00',
		$wynik->format(DateUtil::FORMAT_TIMESTAMP));
    }

    public function test_normalizujDate_godzinaPo() {
	$start = '2016-10-15 20:15:00';

	$wynik = GodzinyPracyUtil::normalizujDate(new \DateTime($start));

	$this->assertEquals('2016-10-14 17:00:00',
		$wynik->format(DateUtil::FORMAT_TIMESTAMP));
    }

    public function test_normalizujDate_weekend() {
	$start = '2016-10-14 17:12:00';

	$wynik = GodzinyPracyUtil::normalizujDate(new \DateTime($start));

	$this->assertEquals('2016-10-14 17:00:00',
		$wynik->format(DateUtil::FORMAT_TIMESTAMP));
    }

    public function test_normalizujDateWprzod_przed() {
	$start = '2016-10-14 06:00:00';

	$wynik = GodzinyPracyUtil::normalizujDateWprzod(new \DateTime($start));

	$this->assertEquals('2016-10-14 09:00:00',
		$wynik->format(DateUtil::FORMAT_TIMESTAMP));
    }

    public function test_normalizujDateWprzod_godzinaOk() {
	$start = '2016-10-14 12:12:00';

	$wynik = GodzinyPracyUtil::normalizujDateWprzod(new \DateTime($start));

	$this->assertEquals('2016-10-14 12:12:00',
		$wynik->format(DateUtil::FORMAT_TIMESTAMP));
    }

    public function test_normalizujDateWprzod_godzinaPo() {
	$start = '2016-10-13 17:12:00';

	$wynik = GodzinyPracyUtil::normalizujDateWprzod(new \DateTime($start));

	$this->assertEquals('2016-10-14 09:00:00',
		$wynik->format(DateUtil::FORMAT_TIMESTAMP));
    }

    public function test_normalizujDateWprzod_nastepny_dzien_to_weekend() {
	$start = '2016-10-14 17:12:00';

	$wynik = GodzinyPracyUtil::normalizujDateWprzod(new \DateTime($start));

	$this->assertEquals('2016-10-17 09:00:00',
		$wynik->format(DateUtil::FORMAT_TIMESTAMP));
    }

    public function test_normalizujDateWprzod_weekend() {
	$start = '2016-10-15 12:12:00';

	$wynik = GodzinyPracyUtil::normalizujDateWprzod(new \DateTime($start));

	$this->assertEquals('2016-10-17 09:00:00',
		$wynik->format(DateUtil::FORMAT_TIMESTAMP));
    }

    public function test_normalizujDateWprzod_obcinanie_koncowki() {
	$start = '2016-10-13 17:00:00';

	$wynik = GodzinyPracyUtil::normalizujDateWprzod(new \DateTime($start),true);

	$this->assertEquals('2016-10-14 09:00:00',
		$wynik->format(DateUtil::FORMAT_TIMESTAMP));
    }

}
