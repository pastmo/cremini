<?php

namespace PastmoTest\Wspolne\Menager;



class WyszukiwaczTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    private $wyszukiwanieMenager;
    private $szukanyNapis = "szukanyNapis";

    public function setUp() {
	parent::setUp();
    }

    public function test_wyszukaj_bez_pol() {
	$this->wyszukiwanieMenager = \Pastmo\Wspolne\Menager\Wyszukiwacz::create()
		->setTable($this->uzytkownikTable)
		->setPoszukiwanyNapis($this->szukanyNapis);

	$this->sprawdzUzycieUtworzWherePoWszystkichPolach(1);

	$this->wyszukiwanieMenager->wyszukaj();
    }

    public function test_wyszukaj_z_polami() {
	$this->wyszukiwanieMenager = \Pastmo\Wspolne\Menager\Wyszukiwacz::create()
		->setTable($this->uzytkownikTable)
		->setPoszukiwanyNapis($this->szukanyNapis)
		->setPola(array());

	$this->sprawdzUzycieUtworzWherePoWszystkichPolach(0);

	$this->wyszukiwanieMenager->wyszukaj();
    }

    public function test_wyszukaj_z_order() {
	$this->wyszukiwanieMenager = \Pastmo\Wspolne\Menager\Wyszukiwacz::create()
		->setTable($this->uzytkownikTable)
		->setPoszukiwanyNapis($this->szukanyNapis)
		->setSortKlucz("kolumna")
		->setSortTyp("ASC");

	$this->sprawdzUzycieUtworzWherePoWszystkichPolach(1);

	$this->wyszukiwanieMenager->wyszukaj();
	//TODOPK: Zrobić jakieś sprawdzanie;
    }
    public function test_wyszukaj_z_limit() {
	$this->wyszukiwanieMenager = \Pastmo\Wspolne\Menager\Wyszukiwacz::create()
		->setTable($this->uzytkownikTable)
		->setPoszukiwanyNapis($this->szukanyNapis)
		->setLimit(5);

	$this->sprawdzUzycieUtworzWherePoWszystkichPolach(1);

	$this->wyszukiwanieMenager->wyszukaj();
	//TODOPK: Zrobić jakieś sprawdzanie;
    }



    private function sprawdzUzycieUtworzWherePoWszystkichPolach($ileRazy) {
	$this->obslugaKlasObcych->ustawMetode(\Logowanie\Menager\UzytkownicyMenager::getClass(),
		'utworzWherePoWszystkichPolach', 'utworzWherePoWszystkichPolach', $ileRazy);
    }

}
