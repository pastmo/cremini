<?php

namespace PastmoTest\Wspolne\Menager;

class AdnotacjeModeluMenagerTest extends \Testy\BazoweKlasyTestow\AbstractMockTest {

    public function setUp() {
	parent::setUp();
    }

    public function testsprawdzCzyMetodaMaUstawionyJsonModel() {
	$menager = new \Pastmo\Wspolne\Menager\AdnotacjeModeluMenager();
	$wynik = $menager->sprawdzCzyMetodaMaUstawionyJsonModel('\Wspolne\Controller\TestsController', 'przedWszystkimi');

	$this->assertTrue($wynik);
    }
    public function testsprawdzCzyMetodaMaUstawionyJsonModel_false() {
	$menager = new \Pastmo\Wspolne\Menager\AdnotacjeModeluMenager();
	$wynik = $menager->sprawdzCzyMetodaMaUstawionyJsonModel('\Wspolne\Controller\TestsController', 'indexAction');

	$this->assertFalse($wynik);
    }

    public function testsprawdzCzyKlasaMaUstawionyJsonModel() {
	$menager = new \Pastmo\Wspolne\Menager\AdnotacjeModeluMenager();
	$wynik = $menager->sprawdzCzyKlasaMaUstawionyJsonModel('\Wspolne\Controller\AjaxController');

	$this->assertTrue($wynik);
    }

}
