<?php

namespace PastmoTest\Wspolne\Menager;

class PobieraczAkcjiZKlasyKontroleraTest extends \Testy\BazoweKlasyTestow\AbstractMockTest {

    public function setUp() {
	parent::setUp();
    }

    public function testpobierzZwyklaAkcjeZKontrolera() {
	$pobieracz = new \Pastmo\Wspolne\Menager\PobieraczAkcjiZKlasyKontrolera($this->sm);
	$akcje = $pobieracz->pobierzZwyklaAkcjeZKontrolera(\Projekty\Controller\ProjektyController::class);

	$this->assertEquals('/projekty', $akcje[0]);
    }

    public function testpobierzZwyklaAkcjeZKontrolera_brak_ajaxowych_akcji() {
	$pobieracz = new \Pastmo\Wspolne\Menager\PobieraczAkcjiZKlasyKontrolera($this->sm);
	$akcje = $pobieracz->pobierzZwyklaAkcjeZKontrolera(\Projekty\Controller\ProjektyController::class);

	$this->assertNotContains('/projekty/szczegoly_pdf', $akcje);
    }

    public function wynikowaAkcjaAction() {

    }

}
