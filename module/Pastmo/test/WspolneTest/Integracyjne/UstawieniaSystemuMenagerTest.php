<?php

namespace Pastmo\WspolneTest\Integracyjne;

use Pastmo\Testy\Util\TB;

class UstawieniaSystemuMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    public function setUp() {
	parent::setUp();
    }

    public function testDodajEncje() {
	$wartosc = "42";

	$encja = TB::create(\Pastmo\Wspolne\Entity\UstawienieSystemu::class, $this)
		->setParameters(array('wartosc' => $wartosc))
		->make();

	$this->assertNotNull($encja->id);
	$this->assertEquals($wartosc, $encja->wartosc);
    }

}
