<?php

namespace PastmoTest\GeneratoryTestow;

class GeneratorTestowUprawnienTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    public function setUp() {
	parent::setUp();
    }

    public function test_generator() {
	return;
	$generator = new \Pastmo\Testy\GeneratoryTestow\GeneratorTestowUprawnien($this->sm);
	$generator->generuj();
	$this->assertEquals(0, count($generator->mapaUprawnien->nieznalezioneUprawnienia),
		'Nie wszystkie akcje sa zmapowane w MapaUprawnien');
    }

}
