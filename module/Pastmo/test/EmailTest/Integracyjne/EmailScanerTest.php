<?php

namespace EmailTest\Integracyjne;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Pastmo\Email\Menager\EmailScaner;

class EmailScanerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    protected $traceError = true;
    protected $emailScaner;
    private $konto;

    public function setUp() {
	parent::setUp();
	$this->konto = \FabrykaRekordow::makeEncje(\Pastmo\Email\Entity\KontoMailowe::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm));

	$this->emailScaner = new EmailScaner($this->sm);
	$this->emailScaner->init($this->konto);
    }

    public function testgetUnhandledMessages() {
	$allMessages = $this->emailScaner->getUnhandledMessages();

	$this->assertTrue(count($allMessages) > 0);
    }

    public function testgetUnhandledMessages_limit_odswiezania() {
	$wynik = $this->emailScaner->getUnhandledMessages(true);

	$this->assertEquals(10, $wynik->ilePobranychMaili);
	$this->assertGreaterThan(0, $wynik->ileMailiZostaloDoPobrania);
    }

    public function testgetAllMessages_basigFields() {

	$allMessages = $this->emailScaner->getAllMessages();

	$this->assertNotNull($allMessages[0]->subject);
	$this->assertNotNull($allMessages[0]->from);
	$this->assertNotNull($allMessages[0]->date);
    }

}
