<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace EmailTest\Integracyjne;

class OdswiezanieWiadomosciTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    const HOST = "pastmo.pl";
    const MAIL = "test@pastmo.pl";
    const HASLO = "testtest!!!AAAA!!!!!@#";

    private $kontoRobocze;
    private $kontaMailoweMenager;
    private $emailMenager;

    public function setUp() {
	parent::setUp();

	$this->kontoRobocze = new \Pastmo\Email\Entity\KontoMailowe($this->sm);
	$this->kontoRobocze->ustawHaslo(self::HASLO);
	$this->kontaMailoweMenager = $this->sm->get(\Pastmo\Email\Menager\KontaMailoweMenager::getClass());
	$this->emailMenager = $this->sm->get(\Pastmo\Email\Menager\EmaileMenager::class);
    }

    public function test_odsiwezMaile() {

	$this->zrobKontoMailowe($this->kontoRobocze->passwordKodowane);

	$this->kontaMailoweMenager->odsiwezMaile($this->kontoMailowe->id);

	$wynik = $this->emailMenager->pobierzZWherem('konto_mailowe_id=' . $this->kontoMailowe->id);

	$this->assertTrue(count($wynik) > 0);

	//sprawdzannie czy się czasem nie dodają dwa razy
	$this->kontaMailoweMenager->odsiwezMaile($this->kontoMailowe->id);

	$wynik2 = $this->emailMenager->pobierzZWherem('konto_mailowe_id=' . $this->kontoMailowe->id);

	$this->assertEquals(count($wynik), count($wynik2));
    }

    public function test_odsiwezMaileCzesciowo() {

	$this->zrobKontoMailowe($this->kontoRobocze->passwordKodowane);

	$odpowiedz = $this->kontaMailoweMenager->odsiwezMaileCzesciowo($this->kontoMailowe->id);
	$wynik = $this->emailMenager->pobierzZWherem('konto_mailowe_id=' . $this->kontoMailowe->id);

	$odpowiedz2 = $this->kontaMailoweMenager->odsiwezMaileCzesciowo($this->kontoMailowe->id);
	$wynik2 = $this->emailMenager->pobierzZWherem('konto_mailowe_id=' . $this->kontoMailowe->id);

	$this->assertEquals(\Pastmo\Email\Menager\KontaMailoweMenager::LIMIT_WIADOMOSCI_W_CZESCIOWYM_ODSWIEZANIU,
		count($wynik));
	$this->assertGreaterThan(count($wynik), count($wynik2));
	$this->assertGreaterThan($odpowiedz2->ileMailiZostaloDoPobrania, $odpowiedz->ileMailiZostaloDoPobrania);
    }

    public function test_odsiwezMaile_nie_mozna_zalogowac() {
	try {

	    $this->zrobKontoMailowe('Błędne hasło');

	    $this->kontaMailoweMenager->odsiwezMaile($this->kontoMailowe->id);

	    $this->fail('brak wyjątku');
	} catch (\Pastmo\Wspolne\Exception\PastmoException $e) {
	    $this->assertEquals('Błędny login lub hasło', $e->getMessage());
	} catch (\Exception $e) {
	    $this->fail($e->getMessage());
	}
    }

    private function zrobKontoMailowe($haslo) {
	$this->kontoMailowe = \FabrykaRekordow::utworzEncje($this->sm, \Pastmo\Email\Entity\KontoMailowe::class,
			array(
			'host' => self::HOST,
			'user' => self::MAIL,
			'passwordKodowane' => $haslo));
    }

}
