<?php

class EmailAjaxControllerTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

    public function setUp() {
	parent::setUp();
    }

    public function testIndex() {
    }

    public function test_odswiezMaileZeWszystkichSkrzynek() {
	$this->kontaMailoweManager
		->expects($this->once())
		->method('odswiezMaileZWszystkichSkrzynekCzesciowo')
		->with($this->equalTo([1, 42]))
		->willReturn([]);

	$this->dispatch('/email_ajax/odswiez_maile_ze_wszystkich_skrzynek?ignorowane_konta[]=1&ignorowane_konta[]=42');
	$this->sprawdzCzySukces();
    }

    public function  getSmallName() {}
}
