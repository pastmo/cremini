<?php

namespace EmailTest\Entity;

use Pastmo\Email\Entity\MailKomunikatZeSkanera;

require_once dirname(__FILE__) . "/TestoweMaile.php";

class EmailPobieranieTresciTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    private $message;
    private $mailKomunikatZeSkanera;
    private $zasob;
    private $mockZasobowBuilder;

    public function setUp() {
	parent::setUp();
    }

    public function testPobieranieTresci() {
	$this->ustawMessage(TestoweMaile::Z_TESTOWEJ_PLAIN);

	$this->assertEquals(' test Jeżeli wiadomość', $this->mailKomunikatZeSkanera->contenet);
	$this->sprawdzNieNull();
    }

    public function testPobieranieTresci_multipart_plain() {
	$this->ustawMessage(TestoweMaile::TESTOWY_PLAIN_MINE);

	$this->assertEquals('Zachęcamy Państwa', $this->mailKomunikatZeSkanera->contenet);
	$this->sprawdzNieNull();
    }

    public function testPobieranieTresci_multipart_html() {
	$this->ustawMessage(TestoweMaile::TESTOWY_HTML_MINE);

	$this->assertTrue(strpos($this->mailKomunikatZeSkanera->contenet, "Wypasiona treść maila") > 0);
	$this->sprawdzNieNull();
    }

    public function testPobieranieTresci_multipart_zalaczniki() {
	$this->ustawZwracanyZasob();
	$this->ustawMessage(TestoweMaile::TESTOWY_MINE_Z_ZALACZNIKIEM);

	$this->assertEquals(1, count($this->mailKomunikatZeSkanera->zasoby));
	$this->sprawdzNieNull();
    }

    public function t_estPobieranieTresci_multipart_zalaczniki_bez_Content_Disposition() {//TODOPK: Aktywować test
	$this->ustawZwracanyZasob();
	$this->ustawMessage(TestoweMaile::TESTOWY_MINE_Z_ZALACZNIKIEM_BEZ_CONTENT_DISPOSITION);

	$this->assertEquals(1, count($this->mailKomunikatZeSkanera->zasoby));
	$this->sprawdzNieNull();
    }

    public function testPobieranieTresci_multipart_zalaczniki_size_w_nazwie_zalacznika() {
	$this->ustawZwracanyZasob();
	$this->sprawdzNazwyPlikow();
	$this->ustawMessage(TestoweMaile::TESTOWY_MINE_Z_ZALACZNIKIEM_SIZE_W_ZALACZNIKU);

	$this->sprawdzNieNull();
    }

    public function testPobieranieTresci_poprawa_duplikatow_gdy_plain_i_html() {
	$this->ustawZwracanyZasob();
	$this->sprawdzNazwyPlikow();
	$this->ustawMessage(TestoweMaile::TESTOWY_MINE_Z_ZALACZNIKIEM_SIZE_W_ZALACZNIKU);

	$this->assertEquals(1, substr_count($this->mailKomunikatZeSkanera->contenet, 'Logo firmy pastmo'),
		'Duplikaty w wersji plain i html');
    }

    public function t_estPobieranieTresci_mailODGrzegorza1() {
	$this->ustawZwracanyZasob();
	$this->ustawMessage(TestoweMaile::TESTOWY_OD_GRZEGORZA_1);

	$this->assertEquals(1, substr_count($this->mailKomunikatZeSkanera->contenet, 'Logo firmy pastmo'),
		'Duplikaty w wersji plain i html');
    }

    public function t_estPobieranieTresci_mailODGrzegorza2() {
	$this->ustawZwracanyZasob();
	$this->ustawMessage(TestoweMaile::TESTOWY_OD_GRZEGORZA_2);

	$this->assertEquals(1, substr_count($this->mailKomunikatZeSkanera->contenet, 'Logo firmy pastmo'),
		'Duplikaty w wersji plain i html');
    }

    public function testPobieranieTresci_TESTOWY_OD_GRZEGORZA_NO_HEADER() {
	$this->ustawZwracanyZasob();
	$this->ustawMessage(TestoweMaile::TESTOWY_OD_GRZEGORZA_NO_HEADER);

	$this->assertEquals(1,
		substr_count($this->mailKomunikatZeSkanera->contenet,
			'Panie Grzegorzu czekam na wizualizację i może być ta statuetka większą tak do tych 300 zł.
')
	);
    }
    public function testPobieranieTresci_ENIGMATYCZNY() {
	$this->ustawZwracanyZasob();
	$this->ustawMessage(TestoweMaile::TESTOWY_OD_GRZEGORZA_ENIGMA);

	$this->sprawdzStringZawiera( $this->mailKomunikatZeSkanera->contenet,'Times New Roman');
    }

    private function ustawMessage($klucz) {
	$this->message = new \Zend\Mail\Storage\Message(array('raw' => TestoweMaile::$arr[$klucz]));
	$zasobyTable = $this->sm->get(\Zasoby\Menager\ZasobyUploadMenager::getClass());
	$this->mailKomunikatZeSkanera = new MailKomunikatZeSkanera($zasobyTable);
	$this->mailKomunikatZeSkanera->init($this->message,TestoweMaile::$arr[$klucz]);
    }

    private function ustawZwracanyZasob() {
	$this->zasob = \FabrykaEncjiMockowych::makeEncje(\Zasoby\Model\Zasob::class,
			\Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane::create());
	$this->mockZasobowBuilder = $this->obslugaKlasObcych->ustawMetode(\Zasoby\Menager\ZasobyUploadMenager::getClass(),
		'zapiszPlik', $this->zasob);
    }

    private function sprawdzNazwyPlikow() {
	$this->mockZasobowBuilder->with($this->callback(function($nazwa) {
		    return $nazwa == "324485fe.jpeg" || $nazwa == "logo.jpg";
		}));
    }

    private function sprawdzNieNull() {
	$this->assertNotNull($this->mailKomunikatZeSkanera->contenet);
	$this->assertNotNull($this->mailKomunikatZeSkanera->subject);
	$this->assertNotNull($this->mailKomunikatZeSkanera->from);
	$this->assertNotNull($this->mailKomunikatZeSkanera->date);
    }

}
