<?php

namespace EmailTest\Entity;

class TestoweMaile {

    const Z_TESTOWEJ_PLAIN = 'Z_TESTOWEJ_PLAIN';
    const TESTOWY_HTML_MINE = 'TESTOWY_HTML_MINE';
    const TESTOWY_PLAIN_MINE = 'TESTOWY_PLAIN_MINE';
    const TESTOWY_MINE_Z_ZALACZNIKIEM = 'TESTOWY_MINE_Z_ZALACZNIKIEM';
    const TESTOWY_MINE_Z_ZALACZNIKIEM_BEZ_CONTENT_DISPOSITION = 'TESTOWY_MINE_Z_ZALACZNIKIEM_BEZ_CONTENT_DISPOSITION';
    const TESTOWY_MINE_Z_ZALACZNIKIEM_SIZE_W_ZALACZNIKU = 'TESTOWY_MINE_Z_ZALACZNIKIEM_SIZE_W_ZALACZNIKU';
    const TESTOWY_OD_GRZEGORZA_1 = 'TESTOWY_OD_GRZEGORZA_1';
    const TESTOWY_OD_GRZEGORZA_2 = 'TESTOWY_OD_GRZEGORZA_2';
    const TESTOWY_OD_GRZEGORZA_NO_HEADER = 'TESTOWY_OD_GRZEGORZA_NO_HEADER';
    const TESTOWY_OD_GRZEGORZA_ENIGMA = 'TESTOWY_OD_GRZEGORZA_ENIGMA';

    public static $arr = array(
	    self::Z_TESTOWEJ_PLAIN => "Return-Path: <paul@tidio.net>
Received: from mail-lf0-f51.google.com (209.85.215.51) (HELO mail-lf0-f51.google.com)
 by serwer1578643.home.pl (79.96.198.239) with SMTP (IdeaSmtpServer v0.80)
 id c525aa351e23c3d4; Fri, 20 Nov 2015 12:13:06 +0100
Received: by lffu14 with SMTP id u14so66912232lff.1
        for <testowe1@grupaglasso.pl>; Fri, 20 Nov 2015 03:13:05 -0800 (PST)
DKIM-Signature: v=1; a=rsa-sha256; c=relaxed/relaxed;
        d=tidio-net.20150623.gappssmtp.com; s=20150623;
        h=to:from:subject:message-id:date:user-agent:mime-version
         :content-type:content-transfer-encoding;
        bh=g3zLYH4xKxcPrHOD18z9YfpQcnk/GaJedfustWU5uGs=;
        b=s0OhTBCxKGXDUZ/LEJea2csom8MMFa/GlWMshNuAwLygRufn2IG00CMV32k7wORYEg
         bB7XeZQKo5PfDsQW1ZSGkOXA8Tu0l2tSVKezgjjh/GaelSg5UPWGIy3qlH486pzjqYKc
         JV/CwtETdVn1ENr53o7Y2uHq1b+tIdXpwR/Eq2TJs4iLg6+tRwUU9fd1+S+4OGjBmNMR
         bOhf0fulAl2pO3clkRbZw9MAX81NWUrB78EWvRG5vy8z0FsTdJVCDKRbV8kRa5sz+CtV
         pNf8kqxixYA06e6UIc6q3VyZEt8TN03+jUmTcyJT/MLK1J06IuUP14PbvqtOCdwWJ8Iz
         hdpA==
X-Google-DKIM-Signature: v=1; a=rsa-sha256; c=relaxed/relaxed;
        d=1e100.net; s=20130820;
        h=x-gm-message-state:to:from:subject:message-id:date:user-agent
         :mime-version:content-type:content-transfer-encoding;
        bh=g3zLYH4xKxcPrHOD18z9YfpQcnk/GaJedfustWU5uGs=;
        b=WanNpiCc9ZwdyZgTZlsnq/KNwuVTRJQm1sUbT06PY5M6XpuQm5GsIXTr1a7qeR+dR+
         VM6SdVGd11bFTTwc/UEOBDzFt5xRZCY5pD3eDXrYOUbbcv7lsEtpIpv2gypol0gkbjDr
         uDNdMz1FMNRfg0lcOG/z33j4hxCDiBv3tFrVdDWi1W11i1PpX3es8uJ8iOppENfPHfZO
         vx8PSVPummDqMCzIlnPZ6cAOMlnXGlCIJxfnO3+DxcJoZ1+drtc4DiyKIlpd7qjevCRN
         nLWNsM025SjaxG3n453I7hxJ6qq5ZwQ33knAaekztxId4eeACzgY0hIhb3HXCBwIy1GP
         Oocg==
X-Gm-Message-State: ALoCoQkmPH4cWqNbamVGqaWXTnxhF35SEby+fnhvpzLwY5ZNPO7g3P0HQGHh5qX2JT4XWRb3ND4K
X-Received: by 10.25.17.196 with SMTP id 65mr4580996lfr.137.1448017985705;
        Fri, 20 Nov 2015 03:13:05 -0800 (PST)
Return-Path: <paul@tidio.net>
Received: from [192.168.1.252] ([37.131.175.241])
        by smtp.gmail.com with ESMTPSA id j123sm1600547lfb.19.2015.11.20.03.13.04
        for <testowe1@grupaglasso.pl>
        (version=TLSv1/SSLv3 cipher=OTHER);
        Fri, 20 Nov 2015 03:13:05 -0800 (PST)
To: testowe1@grupaglasso.pl
From: Pawel Brzoski <paul@tidio.net>
Subject: tes
Message-ID: <564F0042.3020001@tidio.net>
Date: Fri, 20 Nov 2015 12:13:06 +0100
User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64; rv:38.0) Gecko/20100101
 Thunderbird/38.2.0
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8; format=flowed
Content-Transfer-Encoding: 7bit

\t\t\ttest Je=C5=BCeli wiadomo=C5=9B=C4=87=",
	    self::TESTOWY_HTML_MINE => 'Delivered-To: pawel.k.kazmierczak@gmail.com
Received: by 10.103.71.200 with SMTP id b69csp593820vsg;
        Mon, 25 Jul 2016 03:35:19 -0700 (PDT)
X-Received: by 10.194.153.5 with SMTP id vc5mr14634440wjb.75.1469442919399;
        Mon, 25 Jul 2016 03:35:19 -0700 (PDT)
Return-Path: <testowe1@grupaglasso.pl>
Received: from cloudserver101331.home.net.pl (cloudserver101331.home.net.pl. [79.96.198.239])
        by mx.google.com with SMTP id dw15si14223582wjb.134.2016.07.25.03.35.18
        for <pawel.k.kazmierczak@gmail.com>;
        Mon, 25 Jul 2016 03:35:19 -0700 (PDT)
Received-SPF: neutral (google.com: 79.96.198.239 is neither permitted nor denied by best guess record for domain of testowe1@grupaglasso.pl) client-ip=79.96.198.239;
Authentication-Results: mx.google.com;
       spf=neutral (google.com: 79.96.198.239 is neither permitted nor denied by best guess record for domain of testowe1@grupaglasso.pl) smtp.mailfrom=testowe1@grupaglasso.pl
Message-Id: <5795eb67.0f61c20a.78d27.7611SMTPIN_ADDED_MISSING@mx.google.com>
Return-Path: <testowe1@grupaglasso.pl>
Received: from 095160157200.dynamic-ra-10.vectranet.pl (95.160.157.200) (HELO serwer1578643.home.pl)
 by serwer1578643.home.pl (79.96.198.239) with SMTP (IdeaSmtpServer v0.80.3)
 id d6e1ed4878656529; Mon, 25 Jul 2016 12:35:15 +0200
Date: Mon, 25 Jul 2016 12:35:43 +0200
MIME-Version: 1.0
Content-Type: multipart/mixed;
 boundary="=_b96ae319ddefa3928c629e9ca2f670ac"
From: testowe1@grupaglasso.pl <testowe1@grupaglasso.pl>
To: Uzytkownik <pawel.k.kazmierczak@gmail.com>
Subject: =?UTF-8?Q?Ej,=20teraz=20to=20musi=20przej=C5=9B=C4=87!?=
Sender: Uzytkownik <pawel.k.kazmierczak@gmail.com>

This is a message in Mime Format.  If you see this, your mail reader does not support this format.

--=_b96ae319ddefa3928c629e9ca2f670ac
Content-Type: text/html; charset=utf-8
Content-Transfer-Encoding: 8bit

<b>Wypasiona treść maila</b><div style="text-align: center;"><b>W zwariowany sposób sformatowana</b></div><div style="text-align: justify;"><b>Idealnie i perfekcyjnie dopracowana</b></div><div style="text-align: justify;"><b><u><strike><i>o tak!</i></strike></u></b></div>
--=_b96ae319ddefa3928c629e9ca2f670ac--',
	    self::TESTOWY_PLAIN_MINE => 'Delivered-To: pawel.k.kazmierczak@gmail.com
Received: by 10.103.71.200 with SMTP id b69csp593820vsg;
        Mon, 25 Jul 2016 03:35:19 -0700 (PDT)
X-Received: by 10.194.153.5 with SMTP id vc5mr14634440wjb.75.1469442919399;
        Mon, 25 Jul 2016 03:35:19 -0700 (PDT)
Return-Path: <testowe1@grupaglasso.pl>
Received: from cloudserver101331.home.net.pl (cloudserver101331.home.net.pl. [79.96.198.239])
        by mx.google.com with SMTP id dw15si14223582wjb.134.2016.07.25.03.35.18
        for <pawel.k.kazmierczak@gmail.com>;
        Mon, 25 Jul 2016 03:35:19 -0700 (PDT)
Received-SPF: neutral (google.com: 79.96.198.239 is neither permitted nor denied by best guess record for domain of testowe1@grupaglasso.pl) client-ip=79.96.198.239;
Authentication-Results: mx.google.com;
       spf=neutral (google.com: 79.96.198.239 is neither permitted nor denied by best guess record for domain of testowe1@grupaglasso.pl) smtp.mailfrom=testowe1@grupaglasso.pl
Message-Id: <5795eb67.0f61c20a.78d27.7611SMTPIN_ADDED_MISSING@mx.google.com>
Return-Path: <testowe1@grupaglasso.pl>
Received: from 095160157200.dynamic-ra-10.vectranet.pl (95.160.157.200) (HELO serwer1578643.home.pl)
 by serwer1578643.home.pl (79.96.198.239) with SMTP (IdeaSmtpServer v0.80.3)
 id d6e1ed4878656529; Mon, 25 Jul 2016 12:35:15 +0200
Date: Mon, 25 Jul 2016 12:35:43 +0200
MIME-Version: 1.0
Content-Type: multipart/mixed;
 boundary="=_b96ae319ddefa3928c629e9ca2f670ac"
From: testowe1@grupaglasso.pl <testowe1@grupaglasso.pl>
To: Uzytkownik <pawel.k.kazmierczak@gmail.com>
Subject: =?UTF-8?Q?Ej,=20teraz=20to=20musi=20przej=C5=9B=C4=87!?=
Sender: Uzytkownik <pawel.k.kazmierczak@gmail.com>

This is a message in Mime Format.  If you see this, your mail reader does not support this format.

--=_b96ae319ddefa3928c629e9ca2f670ac
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit

Zach=C4=99camy Pa=C5=84stwa--=_b96ae319ddefa3928c629e9ca2f670ac--',
	    self::TESTOWY_MINE_Z_ZALACZNIKIEM => 'Delivered-To: pawel.k.kazmierczak@gmail.com
Received: by 10.103.71.200 with SMTP id b69csp1099944vsg;
        Tue, 26 Jul 2016 03:11:48 -0700 (PDT)
X-Received: by 10.25.17.37 with SMTP id g37mr7884406lfi.113.1469527908398;
        Tue, 26 Jul 2016 03:11:48 -0700 (PDT)
Return-Path: <test@iskra-zlecen.nazwa.pl>
Received: from ane213.rev.netart.pl (ane213.rev.netart.pl. [85.128.213.213])
        by mx.google.com with ESMTPS id 11si24655lfv.306.2016.07.26.03.11.48
        for <pawel.k.kazmierczak@gmail.com>
        (version=TLS1_2 cipher=ECDHE-RSA-AES128-GCM-SHA256 bits=128/128);
        Tue, 26 Jul 2016 03:11:48 -0700 (PDT)
Received-SPF: neutral (google.com: 85.128.213.213 is neither permitted nor denied by best guess record for domain of test@iskra-zlecen.nazwa.pl) client-ip=85.128.213.213;
Authentication-Results: mx.google.com;
       spf=neutral (google.com: 85.128.213.213 is neither permitted nor denied by best guess record for domain of test@iskra-zlecen.nazwa.pl) smtp.mailfrom=test@iskra-zlecen.nazwa.pl
Received: from iskra-zlecen.nazwa.pl (unknown [85.128.142.31])
	by iskra-zlecen.nazwa.pl (Postfix) with ESMTP id C10702DA082
	for <pawel.k.kazmierczak@gmail.com>; Tue, 26 Jul 2016 12:11:47 +0200 (CEST)
Date: Tue, 26 Jul 2016 12:11:47 +0200
MIME-Version: 1.0
Content-Type: multipart/mixed;
 boundary="=_9e3eae895193ecaff22e711c256ed4e0"
From: test@iskra-zlecen.nazwa.pl <test@iskra-zlecen.nazwa.pl>
To: Uzytkownik <pawel.k.kazmierczak@gmail.com>
Subject: =?UTF-8?Q?wiadomo=C5=9B=C4=87=20z=20za=C5=82=C4=85cznikiem?=
Sender: Uzytkownik <pawel.k.kazmierczak@gmail.com>
Message-Id: <20160726101147.C10702DA082@iskra-zlecen.nazwa.pl>

This is a message in Mime Format.  If you see this, your mail reader does not support this format.

--=_9e3eae895193ecaff22e711c256ed4e0
Content-Type: text/html; charset=utf-8
Content-Transfer-Encoding: 8bit

To tak jak ciastko z kremem:)<div>ąśę polska czcionka śmiga świetnie że aż słychać świst.</div>
--=_9e3eae895193ecaff22e711c256ed4e0
Content-Type: image/jpeg
Content-Transfer-Encoding: base64
Content-Disposition: attachment; filename="logo.jpg"

/9j/4AAQSkZJRgABAQEBLAEsAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0a
HBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIy
MjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCACUAPADASIA
AhEBAxEB/8QAHAABAAIDAQEBAAAAAAAAAAAAAAYHAQQFAwII/8QATBAAAQMDAQQECAgKCAcAAAAA
AQACAwQFBhEHEiExE0FRYRQiMnGBkaHRFRYXQlWSlMEIIyRFVHKCk7HCJTNEUlPS4eI0NnODhKLw
/8QAGAEBAQEBAQAAAAAAAAAAAAAAAAECAwT/xAAiEQEBAAIBBAMBAQEAAAAAAAAAAQIREiExUWED
IkFxEzL/2gAMAwEAAhEDEQA/AL/REQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBER
AREQEREBERAREQEREBFhZQEREBEWC4NGpIA7Sgyi1n19HH5dXA39aQBeDr5aWeXdKJvnqGD71dVN
x0EXLOS2JvO9W4eeqZ718HKseHO/Wwf+XH701TcddFx/jXjv0/avtkfvT41459P2v7ZH701TcdhF
yPjXjn0/a/tkfvT41Y79PWv7ZH701TcddFyfjRj/ANO2z7XH71n4z2D6ctv2uP3pqm46qLlfGaw/
Tdu+1M966o5KaXYiIgIiICIiAiIgIixqgysOc1rS5xAAGpJPJQzNdpFpw6MwO/LLk4eJSRu4jsLj
80e3uUJhxjN9pbhVZHXOtFnf4zaSNpDnDqG597tfMumPx7m70jFz66iY37aziliL4/DfDZ2846Qb
/H9byfao63aTmeQf8t4a9kDvJqKskhw7R5I9pXddjWI7NMfnvItgmdStB6WUB8rnEgAAngCSerTm
pTjl6jyPH6O7wwSwR1TC9scum8BqR1ebXzFX6ybk2n2t1ar5tm2tXYa1N8oLY3XyYgCQPQ0/xXoN
luR1Tg+47Qbm8nm2Bjo/bv8A3K0kU/0v4vCfqsDsUt83GryO+VB69+caH2L6j2GYk06yOrpT1l02
n8ArNWNVP9c/Jwx8K7bsSwtv9kqj56hy9m7GcLb+b5T553Lv3PN8Zs7nMrb3RxyNOhY2TfcD2EN1
IUaqttmHU5cGVFVMR/cgOh9J0WpfkvbaWYTu227IMLb+ayfPM73r7GyXCx+aB6ZXe9cMbdbDIfxF
qu0w7WRNP8y9G7bbSedhvbR29A0/err5fab+N2vkmwv6Hb+8d71n5J8L+hmfvHe9SugrGV9BT1kb
HsZPGJGtkGjgCNdCO1bKxzy8t8cfCFfJPhf0Mz9473rHyS4X9Dt/eO96myJzy8nDHwhHySYX9ED9
673p8keF/RI/eu96m68aupio6OaqneGQwsdI9x5BoGpPqCc8vJxx8KPvGGWE7WLJj1moRFFA0VVc
d4u3gDvBp17gPrq9hyVV7JaeW9XG/ZnVNIfX1Bipw75sYOp/lH7JVqK/LbuS/jPxzpsREXN0EREB
ERAREQFWu0LaFPbKpmOY5H4TfaghhLBr0GvLh1u/hzK6W0rN/ilZ2wUY6S71v4umjHEt6t8ju5Dt
K1dmuBfF2ldeLprNfa0F8r38TEHcS3X+8es9q6YyScsmMrbeMeGE7N6TGo3X7IJRWXggzyyy+M2A
8yRrzd2u9SVu2O0OnfTWC2197qW/NpoyG+k6E6d+isggEEEag8wV401JTUUIhpaeKCIcmRMDWj0B
Tnu7y6nHU1H572i5Zld/p6Oz3WyMtUFTM2WCHeJkkI8UanXlq7sHFSKni2v2O208FPT0b6amjbHH
CwRuIa0aAdvILzzAm8berHbz40dN0RLezm8+wBWlkGV2XGKYzXSujhOmrYtdZH+ZvMrtllqYyRzk
3bbUawDaLJlFXUWe60XgN5pQS+MagPAOh0B4gjUajj2qfOeGNLnEBoGpJPABU5gkdXmW06szdtI6
ktrGOih3ucp3dwcevhqT2HQdSn+ZYrNl1BBQfC89DSiTeqY4mA9Oz+6TqNPaO4rlnjJlrs3hbxRq
+7V2vuDrPiFuferly6RmvQs7+Hlewd64V0wvMr3aau6ZbfZWU8MLpTbKIgb4A13dfJHLrDladgx2
141b20drpGQRjynAauee1x5krdro+moKmI/Pic31hWZyX6w4291T7MMTw3Isd+E/gLSZkzoXsqKg
zaEAHXk0cQR1K0KOx2m3aeBWyjp9OuKBrT7Aqx2DPcy2XykJ8WGqbp6QR/KreT5bZlZs+OTjKaJo
iLk6GiIiAiIgKttsN7lp7BTY/Raur7zKIWMaeO5qNfWSB6VY73NYxz3EBrRqSeoKocRDs+2n1+Vy
Autts/J6LUcC7jpp6CXftBdPjnXlfxjPtryszHLNFj+PUVqhILaaIMLgPKd1n0nVdRYCyufdudBE
RAREQEREBalyuNNabZU19W8Mp6eMyPd3BbaqXajX1GR3+14HbHkPqJBNWPb81o4gegau9AWsMeV0
zldTbX2fWqpzbLKvO7zGegZIWW+B3EN05H9kcO8knqVxdS1LXbqa0Wumt9HGGU9PGI2NHYB/FbaZ
5cqY46jCrnJM6yqkvtVabDh89V0JAbVy73RyagHUAADr08rqVjrGimNkvWbWy3s/MdNRZRlW1Koh
kqWWy+uDjLI0lohAjAIG7qfJOnPr5qev2ZYxhttqMgymtqLtJD47+kG62Rx4AbupLiT2krSxsbv4
Rl5DuZbLp9VhXV20TMnfjdpnmZFS1NbvzuedGho0GpPZo5y9WWVuUxnSacMZJLa8KXa3cKGCjrKz
D5KHHJSI4ahjz4jeogboBGnVw7iVZdzv1vtNgkvVTOBQsjEm+3jvA6aadpJIA86jGZ3bF63BLlbR
ebYWOpS2JjahjjvNGrdAD1EBRS32itzrYHRUFLIPDaV5DGuOgf0b3AN+qRp3gLlccbJe3VuWzp3b
fyw3ipaau34NX1Ft18WfpHavb28GEe0+dT3GcmpMsxxt1pI5Io3bzHxyjRzHN4EFQew7WaG2RU9m
yi21NnrYGNiJdGTG7ThqOsD0Ed5ViSV1JPYJq6imilpnQPlZJGQWu4E66+dTOa6a0uN9qx2GcX5M
4cvCm/zro5JtYqLdeK2jslikucFt411SHkNZpzA0B5dp7DwXJ2KudT4hkVz5b0xcD3tYT/Muzsdo
GVGz2oqZmgvudTM+Ukcxru6ew+tbzk5XKs471JGzftqMFJaLPNZaB9xr7u3epqYO03RyO9px1B4a
dx7F1cJzdmVx1VPU0T7fdaJ27U0j3a7vYQdBw9HD2qs9lFkqG7Q6qOqjduWKGWnjDh5JdI4j0+M7
1qTVLxbPwh6YR8BcraOkHa4bwB9UYUywxn1hMr3qU5jnVDiMcELoJK25VR0p6KHy5DyHboNeHI+Z
cGh2oVlNdqWhyrG57IyrcGwVDpd9mp5B3Aaef2BamDwtyTaZk2R1X4x1FKKSkDhr0Y4jh2cB/wCx
Uk2n22G47PLwJWjep4DURu62uZ43D0Aj0rOsZZjY1u2col4WVH8HrZbhg9mqpjrJJSsLj28NPuWx
k2RUWL2Ooulc7RkbfEYOcj+po7yuervTe+m0M2t5LNS2+DF7Zq+6XciLdYeLYyd0/WPDzaqXYfjk
OK4zSWqLQvjbvSvHz5DxcfX7FAdmNirL9eKnPb63WoqXEUcZ+Y3lvDu08Uek9attbz+s4RjHreVE
RFzdBERAREQEREGpc7hBabXU3CqduwU8bpHnuAVa7JLZPc6q7ZtcW/lVylcyHX5kYPHTu1AHmavb
bDcZ6mkteKUDvyu71DQ7T5sYPWO86ehpVhWi2wWe0Ulupm7sNNE2JvmA5+c810/5w/rHfL+N1ERc
2xERBSkLTRfhLyb3BtS0keY0/vauptTt8FxzLDaWsZv0tRUmKRuumoLm6j2rm7R/6E2u43ezqI5O
ja8/qu0I9TlZl9xehyGttVZVPla+2z9PD0btATw4Hu4Bd7lrjl6cZN7ntGavZRh9Bbq2pjthdIyB
7m9JK5wBDTodNVy9kV3oLLszZU3OripYHV742ySu0BcQNB7CrTmhZUQSQyDejkaWOHaCNCo/FguO
x44ywG3NktzJDK2ORxJDzr42vPXisc9zWTXDV3GnkuU4VJYqgXO422tpyw/iWSslc46cNANSD39S
gGJz1No2G5FWyiSKmmMpomv5gP0YCO4uP8e1T2j2WYdRVAmZZo5HA6gSuc8D0E6KO7b7gKXEaOzU
4DX1tQ1oY3gNxnHTT9bd9S3hZbMYmUv/AFX1s0tzqfYxOQCH1jKmXT1sHsYPWt/YpKJNmlG0HjHP
M0/XJ+9Suw2hltxSgtLgd2KlbC4cvm6H70xrGrdilq+DbYx7YOkdId95cS48+PoCxlnvf9XHHWnX
00VVXVnhP4RVo3OPg1t3nd39b/mCtZcZuMWxmVSZG2J/wk+HoHP3zulvDq7eAWcLJtrKbVzi1ygw
nabf7DdHtp4LlKKmlmkOjTqSQNTw46kedui7W1C/xzWcYtbHtqLvd3NgZDG7UsYSCXHTkNB19Wp6
lJ8lxCy5ZTMhu1IJTH/VyNO69mvPQhQqolwPZE2V9LD0t1ezxYg/fmIPUSfIB/8AtV0lmVl/WLLj
Nfiasmt2E4hAK2oZFSUFO1hefnEDkB1knkFVtvpLjtjyht0uDJKbGKF+kUP+KR1d5PWeocAvu3Y5
ke1W5w3jJi+hscbt6CjbqC8dw7+tx4nqVy0VFTW6iipKSFkNPE0NZGwaBoS2Yf0k5fx6xRMhiZFE
wMjYA1rWjQADkAvtEXF1EREBERAREQERad0bWPtdW237grHRObCXnQB5HAnzIKzxgfG7bDeb6/x6
K0N8Epj1F/Eaj1PPpBVsKm8cw3aTitA+jtVXZ443yGR5eN5znHtJaux4Ltd/T7L9T/au2eMt6Vyx
tk6xZiKsvBtrv6bZfqf6J0G139Lsv1f9Fjh7jXP0s1FWXQ7Xf0myn9n/AETotrv6RZfqj3Jw9w5+
kjzbBqLN6Wkhq6memdTSF7JIdNdCNCOPo9Sk1PEIKaKEOc4RtDQXczoNOKrbodrv6VZfqj3J4Ntd
P9tso/YHuV43WtxOXXelmoqyFFtddzullb/2/wDavr4K2tP536ys80Z/yKcPcXl6WWolf8DpMiyq
2XusrJ9KDQtpQBuOIOvHr56a+ZR52O7VZPKyu2MHayM/5FgYZtHlP43PGR/9On19ysx11mSW7/Fn
pqqvfszyup/4raNXkHmI6ct9okXy7YpSVYHwpk15ru0PlGh9eqnHHyvLLwnNxyvH7SHeHXmihc3m
wzAuH7I4qG3XbbjdI/obbFVXOoJ0YyFm61x6uJ+4FbtBscw2iILqCSoI/wAeZxB9A0UttlhtNmZu
223UtLw0JiiDSR3nmVfpPZ976Va+u2n514lJRtx63v4F7yWv0858Y+gBSHFdkllsEza2vc663He3
zNUN8UO7Q3U8e8klWCspfkutToTCd71YCyiLm2IiICIiAiIgIiICaIiAiIgIiICIiAiIgIiICIiA
iIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiI
CIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIg//9k=
--=_9e3eae895193ecaff22e711c256ed4e0--',
	    self::TESTOWY_MINE_Z_ZALACZNIKIEM_BEZ_CONTENT_DISPOSITION =>
	    'Delivered-To: pawel.k.kazmierczak@gmail.com
Received: by 10.103.71.200 with SMTP id b69csp1099944vsg;
        Tue, 26 Jul 2016 03:11:48 -0700 (PDT)
X-Received: by 10.25.17.37 with SMTP id g37mr7884406lfi.113.1469527908398;
        Tue, 26 Jul 2016 03:11:48 -0700 (PDT)
Return-Path: <test@iskra-zlecen.nazwa.pl>
Received: from ane213.rev.netart.pl (ane213.rev.netart.pl. [85.128.213.213])
        by mx.google.com with ESMTPS id 11si24655lfv.306.2016.07.26.03.11.48
        for <pawel.k.kazmierczak@gmail.com>
        (version=TLS1_2 cipher=ECDHE-RSA-AES128-GCM-SHA256 bits=128/128);
        Tue, 26 Jul 2016 03:11:48 -0700 (PDT)
Received-SPF: neutral (google.com: 85.128.213.213 is neither permitted nor denied by best guess record for domain of test@iskra-zlecen.nazwa.pl) client-ip=85.128.213.213;
Authentication-Results: mx.google.com;
       spf=neutral (google.com: 85.128.213.213 is neither permitted nor denied by best guess record for domain of test@iskra-zlecen.nazwa.pl) smtp.mailfrom=test@iskra-zlecen.nazwa.pl
Received: from iskra-zlecen.nazwa.pl (unknown [85.128.142.31])
	by iskra-zlecen.nazwa.pl (Postfix) with ESMTP id C10702DA082
	for <pawel.k.kazmierczak@gmail.com>; Tue, 26 Jul 2016 12:11:47 +0200 (CEST)
Date: Tue, 26 Jul 2016 12:11:47 +0200
MIME-Version: 1.0
Content-Type: multipart/mixed;
 boundary="=_9e3eae895193ecaff22e711c256ed4e0"
From: test@iskra-zlecen.nazwa.pl <test@iskra-zlecen.nazwa.pl>
To: Uzytkownik <pawel.k.kazmierczak@gmail.com>
Subject: =?UTF-8?Q?wiadomo=C5=9B=C4=87=20z=20za=C5=82=C4=85cznikiem?=
Sender: Uzytkownik <pawel.k.kazmierczak@gmail.com>
Message-Id: <20160726101147.C10702DA082@iskra-zlecen.nazwa.pl>

This is a message in Mime Format.  If you see this, your mail reader does not support this format.

--=_9e3eae895193ecaff22e711c256ed4e0
Content-Type: text/html; charset=utf-8
Content-Transfer-Encoding: 8bit

To tak jak ciastko z kremem:)<div>ąśę polska czcionka śmiga świetnie że aż słychać świst.</div>
--=_9e3eae895193ecaff22e711c256ed4e0
Content-Type: image/jpeg
Content-Transfer-Encoding: base64

/9j/4AAQSkZJRgABAQEBLAEsAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0a
HBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIy
MjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCACUAPADASIA
AhEBAxEB/8QAHAABAAIDAQEBAAAAAAAAAAAAAAYHAQQFAwII/8QATBAAAQMDAQQECAgKCAcAAAAA
AQACAwQFBhEHEiExE0FRYRQiMnGBkaHRFRYXQlWSlMEIIyRFVHKCk7HCJTNEUlPS4eI0NnODhKLw
/8QAGAEBAQEBAQAAAAAAAAAAAAAAAAECAwT/xAAiEQEBAAIBBAMBAQEAAAAAAAAAAQIREiExUWED
IkFxEzL/2gAMAwEAAhEDEQA/AL/REQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBER
AREQEREBERAREQEREBFhZQEREBEWC4NGpIA7Sgyi1n19HH5dXA39aQBeDr5aWeXdKJvnqGD71dVN
x0EXLOS2JvO9W4eeqZ718HKseHO/Wwf+XH701TcddFx/jXjv0/avtkfvT41459P2v7ZH701TcdhF
yPjXjn0/a/tkfvT41Y79PWv7ZH701TcddFyfjRj/ANO2z7XH71n4z2D6ctv2uP3pqm46qLlfGaw/
Tdu+1M966o5KaXYiIgIiICIiAiIgIixqgysOc1rS5xAAGpJPJQzNdpFpw6MwO/LLk4eJSRu4jsLj
80e3uUJhxjN9pbhVZHXOtFnf4zaSNpDnDqG597tfMumPx7m70jFz66iY37aziliL4/DfDZ2846Qb
/H9byfao63aTmeQf8t4a9kDvJqKskhw7R5I9pXddjWI7NMfnvItgmdStB6WUB8rnEgAAngCSerTm
pTjl6jyPH6O7wwSwR1TC9scum8BqR1ebXzFX6ybk2n2t1ar5tm2tXYa1N8oLY3XyYgCQPQ0/xXoN
luR1Tg+47Qbm8nm2Bjo/bv8A3K0kU/0v4vCfqsDsUt83GryO+VB69+caH2L6j2GYk06yOrpT1l02
n8ArNWNVP9c/Jwx8K7bsSwtv9kqj56hy9m7GcLb+b5T553Lv3PN8Zs7nMrb3RxyNOhY2TfcD2EN1
IUaqttmHU5cGVFVMR/cgOh9J0WpfkvbaWYTu227IMLb+ayfPM73r7GyXCx+aB6ZXe9cMbdbDIfxF
qu0w7WRNP8y9G7bbSedhvbR29A0/err5fab+N2vkmwv6Hb+8d71n5J8L+hmfvHe9SugrGV9BT1kb
HsZPGJGtkGjgCNdCO1bKxzy8t8cfCFfJPhf0Mz9473rHyS4X9Dt/eO96myJzy8nDHwhHySYX9ED9
673p8keF/RI/eu96m68aupio6OaqneGQwsdI9x5BoGpPqCc8vJxx8KPvGGWE7WLJj1moRFFA0VVc
d4u3gDvBp17gPrq9hyVV7JaeW9XG/ZnVNIfX1Bipw75sYOp/lH7JVqK/LbuS/jPxzpsREXN0EREB
ERAREQFWu0LaFPbKpmOY5H4TfaghhLBr0GvLh1u/hzK6W0rN/ilZ2wUY6S71v4umjHEt6t8ju5Dt
K1dmuBfF2ldeLprNfa0F8r38TEHcS3X+8es9q6YyScsmMrbeMeGE7N6TGo3X7IJRWXggzyyy+M2A
8yRrzd2u9SVu2O0OnfTWC2197qW/NpoyG+k6E6d+isggEEEag8wV401JTUUIhpaeKCIcmRMDWj0B
Tnu7y6nHU1H572i5Zld/p6Oz3WyMtUFTM2WCHeJkkI8UanXlq7sHFSKni2v2O208FPT0b6amjbHH
CwRuIa0aAdvILzzAm8berHbz40dN0RLezm8+wBWlkGV2XGKYzXSujhOmrYtdZH+ZvMrtllqYyRzk
3bbUawDaLJlFXUWe60XgN5pQS+MagPAOh0B4gjUajj2qfOeGNLnEBoGpJPABU5gkdXmW06szdtI6
ktrGOih3ucp3dwcevhqT2HQdSn+ZYrNl1BBQfC89DSiTeqY4mA9Oz+6TqNPaO4rlnjJlrs3hbxRq
+7V2vuDrPiFuferly6RmvQs7+Hlewd64V0wvMr3aau6ZbfZWU8MLpTbKIgb4A13dfJHLrDladgx2
141b20drpGQRjynAauee1x5krdro+moKmI/Pic31hWZyX6w4291T7MMTw3Isd+E/gLSZkzoXsqKg
zaEAHXk0cQR1K0KOx2m3aeBWyjp9OuKBrT7Aqx2DPcy2XykJ8WGqbp6QR/KreT5bZlZs+OTjKaJo
iLk6GiIiAiIgKttsN7lp7BTY/Raur7zKIWMaeO5qNfWSB6VY73NYxz3EBrRqSeoKocRDs+2n1+Vy
Autts/J6LUcC7jpp6CXftBdPjnXlfxjPtryszHLNFj+PUVqhILaaIMLgPKd1n0nVdRYCyufdudBE
RAREQEREBalyuNNabZU19W8Mp6eMyPd3BbaqXajX1GR3+14HbHkPqJBNWPb81o4gegau9AWsMeV0
zldTbX2fWqpzbLKvO7zGegZIWW+B3EN05H9kcO8knqVxdS1LXbqa0Wumt9HGGU9PGI2NHYB/FbaZ
5cqY46jCrnJM6yqkvtVabDh89V0JAbVy73RyagHUAADr08rqVjrGimNkvWbWy3s/MdNRZRlW1Koh
kqWWy+uDjLI0lohAjAIG7qfJOnPr5qev2ZYxhttqMgymtqLtJD47+kG62Rx4AbupLiT2krSxsbv4
Rl5DuZbLp9VhXV20TMnfjdpnmZFS1NbvzuedGho0GpPZo5y9WWVuUxnSacMZJLa8KXa3cKGCjrKz
D5KHHJSI4ahjz4jeogboBGnVw7iVZdzv1vtNgkvVTOBQsjEm+3jvA6aadpJIA86jGZ3bF63BLlbR
ebYWOpS2JjahjjvNGrdAD1EBRS32itzrYHRUFLIPDaV5DGuOgf0b3AN+qRp3gLlccbJe3VuWzp3b
fyw3ipaau34NX1Ft18WfpHavb28GEe0+dT3GcmpMsxxt1pI5Io3bzHxyjRzHN4EFQew7WaG2RU9m
yi21NnrYGNiJdGTG7ThqOsD0Ed5ViSV1JPYJq6imilpnQPlZJGQWu4E66+dTOa6a0uN9qx2GcX5M
4cvCm/zro5JtYqLdeK2jslikucFt411SHkNZpzA0B5dp7DwXJ2KudT4hkVz5b0xcD3tYT/Muzsdo
GVGz2oqZmgvudTM+Ukcxru6ew+tbzk5XKs471JGzftqMFJaLPNZaB9xr7u3epqYO03RyO9px1B4a
dx7F1cJzdmVx1VPU0T7fdaJ27U0j3a7vYQdBw9HD2qs9lFkqG7Q6qOqjduWKGWnjDh5JdI4j0+M7
1qTVLxbPwh6YR8BcraOkHa4bwB9UYUywxn1hMr3qU5jnVDiMcELoJK25VR0p6KHy5DyHboNeHI+Z
cGh2oVlNdqWhyrG57IyrcGwVDpd9mp5B3Aaef2BamDwtyTaZk2R1X4x1FKKSkDhr0Y4jh2cB/wCx
Uk2n22G47PLwJWjep4DURu62uZ43D0Aj0rOsZZjY1u2col4WVH8HrZbhg9mqpjrJJSsLj28NPuWx
k2RUWL2Ooulc7RkbfEYOcj+po7yuervTe+m0M2t5LNS2+DF7Zq+6XciLdYeLYyd0/WPDzaqXYfjk
OK4zSWqLQvjbvSvHz5DxcfX7FAdmNirL9eKnPb63WoqXEUcZ+Y3lvDu08Uek9attbz+s4RjHreVE
RFzdBERAREQEREGpc7hBabXU3CqduwU8bpHnuAVa7JLZPc6q7ZtcW/lVylcyHX5kYPHTu1AHmavb
bDcZ6mkteKUDvyu71DQ7T5sYPWO86ehpVhWi2wWe0Ulupm7sNNE2JvmA5+c810/5w/rHfL+N1ERc
2xERBSkLTRfhLyb3BtS0keY0/vauptTt8FxzLDaWsZv0tRUmKRuumoLm6j2rm7R/6E2u43ezqI5O
ja8/qu0I9TlZl9xehyGttVZVPla+2z9PD0btATw4Hu4Bd7lrjl6cZN7ntGavZRh9Bbq2pjthdIyB
7m9JK5wBDTodNVy9kV3oLLszZU3OripYHV742ySu0BcQNB7CrTmhZUQSQyDejkaWOHaCNCo/FguO
x44ywG3NktzJDK2ORxJDzr42vPXisc9zWTXDV3GnkuU4VJYqgXO422tpyw/iWSslc46cNANSD39S
gGJz1No2G5FWyiSKmmMpomv5gP0YCO4uP8e1T2j2WYdRVAmZZo5HA6gSuc8D0E6KO7b7gKXEaOzU
4DX1tQ1oY3gNxnHTT9bd9S3hZbMYmUv/AFX1s0tzqfYxOQCH1jKmXT1sHsYPWt/YpKJNmlG0HjHP
M0/XJ+9Suw2hltxSgtLgd2KlbC4cvm6H70xrGrdilq+DbYx7YOkdId95cS48+PoCxlnvf9XHHWnX
00VVXVnhP4RVo3OPg1t3nd39b/mCtZcZuMWxmVSZG2J/wk+HoHP3zulvDq7eAWcLJtrKbVzi1ygw
nabf7DdHtp4LlKKmlmkOjTqSQNTw46kedui7W1C/xzWcYtbHtqLvd3NgZDG7UsYSCXHTkNB19Wp6
lJ8lxCy5ZTMhu1IJTH/VyNO69mvPQhQqolwPZE2V9LD0t1ezxYg/fmIPUSfIB/8AtV0lmVl/WLLj
Nfiasmt2E4hAK2oZFSUFO1hefnEDkB1knkFVtvpLjtjyht0uDJKbGKF+kUP+KR1d5PWeocAvu3Y5
ke1W5w3jJi+hscbt6CjbqC8dw7+tx4nqVy0VFTW6iipKSFkNPE0NZGwaBoS2Yf0k5fx6xRMhiZFE
wMjYA1rWjQADkAvtEXF1EREBERAREQERad0bWPtdW237grHRObCXnQB5HAnzIKzxgfG7bDeb6/x6
K0N8Epj1F/Eaj1PPpBVsKm8cw3aTitA+jtVXZ443yGR5eN5znHtJaux4Ltd/T7L9T/au2eMt6Vyx
tk6xZiKsvBtrv6bZfqf6J0G139Lsv1f9Fjh7jXP0s1FWXQ7Xf0myn9n/AETotrv6RZfqj3Jw9w5+
kjzbBqLN6Wkhq6memdTSF7JIdNdCNCOPo9Sk1PEIKaKEOc4RtDQXczoNOKrbodrv6VZfqj3J4Ntd
P9tso/YHuV43WtxOXXelmoqyFFtddzullb/2/wDavr4K2tP536ys80Z/yKcPcXl6WWolf8DpMiyq
2XusrJ9KDQtpQBuOIOvHr56a+ZR52O7VZPKyu2MHayM/5FgYZtHlP43PGR/9On19ysx11mSW7/Fn
pqqvfszyup/4raNXkHmI6ct9okXy7YpSVYHwpk15ru0PlGh9eqnHHyvLLwnNxyvH7SHeHXmihc3m
wzAuH7I4qG3XbbjdI/obbFVXOoJ0YyFm61x6uJ+4FbtBscw2iILqCSoI/wAeZxB9A0UttlhtNmZu
223UtLw0JiiDSR3nmVfpPZ976Va+u2n514lJRtx63v4F7yWv0858Y+gBSHFdkllsEza2vc663He3
zNUN8UO7Q3U8e8klWCspfkutToTCd71YCyiLm2IiICIiAiIgIiICaIiAiIgIiICIiAiIgIiICIiA
iIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiI
CIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIg//9k=
--=_9e3eae895193ecaff22e711c256ed4e0--',
	    self::TESTOWY_MINE_Z_ZALACZNIKIEM_SIZE_W_ZALACZNIKU =>
	    'Return-Path: <pawel.kazmierczak@pastmo.pl>
Received: from m-03.smrt1.linuxpl.com (178.63.127.10) (HELO m-03.smrt1.linuxpl.com)
 by serwer1578643.home.pl (79.96.198.239) with SMTP (IdeaSmtpServer v0.80.3)
 id 168e152dd5329a87; Wed, 27 Jul 2016 21:53:00 +0200
Received: from [88.198.13.93] (helo=bdl.pl)
	by smrt1.linuxpl.com with esmtpsa (TLSv1:DHE-RSA-AES256-SHA:256)
	(Exim 4.84)
	(envelope-from <pawel.kazmierczak@pastmo.pl>)
	id 1bSUt1-001GFL-Ie
	for testowe1@grupaglasso.pl; Wed, 27 Jul 2016 21:52:59 +0200
Received: from [127.0.0.1] (helo=server.bdl.pl)
	by server.bdl.pl with esmtpa (Exim 4.87)
	(envelope-from <pawel.kazmierczak@pastmo.pl>)
	id 1bSUt1-0000Aq-FB
	for testowe1@grupaglasso.pl; Wed, 27 Jul 2016 21:52:59 +0200
MIME-Version: 1.0
Content-Type: multipart/mixed;
 boundary="=_ba510249196a44e076495b13248e2502"
Date: Wed, 27 Jul 2016 21:52:59 +0200
From: =?UTF-8?Q?Pawe=C5=82_Ka=C5=BAmierczak?= <pawel.kazmierczak@pastmo.pl>
To: testowe1@grupaglasso.pl
Subject: =?UTF-8?Q?Wersja_z_za=C5=82=C4=85cznikiem?=
Message-ID: <368878a62af1126f7944c62549120955@pastmo.pl>
X-Sender: pawel.kazmierczak@pastmo.pl
User-Agent: Roundcube Webmail/1.1.4

--=_ba510249196a44e076495b13248e2502
Content-Type: multipart/alternative;
 boundary="=_554fc098c40880c5650e873ec6318020"

--=_554fc098c40880c5650e873ec6318020
Content-Transfer-Encoding: 8bit
Content-Type: text/plain; charset=UTF-8

Logo firmy pastmo

--
Pozdrawiam
PaweĹ KaĹşmierczak
Pastmo

--=_554fc098c40880c5650e873ec6318020
Content-Type: multipart/related;
 boundary="=_f6c64d6eb37b8b44fe97fe1f214322af"

--=_f6c64d6eb37b8b44fe97fe1f214322af
Content-Transfer-Encoding: quoted-printable
Content-Type: text/html; charset=UTF-8

<html><head><meta http-equiv=3D"Content-Type" content=3D"text/html; charset=
=3DUTF-8" /></head><body style=3D\'font-size: 10pt\'>
<p>Logo firmy pastmo</p>
<div>-- <br />
<div class=3D"pre" style=3D"margin: 0; padding: 0; font-family: monospace">=
Pozdrawiam<br /> Pawe=C5=82 Ka=C5=BAmierczak<br /> Pastmo</div>
<div class=3D"pre" style=3D"margin: 0; padding: 0; font-family: monospace">=
<img src=3D"cid:324485fea2b608315b5fac25cc88d487@pastmo.pl" alt=3D"" width=
=3D"150" height=3D"93" /></div>
</div>
</body></html>

--=_f6c64d6eb37b8b44fe97fe1f214322af
Content-Transfer-Encoding: base64
Content-ID: <324485fea2b608315b5fac25cc88d487@pastmo.pl>
Content-Type: image/jpeg;
 name=324485fe.jpeg
Content-Disposition: inline;
 filename=324485fe.jpeg;
 size=12295

/9j/4AAQSkZJRgABAQEBLAEsAAD/4QAiRXhpZgAATU0AKgAAAAgAAQESAAMAAAABAAEAAAAAAAD/
2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwM
BwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM
DAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAC5ASsDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEA
AAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJx
FDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNk
ZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJ
ytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQF
BgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMz
UvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3
eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna
4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKACiiigAooooAKKKKACiiigAoooo
AKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigA
ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACi
iigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAopHbYpY8AckntXyR+05/wXR/
Zd/ZUnuLPW/ino+v63bhgdK8Lq2uXO9TgxO1uGhhcf3ZpI61o0KlWXLSi2/JXM6laFNc1RpLzPri
ivxn+JH/AAdwW/i7xP8A2D8GPgH4o8WaldAi1/tnUViuy3GMWNlHctIPYTKelZdv+3P/AMFWP2qr
Vv8AhDfgjYfDm3yQJpvDkWjzIueCw1u5O4j1WLnH3a9NZHiUr1eWC/vSS/zOH+1aDdqd5eibP2sL
YNGa/GNv+Cf/APwVS+PNuJvFH7RPh/wesmPMhh1/+zrhB7DS7AJx7SD61oWf/Bu5+1N8QrdV8cft
weN+R8yR3mtaug9h52oQjH4Cl/ZuHj/ExEfkm/0GsZVl8NGXzsv1P2MzUN9qVvpkXmXNxDbx/wB6
Vwg/M1+Ow/4NKJfEZ/4qr9pzxlr+77//ABICu7/v7ey/1rX0b/gzx+C8YX+0vij8S7ph1Ntb6bb5
+m63kpPCYFf8xF/SD/VoccRi3/y5t/28v0R+qGp/G7wXouftni7wvabevnarBHj82rCvv2v/AIS6
Z/x8/FH4dW+Ovm+JLNMfnJX53Wv/AAaC/s2wAeZ44+Nkp7j+09IVT+Wm5/WtWx/4NKP2YbTHma58
XLnH/PTW7Jc/98Wa0fV8v61Zf+A/8EJVsZ0pr/wL/gH3Pc/t9fAmy/13xq+EsOOu/wAX6ev85qoz
f8FIf2d7f/WfHr4Lx/73jfTB/wC1q+PbT/g1M/ZWth8y/Ei4/wB/xEoz/wB8witG3/4NZ/2ToR82
keOpP97xNMP5AUeyy3/n5P8A8BX+ZPtMd/JH73/kfVL/APBTj9myM/N+0J8D1+vjvS//AI/UZ/4K
gfs0j/m4j4F/+F5pX/x+vmWP/g14/ZJjHPhrxjJ7t4pux/JqkX/g2A/ZHH/MqeLG+viu+/8Ai6PZ
5b/PP/wFf5lc+N/lj97/AMj6W/4ehfsz/wDRxHwL/wDC90r/AOP0f8PQf2aD/wA3EfAv/wAL3Sv/
AI/XzYv/AAbCfsij/mT/ABU318WX/wD8cpT/AMGw37Ih/wCZO8Uj/ubL/wD+OUvZ5d/PP7l/mHPj
f5Y/e/8AI+lF/wCCnv7NLdP2hvgafp480r/4/T1/4Ka/s2t0/aC+CB+njrS//j9fMrf8GwP7Izf8
yn4sH08V33/xdMb/AINe/wBkc/8AMr+Ll+niq8/+Kp+zy3+ef3L/ADDnxv8ALH73/kfUC/8ABSv9
nJunx/8Agmfp440z/wCP1Iv/AAUi/Z3bp8e/gufp430z/wCPV8rt/wAGuv7JLf8AMt+Mh9PFN3/8
VTG/4Nb/ANkpv+YB41X6eKLn/Gj2eW/zz/8AAV/mHtMb/LH73/kfVq/8FGf2e3+78d/gy308a6b/
APHq6b4XftZ/Cv44eJZNF8FfEz4f+MNYitmvHsNE8RWeoXKQKyI0pjhkZggaRFLYwC6jOSK/Pn9o
X/g3X/Y1/Zt+BPjL4ha9ovjj+x/BOjXet3aL4onDSx28TSmNc9XfbtUdSzADk15h/wAGiP7Ko0T4
W/Ez416jYww3Xia+j8LaMTGwaK1tgJ7po2P3o5J5YY+STusT+Ok8FhHhp4ilOXu2WqSu38/mZxxW
JVeNGpFa3ejey+R+ytFFFeKeoFFFFABRRRQAUUUUAFFFFABRRRQAUUVx3x9/aA8G/su/CXWPHXj/
AMQWHhjwroMPnXt/dsdqDOFRFUF5JHYhUjQM7swVVZiAajFyfLHcUpJK7OwZwilmIVQMkntX5hf8
FHP+Dnj4T/stSah4Z+E8Nr8XfGlvuikvoLny/DemSYI+e6XJumU7TstwUI3KZo2BFfF/7Sf/AAUA
/aM/4OGPjZffB34D6HqXhP4SxyD+0YppjarLasWVbnW7uPcI4mAcrZxbw2GG25ZFKfpL/wAExv8A
ggf8H/8AgnjBp/iK/tofiN8UrcBz4l1a1XytNk4/5B9sSy22McSEvMcsPMCtsHuRwOHwcefHe9Pp
Bf8Atz6HkvFVsS+XC6R/mf6I/PDRf2N/+CgH/BcqRNU+Knia9+F3wr1AiSOx1SKXSNOlgZs/uNHi
ImutuQyPfMNyn5ZmFfaH7L//AAbJfsx/s3W+m3Hjw6p8UtflkjijfxBffYdNe45O2GygZFYMAf3c
7z9/w/SjoK/nu/4L4/GrQf20f+C0vw7+E+teKNM0P4d/Dm50zQ9W1W81KG1stLe8lhu9UuxcOypG
yWv2aM5YESWu3IPA6MLi8TjZuhSfsoJN+6tkvub+8yxGHoYWKq1FzybS959/wR+9fwq+Cng34E+G
/wCx/BHhLwz4N0gNu+w6HpcGn22fXy4VVc/hXTV4Z4V/4Kdfs4eONVhsdI+PXwc1C+uHEcNtF4x0
/wA6Zj0CJ5u5j9Aa9xilWaNXVlZWGVIOQR7V87UhUi71E7+Z7MJQatC3yHUUUVmWFFct8Y/jh4N/
Z58C3Pifx34o0Dwf4etCFl1HWL6Oztw5ztQPIQC7YO1RlmPABNfmp+1T/wAHYvwT+FV7Jpvwv8L+
J/ixqQdY4rtv+JHpMxPBCyTI1yzA8YFttbsxzmuvC4HEYh2owb/L79jnr4qjRX72SX9dj9VKM1+I
8H/BRb/gp9+3CC3wt+C8fw10eVvMtr5/Dsdk7RHBDfaNbk8mYAfxRQjPYE1eg/4JL/8ABSj9oCeO
+8dftRjwjHcg+da6d4x1GCaMHqGg0+CG3I9hIRXb/ZCh/HrQj5Xu/uRy/wBpc38KnKXysvxP2qzR
mvxim/4NU/iL48RZvGX7XXiTVLiT/WxvoN5e5/7aTank/ilSwf8ABnj4Vlw178ePFFxL1Lp4cgXP
/fUzH9aPqWBW+I/8kkH1rF30of8AkyP2YzRX482v/Bol4Z0xG+xftAfECyZhjdBpcMZH/fMgr9dP
BvhxfB3hDStIW6vL5dKs4bMXN3J5lxcCNAm+Rv4nbGSe5JNceKo0IW9jU5+/utW+86cPUrTv7WHL
87mlRRRXGdIUUUUAFFFFAH5W/wDB1l+1bc/D79kPwr8G9B8268RfGLWENxaW6eZNNp9lJFKY1A+Y
PLePZKo/jVZl55Ffd/8AwT5/ZZt/2Kv2K/ht8L4fJa48J6LFBqEkRzHcX8hM15Kv+zJdSTOB2DV+
UHwauP8Ah8V/wcoal4yVv7Q+GP7PW19PlA3QzDTZmjtCki/KTNqkk12hP3oYNv8ACK/cIcV7OYfu
MPSwnX4per2XyR5uD/e1p4jp8K9Fv97CiiivGPSCiiigAooooAKKKKACiiigAoooJxQBynxy+OHh
X9m34R+IPHXjfWrPw94V8MWjXuo39yTshQYAAUAs8jsVRI0BeR3VFDMwB/AnxH4i+NH/AAdDftwH
S9J/tDwR8C/A1yJVEy+Zb+H7Ztyi4mVTsuNUuE3BIwSsSllB2LJJL0//AAUr/ai8bf8ABfT/AIKE
aH+zf8EboSfDHwnfSST6rGTNY30kLeXda3OVIVrSAP5VuoOJWkBDE3Eax/tN+xf+xx4H/YO/Z60P
4b/D/TfsOjaSm+4uZcNeavdsB515cuAPMmkIBJwFUBUQLGiIv0VPly2kqkl++ktF/Ku/q/6638aT
eNqOC/hR3/vPt6En7IP7Hfw//YX+B2m/D/4b6HFo+h6fmWaRsSXeqXLACS6upcAyzvtGWPACqihU
RFX4N/a1/bc/4KBfFX9ojxr8NfgP+z3b+DdD8N6lNp0PjTXfLmXUoA+Ib+1mumhtNkkZVzEsd0yb
ip+ZWA/UKjGK8ijiuSbqVIqbf813r331+Z6NTD80FCEnFLtb+kfi/wCJf+CCH7Xf7YlreXn7Rn7W
SWumzqbi40zT5bzVtPjXGWza7rG0iIGQSkbKMdxXwL/wRD/4JleEf+CoH7Uvinwn4i1bxdovgXw1
4em1dL3RGtra+kla7gitY386KaNA8bzOwCk5iwD1Nf0Pf8FTvie3wb/4Ju/HPxFHM1tdWfgjVYrS
VTgx3M1s8MBH/bWRK/Nn/gzv+Gn2DwB8dPF7R/JqGo6RoULFfufZobmdwD7/AGyPI/2Vr6PC5liP
qFatdK1lFJJWb328jxMRgaP1ylSte927u9+2503xY/4NAfhXqugSR+CPi38RtG1Ir8kniC1sNYti
fQxwRWrY/wCB/n0rxH/gkR+0T8X/APgkt/wU/t/2R/itqjaj4N8QXa6VY24unubHTLqeIzWN5p7O
MrBcMRE8ICgPMWIV43Dfpv8At8f8Fp/gP/wT703ULPxJ4qt/Efje1BWPwl4fkS91QyjGFnAOy1HI
O6dkyoO0OQFP5pf8Es/g18Tv+Cy//BVr/hrnx9of/CO/DzwjqC3mmIgc21zPaoYrGwtJGAMy274m
nnA2mVGXahk2xzhcRiauFqSx+tOzs3vzdOXqy69GjTxEFhNJ31S2t1ufvBmvy9/4KUf8HE+m/Cnx
+3wh/Zr0JfjF8XL2Y6eLuyt5b/S9Nued0MMcPz39woU5SIiKMn5pGZHiH6P/ABg+F2l/HD4T+JvB
mtNfLo/izS7nR742V09rcCCeJopPLlQhkbaxwwOQa8M/4J4/8Em/g3/wTL8M3Nr8O9GurzXdQRor
/wATa28d1rV7EXDCEypHGkcK4QeVDHGhKKzBnyx8HBzw1O9SunJraPR+bf6HrYmNedoUnZdX1+S/
U/On4K/8G9Hxx/4KA+Obb4mftofFfxAl7MC0HhzT7uG61K1jYkmLzApsrBNwBMNpFIrBid0b5r6s
/bE/4Jy/Dv8AYC/4Jg/GbVP2efDFn8NfHHh/wrdanB4t0/MviOOG3AnulXUZS9yvmQRTJ8kg2eYS
mw4I/QCvP/2svBsfxF/ZX+Jnh+RfMj13wpqmnspH3hNZyxkf+PVvLNa9WpHndopr3VpHft/mYxy+
lThLlV5NPV6v7z4V/wCDXb9qTxV+0f8AsLeLLTxt4q1/xd4g8I+MrizjvdZ1CbULz7FNa2s8StNM
zOwEr3IAJ4CgDgYH6VV+I/8AwZueIZbrSf2gtNaT9zCfDd6i/wC1Kupox/KFPyr9uKM8pKnjqkY7
XX4pP9QyupKeFhKW/wDk7BRRRXlHoBRRRQAUUUUAFFFFABXyH/wXD/bpb9gj/gnt4u8Q6XetZ+M/
FA/4Rjwu6MVkhvrpHBuFI6NbwLPOCeC0KKfvCvrwnivwj/b11mf/AILpf8Fz/CnwK0Oea6+E/wAI
JZ7bXbi3ZhG0UEkbazcBlG5S8iwafG3zASBXB2yGvTynDRq1+ep8EPel6Lp89jhzCs4UuWHxS0Xq
/wDLc+yP+DZj9iNf2Vv+CeFj4u1KxFr4o+McsfiKdmTEkWmKpTTYc55Qwl7gcAg3rg9BX6KVDp2n
2+kafBaWkENra2saxQwxIEjiRRhVVRwFAAAA4AFTVyYrESr1pVpbyd/+B8jow9FUacaceiCiiiuc
2CiiigAooooAKKKKACiiigAr8wf+Dl3/AIKZ3H7Lv7Plt8F/Bd5OvxF+LVq8d29p81zpWisxilZA
PmEt04a3jwCdq3LKVdEJ/Rj4z/F3QPgD8JPEvjfxVfLpvhzwnps+ralckbjFBChd9qjlmIXAUcsS
AMkgV+J//BFn4JeIP+Cvf/BTvx5+118TLFj4b8G6ysmhWEx8yFNTVFNjaodu1l0+18mRiApaeS3k
5Jkr2MqowTli6y9ynrbu+i/zPNzCrJ2w9P4p/gurPvb/AIIUf8Etbf8A4Jufspwza/ZQj4q+PI4t
Q8UTZV209QpMGmoy8bIFY7ipIaZ5WDFNgX7goorzcRiJ16jq1N2d1GjGlBU4bIK+T/8Agpz/AMFh
fhn/AMErbfwvD440vxZr+r+Mo7qXTLDQraCRglv5QeSZ5pYkjTdMijBZjzhSAa+sK434rfs5/D34
8Xuk3HjjwH4N8Z3GgNI+mS67ottqL6aZNnmGEzIxjL+XHu2YzsXOcCjDypRqJ1k3Hqk7BWVRwapO
z8z8Bf8AgpV/wck65/wUF/Zm8YfC/wAM/Ci38K+Fde+zRanrFxrL6leRxR3UMyDZHDHFAZGiVDue
QEOQOSCPMf8Agmf+wN+2F+2l+zxfaJ8IPFV14I+DOqa3cy6ldT+JzpOn39/5MEU6yxWu+7n/AHUc
C7XjMOV4IO41+sP/AAcyaPZ+Dv8AgkDr2m6TY2um6eviDRo1trSFYYY1+1q2AigADKjjFWP+DXa0
W3/4JN6C643XHiTWZGx3IuSv8lFfWRzCnSy11sNTUfftZ+9rbfXqfOSwc6mO5K82/d6addvQ8L+E
n/Bun+zb/wAE4PhVf/Fj9pbxxJ440zwnEt5eQTWzab4fgYsgSP7LEXuLyQykIsZkKzFwvkEttq/4
N/4O1PgH4a8Zad4X0/4U+PNB8B6ekdja3VtFYRvp8CKETbYRy4WFFA+VJCwQcITha5b/AIOa/F+t
/tN/ti/s3/staJfTafH4rv7fVr1gm+MTX14dOtLhl7i3SO/cj0kPoK/RH4qf8Et/g/4+/YQvvgDp
/hDQ9G8JLpLWWkvFZoZ9LvBEVh1FZMbmulkxI0rEtIxfeWDsDx1K1OVKnWzBubneyTsora9l1Oun
Tmpzp4NKKjvpdt72Pb/hZ8UvD/xt+HGieLvCerWmueG/ElnHqGm39sxaK6gkUMjjOCODyCAQcggE
EV8w/wDBQb/gt78Cf+CcPiT/AIRzxhq2r+IvGgiSeTw34btUvL+0jcbkedpJI4YMqVYLJIsjKwZU
ZTmvlD/g0s/aC1jxH+zT8TfhDrzXH2r4V6/FdWkEz/8AHjBqHneZbIPRbu1u5D/tXB9RXy7/AMET
PD3wk+MP/BUP45L+1VY+G9S+MF5rU39iaV4tSOSwn1M3t0uoxLFOPJkuUb7MsEZBZUEhjX5CVxhl
NOnWrKteUadnZbtPb5W1ZpLMJzp03TsnPq9lbf8A4B9r/Af/AIOtf2b/AIseM7bR/EWl+Pvh1Ddy
eWuraxY29xpsJPC+a9rNLJHk9WMexerMoBI/RnxhqVprHww1S8guIbqxutLlmjnhkDxzRtESGVhw
VKnII4INeG/tEf8ABIj9m39p/wAI3Gk+Jvg74HhaZNqajo2mx6RqdscHBjurYRygA87SxQ4+ZWHF
d58eLfSf2e/2JPGUWk26adofgfwPfJZQB2Zba2tLCQRpuYkkKkYGSSeOTXn4iWFm4/VouLvqm7ry
szso/WIqXt2multD8gf+DNYMfEvx+PO0aX4aDfXfqmP61+nH/BSr/gq78L/+CXngPTdS8cyanquu
+IPMGjeH9JRJL7UPLxvlO9lSKFCyBpHb+LCh2+Wvzi/4M3NCktdG/aD1Bl/cznw3Zo3+1Eupsw/K
ZfzrT+BvgXSP+Co//By98XNa8bWMfiLwX8BbGXTNK0y+iW4tFubGaOzjidGGGT7XJqN0AQcSKnUC
vazDD06uZVp1vggk3brokl82eZg68oYOnGn8Um0vLV6n29/wSr/4LTfDn/gqnBrum6DpmqeEfGfh
uFby80HU5opnntGfYLq3ljOJY1Yqj5VWRnQEYdGbwr9pD/g6g+B/wB/aU1DwLY+GfGHjbR9BvH07
U/EejvbC3NxGdsotI5ZFNyiMGUybkVipKeYhV2+X/wDgthZP/wAEqf8AgrHofxo+Hdr/AMIrbfFL
4fa/bSrpwFvFNrIsLm180KoCqBNNpE7ADLSxFzlmJr6L/wCDeP8A4JsfDvWP+CSj3vjbwhpGvXHx
wa9k1b7faK039nRzSWlrAkmNyoFhNwhUgq9wWBBAIzng8FTpfXJRbhK1o31Td76+VtPxLjisVOp9
Wi0pxvd23WltPO5+knwL+OfhP9pb4R6D488Da3Z+IvCfia1F5p2oW2QsyElSCrAMkiMrI8bgOjoy
sqspAw/2q/2t/h7+xP8AB698d/EzxJZ+GfDtnItussoaSa8ncEpbwRIDJNMwViERSdqOxwqsw/M3
/g1n+IGsfC7X/wBov9nXXL5rpvhf4oNzYiQndv8APuLG9Cj+FBJZwPtHG6dzjJJPAePPA8n/AAXq
/wCC8XiTwf4kuL2b4Dfs3/aLO4sIZXhjv5YJlgni3qTte7vUdWkXaWtbHCsj4euP+y4RxM4VJfu4
K7fVp2tbzd7HR9elKhGUV78na3n1+SPZvCn/AAdzfs7698Ro9KvvCPxS0XQ5pRF/bE9jZzC3GQDJ
Lbw3LyiMDJ/diR8Y+TPFfpr8MPih4e+NPw+0fxX4T1jT/EHhzX7ZbzT9RspRLBdxN0ZWH5EHkEEE
AgiuO+LH7F/wr+NX7PM3wp17wH4Zm8ANaGzt9Gt9PitrfTV2lUe1WNR9nkTOUeLayEAqQRX5tf8A
BuL431/9mD9qP9or9kPxFqlxqtp8N9Vn1nw48y7W8hbn7PcyAZOxJllsJxGvyq80x6uTU1KOGr0Z
VcNFxcNWm73W1/VdS41a1KpGFZpqWzStZ9j9dKKKxfiR8RtD+EPgDWvFXibU7XRfDvh2yl1HUr+5
bbFaW8SF5JGPoFBPHJ7ZNeOk27I9HbVnyP8A8F1P+CkUf/BOr9inUrrRr+O3+JPjwS6J4SQFTJbS
lR9ov9p/htYm3g4ZfOe3RhiTNeS/8Gy//BO5v2UP2PJPiZ4ksWh8c/GJIdQQTrmbT9GUE2cWTkq0
29rl8EEiWFXG6Kvif9n3wtrv/ByR/wAFedQ8e+KbG8tvgP8ADExFdNuVCqNOSVmtNNcLkfaL2QPN
ccnbEJUEg2wV/QGqhFCgYA4AHavdxn+x4ZYNfHKzn5do/Ldnk4X/AGmu8S/hjpH9WLRRRXhHrBRR
RQAUUUUAFFFFABRRRQAUUVn+K/FWneBfC2pa3rF5b6dpOj2st9fXc7bYrWCJC8kjHsqqpJPoKN9E
B+R//B0p+1vrXiKx+Hf7K3gFJtS8VfE6/tNR1ext2xJdRG5EOm2XPy/v7xTIeQV+xpn5Xr9G/wDg
n7+x3o37BX7Ifgr4XaMYbg+HbEf2leohX+09QkJkurk55xJMzlQSdqbEHCivyk/4IheDr/8A4Km/
8Fcfi1+1x4rsp18P+FL1k8NW1yv+pup4jb2UPAKM1ppqDeBg+dcwyDnNfuBXtZo/YUoYGP2dZf4n
/ktDy8Cvazli310j/hX+b1CiiivFPUCiiigD4G/4OZdBbV/+CPvxAuQuf7J1TQ7o+wOq2sP/ALVr
mP8Ag1g1pdT/AOCVtvbr97TPF2rW7+xLRTfylFe6f8Fxfhu3xU/4JLfHbTVTzDY+GZNbx/2D5I7/
AD+H2bP4V8j/APBof45h1L9iT4neHN+660fx49+Vz92G506yVP8Ax+2lNe9T97J5r+Waf3pI8ipp
mUX3i/zPnH/gsb40+J2k/wDBxR4P1L4R+Hbbxl8SPCvhqwuPD+jXUH2iG8MNtf3MqmPzIywWJp3A
V1bK/Kd2AfVLD/gp9/wVA+JbtpGj/su+H9Hv5x5YvLjwnf2SxMf4lkvNQWBfq+5a6D9p61TwL/wd
v/AfULj5Y9c8IeYp/vGTTdetF/8AH4wK/X4Lg1visbTpUqMZUoy9xau/d9mZ4fCznUqSVRx956K3
ZH4U/wDBsXqPirwt/wAFRf2jvDHjSGO08XSadeTeI7eIxeVFqdtrHl3Cr5RMWFmuJgPLJTH3TjFf
f3/BS/8A4IQfBj/gpTrU3ijVI9Q8E/ESSBYH8R6Ise7UQibIxe27gx3IRQqhvkl2oieaEVVHxV/w
Q1txc/8ABf8A/bO1BTvjtNQ8UwttG45fxMCOBz/yyNd34H/4O+/g+xvIPGnwm+KnhvULWeSIWtg2
n37IFYgCYTT2zRyYA3JtbacjccZOuOp4ueNdbBp3UY3t5rt2MsJKhDDezxL0vK1/J/meJ/Gj4R/t
nf8ABu7oWl+NvDvxXj+MHwPsbyHT7zS9SFw1pZI7bY45bSZ5WsY5GYKktpOQJCodcMFf9BP2+v2y
NF+Lf/BBj4ifFzR0ks9J+IXw1kFtBOQZbR9UhW0EDkcGSOS58skcblOMjFfnX+3J/wAFSfid/wAF
/dNtvgH+zf8ACbxNb+DtQ1O3udf1fVWQtKsTiWFbx4t9tY20ciCViZZJJWjiVACCkvvH/Bd/wrbf
sB/8EFvh38DbDUF1Bry/0PwpPc7fLa++yI9/Pc7Mkqrz2aHGTt81RmqqUZVKlBYlJVXLW1k+XTWV
tLihVUIVXRbdNR0vtfyv0Nv/AINGvAQ0X9hD4heIpI2jm17x5NbxsRxJBbWFmFI+kksw/CuM/wCD
bq8/4SD/AIKN/tuapcfNfXXiTzSx6nzdX1d3/NgtfYH/AAb7/B6T4Nf8EjfhDbXMCw3niKyufEsx
xgypfXc1zAx/7dpIBnuFFfFf/Bv5qKeB/wDgtF+2d4N3fNJq2rzoP7y2fiC4izj6XS/nWVap7V42
S8vuUkaU4+zjhov+ro+4v+CwP/BJDSf+CsXwz8I6TN4uk8C694M1KW8sdXXShqamCeMJPbtD50PD
lIGDB+DCOCCa+jf2dPgjpf7NPwB8FfDzRHml0nwPodnodpLMFEs8dvCkQkfaAN77dzEDlmJrs6K+
fliasqSot+7G7S9T2I0YKo6qXvPdn4n/APBEG7ki/wCDh79sS3jJFrNdeK5JB2Lr4nhC/wDocn51
vf8ABqDc/wDFy/2sYdSLDxQ2v6Y+oiQ/O377Vgc+4l87PuR61mf8G2kEfxk/4KZftifFK3ZJLW61
SfyZVORIup6xfXYIPpi1B/EVxv7Ysfjj/g38/wCCwOr/AB70Xw7feIPgf8arqd9UigbEbS3cguLy
yLkbIbuO5V7m33hVkiZolbAnKfV4iPtatbBr45QhbzcUnb5o+eofu4U8S/hUpX9JaXP3XPSvyM/4
JjOfip/wcs/te+NNJj8zQ9B0afw3dzKPljvBc6Zb7T7l9Lu/++DXR/tNf8HQvwt1H4NR6b8A9L8Y
+PfjF4uiGn6Bosnh+eL+zb6bCxmZSD9pdWbKw23m+ayhNyK28e6f8EMv+CcWqf8ABO79lHUL3x7L
HP8AFf4l3v8AwkXjC5kuBcSWjbSYbN5gdsjRB5XkcEgzXE+13Tax8enRnhMPUlWXLKa5Unvum3bs
rfienKtHEVoKk7qOrfTbRfifbhNfg5/wWb/4KI+J/wDgrR+07on7Jf7Oc39veGZtVS31fUbWUrZ+
Jr6F97MZFBzptlsMrS4KySRb0DLFC8nR/wDBZD/gttr37Y3jr/hmP9lT7f4m/wCEmuW0XVvEGiNv
m8SOciSx06QEAWu0N513uCOivtYQhpX+4v8Agit/wR40H/gl/wDB6S/1b+z9c+Lviy2RfEOsxLui
sIgQy6dZkgFbdGALtgNPIodgFSKOLfDUY4CmsViF+8fwRfT+8/0/q2VarLGT9hRfuL4n+i/U9u/4
J3/sKeFv+CdP7LGg/DPwu32z7Fm81jVXhEU2uajIF8+7kUE43bVVFLNsijiTcwQGvcKKK8GpUlUk
5zd29z1qcIwioR2QUUUVBQUUUUAFFFFABRRRQAUUUUAFfm//AMHP/wC2b/wzT/wTuuPBem3v2bxF
8Yrs6CoSTbMmlxqJdQkA/iRk8q2Yel7X6QV+Jf7SEY/4Ky/8HLvhb4fq66j8O/2fIlm1NEIlt3ax
ZLq8LI3H7zUJbSxlHdYc8459TJ6cXX9rU+GCcn8tl99jgzGpJUuSHxSdl89/wP0U/wCCN/7Fv/DB
/wDwT08A+C7yz+x+KL61/t7xOGQLKdTuwJJY3xwTAvl2wPdbZK+oKKK8+tWlVqOpPdu5106apwUI
7LQKKKKzNAooooA534vfDex+Mvwm8UeD9UGdN8V6Td6PdjGcw3ELwvx/uua/Db/g0k+Jd18LP2s/
jR8J9ajNnqusaHb38kMrYMV1pV3JbTxAd3P27JA5xCT0U1+9Z5FfnN8Gf+CDF18Ff+Cv2qftMaR8
TlsfD99rWpa7/wAIxb6OftE8uoW8yXEElw0xTyjPPJKMRE4CqApAcevl+KpRw1fD1XbmSa9Vsvme
di8POVelWpq/K3f0Z9l+Nv2NPhf8SP2jfDPxc17wZpGqfEbwbaGx0bXJw7T2MJMpCqu7Y20zTFSy
koZGKkEk16dRRXlSnKVlJ7bHfGKWyOf8LfCXwr4G8Ua5rmi+GvD+j614mlSbWL+x06G3utVkXIV7
iVFDTMATguSRk1W8bfA7wV8S9QS78SeD/C/iC6jACTalpUF1IgHQBpEJFdTRRzyve4cqtaxV0XQ7
Lw3pcNjp1na2FjbLsht7aJYooh6KqgAD6Cvws/4OpviHqH7Rn7bPwP8AgD4Y23GsW9srpGj5WTUd
Zu47S1icdmRbYMOM7brPev3dNfnX4X/4IY6ve/8ABZHUf2o/G3xI0/xVpaapJrOlaD/YrQTwyra/
ZbOGSTzSmy1QRsrqu53hRiFy2fUyfEUqFZ16r1inbzeyOHMaM6tJUYLRtX8kfe3wt+HOm/B/4ZeH
fCWixeRo/hfS7bSLCP8A55wW8SxRj8FQCsX4ffszfDv4T/EfxN4w8L+BvCfh/wAVeMpfO13V9P0u
G3vdWcncWmlVQz5bLHJ5YljliSe4ory+eWuu+/md3KtNNgoozVHxH4m03wdolxqWr6hZaXptmu+e
6vJ1gghX1Z2IVR7k1JRz3ww/Z+8B/BLUtevPBvgvwr4Tu/FV39v1mbRtJgsZNWuOf305iVTK/wAz
Hc2Tl2PVjnb8beB9F+JXhS+0HxHo+l6/oeqRGG907UrSO6tbuM9UkikBR14HDAjivj79pj/g4O/Z
V/ZnhuIn+JFr491eFdy6b4Li/tp5vUC4Qi0Vh0xJOp/I4+AviX/wcTftMf8ABQTxTdeC/wBk/wCD
+paP8wil1SKz/t7VrcM2ElkYoLGwU9CZ/NUHkSCvVoZTjK37xrlX80tF+P6Hn1sww1P3L3fZav8A
A/SD4t3/AOyH/wAEefDk/jvUPC/wp+FOoXkcqWh0Xw/a2+t6v93dBbRwR+fKuSoKr+7TILFFyw/K
P9pf/go5+0h/wcFfFW6+DfwD8K6p4T+F7yBNUjafyWntnJCz63epuSGAqrEWkRbf86/6UwQL6z+y
n/wbF/ED9of4gj4j/td/EjVtR1XUCkt1olhqz6lq14BjbFd6lJuWJVwU8q2DgKRsmjwBX7A/AD9n
XwN+yx8MbHwb8O/C+j+EfDOn8xWOnweWrvgAyyNy0srbRulkLO5GWYnmu14jC4N80H7Wr3fwr07/
ANbHN7HEYnSS9nDsvif+R82/8Emv+CNPw/8A+CXfgeS6tmi8WfE/WoBDrXiq4twjiPIY2lmhJ+z2
wYAkAl5WVWkZgsax/Y1FFeHXxFStN1Kru2erRowpQUKaskFFFFYmgUUUUAFFFFABRRRQAUUUUAFF
FFAHnv7WP7Qul/snfsy+PPiVrHltp/gjQ7rV3iaQR/aniiZo4FJ43SybI19WcCv58/8AgiH/AMFg
Pg5/wT11r4q+OPi3a/EDxZ8TfibqMbzX2jafZXMUduGe4mcyzXUT+bPdTyM6hSuIITkkkL/SHrmg
2PifS5bHUrK01Cynx5lvcwrNFJghhlWBBwQDyOoFc8fgJ4Fbr4L8Jn/uEW//AMRXqYLHUaVGdGrB
y5rXs7aLps+pwYrC1KlSNSErct91fc/N9f8Ag7q/Zlb/AJlb4yj/ALhGm/8AyfTx/wAHc37Mp/5l
n4xL9dH07/5Or9GW/Z88Av18D+Dz9dGt/wD4imN+zl8PX+94D8GH66Jbf/EVX1jL/wDn1L/wL/gC
9jjP+fi/8B/4J+di/wDB3F+zGevh74wL9dGsP/k6np/wdu/swt/zAvi8P+4LY/8AybX6Gt+zT8OX
+98P/BJ+uh2v/wARUbfsv/DRuvw78Cn66Da//G6Pb5f/AM+pf+Bf8AXscb/z8X/gP/BPz5X/AIO1
/wBl9v8AmDfFtfroln/8mU4/8Han7L2P+QR8Wj/3A7T/AOS6/QFv2Vvhe3X4b+Aj9fD9p/8AG6Yf
2TfhWT/yTP4f/wDhO2n/AMbo9tl//PqX/gX/AABexxv/AD8X/gP/AAT8/wBv+Dtj9mAf8wT4uH/u
CWX/AMmVG3/B29+zEv8AzAPi8300Wx/+Ta/QZf2U/hcn3fht4BH08PWn/wAbp6/sufDJPu/DnwIP
poFp/wDG6Pb5d/z6l/4F/wAAfscb/wA/F/4D/wAE/O+T/g7p/ZlQ/L4X+Mr/AE0jTf634qtL/wAH
e/7NMfTwX8bH+ml6SP56kK/SCH9m34d2/wDq/APguP8A3dDth/7JVu2+BvgmzP7nwf4Wi/3NJgX+
SUfWMv8A+fUv/Av+AP2OM/5+L/wH/gn5lXf/AAeBfs5on7nwF8aJG9JLHR4x+momsbUf+Dw74Kpn
7F8L/ibcennT6bF/6DcPX6wWXgDQdNP+j6JpFvj/AJ52ca/yFakNvHbptjjWNfRRtFL6zgV/y5f/
AIH/AMAr2OKf/Lxf+A/8E/HiT/g7w8O6u2NB/Z+8c6tn7udYhXP/AH7ikqS0/wCDor4meJn26B+x
f481Xd9xo9bvZCf+Ax6S2fzr9hsUYxT+uYHph/8AyeX/AACFhsVfWt/5Kv8Agn456n/wXn/bS8Wx
58L/ALDnjKzWTiOS90DXr2MfVltYB+oqsv7d/wDwVY+NNv5Ph/8AZ38K+FfMB2yvoy2EqD3Opals
z9UH0r9lcUAYo/tKhH4MPH53f6h9Sqt+9Wl8rL9D8WZP2T/+CtH7TFk0HiX4s6L8OraQH93/AGzY
6XJED2EmkWksn0zIfrTdJ/4NUPH3x01u11n47/tMaz4pv42zLHDaXOsXJB67L2+uCVP1gP8ASv2o
oqv7cxEf4KjD/DFL/MX9l0X/ABHKXq2fA/7Ov/BtZ+yn8ApoLq+8I6v8SNSt3Dx3PjDU2vI/91rW
FYbV19nhbp9c/cHgT4faD8LfCtroXhnQ9H8O6JYrsttP0uzjs7W3X0SKMBFHsAK2KK8+viq1Z3qy
cvVnZRw9KkrU4pegUUUVzmwUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUA
FFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAU
UUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRR
RQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFF
ABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAH//2Q==
--=_f6c64d6eb37b8b44fe97fe1f214322af--

--=_554fc098c40880c5650e873ec6318020--

--=_ba510249196a44e076495b13248e2502
Content-Transfer-Encoding: base64
Content-Type: image/jpeg;
 name=logo.jpg
Content-Disposition: attachment;
 filename=logo.jpg;
 size=4313

/9j/4AAQSkZJRgABAQEBLAEsAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0a
HBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIy
MjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCACUAPADASIA
AhEBAxEB/8QAHAABAAIDAQEBAAAAAAAAAAAAAAYHAQQFAwII/8QATBAAAQMDAQQECAgKCAcAAAAA
AQACAwQFBhEHEiExE0FRYRQiMnGBkaHRFRYXQlWSlMEIIyRFVHKCk7HCJTNEUlPS4eI0NnODhKLw
/8QAGAEBAQEBAQAAAAAAAAAAAAAAAAECAwT/xAAiEQEBAAIBBAMBAQEAAAAAAAAAAQIREiExUWED
IkFxEzL/2gAMAwEAAhEDEQA/AL/REQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBER
AREQEREBERAREQEREBFhZQEREBEWC4NGpIA7Sgyi1n19HH5dXA39aQBeDr5aWeXdKJvnqGD71dVN
x0EXLOS2JvO9W4eeqZ718HKseHO/Wwf+XH701TcddFx/jXjv0/avtkfvT41459P2v7ZH701TcdhF
yPjXjn0/a/tkfvT41Y79PWv7ZH701TcddFyfjRj/ANO2z7XH71n4z2D6ctv2uP3pqm46qLlfGaw/
Tdu+1M966o5KaXYiIgIiICIiAiIgIixqgysOc1rS5xAAGpJPJQzNdpFpw6MwO/LLk4eJSRu4jsLj
80e3uUJhxjN9pbhVZHXOtFnf4zaSNpDnDqG597tfMumPx7m70jFz66iY37aziliL4/DfDZ2846Qb
/H9byfao63aTmeQf8t4a9kDvJqKskhw7R5I9pXddjWI7NMfnvItgmdStB6WUB8rnEgAAngCSerTm
pTjl6jyPH6O7wwSwR1TC9scum8BqR1ebXzFX6ybk2n2t1ar5tm2tXYa1N8oLY3XyYgCQPQ0/xXoN
luR1Tg+47Qbm8nm2Bjo/bv8A3K0kU/0v4vCfqsDsUt83GryO+VB69+caH2L6j2GYk06yOrpT1l02
n8ArNWNVP9c/Jwx8K7bsSwtv9kqj56hy9m7GcLb+b5T553Lv3PN8Zs7nMrb3RxyNOhY2TfcD2EN1
IUaqttmHU5cGVFVMR/cgOh9J0WpfkvbaWYTu227IMLb+ayfPM73r7GyXCx+aB6ZXe9cMbdbDIfxF
qu0w7WRNP8y9G7bbSedhvbR29A0/err5fab+N2vkmwv6Hb+8d71n5J8L+hmfvHe9SugrGV9BT1kb
HsZPGJGtkGjgCNdCO1bKxzy8t8cfCFfJPhf0Mz9473rHyS4X9Dt/eO96myJzy8nDHwhHySYX9ED9
673p8keF/RI/eu96m68aupio6OaqneGQwsdI9x5BoGpPqCc8vJxx8KPvGGWE7WLJj1moRFFA0VVc
d4u3gDvBp17gPrq9hyVV7JaeW9XG/ZnVNIfX1Bipw75sYOp/lH7JVqK/LbuS/jPxzpsREXN0EREB
ERAREQFWu0LaFPbKpmOY5H4TfaghhLBr0GvLh1u/hzK6W0rN/ilZ2wUY6S71v4umjHEt6t8ju5Dt
K1dmuBfF2ldeLprNfa0F8r38TEHcS3X+8es9q6YyScsmMrbeMeGE7N6TGo3X7IJRWXggzyyy+M2A
8yRrzd2u9SVu2O0OnfTWC2197qW/NpoyG+k6E6d+isggEEEag8wV401JTUUIhpaeKCIcmRMDWj0B
Tnu7y6nHU1H572i5Zld/p6Oz3WyMtUFTM2WCHeJkkI8UanXlq7sHFSKni2v2O208FPT0b6amjbHH
CwRuIa0aAdvILzzAm8berHbz40dN0RLezm8+wBWlkGV2XGKYzXSujhOmrYtdZH+ZvMrtllqYyRzk
3bbUawDaLJlFXUWe60XgN5pQS+MagPAOh0B4gjUajj2qfOeGNLnEBoGpJPABU5gkdXmW06szdtI6
ktrGOih3ucp3dwcevhqT2HQdSn+ZYrNl1BBQfC89DSiTeqY4mA9Oz+6TqNPaO4rlnjJlrs3hbxRq
+7V2vuDrPiFuferly6RmvQs7+Hlewd64V0wvMr3aau6ZbfZWU8MLpTbKIgb4A13dfJHLrDladgx2
141b20drpGQRjynAauee1x5krdro+moKmI/Pic31hWZyX6w4291T7MMTw3Isd+E/gLSZkzoXsqKg
zaEAHXk0cQR1K0KOx2m3aeBWyjp9OuKBrT7Aqx2DPcy2XykJ8WGqbp6QR/KreT5bZlZs+OTjKaJo
iLk6GiIiAiIgKttsN7lp7BTY/Raur7zKIWMaeO5qNfWSB6VY73NYxz3EBrRqSeoKocRDs+2n1+Vy
Autts/J6LUcC7jpp6CXftBdPjnXlfxjPtryszHLNFj+PUVqhILaaIMLgPKd1n0nVdRYCyufdudBE
RAREQEREBalyuNNabZU19W8Mp6eMyPd3BbaqXajX1GR3+14HbHkPqJBNWPb81o4gegau9AWsMeV0
zldTbX2fWqpzbLKvO7zGegZIWW+B3EN05H9kcO8knqVxdS1LXbqa0Wumt9HGGU9PGI2NHYB/FbaZ
5cqY46jCrnJM6yqkvtVabDh89V0JAbVy73RyagHUAADr08rqVjrGimNkvWbWy3s/MdNRZRlW1Koh
kqWWy+uDjLI0lohAjAIG7qfJOnPr5qev2ZYxhttqMgymtqLtJD47+kG62Rx4AbupLiT2krSxsbv4
Rl5DuZbLp9VhXV20TMnfjdpnmZFS1NbvzuedGho0GpPZo5y9WWVuUxnSacMZJLa8KXa3cKGCjrKz
D5KHHJSI4ahjz4jeogboBGnVw7iVZdzv1vtNgkvVTOBQsjEm+3jvA6aadpJIA86jGZ3bF63BLlbR
ebYWOpS2JjahjjvNGrdAD1EBRS32itzrYHRUFLIPDaV5DGuOgf0b3AN+qRp3gLlccbJe3VuWzp3b
fyw3ipaau34NX1Ft18WfpHavb28GEe0+dT3GcmpMsxxt1pI5Io3bzHxyjRzHN4EFQew7WaG2RU9m
yi21NnrYGNiJdGTG7ThqOsD0Ed5ViSV1JPYJq6imilpnQPlZJGQWu4E66+dTOa6a0uN9qx2GcX5M
4cvCm/zro5JtYqLdeK2jslikucFt411SHkNZpzA0B5dp7DwXJ2KudT4hkVz5b0xcD3tYT/Muzsdo
GVGz2oqZmgvudTM+Ukcxru6ew+tbzk5XKs471JGzftqMFJaLPNZaB9xr7u3epqYO03RyO9px1B4a
dx7F1cJzdmVx1VPU0T7fdaJ27U0j3a7vYQdBw9HD2qs9lFkqG7Q6qOqjduWKGWnjDh5JdI4j0+M7
1qTVLxbPwh6YR8BcraOkHa4bwB9UYUywxn1hMr3qU5jnVDiMcELoJK25VR0p6KHy5DyHboNeHI+Z
cGh2oVlNdqWhyrG57IyrcGwVDpd9mp5B3Aaef2BamDwtyTaZk2R1X4x1FKKSkDhr0Y4jh2cB/wCx
Uk2n22G47PLwJWjep4DURu62uZ43D0Aj0rOsZZjY1u2col4WVH8HrZbhg9mqpjrJJSsLj28NPuWx
k2RUWL2Ooulc7RkbfEYOcj+po7yuervTe+m0M2t5LNS2+DF7Zq+6XciLdYeLYyd0/WPDzaqXYfjk
OK4zSWqLQvjbvSvHz5DxcfX7FAdmNirL9eKnPb63WoqXEUcZ+Y3lvDu08Uek9attbz+s4RjHreVE
RFzdBERAREQEREGpc7hBabXU3CqduwU8bpHnuAVa7JLZPc6q7ZtcW/lVylcyHX5kYPHTu1AHmavb
bDcZ6mkteKUDvyu71DQ7T5sYPWO86ehpVhWi2wWe0Ulupm7sNNE2JvmA5+c810/5w/rHfL+N1ERc
2xERBSkLTRfhLyb3BtS0keY0/vauptTt8FxzLDaWsZv0tRUmKRuumoLm6j2rm7R/6E2u43ezqI5O
ja8/qu0I9TlZl9xehyGttVZVPla+2z9PD0btATw4Hu4Bd7lrjl6cZN7ntGavZRh9Bbq2pjthdIyB
7m9JK5wBDTodNVy9kV3oLLszZU3OripYHV742ySu0BcQNB7CrTmhZUQSQyDejkaWOHaCNCo/FguO
x44ywG3NktzJDK2ORxJDzr42vPXisc9zWTXDV3GnkuU4VJYqgXO422tpyw/iWSslc46cNANSD39S
gGJz1No2G5FWyiSKmmMpomv5gP0YCO4uP8e1T2j2WYdRVAmZZo5HA6gSuc8D0E6KO7b7gKXEaOzU
4DX1tQ1oY3gNxnHTT9bd9S3hZbMYmUv/AFX1s0tzqfYxOQCH1jKmXT1sHsYPWt/YpKJNmlG0HjHP
M0/XJ+9Suw2hltxSgtLgd2KlbC4cvm6H70xrGrdilq+DbYx7YOkdId95cS48+PoCxlnvf9XHHWnX
00VVXVnhP4RVo3OPg1t3nd39b/mCtZcZuMWxmVSZG2J/wk+HoHP3zulvDq7eAWcLJtrKbVzi1ygw
nabf7DdHtp4LlKKmlmkOjTqSQNTw46kedui7W1C/xzWcYtbHtqLvd3NgZDG7UsYSCXHTkNB19Wp6
lJ8lxCy5ZTMhu1IJTH/VyNO69mvPQhQqolwPZE2V9LD0t1ezxYg/fmIPUSfIB/8AtV0lmVl/WLLj
Nfiasmt2E4hAK2oZFSUFO1hefnEDkB1knkFVtvpLjtjyht0uDJKbGKF+kUP+KR1d5PWeocAvu3Y5
ke1W5w3jJi+hscbt6CjbqC8dw7+tx4nqVy0VFTW6iipKSFkNPE0NZGwaBoS2Yf0k5fx6xRMhiZFE
wMjYA1rWjQADkAvtEXF1EREBERAREQERad0bWPtdW237grHRObCXnQB5HAnzIKzxgfG7bDeb6/x6
K0N8Epj1F/Eaj1PPpBVsKm8cw3aTitA+jtVXZ443yGR5eN5znHtJaux4Ltd/T7L9T/au2eMt6Vyx
tk6xZiKsvBtrv6bZfqf6J0G139Lsv1f9Fjh7jXP0s1FWXQ7Xf0myn9n/AETotrv6RZfqj3Jw9w5+
kjzbBqLN6Wkhq6memdTSF7JIdNdCNCOPo9Sk1PEIKaKEOc4RtDQXczoNOKrbodrv6VZfqj3J4Ntd
P9tso/YHuV43WtxOXXelmoqyFFtddzullb/2/wDavr4K2tP536ys80Z/yKcPcXl6WWolf8DpMiyq
2XusrJ9KDQtpQBuOIOvHr56a+ZR52O7VZPKyu2MHayM/5FgYZtHlP43PGR/9On19ysx11mSW7/Fn
pqqvfszyup/4raNXkHmI6ct9okXy7YpSVYHwpk15ru0PlGh9eqnHHyvLLwnNxyvH7SHeHXmihc3m
wzAuH7I4qG3XbbjdI/obbFVXOoJ0YyFm61x6uJ+4FbtBscw2iILqCSoI/wAeZxB9A0UttlhtNmZu
223UtLw0JiiDSR3nmVfpPZ976Va+u2n514lJRtx63v4F7yWv0858Y+gBSHFdkllsEza2vc663He3
zNUN8UO7Q3U8e8klWCspfkutToTCd71YCyiLm2IiICIiAiIgIiICaIiAiIgIiICIiAiIgIiICIiA
iIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiI
CIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIg//9k=
--=_ba510249196a44e076495b13248e2502--

',
	    self::TESTOWY_OD_GRZEGORZA_1 => 'Return-Path: <pawel.kazmierczak@pastmo.pl>
Received: from amc124.rev.netart.pl (85.128.185.124) (HELO amc124.rev.netart.pl)
 by serwer1578643.home.pl (79.96.198.239) with SMTP (IdeaSmtpServer 0.81.1)
 id 7a95000de17fe16d; Fri, 9 Dec 2016 15:31:52 +0100
Received: from poczta.nazwa.pl (unknown [10.252.0.18])
	by monisia6il.nazwa.pl (Postfix) with ESMTP id 7CB7D2D7618
	for <email@serwer.pl>; Fri,  9 Dec 2016 15:31:51 +0100 (CET)
Date: Fri, 9 Dec 2016 15:31:51 +0100 (CET)
From: =?UTF-8?Q?Pawe=C5=82_Ka=C5=BAmierczak?= <pawel.kazmierczak@pastmo.pl>
To: "Najlepszy Pracownik @ SUPERFIRMA.PL" <email@serwer.pl>
Message-ID: <2051905821.541994.1481293911404@poczta.nazwa.pl>
In-Reply-To: <0dc701d24ef3$32cb2800$98617800$@grupaglasso.pl>
References: <0dc701d24ef3$32cb2800$98617800$@grupaglasso.pl>
Subject: Re: zrodlo e-mail
MIME-Version: 1.0
Content-Type: multipart/alternative;
	boundary="----=_Part_541993_1216384135.1481293911367"
X-Priority: 3
Importance: Medium
X-Mailer: Open-Xchange Mailer v7.8.2-Rev13
X-Originating-Client: com.openexchange.ox.gui.dhtml

------=_Part_541993_1216384135.1481293911367
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: quoted-printable

A mo=C5=BCesz to do zwyk=C5=82ego pliku tekstowego wrzuci=C4=87? Bo mi si=
=C4=99 znaki nowej linii
pododawa=C5=82y...

> Dnia 5 grudzie=C5=84 2016 o 13:29 "Najlepszy Pracownik @ SUPERFIRMA.PL"
> <email@serwer.pl> napisa=C5=82(a):
>=20
>=20
>  to jest jaki=C5=9B inne e-mail, tamtych ju=C5=BC nie mam na serwerze.
>=20
>  =20
>=20
>  Pozdrawiam,
>=20
>  Najlepszy Pracownik
>         SALES MANAGER
>=20
>         tel. kom.: +48 502 268 786
>  e-mail: email@serwer.pl mailto:email@serwer.pl
>=20
>  GLASSO Pawe=C5=82 Szczerba | tel.: +48 71 344 44 17
>  ul. Kazimierza Wielkiego 67 | 50-077 Wroc=C5=82aw
>  NIP: 911-170-84-74 | REGON: 020923887
>=20
>=20
>  =20
>=20
>  [glasso] http://grupaglasso.pl/
>  www.grupaglasso.pl http://grupaglasso.pl/
>  www.glasso.pl http://glasso.pl/
>  www.metaleo.pl http://metaleo.pl/
>  www.woodeo.pl http://woodeo.pl/
>  www.figro.pl http://figro.pl/
>  www.statuetkiszklane.org http://statuetkiszklane.org/
>  www.karykatura3d.pl http://karykatura3d.pl/
>=20
>  [spragia] http://spragia.pl/
>  www.spragia.pl http://spragia.pl/
>  www.ekskluzywnewizytowki.pl http://ekskluzywnewizytowki.pl/
>=20
>  =20
>=20
>  Tre=C5=9B=C4=87 tej wiadomo=C5=9Bci zawiera informacje przeznaczone tylk=
o dla adresata.
> Je=C5=BCeli nie jeste=C5=9Bcie Pa=C5=84stwo jej adresatem, b=C4=85d=C5=BA=
 otrzymali=C5=9Bcie j=C4=85 przez
> pomy=C5=82k=C4=99, prosimy o powiadomienie o tym nadawcy oraz trwa=C5=82e=
 jej usuni=C4=99cie.
>=20
>  =20
>=20

=20
------=_Part_541993_1216384135.1481293911367
MIME-Version: 1.0
Content-Type: text/html; charset=UTF-8
Content-Transfer-Encoding: quoted-printable

<!DOCTYPE html>
<html><head>
    <meta charset=3D"UTF-8">
<style type=3D"text/css">.mceResizeHandle {position: absolute;border: 1px s=
olid black;background: #FFF;width: 5px;height: 5px;z-index: 10000}.mceResiz=
eHandle:hover {background: #000}img[data-mce-selected] {outline: 1px solid =
black}img.mceClonedResizable, table.mceClonedResizable {position: absolute;=
outline: 1px dashed black;opacity: .5;z-index: 10000}

<!--
  @font-face  {font-family: "Cambria Math";  }
 @font-face  {font-family: Calibri;  }
  p.MsoNormal, li.MsoNormal, div.MsoNormal  {margin: 0cm;  margin-bottom: .=
0001pt;  font-size: 11.0pt;  font-family: "Calibri",sans-serif;  }
 a:link, span.MsoHyperlink  {  color: #0563C1;  text-decoration: underline;=
}
 a:visited, span.MsoHyperlinkFollowed  {  color: #954F72;  text-decoration:=
 underline;}
 span.Stylwiadomocie-mail17  {  font-family: "Calibri",sans-serif;  color: =
windowtext;}
 .MsoChpDefault  {  font-family: "Calibri",sans-serif;  }
 @page WordSection1  {  margin: 70.85pt 70.85pt 70.85pt 70.85pt;}
 div.WordSection1  {}

-->
</style></head><body style=3D""><div>A mo&#380;esz to do zwyk&#322;ego plik=
u tekstowego wrzuci&#263;? Bo mi si&#281; znaki nowej linii pododawa&#322;y=
...</div>
<blockquote type=3D"cite" style=3D"position: relative; margin-left: 0px; pa=
dding-left: 10px; border-left: solid 1px blue;"><!-- [if !mso]><style>v\:* =
{behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
w\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style><![endif] --><!-- [if gte mso 9]><xml>
<o:shapedefaults v:ext=3D"edit" spidmax=3D"1026" />
</xml><![endif] --><!-- [if gte mso 9]><xml>
<o:shapelayout v:ext=3D"edit">
<o:idmap v:ext=3D"edit" data=3D"1" />
</o:shapelayout></xml><![endif] -->Dnia 5 grudzie&#324; 2016 o 13:29 &#34;G=
rzegorz Lachowicz @ SUPERFIRMA.PL&#34; &#60;email@serwer.pl&#62=
; napisa&#322;(a):<br><br>
<div class=3D"WordSection1">
<p class=3D"MsoNormal">to jest jaki&#347; inne e-mail, tamtych ju&#380; nie=
 mam na serwerze.</p>
<p class=3D"MsoNormal">&#160;</p>
<p class=3D"MsoNormal"><span style=3D"color: black;">Pozdrawiam,</span></p>
<div align=3D"center">
<table class=3D"MsoNormalTable" style=3D"width: 95.0%;" border=3D"0" cellpa=
dding=3D"0">
<tbody>
<tr style=3D"height: 33.75pt;">
<td style=3D"padding: .75pt .75pt .75pt .75pt; height: 33.75pt;">
<p class=3D"MsoNormal" style=3D"margin-right: 0cm; margin-bottom: 22.5pt; m=
argin-left: 0cm;"><strong><span style=3D"font-size: 10.0pt; font-family: \'T=
imes New Roman\',serif; color: #11aeab; text-transform: uppercase; letter-sp=
acing: 1.5pt;">Najlepszy Pracownik</span></strong><span style=3D"font-size: =
12.0pt; font-family: \'Times New Roman\',serif;"><br></span><span style=3D"fo=
nt-size: 9.0pt; font-family: \'Times New Roman\',serif; color: #555555; text-=
transform: uppercase; letter-spacing: 1.5pt;">&#160;&#160;&#160;&#160;&#160=
;&#160;&#160;SALES MANAGER</span><span style=3D"font-size: 12.0pt; font-fam=
ily: \'Times New Roman\',serif;"><br><br></span><strong><span style=3D"font-s=
ize: 10.0pt; font-family: \'Times New Roman\',serif; color: #888888;">&#160;&=
#160;&#160;&#160;&#160;&#160;&#160;tel. kom.:</span></strong><span style=3D=
"font-size: 10.0pt; font-family: \'Times New Roman\',serif; color: #555555;">=
 +48 502 268 786</span><span style=3D"font-size: 10.0pt; font-family: \'Time=
s New Roman\',serif;"><br><strong><span style=3D"color: #888888;">e-mail:</s=
pan></strong><span style=3D"color: #555555;"> <a href=3D"mailto:s.pracownik=
@grupaglasso.pl"><span style=3D"color: #555555; text-decoration: none;">g.l=
achowicz@grupaglasso.pl</span></a> </span></span><span style=3D"font-size: =
12.0pt; font-family: \'Times New Roman\',serif;"><br><br></span><span style=
=3D"font-size: 10.0pt; font-family: \'Times New Roman\',serif; color: #555555=
;">GLASSO Pawe&#322; Szczerba | tel.: +48 71 344 44 17<br>ul. Kazimierza Wi=
elkiego 67 | 50-077 Wroc&#322;aw</span><span style=3D"font-size: 12.0pt; fo=
nt-family: \'Times New Roman\',serif;"><br></span><span style=3D"font-size: 1=
0.0pt; font-family: \'Times New Roman\',serif; color: #555555;">NIP: 911-170-=
84-74 | REGON: 020923887 </span><span style=3D"font-size: 12.0pt; font-fami=
ly: \'Times New Roman\',serif;"></span></p>
</td>
</tr>
</tbody>
</table>
</div>
<p class=3D"MsoNormal"><span style=3D"font-size: 12.0pt; font-family: \'Time=
s New Roman\',serif; display: none;">&#160;</span></p>
<table class=3D"MsoNormalTable" border=3D"0" cellpadding=3D"0">
<tbody>
<tr>
<td style=3D"width: 120.0pt; padding: .75pt .75pt .75pt .75pt;" valign=3D"t=
op" width=3D"160">
<p class=3D"MsoNormal" style=3D"text-align: center;" align=3D"center"><a hr=
ef=3D"http://grupaglasso.pl/"><span style=3D"font-size: 12.0pt; font-family=
: \'Times New Roman\',serif; color: blue; text-decoration: none;"><img id=3D"=
_x0000_i1026" src=3D"http://www.grupaglasso.pl/grupalogo.gif" border=3D"0" =
alt=3D"glasso" width=3D"160" height=3D"58"></span></a><span style=3D"font-s=
ize: 12.0pt; font-family: \'Times New Roman\',serif;"><br></span><span style=
=3D"font-size: 10.0pt; font-family: \'Times New Roman\',serif; color: #555555=
;"><a href=3D"http://grupaglasso.pl/"><span style=3D"color: #555555; text-d=
ecoration: none;">www.grupaglasso.pl</span></a> </span><span style=3D"font-=
size: 12.0pt; font-family: \'Times New Roman\',serif;"><br></span><span style=
=3D"font-size: 10.0pt; font-family: \'Times New Roman\',serif; color: #555555=
;"><a href=3D"http://glasso.pl/"><span style=3D"color: #555555; text-decora=
tion: none;">www.glasso.pl</span></a> </span><span style=3D"font-size: 12.0=
pt; font-family: \'Times New Roman\',serif;"><br></span><span style=3D"font-s=
ize: 10.0pt; font-family: \'Times New Roman\',serif; color: #555555;"><a href=
=3D"http://metaleo.pl/"><span style=3D"color: #555555; text-decoration: non=
e;">www.metaleo.pl</span></a> </span><span style=3D"font-size: 12.0pt; font=
-family: \'Times New Roman\',serif;"><br></span><span style=3D"font-size: 10.=
0pt; font-family: \'Times New Roman\',serif; color: #555555;"><a href=3D"http=
://woodeo.pl/"><span style=3D"color: #555555; text-decoration: none;">www.w=
oodeo.pl</span></a> </span><span style=3D"font-size: 12.0pt; font-family: \'=
Times New Roman\',serif;"><br></span><span style=3D"font-size: 10.0pt; font-=
family: \'Times New Roman\',serif; color: #555555;"><a href=3D"http://figro.p=
l/"><span style=3D"color: #555555; text-decoration: none;">www.figro.pl</sp=
an></a> </span><span style=3D"font-size: 12.0pt; font-family: \'Times New Ro=
man\',serif;"><br></span><span style=3D"font-size: 10.0pt; font-family: \'Tim=
es New Roman\',serif; color: #555555;"><a href=3D"http://statuetkiszklane.or=
g/"><span style=3D"color: #555555; text-decoration: none;">www.statuetkiszk=
lane.org</span></a> </span><span style=3D"font-size: 12.0pt; font-family: \'=
Times New Roman\',serif;"><br></span><span style=3D"font-size: 10.0pt; font-=
family: \'Times New Roman\',serif; color: #555555;"><a href=3D"http://karykat=
ura3d.pl/"><span style=3D"color: #555555; text-decoration: none;">www.karyk=
atura3d.pl</span></a> </span><span style=3D"font-size: 12.0pt; font-family:=
 \'Times New Roman\',serif;"></span></p>
</td>
<td style=3D"width: 120.0pt; padding: .75pt .75pt .75pt .75pt;" valign=3D"t=
op" width=3D"160">
<p class=3D"MsoNormal" style=3D"text-align: center;" align=3D"center"><a hr=
ef=3D"http://spragia.pl/"><span style=3D"font-size: 12.0pt; font-family: \'T=
imes New Roman\',serif; color: blue; text-decoration: none;"><img id=3D"_x00=
00_i1025" src=3D"http://www.grupaglasso.pl/spragialogo.gif" border=3D"0" al=
t=3D"spragia" width=3D"160" height=3D"58"></span></a><span style=3D"font-si=
ze: 12.0pt; font-family: \'Times New Roman\',serif;"><br></span><span style=
=3D"font-size: 10.0pt; font-family: \'Times New Roman\',serif; color: #555555=
;"><a href=3D"http://spragia.pl/"><span style=3D"color: #555555; text-decor=
ation: none;">www.spragia.pl</span></a> </span><span style=3D"font-size: 12=
.0pt; font-family: \'Times New Roman\',serif;"><br></span><span style=3D"font=
-size: 10.0pt; font-family: \'Times New Roman\',serif; color: #555555;"><a hr=
ef=3D"http://ekskluzywnewizytowki.pl/"><span style=3D"color: #555555; text-=
decoration: none;">www.ekskluzywnewizytowki.pl</span></a> </span><span styl=
e=3D"font-size: 12.0pt; font-family: \'Times New Roman\',serif;"></span></p>
</td>
</tr>
</tbody>
</table>
<p class=3D"MsoNormal"><span>&#160;</span></p>
<p class=3D"MsoNormal"><em><span style=3D"font-size: 10.0pt; font-family: \'=
Times New Roman\',serif; color: #b2b2b2;">Tre&#347;&#263; tej wiadomo&#347;c=
i zawiera informacje przeznaczone tylko dla adresata. Je&#380;eli nie jeste=
&#347;cie Pa&#324;stwo jej adresatem, b&#261;d&#378; otrzymali&#347;cie j&#=
261; przez pomy&#322;k&#281;, prosimy o powiadomienie o tym nadawcy oraz tr=
wa&#322;e jej usuni&#281;cie.</span></em><span style=3D"font-size: 12.0pt; =
font-family: \'Times New Roman\',serif;"> </span><span></span></p>
<p class=3D"MsoNormal">&#160;</p>
</div>
</blockquote>
<div><br>&#160;</div></body></html>
=20
------=_Part_541993_1216384135.1481293911367--
',
	    self::TESTOWY_OD_GRZEGORZA_2 => 'Return-Path: <prvs=140a3a97b=klient.dobry@jegofirma.com>
Received: from mail2.boehringer-ingelheim.com (148.188.1.172) (HELO mail2.boehringeringelheim.com) by serwer1578643.home.pl (79.96.198.239) with SMTP (IdeaSmtpServer 0.81.1)id e1ad36850476e0cb; Mon, 5 Dec 2016 13:07:14 +0100 DKIM-Signature: v=1; a=rsa-sha256; c=simple/simple;
 d=boehringer-ingelheim.com; i=@boehringer-ingelheim.com;
 q=dns/txt; s=boehringer; t=1480939634; x=1512475634;
 h=from:to:subject:date:message-id:references:in-reply-to: mime-version;
 bh=OeAsuasmAglknZtyOMHAPlt3ACHk51B3jguk+x1vc0w=;
 b=AHRUqtMr3Y1PIjR7wx/nWbyegrH1hD0WskXjdlMvFbhfTIkhl3fMhXWe
 cTJGtwu61VV6XyA2mebNZCrFRjwi94mrhX6EmMwdzZjmZ9xD18Mofa1cN
 M3jZPg6Oit7G3wFVRprGWjZpINCnZ9PYQgG5UBKWo2NBgam2CeQLi2rd0
 Q=;
X-IronPort-AV: E=McAfee;i="5700,7163,8369"; a="317474009"
X-IronPort-AV: E=Sophos;i="5.33,747,1477954800";
 d="png\'150?scan\'150,208,217,150";a="317474009"
Received: from unknown (HELO INHEXCH07.eu.boehringer.com) ([10.183.141.196])
 by mail2.boehringer-ingelheim.com with ESMTP/TLS/AES128-SHA; 05 Dec 2016 13:07:12 +0100
Received: from INHEXMB01.eu.boehringer.com ([fe80::ad15:312:c981:63cf]) by INHEXCH07.eu.boehringer.com ([fe80::e5c5:2c5b:2701:cb27%22]) with mapi id 14.03.0301.000; Mon, 5 Dec 2016 13:07:11 +0100
From: <klient.dobry@jegofirma.com>
To: <email@serwer.pl>
Subject: RE: statuetki szklane
Thread-Topic: statuetki szklane
Thread-Index: AdI1ufQQYs1nzTS7RDyT7fOpr+QG3AADxEwABklKq8A=
Date: Mon, 5 Dec 2016 12:07:11 +0000
Message-ID: <AAEAC27B596BDB4893A0CA9247CBB9A60F0F5E61@INHEXMB01.eu.boehringer.com>
References: <AAEAC27B596BDB4893A0CA9247CBB9A60F0BD315@INHEXMB01.eu.boehringer.com> <050e01d235d1$67346650$359d32f0$@grupaglasso.pl>
In-Reply-To: <050e01d235d1$67346650$359d32f0$@grupaglasso.pl>
Accept-Language: de-DE, en-US
Content-Language: pl-PL
X-MS-Has-Attach: yes
X-MS-TNEF-Correlator:
x-originating-ip: [10.183.157.160]
Content-Type: multipart/related;
boundary="_004_AAEAC27B596BDB4893A0CA9247CBB9A60F0F5E61INHEXMB01euboeh_";
type="multipart/alternative"
MIME-Version: 1.0

--_004_AAEAC27B596BDB4893A0CA9247CBB9A60F0F5E61INHEXMB01euboeh_
Content-Type: multipart/alternative;
boundary="_000_AAEAC27B596BDB4893A0CA9247CBB9A60F0F5E61INHEXMB01euboeh_"
--_000_AAEAC27B596BDB4893A0CA9247CBB9A60F0F5E61INHEXMB01euboeh_
Content-Type: text/plain; charset="iso-8859-2"
Content-Transfer-Encoding: quoted-printable
Panie Grzegorzu,

From: Najlepszy Pracownik @ SUPERFIRMA.PL [mailto:s.pracownik@superfirma.p=
l]
Sent: Thursday, November 03, 2016 1:54 PM
To: Nazwiskoklienta,Maria (HR) BI-PL-W
Subject: RE: statuetki szklane
Dzie=F1 dobry,
Rozumiem, =BFe druga statuetka, to ta z za=B3=B1cznika?
Prosz=EA zatem pracowni=EA o produkcj=EA standardowych statuetek (natomiast=
t=B1 z za=B3=B1cznika mamy gotow=B1.) i czekam na finalne tre=B6ci od Pani=
.
Pozdrawiam,
Najlepszy Pracownik
 SALES MANAGER
 tel. kom.: +48 502 268 786
e-mail: email@serwer.pl<mailto:email@serwer.pl>
GLASSO Pawe=B3 Szczerba | tel.: +48 71 344 44 17
ul. Kazimierza Wielkiego 67 | 50-077 Wroc=B3aw
NIP: 911-170-84-74 | REGON: 020923887
[glasso]<http://grupaglasso.pl/>
www.grupaglasso.pl<http://grupaglasso.pl/>
www.glasso.pl<http://glasso.pl/>
www.metaleo.pl<http://metaleo.pl/>
www.woodeo.pl<http://woodeo.pl/>
www.figro.pl<http://figro.pl/>
www.statuetkiszklane.org<http://statuetkiszklane.org/>
www.karykatura3d.pl<http://karykatura3d.pl/>
[spragia]<http://spragia.pl/>
www.spragia.pl<http://spragia.pl/>
www.ekskluzywnewizytowki.pl<http://ekskluzywnewizytowki.pl/>
Tre=B6=E6 tej wiadomo=B6ci zawiera informacje przeznaczone tylko dla adresa=
ta. Je=BFeli nie jeste=B6cie Pa=F1stwo jej adresatem, b=B1d=BC otrzymali=B6=
cie j=B1 przez pomy=B3k=EA, prosimy o powiadomienie o tym nadawcy oraz trwa=
=B3e jej usuni=EAcie.
From: klient.dobry@jegofirma.com<mailto:maria.slowikowska@b=
oehringer-ingelheim.com> [mailto:klient.dobry@jegofirma.com=
]
Sent: Thursday, November 3, 2016 11:14 AM
To: email@serwer.pl<mailto:email@serwer.pl>
Subject: statuetki szklane
Panie Grzegorzu,
Prosz=EA o ofert=EA na statuetki szklane - takie, jakie zamawiamy w ka=BFdy=
m roku.
Termin dostawy: do 12.12.2016.
Najlepszy Przedstawiciel Animal Health roku 2016
I miejsce - .............
Najlepszy Przedstawiciel Handlowy CHC roku 2016
I miejsce - ...............
Najlepszy Przedstawiciel Medyczny roku 2016
I miejsce -..........................
Najlepszy District Manager roku 2016
I miejsce - ................
Najlepszy Specjalista ds. Kluczowych Klient=F3w HSL roku 2016
I miejsce - ................
Prawdopodobnie b=EAdziemy te=BF zamawia=E6 dodatkowe statuetki (Agility Awa=
rd - takie, jak w ubieg=B3ym roku), ale to potwierdz=EA w p=F3=BCniejszym t=
erminie.
Pozdrawiam / Kind regards,
Maria Nazwiskoklienta
Boehringer Ingelheim Sp. z o.o.
1 Klimczaka Str.
02-797 Warsaw, Poland
Tel.: +48 (22) 6990-632
Mobile: +48 () 508008941
Fax: +48 (22) 6990-698
mailto:klient.dobry@jegofirma.com
---------------------------------------------------------------------------=
------------------
Boehringer Ingelheim Sp. z o.o. z siedzib=B1 przy ul. Klimczaka 1, 02-797 W=
arszawa, wpisana do rejestru przedsi=EAbiorc=F3w Krajowego Rejestru S=B1dow=
ego przez S=B1d Rejonowy dla m.st. Warszawy, XIII Wydzia=B3 Gospodarczy, po=
d nr KRS 0000017946, NIP 521-053-29-35, o kapitale zak=B3adowym w wysoko=B6=
ci 6 548 000,00 z=B3.
Do you really need to print this e-mail?
[cid:image001.png@01D24EF7.03BAAD20]
--_000_AAEAC27B596BDB4893A0CA9247CBB9A60F0F5E61INHEXMB01euboeh_
Content-Type: text/html; charset="iso-8859-2"
Content-Transfer-Encoding: quoted-printable
<html xmlns:v=3D"urn:schemas-microsoft-com:vml" xmlns:o=3D"urn:schemas-micr=
osoft-com:office:office" xmlns:w=3D"urn:schemas-microsoft-com:office:word" =
xmlns:m=3D"http://schemas.microsoft.com/office/2004/12/omml" xmlns=3D"http:=
//www.w3.org/TR/REC-html40">
<head>
<meta http-equiv=3D"Content-Type" content=3D"text/html; charset=3Diso-8859-=
2">
<meta name=3D"Generator" content=3D"Microsoft Word 14 (filtered medium)">
<!--[if !mso]><style>v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
w\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style><![endif]--><style><!--
/* Font Definitions */
@font-face
{font-family:Calibri;
panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
{font-family:Tahoma;
panose-1:2 11 6 4 3 5 4 4 2 4;}
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
{margin:0cm;
margin-bottom:.0001pt;
font-size:11.0pt;
font-family:"Calibri","sans-serif";
mso-fareast-language:EN-US;}
a:link, span.MsoHyperlink
{mso-style-priority:99;
color:blue;
text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
{mso-style-priority:99;
color:purple;
text-decoration:underline;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
{mso-style-priority:99;
mso-style-link:"Tekst dymka Znak";
margin:0cm;
margin-bottom:.0001pt;
font-size:8.0pt;
font-family:"Tahoma","sans-serif";
mso-fareast-language:EN-US;}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
{mso-style-priority:34;
margin-top:0cm;
margin-right:0cm;
margin-bottom:0cm;
margin-left:36.0pt;
margin-bottom:.0001pt;
font-size:11.0pt;
font-family:"Calibri","sans-serif";}
span.TekstdymkaZnak
{mso-style-name:"Tekst dymka Znak";
mso-style-priority:99;
mso-style-link:"Tekst dymka";
font-family:"Tahoma","sans-serif";}
span.Stylwiadomocie-mail20
{mso-style-type:personal;
font-family:"Calibri","sans-serif";
color:windowtext;}
span.Stylwiadomocie-mail21
{mso-style-type:personal;
font-family:"Calibri","sans-serif";
color:windowtext;}
span.Stylwiadomocie-mail22
{mso-style-type:personal-reply;
font-family:"Calibri","sans-serif";
color:#1F497D;}
.MsoChpDefault
{mso-style-type:export-only;
font-size:10.0pt;}
@page WordSection1
{size:612.0pt 792.0pt;
margin:70.85pt 70.85pt 70.85pt 70.85pt;}
div.WordSection1
{page:WordSection1;}
--></style><!--[if gte mso 9]><xml>
<o:shapedefaults v:ext=3D"edit" spidmax=3D"1026" />
</xml><![endif]--><!--[if gte mso 9]><xml>
<o:shapelayout v:ext=3D"edit">
<o:idmap v:ext=3D"edit" data=3D"1" />
</o:shapelayout></xml><![endif]-->
</head>
<body lang=3D"PL" link=3D"blue" vlink=3D"purple">
<div class=3D"WordSection1">
<p class=3D"MsoNormal"><span style=3D"color:#1F497D">Panie Grzegorzu,<o:p><=
/o:p></span></p>
<p class=3D"MsoNormal"><span style=3D"color:#1F497D"><o:p>&nbsp;</o:p></spa=
n></p>
<p class=3D"MsoNormal"><span style=3D"color:#1F497D"><o:p>&nbsp;</o:p></spa=
n></p>
<p class=3D"MsoNormal"><span style=3D"color:#1F497D">Przesy=B3am nazwiska o=
s=F3b do standardowych statuetek:<o:p></o:p></span></p>
<p class=3D"MsoNormal"><span style=3D"color:#1F497D"><o:p>&nbsp;</o:p></spa=
n></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><i>Najlepszy Przedst=
awiciel Animal Health roku 2016<o:p></o:p></i></b></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><i>I miejsce &#8211;=
Agata Dolna</i></b><o:p></o:p></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><span style=3D"color:#1=
F497D"><o:p>&nbsp;</o:p></span></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><i>Najlepszy Przedst=
awiciel Handlowy CHC roku 2016<o:p></o:p></i></b></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><i>I miejsce - &nbsp=
;Daniel Korzeniewski<o:p></o:p></i></b></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><span style=3D"color:#1=
F497D"><o:p>&nbsp;</o:p></span></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><i>Najlepszy Przedst=
awiciel Medyczny roku 2016<o:p></o:p></i></b></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><i>I miejsce &#8211;=
Renata Szymczyk<o:p></o:p></i></b></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><i><o:p>&nbsp;</o:p>=
</i></b></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><i>Najlepszy Distric=
t Manager roku 2016&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&n=
bsp;&nbsp;&nbsp;
<o:p></o:p></i></b></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><i>I miejsce &#8211;=
Anna Dryndos<o:p></o:p></i></b></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><span style=3D"color:#1=
F497D"><o:p>&nbsp;</o:p></span></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><i>Najlepszy Specjal=
ista ds. Kluczowych Klient=F3w HSL roku 2016<o:p></o:p></i></b></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><i>I miejsce &#8211;=
Justyna Przyborowicz</i></b><span style=3D"color:#1F497D"><o:p></o:p></spa=
n></p>
<p class=3D"MsoNormal"><span style=3D"color:#1F497D"><o:p>&nbsp;</o:p></spa=
n></p>
<p class=3D"MsoNormal"><span style=3D"color:#1F497D"><o:p>&nbsp;</o:p></spa=
n></p>
<p class=3D"MsoNormal"><span style=3D"color:#1F497D">Dodatkowych statuetek =
(te z ptakiem) nie b=EAdzie w tym roku.<o:p></o:p></span></p>
<p class=3D"MsoNormal"><span style=3D"color:#1F497D"><o:p>&nbsp;</o:p></spa=
n></p>
<p class=3D"MsoNormal"><span style=3D"color:#1F497D"><o:p>&nbsp;</o:p></spa=
n></p>
<p class=3D"MsoNormal"><span style=3D"color:#1F497D">Pozdrawiam<o:p></o:p><=
/span></p>
<p class=3D"MsoNormal"><span style=3D"color:#1F497D">Maria S=B3owikowska<o:=
p></o:p></span></p>
<p class=3D"MsoNormal"><span style=3D"color:#1F497D"><o:p>&nbsp;</o:p></spa=
n></p>
<p class=3D"MsoNormal"><o:p>&nbsp;</o:p></p>
<div>
<div style=3D"border:none;border-top:solid #B5C4DF 1.0pt;padding:3.0pt 0cm =
0cm 0cm">
<p class=3D"MsoNormal"><b><span style=3D"font-size:10.0pt;font-family:&quot=
;Tahoma&quot;,&quot;sans-serif&quot;;mso-fareast-language:PL">From:</span><=
/b><span style=3D"font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;san=
s-serif&quot;;mso-fareast-language:PL"> Najlepszy Pracownik @ SUPERFIRMA.PL=
[mailto:email@serwer.pl]
<br>
<b>Sent:</b> Thursday, November 03, 2016 1:54 PM<br>
<b>To:</b> Nazwiskoklienta,Maria (HR) BI-PL-W<br>
<b>Subject:</b> RE: statuetki szklane<o:p></o:p></span></p>
</div>
</div>
<p class=3D"MsoNormal"><o:p>&nbsp;</o:p></p>
<p class=3D"MsoNormal">Dzie=F1 dobry,<o:p></o:p></p>
<p class=3D"MsoNormal"><o:p>&nbsp;</o:p></p>
<p class=3D"MsoNormal">Rozumiem, =BFe druga statuetka, to ta z za=B3=B1czni=
ka?<o:p></o:p></p>
<p class=3D"MsoNormal"><o:p>&nbsp;</o:p></p>
<p class=3D"MsoNormal">Prosz=EA zatem pracowni=EA o produkcj=EA standardowy=
ch statuetek (natomiast t=B1 z za=B3=B1cznika mamy gotow=B1.) i czekam na f=
inalne tre=B6ci od Pani.<o:p></o:p></p>
<div>
<p class=3D"MsoNormal" style=3D"mso-margin-top-alt:auto;mso-margin-bottom-a=
lt:auto"><span style=3D"color:black;mso-fareast-language:PL">Pozdrawiam,<o:=
p></o:p></span></p>
<div align=3D"center">
<table class=3D"MsoNormalTable" border=3D"0" cellpadding=3D"0" width=3D"95%=
" style=3D"width:95.0%">
<tbody>
<tr style=3D"height:33.75pt">
<td style=3D"padding:.75pt .75pt .75pt .75pt;height:33.75pt">
<p class=3D"MsoNormal" style=3D"mso-margin-top-alt:7.5pt;margin-right:0cm;m=
argin-bottom:22.5pt;margin-left:0cm">
<b><span style=3D"font-size:10.0pt;font-family:&quot;Times New Roman&quot;,=
&quot;serif&quot;;color:#11AEAB;text-transform:uppercase;letter-spacing:1.5=
pt;mso-fareast-language:PL">Najlepszy Pracownik</span></b><span style=3D"fon=
t-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso=
-fareast-language:PL"><br>
</span><span style=3D"font-size:9.0pt;font-family:&quot;Times New Roman&quo=
t;,&quot;serif&quot;;color:#555555;text-transform:uppercase;letter-spacing:=
1.5pt;mso-fareast-language:PL">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SA=
LES MANAGER</span><span style=3D"font-size:12.0pt;font-family:&quot;Times N=
ew Roman&quot;,&quot;serif&quot;;mso-fareast-language:PL"><br>
<br>
</span><b><span style=3D"font-size:10.0pt;font-family:&quot;Times New Roman=
&quot;,&quot;serif&quot;;color:#888888;mso-fareast-language:PL">&nbsp;&nbsp=
;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tel. kom.:</span></b><span style=3D"font-siz=
e:10.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;color:#5=
55555;mso-fareast-language:PL"> &#43;48
502 268 786</span><span style=3D"font-size:10.0pt;font-family:&quot;Times =
New Roman&quot;,&quot;serif&quot;;mso-fareast-language:PL"><br>
<b><span style=3D"color:#888888">e-mail:</span></b><span style=3D"color:#55=
5555"> <a href=3D"mailto:email@serwer.pl" title=3D"email Michal"=
>
<span style=3D"color:#555555;text-decoration:none">s.pracownik@superfirma.=
pl</span></a>
</span></span><span style=3D"font-size:12.0pt;font-family:&quot;Times New R=
oman&quot;,&quot;serif&quot;;mso-fareast-language:PL"><br>
<br>
</span><span style=3D"font-size:10.0pt;font-family:&quot;Times New Roman&qu=
ot;,&quot;serif&quot;;color:#555555;mso-fareast-language:PL">GLASSO Pawe=B3=
Szczerba | tel.: &#43;48 71 344 44 17<br>
ul. Kazimierza Wielkiego 67 | 50-077 Wroc=B3aw</span><span style=3D"font-si=
ze:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-far=
east-language:PL"><br>
</span><span style=3D"font-size:10.0pt;font-family:&quot;Times New Roman&qu=
ot;,&quot;serif&quot;;color:#555555;mso-fareast-language:PL">NIP: 911-170-8=
4-74 | REGON: 020923887
</span><span style=3D"font-size:12.0pt;font-family:&quot;Times New Roman&qu=
ot;,&quot;serif&quot;;mso-fareast-language:PL"><o:p></o:p></span></p>
</td>
</tr>
</tbody>
</table>
</div>
<p class=3D"MsoNormal"><span style=3D"font-size:12.0pt;font-family:&quot;Ti=
mes New Roman&quot;,&quot;serif&quot;;display:none;mso-fareast-language:PL"=
><o:p>&nbsp;</o:p></span></p>
<table class=3D"MsoNormalTable" border=3D"0" cellpadding=3D"0">
<tbody>
<tr>
<td width=3D"160" valign=3D"top" style=3D"width:120.0pt;padding:.75pt .75pt=
.75pt .75pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><a href=
=3D"http://grupaglasso.pl/"><span style=3D"font-size:12.0pt;font-family:&qu=
ot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-language:PL;text-dec=
oration:none"><img border=3D"0" width=3D"160" height=3D"58" id=3D"_x0000_i1=
025" src=3D"http://www.grupaglasso.pl/grupalogo.gif" alt=3D"glasso"></span>=
</a><span style=3D"font-size:12.0pt;font-family:&quot;Times New Roman&quot;=
,&quot;serif&quot;;mso-fareast-language:PL"><br>
</span><span style=3D"font-size:10.0pt;font-family:&quot;Times New Roman&qu=
ot;,&quot;serif&quot;;color:#555555;mso-fareast-language:PL"><a href=3D"htt=
p://grupaglasso.pl/"><span style=3D"color:#555555;text-decoration:none">www=
.grupaglasso.pl</span></a>
</span><span style=3D"font-size:12.0pt;font-family:&quot;Times New Roman&qu=
ot;,&quot;serif&quot;;mso-fareast-language:PL"><br>
</span><span style=3D"font-size:10.0pt;font-family:&quot;Times New Roman&qu=
ot;,&quot;serif&quot;;color:#555555;mso-fareast-language:PL"><a href=3D"htt=
p://glasso.pl/"><span style=3D"color:#555555;text-decoration:none">www.glas=
so.pl</span></a>
</span><span style=3D"font-size:12.0pt;font-family:&quot;Times New Roman&qu=
ot;,&quot;serif&quot;;mso-fareast-language:PL"><br>
</span><span style=3D"font-size:10.0pt;font-family:&quot;Times New Roman&qu=
ot;,&quot;serif&quot;;color:#555555;mso-fareast-language:PL"><a href=3D"htt=
p://metaleo.pl/"><span style=3D"color:#555555;text-decoration:none">www.met=
aleo.pl</span></a>
</span><span style=3D"font-size:12.0pt;font-family:&quot;Times New Roman&qu=
ot;,&quot;serif&quot;;mso-fareast-language:PL"><br>
</span><span style=3D"font-size:10.0pt;font-family:&quot;Times New Roman&qu=
ot;,&quot;serif&quot;;color:#555555;mso-fareast-language:PL"><a href=3D"htt=
p://woodeo.pl/"><span style=3D"color:#555555;text-decoration:none">www.wood=
eo.pl</span></a>
</span><span style=3D"font-size:12.0pt;font-family:&quot;Times New Roman&qu=
ot;,&quot;serif&quot;;mso-fareast-language:PL"><br>
</span><span style=3D"font-size:10.0pt;font-family:&quot;Times New Roman&qu=
ot;,&quot;serif&quot;;color:#555555;mso-fareast-language:PL"><a href=3D"htt=
p://figro.pl/"><span style=3D"color:#555555;text-decoration:none">www.figro=
.pl</span></a>
</span><span style=3D"font-size:12.0pt;font-family:&quot;Times New Roman&qu=
ot;,&quot;serif&quot;;mso-fareast-language:PL"><br>
</span><span style=3D"font-size:10.0pt;font-family:&quot;Times New Roman&qu=
ot;,&quot;serif&quot;;color:#555555;mso-fareast-language:PL"><a href=3D"htt=
p://statuetkiszklane.org/"><span style=3D"color:#555555;text-decoration:non=
e">www.statuetkiszklane.org</span></a>
</span><span style=3D"font-size:12.0pt;font-family:&quot;Times New Roman&qu=
ot;,&quot;serif&quot;;mso-fareast-language:PL"><br>
</span><span style=3D"font-size:10.0pt;font-family:&quot;Times New Roman&qu=
ot;,&quot;serif&quot;;color:#555555;mso-fareast-language:PL"><a href=3D"htt=
p://karykatura3d.pl/"><span style=3D"color:#555555;text-decoration:none">ww=
w.karykatura3d.pl</span></a>
</span><span style=3D"font-size:12.0pt;font-family:&quot;Times New Roman&qu=
ot;,&quot;serif&quot;;mso-fareast-language:PL"><o:p></o:p></span></p>
</td>
<td width=3D"160" valign=3D"top" style=3D"width:120.0pt;padding:.75pt .75pt=
.75pt .75pt">
<p class=3D"MsoNormal" align=3D"center" style=3D"text-align:center"><a href=
=3D"http://spragia.pl/"><span style=3D"font-size:12.0pt;font-family:&quot;T=
imes New Roman&quot;,&quot;serif&quot;;mso-fareast-language:PL;text-decorat=
ion:none"><img border=3D"0" width=3D"160" height=3D"58" id=3D"_x0000_i1026"=
src=3D"http://www.grupaglasso.pl/spragialogo.gif" alt=3D"spragia"></span><=
/a><span style=3D"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,=
&quot;serif&quot;;mso-fareast-language:PL"><br>
</span><span style=3D"font-size:10.0pt;font-family:&quot;Times New Roman&qu=
ot;,&quot;serif&quot;;color:#555555;mso-fareast-language:PL"><a href=3D"htt=
p://spragia.pl/"><span style=3D"color:#555555;text-decoration:none">www.spr=
agia.pl</span></a>
</span><span style=3D"font-size:12.0pt;font-family:&quot;Times New Roman&qu=
ot;,&quot;serif&quot;;mso-fareast-language:PL"><br>
</span><span style=3D"font-size:10.0pt;font-family:&quot;Times New Roman&qu=
ot;,&quot;serif&quot;;color:#555555;mso-fareast-language:PL"><a href=3D"htt=
p://ekskluzywnewizytowki.pl/"><span style=3D"color:#555555;text-decoration:=
none">www.ekskluzywnewizytowki.pl</span></a>
</span><span style=3D"font-size:12.0pt;font-family:&quot;Times New Roman&qu=
ot;,&quot;serif&quot;;mso-fareast-language:PL"><o:p></o:p></span></p>
</td>
</tr>
</tbody>
</table>
<p class=3D"MsoNormal"><span style=3D"mso-fareast-language:PL"><o:p>&nbsp;<=
/o:p></span></p>
<p class=3D"MsoNormal"><i><span style=3D"font-size:10.0pt;font-family:&quot=
;Times New Roman&quot;,&quot;serif&quot;;color:#B2B2B2;mso-fareast-language=
:PL">Tre=B6=E6 tej wiadomo=B6ci zawiera informacje przeznaczone tylko dla a=
dresata. Je=BFeli nie jeste=B6cie Pa=F1stwo jej adresatem, b=B1d=BC otrzyma=
li=B6cie
j=B1 przez pomy=B3k=EA, prosimy o powiadomienie o tym nadawcy oraz trwa=B3=
e jej usuni=EAcie.</span></i><span style=3D"font-size:12.0pt;font-family:&q=
uot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-language:PL">
</span><span style=3D"mso-fareast-language:PL"><o:p></o:p></span></p>
</div>
<p class=3D"MsoNormal"><o:p>&nbsp;</o:p></p>
<div>
<div style=3D"border:none;border-top:solid #E1E1E1 1.0pt;padding:3.0pt 0cm =
0cm 0cm">
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><span style=3D"mso-f=
areast-language:PL">From:</span></b><span style=3D"mso-fareast-language:PL"=
>
<a href=3D"mailto:klient.dobry@jegofirma.com">maria.slowiko=
wska@boehringer-ingelheim.com</a> [<a href=3D"mailto:maria.slowikowska@boeh=
ringer-ingelheim.com">mailto:klient.dobry@jegofirma.com</a>=
]
<br>
<b>Sent:</b> Thursday, November 3, 2016 11:14 AM<br>
<b>To:</b> <a href=3D"mailto:email@serwer.pl">s.pracownik@grupag=
lasso.pl</a><br>
<b>Subject:</b> statuetki szklane<o:p></o:p></span></p>
</div>
</div>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><o:p>&nbsp;</o:p></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt">Panie Grzegorzu,<o:p></=
o:p></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><o:p>&nbsp;</o:p></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt">Prosz=EA o ofert=EA na =
statuetki szklane &#8211; takie, jakie zamawiamy w ka=BFdym roku.<o:p></o:p=
></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><o:p>&nbsp;</o:p></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt">Termin dostawy: do 12.1=
2.2016.<o:p></o:p></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><o:p>&nbsp;</o:p></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><i>Najlepszy Przedst=
awiciel Animal Health roku 2016<o:p></o:p></i></b></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><i>I miejsce &#8211;=
&#8230;&#8230;&#8230;&#8230;.</i></b><o:p></o:p></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><span style=3D"color:#1=
F497D"><o:p>&nbsp;</o:p></span></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><i>Najlepszy Przedst=
awiciel Handlowy CHC roku 2016<o:p></o:p></i></b></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><i>I miejsce - &nbsp=
;&#8230;&#8230;&#8230;&#8230;&#8230;<o:p></o:p></i></b></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><span style=3D"color:#1=
F497D"><o:p>&nbsp;</o:p></span></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><i>Najlepszy Przedst=
awiciel Medyczny roku 2016<o:p></o:p></i></b></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><i>I miejsce -&#8230=
;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;..<o:p></o:p></i></b></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><i><o:p>&nbsp;</o:p>=
</i></b></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><i>Najlepszy Distric=
t Manager roku 2016&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&n=
bsp;&nbsp;&nbsp;
<o:p></o:p></i></b></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><i>I miejsce - &#823=
0;&#8230;&#8230;&#8230;&#8230;.<o:p></o:p></i></b></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><span style=3D"color:#1=
F497D"><o:p>&nbsp;</o:p></span></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><i>Najlepszy Specjal=
ista ds. Kluczowych Klient=F3w HSL roku 2016<o:p></o:p></i></b></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><i>I miejsce &#8211;=
&#8230;&#8230;&#8230;&#8230;&#8230;.</i></b><span style=3D"color:#1F497D">=
<o:p></o:p></span></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><o:p>&nbsp;</o:p></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><o:p>&nbsp;</o:p></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><o:p>&nbsp;</o:p></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt">Prawdopodobnie b=EAdzie=
my te=BF zamawia=E6 dodatkowe statuetki (Agility Award - takie, jak w ubieg=
=B3ym roku), ale to potwierdz=EA w p=F3=BCniejszym terminie.<o:p></o:p></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><o:p>&nbsp;</o:p></p>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><o:p>&nbsp;</o:p></p>
<div>
<div>
<p class=3D"MsoNormal" style=3D"mso-margin-top-alt:auto;mso-margin-bottom-a=
lt:auto;margin-left:35.4pt">
<span lang=3D"EN-US" style=3D"font-size:10.0pt;mso-fareast-language:PL">Poz=
drawiam&nbsp;/ Kind regards,
<br>
<b>Maria Nazwiskoklienta </b></span><span lang=3D"EN-US" style=3D"font-size:10.=
0pt;font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;;mso-fareast-langu=
age:PL"><o:p></o:p></span></p>
</div>
</div>
<p class=3D"MsoNormal" style=3D"margin-left:35.4pt"><b><span lang=3D"EN-US"=
style=3D"font-size:10.0pt;mso-fareast-language:PL">Boehringer Ingelheim Sp=
. z o.o.&nbsp;
</span></b><span lang=3D"EN-US" style=3D"font-size:10.0pt;mso-fareast-langu=
age:PL">&nbsp; <br>
</span><span style=3D"font-size:10.0pt;mso-fareast-language:PL">1 Klimczaka=
Str.&nbsp;&nbsp; </span>
<span style=3D"font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot;sans-s=
erif&quot;;mso-fareast-language:PL"><br>
</span><span style=3D"font-size:10.0pt;mso-fareast-language:PL">02-797 Wars=
aw, Poland
<br>
Tel.: &#43;48 (22) 6990-632 <br>
Mobile: &#43;48 () 508008941 <br>
Fax: &#43;48 (22) 6990-698 <br>
</span><span style=3D"font-size:10.0pt;font-family:&quot;Tahoma&quot;,&quot=
;sans-serif&quot;;mso-fareast-language:PL"><a href=3D"mailto:maria.slowikow=
ska@boehringer-ingelheim.com"><span style=3D"font-family:&quot;Calibri&quot=
;,&quot;sans-serif&quot;">mailto:klient.dobry@jegofirma.com=
<br>
</span></a></span><span style=3D"font-size:7.5pt;mso-fareast-language:PL">-=
---------------------------------------------------------------------------=
-----------------<br>
</span><span style=3D"font-size:7.5pt;font-family:&quot;Tahoma&quot;,&quot;=
sans-serif&quot;;mso-fareast-language:PL">Boehringer Ingelheim Sp. z o.o. z=
siedzib=B1 przy ul. Klimczaka 1, 02-797 Warszawa, wpisana do rejestru prze=
dsi=EAbiorc=F3w Krajowego Rejestru S=B1dowego przez S=B1d Rejonowy
dla m.st. Warszawy, XIII Wydzia=B3 Gospodarczy, pod nr KRS 0000017946, NIP=
521-053-29-35, o kapitale zak=B3adowym w wysoko=B6ci 6 548 000,00 z=B3.</s=
pan><span style=3D"font-size:7.5pt;mso-fareast-language:PL">
<br>
</span><span lang=3D"EN-GB" style=3D"font-size:7.5pt;color:green;mso-fareas=
t-language:PL">Do you really need to print this e-mail?<br>
<br>
&nbsp;</span><span style=3D"font-size:7.5pt;color:green;mso-fareast-languag=
e:PL"><img border=3D"0" width=3D"150" height=3D"67" id=3D"_x0000_i1027" src=
=3D"cid:image001.png@01D24EF7.03BAAD20">
</span><o:p></o:p></p>
</div>
</body>
</html>
--_000_AAEAC27B596BDB4893A0CA9247CBB9A60F0F5E61INHEXMB01euboeh_--
--_004_AAEAC27B596BDB4893A0CA9247CBB9A60F0F5E61INHEXMB01euboeh_
Content-Type: image/png; name="image001.png"
Content-Description: image001.png
Content-Disposition: inline; filename="image001.png"; size=70263;
creation-date="Mon, 05 Dec 2016 11:56:36 GMT";
modification-date="Mon, 05 Dec 2016 11:56:36 GMT"
Content-ID: <image001.png@01D24EF7.03BAAD20>
Content-Transfer-Encoding: base64
iVBORw0KGgoAAAANSUhEUgAAAJYAAABDCAYAAAB+8vx+AAAABGdBTUEAALGOfPtRkwAAACBjSFJN
AACHDwAAjA8AAP1SAACBQAAAfXkAAOmLAAA85QAAGcxzPIV3AAAKOWlDQ1BQaG90b3Nob3AgSUN
D
IHByb2ZpbGUAAEjHnZZ3VFTXFofPvXd6oc0wAlKG3rvAANJ7k15FYZgZYCgDDjM0sSGiAhFFRJoi
SFDEgNFQJFZEsRAUVLAHJAgoMRhFVCxvRtaLrqy89/Ly++Osb+2z97n77L3PWhcAkqcvl5cGSwGQ
yhPwgzyc6RGRUXTsAIABHmCAKQBMVka6X7B7CBDJy82FniFyAl8EAfB6WLwCcNPQM4BOB/+fpFnp
fIHomAARm7M5GSwRF4g4JUuQLrbPipgalyxmGCVmvihBEcuJOWGRDT77LLKjmNmpPLaIxTmns1PZ
Yu4V8bZMIUfEiK+ICzO5nCwR3xKxRoowlSviN+LYVA4zAwAUSWwXcFiJIjYRMYkfEuQi4uUA4EgJ
X3HcVyzgZAvEl3JJS8/hcxMSBXQdli7d1NqaQffkZKVwBALDACYrmcln013SUtOZvBwAFu/8WTLi
2tJFRbY0tba0NDQzMv2qUP91829K3NtFehn4uWcQrf+L7a/80hoAYMyJarPziy2uCoDOLQDI3fti
0zgAgKSobx3Xv7oPTTwviQJBuo2xcVZWlhGXwzISF/QP/U+Hv6GvvmckPu6P8tBdOfFMYYqALq4b
Ky0lTcinZ6QzWRy64Z+H+B8H/nUeBkGceA6fwxNFhImmjMtLELWbx+YKuGk8Opf3n5r4D8P+pMW5
FonS+BFQY4yA1HUqQH7tBygKESDR+8Vd/6NvvvgwIH554SqTi3P/7zf9Z8Gl4iWDm/A5ziUohM4S
8jMX98TPEqABAUgCKpAHykAd6ABDYAasgC1wBG7AG/iDEBAJVgMWSASpgA+yQB7YBApBMdgJ9oBq
UAcaQTNoBcdBJzgFzoNL4Bq4AW6D+2AUTIBnYBa8BgsQBGEhMkSB5CEVSBPSh8wgBmQPuUG+UBAU
CcVCCRAPEkJ50GaoGCqDqqF6qBn6HjoJnYeuQIPQXWgMmoZ+h97BCEyCqbASrAUbwwzYCfaBQ+BV
cAK8Bs6FC+AdcCXcAB+FO+Dz8DX4NjwKP4PnEIAQERqiihgiDMQF8UeikHiEj6xHipAKpAFpRbqR
PuQmMorMIG9RGBQFRUcZomxRnqhQFAu1BrUeVYKqRh1GdaB6UTdRY6hZ1Ec0Ga2I1kfboL3QEegE
dBa6EF2BbkK3oy+ib6Mn0K8xGAwNo42xwnhiIjFJmLWYEsw+TBvmHGYQM46Zw2Kx8lh9rB3WH8vE
CrCF2CrsUexZ7BB2AvsGR8Sp4Mxw7rgoHA+Xj6vAHcGdwQ3hJnELeCm8Jt4G749n43PwpfhGfDf+
On4Cv0CQJmgT7AghhCTCJkIloZVwkfCA8JJIJKoRrYmBRC5xI7GSeIx4mThGfEuSIemRXEjRJCFp
B+kQ6RzpLuklmUzWIjuSo8gC8g5yM/kC+RH5jQRFwkjCS4ItsUGiRqJDYkjiuSReUlPSSXK1ZK5k
heQJyeuSM1J4KS0pFymm1HqpGqmTUiNSc9IUaVNpf+lU6RLpI9JXpKdksDJaMm4ybJkCmYMyF2TG
KQhFneJCYVE2UxopFykTVAxVm+pFTaIWU7+jDlBnZWVkl8mGyWbL1sielh2lITQtmhcthVZKO04b
pr1borTEaQlnyfYlrUuGlszLLZVzlOPIFcm1yd2WeydPl3eTT5bfJd8p/1ABpaCnEKiQpbBf4aLC
zFLqUtulrKVFS48vvacIK+opBimuVTyo2K84p6Ss5KGUrlSldEFpRpmm7KicpFyufEZ5WoWiYq/C
VSlXOavylC5Ld6Kn0CvpvfRZVUVVT1Whar3qgOqCmrZaqFq+WpvaQ3WCOkM9Xr1cvUd9VkNFw08j
T6NF454mXpOhmai5V7NPc15LWytca6tWp9aUtpy2l3audov2Ax2yjoPOGp0GnVu6GF2GbrLuPt0b
erCehV6iXo3edX1Y31Kfq79Pf9AAbWBtwDNoMBgxJBk6GWYathiOGdGMfI3yjTqNnhtrGEcZ7zLu
M/5oYmGSYtJoct9UxtTbNN+02/R3Mz0zllmN2S1zsrm7+QbzLvMXy/SXcZbtX3bHgmLhZ7HVosfi
g6WVJd+y1XLaSsMq1qrWaoRBZQQwShiXrdHWztYbrE9Zv7WxtBHYHLf5zdbQNtn2iO3Ucu3lnOWN
y8ft1OyYdvV2o/Z0+1j7A/ajDqoOTIcGh8eO6o5sxybHSSddpySno07PnU2c+c7tzvMuNi7rXM65
Iq4erkWuA24ybqFu1W6P3NXcE9xb3Gc9LDzWepzzRHv6eO7yHPFS8mJ5NXvNelt5r/Pu9SH5BPtU
+zz21fPl+3b7wX7efrv9HqzQXMFb0ekP/L38d/s/DNAOWBPwYyAmMCCwJvBJkGlQXlBfMCU4JvhI
8OsQ55DSkPuhOqHC0J4wybDosOaw+XDX8LLw0QjjiHUR1yIVIrmRXVHYqLCopqi5lW4r96yciLaI
LoweXqW9KnvVldUKq1NWn46RjGHGnIhFx4bHHol9z/RnNjDn4rziauNmWS6svaxnbEd2OXuaY8cp
40zG28WXxU8l2CXsTphOdEisSJzhunCruS+SPJPqkuaT/ZMPJX9KCU9pS8Wlxqae5Mnwknm9acpp
2WmD6frphemja2zW7Fkzy/fhN2VAGasyugRU0c9Uv1BHuEU4lmmfWZP5Jiss60S2dDYvuz9HL2d7
zmSue+63a1FrWWt78lTzNuWNrXNaV78eWh+3vmeD+oaCDRMbPTYe3kTYlLzpp3yT/LL8V5vDN3cX
KBVsLBjf4rGlpVCikF84stV2a9021DbutoHt5turtn8sYhddLTYprih+X8IqufqN6TeV33zaEb9j
oNSydP9OzE7ezuFdDrsOl0mX5ZaN7/bb3VFOLy8qf7UnZs+VimUVdXsJe4V7Ryt9K7uqNKp2Vr2v
Tqy+XeNc01arWLu9dn4fe9/Qfsf9rXVKdcV17w5wD9yp96jvaNBqqDiIOZh58EljWGPft4xvm5sU
moqbPhziHRo9HHS4t9mqufmI4pHSFrhF2DJ9NProje9cv+tqNWytb6O1FR8Dx4THnn4f+/3wcZ/j
PScYJ1p/0Pyhtp3SXtQBdeR0zHYmdo52RXYNnvQ+2dNt293+o9GPh06pnqo5LXu69AzhTMGZT2dz
z86dSz83cz7h/HhPTM/9CxEXbvUG9g5c9Ll4+ZL7pQt9Tn1nL9tdPnXF5srJq4yrndcsr3X0W/S3
/2TxU/uA5UDHdavrXTesb3QPLh88M+QwdP6m681Lt7xuXbu94vbgcOjwnZHokdE77DtTd1PuvriX
eW/h/sYH6AdFD6UeVjxSfNTws+7PbaOWo6fHXMf6Hwc/vj/OGn/2S8Yv7ycKnpCfVEyqTDZPmU2d
mnafvvF05dOJZ+nPFmYKf5X+tfa5zvMffnP8rX82YnbiBf/Fp99LXsq/PPRq2aueuYC5R69TXy/M
F72Rf3P4LeNt37vwd5MLWe+x7ys/6H7o/ujz8cGn1E+f/gUDmPP8usTo0wAAAAlwSFlzAAALEgAA
CxIB0t1+/AAA5K1pVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/
IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+Cjx4OnhtcG1ldGEgeG1sbnM6eD0iYWRv
YmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTExIDc5LjE1ODMyNSwg
MjAxNS8wOS8xMC0wMToxMDoyMCAgICAgICAgIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRw
Oi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNj
cmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9y
Zy9kYy9lbGVtZW50cy8xLjEvIgogICAgICAgICAgICB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9i
ZS5jb20veGFwLzEuMC8iCiAgICAgICAgICAgIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUu
Y29tL3hhcC8xLjAvbW0vIgogICAgICAgICAgICB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2Jl
LmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIKICAgICAgICAgICAgeG1sbnM6c3RFdnQ9
Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIKICAgICAg
ICAgICAgeG1sbnM6aWxsdXN0cmF0b3I9Imh0dHA6Ly9ucy5hZG9iZS5jb20vaWxsdXN0cmF0b3Iv
MS4wLyIKICAgICAgICAgICAgeG1sbnM6eG1wVFBnPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8x
LjAvdC9wZy8iCiAgICAgICAgICAgIHhtbG5zOnN0RGltPSJodHRwOi8vbnMuYWRvYmUuY29tL3hh
cC8xLjAvc1R5cGUvRGltZW5zaW9ucyMiCiAgICAgICAgICAgIHhtbG5zOnhtcEc9Imh0dHA6Ly9u
cy5hZG9iZS5jb20veGFwLzEuMC9nLyIKICAgICAgICAgICAgeG1sbnM6cGRmPSJodHRwOi8vbnMu
YWRvYmUuY29tL3BkZi8xLjMvIgogICAgICAgICAgICB4bWxuczpwaG90b3Nob3A9Imh0dHA6Ly9u
cy5hZG9iZS5jb20vcGhvdG9zaG9wLzEuMC8iCiAgICAgICAgICAgIHhtbG5zOnRpZmY9Imh0dHA6
Ly9ucy5hZG9iZS5jb20vdGlmZi8xLjAvIgogICAgICAgICAgICB4bWxuczpleGlmPSJodHRwOi8v
bnMuYWRvYmUuY29tL2V4aWYvMS4wLyI+CiAgICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2UvcG5nPC9k
Yzpmb3JtYXQ+CiAgICAgICAgIDxkYzp0aXRsZT4KICAgICAgICAgICAgPHJkZjpBbHQ+CiAgICAg
ICAgICAgICAgIDxyZGY6bGkgeG1sOmxhbmc9IngtZGVmYXVsdCI+VEVfS0VVUk1FUktfRVVST1BB
IDE0PC9yZGY6bGk+CiAgICAgICAgICAgIDwvcmRmOkFsdD4KICAgICAgICAgPC9kYzp0aXRsZT4K
ICAgICAgICAgPHhtcDpNZXRhZGF0YURhdGU+MjAxNi0wMi0yNFQxMDowMTozOSswMTowMDwveG1
w
Ok1ldGFkYXRhRGF0ZT4KICAgICAgICAgPHhtcDpNb2RpZnlEYXRlPjIwMTYtMDItMjRUMTA6MDE6
MzkrMDE6MDA8L3htcDpNb2RpZnlEYXRlPgogICAgICAgICA8eG1wOkNyZWF0ZURhdGU+MjAxNC0w
NC0wNFQwOToxODoyMyswMjowMDwveG1wOkNyZWF0ZURhdGU+CiAgICAgICAgIDx4bXA6Q3JlYXRv
clRvb2w+QWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCk8L3htcDpDcmVhdG9yVG9v
bD4KICAgICAgICAgPHhtcE1NOkluc3RhbmNlSUQ+eG1wLmlpZDpjMDQ2MmFjZS01ZDZiLTQwNjEt
YjMwMC01YjJhOWM3Y2JkMzI8L3htcE1NOkluc3RhbmNlSUQ+CiAgICAgICAgIDx4bXBNTTpEb2N1
bWVudElEPmFkb2JlOmRvY2lkOnBob3Rvc2hvcDozYmJhNDc1Zi0xOWRmLTExNzktYjI5ZS1iNTc1
MjMzNDEzMzQ8L3htcE1NOkRvY3VtZW50SUQ+CiAgICAgICAgIDx4bXBNTTpPcmlnaW5hbERvY3Vt
ZW50SUQ+dXVpZDo1RDIwODkyNDkzQkZEQjExOTE0QTg1OTBEMzE1MDhDODwveG1wTU06T3JpZ2lu
YWxEb2N1bWVudElEPgogICAgICAgICA8eG1wTU06UmVuZGl0aW9uQ2xhc3M+cHJvb2Y6cGRmPC94
bXBNTTpSZW5kaXRpb25DbGFzcz4KICAgICAgICAgPHhtcE1NOkRlcml2ZWRGcm9tIHJkZjpwYXJz
ZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgPHN0UmVmOmluc3RhbmNlSUQ+eG1wLmlpZDpi
ZDczNzBiYS1lNmM1LTRlZTctOGM3YS02NzZmMjc0OTYyMjA8L3N0UmVmOmluc3RhbmNlSUQ+CiAg
ICAgICAgICAgIDxzdFJlZjpkb2N1bWVudElEPnhtcC5kaWQ6YmQ3MzcwYmEtZTZjNS00ZWU3LThj
N2EtNjc2ZjI3NDk2MjIwPC9zdFJlZjpkb2N1bWVudElEPgogICAgICAgICAgICA8c3RSZWY6b3Jp
Z2luYWxEb2N1bWVudElEPnV1aWQ6NUQyMDg5MjQ5M0JGREIxMTkxNEE4NTkwRDMxNTA4Qzg8L3N
0
UmVmOm9yaWdpbmFsRG9jdW1lbnRJRD4KICAgICAgICAgICAgPHN0UmVmOnJlbmRpdGlvbkNsYXNz
PnByb29mOnBkZjwvc3RSZWY6cmVuZGl0aW9uQ2xhc3M+CiAgICAgICAgIDwveG1wTU06RGVyaXZl
ZEZyb20+CiAgICAgICAgIDx4bXBNTTpIaXN0b3J5PgogICAgICAgICAgICA8cmRmOlNlcT4KICAg
ICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAg
ICAgICAgIDxzdEV2dDphY3Rpb24+c2F2ZWQ8L3N0RXZ0OmFjdGlvbj4KICAgICAgICAgICAgICAg
ICAgPHN0RXZ0Omluc3RhbmNlSUQ+eG1wLmlpZDpGQzdGMTE3NDA3MjA2ODExODA4Mzk0RjNDNU
Q0
MDREQjwvc3RFdnQ6aW5zdGFuY2VJRD4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OndoZW4+MjAx
My0wNi0wNVQxMTo0ODowNiswMjowMDwvc3RFdnQ6d2hlbj4KICAgICAgICAgICAgICAgICAgPHN0
RXZ0OnNvZnR3YXJlQWdlbnQ+QWRvYmUgSWxsdXN0cmF0b3IgQ1M1PC9zdEV2dDpzb2Z0d2FyZUFn
ZW50PgogICAgICAgICAgICAgICAgICA8c3RFdnQ6Y2hhbmdlZD4vPC9zdEV2dDpjaGFuZ2VkPgog
ICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VU
eXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDphY3Rpb24+c2F2ZWQ8L3N0
RXZ0OmFjdGlvbj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0Omluc3RhbmNlSUQ+eG1wLmlpZDpG
RTdGMTE3NDA3MjA2ODExODA4Mzk0RjNDNUQ0MDREQjwvc3RFdnQ6aW5zdGFuY2VJRD4KICAgICAg
ICAgICAgICAgICAgPHN0RXZ0OndoZW4+MjAxMy0wNi0wNVQxMTo0OTo0NSswMjowMDwvc3RFdnQ6
d2hlbj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OnNvZnR3YXJlQWdlbnQ+QWRvYmUgSWxsdXN0
cmF0b3IgQ1M1PC9zdEV2dDpzb2Z0d2FyZUFnZW50PgogICAgICAgICAgICAgICAgICA8c3RFdnQ6
Y2hhbmdlZD4vPC9zdEV2dDpjaGFuZ2VkPgogICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAg
ICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAg
ICAgIDxzdEV2dDphY3Rpb24+c2F2ZWQ8L3N0RXZ0OmFjdGlvbj4KICAgICAgICAgICAgICAgICAg
PHN0RXZ0Omluc3RhbmNlSUQ+eG1wLmlpZDpGRjdGMTE3NDA3MjA2ODExODA4Mzk0RjNDNUQ0MD
RE
Qjwvc3RFdnQ6aW5zdGFuY2VJRD4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OndoZW4+MjAxMy0w
Ni0wNVQxMTo1MzowOCswMjowMDwvc3RFdnQ6d2hlbj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0
OnNvZnR3YXJlQWdlbnQ+QWRvYmUgSWxsdXN0cmF0b3IgQ1M1PC9zdEV2dDpzb2Z0d2FyZUFnZW50
PgogICAgICAgICAgICAgICAgICA8c3RFdnQ6Y2hhbmdlZD4vPC9zdEV2dDpjaGFuZ2VkPgogICAg
ICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBl
PSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDphY3Rpb24+c2F2ZWQ8L3N0RXZ0
OmFjdGlvbj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0Omluc3RhbmNlSUQ+eG1wLmlpZDowMDgw
MTE3NDA3MjA2ODExODA4Mzk0RjNDNUQ0MDREQjwvc3RFdnQ6aW5zdGFuY2VJRD4KICAgICAgICAg
ICAgICAgICAgPHN0RXZ0OndoZW4+MjAxMy0wNi0wNVQxMTo1Nzo1MiswMjowMDwvc3RFdnQ6d2hl
bj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OnNvZnR3YXJlQWdlbnQ+QWRvYmUgSWxsdXN0cmF0
b3IgQ1M1PC9zdEV2dDpzb2Z0d2FyZUFnZW50PgogICAgICAgICAgICAgICAgICA8c3RFdnQ6Y2hh
bmdlZD4vPC9zdEV2dDpjaGFuZ2VkPgogICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAg
ICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAg
IDxzdEV2dDphY3Rpb24+c2F2ZWQ8L3N0RXZ0OmFjdGlvbj4KICAgICAgICAgICAgICAgICAgPHN0
RXZ0Omluc3RhbmNlSUQ+eG1wLmlpZDo2NDM1RkYwMzExMjA2ODExODA4Mzk0RjNDNUQ0MDREQj
wv
c3RFdnQ6aW5zdGFuY2VJRD4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OndoZW4+MjAxMy0wNi0w
NVQxMTo1Nzo1OSswMjowMDwvc3RFdnQ6d2hlbj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OnNv
ZnR3YXJlQWdlbnQ+QWRvYmUgSWxsdXN0cmF0b3IgQ1M1PC9zdEV2dDpzb2Z0d2FyZUFnZW50Pgog
ICAgICAgICAgICAgICAgICA8c3RFdnQ6Y2hhbmdlZD4vPC9zdEV2dDpjaGFuZ2VkPgogICAgICAg
ICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJS
ZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDphY3Rpb24+c2F2ZWQ8L3N0RXZ0OmFj
dGlvbj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0Omluc3RhbmNlSUQ+eG1wLmlpZDo2NTM1RkYw
MzExMjA2ODExODA4Mzk0RjNDNUQ0MDREQjwvc3RFdnQ6aW5zdGFuY2VJRD4KICAgICAgICAgICAg
ICAgICAgPHN0RXZ0OndoZW4+MjAxMy0wNi0wNVQxMjowMDowMyswMjowMDwvc3RFdnQ6d2hlbj
4K
ICAgICAgICAgICAgICAgICAgPHN0RXZ0OnNvZnR3YXJlQWdlbnQ+QWRvYmUgSWxsdXN0cmF0b3Ig
Q1M1PC9zdEV2dDpzb2Z0d2FyZUFnZW50PgogICAgICAgICAgICAgICAgICA8c3RFdnQ6Y2hhbmdl
ZD4vPC9zdEV2dDpjaGFuZ2VkPgogICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAg
ICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAgIDxz
dEV2dDphY3Rpb24+c2F2ZWQ8L3N0RXZ0OmFjdGlvbj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0
Omluc3RhbmNlSUQ+eG1wLmlpZDo2NjM1RkYwMzExMjA2ODExODA4Mzk0RjNDNUQ0MDREQjwvc3
RF
dnQ6aW5zdGFuY2VJRD4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OndoZW4+MjAxMy0wNi0wNVQx
MjowMDoxMSswMjowMDwvc3RFdnQ6d2hlbj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OnNvZnR3
YXJlQWdlbnQ+QWRvYmUgSWxsdXN0cmF0b3IgQ1M1PC9zdEV2dDpzb2Z0d2FyZUFnZW50PgogICAg
ICAgICAgICAgICAgICA8c3RFdnQ6Y2hhbmdlZD4vPC9zdEV2dDpjaGFuZ2VkPgogICAgICAgICAg
ICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNv
dXJjZSI+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDphY3Rpb24+Y29udmVydGVkPC9zdEV2dDph
Y3Rpb24+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDpwYXJhbWV0ZXJzPmZyb20gYXBwbGljYXRp
b24vcG9zdHNjcmlwdCB0byBhcHBsaWNhdGlvbi92bmQuYWRvYmUuaWxsdXN0cmF0b3I8L3N0RXZ0
OnBhcmFtZXRlcnM+CiAgICAgICAgICAgICAgIDwvcmRmOmxpPgogICAgICAgICAgICAgICA8cmRm
OmxpIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OmFj
dGlvbj5jb252ZXJ0ZWQ8L3N0RXZ0OmFjdGlvbj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OnBh
cmFtZXRlcnM+ZnJvbSBhcHBsaWNhdGlvbi9wb3N0c2NyaXB0IHRvIGFwcGxpY2F0aW9uL3ZuZC5h
ZG9iZS5pbGx1c3RyYXRvcjwvc3RFdnQ6cGFyYW1ldGVycz4KICAgICAgICAgICAgICAgPC9yZGY6
bGk+CiAgICAgICAgICAgICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAg
ICAgICAgICAgICAgICA8c3RFdnQ6YWN0aW9uPnNhdmVkPC9zdEV2dDphY3Rpb24+CiAgICAgICAg
ICAgICAgICAgIDxzdEV2dDppbnN0YW5jZUlEPnhtcC5paWQ6Rjc3RjExNzQwNzIwNjgxMTgyMkE5
MTA4ODg2RjEwRjM8L3N0RXZ0Omluc3RhbmNlSUQ+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDp3
aGVuPjIwMTMtMDctMDRUMDg6NTU6NTIrMDI6MDA8L3N0RXZ0OndoZW4+CiAgICAgICAgICAgICAg
ICAgIDxzdEV2dDpzb2Z0d2FyZUFnZW50PkFkb2JlIElsbHVzdHJhdG9yIENTNiAoTWFjaW50b3No
KTwvc3RFdnQ6c29mdHdhcmVBZ2VudD4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OmNoYW5nZWQ+
Lzwvc3RFdnQ6Y2hhbmdlZD4KICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAgICAgICAgICAg
IDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICA8c3RF
dnQ6YWN0aW9uPmNvbnZlcnRlZDwvc3RFdnQ6YWN0aW9uPgogICAgICAgICAgICAgICAgICA8c3RF
dnQ6cGFyYW1ldGVycz5mcm9tIGFwcGxpY2F0aW9uL3Bvc3RzY3JpcHQgdG8gYXBwbGljYXRpb24v
dm5kLmFkb2JlLmlsbHVzdHJhdG9yPC9zdEV2dDpwYXJhbWV0ZXJzPgogICAgICAgICAgICAgICA8
L3JkZjpsaT4KICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+
CiAgICAgICAgICAgICAgICAgIDxzdEV2dDphY3Rpb24+Y29udmVydGVkPC9zdEV2dDphY3Rpb24+
CiAgICAgICAgICAgICAgICAgIDxzdEV2dDpwYXJhbWV0ZXJzPmZyb20gYXBwbGljYXRpb24vcG9z
dHNjcmlwdCB0byBhcHBsaWNhdGlvbi92bmQuYWRvYmUuaWxsdXN0cmF0b3I8L3N0RXZ0OnBhcmFt
ZXRlcnM+CiAgICAgICAgICAgICAgIDwvcmRmOmxpPgogICAgICAgICAgICAgICA8cmRmOmxpIHJk
ZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OmFjdGlvbj5z
YXZlZDwvc3RFdnQ6YWN0aW9uPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6aW5zdGFuY2VJRD54
bXAuaWlkOkJEQTQ2RERFOTYyMDY4MTE4MjJBOTEwODg4NkYxMEYzPC9zdEV2dDppbnN0YW5jZUlE
PgogICAgICAgICAgICAgICAgICA8c3RFdnQ6d2hlbj4yMDEzLTA3LTA4VDEyOjQ4OjI3KzAyOjAw
PC9zdEV2dDp3aGVuPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6c29mdHdhcmVBZ2VudD5BZG9i
ZSBJbGx1c3RyYXRvciBDUzYgKE1hY2ludG9zaCk8L3N0RXZ0OnNvZnR3YXJlQWdlbnQ+CiAgICAg
ICAgICAgICAgICAgIDxzdEV2dDpjaGFuZ2VkPi88L3N0RXZ0OmNoYW5nZWQ+CiAgICAgICAgICAg
ICAgIDwvcmRmOmxpPgogICAgICAgICAgICAgICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9IlJlc291
cmNlIj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OmFjdGlvbj5jb252ZXJ0ZWQ8L3N0RXZ0OmFj
dGlvbj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OnBhcmFtZXRlcnM+ZnJvbSBhcHBsaWNhdGlv
bi9wb3N0c2NyaXB0IHRvIGFwcGxpY2F0aW9uL3ZuZC5hZG9iZS5pbGx1c3RyYXRvcjwvc3RFdnQ6
cGFyYW1ldGVycz4KICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAgICAgICAgICAgIDxyZGY6
bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6YWN0
aW9uPmNvbnZlcnRlZDwvc3RFdnQ6YWN0aW9uPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6cGFy
YW1ldGVycz5mcm9tIGFwcGxpY2F0aW9uL3Bvc3RzY3JpcHQgdG8gYXBwbGljYXRpb24vdm5kLmFk
b2JlLmlsbHVzdHJhdG9yPC9zdEV2dDpwYXJhbWV0ZXJzPgogICAgICAgICAgICAgICA8L3JkZjps
aT4KICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAg
ICAgICAgICAgICAgIDxzdEV2dDphY3Rpb24+Y29udmVydGVkPC9zdEV2dDphY3Rpb24+CiAgICAg
ICAgICAgICAgICAgIDxzdEV2dDpwYXJhbWV0ZXJzPmZyb20gYXBwbGljYXRpb24vcG9zdHNjcmlw
dCB0byBhcHBsaWNhdGlvbi92bmQuYWRvYmUuaWxsdXN0cmF0b3I8L3N0RXZ0OnBhcmFtZXRlcnM+
CiAgICAgICAgICAgICAgIDwvcmRmOmxpPgogICAgICAgICAgICAgICA8cmRmOmxpIHJkZjpwYXJz
ZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OmFjdGlvbj5zYXZlZDwv
c3RFdnQ6YWN0aW9uPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6aW5zdGFuY2VJRD54bXAuaWlk
OjA1ODAxMTc0MDcyMDY4MTE4MDgzODkzQ0Y5MkIzNTk2PC9zdEV2dDppbnN0YW5jZUlEPgogICAg
ICAgICAgICAgICAgICA8c3RFdnQ6d2hlbj4yMDEzLTA3LTIzVDEyOjE1OjQyKzAyOjAwPC9zdEV2
dDp3aGVuPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6c29mdHdhcmVBZ2VudD5BZG9iZSBJbGx1
c3RyYXRvciBDUzYgKE1hY2ludG9zaCk8L3N0RXZ0OnNvZnR3YXJlQWdlbnQ+CiAgICAgICAgICAg
ICAgICAgIDxzdEV2dDpjaGFuZ2VkPi88L3N0RXZ0OmNoYW5nZWQ+CiAgICAgICAgICAgICAgIDwv
cmRmOmxpPgogICAgICAgICAgICAgICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4K
ICAgICAgICAgICAgICAgICAgPHN0RXZ0OmFjdGlvbj5jb252ZXJ0ZWQ8L3N0RXZ0OmFjdGlvbj4K
ICAgICAgICAgICAgICAgICAgPHN0RXZ0OnBhcmFtZXRlcnM+ZnJvbSBhcHBsaWNhdGlvbi9wb3N0
c2NyaXB0IHRvIGFwcGxpY2F0aW9uL3ZuZC5hZG9iZS5pbGx1c3RyYXRvcjwvc3RFdnQ6cGFyYW1l
dGVycz4KICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAgICAgICAgICAgIDxyZGY6bGkgcmRm
OnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6YWN0aW9uPnNh
dmVkPC9zdEV2dDphY3Rpb24+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDppbnN0YW5jZUlEPnht
cC5paWQ6QTFCNjBFMTgwRTIwNjgxMTgwODM4OTNDRjkyQjM1OTY8L3N0RXZ0Omluc3RhbmNlSUQ
+
CiAgICAgICAgICAgICAgICAgIDxzdEV2dDp3aGVuPjIwMTMtMDctMjNUMTI6NTg6MjYrMDI6MDA8
L3N0RXZ0OndoZW4+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDpzb2Z0d2FyZUFnZW50PkFkb2Jl
IElsbHVzdHJhdG9yIENTNiAoTWFjaW50b3NoKTwvc3RFdnQ6c29mdHdhcmVBZ2VudD4KICAgICAg
ICAgICAgICAgICAgPHN0RXZ0OmNoYW5nZWQ+Lzwvc3RFdnQ6Y2hhbmdlZD4KICAgICAgICAgICAg
ICAgPC9yZGY6bGk+CiAgICAgICAgICAgICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3Vy
Y2UiPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6YWN0aW9uPmNvbnZlcnRlZDwvc3RFdnQ6YWN0
aW9uPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6cGFyYW1ldGVycz5mcm9tIGFwcGxpY2F0aW9u
L3Bvc3RzY3JpcHQgdG8gYXBwbGljYXRpb24vdm5kLmFkb2JlLmlsbHVzdHJhdG9yPC9zdEV2dDpw
YXJhbWV0ZXJzPgogICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAgPHJkZjps
aSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDphY3Rp
b24+c2F2ZWQ8L3N0RXZ0OmFjdGlvbj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0Omluc3RhbmNl
SUQ+eG1wLmlpZDpiZWFkOWEyMS0wOWJjLTQxOTgtYjJlYy01NTg2YzFmZTI1NGE8L3N0RXZ0Omlu
c3RhbmNlSUQ+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDp3aGVuPjIwMTQtMDMtMjZUMTE6MDI6
NTkrMDE6MDA8L3N0RXZ0OndoZW4+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDpzb2Z0d2FyZUFn
ZW50PkFkb2JlIElsbHVzdHJhdG9yIENDIChNYWNpbnRvc2gpPC9zdEV2dDpzb2Z0d2FyZUFnZW50
PgogICAgICAgICAgICAgICAgICA8c3RFdnQ6Y2hhbmdlZD4vPC9zdEV2dDpjaGFuZ2VkPgogICAg
ICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBl
PSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDphY3Rpb24+Y29udmVydGVkPC9z
dEV2dDphY3Rpb24+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDpwYXJhbWV0ZXJzPmZyb20gYXBw
bGljYXRpb24vcG9zdHNjcmlwdCB0byBhcHBsaWNhdGlvbi92bmQuYWRvYmUuaWxsdXN0cmF0b3I8
L3N0RXZ0OnBhcmFtZXRlcnM+CiAgICAgICAgICAgICAgIDwvcmRmOmxpPgogICAgICAgICAgICAg
ICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgICAgICAgPHN0
RXZ0OmFjdGlvbj5jb252ZXJ0ZWQ8L3N0RXZ0OmFjdGlvbj4KICAgICAgICAgICAgICAgICAgPHN0
RXZ0OnBhcmFtZXRlcnM+ZnJvbSBhcHBsaWNhdGlvbi9wb3N0c2NyaXB0IHRvIGFwcGxpY2F0aW9u
L3ZuZC5hZG9iZS5pbGx1c3RyYXRvcjwvc3RFdnQ6cGFyYW1ldGVycz4KICAgICAgICAgICAgICAg
PC9yZGY6bGk+CiAgICAgICAgICAgICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2Ui
PgogICAgICAgICAgICAgICAgICA8c3RFdnQ6YWN0aW9uPnNhdmVkPC9zdEV2dDphY3Rpb24+CiAg
ICAgICAgICAgICAgICAgIDxzdEV2dDppbnN0YW5jZUlEPnhtcC5paWQ6MGE2NmMxODYtYTk5OC00
ZWM2LTg4YTAtNzg4ODc1NjUwZDk2PC9zdEV2dDppbnN0YW5jZUlEPgogICAgICAgICAgICAgICAg
ICA8c3RFdnQ6d2hlbj4yMDE0LTA0LTA0VDA5OjE3OjU1KzAyOjAwPC9zdEV2dDp3aGVuPgogICAg
ICAgICAgICAgICAgICA8c3RFdnQ6c29mdHdhcmVBZ2VudD5BZG9iZSBJbGx1c3RyYXRvciBDQyAo
TWFjaW50b3NoKTwvc3RFdnQ6c29mdHdhcmVBZ2VudD4KICAgICAgICAgICAgICAgICAgPHN0RXZ0
OmNoYW5nZWQ+Lzwvc3RFdnQ6Y2hhbmdlZD4KICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAg
ICAgICAgICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAg
ICAgICA8c3RFdnQ6YWN0aW9uPnNhdmVkPC9zdEV2dDphY3Rpb24+CiAgICAgICAgICAgICAgICAg
IDxzdEV2dDppbnN0YW5jZUlEPnhtcC5paWQ6YjA3NmM2MmUtNWQzNS00NmQzLThmZTEtMmQ2OD
hk
MDg5YTFkPC9zdEV2dDppbnN0YW5jZUlEPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6d2hlbj4y
MDE0LTA0LTA0VDA5OjE4OjIzKzAyOjAwPC9zdEV2dDp3aGVuPgogICAgICAgICAgICAgICAgICA8
c3RFdnQ6c29mdHdhcmVBZ2VudD5BZG9iZSBJbGx1c3RyYXRvciBDQyAoTWFjaW50b3NoKTwvc3RF
dnQ6c29mdHdhcmVBZ2VudD4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OmNoYW5nZWQ+Lzwvc3RF
dnQ6Y2hhbmdlZD4KICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAgICAgICAgICAgIDxyZGY6
bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6YWN0
aW9uPmNvbnZlcnRlZDwvc3RFdnQ6YWN0aW9uPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6cGFy
YW1ldGVycz5mcm9tIGFwcGxpY2F0aW9uL3Bvc3RzY3JpcHQgdG8gYXBwbGljYXRpb24vdm5kLmFk
b2JlLmlsbHVzdHJhdG9yPC9zdEV2dDpwYXJhbWV0ZXJzPgogICAgICAgICAgICAgICA8L3JkZjps
aT4KICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAg
ICAgICAgICAgICAgIDxzdEV2dDphY3Rpb24+Y29udmVydGVkPC9zdEV2dDphY3Rpb24+CiAgICAg
ICAgICAgICAgICAgIDxzdEV2dDpwYXJhbWV0ZXJzPmZyb20gYXBwbGljYXRpb24vcG9zdHNjcmlw
dCB0byBhcHBsaWNhdGlvbi92bmQuYWRvYmUucGhvdG9zaG9wPC9zdEV2dDpwYXJhbWV0ZXJzPgog
ICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VU
eXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDphY3Rpb24+c2F2ZWQ8L3N0
RXZ0OmFjdGlvbj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0Omluc3RhbmNlSUQ+eG1wLmlpZDpi
ZDczNzBiYS1lNmM1LTRlZTctOGM3YS02NzZmMjc0OTYyMjA8L3N0RXZ0Omluc3RhbmNlSUQ+CiAg
ICAgICAgICAgICAgICAgIDxzdEV2dDp3aGVuPjIwMTQtMDQtMDRUMDk6MjA6MTYrMDI6MDA8L3N0
RXZ0OndoZW4+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDpzb2Z0d2FyZUFnZW50PkFkb2JlIFBo
b3Rvc2hvcCBDQyAoTWFjaW50b3NoKTwvc3RFdnQ6c29mdHdhcmVBZ2VudD4KICAgICAgICAgICAg
ICAgICAgPHN0RXZ0OmNoYW5nZWQ+Lzwvc3RFdnQ6Y2hhbmdlZD4KICAgICAgICAgICAgICAgPC9y
ZGY6bGk+CiAgICAgICAgICAgICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgog
ICAgICAgICAgICAgICAgICA8c3RFdnQ6YWN0aW9uPmNvbnZlcnRlZDwvc3RFdnQ6YWN0aW9uPgog
ICAgICAgICAgICAgICAgICA8c3RFdnQ6cGFyYW1ldGVycz5mcm9tIGFwcGxpY2F0aW9uL3Bvc3Rz
Y3JpcHQgdG8gaW1hZ2UvcG5nPC9zdEV2dDpwYXJhbWV0ZXJzPgogICAgICAgICAgICAgICA8L3Jk
ZjpsaT4KICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAg
ICAgICAgICAgICAgICAgIDxzdEV2dDphY3Rpb24+ZGVyaXZlZDwvc3RFdnQ6YWN0aW9uPgogICAg
ICAgICAgICAgICAgICA8c3RFdnQ6cGFyYW1ldGVycz5jb252ZXJ0ZWQgZnJvbSBhcHBsaWNhdGlv
bi92bmQuYWRvYmUucGhvdG9zaG9wIHRvIGltYWdlL3BuZzwvc3RFdnQ6cGFyYW1ldGVycz4KICAg
ICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAgICAgICAgICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlw
ZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6YWN0aW9uPnNhdmVkPC9zdEV2
dDphY3Rpb24+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDppbnN0YW5jZUlEPnhtcC5paWQ6MzRj
YjZjODMtMTc0Ny00OGNlLWI1Y2UtMzNhZjk3NTU5ZDQ2PC9zdEV2dDppbnN0YW5jZUlEPgogICAg
ICAgICAgICAgICAgICA8c3RFdnQ6d2hlbj4yMDE0LTA0LTA0VDA5OjIwOjE2KzAyOjAwPC9zdEV2
dDp3aGVuPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6c29mdHdhcmVBZ2VudD5BZG9iZSBQaG90
b3Nob3AgQ0MgKE1hY2ludG9zaCk8L3N0RXZ0OnNvZnR3YXJlQWdlbnQ+CiAgICAgICAgICAgICAg
ICAgIDxzdEV2dDpjaGFuZ2VkPi88L3N0RXZ0OmNoYW5nZWQ+CiAgICAgICAgICAgICAgIDwvcmRm
OmxpPgogICAgICAgICAgICAgICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAg
ICAgICAgICAgICAgICAgPHN0RXZ0OmFjdGlvbj5zYXZlZDwvc3RFdnQ6YWN0aW9uPgogICAgICAg
ICAgICAgICAgICA8c3RFdnQ6aW5zdGFuY2VJRD54bXAuaWlkOmMwNDYyYWNlLTVkNmItNDA2MS1i
MzAwLTViMmE5YzdjYmQzMjwvc3RFdnQ6aW5zdGFuY2VJRD4KICAgICAgICAgICAgICAgICAgPHN0
RXZ0OndoZW4+MjAxNi0wMi0yNFQxMDowMTozOSswMTowMDwvc3RFdnQ6d2hlbj4KICAgICAgICAg
ICAgICAgICAgPHN0RXZ0OnNvZnR3YXJlQWdlbnQ+QWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1h
Y2ludG9zaCk8L3N0RXZ0OnNvZnR3YXJlQWdlbnQ+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDpj
aGFuZ2VkPi88L3N0RXZ0OmNoYW5nZWQ+CiAgICAgICAgICAgICAgIDwvcmRmOmxpPgogICAgICAg
ICAgICA8L3JkZjpTZXE+CiAgICAgICAgIDwveG1wTU06SGlzdG9yeT4KICAgICAgICAgPGlsbHVz
dHJhdG9yOlN0YXJ0dXBQcm9maWxlPlByaW50PC9pbGx1c3RyYXRvcjpTdGFydHVwUHJvZmlsZT4K
ICAgICAgICAgPHhtcFRQZzpIYXNWaXNpYmxlT3ZlcnByaW50PkZhbHNlPC94bXBUUGc6SGFzVmlz
aWJsZU92ZXJwcmludD4KICAgICAgICAgPHhtcFRQZzpIYXNWaXNpYmxlVHJhbnNwYXJlbmN5PkZh
bHNlPC94bXBUUGc6SGFzVmlzaWJsZVRyYW5zcGFyZW5jeT4KICAgICAgICAgPHhtcFRQZzpOUGFn
ZXM+MTwveG1wVFBnOk5QYWdlcz4KICAgICAgICAgPHhtcFRQZzpNYXhQYWdlU2l6ZSByZGY6cGFy
c2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgIDxzdERpbTp3Pjc5LjY3OTIzNDwvc3REaW06
dz4KICAgICAgICAgICAgPHN0RGltOmg+NDguMzU0ODI3PC9zdERpbTpoPgogICAgICAgICAgICA8
c3REaW06dW5pdD5NaWxsaW1ldGVyczwvc3REaW06dW5pdD4KICAgICAgICAgPC94bXBUUGc6TWF4
UGFnZVNpemU+CiAgICAgICAgIDx4bXBUUGc6UGxhdGVOYW1lcz4KICAgICAgICAgICAgPHJkZjpT
ZXE+CiAgICAgICAgICAgICAgIDxyZGY6bGk+TWFnZW50YTwvcmRmOmxpPgogICAgICAgICAgICAg
ICA8cmRmOmxpPlllbGxvdzwvcmRmOmxpPgogICAgICAgICAgICA8L3JkZjpTZXE+CiAgICAgICAg
IDwveG1wVFBnOlBsYXRlTmFtZXM+CiAgICAgICAgIDx4bXBUUGc6U3dhdGNoR3JvdXBzPgogICAg
ICAgICAgICA8cmRmOlNlcT4KICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJS
ZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAgIDx4bXBHOmdyb3VwTmFtZT5TdGFuZGFhcmRzdGFh
bGdyb2VwPC94bXBHOmdyb3VwTmFtZT4KICAgICAgICAgICAgICAgICAgPHhtcEc6Z3JvdXBUeXBl
PjA8L3htcEc6Z3JvdXBUeXBlPgogICAgICAgICAgICAgICAgICA8eG1wRzpDb2xvcmFudHM+CiAg
ICAgICAgICAgICAgICAgICAgIDxyZGY6U2VxPgogICAgICAgICAgICAgICAgICAgICAgICA8cmRm
OmxpIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgICAgICAgICAgICAgICAg
PHhtcEc6c3dhdGNoTmFtZT5XaXQ8L3htcEc6c3dhdGNoTmFtZT4KICAgICAgICAgICAgICAgICAg
ICAgICAgICAgPHhtcEc6bW9kZT5DTVlLPC94bXBHOm1vZGU+CiAgICAgICAgICAgICAgICAgICAg
ICAgICAgIDx4bXBHOnR5cGU+UFJPQ0VTUzwveG1wRzp0eXBlPgogICAgICAgICAgICAgICAgICAg
ICAgICAgICA8eG1wRzpjeWFuPjAuMDAwMDAwPC94bXBHOmN5YW4+CiAgICAgICAgICAgICAgICAg
ICAgICAgICAgIDx4bXBHOm1hZ2VudGE+MC4wMDAwMDA8L3htcEc6bWFnZW50YT4KICAgICAgICAg
ICAgICAgICAgICAgICAgICAgPHhtcEc6eWVsbG93PjAuMDAwMDAwPC94bXBHOnllbGxvdz4KICAg
ICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6YmxhY2s+MC4wMDAwMDA8L3htcEc6YmxhY2s+
CiAgICAgICAgICAgICAgICAgICAgICAgIDwvcmRmOmxpPgogICAgICAgICAgICAgICAgICAgICAg
ICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgICAgICAgICAg
ICAgICAgPHhtcEc6c3dhdGNoTmFtZT5ad2FydDwveG1wRzpzd2F0Y2hOYW1lPgogICAgICAgICAg
ICAgICAgICAgICAgICAgICA8eG1wRzptb2RlPkNNWUs8L3htcEc6bW9kZT4KICAgICAgICAgICAg
ICAgICAgICAgICAgICAgPHhtcEc6dHlwZT5QUk9DRVNTPC94bXBHOnR5cGU+CiAgICAgICAgICAg
ICAgICAgICAgICAgICAgIDx4bXBHOmN5YW4+MC4wMDAwMDA8L3htcEc6Y3lhbj4KICAgICAgICAg
ICAgICAgICAgICAgICAgICAgPHhtcEc6bWFnZW50YT4wLjAwMDAwMDwveG1wRzptYWdlbnRhPgog
ICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp5ZWxsb3c+MC4wMDAwMDA8L3htcEc6eWVs
bG93PgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpibGFjaz4xMDAuMDAwMDAwPC94
bXBHOmJsYWNrPgogICAgICAgICAgICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAg
ICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAg
ICAgICAgICAgICAgICAgIDx4bXBHOnN3YXRjaE5hbWU+Q01ZSy1yb29kPC94bXBHOnN3YXRjaE5h
bWU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOm1vZGU+Q01ZSzwveG1wRzptb2Rl
PgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp0eXBlPlBST0NFU1M8L3htcEc6dHlw
ZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6Y3lhbj4wLjAwMDAwMDwveG1wRzpj
eWFuPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzptYWdlbnRhPjEwMC4wMDAwMDA8
L3htcEc6bWFnZW50YT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6eWVsbG93PjEw
MC4wMDAwMDA8L3htcEc6eWVsbG93PgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpi
bGFjaz4wLjAwMDAwMDwveG1wRzpibGFjaz4KICAgICAgICAgICAgICAgICAgICAgICAgPC9yZGY6
bGk+CiAgICAgICAgICAgICAgICAgICAgICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3Vy
Y2UiPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpzd2F0Y2hOYW1lPkNNWUstZ2Vl
bDwveG1wRzpzd2F0Y2hOYW1lPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzptb2Rl
PkNNWUs8L3htcEc6bW9kZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6dHlwZT5Q
Uk9DRVNTPC94bXBHOnR5cGU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOmN5YW4+
MC4wMDAwMDA8L3htcEc6Y3lhbj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6bWFn
ZW50YT4wLjAwMDAwMDwveG1wRzptYWdlbnRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8
eG1wRzp5ZWxsb3c+MTAwLjAwMDAwMDwveG1wRzp5ZWxsb3c+CiAgICAgICAgICAgICAgICAgICAg
ICAgICAgIDx4bXBHOmJsYWNrPjAuMDAwMDAwPC94bXBHOmJsYWNrPgogICAgICAgICAgICAgICAg
ICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFy
c2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnN3YXRj
aE5hbWU+Q01ZSy1ncm9lbjwveG1wRzpzd2F0Y2hOYW1lPgogICAgICAgICAgICAgICAgICAgICAg
ICAgICA8eG1wRzptb2RlPkNNWUs8L3htcEc6bW9kZT4KICAgICAgICAgICAgICAgICAgICAgICAg
ICAgPHhtcEc6dHlwZT5QUk9DRVNTPC94bXBHOnR5cGU+CiAgICAgICAgICAgICAgICAgICAgICAg
ICAgIDx4bXBHOmN5YW4+MTAwLjAwMDAwMDwveG1wRzpjeWFuPgogICAgICAgICAgICAgICAgICAg
ICAgICAgICA8eG1wRzptYWdlbnRhPjAuMDAwMDAwPC94bXBHOm1hZ2VudGE+CiAgICAgICAgICAg
ICAgICAgICAgICAgICAgIDx4bXBHOnllbGxvdz4xMDAuMDAwMDAwPC94bXBHOnllbGxvdz4KICAg
ICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6YmxhY2s+MC4wMDAwMDA8L3htcEc6YmxhY2s+
CiAgICAgICAgICAgICAgICAgICAgICAgIDwvcmRmOmxpPgogICAgICAgICAgICAgICAgICAgICAg
ICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgICAgICAgICAg
ICAgICAgPHhtcEc6c3dhdGNoTmFtZT5DTVlLLWN5YWFuPC94bXBHOnN3YXRjaE5hbWU+CiAgICAg
ICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOm1vZGU+Q01ZSzwveG1wRzptb2RlPgogICAgICAg
ICAgICAgICAgICAgICAgICAgICA8eG1wRzp0eXBlPlBST0NFU1M8L3htcEc6dHlwZT4KICAgICAg
ICAgICAgICAgICAgICAgICAgICAgPHhtcEc6Y3lhbj4xMDAuMDAwMDAwPC94bXBHOmN5YW4+CiAg
ICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOm1hZ2VudGE+MC4wMDAwMDA8L3htcEc6bWFn
ZW50YT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6eWVsbG93PjAuMDAwMDAwPC94
bXBHOnllbGxvdz4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6YmxhY2s+MC4wMDAw
MDA8L3htcEc6YmxhY2s+CiAgICAgICAgICAgICAgICAgICAgICAgIDwvcmRmOmxpPgogICAgICAg
ICAgICAgICAgICAgICAgICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAg
ICAgICAgICAgICAgICAgICAgICAgPHhtcEc6c3dhdGNoTmFtZT5DTVlLLWJsYXV3PC94bXBHOnN3
YXRjaE5hbWU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOm1vZGU+Q01ZSzwveG1w
Rzptb2RlPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp0eXBlPlBST0NFU1M8L3ht
cEc6dHlwZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6Y3lhbj4xMDAuMDAwMDAw
PC94bXBHOmN5YW4+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOm1hZ2VudGE+MTAw
LjAwMDAwMDwveG1wRzptYWdlbnRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp5
ZWxsb3c+MC4wMDAwMDA8L3htcEc6eWVsbG93PgogICAgICAgICAgICAgICAgICAgICAgICAgICA8
eG1wRzpibGFjaz4wLjAwMDAwMDwveG1wRzpibGFjaz4KICAgICAgICAgICAgICAgICAgICAgICAg
PC9yZGY6bGk+CiAgICAgICAgICAgICAgICAgICAgICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0i
UmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpzd2F0Y2hOYW1lPkNN
WUstbWFnZW50YTwveG1wRzpzd2F0Y2hOYW1lPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8
eG1wRzptb2RlPkNNWUs8L3htcEc6bW9kZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHht
cEc6dHlwZT5QUk9DRVNTPC94bXBHOnR5cGU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4
bXBHOmN5YW4+MC4wMDAwMDA8L3htcEc6Y3lhbj4KICAgICAgICAgICAgICAgICAgICAgICAgICAg
PHhtcEc6bWFnZW50YT4xMDAuMDAwMDAwPC94bXBHOm1hZ2VudGE+CiAgICAgICAgICAgICAgICAg
ICAgICAgICAgIDx4bXBHOnllbGxvdz4wLjAwMDAwMDwveG1wRzp5ZWxsb3c+CiAgICAgICAgICAg
ICAgICAgICAgICAgICAgIDx4bXBHOmJsYWNrPjAuMDAwMDAwPC94bXBHOmJsYWNrPgogICAgICAg
ICAgICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAgICAgICAgICAgPHJkZjps
aSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4
bXBHOnN3YXRjaE5hbWU+Qz0xNSBNPTEwMCBZPTkwIEs9MTA8L3htcEc6c3dhdGNoTmFtZT4KICAg
ICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6bW9kZT5DTVlLPC94bXBHOm1vZGU+CiAgICAg
ICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnR5cGU+UFJPQ0VTUzwveG1wRzp0eXBlPgogICAg
ICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpjeWFuPjE1LjAwMDAwMDwveG1wRzpjeWFuPgog
ICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzptYWdlbnRhPjEwMC4wMDAwMDA8L3htcEc6
bWFnZW50YT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6eWVsbG93PjkwLjAwMDAw
MDwveG1wRzp5ZWxsb3c+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOmJsYWNrPjEw
LjAwMDAwMDwveG1wRzpibGFjaz4KICAgICAgICAgICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAg
ICAgICAgICAgICAgICAgICAgICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgog
ICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpzd2F0Y2hOYW1lPkM9MCBNPTkwIFk9ODUg
Sz0wPC94bXBHOnN3YXRjaE5hbWU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOm1v
ZGU+Q01ZSzwveG1wRzptb2RlPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp0eXBl
PlBST0NFU1M8L3htcEc6dHlwZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6Y3lh
bj4wLjAwMDAwMDwveG1wRzpjeWFuPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpt
YWdlbnRhPjkwLjAwMDAwMDwveG1wRzptYWdlbnRhPgogICAgICAgICAgICAgICAgICAgICAgICAg
ICA8eG1wRzp5ZWxsb3c+ODUuMDAwMDAwPC94bXBHOnllbGxvdz4KICAgICAgICAgICAgICAgICAg
ICAgICAgICAgPHhtcEc6YmxhY2s+MC4wMDAwMDA8L3htcEc6YmxhY2s+CiAgICAgICAgICAgICAg
ICAgICAgICAgIDwvcmRmOmxpPgogICAgICAgICAgICAgICAgICAgICAgICA8cmRmOmxpIHJkZjpw
YXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6c3dh
dGNoTmFtZT5DPTAgTT04MCBZPTk1IEs9MDwveG1wRzpzd2F0Y2hOYW1lPgogICAgICAgICAgICAg
ICAgICAgICAgICAgICA8eG1wRzptb2RlPkNNWUs8L3htcEc6bW9kZT4KICAgICAgICAgICAgICAg
ICAgICAgICAgICAgPHhtcEc6dHlwZT5QUk9DRVNTPC94bXBHOnR5cGU+CiAgICAgICAgICAgICAg
ICAgICAgICAgICAgIDx4bXBHOmN5YW4+MC4wMDAwMDA8L3htcEc6Y3lhbj4KICAgICAgICAgICAg
ICAgICAgICAgICAgICAgPHhtcEc6bWFnZW50YT44MC4wMDAwMDA8L3htcEc6bWFnZW50YT4KICAg
ICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6eWVsbG93Pjk1LjAwMDAwMDwveG1wRzp5ZWxs
b3c+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOmJsYWNrPjAuMDAwMDAwPC94bXBH
OmJsYWNrPgogICAgICAgICAgICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAg
ICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAg
ICAgICAgICAgICAgIDx4bXBHOnN3YXRjaE5hbWU+Qz0wIE09NTAgWT0xMDAgSz0wPC94bXBHOnN3
YXRjaE5hbWU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOm1vZGU+Q01ZSzwveG1w
Rzptb2RlPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp0eXBlPlBST0NFU1M8L3ht
cEc6dHlwZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6Y3lhbj4wLjAwMDAwMDwv
eG1wRzpjeWFuPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzptYWdlbnRhPjUwLjAw
MDAwMDwveG1wRzptYWdlbnRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp5ZWxs
b3c+MTAwLjAwMDAwMDwveG1wRzp5ZWxsb3c+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4
bXBHOmJsYWNrPjAuMDAwMDAwPC94bXBHOmJsYWNrPgogICAgICAgICAgICAgICAgICAgICAgICA8
L3JkZjpsaT4KICAgICAgICAgICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJS
ZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnN3YXRjaE5hbWU+Qz0w
IE09MzUgWT04NSBLPTA8L3htcEc6c3dhdGNoTmFtZT4KICAgICAgICAgICAgICAgICAgICAgICAg
ICAgPHhtcEc6bW9kZT5DTVlLPC94bXBHOm1vZGU+CiAgICAgICAgICAgICAgICAgICAgICAgICAg
IDx4bXBHOnR5cGU+UFJPQ0VTUzwveG1wRzp0eXBlPgogICAgICAgICAgICAgICAgICAgICAgICAg
ICA8eG1wRzpjeWFuPjAuMDAwMDAwPC94bXBHOmN5YW4+CiAgICAgICAgICAgICAgICAgICAgICAg
ICAgIDx4bXBHOm1hZ2VudGE+MzUuMDAwMDAwPC94bXBHOm1hZ2VudGE+CiAgICAgICAgICAgICAg
ICAgICAgICAgICAgIDx4bXBHOnllbGxvdz44NS4wMDAwMDA8L3htcEc6eWVsbG93PgogICAgICAg
ICAgICAgICAgICAgICAgICAgICA8eG1wRzpibGFjaz4wLjAwMDAwMDwveG1wRzpibGFjaz4KICAg
ICAgICAgICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAgICAgICAgICAgICAgICAgICAgIDxy
ZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICAgICAgICAg
ICA8eG1wRzpzd2F0Y2hOYW1lPkM9NSBNPTAgWT05MCBLPTA8L3htcEc6c3dhdGNoTmFtZT4KICAg
ICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6bW9kZT5DTVlLPC94bXBHOm1vZGU+CiAgICAg
ICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnR5cGU+UFJPQ0VTUzwveG1wRzp0eXBlPgogICAg
ICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpjeWFuPjUuMDAwMDAwPC94bXBHOmN5YW4+CiAg
ICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOm1hZ2VudGE+MC4wMDAwMDA8L3htcEc6bWFn
ZW50YT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6eWVsbG93PjkwLjAwMDAwMDwv
eG1wRzp5ZWxsb3c+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOmJsYWNrPjAuMDAw
MDAwPC94bXBHOmJsYWNrPgogICAgICAgICAgICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAg
ICAgICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAg
ICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnN3YXRjaE5hbWU+Qz0yMCBNPTAgWT0xMDAgSz0w
PC94bXBHOnN3YXRjaE5hbWU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOm1vZGU+
Q01ZSzwveG1wRzptb2RlPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp0eXBlPlBS
T0NFU1M8L3htcEc6dHlwZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6Y3lhbj4y
MC4wMDAwMDA8L3htcEc6Y3lhbj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6bWFn
ZW50YT4wLjAwMDAwMDwveG1wRzptYWdlbnRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8
eG1wRzp5ZWxsb3c+MTAwLjAwMDAwMDwveG1wRzp5ZWxsb3c+CiAgICAgICAgICAgICAgICAgICAg
ICAgICAgIDx4bXBHOmJsYWNrPjAuMDAwMDAwPC94bXBHOmJsYWNrPgogICAgICAgICAgICAgICAg
ICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFy
c2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnN3YXRj
aE5hbWU+Qz01MCBNPTAgWT0xMDAgSz0wPC94bXBHOnN3YXRjaE5hbWU+CiAgICAgICAgICAgICAg
ICAgICAgICAgICAgIDx4bXBHOm1vZGU+Q01ZSzwveG1wRzptb2RlPgogICAgICAgICAgICAgICAg
ICAgICAgICAgICA8eG1wRzp0eXBlPlBST0NFU1M8L3htcEc6dHlwZT4KICAgICAgICAgICAgICAg
ICAgICAgICAgICAgPHhtcEc6Y3lhbj41MC4wMDAwMDA8L3htcEc6Y3lhbj4KICAgICAgICAgICAg
ICAgICAgICAgICAgICAgPHhtcEc6bWFnZW50YT4wLjAwMDAwMDwveG1wRzptYWdlbnRhPgogICAg
ICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp5ZWxsb3c+MTAwLjAwMDAwMDwveG1wRzp5ZWxs
b3c+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOmJsYWNrPjAuMDAwMDAwPC94bXBH
OmJsYWNrPgogICAgICAgICAgICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAg
ICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAg
ICAgICAgICAgICAgIDx4bXBHOnN3YXRjaE5hbWU+Qz03NSBNPTAgWT0xMDAgSz0wPC94bXBHOnN3
YXRjaE5hbWU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOm1vZGU+Q01ZSzwveG1w
Rzptb2RlPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp0eXBlPlBST0NFU1M8L3ht
cEc6dHlwZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6Y3lhbj43NS4wMDAwMDA8
L3htcEc6Y3lhbj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6bWFnZW50YT4wLjAw
MDAwMDwveG1wRzptYWdlbnRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp5ZWxs
b3c+MTAwLjAwMDAwMDwveG1wRzp5ZWxsb3c+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4
bXBHOmJsYWNrPjAuMDAwMDAwPC94bXBHOmJsYWNrPgogICAgICAgICAgICAgICAgICAgICAgICA8
L3JkZjpsaT4KICAgICAgICAgICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJS
ZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnN3YXRjaE5hbWU+Qz04
NSBNPTEwIFk9MTAwIEs9MTA8L3htcEc6c3dhdGNoTmFtZT4KICAgICAgICAgICAgICAgICAgICAg
ICAgICAgPHhtcEc6bW9kZT5DTVlLPC94bXBHOm1vZGU+CiAgICAgICAgICAgICAgICAgICAgICAg
ICAgIDx4bXBHOnR5cGU+UFJPQ0VTUzwveG1wRzp0eXBlPgogICAgICAgICAgICAgICAgICAgICAg
ICAgICA8eG1wRzpjeWFuPjg1LjAwMDAwMDwveG1wRzpjeWFuPgogICAgICAgICAgICAgICAgICAg
ICAgICAgICA8eG1wRzptYWdlbnRhPjEwLjAwMDAwMDwveG1wRzptYWdlbnRhPgogICAgICAgICAg
ICAgICAgICAgICAgICAgICA8eG1wRzp5ZWxsb3c+MTAwLjAwMDAwMDwveG1wRzp5ZWxsb3c+CiAg
ICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOmJsYWNrPjEwLjAwMDAwMDwveG1wRzpibGFj
az4KICAgICAgICAgICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAgICAgICAgICAgICAgICAg
ICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICAg
ICAgICAgICA8eG1wRzpzd2F0Y2hOYW1lPkM9OTAgTT0zMCBZPTk1IEs9MzA8L3htcEc6c3dhdGNo
TmFtZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6bW9kZT5DTVlLPC94bXBHOm1v
ZGU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnR5cGU+UFJPQ0VTUzwveG1wRzp0
eXBlPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpjeWFuPjkwLjAwMDAwMDwveG1w
RzpjeWFuPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzptYWdlbnRhPjMwLjAwMDAw
MDwveG1wRzptYWdlbnRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp5ZWxsb3c+
OTUuMDAwMDAwPC94bXBHOnllbGxvdz4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6
YmxhY2s+MzAuMDAwMDAwPC94bXBHOmJsYWNrPgogICAgICAgICAgICAgICAgICAgICAgICA8L3Jk
ZjpsaT4KICAgICAgICAgICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNv
dXJjZSI+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnN3YXRjaE5hbWU+Qz03NSBN
PTAgWT03NSBLPTA8L3htcEc6c3dhdGNoTmFtZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAg
PHhtcEc6bW9kZT5DTVlLPC94bXBHOm1vZGU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4
bXBHOnR5cGU+UFJPQ0VTUzwveG1wRzp0eXBlPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8
eG1wRzpjeWFuPjc1LjAwMDAwMDwveG1wRzpjeWFuPgogICAgICAgICAgICAgICAgICAgICAgICAg
ICA8eG1wRzptYWdlbnRhPjAuMDAwMDAwPC94bXBHOm1hZ2VudGE+CiAgICAgICAgICAgICAgICAg
ICAgICAgICAgIDx4bXBHOnllbGxvdz43NS4wMDAwMDA8L3htcEc6eWVsbG93PgogICAgICAgICAg
ICAgICAgICAgICAgICAgICA8eG1wRzpibGFjaz4wLjAwMDAwMDwveG1wRzpibGFjaz4KICAgICAg
ICAgICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAgICAgICAgICAgICAgICAgICAgIDxyZGY6
bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8
eG1wRzpzd2F0Y2hOYW1lPkM9ODAgTT0xMCBZPTQ1IEs9MDwveG1wRzpzd2F0Y2hOYW1lPgogICAg
ICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzptb2RlPkNNWUs8L3htcEc6bW9kZT4KICAgICAg
ICAgICAgICAgICAgICAgICAgICAgPHhtcEc6dHlwZT5QUk9DRVNTPC94bXBHOnR5cGU+CiAgICAg
ICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOmN5YW4+ODAuMDAwMDAwPC94bXBHOmN5YW4+CiAg
ICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOm1hZ2VudGE+MTAuMDAwMDAwPC94bXBHOm1h
Z2VudGE+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnllbGxvdz40NS4wMDAwMDA8
L3htcEc6eWVsbG93PgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpibGFjaz4wLjAw
MDAwMDwveG1wRzpibGFjaz4KICAgICAgICAgICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAg
ICAgICAgICAgICAgICAgICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAg
ICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpzd2F0Y2hOYW1lPkM9NzAgTT0xNSBZPTAgSz0w
PC94bXBHOnN3YXRjaE5hbWU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOm1vZGU+
Q01ZSzwveG1wRzptb2RlPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp0eXBlPlBS
T0NFU1M8L3htcEc6dHlwZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6Y3lhbj43
MC4wMDAwMDA8L3htcEc6Y3lhbj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6bWFn
ZW50YT4xNS4wMDAwMDA8L3htcEc6bWFnZW50YT4KICAgICAgICAgICAgICAgICAgICAgICAgICAg
PHhtcEc6eWVsbG93PjAuMDAwMDAwPC94bXBHOnllbGxvdz4KICAgICAgICAgICAgICAgICAgICAg
ICAgICAgPHhtcEc6YmxhY2s+MC4wMDAwMDA8L3htcEc6YmxhY2s+CiAgICAgICAgICAgICAgICAg
ICAgICAgIDwvcmRmOmxpPgogICAgICAgICAgICAgICAgICAgICAgICA8cmRmOmxpIHJkZjpwYXJz
ZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6c3dhdGNo
TmFtZT5DPTg1IE09NTAgWT0wIEs9MDwveG1wRzpzd2F0Y2hOYW1lPgogICAgICAgICAgICAgICAg
ICAgICAgICAgICA8eG1wRzptb2RlPkNNWUs8L3htcEc6bW9kZT4KICAgICAgICAgICAgICAgICAg
ICAgICAgICAgPHhtcEc6dHlwZT5QUk9DRVNTPC94bXBHOnR5cGU+CiAgICAgICAgICAgICAgICAg
ICAgICAgICAgIDx4bXBHOmN5YW4+ODUuMDAwMDAwPC94bXBHOmN5YW4+CiAgICAgICAgICAgICAg
ICAgICAgICAgICAgIDx4bXBHOm1hZ2VudGE+NTAuMDAwMDAwPC94bXBHOm1hZ2VudGE+CiAgICAg
ICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnllbGxvdz4wLjAwMDAwMDwveG1wRzp5ZWxsb3c+
CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOmJsYWNrPjAuMDAwMDAwPC94bXBHOmJs
YWNrPgogICAgICAgICAgICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAgICAg
ICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAg
ICAgICAgICAgIDx4bXBHOnN3YXRjaE5hbWU+Qz0xMDAgTT05NSBZPTUgSz0wPC94bXBHOnN3YXRj
aE5hbWU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOm1vZGU+Q01ZSzwveG1wRzpt
b2RlPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp0eXBlPlBST0NFU1M8L3htcEc6
dHlwZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6Y3lhbj4xMDAuMDAwMDAwPC94
bXBHOmN5YW4+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOm1hZ2VudGE+OTUuMDAw
MDAwPC94bXBHOm1hZ2VudGE+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnllbGxv
dz41LjAwMDAwMDwveG1wRzp5ZWxsb3c+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBH
OmJsYWNrPjAuMDAwMDAwPC94bXBHOmJsYWNrPgogICAgICAgICAgICAgICAgICAgICAgICA8L3Jk
ZjpsaT4KICAgICAgICAgICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNv
dXJjZSI+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnN3YXRjaE5hbWU+Qz0xMDAg
TT0xMDAgWT0yNSBLPTI1PC94bXBHOnN3YXRjaE5hbWU+CiAgICAgICAgICAgICAgICAgICAgICAg
ICAgIDx4bXBHOm1vZGU+Q01ZSzwveG1wRzptb2RlPgogICAgICAgICAgICAgICAgICAgICAgICAg
ICA8eG1wRzp0eXBlPlBST0NFU1M8L3htcEc6dHlwZT4KICAgICAgICAgICAgICAgICAgICAgICAg
ICAgPHhtcEc6Y3lhbj4xMDAuMDAwMDAwPC94bXBHOmN5YW4+CiAgICAgICAgICAgICAgICAgICAg
ICAgICAgIDx4bXBHOm1hZ2VudGE+MTAwLjAwMDAwMDwveG1wRzptYWdlbnRhPgogICAgICAgICAg
ICAgICAgICAgICAgICAgICA8eG1wRzp5ZWxsb3c+MjUuMDAwMDAwPC94bXBHOnllbGxvdz4KICAg
ICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6YmxhY2s+MjUuMDAwMDAwPC94bXBHOmJsYWNr
PgogICAgICAgICAgICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAgICAgICAg
ICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAgICAg
ICAgICAgIDx4bXBHOnN3YXRjaE5hbWU+Qz03NSBNPTEwMCBZPTAgSz0wPC94bXBHOnN3YXRjaE5h
bWU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOm1vZGU+Q01ZSzwveG1wRzptb2Rl
PgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp0eXBlPlBST0NFU1M8L3htcEc6dHlw
ZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6Y3lhbj43NS4wMDAwMDA8L3htcEc6
Y3lhbj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6bWFnZW50YT4xMDAuMDAwMDAw
PC94bXBHOm1hZ2VudGE+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnllbGxvdz4w
LjAwMDAwMDwveG1wRzp5ZWxsb3c+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOmJs
YWNrPjAuMDAwMDAwPC94bXBHOmJsYWNrPgogICAgICAgICAgICAgICAgICAgICAgICA8L3JkZjps
aT4KICAgICAgICAgICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJj
ZSI+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnN3YXRjaE5hbWU+Qz01MCBNPTEw
MCBZPTAgSz0wPC94bXBHOnN3YXRjaE5hbWU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4
bXBHOm1vZGU+Q01ZSzwveG1wRzptb2RlPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1w
Rzp0eXBlPlBST0NFU1M8L3htcEc6dHlwZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHht
cEc6Y3lhbj41MC4wMDAwMDA8L3htcEc6Y3lhbj4KICAgICAgICAgICAgICAgICAgICAgICAgICAg
PHhtcEc6bWFnZW50YT4xMDAuMDAwMDAwPC94bXBHOm1hZ2VudGE+CiAgICAgICAgICAgICAgICAg
ICAgICAgICAgIDx4bXBHOnllbGxvdz4wLjAwMDAwMDwveG1wRzp5ZWxsb3c+CiAgICAgICAgICAg
ICAgICAgICAgICAgICAgIDx4bXBHOmJsYWNrPjAuMDAwMDAwPC94bXBHOmJsYWNrPgogICAgICAg
ICAgICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAgICAgICAgICAgPHJkZjps
aSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4
bXBHOnN3YXRjaE5hbWU+Qz0zNSBNPTEwMCBZPTM1IEs9MTA8L3htcEc6c3dhdGNoTmFtZT4KICAg
ICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6bW9kZT5DTVlLPC94bXBHOm1vZGU+CiAgICAg
ICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnR5cGU+UFJPQ0VTUzwveG1wRzp0eXBlPgogICAg
ICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpjeWFuPjM1LjAwMDAwMDwveG1wRzpjeWFuPgog
ICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzptYWdlbnRhPjEwMC4wMDAwMDA8L3htcEc6
bWFnZW50YT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6eWVsbG93PjM1LjAwMDAw
MDwveG1wRzp5ZWxsb3c+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOmJsYWNrPjEw
LjAwMDAwMDwveG1wRzpibGFjaz4KICAgICAgICAgICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAg
ICAgICAgICAgICAgICAgICAgICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgog
ICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpzd2F0Y2hOYW1lPkM9MTAgTT0xMDAgWT01
MCBLPTA8L3htcEc6c3dhdGNoTmFtZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6
bW9kZT5DTVlLPC94bXBHOm1vZGU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnR5
cGU+UFJPQ0VTUzwveG1wRzp0eXBlPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpj
eWFuPjEwLjAwMDAwMDwveG1wRzpjeWFuPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1w
RzptYWdlbnRhPjEwMC4wMDAwMDA8L3htcEc6bWFnZW50YT4KICAgICAgICAgICAgICAgICAgICAg
ICAgICAgPHhtcEc6eWVsbG93PjUwLjAwMDAwMDwveG1wRzp5ZWxsb3c+CiAgICAgICAgICAgICAg
ICAgICAgICAgICAgIDx4bXBHOmJsYWNrPjAuMDAwMDAwPC94bXBHOmJsYWNrPgogICAgICAgICAg
ICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAgICAgICAgICAgPHJkZjpsaSBy
ZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBH
OnN3YXRjaE5hbWU+Qz0wIE09OTUgWT0yMCBLPTA8L3htcEc6c3dhdGNoTmFtZT4KICAgICAgICAg
ICAgICAgICAgICAgICAgICAgPHhtcEc6bW9kZT5DTVlLPC94bXBHOm1vZGU+CiAgICAgICAgICAg
ICAgICAgICAgICAgICAgIDx4bXBHOnR5cGU+UFJPQ0VTUzwveG1wRzp0eXBlPgogICAgICAgICAg
ICAgICAgICAgICAgICAgICA8eG1wRzpjeWFuPjAuMDAwMDAwPC94bXBHOmN5YW4+CiAgICAgICAg
ICAgICAgICAgICAgICAgICAgIDx4bXBHOm1hZ2VudGE+OTUuMDAwMDAwPC94bXBHOm1hZ2VudGE+
CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnllbGxvdz4yMC4wMDAwMDA8L3htcEc6
eWVsbG93PgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpibGFjaz4wLjAwMDAwMDwv
eG1wRzpibGFjaz4KICAgICAgICAgICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAgICAgICAg
ICAgICAgICAgICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAg
ICAgICAgICAgICAgICAgICA8eG1wRzpzd2F0Y2hOYW1lPkM9MjUgTT0yNSBZPTQwIEs9MDwveG1w
Rzpzd2F0Y2hOYW1lPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzptb2RlPkNNWUs8
L3htcEc6bW9kZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6dHlwZT5QUk9DRVNT
PC94bXBHOnR5cGU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOmN5YW4+MjUuMDAw
MDAwPC94bXBHOmN5YW4+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOm1hZ2VudGE+
MjUuMDAwMDAwPC94bXBHOm1hZ2VudGE+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBH
OnllbGxvdz40MC4wMDAwMDA8L3htcEc6eWVsbG93PgogICAgICAgICAgICAgICAgICAgICAgICAg
ICA8eG1wRzpibGFjaz4wLjAwMDAwMDwveG1wRzpibGFjaz4KICAgICAgICAgICAgICAgICAgICAg
ICAgPC9yZGY6bGk+CiAgICAgICAgICAgICAgICAgICAgICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlw
ZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpzd2F0Y2hOYW1l
PkM9NDAgTT00NSBZPTUwIEs9NTwveG1wRzpzd2F0Y2hOYW1lPgogICAgICAgICAgICAgICAgICAg
ICAgICAgICA8eG1wRzptb2RlPkNNWUs8L3htcEc6bW9kZT4KICAgICAgICAgICAgICAgICAgICAg
ICAgICAgPHhtcEc6dHlwZT5QUk9DRVNTPC94bXBHOnR5cGU+CiAgICAgICAgICAgICAgICAgICAg
ICAgICAgIDx4bXBHOmN5YW4+NDAuMDAwMDAwPC94bXBHOmN5YW4+CiAgICAgICAgICAgICAgICAg
ICAgICAgICAgIDx4bXBHOm1hZ2VudGE+NDUuMDAwMDAwPC94bXBHOm1hZ2VudGE+CiAgICAgICAg
ICAgICAgICAgICAgICAgICAgIDx4bXBHOnllbGxvdz41MC4wMDAwMDA8L3htcEc6eWVsbG93Pgog
ICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpibGFjaz41LjAwMDAwMDwveG1wRzpibGFj
az4KICAgICAgICAgICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAgICAgICAgICAgICAgICAg
ICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICAg
ICAgICAgICA8eG1wRzpzd2F0Y2hOYW1lPkM9NTAgTT01MCBZPTYwIEs9MjU8L3htcEc6c3dhdGNo
TmFtZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6bW9kZT5DTVlLPC94bXBHOm1v
ZGU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnR5cGU+UFJPQ0VTUzwveG1wRzp0
eXBlPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpjeWFuPjUwLjAwMDAwMDwveG1w
RzpjeWFuPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzptYWdlbnRhPjUwLjAwMDAw
MDwveG1wRzptYWdlbnRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp5ZWxsb3c+
NjAuMDAwMDAwPC94bXBHOnllbGxvdz4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6
YmxhY2s+MjUuMDAwMDAwPC94bXBHOmJsYWNrPgogICAgICAgICAgICAgICAgICAgICAgICA8L3Jk
ZjpsaT4KICAgICAgICAgICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNv
dXJjZSI+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnN3YXRjaE5hbWU+Qz01NSBN
PTYwIFk9NjUgSz00MDwveG1wRzpzd2F0Y2hOYW1lPgogICAgICAgICAgICAgICAgICAgICAgICAg
ICA8eG1wRzptb2RlPkNNWUs8L3htcEc6bW9kZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAg
PHhtcEc6dHlwZT5QUk9DRVNTPC94bXBHOnR5cGU+CiAgICAgICAgICAgICAgICAgICAgICAgICAg
IDx4bXBHOmN5YW4+NTUuMDAwMDAwPC94bXBHOmN5YW4+CiAgICAgICAgICAgICAgICAgICAgICAg
ICAgIDx4bXBHOm1hZ2VudGE+NjAuMDAwMDAwPC94bXBHOm1hZ2VudGE+CiAgICAgICAgICAgICAg
ICAgICAgICAgICAgIDx4bXBHOnllbGxvdz42NS4wMDAwMDA8L3htcEc6eWVsbG93PgogICAgICAg
ICAgICAgICAgICAgICAgICAgICA8eG1wRzpibGFjaz40MC4wMDAwMDA8L3htcEc6YmxhY2s+CiAg
ICAgICAgICAgICAgICAgICAgICAgIDwvcmRmOmxpPgogICAgICAgICAgICAgICAgICAgICAgICA8
cmRmOmxpIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgICAgICAgICAgICAg
ICAgPHhtcEc6c3dhdGNoTmFtZT5DPTI1IE09NDAgWT02NSBLPTA8L3htcEc6c3dhdGNoTmFtZT4K
ICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6bW9kZT5DTVlLPC94bXBHOm1vZGU+CiAg
ICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnR5cGU+UFJPQ0VTUzwveG1wRzp0eXBlPgog
ICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpjeWFuPjI1LjAwMDAwMDwveG1wRzpjeWFu
PgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzptYWdlbnRhPjQwLjAwMDAwMDwveG1w
RzptYWdlbnRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp5ZWxsb3c+NjUuMDAw
MDAwPC94bXBHOnllbGxvdz4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6YmxhY2s+
MC4wMDAwMDA8L3htcEc6YmxhY2s+CiAgICAgICAgICAgICAgICAgICAgICAgIDwvcmRmOmxpPgog
ICAgICAgICAgICAgICAgICAgICAgICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4K
ICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6c3dhdGNoTmFtZT5DPTMwIE09NTAgWT03
NSBLPTEwPC94bXBHOnN3YXRjaE5hbWU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBH
Om1vZGU+Q01ZSzwveG1wRzptb2RlPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp0
eXBlPlBST0NFU1M8L3htcEc6dHlwZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6
Y3lhbj4zMC4wMDAwMDA8L3htcEc6Y3lhbj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHht
cEc6bWFnZW50YT41MC4wMDAwMDA8L3htcEc6bWFnZW50YT4KICAgICAgICAgICAgICAgICAgICAg
ICAgICAgPHhtcEc6eWVsbG93Pjc1LjAwMDAwMDwveG1wRzp5ZWxsb3c+CiAgICAgICAgICAgICAg
ICAgICAgICAgICAgIDx4bXBHOmJsYWNrPjEwLjAwMDAwMDwveG1wRzpibGFjaz4KICAgICAgICAg
ICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAgICAgICAgICAgICAgICAgICAgIDxyZGY6bGkg
cmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1w
Rzpzd2F0Y2hOYW1lPkM9MzUgTT02MCBZPTgwIEs9MjU8L3htcEc6c3dhdGNoTmFtZT4KICAgICAg
ICAgICAgICAgICAgICAgICAgICAgPHhtcEc6bW9kZT5DTVlLPC94bXBHOm1vZGU+CiAgICAgICAg
ICAgICAgICAgICAgICAgICAgIDx4bXBHOnR5cGU+UFJPQ0VTUzwveG1wRzp0eXBlPgogICAgICAg
ICAgICAgICAgICAgICAgICAgICA8eG1wRzpjeWFuPjM1LjAwMDAwMDwveG1wRzpjeWFuPgogICAg
ICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzptYWdlbnRhPjYwLjAwMDAwMDwveG1wRzptYWdl
bnRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp5ZWxsb3c+ODAuMDAwMDAwPC94
bXBHOnllbGxvdz4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6YmxhY2s+MjUuMDAw
MDAwPC94bXBHOmJsYWNrPgogICAgICAgICAgICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAg
ICAgICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAg
ICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnN3YXRjaE5hbWU+Qz00MCBNPTY1IFk9OTAgSz0z
NTwveG1wRzpzd2F0Y2hOYW1lPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzptb2Rl
PkNNWUs8L3htcEc6bW9kZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6dHlwZT5Q
Uk9DRVNTPC94bXBHOnR5cGU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOmN5YW4+
NDAuMDAwMDAwPC94bXBHOmN5YW4+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOm1h
Z2VudGE+NjUuMDAwMDAwPC94bXBHOm1hZ2VudGE+CiAgICAgICAgICAgICAgICAgICAgICAgICAg
IDx4bXBHOnllbGxvdz45MC4wMDAwMDA8L3htcEc6eWVsbG93PgogICAgICAgICAgICAgICAgICAg
ICAgICAgICA8eG1wRzpibGFjaz4zNS4wMDAwMDA8L3htcEc6YmxhY2s+CiAgICAgICAgICAgICAg
ICAgICAgICAgIDwvcmRmOmxpPgogICAgICAgICAgICAgICAgICAgICAgICA8cmRmOmxpIHJkZjpw
YXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6c3dh
dGNoTmFtZT5DPTQwIE09NzAgWT0xMDAgSz01MDwveG1wRzpzd2F0Y2hOYW1lPgogICAgICAgICAg
ICAgICAgICAgICAgICAgICA8eG1wRzptb2RlPkNNWUs8L3htcEc6bW9kZT4KICAgICAgICAgICAg
ICAgICAgICAgICAgICAgPHhtcEc6dHlwZT5QUk9DRVNTPC94bXBHOnR5cGU+CiAgICAgICAgICAg
ICAgICAgICAgICAgICAgIDx4bXBHOmN5YW4+NDAuMDAwMDAwPC94bXBHOmN5YW4+CiAgICAgICAg
ICAgICAgICAgICAgICAgICAgIDx4bXBHOm1hZ2VudGE+NzAuMDAwMDAwPC94bXBHOm1hZ2VudGE+
CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnllbGxvdz4xMDAuMDAwMDAwPC94bXBH
OnllbGxvdz4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6YmxhY2s+NTAuMDAwMDAw
PC94bXBHOmJsYWNrPgogICAgICAgICAgICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAg
ICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAg
ICAgICAgICAgICAgICAgICAgIDx4bXBHOnN3YXRjaE5hbWU+Qz01MCBNPTcwIFk9ODAgSz03MDwv
eG1wRzpzd2F0Y2hOYW1lPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzptb2RlPkNN
WUs8L3htcEc6bW9kZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6dHlwZT5QUk9D
RVNTPC94bXBHOnR5cGU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOmN5YW4+NTAu
MDAwMDAwPC94bXBHOmN5YW4+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOm1hZ2Vu
dGE+NzAuMDAwMDAwPC94bXBHOm1hZ2VudGE+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4
bXBHOnllbGxvdz44MC4wMDAwMDA8L3htcEc6eWVsbG93PgogICAgICAgICAgICAgICAgICAgICAg
ICAgICA8eG1wRzpibGFjaz43MC4wMDAwMDA8L3htcEc6YmxhY2s+CiAgICAgICAgICAgICAgICAg
ICAgICAgIDwvcmRmOmxpPgogICAgICAgICAgICAgICAgICAgICA8L3JkZjpTZXE+CiAgICAgICAg
ICAgICAgICAgIDwveG1wRzpDb2xvcmFudHM+CiAgICAgICAgICAgICAgIDwvcmRmOmxpPgogICAg
ICAgICAgICAgICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAg
ICAgICAgPHhtcEc6Z3JvdXBOYW1lPkdyaWpzd2FhcmRlbjwveG1wRzpncm91cE5hbWU+CiAgICAg
ICAgICAgICAgICAgIDx4bXBHOmdyb3VwVHlwZT4xPC94bXBHOmdyb3VwVHlwZT4KICAgICAgICAg
ICAgICAgICAgPHhtcEc6Q29sb3JhbnRzPgogICAgICAgICAgICAgICAgICAgICA8cmRmOlNlcT4K
ICAgICAgICAgICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+
CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnN3YXRjaE5hbWU+Qz0wIE09MCBZPTAg
Sz0xMDA8L3htcEc6c3dhdGNoTmFtZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6
bW9kZT5DTVlLPC94bXBHOm1vZGU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnR5
cGU+UFJPQ0VTUzwveG1wRzp0eXBlPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpj
eWFuPjAuMDAwMDAwPC94bXBHOmN5YW4+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBH
Om1hZ2VudGE+MC4wMDAwMDA8L3htcEc6bWFnZW50YT4KICAgICAgICAgICAgICAgICAgICAgICAg
ICAgPHhtcEc6eWVsbG93PjAuMDAwMDAwPC94bXBHOnllbGxvdz4KICAgICAgICAgICAgICAgICAg
ICAgICAgICAgPHhtcEc6YmxhY2s+MTAwLjAwMDAwMDwveG1wRzpibGFjaz4KICAgICAgICAgICAg
ICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAgICAgICAgICAgICAgICAgICAgIDxyZGY6bGkgcmRm
OnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpz
d2F0Y2hOYW1lPkM9MCBNPTAgWT0wIEs9OTA8L3htcEc6c3dhdGNoTmFtZT4KICAgICAgICAgICAg
ICAgICAgICAgICAgICAgPHhtcEc6bW9kZT5DTVlLPC94bXBHOm1vZGU+CiAgICAgICAgICAgICAg
ICAgICAgICAgICAgIDx4bXBHOnR5cGU+UFJPQ0VTUzwveG1wRzp0eXBlPgogICAgICAgICAgICAg
ICAgICAgICAgICAgICA8eG1wRzpjeWFuPjAuMDAwMDAwPC94bXBHOmN5YW4+CiAgICAgICAgICAg
ICAgICAgICAgICAgICAgIDx4bXBHOm1hZ2VudGE+MC4wMDAwMDA8L3htcEc6bWFnZW50YT4KICAg
ICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6eWVsbG93PjAuMDAwMDAwPC94bXBHOnllbGxv
dz4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6YmxhY2s+ODkuOTk5NDAwPC94bXBH
OmJsYWNrPgogICAgICAgICAgICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAg
ICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAg
ICAgICAgICAgICAgIDx4bXBHOnN3YXRjaE5hbWU+Qz0wIE09MCBZPTAgSz04MDwveG1wRzpzd2F0
Y2hOYW1lPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzptb2RlPkNNWUs8L3htcEc6
bW9kZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6dHlwZT5QUk9DRVNTPC94bXBH
OnR5cGU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOmN5YW4+MC4wMDAwMDA8L3ht
cEc6Y3lhbj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6bWFnZW50YT4wLjAwMDAw
MDwveG1wRzptYWdlbnRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp5ZWxsb3c+
MC4wMDAwMDA8L3htcEc6eWVsbG93PgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpi
bGFjaz43OS45OTg4MDA8L3htcEc6YmxhY2s+CiAgICAgICAgICAgICAgICAgICAgICAgIDwvcmRm
OmxpPgogICAgICAgICAgICAgICAgICAgICAgICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9IlJlc291
cmNlIj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6c3dhdGNoTmFtZT5DPTAgTT0w
IFk9MCBLPTcwPC94bXBHOnN3YXRjaE5hbWU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4
bXBHOm1vZGU+Q01ZSzwveG1wRzptb2RlPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1w
Rzp0eXBlPlBST0NFU1M8L3htcEc6dHlwZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHht
cEc6Y3lhbj4wLjAwMDAwMDwveG1wRzpjeWFuPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8
eG1wRzptYWdlbnRhPjAuMDAwMDAwPC94bXBHOm1hZ2VudGE+CiAgICAgICAgICAgICAgICAgICAg
ICAgICAgIDx4bXBHOnllbGxvdz4wLjAwMDAwMDwveG1wRzp5ZWxsb3c+CiAgICAgICAgICAgICAg
ICAgICAgICAgICAgIDx4bXBHOmJsYWNrPjY5Ljk5OTcwMDwveG1wRzpibGFjaz4KICAgICAgICAg
ICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAgICAgICAgICAgICAgICAgICAgIDxyZGY6bGkg
cmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1w
Rzpzd2F0Y2hOYW1lPkM9MCBNPTAgWT0wIEs9NjA8L3htcEc6c3dhdGNoTmFtZT4KICAgICAgICAg
ICAgICAgICAgICAgICAgICAgPHhtcEc6bW9kZT5DTVlLPC94bXBHOm1vZGU+CiAgICAgICAgICAg
ICAgICAgICAgICAgICAgIDx4bXBHOnR5cGU+UFJPQ0VTUzwveG1wRzp0eXBlPgogICAgICAgICAg
ICAgICAgICAgICAgICAgICA8eG1wRzpjeWFuPjAuMDAwMDAwPC94bXBHOmN5YW4+CiAgICAgICAg
ICAgICAgICAgICAgICAgICAgIDx4bXBHOm1hZ2VudGE+MC4wMDAwMDA8L3htcEc6bWFnZW50YT4K
ICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6eWVsbG93PjAuMDAwMDAwPC94bXBHOnll
bGxvdz4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6YmxhY2s+NTkuOTk5MTAwPC94
bXBHOmJsYWNrPgogICAgICAgICAgICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAg
ICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAg
ICAgICAgICAgICAgICAgIDx4bXBHOnN3YXRjaE5hbWU+Qz0wIE09MCBZPTAgSz01MDwveG1wRzpz
d2F0Y2hOYW1lPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzptb2RlPkNNWUs8L3ht
cEc6bW9kZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6dHlwZT5QUk9DRVNTPC94
bXBHOnR5cGU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOmN5YW4+MC4wMDAwMDA8
L3htcEc6Y3lhbj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6bWFnZW50YT4wLjAw
MDAwMDwveG1wRzptYWdlbnRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp5ZWxs
b3c+MC4wMDAwMDA8L3htcEc6eWVsbG93PgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1w
RzpibGFjaz41MC4wMDAwMDA8L3htcEc6YmxhY2s+CiAgICAgICAgICAgICAgICAgICAgICAgIDwv
cmRmOmxpPgogICAgICAgICAgICAgICAgICAgICAgICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9IlJl
c291cmNlIj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6c3dhdGNoTmFtZT5DPTAg
TT0wIFk9MCBLPTQwPC94bXBHOnN3YXRjaE5hbWU+CiAgICAgICAgICAgICAgICAgICAgICAgICAg
IDx4bXBHOm1vZGU+Q01ZSzwveG1wRzptb2RlPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8
eG1wRzp0eXBlPlBST0NFU1M8L3htcEc6dHlwZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAg
PHhtcEc6Y3lhbj4wLjAwMDAwMDwveG1wRzpjeWFuPgogICAgICAgICAgICAgICAgICAgICAgICAg
ICA8eG1wRzptYWdlbnRhPjAuMDAwMDAwPC94bXBHOm1hZ2VudGE+CiAgICAgICAgICAgICAgICAg
ICAgICAgICAgIDx4bXBHOnllbGxvdz4wLjAwMDAwMDwveG1wRzp5ZWxsb3c+CiAgICAgICAgICAg
ICAgICAgICAgICAgICAgIDx4bXBHOmJsYWNrPjM5Ljk5OTQwMDwveG1wRzpibGFjaz4KICAgICAg
ICAgICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAgICAgICAgICAgICAgICAgICAgIDxyZGY6
bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8
eG1wRzpzd2F0Y2hOYW1lPkM9MCBNPTAgWT0wIEs9MzA8L3htcEc6c3dhdGNoTmFtZT4KICAgICAg
ICAgICAgICAgICAgICAgICAgICAgPHhtcEc6bW9kZT5DTVlLPC94bXBHOm1vZGU+CiAgICAgICAg
ICAgICAgICAgICAgICAgICAgIDx4bXBHOnR5cGU+UFJPQ0VTUzwveG1wRzp0eXBlPgogICAgICAg
ICAgICAgICAgICAgICAgICAgICA8eG1wRzpjeWFuPjAuMDAwMDAwPC94bXBHOmN5YW4+CiAgICAg
ICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOm1hZ2VudGE+MC4wMDAwMDA8L3htcEc6bWFnZW50
YT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6eWVsbG93PjAuMDAwMDAwPC94bXBH
OnllbGxvdz4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6YmxhY2s+MjkuOTk4ODAw
PC94bXBHOmJsYWNrPgogICAgICAgICAgICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAg
ICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAg
ICAgICAgICAgICAgICAgICAgIDx4bXBHOnN3YXRjaE5hbWU+Qz0wIE09MCBZPTAgSz0yMDwveG1w
Rzpzd2F0Y2hOYW1lPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzptb2RlPkNNWUs8
L3htcEc6bW9kZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6dHlwZT5QUk9DRVNT
PC94bXBHOnR5cGU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOmN5YW4+MC4wMDAw
MDA8L3htcEc6Y3lhbj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6bWFnZW50YT4w
LjAwMDAwMDwveG1wRzptYWdlbnRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp5
ZWxsb3c+MC4wMDAwMDA8L3htcEc6eWVsbG93PgogICAgICAgICAgICAgICAgICAgICAgICAgICA8
eG1wRzpibGFjaz4xOS45OTk3MDA8L3htcEc6YmxhY2s+CiAgICAgICAgICAgICAgICAgICAgICAg
IDwvcmRmOmxpPgogICAgICAgICAgICAgICAgICAgICAgICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9
IlJlc291cmNlIj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6c3dhdGNoTmFtZT5D
PTAgTT0wIFk9MCBLPTEwPC94bXBHOnN3YXRjaE5hbWU+CiAgICAgICAgICAgICAgICAgICAgICAg
ICAgIDx4bXBHOm1vZGU+Q01ZSzwveG1wRzptb2RlPgogICAgICAgICAgICAgICAgICAgICAgICAg
ICA8eG1wRzp0eXBlPlBST0NFU1M8L3htcEc6dHlwZT4KICAgICAgICAgICAgICAgICAgICAgICAg
ICAgPHhtcEc6Y3lhbj4wLjAwMDAwMDwveG1wRzpjeWFuPgogICAgICAgICAgICAgICAgICAgICAg
ICAgICA8eG1wRzptYWdlbnRhPjAuMDAwMDAwPC94bXBHOm1hZ2VudGE+CiAgICAgICAgICAgICAg
ICAgICAgICAgICAgIDx4bXBHOnllbGxvdz4wLjAwMDAwMDwveG1wRzp5ZWxsb3c+CiAgICAgICAg
ICAgICAgICAgICAgICAgICAgIDx4bXBHOmJsYWNrPjkuOTk5MTAwPC94bXBHOmJsYWNrPgogICAg
ICAgICAgICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAgICAgICAgICAgPHJk
ZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAgICAgICAgICAg
IDx4bXBHOnN3YXRjaE5hbWU+Qz0wIE09MCBZPTAgSz01PC94bXBHOnN3YXRjaE5hbWU+CiAgICAg
ICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOm1vZGU+Q01ZSzwveG1wRzptb2RlPgogICAgICAg
ICAgICAgICAgICAgICAgICAgICA8eG1wRzp0eXBlPlBST0NFU1M8L3htcEc6dHlwZT4KICAgICAg
ICAgICAgICAgICAgICAgICAgICAgPHhtcEc6Y3lhbj4wLjAwMDAwMDwveG1wRzpjeWFuPgogICAg
ICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzptYWdlbnRhPjAuMDAwMDAwPC94bXBHOm1hZ2Vu
dGE+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnllbGxvdz4wLjAwMDAwMDwveG1w
Rzp5ZWxsb3c+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOmJsYWNrPjQuOTk4ODAw
PC94bXBHOmJsYWNrPgogICAgICAgICAgICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAg
ICAgICAgICAgICAgPC9yZGY6U2VxPgogICAgICAgICAgICAgICAgICA8L3htcEc6Q29sb3JhbnRz
PgogICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFy
c2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAgIDx4bXBHOmdyb3VwTmFtZT5IZWxk
ZXJlIGtsZXVyZW48L3htcEc6Z3JvdXBOYW1lPgogICAgICAgICAgICAgICAgICA8eG1wRzpncm91
cFR5cGU+MTwveG1wRzpncm91cFR5cGU+CiAgICAgICAgICAgICAgICAgIDx4bXBHOkNvbG9yYW50
cz4KICAgICAgICAgICAgICAgICAgICAgPHJkZjpTZXE+CiAgICAgICAgICAgICAgICAgICAgICAg
IDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICAgICAg
ICAgICA8eG1wRzpzd2F0Y2hOYW1lPkM9MCBNPTEwMCBZPTEwMCBLPTA8L3htcEc6c3dhdGNoTmFt
ZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6bW9kZT5DTVlLPC94bXBHOm1vZGU+
CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnR5cGU+UFJPQ0VTUzwveG1wRzp0eXBl
PgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpjeWFuPjAuMDAwMDAwPC94bXBHOmN5
YW4+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOm1hZ2VudGE+MTAwLjAwMDAwMDwv
eG1wRzptYWdlbnRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp5ZWxsb3c+MTAw
LjAwMDAwMDwveG1wRzp5ZWxsb3c+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOmJs
YWNrPjAuMDAwMDAwPC94bXBHOmJsYWNrPgogICAgICAgICAgICAgICAgICAgICAgICA8L3JkZjps
aT4KICAgICAgICAgICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJj
ZSI+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnN3YXRjaE5hbWU+Qz0wIE09NzUg
WT0xMDAgSz0wPC94bXBHOnN3YXRjaE5hbWU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4
bXBHOm1vZGU+Q01ZSzwveG1wRzptb2RlPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1w
Rzp0eXBlPlBST0NFU1M8L3htcEc6dHlwZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHht
cEc6Y3lhbj4wLjAwMDAwMDwveG1wRzpjeWFuPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8
eG1wRzptYWdlbnRhPjc1LjAwMDAwMDwveG1wRzptYWdlbnRhPgogICAgICAgICAgICAgICAgICAg
ICAgICAgICA8eG1wRzp5ZWxsb3c+MTAwLjAwMDAwMDwveG1wRzp5ZWxsb3c+CiAgICAgICAgICAg
ICAgICAgICAgICAgICAgIDx4bXBHOmJsYWNrPjAuMDAwMDAwPC94bXBHOmJsYWNrPgogICAgICAg
ICAgICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAgICAgICAgICAgPHJkZjps
aSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4
bXBHOnN3YXRjaE5hbWU+Qz0wIE09MTAgWT05NSBLPTA8L3htcEc6c3dhdGNoTmFtZT4KICAgICAg
ICAgICAgICAgICAgICAgICAgICAgPHhtcEc6bW9kZT5DTVlLPC94bXBHOm1vZGU+CiAgICAgICAg
ICAgICAgICAgICAgICAgICAgIDx4bXBHOnR5cGU+UFJPQ0VTUzwveG1wRzp0eXBlPgogICAgICAg
ICAgICAgICAgICAgICAgICAgICA8eG1wRzpjeWFuPjAuMDAwMDAwPC94bXBHOmN5YW4+CiAgICAg
ICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOm1hZ2VudGE+MTAuMDAwMDAwPC94bXBHOm1hZ2Vu
dGE+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnllbGxvdz45NS4wMDAwMDA8L3ht
cEc6eWVsbG93PgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpibGFjaz4wLjAwMDAw
MDwveG1wRzpibGFjaz4KICAgICAgICAgICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAgICAg
ICAgICAgICAgICAgICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAg
ICAgICAgICAgICAgICAgICAgICA8eG1wRzpzd2F0Y2hOYW1lPkM9ODUgTT0xMCBZPTEwMCBLPTA8
L3htcEc6c3dhdGNoTmFtZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6bW9kZT5D
TVlLPC94bXBHOm1vZGU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnR5cGU+UFJP
Q0VTUzwveG1wRzp0eXBlPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpjeWFuPjg1
LjAwMDAwMDwveG1wRzpjeWFuPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzptYWdl
bnRhPjEwLjAwMDAwMDwveG1wRzptYWdlbnRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8
eG1wRzp5ZWxsb3c+MTAwLjAwMDAwMDwveG1wRzp5ZWxsb3c+CiAgICAgICAgICAgICAgICAgICAg
ICAgICAgIDx4bXBHOmJsYWNrPjAuMDAwMDAwPC94bXBHOmJsYWNrPgogICAgICAgICAgICAgICAg
ICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAgICAgICAgICAgPHJkZjpsaSByZGY6cGFy
c2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnN3YXRj
aE5hbWU+Qz0xMDAgTT05MCBZPTAgSz0wPC94bXBHOnN3YXRjaE5hbWU+CiAgICAgICAgICAgICAg
ICAgICAgICAgICAgIDx4bXBHOm1vZGU+Q01ZSzwveG1wRzptb2RlPgogICAgICAgICAgICAgICAg
ICAgICAgICAgICA8eG1wRzp0eXBlPlBST0NFU1M8L3htcEc6dHlwZT4KICAgICAgICAgICAgICAg
ICAgICAgICAgICAgPHhtcEc6Y3lhbj4xMDAuMDAwMDAwPC94bXBHOmN5YW4+CiAgICAgICAgICAg
ICAgICAgICAgICAgICAgIDx4bXBHOm1hZ2VudGE+OTAuMDAwMDAwPC94bXBHOm1hZ2VudGE+CiAg
ICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnllbGxvdz4wLjAwMDAwMDwveG1wRzp5ZWxs
b3c+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOmJsYWNrPjAuMDAwMDAwPC94bXBH
OmJsYWNrPgogICAgICAgICAgICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgICAg
ICAgICAgICAgPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+CiAgICAgICAgICAgICAg
ICAgICAgICAgICAgIDx4bXBHOnN3YXRjaE5hbWU+Qz02MCBNPTkwIFk9MCBLPTA8L3htcEc6c3dh
dGNoTmFtZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgPHhtcEc6bW9kZT5DTVlLPC94bXBH
Om1vZGU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgIDx4bXBHOnR5cGU+UFJPQ0VTUzwveG1w
Rzp0eXBlPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzpjeWFuPjYwLjAwMDAwMDwv
eG1wRzpjeWFuPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzptYWdlbnRhPjkwLjAw
MDAwMDwveG1wRzptYWdlbnRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1wRzp5ZWxs
b3c+MC4wMDMxMDA8L3htcEc6eWVsbG93PgogICAgICAgICAgICAgICAgICAgICAgICAgICA8eG1w
RzpibGFjaz4wLjAwMzEwMDwveG1wRzpibGFjaz4KICAgICAgICAgICAgICAgICAgICAgICAgPC9y
ZGY6bGk+CiAgICAgICAgICAgICAgICAgICAgIDwvcmRmOlNlcT4KICAgICAgICAgICAgICAgICAg
PC94bXBHOkNvbG9yYW50cz4KICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAgICAgICAgIDwv
cmRmOlNlcT4KICAgICAgICAgPC94bXBUUGc6U3dhdGNoR3JvdXBzPgogICAgICAgICA8cGRmOlBy
b2R1Y2VyPkFkb2JlIFBERiBsaWJyYXJ5IDkuOTA8L3BkZjpQcm9kdWNlcj4KICAgICAgICAgPHBo
b3Rvc2hvcDpDb2xvck1vZGU+MzwvcGhvdG9zaG9wOkNvbG9yTW9kZT4KICAgICAgICAgPHBob3Rv
c2hvcDpJQ0NQcm9maWxlPnNSR0IgSUVDNjE5NjYtMi4xPC9waG90b3Nob3A6SUNDUHJvZmlsZT4K
ICAgICAgICAgPHRpZmY6T3JpZW50YXRpb24+MTwvdGlmZjpPcmllbnRhdGlvbj4KICAgICAgICAg
PHRpZmY6WFJlc29sdXRpb24+NzIwMDAwLzEwMDAwPC90aWZmOlhSZXNvbHV0aW9uPgogICAgICAg
ICA8dGlmZjpZUmVzb2x1dGlvbj43MjAwMDAvMTAwMDA8L3RpZmY6WVJlc29sdXRpb24+CiAgICAg
ICAgIDx0aWZmOlJlc29sdXRpb25Vbml0PjI8L3RpZmY6UmVzb2x1dGlvblVuaXQ+CiAgICAgICAg
IDxleGlmOkNvbG9yU3BhY2U+MTwvZXhpZjpDb2xvclNwYWNlPgogICAgICAgICA8ZXhpZjpQaXhl
bFhEaW1lbnNpb24+MTUwPC9leGlmOlBpeGVsWERpbWVuc2lvbj4KICAgICAgICAgPGV4aWY6UGl4
ZWxZRGltZW5zaW9uPjY3PC9leGlmOlBpeGVsWURpbWVuc2lvbj4KICAgICAgPC9yZGY6RGVzY3Jp
cHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CiAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAK
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
IAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAog
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAK
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
IAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAog
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgCjw/
eHBhY2tldCBlbmQ9InciPz6BMmGAAAAQiElEQVR4Xu1dB3xUxdbfJTQJSK8BQekBTEKRqnRBkCaC
0qR3KYJIl45SpClNBPE9RQTrU/hQnooFfWJ5YkUFJMn23tLL+c7/3LvJJtmFLIJ+fO78fv9kd+7M
3Jkz/3vmzMyZuxpdidv1DGJ4/ubwMTIYvzC2MOozIJ8KjN8YkJGLESzv3xFeRjrDwHiZ0YYBeQnw
B4kgtAiKYioDMooPiIsgNLYzIsQqJiYzIKdZAXERhMYzjAixioE0RjQDsrqgxkVwedx9FcSKI522
BSVrmjKaFUDw9P8vsJgBWW0IiIsgNI5eBbESKFnblJI0dRj1VNRl3MLXWjGYeEHz3dA4xoCsHgyI
iyA0EsMm1iVNFbLfP4+ydWbK+Oxbyjj9X8r64Tyln/yc9OXvYILdGjTfDY4vGJBV74C4CELDEzax
ftfcRM5Z66lwyPX4SF+xo6q5gue9gXGGAVn1CoiLIDQixComIhorPESIVUxEiBUeQhErnhHH9lQl
uqgpzWQqJ7ioKUnnNRpyzdqs0ik/5Hg8lFihJl3g6/70CsoybuJZ422k17bh/00pUVNNvXaT4JIm
muNqqjNL3LtwfRToS7SjJG1drkeJvLwXNaXokrYyJZeI5e/lGWW4vApcXgzpNC04n788/q9txfG1
xU5MFluwtXrtiogQKzwEJ1ayJpY7pSXZH1hArvmbyTl1tcC9eDvpb+5AjtGLVDrlh9y0DHLNeZIc
oxblpXeMf5zcy54m33NvkilusJAOs0hz0yFc9qPknLmOnNPXkv2+uWSs3186HeRNAgmDdDryWtqP
JPfK3eR8eL3AtXAr2YbNl3o5J63kuHVk7TudDNW6MYFuFvIla9GeOEou3ZQs3UaTffB8MjbuLuVd
jsgBiBArPIQiViNGc8o697tKm/zgnLySrD0nqt+KF1yPbiZDlS7kmvskZV1IVmOLhswffiPHhDXc
4XWZEGVZw6A++R0PbeRZtVtNnR8wOzXe2ody3F41Rgnp739B1u5ThVwXmdSWdmMo9dWTlPbOR0z
4
HTKLTdY0ziv/MogQKzxcjljNKOvni2oX5QfHxMfJ2qN4xMox2oSgxro9KMfmUmOvHHLMTrLcNVq0
lw7aUyVXuMTyh/T3zrA2rEHGDr3J/eRO8mx6lpxLnyBLm5FC4sLtD4IIscLD9SNWrjNFOtP2wEw1
JvzgmLaMyaVlcikLr1dLLITsJCPZ+s0gx9RV5DvwBrlX7JQ2gviF2x8EEWKFhysQK8hQ6Ji0olhD
oSG2K5m6DFa/XX2wDpooBjpsrj9CLISs80lkbvcgWTqOJX0tngho6nN7IzbWdcDliZX53a/SIbkZ
mQIEx9ilTKxJ8rlwyM3Mkv+uFZspqWqsfL5cQPrcrGz1W/CQ60snfUx71n7VxVYKSqzPmVj176Zs
o1WNCR1SjhyXmS2WRYJNEEIgQqzwEJxYmJYDhup3kiGmBxlqdVNQtyfPtKryTG6N2k35IdebQsZG
/ThfPKepRKnvfaBeKRp8+18nc8IwWffSV+5Mlk6jKe2N99WrRYPv4FEmVTSjdFBiZV3UkS4qTmay
xrq9yD58Pj8Uv6hXiwb72Hmi/YqprYAIscJDCGLJRnKcPNVJmtqMGBW1ZHblmrVB7aL8kOPxkq5C
M/qNr5t7DFVjiwZrt0l5a13+DWx0Msp1jF6mpioajI17iaYJRqzsS0YmdAvWalXErkNZWGpI2fum
mqJgyDjzrdwXWrlo24MiQqzwEIpYoYHhKOTKe/kO0qm+/UfV2ILBNmqOkAqapaC2SOBObqosvi4u
uviK4Jz/BP0KYq0MRiy9lJOkuU3+Y4jDgis0XPrJM2qqgsHceginqRZQh8siQqzwcG2JhXUnLHJm
XUhUY/ND2nufiJYCgYK71sSLFkkq1YCydSY1V35IffVd0Ybu5c+oMfmhMLEUwNgvRebuwbWnc/oq
aUt++ssiQqzwcG2JlaxpQrpS8XmGfmBwzF3O2kzLZSQUKTMPGiwplCXfP4tqvKwfLyjD8IKn1Jj8
EJxYmIQ0pORSTShbr1NT5gfPxueF6GJPBuQJgQixwsO1JlZDMciDBduDszhvdNAy88AaT5YUNu9R
c+WHHJ7xYU/RNbeofReaWM04rh5lfHNWTZkffM+8zOWVV9fI8vOEQIRY4eFaE6sx6cu0YfVSdAnB
MWMJ5y3BZVxmJiYaqwz5DhxSc+WHrF8uscYqIdtDhUNoYjWiZG0jyrpUdGj2bj7I94porOuEa2xj
8dMPDZGdZFBj80PqGyc4b2lOA4+DYGVjFtqAERPcRnvrfTHu3cuKa2Ml8P3KkqnjADVVweCctVau
56e/LCLECg/Xllj66PZiB6UefkeNLRgs/cYos0IttFagAc/feaaIa44ZwZccXEu3FHtWCFJhqQR1
SXs9+PqYuf1wHgqrqOmviCDEilNmn9pqfJ+SLBe4AZXm+9bl+DYCuOZAA/uvJWrqSB59VFu+d1Vx
AdKXaKuWxzKQNUC4FCEP3I2ipZxEbR1GDf7cTr4naevzNZ54aPGQwmZVloewxJKkvVXS5NfzL8G1
JZahYichh3VAiJV5TxqZmveXNIkQLNtkAAQi+XpPUVMWDca4u+kXTuNeuV2NyQ9Zl3Rczq1cZg0h
1O+aKO7sKPKsPaCmKBgyv/uJ0zXgjufJRpA2BkFRYrF2hk1p7TuNfM++Sin/eJtSnn2TDI27y8xY
ljJqxklc2olPKfXo+2RK6Md1w8y4MdkGzpbtJdRDyhNSVSZLn/GUduxTSnv3NHme3C/XsYBs7TNV
7ge7UN+4A/l2HCFD7e4Sp9NgRl2PbMNnkrER3IHwgAWbef9puMYaiw13pXPrUsbX36lXCoXsHNkA
NtTuxkLBJnAsGW/rS76dL6sJioaU1//FRCnF92bDPpjG+t2vsRqQ/qa2ZL1rHKV/GHz9CgFaUYbl
y9l7BRGEWLHc1lrKYZLfksRXLf2jr+SQiU7TnOvSkLKTTZT+wRnZqUh5+bjc29J1hAzp6e99Lr5q
MoHg8uSQyvBH+SHRy8wXs9a0tz7kcm4h79Z/UNor/xaNrYtpLeWkvv5vIRBkiIfU0LirxLufeJpl
xbas9jKz7+uPa08saA6ksfQdo14JEXJyxOMg23CF/b30bEquGkvuDbtY1eRQrsujXggIWVlKWZcM
lOtLUSODh4wzZ7kj6ihPepD2hUAQYmGlvw5lnf2dbLNm0DnudHR8+pdfkX3QPPKytkn714dCIqyn
YQ3OtfQpyjqXyB2vFdK4l+7IIxZklvLsG5R+6kxeWfCExUw49eAJcm/ewp+rSRs8G/aJhseaoF6j
bM77DrxMGV/8ROknT5O+Nk5LwUX8L9Na14NY3CAeJjAcudfvVK9efTC27UvGO/qq3/5gyCIyNOsu
HRaGWzIQgli1KPPbc+RetZvLrEqmlkMox+Iga1cezt4+RbaRMzj+ZtJFYaiqT/p67Vm7XeD88ZTy
4jsFiCXXy7ej7ERl4mMbNFs0D+xEz6pnKcfuouwLBrIOmSak8ttXGCHMne6jzB8vilZPefU4efe9
KPnCbOO1xHUiFgsOmgsdmPLPt9UU4Qfr0OksuHqshTLUmD8WLL1Gcv1LXs0wEYJYNSj93/8RDw14
peZYXeRerQxvmWd/YZtnGn/GBCFONCRcdTJ//ZX0ZdqJXAKJBYMb5IQWTNn9utTXt++okMi1YAsL
OJdynT6y9lf2WjEU4wwB2pP6Kmu0bXvpZ443dujD2ttK+jrQWnC9/ku01vUiFtK2loYhvWvBNjVV
8UKO00PG+j1F1eeYHGrs1YfMsxdIX7cT16UU10uZQRVu1xUQWmP992c2sp8TraSv1kkeJgxfGZ9x
/K4DMgTC9sOQaB0+gbWOma9XpLTXPiTnopUSn6ipRMlatpe0jUXTSJ7SDSjX7CNTk3vJ8/gecm3c
IjM+BO82RSNhImDgduWmZrCt9y2lHnmPUl46Jmnc63fw9b/M1rqexFI2g7FIiRmaIaYbG+hHWPuk
qTmKhhybU/YC4aLjeoyf0j8Ysn66QI4RS7kjqzBu5ifcPzUv2q4rIAixYLyzjfXT7+Scu0q1eXg2
Jvul1chy5wipAw57GKrdSfYHF8h356xVQqa0Nz7g2eJpTjeWHFNWiqMiDpj4Dr7Jk5l75NBItt5M
hsqdyLPuWUo9rBrvtRN4tsKmJ2tK3Cf9gy/EkLe0H0HOGWvINng2OSaukKHT1GKw1DGvzn8ewicW
FjAtHUaSb9dh8qzeI24s3k3Pk3vJDp6RYe2m8BQ+jjs0XoQgPuxl4snaYzK5HtkgAvOs3StEtXQa
p5JQQ5ZuY2XKjXtguMA9ABfPrjJOfSkdFBhy2KDHdffyp2WmBWdEUxN4L9TiB6Ekl4sDE3/I3ghC
rJaiiTBjsw18WO6Vd01dnLX0H8/Eu0QZH39NWb8kknMOFmXLcdoacooJjpRp751m++i8zBxNTe+l
zG9+EsfFrB8ukn3sY0JY58z1LK+NQhJoxOSKjSn9+H9kJurb8xrpG7QRuSkPUEXJ4939Ipe5nr9X
DajXn4bwiYVFuiQtFh+1LCQs5AFYIISnANxhQqle1hRaZXUd2kNZAMSZRWUxEAIBsfRaDKEN1GvK
EoMfWMdyrizqZIgtG1yHQJW0NwmRFX/2Yi8pXA5FiSWA8VxLhrGCxIVWTOA2VZZ66KPuEFLIXqm2
lSyQJmnrcVx1bit80mpzGc0kDsSRxVUuEzLxL4gqi6nKwqsiQ75Wti0lRnFetmfzvWFxb5yfrE7J
UfAkuUGGwr8SIE1wR7/ge4XXECGIxYBvWcj9Ru5ktsVEi8tpo4BhWMv5EMfXBfguD19LSZ93yAOe
sbjmv14kP+LVuDyEyPPnIUKsYiI0sSIIhgixiokIscLDjUUsrPngGD8CtlGAHHcKpZ/6Sq7DRiuc
5xohQqzwcDXGO4xJbNtEc0dX4v/lZfc9uURzMVSVt/rlG8xIj01ZGPz+F3fA8JSdeDZC9SXYsNXG
5Bmn+flgxGORtRyjrBijMOyTtY3kvsliu8AeAZQ1JdzDPzNC+TCeYWOgfKW+iEdezBLDMmpDGu94
u6EyGSktZcOugXEOYxwPguLVUJPjWSZR8GCAP351rlNtbiM8G1BGc06L9S/lhSb4/heuml8LhE8s
dIqhahdyjFnM2mMDuRdsIds900lfrj2Z4gYI6eCwp6RX3FewQWq8tZ8YojiaBWc915zNpK/XgQVf
lszN7pOjY/6lChBMZkd17xAPBd+uI2S9e4osVxhv6UnmTvfz9Wipi7LjX4Es3UeSOXYoWbpNINfs
J0WzOSeukHtik9s5Y62sjTnHr6Lk6CbcuYoLS2DbLoOgxBINWS6BXPM2UerhE2Qf+aiQSGZ6JeqT
+7GtlHroBNdpFNexkjwYIKGp9QCytB0p9cfDhbq45mxUVuMXbZUXnFxH7ftnIHxiYSrvXqK4rqQe
Ok6ZX/4ob3gxtRgkcea70OnYi4tn4eBJrEjZVgvbRnvIfMcIopxc8j3/puIJYMK7HVqQd8sL5Ntz
RMpGPmgaU7vBlGNxU+qb75MXng+pmeRev0u0WK4rgyxDJnA6nDUsS7aHZlHm17+S7b65nMcum794
IUjqkRNcflNKe+sD2aDGuljmf8+JR4IuCsQo9kZ0UWJxvUEg33OvU8aZ7x5ghREAABJbSURBVGVN
Lofb41qwVZY9Ul/9H8o8+yu5H98p762wDX1YWZNSh3LPhgNc95JMqmqU+voJyjqfLPYj3GUsLCfR
cgXrcCPhaohVjlL2vkG+Ay/J3tQFDd5VVZLMbZWV5tQ34SlaTrQWhgHroAkS73hoCTmGLaKUd96W
FWQg5ci/yLf1EAv/Ge70VySf4qfeQFyRsTiKVWp0iK7C7ZRrSydT+4Fk6TWKcu2pQqzEkjGU684U
XydL11GU8ck3siWibJXA5+tWyvz8RzLfP0TuifiML8+yNsE+HR6A4O0shCDEaiVu2KYmAykxqpZ4
JFhnzaS01z4hU8vBTKrzlFiiurL2tmgdZXz0rbQPK+3YBoLPPepiHzqPMr/9RRY48R1I1jZRh8kC
dbiRcHXEgu8UtAyGNKz+ouPxwtuUo++IprBPWciCKsGd2pBSXnyTMn84R47xS8k+bD6l/8+n3OGV
5GnP/OZn0X7YxvETC8OCpeMYyvzxZ0ouh6P+jWTIguC9B/9Jaa98KJ2VeuykDJGp/zhG3ucPCWkc
Yx8XrwH7yIU8rLzD98CB2zrybgfX7Ce4LB7GK3dmoz9R6gviBWtjEAQZCjHct2ISwFkRh26juZ0X
ZMi13zeP0k59KsSF5rH2n0AZX38vZJQjbNt2sLY/qbjSLHhKVt49q3dzfv6/cT/niZEHLP9eNxyu
klhPHxLvxrTjn1DqayflKXOMWUae7fvlJR7Zl0wS55y/lrzbDpJj6mPkXraLzG2Gi/ZKP/WlDFnw
PYK28z17lHy7/cSqRdaeUynju7Ns7INYMGSh/UqRZ9dzPMx9LETGKjVl5VLWuQvSgRgSHeOWy5Dq
3XiAcsx26XAY89hPQ8j8XnkXhX3cQrkXdgKCtTEIQhrv0IggfcYXP0hd0G5sb6V/cJrvXVHaY+k5
jrXUj5w+Th44746XxIwQYj2yUeoEjW5qNoByXF6pv3g9yPvBbkhc/VDo3f+SaAl0KATrGL+MO/0T
+Zx++ism0nZKOXSMDPU781O8ktwrdpFtwMPcAd+RoU53MrcaTklRdYQkaYdPkWfnPukUDJ+6mvGU
k+QgQ5seUh60G66lnfqMfDsOC5EQn3riJHn3viCfMdNyjF9J6R+fkTKx5QRSYW8z47PvRZNAE2R+
eY7cG+FlqfmDxjtI1VDuk/rKu6x9z/Nn9VWaMzZT+mf/4e+KP5V1xETKPKO4Q6N9gcTyLNtL6We/
ls+Qp33Vcso8/RPXnbVp0FX1GwJXZ7zj6FTm1z8xOe7j4W0emZoOIOcU7tQTn0mnGlv2lKfQ9/xh
EZZn3QFxR3aMWCxDmEKWOjIlF6LuZwP487NkjhtK9gcXkr5aR/I89RzlOL1kun0IGap0EneQHJuD
Z0zYK6svT3T6e6fJt1cx+mHw2+9/RNyUrd0nkK3/DLZfHuE6x4l7MPzB0en6em0p15su7/jCZnCw
NgbBlwzIqqc/DjPYJM1tMglBMMb0kBeSGGN6kb5qR56YWMkxYTmna0pZvyZSyoHXuJ6YbJQj7zOH
KPXl4yIrU+y98t4L28BZUiY8MkA8sf+KdzTt/yKEWP6flSsWMIux9Z+p7Myf/JwN4R/Iw0TDS81g
b6HTYbu4N+4mc7cHhESOh9i+Gv4oWdlwxSwJ61p+oeHJdIxYKCed09j+yvz+PLmWbOV8Wp457Re7
I/3jryjj0+/JGIu33VRU14JqyCwK7yFFnWCbwXcJi6UYauHznv7x12Ss10uWAyxdHsrTGO4nd5B7
wXYhY+H2hcD7DMhqsD8OWhSHGbJ5yE17+yNK+ce/KPPni2x7wleqBJsEUyn7N53YT2nHPqLk0vCD
x7pcVRn+PCt3iZyw/GAfu4BnhXqW5Y88Az4kMvQvvdygMENYRwtFXhZYY0rW+k/E3Mq4hYWgHAxV
FjmV4UUMdG0D0ke1l//Ig9kO7J7AxT9lwRVkRHm3KeWxJsMiIZ7u5JLNSV/6Du6A8pympqRX8mIB
FQureM8V4pTFSqWc+irgG9ZK6gUnOuW+8ZSorUXJUWG50jzFgKyW5scp9hkIJiTAAigeFrGL4NlQ
iZLLYrIAV2hMVuoH1BPyaki6KKU+IJcuOpY1c1f5LNeKX7f/i3i/wFNYPLBAIUDsyOcBO+kQaoBN
IJ+hlZDefx35CtsN/vIK7fRjV5+vKyd50HHIp8TlQe4bEF+kXgyJRzrUBfn89QlSXmi0ZEBWp/Pj
kBdHwJQfqFKAH65SJhuA8p1JUuBeuH8LJk+AtwNPIpS0jfgz6ulPe8NiIoQFvB4QGUFBPM2AjPoF
xEUQGh8x5PcK/TjFCJbw74zDDL98/D/fG0FofMuQ33aEwGA3+IW3hJHKCJbp7wQrYzrDL5cPGMHS
RaAgk4HfcvTLayb+4MKJgEgAP7R9J6Mzo+PfBGhrF0Y9RqAsqjNGMHC9AyNY3r8jOjHAkYaMQHnh
h8cJH/zrWE7GMkY1RmDCCCK4EioyHmGYGOBSyAVSkOwi43fGpQgiCAJwA7+RbWMU5k/4K+8RRFAM
XI5YyoIfNlGVrY+WpBxPqkbKUSUcT8IRq8byXbwisTipxa89xHJcE76Od0DVl89Y8CyaH24tnF42
g+HRqfzsm1IOFlUby6KisqDYlNPFcp4qfA2Lrf57xkg6cTURz03ci/NFtZPPuB+O6eMz9vaU8jhN
iWZcHhZyazBqSdko6xKXibIDy8L9lbWolspnyKYEjrJhk7uqmkaRgy6K2yHrVPgO79Jqsoiq4/Yr
edFWLO7CE7Yyyw8noJvKNf89Fec/yF3xzFXuA7nBnQYLp8o+JfJDjojDYrFSF6zdKav8cL1Rym4q
8lTkjdcmYZEW7knKwrTImz/jXnopG2VV4TRYa4tVy1LlJu2AXHF6W/E8CcKf0MTC64VQGWu/qbId
g5fWYvcdh0ENNbqSoWY38WjQ1+5IhjqdydZ/lniSwoMBC5OGmzuIa4q123jSV2jPgm1F5lZD8vIb
kX/0UjK1UU/ryvumYsnS7kFyjHyMLG1Hqcfio8lQ704y1OpMydHNyTlhlbxXCvXBfqDt3ply0lhf
joWoaSqnsQFxt4luJ4dJbX2nkqHyXaSrBOLWIUPFO0lXNp6SSjSQk8NwENSXSiBz/P3keGixnMQW
YXIbDJU6k75KW0oqyd9LtSFDlc5cV7hDN5X2OCcsJ1Oje0lXhmVWDouycfJCXd3NCeJVax84hyxd
RlByVBPOy3XQ4PBubTLU70TOaRvI2KQnl9+aDLdg1b0i6WM6kKlBP3knln0o1+umtmRuyXIbt1R+
fQP3lfwN7yLn1HVkjsML5KqS+c4R3B/LuQ78ALJs9DXgEs5tvbkL6UsncB37kZP7yxTXj+NrMonb
iKxt/aaLfMxtHuA+Qt1qicerY+wKbgO3J6oV6athCy2Gy2K5RbXg73eQc/Jq4YOc/i7KnxDEUp9k
x/gl5N3+khwPx16bZ80e2eg1NbxH/KisvSaLuy82UPFrEymHjgsJIRDHqIXk2fQ8k4gFMHml/MYh
/K6QH/t3KAt7ZvDDAnlxAgeb2nCfsfaeLJX2Pvk8mRL6k2vhJr7XBMLrHd2P7yJz6+GywQufMNNt
fcXPyzF5hWzqikPhhMeVz+OWidcoNoixMZ31c6IiuD7jyHb3VCblDPJufkE2q+F5AVcg2z3TpH7w
2rCPeExIl3roXbJPms8PWj1yz9vKpGsg7tne7S/K/fSlW1PqC8fItWqLeCnYRswm355XhfSuhVu4
bivJ88Rz5Jy0gu9fh/SVO/B9n5d6wvMUp6BdizZTUul63MZ18g4w3zOHRAaWu8bJpj8eEFNj/KZj
Da5fD/Ju+ye55m+SU+nW3hPJ+9QL5F64lQmxmEkyTN5Mg7SWHqPIOX6F+M7htyF9Ow+znAeTlckE
BwK8twsu0Y4xS8jaY4LkRbvQNzj6b273AGV9f4HLqsZ1eZCcM9ZLn+PHukyxg1gWYRAL2xOGqneR
Z/1eSiyPXzTVkPGWbtzYo3JzfYU2QhIrayjvEwdFE5h7DSXLoNGkqxHPT9J6IZZ364vy5NsfnCdC
lDw9J7K260rOOWv4SStPhuadmGT7uOLV5cn07T7CJB7L+Xj44HIpLZcs94zhp2gEuVfvkI4DaZwT
V5Lv6VdId1MCWe+eKELLI9b4Zcpn/u/bfZRVfAsytRhA6Se/YMGsIeuQCeRevIPrhBfulpO05oQh
4g5kbjuMdOWU1y7aWXOiDAgx/Z3TZB87V97+jG0aaEk8dLZByne4Xqef+kLakfLSW0yY54SwIBXy
+/YeFa2D1zs5x3Hc3OXisGgdMo7JsIgs3UdT5plzrFFAphFCZn3F9mS6fRDL/RWydBxJumj4zEeT
a9Ymsk2cI7LAkORdu58M8V3Fe8MxZSm5Zm+S32V0TF9F1uFTuD/Wku0BvBYJb6PpS645m/ieKym5
QjOyDB5NlgE8OsS0Jdf0J0WOhhadpGzHlMWyWZ/y4jGW22qyDhrLZOQ2jVnKsluqkApbcYX4wxBi
wbIvcEH2u1id4hWIppaDWFh1yNR8oLjT4tWIGNqgsbCjn/rKceXpFk31iAyRzmmryc7E8u17VcqD
xoNw3St3CRmNdXqIxwGuQdt5+WkGyURj8T0t3SdyhVuwZhomHg/Ijw5E56AueHqc09aQl+ujr9RJ
XqPoeni94knBRMB7EeQzawiUpyvbhjXRw2QdOoFMbQdS9kUjC4eFtmibkFy0WOcxoi3NncaQvnx7
qQ8eDtfcp3iYGUbG+Lsp69vzoiVhf4BYkIf8umv5jqIFXWu2UtY3F8gxg4f4hAGsqdewfE6QqdUg
MsUPIs/afZy3gmhveG6A0PBsdc1aLw+Sd99Liizvn0+pR94VkwG/OQSta2GTwsBthYsQNJxnHcpi
m4o7FxrG2mcKt6OmOBm6l+4kc+/hZGo3iLITLdIXkCEeADvfG4dg0DaYDPh1W9uQOawFe4uGxy/q
WntP4rIrczlP8wOzj0wd7iVzl2GUY/OIphefMr4ntCbqkL8PmwcdiLW/UCQjThF2t9HceYflAADG
YByacC3YzJ0wjhsynm9QhuwPPcpD4wARgOWuseL6axs8V4YzQ7vu0sG2/rPF/8k5m9Uoq2SQFJ0O
QslrIxvgvZ1VecjqqbwkZCp+xXUSk2cdJZasQ/aJC5lsY7lcHrp2MLmZnLYBM3l42cf2wTSxRzCM
OUYtkmERw4J90GzROBhyURc8ENbeU6U+nk37WDMNFy0l7ZuxVjrO9egm9WDIYBZYNA+/k2Totnad
zN8r8tM9TnzLMMTA9oFPPk7V4JCItcckMt7eh9xrtrPd1E2Ga/tgNhu6DlT8wKq0Fw3uWb2XiTaQ
7GO4bkwO96qd3MHtSFeabUbWhiCHpfNDMhTCpDC3H6mcanpkg9iW4nJUqqW4Fnk2HWRZ8NDdhO
+7
eg95Nx5kOT0k73m1D5ojbXU/sZvtqJnywEG27iXbRCPDtsIDhD6DHz4UgqU7k7dWF5blAZbtfi57
Mpd9j/wUsshtw15xu8aL5TD6wHxQJiaF+XP7WyAWVk8LXxBrH+TSVW3NbO7LbOcZQelYNrp7iKGM
WQlmP0lRrA5Zu8GolFmRzJwwe1C+oxxlpsPpy7RU8pfhODZmjTV6cJmNpHIyg+EydGU5DcfDwE4q
CTcangGWbMx5cXawttg/MILFuK7YhozVuQzc9ybOV6s3l8vaNjqO79OLP3N8+QQlTRTPVMXHCQ9N
Dckj9g4bosa6ffgaz9hw71psTLOG02lYc0sb1NmezLzqMmKUGRJrdUUe3eU3hJTZMY6V1VTS84zK
LweRAbwfyjTn8rleaIu2Lpnqs/FbFkfA4N6DmRpceSBL/h99u9oGLgf3Qb3KMQExOWAtlRRVj435
fmSo0Fnao6scL46GqKP0FWa9altF9qVbsVx7siwhU8y44YUBn/3AfsMMsK5S1i191LK4XTxbVMrC
DPdWNuI7kaEu+oDrHdy9ezCIBWwLiAwAGqG4g+A7bizfMaX1qz+/S4oAU3J/nBrPZfg/+/PjO8rw
f5Y06j2lw+SePHbjWoGXYuA6k0Ou+V1WuNHyGfkwFUa5yme5j3xWyypSXkD7+KlDurw6yeFSNZ2k
Rf0C2ih18af3y0OVA67jf15epX356ZV0QiDk9bc/L21+exBXoF5yXWmD5EfbhIxIjwcnoJ4BbS0i
b/mv3jOv7oi7Uln4jrL4umgq1Af/8/Aio4B3w05G4UQRRBAOsE8ofAokFoBN2OMMLNPDy8HLwKwx
gggKA9wARxwMuG73Yag8ul3zv/ckA6AXbpv/AAAAAElFTkSuQmCC
--_004_AAEAC27B596BDB4893A0CA9247CBB9A60F0F5E61INHEXMB01euboeh_--'
	    ,
	    self::TESTOWY_OD_GRZEGORZA_NO_HEADER => 'Return-Path: <hubaljanusz@op.pl>
Received: from smtpo79.poczta.onet.pl (141.105.16.29) (HELO smtpo79.poczta.onet.pl)
 by serwer1578643.home.pl (79.96.198.239) with SMTP (IdeaSmtpServer 0.81.1)
 id 6a3c5e533e6d59b1; Thu, 5 Jan 2017 09:16:11 +0100
Received: from pmq2v.m5r2.onet (pmq2v.m5r2.onet [10.174.32.68])
	by smtp.poczta.onet.pl (Onet) with ESMTP id 3tvL9K55FszjKpYSl
	for <G.Lachowicz@grupaglasso.pl>; Thu,  5 Jan 2017 09:16:05 +0100 (CET)
DKIM-Signature: v=1; a=rsa-sha256; c=relaxed/relaxed; d=op.pl; s=2011;
	t=1483604165; bh=XN6u6c5GU5Z2cmOeOxQA/S5LXJGQln5fPjzFKModrA4=;
	h=From:To:Date:From;
	b=Ba6k/I57IjzmFjkW5oYNUXZoB/Xd52EaD8HqPQvfqAVy3le1ho/gEsmqoMVaKS7lT
	 1ZVumSteBhKhZz7tBQ3yeCQ2BwbmtKIyCit4BUv9QDAphLi8vDWPJ+W+VmLOnkygwH
	 F36NukoflkZz/Ajsl36uLCbvla00SBxc+lzINvYM=
Content-Type: text/plain; charset="utf-8"
MIME-Version: 1.0
Content-Transfer-Encoding: quoted-printable
Received: from [188.146.18.86] by pmq2v.m5r2.onet via HTTP id
	201701050917471699010001; Thu, 05 Jan 2017 09:16:05 +0100
From: hubaljanusz@op.pl
X-Priority: 3
To: "G.Lachowicz@grupaglasso.pl " <G.Lachowicz@grupaglasso.pl>
Date: Thu, 05 Jan 2017 09:16:04 +0100
Message-Id: <127411493-f24ee6b1681137fcb5e78033d1daa21b@pmq2v.m5r2.onet>
X-Mailer: onet.poczta
X-Onet-PMQ: <hubaljanusz@op.pl>;188.146.18.86;PL;1

Panie Grzegorzu czekam na wizualizacj=C4=99 i mo=C5=BCe by=C4=87 ta statuet=
ka wi=C4=99ksz=C4=85 tak do tych 300 z=C5=82.

=20
',
	    self::TESTOWY_OD_GRZEGORZA_ENIGMA => 'Return-Path: <K.Jablonska@krispol.pl>
Received: from poczta.krispol.pl (213.17.203.50) (HELO poczta.krispol.pl)
 by serwer1578643.home.pl (79.96.198.239) with SMTP (IdeaSmtpServer 0.81.2)
 id 98454d6645b3a195; Wed, 1 Feb 2017 08:11:30 +0100
Received: from MasterFoo.local (192.168.1.107) by poczta.krispol.pl
 (192.168.1.106) with Microsoft SMTP Server (TLS) id 14.2.247.3; Wed, 1 Feb
 2017 08:11:16 +0100
Content-Type: multipart/mixed; boundary="===============1304073405=="
MIME-Version: 1.0
Message-ID: <20170201071125.29925.72658.foonsy-565464@MasterFoo.local>
Subject: =?utf-8?q?Fwd=3A_Re=3A_Fwd=3A_RE=3A_RE=3A_PD=3A_RE=3A_Giganci_Krispol-_re?=
 =?utf-8?q?alizacja_zam=C3=B3wienia?=
To: =?utf-8?q?Grzegorz_Lachowicz_=40_SUPERFIRMA=2EPL_Grzegorz?=
	<email@serwer.pl>
From: =?utf-8?b?SmFixYJvxYRza2EgS2F0YXJ6eW5h?= <k.jablonska@krispol.pl>
Organization: =?utf-8?q?KRISPOL_Sp=2E_z_o=2Eo=2E?=
Date: Wed, 1 Feb 2017 08:11:25 +0100
References: <20170131150034.7728.44899.foonsy-565322@MasterFoo.local>
In-Reply-To: <20170131150034.7728.44899.foonsy-565322@MasterFoo.local>
X-Forwarded-Message-Id: <20170131150034.7728.44899.foonsy-565322@MasterFoo.local>
User-agent: Foonsy ERP
Return-Path: k.jablonska@krispol.pl
X-Originating-IP: [192.168.1.107]

--===============1304073405==
Content-Type: multipart/alternative; boundary="===============0656941856=="
MIME-Version: 1.0

--===============0656941856==
MIME-Version: 1.0
Content-Type: text/plain; charset="utf-8"
Content-Transfer-Encoding: base64

ICAKCgpLYXRhcnp5bmEgSmFixYJvxYRza2EKClNwZWNqYWxpc3RhIGRzLiBNYXJrZXRpbmd1CgpF
OiBrLmphYmxvbnNrYUBrcmlzcG9sLnBsCgpUZWwuIDUwOSAyMDAgNDY2CgogIAoKT2Q6IEvFgm9z
b3dza2kgUHJ6ZW15c8WCYXcKW1twLmtsb3Nvd3NraUBrcmlzcG9sLnBsXShtYWlsdG86cC5rbG9z

--===============0656941856==
Content-Type: multipart/related; boundary="===============0471621342=="
MIME-Version: 1.0

--===============0471621342==
MIME-Version: 1.0
Content-Type: text/html; charset="utf-8"
Content-Transfer-Encoding: base64

dDtmb250LWZhbWlseTpUaW1lcyBOZXcgUm9tYW47Zm9udC1zaXplOjEwcHQ7Zm9udC13ZWlnaHQ6
bm9ybWFsO2ZvbnQtc3R5bGU6bm9ybWFsO3RleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lOyI+PHNw
--===============0471621342==
Content-Type: image/gif
MIME-Version: 1.0
Content-Transfer-Encoding: base64
Content-ID: <image001.gif@01D2788E.2435CB00>
Content-Disposition: inline; filename="image001.gif"
Content-Length: 9378

R0lGODlhyAGlAIf/ANq4l7WGPNCVNNWndNmnWbl3OMGIVdWcN7BqK9eoaMqWZNSqgfbt4suUV+3G
dsaJSPry48aFMM6SM76CSuC8it+qO9ynOvPXnezUq+DEqs+iebdtKs+WTNq1ieS7aLx4LfTq3/Ha

--===============0471621342==
Content-Type: image/png
MIME-Version: 1.0
Content-Transfer-Encoding: base64
Content-ID: <foonsy_image_0@636214751957033940>
Content-Disposition: inline; filename="foonsy_image_0.png"
Content-Length: 143

iVBORw0KGgoAAAANSUhEUgAAAAIAAAACCAYAAABytg0kAAAABGdBTUEAALGPC/xhBQAAAAlwSFlz
AAAOwwAADsMBx2+oZAAAABl0RVh0U29mdHdhcmUATWljcm9zb2Z0IE9mZmljZX/tNXEAAAAMSURB
VBhXY0ADDAwAABIAAaMn47oAAAAASUVORK5CYII=
--===============0471621342==--

--===============0656941856==--

--===============1304073405==
Content-Type: application/pdf;
	name="=?utf-8?q?statuetki=5Fmagazyn_wysy=C5=82ek=2Epdf?="
MIME-Version: 1.0
Content-Transfer-Encoding: base64
Content-Disposition: attachment;
	filename="=?utf-8?q?statuetki=5Fmagazyn_wysy=C5=82ek=2Epdf?="
Content-Length: 974914

JVBERi0xLjQNJeLjz9MNCjQgMCBvYmoNPDwvTGluZWFyaXplZCAxL0wgOTc0OTE0L08gNi9FIDky
NDE3NS9OIDEvVCA5NzQ3MTUvSCBbIDEzMzYgMjI5XT4+DWVuZG9iag0gICAgICAgICAgICAgICAg


--===============1304073405==--
'
    );

}
