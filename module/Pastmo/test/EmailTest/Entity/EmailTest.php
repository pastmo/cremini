<?php

use Pastmo\Testy\Util\TB;

class EmailTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    private $nadawca = "Paweł Kaźmierczak <pawel.kazmierczak@pastmo.pl>";
    private $nadawca2 = "home.pl <info@mailing.home.pl>";
    private $email;
    private $projekt;

    public function setUp() {
	parent::setUp();
	$this->email = new \Pastmo\Email\Entity\Email();
	$this->email->nadawca = $this->nadawca;
    }

    public function testgetNadawcaEmail() {
	$wynik = $this->email->getNadawcaEmail();
	$this->assertEquals("pawel.kazmierczak@pastmo.pl", $wynik);
    }

    public function testgetNadawcaImie() {
	$wynik = $this->email->getNadawcaImie();
	$this->assertEquals("Paweł", $wynik);
    }

    public function testgetNadawcaNazwisko() {
	$wynik = $this->email->getNadawcaNazwisko();
	$this->assertEquals("Kaźmierczak", $wynik);
    }

    public function testgetNadawcaEmail2() {
	$this->email->nadawca = $this->nadawca2;
	$wynik = $this->email->getNadawcaEmail();
	$this->assertEquals("info@mailing.home.pl", $wynik);
    }

    public function testgetNadawcaImie2() {
	$this->email->nadawca = $this->nadawca2;
	$wynik = $this->email->getNadawcaImie();
	$this->assertEquals("home.pl", $wynik);
    }

    public function testgetNadawcaNazwisko2() {
	$this->email->nadawca = $this->nadawca2;
	$wynik = $this->email->getNadawcaNazwisko();
	$this->assertEquals("", $wynik);
    }

    public function test_getUzytkownika() {
	$uzytkownik = $this->email->getUzytkownika();
	$this->assertNull($uzytkownik);
    }

    public function test_getUzytkownika_wiadomosc() {
	$this->dodajWiadomoscDoEmaila();
	$uzytkownik = $this->email->getUzytkownika();
	$this->assertNull($uzytkownik);
    }

    public function test_getUzytkownika_autor() {
	$this->dodajWiadomoscDoEmaila();
	$this->dodajAutora();
	$uzytkownik = $this->email->getUzytkownika();
	$this->assertNotNull($uzytkownik);
    }

    public function test_getProjekt() {
	$wynik = $this->email->getProjekt();
	$this->assertInstanceOf(Projekty\Entity\Projekt::class, $wynik);
    }

    public function test_getProjektZWatkiem() {
	$this->dodajWiadomoscDoEmaila();
	$this->dodajWatek();
	$this->mockujpobierzWlasciciela();

	$wynik = $this->email->getProjekt();

	$this->assertEquals(__CLASS__, $wynik->nazwa);
    }

    public function test_pobierzDate() {
	$this->email->data_naglowka = "Wed, 28 Apr 2010 21:59:49 -0400";
	$wynik = $this->email->pobierzDate();
	$this->assertEquals("2010-04-28 21:59:49", $wynik);
    }

    public function test_pobierzDate_przyklad_z_istniejacego_maila() {
	$this->email->data_naglowka = "Mon, 18 Jul 2016 16:33:48 +0200";
	$wynik = $this->email->pobierzDate();
	$this->assertEquals("2016-07-18 16:33:48", $wynik);
    }

    public function test_pobierzDate_przyklad_z_istniejacego_maila_cest() {
	$this->email->data_naglowka = "Thu, 12 May 2016 15:26:04 +0200 (CEST)";
	$wynik = $this->email->pobierzDate();
	$this->assertEquals("2016-05-12 15:26:04", $wynik);
    }

    private function dodajWiadomoscDoEmaila() {
	$this->email->wiadomosc = TB::create(\Konwersacje\Entity\Wiadomosc::class, $this)->make();
    }

    private function dodajWatek() {
	$this->email->wiadomosc->watek = TB::create(\Konwersacje\Entity\WatekWiadomosci::class, $this)->make();
    }

    private function dodajAutora() {
	$this->email->wiadomosc->autor = TB::create(\Logowanie\Model\Uzytkownik::class, $this)->make();
    }

    private function mockujpobierzWlasciciela() {
	$this->projekt = TB::create(Projekty\Entity\Projekt::class, $this)
		->setParameters(array('nazwa' => __CLASS__))
		->make();

	$this->watkiWiadomosciTable->expects($this->once())->method('pobierzWlasciciela')->
		willReturn($this->projekt);
    }

}
