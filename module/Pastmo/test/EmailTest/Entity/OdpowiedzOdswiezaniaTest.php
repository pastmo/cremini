<?php

namespace EmailTest\Entity;

use Pastmo\Email\Entity\OdpowiedzOdswiezania;

class OdpowiedzOdswiezaniaTest extends \Testy\BazoweKlasyTestow\AbstractMockTest {

    public function test_jsonSerialize() {
	$odpowiedzOdswiezania = OdpowiedzOdswiezania::create()
		->addPrzetworzoneMaile(42, __CLASS__)
		->addToListaPobranychMaili("To nie powinno sie wyswietlac");
	$wynik = json_encode($odpowiedzOdswiezania);
	$this->sprawdzStringNieZawiera($wynik, "To nie powinno sie wyswietlac");
    }

}
