<?php

namespace PastmoTest\Email\Helper;

use Application\Controller\LadowaczSkryptow;

class WyswietlGadzetEmailTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    private $wyswietlGadzetEmail;

    public function setUp() {
	parent::setUp();

	$this->wyswietlGadzetEmail = new \Email\Helper\WyswietlGadzetEmail($this->sm);

	$this->view = new \Pastmo\Testy\Mocki\ViewMock();
	LadowaczSkryptow::$skrypty = array();
    }

    public function test_dodania_skryptow() {

	$this->wyswietlGadzetEmail->__invoke($this->view);

	$skrypty = LadowaczSkryptow::$skrypty;
	$this->assertContains('js/pastmo/email/odswiezacz_email.js', $skrypty);
    }

    public function testWywolania() {

	$this->wyswietlGadzetEmail->__invoke($this->view);
	$this->sprawdzCzyStatus200();
    }

}
