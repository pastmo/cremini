<?php

namespace PastmoTest\Testy;

class TestBuilderWrapperIntegracyjnyTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    public function setUp() {
	parent::setUp();
    }

    public function test_makeEntityBuilder() {
	$builder = $this->makeEntityBuilder(\Projekty\Entity\Projekt::class);

	$this->assertNotNull($builder);
    }

    public function test_setDefaultFactoryParameters() {

	$this->assertEquals(\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::class,get_class ($this->defaultFactoryParameters));
    }

}
