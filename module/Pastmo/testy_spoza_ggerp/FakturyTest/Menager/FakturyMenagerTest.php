<?php

namespace ProjektyTest\Menager;

use \Projekty\Entity\Projekt;

class FakturyMenagerTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    protected $manager;

    public function setUp() {
	parent::setUp();

	$this->ustawMockaGateway(\Pastmo\Faktury\KonfiguracjaModulu::FAKTURY_GATEWAY);
	$this->menager = new \Pastmo\Faktury\Menager\FakturyMenager($this->sm);
    }

    public function testutworzNumerNowejFaktury() {
	$this->obslugaTestowanejKlasy->ustawSelectWithCount('363');

	$numer = $this->menager->utworzNumerNowejFaktury();

	$this->assertEquals("FVS/364/2016", $numer);
    }

}
