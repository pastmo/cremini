<?php

namespace WystawcaFakturTest\Integracyjne;

class FakturyMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $menager;

    public function setUp() {
	parent::setUp();

	$this->menager = $this->sm->get(\Pastmo\Faktury\Menager\FakturyMenager::class);
    }

    public function testzrobSzybkaFakture() {
	$this->ustawZalogowanegoUseraId1();
	$klient = $this->zrobKlienta();
	$netto = '25';
	$termin = '2016-09-12';
	$data_wykonania = '2016-09-30';
	$vat = "23";
	$slownie = "Cała masa kasy";

	$faktura = $this->menager->zrobSzybkaFakture($klient->id, $netto, $vat, $termin, $data_wykonania, $slownie);

	$this->assertNotNull($faktura);
	$this->assertNotNull($faktura->nr_faktury);
	$this->assertNotNull($faktura->rok);
	$this->assertEquals($vat, $faktura->vat);
	$this->assertEquals($data_wykonania, $faktura->data_wykonania);
	$this->assertEquals($slownie, $faktura->slownie);
    }

    private function zrobKlienta() {
	$klient = new \Pastmo\Faktury\Entity\Klient();
	$klient->nazwa = "nazwa";
	$this->sm->get(\Pastmo\Faktury\Menager\KlienciMenager::class)->zapisz($klient);
	$dodany = $this->sm->get(\Pastmo\Faktury\Menager\KlienciMenager::class)->getPoprzednioDodany();
	return $dodany;
    }

}
