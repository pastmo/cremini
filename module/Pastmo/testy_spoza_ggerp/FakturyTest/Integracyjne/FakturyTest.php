<?php

namespace WystawcaFakturTest\Integracyjne;

class FakturyTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $menager;

    public function setUp() {
	parent::setUp();

	$this->menager = $this->sm->get(\Pastmo\Faktury\Menager\FakturyMenager::class);
    }

    public function testZapisu() {

	$today = new \DateTime();
	$dzisiejszaData = $today->format('Y-m-d');

	$wystawca = \FabrykaRekordow::makeEncje(\Pastmo\Faktury\Entity\WystawcaFaktur::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm));


	$faktura = new \Pastmo\Faktury\Entity\Faktura();
	$faktura->data_wykonania = $dzisiejszaData;
	$faktura->data_wystawienia = $dzisiejszaData;
	$faktura->termin_platnosci = $dzisiejszaData;
	$faktura->nr_faktury = "nr faktury";
	$faktura->miejsce_wystawienia = "Wrocław";
	$faktura->kwota_netto = 42.99;
	$faktura->zaplacono = 1024;
	$faktura->rok = 2016;
	$faktura->wystawca = $wystawca;
	$faktura->vat = "22";
	$faktura->slownie = "slownie";


	$this->menager->zapisz($faktura);
	$dodany = $this->menager->getPoprzednioDodany();

	$this->assertEquals($faktura->nr_faktury, $dodany->nr_faktury);
	$this->assertEquals($faktura->data_wystawienia, $dodany->data_wystawienia);
	$this->assertEquals($faktura->data_wykonania, $dodany->data_wykonania);
	$this->assertEquals($faktura->termin_platnosci, $dodany->termin_platnosci);
	$this->assertEquals($faktura->miejsce_wystawienia, $dodany->miejsce_wystawienia);
	$this->assertEquals($faktura->kwota_netto, $dodany->kwota_netto);
	$this->assertEquals($faktura->zaplacono, $dodany->zaplacono);
	$this->assertEquals($faktura->rok, $dodany->rok);
	$this->assertEquals($faktura->vat, $dodany->vat);
	$this->assertEquals($faktura->slownie, $dodany->slownie);
	$this->assertNotNull($dodany->wystawca->id);
    }

}
