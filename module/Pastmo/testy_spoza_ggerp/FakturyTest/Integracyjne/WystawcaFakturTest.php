<?php

namespace WystawcaFakturTest\Integracyjne;

class WystawcaFakturTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $menager;

    public function setUp() {
	parent::setUp();

	$this->menager = $this->sm->get(\Pastmo\Faktury\Menager\WystawcyFakturMenager::class);
    }

    public function testZapisu() {
	$adres = \FabrykaRekordow::makeEncje(\Firmy\Entity\Adres::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm));

	$wystawcaFaktury = \FabrykaRekordow::makeEncje(\Pastmo\Faktury\Entity\WystawcaFaktur::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneBrakzapisuNiepowiazane::create());

	$wystawcaFaktury->adres = $adres;

	$this->menager->zapisz($wystawcaFaktury);

	$dodany = $this->menager->getPoprzednioDodany();

	$this->assertEquals($wystawcaFaktury->imie_nazwisko, $dodany->imie_nazwisko);
	$this->assertEquals($wystawcaFaktury->miejsce_wystawienia, $dodany->miejsce_wystawienia);
	$this->assertEquals($wystawcaFaktury->nazwa, $dodany->nazwa);
	$this->assertEquals($wystawcaFaktury->nip, $dodany->nip);
	$this->assertNotNull($wystawcaFaktury->adres->id);
    }

}
