<?php

namespace PastmoTest\Faktury\Entity;

class FakturaTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    private $faktura;

    public function setUp() {
	parent::setUp();

	$this->faktura = new \Pastmo\Faktury\Entity\Faktura($this->sm);
	$this->faktura->kwota_netto = 33;
	$this->faktura->vat = 22;
    }

    public function test_getKwoteNetto() {

	$wynik = $this->faktura->getKwoteNetto();

	$this->assertEquals('33,00', $wynik);
    }

    public function test_getKwotaVat() {

	$wynik = $this->faktura->getKwotaVat();

	$this->assertEquals('7,26', $wynik);
    }

    public function test_getKwotaBrutto() {

	$wynik = $this->faktura->getKwotaBrutto();

	$this->assertEquals('40,26', $wynik);
    }

}
