<?php

namespace Application\Controller;

class LadowaczSkryptow {

    const WERSJA = "0_02_2";

    static $skrypty = array();
    static $cssy = array();

    public static function add($skrypt) {

	if (!in_array($skrypt, self::$skrypty)) {
	    self::$skrypty[] = $skrypt;
	}
    }

    public static function addCss($css) {

	if (!in_array($css, self::$cssy)) {
	    self::$cssy[] = $css;
	}
    }

    public static function wyswietl() {
	foreach (self::$skrypty as $skrypt) {
	    echo '<script src="' . $skrypt . '?wersja=' . self::WERSJA . ' "></script>' . PHP_EOL;
	}
    }

    public static function wyswietlCssy() {
	foreach (self::$cssy as $css) {
	    echo '<link href="' . $css . '?wersja=' . self::WERSJA . '" rel="stylesheet">' . PHP_EOL;
	}
    }

}
