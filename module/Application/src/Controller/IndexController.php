<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

class IndexController extends \Wspolne\Controller\WspolneController {

    public function indexAction() {

	$layout = $this->layout();
	$layout->setTemplate('logowanie/layout/start');
//	return $this->redirect()->toRoute('pulpit');
    }

}
