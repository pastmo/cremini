<?php

namespace Application;

class AktywneModulyZPastmo {

    public static function pobierzAktywneModuly() {
	return array(
		new \Pastmo\Wspolne\KonfiguracjaModulu(),
		new \Pastmo\Sesja\KonfiguracjaModulu(),
		new \Pastmo\Wiadomosci\KonfiguracjaModulu(),
		new \Pastmo\Email\KonfiguracjaModulu(),
		new \Pastmo\Uzytkownicy\KonfiguracjaModulu(),
		new \Pastmo\Pdf\KonfiguracjaModulu(),
		new \Pastmo\Faktury\KonfiguracjaModulu(),
		new \Pastmo\Zasoby\KonfiguracjaModulu(),
		new \Pastmo\Logi\KonfiguracjaModulu(),
		new \Pastmo\Lokalizacje\KonfiguracjaModulu(),
		new \Pastmo\Wiki\KonfiguracjaModulu(),
	);
    }

}
