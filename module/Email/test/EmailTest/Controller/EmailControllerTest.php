<?php

namespace EmailTest\Controller;

use Pastmo\Testy\Util\TB;

class EmailControllerTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

    protected $traceError = true;
    private $kontoMailowe;

    public function setUp() {
	parent::setUp();
	$this->kontoMailowe = TB::create(\Pastmo\Email\Entity\KontoMailowe::class,
			$this)->make();
	$this->obslugaKlasObcych->ustawZalogowanegoUsera();
    }

    public function testIndex() {
	$this->mockiDlaIndexu(array($this->kontoMailowe));
	$this->dispatch('/email');
	$this->sprawdzCzyStatus200();
    }
    public function testIndex_brak_skrzynek() {
	$this->mockiDlaIndexu(array());
	$this->dispatch('/email');
	$this->sprawdzCzyStatus200();
    }

    public function testIndex_route() {
	$this->mockiDlaIndexu(array($this->kontoMailowe));
	$this->dispatch('/email/0/folder1');

//	$this->sprawdzCzyStatus200();
    }

    public function testusunFolderAction() {
	$this->emaileFolderyMenager
		->expects($this->once())
		->method('usunPoId')
		->with($this->equalTo('42'));

	$this->dispatch('/email/usun_folder/42');

	$this->sprawdzCzyPrzekierowanie();
    }

    public function testudodajFolder() {
	$this->ustawParametryPosta(array('nazwa'=>__METHOD__));

	$this->emaileFolderyMenager
		->expects($this->once())
		->method('dodajFolder')
		->with($this->equalTo('42'),$this->equalTo(__METHOD__));

	$this->dispatch('/email/dodaj_folder/42');

	$this->sprawdzCzySukces();
    }

    private function mockiDlaIndexu($kontaMailowe) {
	$this->kontaMailoweManager->expects($this->exactly(3))->method('pobierzKontaMailoweUzytkownika')->willReturn($kontaMailowe);
	$this->emaileMenager->expects($this->once())->method('pobierzEmaileDlaKontaISkrzynki')->willReturn(array());
	$this->uzytkownicyProjektyMenager->expects($this->any())->method('pobierzUzytkownicyProjektyWgStatusu')->willReturn(array());
    }

}
