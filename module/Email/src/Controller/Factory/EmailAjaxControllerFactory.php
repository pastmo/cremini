<?php

namespace Email\Controller\Factory;

use Email\Controller\KonwersacjeController;

class EmailAjaxControllerFactory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

	protected function newController() {
		return new \Email\Controller\EmailAjaxController();
	}

}
