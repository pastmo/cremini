<?php

namespace UzytkownicyTest\Integracyjne;

use Pastmo\Testy\Util\TB;

class KontaMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    public function setUp() {
	parent::setUp();
    }

    public function test_dodajEncje() {
	$konto = TB::create(\Uzytkownicy\Entity\Konto::class, $this)
		->setPF_IZP($this->sm)
		->make();

	$this->assertNotNull($konto->id);
	$this->sprawdzCzyUtworzonyFolderDlaKonta($konto->id);
    }

    private function sprawdzCzyUtworzonyFolderDlaKonta($id) {
	$menager = $this->sm->get(\Zasoby\Menager\ZasobyDownloadMenager::class);
	$fullPath = $menager->zasobRealPathZeStringa($id);
	$this->assertTrue(file_exists($fullPath));
    }

}
