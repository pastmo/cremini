<?php

namespace Uzytkownicy\Testy\FabrykiKonkretnychEncji;

class UzytkownikFactory extends \Pastmo\Testy\FabrykiKonkretnychEncji\UzytkownikFactory {

    public function dowiazInneTabele() {
	parent::dowiazInneTabele();
	$this->encja->konto = $this->uzyjWlasciwejFabryki(\Uzytkownicy\Entity\Konto::class);
    }

}
