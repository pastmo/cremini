<?php

namespace Uzytkownicy;

use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface {

    use \Pastmo\FunkcjeKonfiguracyjne;

    const KONTA_GATEWAY = "KONTA_GATEWAY";

    public function getConfig() {
	return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig() {
	return array(
		'factories' => array(
			Menager\KontaMenager::class => function($sm) {
			    $menager = new Menager\KontaMenager($sm);
			    return $menager;
			},
			self::KONTA_GATEWAY => function ($sm) {
			    return $this->ustawGateway($sm, Entity\Konto::class, 'konta');
			}
		),
	);
    }

}
