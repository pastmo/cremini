<?php

namespace Uzytkownicy\Menager;

class KontaMenager extends \Pastmo\Wspolne\Menager\WspolnyMenagerBazodanowy {

    protected $wyszukiwanieZKontem = false;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm, \Uzytkownicy\Module::KONTA_GATEWAY);
    }

    public function zapisz(\Pastmo\Wspolne\Entity\WspolnyModel $model) {
	parent::zapisz($model);

	if (!$model->id) {
	    $zapisany = $this->getPoprzednioDodany();
	    $zasobyUploadMenager = $this->get(\Zasoby\Menager\ZasobyUploadMenager::class);
	    $zasobyUploadMenager->zrobFolder($zapisany->id);
	}
    }

}
