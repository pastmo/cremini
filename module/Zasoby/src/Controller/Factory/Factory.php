<?php

namespace Zasoby\Controller\Factory;

use Zasoby\Controller\ZasobyController;

class Factory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

    protected function newController() {
	return new ZasobyController();
    }

}
