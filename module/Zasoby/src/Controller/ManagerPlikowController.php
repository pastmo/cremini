<?php

namespace Zasoby\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class ManagerPlikowController extends \Wspolne\Controller\WspolneController {

    public function indexAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$folderId = (int) $this->params()->fromRoute('id', 0);
	$uzytkownik = $this->uzytkownikTable->getZalogowanegoUsera();

	$pliki = $this->zasobyFasada->pobierzPlikiUzytkownika($uzytkownik->id, $folderId);
	$foldery = $this->zasobyFasada->pobierzFolderyUzytkownika($uzytkownik->id, $folderId);
	$folderNadrzednyId = $this->zasobyFasada->pobierzIdFolderuNadrzednego($folderId);
	$uzytkownicy = $this->uzytkownikTable->pobierzZWherem("id != $uzytkownik->id");

	return new ViewModel(array(
		'pliki' => $pliki,
		'foldery' => $foldery,
		'folderId' => $folderId,
		'folderNadrzednyId' => $folderNadrzednyId,
		'uzytkownicy' => $uzytkownicy
	));
    }

    public function indexAkcjeAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$uzytkownik = $this->uzytkownikTable->getZalogowanegoUsera();
	$folderId = 0;

	$request = $this->getRequest();
	if ($request->isPost()) {
	    $post = $request->getPost();
	    switch ($post->submit) {
		case 'dodaj_nowy_folder':
		    $this->zasobyFasada->dodajNowyFolder($post->folder_nadrzedny_id, $post->nazwa, $uzytkownik->id);
		    $folderId = $post->folder_nadrzedny_id;
		    break;
		case 'usun_zasob':
		    $stan = $this->zasobyFasada->usunUdostepnionyPlik($post['file_id']);
		    if ($stan)
			return $this->returnSuccess();
		    return $this->returnFail(['msg' => "Nie można usunąć - plik jest udostępniany innym użytkownikom."]);
		case 'usun_folder':
		    $this->zasobyFasada->usunUdostepnionyFolder($post['file_id']);
		    return $this->returnSuccess();
		case 'udostepnij_zasob':
		    $this->zasobyFasada->udostepnijZasob($post->uzytkownicy_id, $post->file_id);
		    $folderId = $post->folder_id;
		    break;
	    }
	}

	return $this->redirect()->toRoute('manager_plikow', array('action' => 'index', 'id' => $folderId));
    }

    public function udostepnioneAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);
	$uzytkownik = $this->uzytkownikTable->getZalogowanegoUsera();
	$pliki = $this->zasobyFasada->pobierzUdostepnionePlikiDlaMnie($uzytkownik->id);

	return new ViewModel(array(
		'pliki' => $pliki
	));
    }

    public function udostepnianeAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$request = $this->getRequest();
	if ($request->isPost()) {
	    $post = $request->getPost();
	    $this->zasobyFasada->przestanUdostepniacZasob($post->submit, $post->userId);
	}

	$uzytkownik = $this->uzytkownikTable->getZalogowanegoUsera();
	$pliki = $this->zasobyFasada->pobierzUdostepnianeUzytkownikomZasoby($uzytkownik->id);

	return new ViewModel(array(
		'pliki' => $pliki
	));
    }

    public function uploadPlikAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);
	$uzytkownik = $this->uzytkownikTable->getZalogowanegoUsera();

	$request = $this->getRequest();
	if ($request->isPost()) {
	    $post = $request->getPost();
	    $zasoby = $this->zasobyFasada->zaladujPlik();
	    $zwrotne = $this->zasobyFasada->dodajZasobyUzytkownika($zasoby, $uzytkownik->id, $post->folder_id);
	    if (!empty($zwrotne)) {
		echo json_encode(['files' => $this->stworzFileclass($zwrotne)]);
		exit();
	    }
	}
	return false;
    }

    public function sprawdzKomuUdostepnionoAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);
	$uzytkownik = $this->uzytkownikTable->getZalogowanegoUsera();

	$request = $this->getRequest();
	if ($request->isPost()) {
	    $post = $request->getPost();
	    $usersArray = $this->zasobyFasada->sprawdzKomuUdostepniam($post['fileId'], $uzytkownik->id);
	    return $this->returnSuccess(['usersId' => $usersArray]);
	}
	return $this->returnFail();
    }

    private function stworzFileclass($zasobyUdostepnione) {
	$zasoby = [];
	foreach ($zasobyUdostepnione as $udo) {
	    $zasob = $udo->zasob;
	    $fileclass = new \Zasoby\Entity\FileClass();
	    $fileclass->name = $zasob->nazwa;
	    $fileclass->size = $zasob->rozmiar;
	    $fileclass->url = $this->zasobyFasada->downloadZasobResponse($zasob->id);
	    $fileclass->thumbnailUrl = $zasob->wyswietl();

	    $zasoby[] = $fileclass;
	}
	return $zasoby;
    }

}
