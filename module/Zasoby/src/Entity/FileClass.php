<?php

namespace Zasoby\Entity;

class FileClass {

    public $name;
    public $size;
    public $url;
    public $thumbnailUrl;
    public $deleteUrl;
    public $deleteType;

}
