<?php

namespace Zasoby\Fasada;

class ZasobyFasada extends \Pastmo\Zasoby\Fasada\ZasobyFasada {

    protected $udostepnioneZasobyMenager;
    protected $udostepnioneZasobyFolderyMenager;
    protected $udostepnioneZasobyUzytkownicyMenager;

    public function __construct(\Zend\ServiceManager\ServiceManager $serviceMenager) {
	parent::__construct($serviceMenager);

	$this->udostepnioneZasobyMenager = $serviceMenager->get(\Zasoby\Menager\UdostepnioneZasobyMenager::class);
	$this->udostepnioneZasobyFolderyMenager = $serviceMenager->get(\Zasoby\Menager\UdostepnioneZasobyFolderyMenager::class);
	$this->udostepnioneZasobyUzytkownicyMenager = $serviceMenager->get(\Zasoby\Menager\UdostepnioneZasobyUzytkownicyMenager::class);
    }

    public function pobierzPlikiUzytkownika($uzytkownik_id, $folder_id) {
	return $this->udostepnioneZasobyMenager->pobierzPlikiUzytkownika($uzytkownik_id, $folder_id);
    }

    public function pobierzFolderyUzytkownika($uzytkownik_id, $folder_id) {
	return $this->udostepnioneZasobyFolderyMenager->pobierzFolderyUzytkownika($uzytkownik_id, $folder_id);
    }

    public function dodajZasobyUzytkownika($zasoby, $uzytkownik_id, $folder_id) {
	return $this->udostepnioneZasobyMenager->dodajZasobyUzytkownika($zasoby, $uzytkownik_id, $folder_id);
    }

    public function pobierzIdFolderuNadrzednego($folderId) {
	return $this->udostepnioneZasobyFolderyMenager->pobierzIdFolderuNadrzednego($folderId);
    }

    public function dodajNowyFolder($folderNadrzedny, $nazwa, $uzytkownik) {
	$this->udostepnioneZasobyFolderyMenager->dodajNowyFolder($folderNadrzedny, $nazwa, $uzytkownik);
    }

    public function usunUdostepnionyPlik($udostepnionyZasobId) {
	return $this->udostepnioneZasobyMenager->usunUdostepnionyPlik($udostepnionyZasobId);
    }

    public function usunUdostepnionyFolder($udostepnionyFolderId) {
	$this->udostepnioneZasobyMenager->usunUdostepnionyFolder($udostepnionyFolderId);
    }

    public function udostepnijZasob($uzytkownicyId, $udostepnionyZasobId) {
	$this->udostepnioneZasobyUzytkownicyMenager->udostepnijZasob($uzytkownicyId, $udostepnionyZasobId);
    }

    public function przestanUdostepniacZasob($udostepnionyZasobId, $uzytkownikId) {
	$this->udostepnioneZasobyUzytkownicyMenager->przestanUdostepniacZasob($udostepnionyZasobId, $uzytkownikId);
    }

    public function pobierzUdostepnionePlikiDlaMnie($uzytkownikId) {
	return $this->udostepnioneZasobyUzytkownicyMenager->pobierzUdostepnionePlikiDlaMnie($uzytkownikId);
    }

    public function pobierzPlikiKtoreUdostepniam($uzytkownikId) {
	return $this->udostepnioneZasobyUzytkownicyMenager->pobierzPlikiKtoreUdostepniam($uzytkownikId);
    }

    public function sprawdzKomuUdostepniam($udostepnionyZasobId, $wlascicielId) {
	return $this->udostepnioneZasobyUzytkownicyMenager->sprawdzKomuUdostepniam($udostepnionyZasobId,
			$wlascicielId);
    }

    public function pobierzUdostepnianeUzytkownikomZasoby($uzytkownikId) {
	return $this->udostepnioneZasobyMenager->pobierzUdostepnianeUzytkownikomZasoby($uzytkownikId);
    }

}
