<?php

namespace Zasoby\Menager;

use Zend\ServiceManager\ServiceManager;

class UdostepnioneZasobyFolderyMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, \Zasoby\Module::UDOSTEPNIONE_ZASOBY_FOLDERY_GATEWAY);
    }

    public function pobierzFolderyUzytkownika($uzytkownik_id, $folder_id) {

	if (empty($folder_id)) {
	    $query = "uzytkownik_id = $uzytkownik_id AND folder_nadrzedny_id IS NULL";
	} else {
	    $query = "uzytkownik_id = $uzytkownik_id AND folder_nadrzedny_id = $folder_id";
	}

	$foldery = $this->pobierzZWherem($query);

	usort($foldery, function($a, $b) {
	    return strcmp($a->nazwa, $b->nazwa);
	});

	return $foldery;
    }

    public function pobierzIdFolderuNadrzednego($folderId) {
	$id = 0;
	if ($folderId) {
	    $folder = $this->getRekord($folderId);
	    if (isset($folder->folder_nadrzedny))
		$id = $folder->folder_nadrzedny;
	}
	return $id;
    }

    public function dodajNowyFolder($folderNadrzedny, $nazwa, $uzytkownik) {

	try {
	    $folder = new \Zasoby\Entity\UdostepnionyZasobFolder();
	    if (!empty($folderNadrzedny))
		$folder->folder_nadrzedny = $folderNadrzedny;
	    $folder->nazwa = $nazwa;
	    $folder->uzytkownik = $uzytkownik;
	    $this->zapisz($folder);
	} catch (Exception $ex) {
	    echo $ex->getMessage();
	}
    }

    public function sprawdzPodfoldery($folderId) {
	$podfoldery = $this->pobierzZWherem("folder_nadrzedny_id = $folderId");
	if (count($podfoldery)) {
	    return $podfoldery;
	} else {
	    return false;
	}
    }

    public function usunFolder($folderId) {
	$this->usunPoId($folderId);
    }

}
