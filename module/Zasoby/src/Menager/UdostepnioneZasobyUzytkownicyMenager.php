<?php

namespace Zasoby\Menager;

use Zend\ServiceManager\ServiceManager;

class UdostepnioneZasobyUzytkownicyMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, \Zasoby\Module::UDOSTEPNIONE_ZASOBY_UZYTKOWNICY_GATEWAY);
    }

    public function udostepnijZasob($uzytkownicyId, $udostepnionyZasobId) {
	try {
	    $this->beginTransaction();
	    if (isset($uzytkownicyId)) {
		$this->usunOdznaczonychUzytkownikow($uzytkownicyId, $udostepnionyZasobId);
		$this->udostepnijJesliNieIstnieje($uzytkownicyId, $udostepnionyZasobId);
	    } else {
		$this->usunWszystkichUzytkownikow($udostepnionyZasobId);
	    }
	    $this->commit();
	} catch (Exception $ex) {
	    $this->rollback();
	}
    }

    public function sprawdzKomuUdostepniam($udostepnionyZasobId, $wlascicielId) {

	$pliki = $this->pobierzPlikiKtoreUdostepniam($wlascicielId);
	$uzytkownicyArray = [];

	foreach ($pliki as $udostepnionyUzytkownik) {
	    if ($udostepnionyUzytkownik->udostepniony_zasob->id === $udostepnionyZasobId)
		$uzytkownicyArray[] = $udostepnionyUzytkownik->uzytkownik->id;
	}

	return $uzytkownicyArray;
    }

    public function pobierzUdostepnionePlikiDlaMnie($uzytkownikId) {
	$pliki = $this->pobierzZWherem("uzytkownik_id = $uzytkownikId");
	return $pliki;
    }

    public function pobierzPlikiKtoreUdostepniam($uzytkownikId) {
	$pliki = $this->pobierzZWherem("udostepniony_zasob_id IN (select id from udostepnione_zasoby where wlasciciel_id = $uzytkownikId)");
	return $pliki;
    }

    public function dowiazanieUzytkownikowDoPliku($udostepnionyZasobId) {
	$udostepnioneZasobyUzytkownicy = $this->pobierzZWherem("udostepniony_zasob_id = $udostepnionyZasobId");
	$uzytkownicy = [];
	foreach ($udostepnioneZasobyUzytkownicy as $udostepnionyUzytkownik) {
	    $uzytkownicy[] = $udostepnionyUzytkownik->uzytkownik;
	}
	return $uzytkownicy;
    }

    public function przestanUdostepniacZasob($udostepnionyZasobId, $uzytkownikId) {
	$encja = $this->pobierzZWherem("uzytkownik_id = $uzytkownikId AND udostepniony_zasob_id = $udostepnionyZasobId");
	if (!empty($encja)) {
	    $this->usunPoId($encja[0]->id);
	}
    }

    public function sprawdzCzyUdostepniany($udostepnioneZasobyId) {
	return !!$this->pobierzZWheremCount("udostepniony_zasob_id = $udostepnioneZasobyId");
    }

    private function usunWszystkichUzytkownikow($udostepnionyZasobId) {
	$wszystkiePliki = $this->pobierzZWherem("udostepniony_zasob_id = $udostepnionyZasobId");
	foreach ($wszystkiePliki as $plik) {
	    $this->usunPoId($plik->id);
	}
    }

    private function usunOdznaczonychUzytkownikow($uzytkownicyId, $udostepnionyZasobId) {
	$wszystkiePliki = $this->pobierzZWherem("udostepniony_zasob_id = $udostepnionyZasobId");
	foreach ($wszystkiePliki as $plik) {
	    if (!in_array($plik->uzytkownik->id, $uzytkownicyId)) {
		$this->usunPoId($plik->id);
	    }
	}
    }

    private function udostepnijJesliNieIstnieje($uzytkownicyId, $udostepnionyZasobId) {
	foreach ($uzytkownicyId as $uzytkownikId) {
	    if (!$this->pobierzZWheremCount("uzytkownik_id = $uzytkownikId AND udostepniony_zasob_id = $udostepnionyZasobId")) {
		$udostepnionyZasobUzytkownik = new \Zasoby\Entity\UdostepnionyZasobUzytkownik();
		$udostepnionyZasobUzytkownik->uzytkownik = $uzytkownikId;
		$udostepnionyZasobUzytkownik->udostepniony_zasob = $udostepnionyZasobId;
		$this->zapisz($udostepnionyZasobUzytkownik);
	    }
	}
    }

}
