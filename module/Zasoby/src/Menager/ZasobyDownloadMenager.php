<?php

namespace Zasoby\Menager;

use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;

/**
 * Do poprawnego działania konieczna jest aktywacja extension=php_fileinfo.dll, w php.ini
 */
class ZasobyDownloadMenager extends \Pastmo\Zasoby\Menager\ZasobyDownloadMenager {

    protected function kodujNazwe($nazwa, $rozszerzenie) {
	if ($rozszerzenie == ".obj") {
	    return $this->id . 'model' . $rozszerzenie;
	} else {
	    return parent::kodujNazwe($nazwa, $rozszerzenie);
	}
    }

    public static function getClass() {
	return __CLASS__;
    }

}
