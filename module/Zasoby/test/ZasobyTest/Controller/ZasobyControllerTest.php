<?php

namespace ZasobyTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zasoby\Controller\ZasobyController;
use \Zasoby\Model\Zasob;

class ZasobyControllerTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

    protected $traceError = true;

    public function setUp() {
	$this->setApplicationConfig(
		include \Application\Stale::configPath
	);
	parent::setUp();
    }

//TODOPK: Aktywować testy
    public function testIndex() {

    }

    public function testPrzekierowaniePrzyNiezalogwanym() {

    }

    public function getBigName() {
	return 'Zasoby';
    }

    public function getSmallName() {
	return 'zasoby';
    }

    public function testDownloadZasob() {
	$this->zasobyDownloadMenager
		->expects($this->once())
		->method('downloadZasobResponse')
		->willReturn(new \Zend\Http\Response\Stream());
	$this->dispatch('/zasoby/download/5');
    }

    public function testDownloadZipZasob() {
	$this->zasobyDownloadMenager
		->expects($this->once())
		->method('downloadZip')
		->willReturn(new \Zend\Http\Response\Stream())
		->with($this->equalTo(array('111', '110', '109', '108')), $this->equalTo('pliki'));


	$this->dispatch('/zasoby/downloadZip?nazwa=pliki&ids%5B0%5D=111&ids%5B1%5D=110&ids%5B2%5D=109&ids%5B3%5D=108');
    }

    public function test_upload_true() {
	$this->obslugaKlasObcych->ustawZalogowanegoUsera(new \Logowanie\Model\Uzytkownik(), 1);
	$this->ustawParametryPosta(array());
	$this->zasobyUploadMenager
		->expects($this->once())->method('zaladujPlik')->willReturn(array(new Zasob()));
	$this->dispatch('/zasoby/upload');
	$this->sprawdzCzySukces();
    }

    public function test_upload_false() {
	$this->obslugaKlasObcych->ustawZalogowanegoUsera(new \Logowanie\Model\Uzytkownik(), 1);
	$this->ustawParametryPosta(array());
	$this->zasobyUploadMenager
		->expects($this->once())->method('zaladujPlik')->willReturn(array());
	$this->dispatch('/zasoby/upload');
	$this->sprawdzCzyFail();
    }

}
