<?php

namespace ZasobyTest\Integracyjne;

use Zasoby\Entity\UdostepnionyZasobUzytkownik;
use Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane;
use \Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane;

require_once dirname(__FILE__) . '\..\..\..\..\Wspolne\test\Fabryki\FabrykaRekordow.php';

class UdostepnioneZasobyUzytkownicyTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

	protected $traceError = true;
	protected $sm;
	protected $udostepnioneZasobyUzytkownicyMenager;

	public function setUp() {
		$this->setApplicationConfig(
			include \Application\Stale::configTestPath
		);
		parent::setUp();

		$this->sm = $this->getApplicationServiceLocator();
		$this->udostepnioneZasobyUzytkownicyMenager = $this->sm->get(\Zasoby\Menager\UdostepnioneZasobyUzytkownicyMenager::class);
	}

	public function test_dodaj() {
		$uzytkownik = \FabrykaRekordow::makeEncje(UdostepnionyZasobUzytkownik::class,
				\Pastmo\Testy\ParametryFabryki\PFIntegracyjneBrakzapisuPowiazane::create($this->sm));
		$uz = $uzytkownik->uzytkownik;
		$udostepnionyZasob = $uzytkownik->udostepniony_zasob;
		$this->udostepnioneZasobyUzytkownicyMenager->zapisz($uzytkownik);
		$ostatniRekord = $this->udostepnioneZasobyUzytkownicyMenager->getPoprzednioDodany();

		$this->assertEquals($ostatniRekord->uzytkownik->id, $uz->id);
		$this->assertEquals($ostatniRekord->udostepniony_zasob->id,
			$udostepnionyZasob->id);
	}

}
