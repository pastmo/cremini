<?php

namespace ZasobyTest\Integracyjne;

class ZasobyControllerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    public function setUp() {
	parent::setUp();
	$this->traceError = true;
    }

    public function testDownloadZasob() {
	$this->ustawZalogowanegoUseraId1();
	$this->dispatch('/zasoby/download/1');
	$odpowiedz = $this->getResponse()->getBody();
	$this->assertResponseStatusCode(200);
    }

    //TODOPK: Test niby przechodzi, ale wysypuje to się na stronie- trzeba to sprawdzić.
    public function testwyswietlMiniaturke() {
	$this->ustawZalogowanegoUseraId1();
	$_SERVER['HTTP_HOST'] = 'localhost';
	$this->dispatch('/zasoby/wyswietl_miniaturke/1');
	$odpowiedz = $this->getResponse()->getBody();
	$this->assertResponseStatusCode(200);
    }

}
