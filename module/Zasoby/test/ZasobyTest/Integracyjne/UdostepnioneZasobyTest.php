<?php

namespace ZasobyTest\Integracyjne;

use Zasoby\Entity\UdostepnionyZasob;
use Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane;
use \Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane;

require_once dirname(__FILE__) . '\..\..\..\..\Wspolne\test\Fabryki\FabrykaRekordow.php';

class UdostepnioneZasobyTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

	protected $traceError = true;
	protected $sm;
	protected $udostepnioneZasobyMenager;

	public function setUp() {
		$this->setApplicationConfig(
			include \Application\Stale::configTestPath
		);
		parent::setUp();

		$this->sm = $this->getApplicationServiceLocator();
		$this->udostepnioneZasobyMenager = $this->sm->get(\Zasoby\Menager\UdostepnioneZasobyMenager::class);
	}

	public function test_dodaj() {
		$udostepnionyZasob = \FabrykaRekordow::makeEncje(UdostepnionyZasob::class,
				\Pastmo\Testy\ParametryFabryki\PFIntegracyjneBrakzapisuPowiazane::create($this->sm));
		$wlasciciel = $udostepnionyZasob->wlasciciel;
		$zasob = $udostepnionyZasob->zasob;
		$this->udostepnioneZasobyMenager->zapisz($udostepnionyZasob);
		$ostatniRekord = $this->udostepnioneZasobyMenager->getPoprzednioDodany();

		$this->assertEquals($ostatniRekord->wlasciciel->id, $wlasciciel->id);
		$this->assertEquals($ostatniRekord->zasob->id, $zasob->id);
	}

}
