<?php

namespace ZasobyTest\Integracyjne;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zasoby\Model\Zasob;
use Zasoby\Menager\ZasobyUploadMenager;
use Pastmo\Testy\Util\TB;

class ZasobyUploadMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    protected $traceError = true;
    private $zasobyTableObiect;
    private $fileOperator;

    const IMAGE_TYPE = 'image/png';
    const ORYGINAL_NAME = "name.jpg";

    public function setUp() {
	parent::setUp();

	$this->zasobyTableObiect = $this->sm->get(\Zasoby\Menager\ZasobyUploadMenager::class);
	$this->zalogowany = TB::create(\Logowanie\Model\Uzytkownik::class, $this)->setPF_IZP($this->sm)->make();
	$this->ustawZalogowanegoUsera($this->zalogowany->id);
    }

    public function testzaladujPlik() {
	$this->ustawFiles("image-file");

	$aktualnyId = $this->pobierzAktualnyId();

	$this->fileOperator = $this->ustawMockaFileOperator(1);
	$this->ustawSprawdzanieZapisywanegoUrla(self::ORYGINAL_NAME, $aktualnyId);
	$this->zasobyTableObiect->fileOperator = $this->fileOperator;
	$expectedUrl=$this->getFinalUrl(self::ORYGINAL_NAME, $aktualnyId);

	$wynik = $this->zasobyTableObiect->zaladujPlik();

	$this->assertEquals(count($wynik), 1);
	$this->assertEquals($wynik[0]->url, $expectedUrl);
	$this->assertEquals($wynik[0]->nazwa, self::ORYGINAL_NAME);
	$this->assertEquals($wynik[0]->rozmiar, '42');
    }

    public function testzaladujPlik_wiecejPlikow() {
	$this->ustawFiles("image-file2");
	$this->ustawFiles("image-file3");

	$this->fileOperator = $this->ustawMockaFileOperator(2);

	$this->zasobyTableObiect->fileOperator = $this->fileOperator;

	$wynik = $this->zasobyTableObiect->zaladujPlik();

	$this->assertEquals(2, count($wynik));
    }

    private function ustawFiles($klucz, $nazwaPliku = 'name.jpg') {
	$_FILES[$klucz] = array();
	$_FILES[$klucz]['name'] = $nazwaPliku;
	$_FILES[$klucz]['tmp_name'] = 'tmp_name';
	$_FILES[$klucz]['type'] = self::IMAGE_TYPE;
	$_FILES[$klucz]['size'] = '42';
    }

    private function pobierzAktualnyId() {
	$aktualnyId = $this->zasobyTableObiect->pobierz_ilosc_zasobow() + 1;
	return $aktualnyId;
    }

    private function ustawMockaFileOperator($ileRazy) {
	$fileOperator = $this->getMockBuilder('Zasoby\Model\FileOperator')
		->disableOriginalConstructor()
		->getMock();

	$fileOperator->expects($this->exactly($ileRazy))
		->method('move_uploaded_file')
		->will($this->returnValue(true));

	return $fileOperator;
    }

    private function ustawSprawdzanieZapisywanegoUrla($expectedName, $id) {

	$url = $this->getFinalUrl($expectedName, $id);

	$this->fileOperator->method('move_uploaded_file')
		->with($this->anything(), $this->equalTo("./public_html/test/$url"));
    }

    private function getFinalUrl($expectedName, $id) {
	$idKonta = $this->zalogowany->konto->id;
	$finalName = $id . md5($expectedName) . ".jpg";

	return "$idKonta/$finalName";
    }

}
