<?php

namespace ZasobyTable\Model;

class ZasobyTableTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    public function setUp() {
	parent::setUp();
	$this->ustawMockaGateway(\Pastmo\Uzytkownicy\KonfiguracjaModulu::UZYTKOWNIK_TABLE_GATEWAY);
	$this->zasobyDownloadMenager = new \Zasoby\Menager\ZasobyUploadMenager($this->sm);
    }

    public function testdownloadZasobResponse() {

	$zasob = \FabrykaEncjiMockowych::utworzEncje(\Zasoby\Model\Zasob::class, array('url' => '1.jpg'));
	$this->obslugaTestowanejKlasy->ustawWynikGetRekord($zasob, $this->gateway, 1);

	$response = $this->zasobyDownloadMenager->downloadZasobResponse($zasob->id);
    }

    public function testdownloadZip() {

	$zasob = \FabrykaEncjiMockowych::utworzEncje(\Zasoby\Model\Zasob::class, array('url' => '1.jpg'));
	$this->obslugaTestowanejKlasy->ustawWynikGetRekord($zasob, $this->gateway, 3);

	$response = $this->zasobyDownloadMenager->downloadZip(array(2, 4, 6), 'pliki');
	$this->assertNotNull($response);
    }

    public function test_pobierzIdZasobow() {
	$post = array('zasoby' => '["15","33"]');
	$expected = array("15", "33");

	$wynik = $this->zasobyDownloadMenager->pobierzIdZasobow($post, 'zasoby');

	$this->assertEquals($expected, $wynik);
    }

    public function test_pobierzIdZasobow_puste() {
	$post = array('zasoby' => '[]');
	$expected = array();

	$wynik = $this->zasobyDownloadMenager->pobierzIdZasobow($post, 'zasoby');

	$this->assertEquals($expected, $wynik);
    }

}
