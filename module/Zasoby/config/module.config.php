<?php

namespace Zasoby;

return array(
	'controllers' => array(
		'factories' => array(
			'Zasoby\Controller\Zasoby' => Controller\Factory\Factory::class,
			'Zasoby\Controller\ManagerPlikow' => Controller\Factory\ManagerPlikowFactory::class,
		),
	),
	'router' => array(
		'routes' => array(
			'zasoby' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/zasoby[/:action][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id' => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Zasoby\Controller\Zasoby',
						'action' => 'index',
					),
				),
			),
			'manager_plikow' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/manager_plikow[/:action][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id' => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Zasoby\Controller\ManagerPlikow',
						'action' => 'index',
					),
				),
			),
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Zasoby' => __DIR__ . '/../view',
		),
	),
);
