<?php

namespace Logowanie\Builder;

class UzytkownikBuilder implements \Wspolne\Interfejs\UzytkownikBuilderInterfejs {

    private $email;
    private $telefon;
    private $imieINazwisko;

    function getEmail() {
	return $this->email;
    }

    function getTelefon() {
	return $this->telefon;
    }

    function getImieINazwisko() {
	return $this->imieINazwisko;
    }

    function setEmail($email) {
	$this->email = $email;
	return $this;
    }

    function setTelefon($telefon) {
	$this->telefon = $telefon;
	return $this;
    }

    function setImieINazwisko($imieINazwisko) {
	$this->imieINazwisko = $imieINazwisko;
	return $this;
    }

    public static function create() {
	return new UzytkownikBuilder();
    }

}
