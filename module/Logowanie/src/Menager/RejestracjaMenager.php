<?php

namespace Logowanie\Menager;

use Zend\ServiceManager\ServiceManager;
use Logowanie\Polecenie\WynikPrzetwarzaniaFormularza;
use \Logowanie\Model\Uzytkownik;

class RejestracjaMenager extends \Pastmo\Uzytkownicy\Menager\RejestracjaMenager {

    protected function akcjePoRejestracji() {
	parent::akcjePoRejestracji();

	$this->utworzKonto();
	$this->wyslijMailaDoAdmina();
    }

    private function wyslijMailaDoAdmina() {
	$infoMail = $this->get(\Logowanie\Model\InfoDoAdminaMail::class);
	$infoMail->ustawParametry('monika.rymsza@pastmo.pl',
		$this->dodanyUzytkownik->id . ' ' . $this->dodanyUzytkownik->mail, '');
	$infoMail->send();
    }

    private function utworzKonto() {

	$this->dodanyUzytkownik;
	$konto = new \Uzytkownicy\Entity\Konto();
	$konto->rejestrujacy_id = $this->dodanyUzytkownik->id;

	$kontaMenager = $this->get(\Uzytkownicy\Menager\KontaMenager::class);
	$kontaMenager->zapisz($konto);
	$zapisaneKonto = $kontaMenager->getPoprzednioDodany();

	$this->dodanyUzytkownik->konto = $zapisaneKonto;
	$this->zapisz($this->dodanyUzytkownik);
    }

    public function konwertujPost($post) {
	if (isset($post['uzytkownik'])) {
	    $post['mail'] = $post['uzytkownik']['mail'];
	    $post['haslo'] = $post['uzytkownik']['haslo'];
	    $post['haslo2'] = $post['uzytkownik']['haslo2'];
	}
	return $post;
    }

}
