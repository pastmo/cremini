<?php

namespace Logowanie\Entity;

use Zasoby\Model\Zasob;
use Zend\Exception;

class Uprawnienie extends \Wspolne\Model\WspolneModel {

    public $kod;
    public $opis;

    public function __construct($sm = null) {
	parent::__construct($sm);
    }

    public function exchangeArray($data) {
	$this->kod = $this->pobierzLubNull($data, 'kod');
	$this->opis = $this->pobierzLubNull($data, 'opis');
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public function dowiazListyTabelObcych() {

    }

    public static function create($opis) {
	$wynik = new Uprawnienie();
	$wynik->opis = $opis;
	return $wynik;
    }

}
