<?php

namespace Logowanie\Entity;

use Zasoby\Model\Zasob;
use Zend\Exception;

class UprawnienieKategoria extends \Wspolne\Model\WspolneModel {

    public $kod;
    public $opis;
    private $uprawnienia = array();

    public function __construct($sm = null) {
	parent::__construct($sm);
    }

    public function getUprawnienia() {
	$this->dowiazListyTabelObcych();
	return $this->uprawnienia;
    }

    public function exchangeArray($data) {
	$this->kod = $this->pobierzLubNull($data, 'kod');
	$this->opis = $this->pobierzLubNull($data, 'opis');
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public function dowiazListyTabelObcych() {
	if ($this->czyTabeleDostepne()) {
	    $uprawnieniaMenager = $this->sm->get(\Logowanie\Menager\UprawnieniaMenager::class);
	    $this->uprawnienia = $uprawnieniaMenager->pobierzZWherem("kod like '" . $this->kod . "%'", "opis ASC");
	}
    }

}
