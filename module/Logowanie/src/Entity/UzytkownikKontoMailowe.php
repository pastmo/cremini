<?php

namespace Logowanie\Entity;

use \Logowanie\Menager\UzytkownicyMenager;
use \Logowanie\Model\Uzytkownik;
use \Pastmo\Email\Menager\KontaMailoweMenager;
use \Pastmo\Email\Entity\KontoMailowe;

class UzytkownikKontoMailowe extends \Pastmo\Email\Entity\UzytkownikKontoMailowe {

    public function pobierzKlase() {
	return __CLASS__;
    }

}
