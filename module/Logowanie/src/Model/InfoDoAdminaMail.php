<?php

namespace Logowanie\Model;

class InfoDoAdminaMail extends \Pastmo\Email\Menager\BazowyWysylaczMaili {

    protected $title = "Potwierdzenie rejestracji";
    protected $body = "Przed chwią nowy użytkownik zarejestrował się do systemu: %s";
    protected $copyIfNotWork = "";
    protected $akcja = 'profil';

    public function generujLinka($id, $kod) {
	$link = $this->generujUrl('logowanie', array('action' => $this->akcja,'id'=>$id), array(),
		!$this->test);
	return $link;
    }

}
