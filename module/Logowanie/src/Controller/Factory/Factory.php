<?php

namespace Logowanie\Controller\Factory;

use Logowanie\Controller\LogowanieController;

class Factory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

    protected function newController() {
	return new LogowanieController();
    }

}
