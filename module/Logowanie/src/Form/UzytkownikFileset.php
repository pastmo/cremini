<?php

namespace Logowanie\Form;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator\ClassMethods as ClassMethodsHydrator;
use Logowanie\Model\Uzytkownik;

class UzytkownikFileset extends Fieldset implements InputFilterProviderInterface {

    public function __construct() {
	parent::__construct('uzytkownik');

	$this
		->setHydrator(new ClassMethodsHydrator(false))
		->setObject(new Uzytkownik())
	;

	$this->setLabel('Użytkownik');

	$this->add(array(
		'name' => 'login',
		'attributes' => array(
			'required' => 'required',
			'class' => 'form-control',
			'placeholder'=>'E-mail'
		),
	));
	$this->add(array(
		'name' => 'haslo',
		'type' => 'password',
		'attributes' => array(
			'required' => 'required',
			'class' => 'form-control',
			'placeholder'=>'Hasło'
		),
	));
	$this->add(array(
		'name' => 'haslo2',
		'type' => 'password',
		'attributes' => array(
			'required' => 'required',
			'class' => 'form-control',
			'placeholder'=>'Powtórz hasło'
		),
	));
	$this->add(array(
		'name' => 'imie',
		'attributes' => array(
			'class' => 'form-control',
			'placeholder'=>'Imię'
		),
	));
	$this->add(array(
		'name' => 'nazwisko',
		'attributes' => array(
			'class' => 'form-control',
			'placeholder'=>'Nazwisko'
		),
	));
	$this->add(array(
		'name' => 'mail',
		'attributes' => array(
			'class' => 'form-control',
			'placeholder'=>'E-mail'
		),
	));
	$this->add(array(
		'name' => 'typ',
		'attributes' => array(
			'class' => 'form-control'
		),
	));
    }

    public function getInputFilterSpecification() {
	return array();
    }

}
