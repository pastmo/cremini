<?php

namespace Logowanie\Polecenie;

class WynikPrzetwarzaniaFormularza {

    public $isSuccess;
    public $message;

    public function __construct($wynik, $message) {
	$this->isSuccess = $wynik;
	$this->message = $message;
    }

}
