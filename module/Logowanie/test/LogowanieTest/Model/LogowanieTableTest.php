<?php

namespace LogowanieTest\Model;

use Logowanie\Model\Uzytkownik;
use Logowanie\Menager\UzytkownicyMenager;
use Zend\Session\Container;



class LogowanieTableTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

	public $uzytkownikTable;
	private $uzytkownik;

	public function setUp() {
		parent::setUp();
		$this->ustawMockaGateway(\Logowanie\Module::UZYTKOWNIK_TABLE_GATEWAY);
		$this->uzytkownikTable = new UzytkownicyMenager($this->sm);
	}

	public function testZaloguj() {
		$this->ustawUzytkownika(true);
		$mail = "mail";
		$haslo = "haslo";

		$this->obslugaTestowanejKlasy->ustawWynikGetRekord($this->uzytkownik,
				$this->gateway, 1);

		$wynik = $this->uzytkownikTable->zaloguj($mail, $haslo);

		$this->assertNotNull($wynik);
	}

	public function testZaloguj_nieaktywny() {
		$this->ustawUzytkownika(false);
		$mail = "mail";
		$haslo = "haslo";

		$this->obslugaTestowanejKlasy->ustawWynikGetRekord($this->uzytkownik,
				$this->gateway, 1);

		$wynik = $this->uzytkownikTable->zaloguj($mail, $haslo);
		$wiadomosc=\Wspolne\Controller\ElementyWidokuController::pobierzWiadomosc();

		$this->assertFalse($wynik);
		$this->assertEquals("Użytkownik nieaktywny",$wiadomosc);
	}

	public function testZaloguj_brak_uzytkownika() {
		$mail = "mail";
		$haslo = "haslo";

		$this->obslugaTestowanejKlasy->ustawWynikGetRekord(null,
				$this->gateway, 1);

		$wynik = $this->uzytkownikTable->zaloguj($mail, $haslo);
		$wiadomosc=\Wspolne\Controller\ElementyWidokuController::pobierzWiadomosc();

		$this->assertFalse($wynik);
		$this->assertEquals("Błędny login lub hasło",$wiadomosc);
	}

	public function testGetZalogowanego() {
		$this->ustawUzytkownika(true);
		$container = new Container('namespace');

		$container->user_id = 42;
		$this->obslugaTestowanejKlasy->ustawWynikGetRekord($this->uzytkownik,
				$this->gateway, 1);

		$zalogowany = $this->uzytkownikTable->getZalogowanegoUsera();
		$this->assertTrue($zalogowany->id > 0);
	}

	private function ustawUzytkownika($czyAktywny) {
		$this->uzytkownik = \FabrykaEncjiMockowych::makeEncje(Uzytkownik::class,
						\Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane::create()
								->setparametry(array('czy_aktywny' => $czyAktywny)));
	}

}
