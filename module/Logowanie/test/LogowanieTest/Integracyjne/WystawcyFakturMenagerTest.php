<?php

namespace UzytkownicyTest\Integracyjne;

class UzytkownicyMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $post = [
        'imie_nazwisko' => 'Rafał Giemza',
        'nazwa' => 'Termobud',
        'nip' => '5761360252',
        'ulica' => array(
            0 => 'Borkowa',
            1 => 'Dworcowa'
        ),
        'nr_domu' => array(
            0 => '9',
            1 => '19'
        ),
        'nr_lokalu' => array(
            0 => '',
            1 => '2'
        ),
        'kod' => array(
            0 => '12-345',
            1 => '46-300'
        ),
        'miasto' => array(
            0 => 'Zborowskie',
            1 => 'Olesno'
        )
    ];
    private $wystawcyFaktur;

    public function setUp() {
        parent::setUp();

        $this->wystawcyFaktur = $this->sm->get(\Pastmo\Faktury\Menager\WystawcyFakturMenager::class);
    }

    public function test_zapiszUstawieniaOsobaPrywatna() {

        $post = array_merge($this->post, ['typ' => \Pastmo\Faktury\Enumy\TypyWystawcowFaktur::OSOBA_PRYWATNA]);
        $this->wystawcyFaktur->zapiszWystawceFaktur($post);
        $result = $this->wystawcyFaktur->pobierzUtworzWystawce();
        
        $this->assertEquals($post['typ'], $result->typ);
        $this->assertEquals($post['imie_nazwisko'], $result->imie_nazwisko);
        $this->assertEquals($post['imie_nazwisko'], $result->nazwa);
        $this->assertEquals($post['ulica'][1], $result->adres->ulica);
        $this->assertEquals($post['nr_domu'][1], $result->adres->nr_domu);
        $this->assertEquals($post['nr_lokalu'][1], $result->adres->nr_lokalu);
        $this->assertEquals($post['kod'][1], $result->adres->kod);
        $this->assertEquals($post['miasto'][1], $result->adres->miasto);
        
    }

    public function test_zapiszUstawieniaFirma() {

        $post = array_merge($this->post, ['typ' => \Pastmo\Faktury\Enumy\TypyWystawcowFaktur::FIRMA]);
        $this->wystawcyFaktur->zapiszWystawceFaktur($post);
        $result = $this->wystawcyFaktur->pobierzUtworzWystawce();
        
        $this->assertEquals($post['typ'], $result->typ);
        $this->assertEquals($post['nazwa'], $result->nazwa);
        $this->assertEquals($post['ulica'][0], $result->adres->ulica);
        $this->assertEquals($post['nr_domu'][0], $result->adres->nr_domu);
//todo aktywowac asercje
//        $this->assertEquals($post['nr_lokalu'][0], $result->adres->nr_lokalu);
        $this->assertEquals($post['kod'][0], $result->adres->kod);
        $this->assertEquals($post['miasto'][0], $result->adres->miasto);
    }

}
