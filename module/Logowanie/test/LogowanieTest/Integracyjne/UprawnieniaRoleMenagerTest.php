<?php

namespace LogowanieTest\Integracyjne;

class UprawnieniaRoleMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

	protected $menager;

	private $rola;

	public function setUp() {
		parent::setUp();

		$this->menager=  $this->sm->get(\Logowanie\Menager\UprawnieniaRoleMenager::class);
	}

	protected function tearDown()
	{	$uprawnieniaRole= $this->sm->get(\Logowanie\Menager\UprawnieniaRoleMenager::class);
		$uprawnieniaRole->usun("rola_id=".$this->rola->id);

	}

	public function testUaktualnijUprawnienia() {
		$kodPierwotny = \Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE;
		$kodNowy = 'fghj';

		$this->rola = \FabrykaRekordow::utworzEncje($this->sm, \Logowanie\Entity\Rola::class);
		$uprawnienieRola = \FabrykaRekordow::utworzEncje($this->sm, \Logowanie\Entity\UprawnienieRola::class, array('rola_id' => $this->rola->id, 'uprawnienie_kod' => $kodPierwotny));

		$this->assertFalse($this->rola->sprawdzUprawnienia($kodNowy));
		$this->assertTrue($this->rola->sprawdzUprawnienia($kodPierwotny));

		$wybraneUprawnienia=array($kodNowy=>$kodNowy);
		$this->menager->uaktualnijUprawnienia($this->rola->id,$wybraneUprawnienia);

		$this->rola=  $this->sm->get(\Logowanie\Menager\RoleMenager::class)->getRekord($this->rola->id);
		$this->assertTrue($this->rola->sprawdzUprawnienia($kodNowy));
		$this->assertFalse($this->rola->sprawdzUprawnienia($kodPierwotny));
		}

}
