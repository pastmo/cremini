<?php

namespace LogowanieTest\Menager;

use \ProjektyTest\Menager\ProjektyMenagerTest;

class UzytkownicyTableTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    public $uzytkownik;

    public function setUp() {
	parent::setUp();

	$this->ustawMockaGateway(\Logowanie\Module::UZYTKOWNIK_TABLE_GATEWAY);
	$this->uzytkownikTable = new \Logowanie\Menager\UzytkownicyMenager($this->sm);

	$this->uzytkownik = $uzytkownik = \FabrykaEncjiMockowych::utworzEncje(\Logowanie\Model\Uzytkownik::class);
    }

    public function testZapiszZPosta() {
	$post = $this->utworzPost();

	$this->ustawKonwerter($post);
	$this->ustawWynikZaladujPlik();
	$this->ustawGetRekord();
	$this->sprawdzWykonanieUpdate();


	$this->uzytkownikTable->zapiszZPosta($post);
    }

    public function testzarejestrujLubUaktualnijZBuildera_brak_uzytkownika_o_danym_mailu() {
	$builder = \Logowanie\Builder\UzytkownikBuilder::create();
	$builder->setEmail("email")->setImieINazwisko("Imie nazwisko")->setTelefon("123");


	$this->ustawWynikPobieraniaZWhera(array());

	$this->sprawdzRejestrowanieUzytkownika(1);

	$this->uzytkownikTable->zarejestrujLubUaktualnijZBuildera($builder);
    }

    public function testzarejestrujLubUaktualnijZBuildera_znalezionyUzytkownik() {
	$builder = \Logowanie\Builder\UzytkownikBuilder::create();
	$builder->setEmail("email")->setImieINazwisko("Imie nazwisko")->setTelefon("123");


	$this->ustawWynikPobieraniaZWhera(array($this->uzytkownik));

	$this->sprawdzRejestrowanieUzytkownika(0);

	
	$this->obslugaTestowanejKlasy->ustawieniaPotrzebneDoUpdate($this->uzytkownik,
		$this->gateway);
	$this->obslugaTestowanejKlasy->ustawSprawdzanieUpdate(1);

	$wynik = $this->uzytkownikTable->zarejestrujLubUaktualnijZBuildera($builder);

	$this->assertNotNull($wynik);
    }

    private function ustawKonwerter($post) {
	$rejestracjaMenager = new \Logowanie\Menager\RejestracjaMenager($this->sm);
	$konwertowanyPost = $rejestracjaMenager->konwertujPost($post);

	$this->rejestracjaMenager->expects($this->once())->method('konwertujPost')->willReturn($konwertowanyPost);
    }

    private function ustawWynikZaladujPlik() {
	$this->zasobyUploadMenager->expects($this->once())->method('zaladujPlik')->willReturn(array());
    }

    private function ustawGetRekord() {
	$this->obslugaTestowanejKlasy->ustawWynikGetRekord($this->uzytkownik,
		$this->gateway, 2);
    }

    private function ustawWynikPobieraniaZWhera($wynik) {
	$this->obslugaTestowanejKlasy->ustawMockiDoPobierzZWherem($wynik);
    }

    private function sprawdzWykonanieUpdate() {
	$this->gateway->expects($this->once())->method('update');
    }

    private function sprawdzRejestrowanieUzytkownika($ileRazy) {
	$this->obslugaKlasObcych->ustawMetode(\Logowanie\Menager\RejestracjaMenager::class,
		'zarejestrujUzytkownika', null, $ileRazy);
    }

    private function utworzPost() {
	return array('user_id' => $this->uzytkownik->id,
	    'phone' => '123654789',
	    'phone2' => '997');
    }

}
