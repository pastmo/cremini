<?php

namespace LogowanieTest\Integracyjne;



class AktywacjaMenagerTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

	private $get = array(
		'id' => '1',
		'kod' => 'c13fde8c21332db79e4f057dcd518e48'
	);
	private $uzytkownik;

	public function setUp() {
		parent::setUp();

		$this->aktywacjaMenager = new \Logowanie\Menager\AktywacjaMenager($this->sm);
	}

	public function test_aktywujKonto() {


		$this->utworzUzytkownika();
		$this->ustawUzytkownikaPoId();
		$this->ustawPrawidlowyKod();

		$this->sprawdzCzyUzytkownikJestZapisywany();

		$wynik = $this->aktywacjaMenager->aktywujKonto($this->get);
		$this->assertTrue($wynik->isSuccess);
	}

	public function test_aktywujKonto_brak_uzytkownika() {

		$this->ustawWyjatekPrzyPobieraniuUzytkownikaPoId();

		$wynik = $this->aktywacjaMenager->aktywujKonto($this->get);
		$this->assertFalse($wynik->isSuccess);
		$this->assertEquals("Błędny kod aktywacji lub brak użytkownika",$wynik->message);
	}

	public function test_aktywujKonto_blednyKod() {


		$this->utworzUzytkownika();
		$this->ustawUzytkownikaPoId();

		$wynik = $this->aktywacjaMenager->aktywujKonto($this->get);
		$this->assertFalse($wynik->isSuccess);
	}

	protected function utworzUzytkownika() {
		$this->uzytkownik = \FabrykaEncjiMockowych::utworzEncje(\Logowanie\Model\Uzytkownik::class,
						array('nazwisko' => 'nazwisko'));
	}

	protected function ustawUzytkownikaPoId() {

		$this->uzytkownikTable->expects($this->any())->method('getRekord')->willReturn($this->uzytkownik);
	}

	protected function ustawPrawidlowyKod() {
		$this->get['kod'] = \Logowanie\Menager\RejestracjaMenager::generujKod($this->uzytkownik);
	}

	protected function ustawWyjatekPrzyPobieraniuUzytkownikaPoId() {

		$this->uzytkownikTable->expects($this->any())->method('getRekord')->will($this->throwException(new \Exception("Could not find row 2")));
	}

	protected function ustawFabrykeUzytkownikow() {

		$this->uzytkownikTable->expects($this->any())->method('createUzytkownik')->willReturn(new \Logowanie\Model\Uzytkownik());
	}

	protected function ustawFirme() {

		$firma = \FabrykaEncjiMockowych::utworzEncje(\Firmy\Entity\Firma::class);
		$this->firmyMenager->expects($this->once())->method('pobierzLubUtworzFirme')->willReturn($firma);
	}

	protected function ustawPoprzednioDodany() {
		$uzytkownik = \FabrykaEncjiMockowych::utworzEncje(\Logowanie\Model\Uzytkownik::class);
		$this->uzytkownikTable->expects($this->any())->method('getPoprzednioDodany')->willReturn($uzytkownik);
	}

	protected function sprawdzCzyUzytkownikJestZapisywany() {
		$this->uzytkownikTable->expects($this->exactly(1))->method('zapisz');
	}

	protected function nowyObiekt() {

	}

	public function aktywujKonto($get) {

	}

}
