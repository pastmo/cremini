<?php

namespace LogowanieTest\Model;

use Logowanie\Model\InfoDoAdminaMail;

class InfoDoAdminaMailTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    public $aktywacjaTableObject;
    private $id = "123";
    private $kod = "";
    private $mail = "mail@email.pl";
    private $link;

    public function setUp() {
	parent::setUp();

	$this->link = "/logowanie/profil/$this->id";
    }

    public function testUworzTytul() {
	$this->utworzEncjeIPrzypiszNaSztywnoParametry();
	$expected = "Potwierdzenie rejestracji";
	$result = $this->aktywacjaTableObject->utworzTytul();
	$this->assertEquals($expected, $result);
    }

    public function testGenerujLinka() {

	$this->utworzEncjeIPrzypiszNaSztywnoParametry();

	$expected = $this->link;
	$result = $this->aktywacjaTableObject->generujLinka($this->id, $this->kod);
	$this->assertEquals($expected, $result);
    }

    public function testGenerujTresc() {

	$this->utworzEncjeIPrzypiszNaSztywnoParametry();

	$expected = 'Przed chwią nowy użytkownik zarejestrował się do systemu: <a href="'.
	$this->link. '">'.
	$this->link. '</a>';

	$result = $this->aktywacjaTableObject->generujTresc();

	$this->assertEquals($expected, $result);
    }

    public function testUstawParametry_mail() {

	$this->utworzEncjeIPrzypiszNaSztywnoParametry();

	$this->utworzEncjeIUstawParametry();
	$this->assertEquals($this->mail, $this->aktywacjaTableObject->mail);
    }

    public function testUstawParametry_id() {

	$this->utworzEncjeIUstawParametry();
	$this->assertEquals($this->id, $this->aktywacjaTableObject->id);
    }

    public function testUstawParametry_kod() {

	$this->utworzEncjeIUstawParametry();
	$this->assertEquals($this->kod, $this->aktywacjaTableObject->kod);
    }

    public function testUstawParametry_tresc() {

	$this->utworzEncjeIUstawParametry();
	$this->assertNotNull($this->aktywacjaTableObject->getTresc());
    }

    public function testUstawParametry_tytul() {

	$this->utworzEncjeIUstawParametry();
	$this->assertNotNull($this->aktywacjaTableObject->getTytul());
    }

    protected function nowyObiekt() {

    }

    private function utworzEncjeIUstawParametry() {
	$this->aktywacjaTableObject = new InfoDoAdminaMail($this->sm);
	$this->aktywacjaTableObject->test = true;

	$this->aktywacjaTableObject->ustawParametry($this->mail, $this->id, $this->kod);
    }

    private function utworzEncjeIPrzypiszNaSztywnoParametry() {
	$this->aktywacjaTableObject = new InfoDoAdminaMail($this->sm);
	$this->aktywacjaTableObject->id = $this->id;
	$this->aktywacjaTableObject->kod = $this->kod;
	$this->aktywacjaTableObject->test = true;
    }

}
