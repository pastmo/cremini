<?php

namespace LogowanieTest\Model;

use Logowanie\Model\AktywacjaMail;



class AktywacjaMailTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

	public $aktywacjaTableObject;
	private $id = "123";
	private $kod = "kodxxx";
	private $link;

	public function setUp() {
		parent::setUp();

		$this->aktywacjaTableObject = new AktywacjaMail($this->sm);
		$this->aktywacjaTableObject->id = $this->id;
		$this->aktywacjaTableObject->kod = $this->kod;
		$this->aktywacjaTableObject->test = true;

		$this->link = "/logowanie/aktywuj?id=$this->id&kod=$this->kod";
	}

	public function testUworzTytul() {
		$expected = "Potwierdzenie rejestracji";
		$result = $this->aktywacjaTableObject->utworzTytul();
		$this->assertEquals($expected, $result);
	}

	public function testGenerujLinka() {
		$expected = $this->link;
		$result = $this->aktywacjaTableObject->generujLinka($this->id, $this->kod);
		$this->assertEquals($expected, $result);
	}

	public function testGenerujTresc() {
		$expected = 'Witaj, Twoje konto zostało założone. Możesz je aktywować klikając poniższy link: <a href="'
				. $this->link . '">'
				. $this->link . '</a> (jeśli nie zadziała, to skopiuj  go do przeglądarki)';

		$result = $this->aktywacjaTableObject->generujTresc();

		$this->assertEquals($expected, $result);
	}

	public function testUstawParametry_mail() {
		$mail = 'mail';
		$this->aktywacjaTableObject = new AktywacjaMail($this->sm);
		$this->aktywacjaTableObject->test = true;

		$this->aktywacjaTableObject->ustawParametry($mail, $this->id, $this->kod);
		$this->assertEquals($mail, $this->aktywacjaTableObject->mail);
	}

	public function testUstawParametry_id() {
		$mail = 'mail';
		$this->aktywacjaTableObject = new AktywacjaMail($this->sm);
		$this->aktywacjaTableObject->test = true;

		$this->aktywacjaTableObject->ustawParametry($mail, $this->id, $this->kod);

		$this->assertEquals($this->id, $this->aktywacjaTableObject->id);
	}

	public function testUstawParametry_kod() {
		$mail = 'mail';
		$this->aktywacjaTableObject = new AktywacjaMail($this->sm);
		$this->aktywacjaTableObject->test = true;

		$this->aktywacjaTableObject->ustawParametry($mail, $this->id, $this->kod);

		$this->assertEquals($this->kod, $this->aktywacjaTableObject->kod);
	}

	public function testUstawParametry_tresc() {
		$mail = 'mail';
		$this->aktywacjaTableObject = new AktywacjaMail($this->sm);
		$this->aktywacjaTableObject->test = true;

		$this->aktywacjaTableObject->ustawParametry($mail, $this->id, $this->kod);

		$this->assertNotNull($this->aktywacjaTableObject->getTresc());
	}

	public function testUstawParametry_tytul() {
		$mail = 'mail';
		$this->aktywacjaTableObject = new AktywacjaMail($this->sm);
		$this->aktywacjaTableObject->test = true;

		$this->aktywacjaTableObject->ustawParametry($mail, $this->id, $this->kod);

		$this->assertNotNull($this->aktywacjaTableObject->getTytul());
	}

	protected function nowyObiekt() {

	}

}
