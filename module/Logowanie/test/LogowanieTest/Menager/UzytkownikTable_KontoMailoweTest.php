<?php

namespace LogowanieTest\Menager;



class UzytkownicyMenager_KontoMailoweTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

	private $post = array(
		"host" => array("serwer1578643.home.pl", "inny_host.pl", ""),
		"user" => array("testowe1@grupaglasso.pl", "inny_uzytkownik", ""),
		"password" => array("Testowe1", "haslo", ""));

	public function setUp() {
		parent::setUp();
		$this->ustawMockaGateway(\Logowanie\Module::UZYTKOWNICY_KONTA_MAILOWE_GATEWAY);
	}

	public function test_zapiszKontaMailowe() {
		$this->ustawParametry();
		$this->ustawIloscWczesniejDodanych(1);
		$this->obslugaTestowanejKlasy->ustawSprawdzanieInsert(0);
		$this->zapiszKontaWspolne();
	}

	public function test_zapiszKontaMailowe_brakZapisanychWczesniejKont() {
		$this->ustawParametry();
		$this->ustawIloscWczesniejDodanych(0);
		$this->obslugaTestowanejKlasy->ustawSprawdzanieInsert(2);
		$this->zapiszKontaWspolne();
	}

	private function ustawParametry() {
		$this->obslugaKlasObcych->ustawZalogowanegoUsera();

		$kontoMailowe = \FabrykaEncjiMockowych::makeEncje(\Pastmo\Email\Entity\KontoMailowe::getClass(),
						\Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane::create());

		$this->kontaMailoweManager
				->expects($this->exactly(2))
				->method('getPoprzednioDodany')
				->will($this->returnValue($kontoMailowe));

		$this->kontaMailoweManager
				->expects($this->exactly(2))
				->method('zapisz');
	}

	private function ustawIloscWczesniejDodanych($ilosc) {
		$this->obslugaTestowanejKlasy->ustawSelectWithCount($ilosc, 2);
	}

	private function zapiszKontaWspolne() {

		$uzytkownikTable = new \Logowanie\Menager\UzytkownicyMenager($this->sm);
		$uzytkownikTable->zapiszKontaMailowe($this->post);
	}

	protected function nowyObiekt() {

	}

}
