<?php

namespace Logowanie;

return array(
	'controllers' => array(
		'factories' => array(
			'Logowanie\Controller\Logowanie' => Controller\Factory\Factory::class,
		),
	),
	'router' => array(
		'routes' => array(
			'logowanie' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/logowanie[/:action][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id' => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Logowanie\Controller\Logowanie',
						'action' => 'index',
					),
				),
			),
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'album' => __DIR__ . '/../view',
		),
	),
);
