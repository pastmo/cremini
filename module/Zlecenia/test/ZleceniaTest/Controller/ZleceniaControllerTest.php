<?php

namespace ZleceniaTest\Controller;

class ZleceniaControllerTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

	protected $traceError = true;

	public function setUp() {
		parent::setUp();
	}

	public function testIndex() {
		$this->ustawZalogowanegoUsera();
		parent::testIndex();
	}

	public function getBigName() {
		return 'Zlecenia';
	}

	public function getSmallName() {
		return 'zlecenia';
	}

}
