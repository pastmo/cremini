<?php

namespace ZleceniaTest\Integracyjne;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Klienci\Entity\Klient;
use Logowanie\Model\Uzytkownik;
use Zlecenia\Entity\Kategoria;
use Pastmo\Testy\Util\TB;

//TODO: aktywować test
class ZleceniaTableTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    protected $nazwaKlienta = "nazwa klienta";
    protected $daneFormularza = ["nazwa" => "Nazwa zlecenia",
	    "opis" => "Opis zlecenia",
	    "pliki" => "122",
	    "klient_id" => "",
	    "data_rozpoczecia" => "2017-05-10",
	    "data_zakonczenia" => "2017-05-22",
	    'pliki' => '[]',
	    "submit" => "Zapisz"];
    private $zleceniaTable;

    public function setUp() {
	parent::setUp();

	$this->serviceManager = $this->getApplicationServiceLocator();


	$this->utworzIZalogujUzytkownika();
	$this->zleceniaTable = $this->serviceManager->get(\Zlecenia\Module::ZLECENIA_TABLE);
    }

    public function test_dodawanieRekordu() {
	$zlecenie = TB::create(\Zlecenia\Entity\Zlecenie::class, $this)->make();
	$this->assertNotNull($zlecenie->id);
    }

    public function test_zapiszZlecenie_brak_klienta() {
	$post = $this->arrayToParameters($this->daneFormularza);

	$this->zleceniaTable->zapiszZlecenie($post);

	$zlecenie = $this->zleceniaTable->getPoprzednioDodany();
	$this->assertNotNull($zlecenie->id);
	$this->assertNotNull($zlecenie->nr_zlecenia);
	$this->assertNotNull($zlecenie->data_zakonczenia);
	$this->assertEquals(\Zlecenia\Enumy\ZleceniaStatusy::NOWE, $zlecenie->status);
    }

    public function test_zapiszZlecenie_powiazanie_z_innymi_tabelami() {
	$klient = TB::create(Klient::class, $this)->make();
	$zasob = TB::create(\Zasoby\Model\Zasob::class, $this)->make();

	$this->daneFormularza['klient_id'] = $klient->id;
	$this->daneFormularza['pliki'] = "[{$zasob->id}]";

	$post = $this->arrayToParameters($this->daneFormularza);

	$this->zleceniaTable->zapiszZlecenie($post);

	$zlecenie = $this->zleceniaTable->getPoprzednioDodany();
	$this->assertNotNull($zlecenie->klient->id);

	$zalaczniki = $zlecenie->getZalaczniki();
	$this->assertEquals(1, count($zalaczniki));
    }

    public function testMetodAnalitycznych() {
	$this->ustawZalogowanegoUseraId1();

	$liczbaZlecenTydzien = $this->zleceniaTable->liczbaZlecenZakonczonychOstatniTydzien();
	$liczbaZlecenMiesiac = $this->zleceniaTable->liczbaZlecenZakonczonychOstatniMiesiac();
	$liczbaZlecenRok = $this->zleceniaTable->liczbaZlecenZakonczonychOstatniRok();
	$liczbaZlecen = $this->zleceniaTable->liczbaZlecenZakonczonych();

	$this->assertGreaterThanOrEqual($liczbaZlecenRok, $liczbaZlecen);
	$this->assertGreaterThanOrEqual($liczbaZlecenMiesiac, $liczbaZlecenRok);
	$this->assertGreaterThanOrEqual($liczbaZlecenTydzien, $liczbaZlecenMiesiac);
    }

}
