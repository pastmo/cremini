<?php

namespace ZasobyTest\Model;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zasoby\Model\Zasob;
use Zasoby\Menager\ZasobyUploadMenager;

class ZleceniaTableTest  extends \Pastmo\Testy\BazoweKlasyTestow\AbstractMockTest{// \WspolneTest\Model\WspolnyHttpTest {

	protected $traceError = true;
	private $zasobyTableObiect;

	public function setUp() {
		$this->setApplicationConfig(
				include \Application\Stale::configTestPath
		);
		parent::setUp();


		//$this->initMock();
	}

	//TODO:Sprawdzić sens istniena tego testu

	public function t_estwyswietlUrlObrazka() {
		$input = "abc.jpg";
		$expected = "upload/abc.jpg";

		$zasob = new Zasob();
		$zasob->url = $input;

		$result = ZasobyTable::wyswietlUrlObrazka($zasob);

		$this->assertEquals($expected, $result);
	}

	protected function nowyObiekt() {
		new Zgloszenie();
	}

}
