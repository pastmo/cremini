<?php
return array(
     'controllers' => array(
         'factories' => array(
             'Zlecenia\Controller\Zlecenia' => 'Zlecenia\Controller\Factory\ZleceniaControllerFactory',
		),
     ),
	  'router' => array(
         'routes' => array(
             'zlecenia' => array(
				'type'    => 'segment',
                 'options' => array(
                     'route' => '/zlecenia[/:action][/:id][.:format]',
					'constraints' => array(
                         'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                         'id'     => '[0-9]+',
                     ),
                     'defaults' => array(
                         'controller' => 'Zlecenia\Controller\Zlecenia',
						'action'     => 'index',
                     ),
                 ),
             ),
         ),
     ),

     'view_manager' => array(
         'template_path_stack' => array(
             'Zlecenia' => __DIR__ . '/../view',
		),
     ),
 );