<?php

namespace Zlecenia\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Wspolne\Utils\SearchPole;
use Zend\View\Model\JsonModel;
use Klienci\Helper\KolorKlientaHelper;

class ZleceniaController extends \Wspolne\Controller\WspolneController {

    public function indexAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	return $this->redirect()->toRoute('zlecenia', ['action' => 'zlecenia_aktywne']);
    }

    public function dodajAction() {
	return $this->dodajEdytujaWspolne('Dodaj zlecenie');
    }

    public function edytujAction() {
	return $this->dodajEdytujaWspolne('Edytuj zlecenie');
    }

    private function dodajEdytujaWspolne($tytul) {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$post = $this->getPost();
	if ($post) {
	    $this->zleceniaTable->zapiszZlecenie($post);

	    $modulPowrotu = $this->getBackModule('zlecenia');
	    $action = $this->getBackAction('zlecenia_aktywne');

	    return $this->redirect()->toRoute($modulPowrotu, array('action' => $action));
	}

	$id = $this->params()->fromRoute('id', 0);
	$widok = $this->dodajEdytujWidok($tytul, $id);

	$glowny = new ViewModel();
	$glowny->addChild($widok, 'dodaj_edytuj');

	return $glowny;
    }

    public function zleceniaAktywneAction() {
	return $this->listaZlecen('zakonczone=0', true);
    }

    public function zleceniaZamknieteAction() {
	return $this->listaZlecen('zakonczone=1', false);
    }

    private function listaZlecen($whereDodatkowy, $otwarte) {
	$zalogowany = $this->uzytkownikTable->getZalogowanegoUsera();
	if (!$zalogowany) {
	    return $this->redirect()->toRoute('logowanie', array('action' => 'index'));
	}

	$pola = array(
		SearchPole::create()->setDataType('int')->setName('id'),
		SearchPole::create()->setDataType('string')->setName('termi_oddania'),
		SearchPole::create()->setDataType('string')->setName('status'),
			SearchPole::create()->setDataType('query')->setName('klient_id')
			->setQuery('in (select id from klienci where nazwa %s)'),
			SearchPole::create()->setDataType('query')->setName('opiekun_id')
			->setQuery('in (select id from uzytkownicy where imie %s)'),
			SearchPole::create()->setDataType('query')->setName('opiekun_id')
			->setQuery('in (select id from uzytkownicy where nazwisko %s)'),
	);


	$get = $this->getGet();
	if ($get && isset($get->kategoria_id) && $get->kategoria_id > 0) {
	    $whereDodatkowy.=" AND kategoria_id={$get->kategoria_id}";
	}


	$zlecenia = $this->pobierzZGeta($this->zleceniaTable, $whereDodatkowy, $pola);

	$wyszukiwanie = $this->pobierzWartoscWhere();

	$requestId = 0;
	if ($get) {
	    $requestId = $get->request_id;
	}

	if ($this->params('format') === 'json') {
	    $zleceniaArray = $this->konwertujNaAjaxoweZlecenie($zlecenia);
	    return $this->returnSuccess(array(
			    'zlecenia' => $zleceniaArray,
			    'request_id' => $requestId));
	}

	$kategorie = $this->zleceniaKategorieTable->fetchAll("id ASC");

	$lista = new ViewModel(array(
		'zlecenia' => $zlecenia,
		'wyszukiwanie' => $wyszukiwanie,
		'czy_otwarte' => $otwarte,
		'akcja' => $otwarte ? 'otwarte_projekty' : 'zamkniete_projekty',
		'kategorie' => $kategorie
	));

	$lista->setTemplate('zlecenia/zlecenia/lista_zlecen');

	$result = new ViewModel();
	$result->addChild($lista, 'lista_zlecen');

	return $result;
    }

    public function szczegolyAction() {
	if (!$this->getUzytkownicyMenager()->getZalogowanegoUsera()) {
	    return $this->redirect()->toRoute('logowanie', array('action' => 'index'));
	}
	$id = (int) $this->params()->fromRoute('id', 0);

	$zlecenie = $this->zleceniaTable->getRekord($id);


	$request = $this->getRequest();
	if ($request->isPost()) {
	    $zapisz_status = $request->getPost('submit_status', null);

	    if ($zapisz_status == 'Zmień status') {
		$status = $request->getPost('status');
		$this->zleceniaTable->zaktualizujStatus($status, $id);
	    }

	    $zlecenie = $this->zleceniaTable->getRekord($id);
	}

	$this->zleceniaTable->zapiszZalacznikDlaZlecenia($request, $id);

	$status = $this->generateSelectforSzczegoly('status', $zlecenie->status);

	return new ViewModel(array(
		'zlecenie' => $zlecenie,
		'status' => $status,
	));
    }

    public function zakonczAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);


	$id = $this->params()->fromRoute('id', 0);
	if ($id > 0) {
	    $this->zleceniaTable->zakoncz($id);
	}

	return $this->redirect()->toRoute('zlecenia');
    }


    private function generateSelectforSzczegoly($name, $optionToSelect) {

	$options = array(
		\Zlecenia\Enumy\ZleceniaStatusy::NOWE,
		\Zlecenia\Enumy\ZleceniaStatusy::PRZYJETE,
		\Zlecenia\Enumy\ZleceniaStatusy::W_TRAKCIE_REALIZACJI,
		\Zlecenia\Enumy\ZleceniaStatusy::GOTOWE_DO_ODBIORU,
		\Zlecenia\Enumy\ZleceniaStatusy::ODEBRANE
	);

	$html = '<select name="' . $name . '" class="form-control status_obok">';
	foreach ($options as $option) {
	    if ($option == \Zlecenia\Enumy\ZleceniaStatusy::PRZYJETE)
		$option_polskie = 'Przyjęte';
	    else
		$option_polskie = $option;
	    if ($option == $optionToSelect)
		$html .= '<option value="' . $option . '" selected="selected">' . $option_polskie . '</option>';
	    else
		$html .= '<option value="' . $option . '">' . $option_polskie . '</option>';
	}
	$html .= '</select>';

	return $html;
    }

    private function konwertujNaAjaxoweZlecenie($zlecenia) {
	$wynik = array();
	$kolorHelper = new KolorKlientaHelper();

	foreach ($zlecenia as $zlecenie) {
	    $ajaxZlecenie = new \Zlecenia\Entity\ZlecenieAjax();

	    $dniDoKonca = $zlecenie->getDniDoOddania();

	    $ajaxZlecenie->nazwa = $zlecenie->nazwa;
	    $ajaxZlecenie->klient = '' . $zlecenie->klient;
	    $ajaxZlecenie->opiekun = '' . $zlecenie->opiekun;
	    $ajaxZlecenie->dataOddania = '' . $zlecenie->termi_oddania;
	    $ajaxZlecenie->iloscDniDoKonca = $dniDoKonca;
	    $ajaxZlecenie->status = '' . $zlecenie->status;
	    $ajaxZlecenie->stylKlient = $kolorHelper->pobierzStyl($zlecenie->klient);
	    $ajaxZlecenie->dniKolor = $zlecenie->getKlaseKoloru();
	    $ajaxZlecenie->klientId = $zlecenie->klient->id . '';
	    $ajaxZlecenie->id = $zlecenie->id . '';


	    $wynik[] = $ajaxZlecenie;
	}

	return $wynik;
    }

}
