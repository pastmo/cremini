<?php

namespace Zlecenia\Controller\Factory;

use Zlecenia\Controller\ZleceniaController;

class ZleceniaControllerFactory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

	protected function newController() {
		return new ZleceniaController();
	}

}
