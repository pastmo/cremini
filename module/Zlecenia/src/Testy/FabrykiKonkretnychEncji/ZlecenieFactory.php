<?php

namespace Zlecenia\Testy\FabrykiKonkretnychEncji;

class ZlecenieFactory extends \Pastmo\Testy\Fabryki\FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Zlecenia\Entity\Zlecenie($this->parametryFabryki->sm);
	$encja->nazwa = __CLASS__;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Zlecenia\Model\ZleceniaTable::class;
    }

    public function dowiazInneTabele() {

    }

}
