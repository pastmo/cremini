<?php

namespace Zlecenia\Testy;

class FabrykaRekordow {

    public static function makeEncje($klasa, $parametryFabryki) {
	switch ($klasa) {
	    case \Zlecenia\Entity\Zlecenie::class:
		return (new FabrykiKonkretnychEncji\ZlecenieFactory($parametryFabryki))->makeEncje();
	}
	return false;
    }

}
