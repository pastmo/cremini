<?php

namespace Zlecenia\Entity;

use Klienci\Entity\Klient;

class Zlecenie extends \Wspolne\Model\WspolneModel {

    public $id;
    public $klient;
    public $nr_zlecenia;
    public $nazwa;
    public $opis;
    public $status;
    public $data_rozpoczecia;
    public $data_zakonczenia;
    private $zalaczniki;

    public function __construct($sm = null) {
	parent::__construct($sm);
    }

    public function getZalaczniki() {
	$this->dowiazListyTabelObcych();
	return $this->zalaczniki;
    }

    public function exchangeArray($data) {
	parent::exchangeArray($data);

	$this->id = $this->pobierzLubNull($data, 'id');
	$this->nr_zlecenia = $this->pobierzLubNull($data, 'nr_zlecenia');
	$this->nazwa = $this->pobierzLubNull($data, 'nazwa');
	$this->opis = $this->pobierzLubNull($data, 'opis');
	$this->status = $this->pobierzLubNull($data, 'status');
	$this->data_rozpoczecia = $this->pobierzLubNull($data, 'data_rozpoczecia');
	$this->data_zakonczenia = $this->pobierzLubNull($data, 'data_zakonczenia');

	$this->klient = $this->pobierzTabeleObca('klient_id', \Klienci\Module::KLIENCI_TABLE, Klient::create());
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'klient':
		return 'klient_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public function dowiazListyTabelObcych() {
	if ($this->czyTabeleDostepne()) {
	    $zasobyTable = $this->sm->get(\Zasoby\Menager\ZasobyUploadMenager::class);

	    $this->zalaczniki = $zasobyTable->pobierzZWherem("id in (select zasob_id from zlecenia_zasoby zz where zlecenie_id=" . $this->id . ")");
	}
    }

    public static function create() {
	$zlecenie = new Zlecenie();
	$zlecenie->klient = new Klient();
	return $zlecenie;
    }

}
