<?php

namespace Zlecenia\Entity;

class Kategoria extends \Wspolne\Model\WspolneModel {

	public $id;
	public $nazwa;

	public function __construct($sm = null) {
		parent::__construct($sm);
	}

	public function exchangeArray($data) {
		$this->id = $this->pobierzLubNull($data, 'id');
		$this->nazwa = $this->pobierzLubNull($data, 'nazwa');
	}

	public function konwertujNaKolumneDB($nazwaWKodzie) {

		return $nazwaWKodzie;
	}

	public function pobierzKlase() {
		return __CLASS__;
	}

	public function dowiazListyTabelObcych() {

	}

}
