<?php

namespace Zlecenia\Entity;

class ZlecenieZasob extends \Wspolne\Model\WspolneModel {

	public $id;
	public $zlecenie_id;
	public $zasob_id;

	public function __construct($sm = null) {
		parent::__construct($sm);
	}

	public function exchangeArray($data) {
		$this->id = $this->pobierzLubNull($data, 'id');
		$this->zlecenie_id = $this->pobierzLubNull($data, 'zlecenie_id');
		$this->zasob_id = $this->pobierzLubNull($data, 'zasob_id');
	}

	public function konwertujNaKolumneDB($nazwaWKodzie) {

		return $nazwaWKodzie;
	}

	public function pobierzKlase() {
		return __CLASS__;
	}

	public function dowiazListyTabelObcych() {

	}

}
