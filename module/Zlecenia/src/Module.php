<?php

namespace Zlecenia;

use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zlecenia\Entity\Zlecenie;
use Zlecenia\Entity\Kategoria;
use Zlecenia\Entity\ZlecenieZasob;
use Zlecenia\Entity\CenaZlecenia;
use Zlecenia\Entity\CenaZleceniaOplata;

class Module implements ConfigProviderInterface {

	const ZLECENIA_TABLE='Zlecenia\Model\ZleceniaTable';
	const ZLECENIA_GATEWAY='ZleceniaGateway';
	const KATEGORIE_TABLE='Zlecenia\Model\KategorieTable';
	const KATEGORIE_GATEWAY='ZleceniaKategorieGateway';
	const ZLECENIE_ZASOB_TABLE='Zlecenia\Model\ZlecenieZasobTable';
	const ZLECENIE_ZASOB_GATEWAY='ZlecenieZasobGateway';
	const CENNIK_ZLECEN_TABLE='CennikZlecenTable';
	const CENNIK_ZLECEN_GATEWAY='CennikZlecenGateway';
	const CENNIK_ZLECEN_OPLATY_GATEWAY='CennikZlecenOplatyGateway';


	public function getConfig() {
		return include __DIR__ . '/../config/module.config.php';
	}

	public function getServiceConfig() {
		return array(
			'factories' => array(
				self::ZLECENIA_TABLE => function($sm) {

					$table = new Model\ZleceniaTable($sm);
					return $table;
				},
				self::ZLECENIA_GATEWAY => function ($sm) {
					$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
					$resultSetPrototype = new ResultSet();
					$resultSetPrototype->setArrayObjectPrototype(new Zlecenie($sm));
					return new TableGateway('zlecenia', $dbAdapter, null, $resultSetPrototype);
				},
				self::KATEGORIE_TABLE=> function($sm) {
					$table = new Model\KategorieTable($sm);
					return $table;
				},
				self::KATEGORIE_GATEWAY => function ($sm) {
					$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
					$resultSetPrototype = new ResultSet();
					$resultSetPrototype->setArrayObjectPrototype(new Kategoria($sm));
					return new TableGateway('zlecenia_kategorie', $dbAdapter, null, $resultSetPrototype);
				},
				self::ZLECENIE_ZASOB_TABLE=> function($sm) {
					$table = new Model\ZlecenieZasobTable($sm);
					return $table;
				},
				self::ZLECENIE_ZASOB_GATEWAY => function ($sm) {
					$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
					$resultSetPrototype = new ResultSet();
					$resultSetPrototype->setArrayObjectPrototype(new ZlecenieZasob($sm));
					return new TableGateway('zlecenia_zasoby', $dbAdapter, null, $resultSetPrototype);
				},

			),
		);
	}

}
