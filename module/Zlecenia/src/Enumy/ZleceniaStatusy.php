<?php
namespace Zlecenia\Enumy;

class ZleceniaStatusy {

	const NOWE = 'Nowe';
	const PRZYJETE = 'Przyjete';
	const W_TRAKCIE_REALIZACJI = 'W trakcie realizacji';
	const GOTOWE_DO_ODBIORU = 'Gotowe do odbioru';
	const ODEBRANE = 'Odebrane';

}
