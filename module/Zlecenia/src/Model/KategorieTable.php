<?php

namespace Zlecenia\Model;

use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;

class KategorieTable extends BazowyMenagerBazodanowy {

	protected $sm;

	public function __construct(ServiceManager $sm) {
		parent::__construct($sm,'ZleceniaKategorieGateway');
	}

	public function wydobadzKategorieZPosta($post) {

		$kategoriId = $this->pobierzLubNull($post, 'kategoria_id');
		$czyNowa = $this->pobierzLubNull($post, 'czy_nowa_kategoria');
		$nowaKategoria = $this->pobierzLubNull($post, 'nowa_kategoria');

		if ($czyNowa) {
			$kategoria = new \Zlecenia\Entity\Kategoria();
			$kategoria->nazwa = $nowaKategoria;
			$this->zapisz($kategoria);
			return $this->getPoprzednioDodany();
		} else {
			return $this->getRekord($kategoriId);
		}
	}

}
