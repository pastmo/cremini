<?php

namespace Zlecenia\Model;

use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;

class ZlecenieZasobTable extends BazowyMenagerBazodanowy {

	protected $sm;

	public function __construct(ServiceManager $sm) {
		parent::__construct($sm, \Zlecenia\Module::ZLECENIE_ZASOB_GATEWAY);
	}

}
