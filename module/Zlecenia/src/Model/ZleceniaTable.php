<?php

namespace Zlecenia\Model;

use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;
use Zlecenia\Entity\Zlecenie;
use Zlecenia\Entity\ZlecenieZasob;
use Zlecenia\Entity\CenaZlecenia;

class ZleceniaTable extends BazowyMenagerBazodanowy {

    protected $sm;

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, 'ZleceniaGateway');
    }

    public function zapisz(\Pastmo\Wspolne\Entity\WspolnyModel $model) {

	if (!$model->nr_zlecenia) {
	    $poprzednich = $this->pobierzZWheremCount('1');
	    $poprzednich++;
	    $model->nr_zlecenia = $poprzednich;
	}

	parent::zapisz($model);
    }

    public function zapiszZlecenie($form) {


	$zlecenie = new Zlecenie($this->sm);
	$zlecenie->exchangeArray($form);

	$this->zapisz($zlecenie);
	$zlecenie = $this->getPoprzednioDodany();

	$zasobyTable = $this->sm->get(\Zasoby\Menager\ZasobyUploadMenager::class);
	$zasoby = $zasobyTable->pobierzIdZasobow($form, 'pliki');

	foreach ($zasoby as $zasob) {
	    $zlecenieZasob = new ZlecenieZasob();
	    $zlecenieZasob->zasob_id = $zasob;
	    $zlecenieZasob->zlecenie_id = $zlecenie->id;
	    $this->zapiszWInnymTable($zlecenieZasob, \Zlecenia\Module::ZLECENIE_ZASOB_TABLE);
	}
    }

    public function zakoncz($id) {
	$this->update($id, array('zakonczone' => true,
	));
    }

    public function zaktualizujStatus($status, $id) {

	$set = array('status' => $status);

	$this->update($id, $set);
    }

    public function zapiszZalacznikDlaZlecenia($request, $id) {
	if ($request->isPost()) {
	    $post = $request->getPost();

	    $zasobyTable = $this->sm->get(\Zasoby\Menager\ZasobyUploadMenager::class);
	    $zasoby = $zasobyTable->pobierzIdZasobow($post, 'pliki');

	    foreach ($zasoby as $zasob) {
		$zlecenieZasob = new ZlecenieZasob();
		$zlecenieZasob->zasob_id = $zasob;
		$zlecenieZasob->zlecenie_id = $id;
		$this->zapiszWInnymTable($zlecenieZasob, \Zlecenia\Module::ZLECENIE_ZASOB_TABLE);
	    }
	}
    }

    public function usun($where) {
	$zlecenia = $this->pobierzZWherem($where);

	foreach ($zlecenia as $zlecenie) {
	    $this->usunZTabeliObcejZWherem(\Zlecenia\Module::ZLECENIE_ZASOB_TABLE, 'zlecenie_id', $zlecenie->id);
	}

	parent::usun($where);
    }

    public function liczbaZlecenZakonczonychOstatniTydzien() {
	$where = $this->whereTenTydzien();
	return $this->pobierzZWheremCount($where);
    }

    public function liczbaZlecenZakonczonychOstatniMiesiac() {
	$where = $this->whereTenMiesiac();
	return $this->pobierzZWheremCount($where);
    }

    public function liczbaZlecenZakonczonychOstatniRok() {
	$where = $this->whereTegoroczne();
	return $this->pobierzZWheremCount($where);
    }

    public function liczbaZlecenZakonczonych() {
	$where = $this->whereZakonczone();
	return $this->pobierzZWheremCount($where);
    }

    private function whereTenTydzien() {
	return 'WEEK(data_zakonczenia) = WEEK(CURDATE()) AND '.$this->whereTenMiesiac();
    }
    private function whereTenMiesiac() {
	return 'MONTH(data_zakonczenia) = MONTH(CURDATE()) AND '.$this->whereTegoroczne();
    }
    private function whereTegoroczne() {
	return 'YEAR(data_zakonczenia) = YEAR(CURDATE()) AND '.$this->whereZakonczone();
    }

    private function whereZakonczone() {
	return '1';
    }

}
