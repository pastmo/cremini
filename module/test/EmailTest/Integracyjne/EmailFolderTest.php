<?php

namespace EmailTest\Integracyjne;

use Pastmo\Testy\Util\TB;
use \Email\Entity\EmailFolder;

class EmailFolderTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    public function setUp() {
	parent::setUp();
    }

    public function test_zapisEncji() {
	$emailFolder = TB::create(EmailFolder::class, $this)->setPf(\Pastmo\Testy\ParametryFabryki\PFIntegracyjneBrakzapisuPowiazane::create($this->sm))->make();
	$emailFolderMenager = $this->sm->get(\Email\Menager\EmaileFolderyMenager::class);

	$emailFolderMenager->zapisz($emailFolder);
	$dodany = $emailFolderMenager->getPoprzednioDodany();
	$this->assertNotNull($dodany->id);
	$this->assertInstanceOf(EmailFolder::class, $dodany);
    }

}
