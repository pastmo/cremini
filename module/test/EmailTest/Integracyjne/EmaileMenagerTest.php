<?php

namespace EmailTest\Integracyjne;

require_once dirname(__FILE__) . "/../../../../Pastmo/test/EmailTest/Integracyjne/EmaileMenagerTest.php";

use Pastmo\Testy\Util\TB;
use \Pastmo\Wiadomosci\Enumy\SkrzynkiWiadomosci;

class EmaileMenagerTest extends \Pastmo\EmailTest\Integracyjne\EmaileMenagerTest {

    protected $kontoMailowe;
    private $emailFolder;
    private $emaile;

    public function setUp() {
	parent::setUp();
    }

    public function test_zapisEncji() {
	parent::test_zapisEncji();
	$this->emaile = array();
    }

    public function test_zapisEncji_zalaczniki() {
	parent::test_zapisEncji_zalaczniki();
    }

    public function test_pobierzEmaileDlaKontaISkrzynki() {
	$this->utworzSkrzynkeMailowa();
	$this->utworzEmail(SkrzynkiWiadomosci::ODBIORCZA);

	$emaile = $this->emaileMenager->pobierzEmaileDlaKontaISkrzynki($this->kontoMailowe->id,
		SkrzynkiWiadomosci::ODBIORCZA);

	$this->assertEquals(1, $emaile->count());
    }

    public function test_pobierzEmaileDlaKontaISkrzynkiFolder() {
	$this->utworzSkrzynkeMailowa();
	$this->utworzEmailFolder();
	$this->utworzEmail(SkrzynkiWiadomosci::ODBIORCZA);
	$this->utworzEmail(SkrzynkiWiadomosci::ODBIORCZA, $this->emailFolder->id . '');

	$emaile = $this->emaileMenager->pobierzEmaileDlaKontaISkrzynki($this->kontoMailowe->id,
		$this->emailFolder->id . '');

	$this->assertEquals(1, $emaile->count());
	$this->assertEquals($this->emaile[1]->id, $emaile->current()->id);
    }

    public function test_zmienSkrzynke_folder() {
	$this->parametryDoZmianySkrzynek();
	$this->zmienSkrzynke($this->emailFolder->id, $this->emailFolder->id);
    }

    public function test_zmienSkrzynke_skrzynka() {
	$this->parametryDoZmianySkrzynek();
	$this->zmienSkrzynke(SkrzynkiWiadomosci::ODBIORCZA, null);
    }

    private function parametryDoZmianySkrzynek(){
	$this->utworzSkrzynkeMailowa();
	$this->utworzEmailFolder();
	$this->utworzEmail(SkrzynkiWiadomosci::KOSZ);
    }

    public function zmienSkrzynke($nowaSkrzynka, $emailFolderSprawdzanie) {


	$this->emaileMenager->zmienSkrzynke($this->emaile[0]->id, $nowaSkrzynka);

	$zmieniony = $this->emaileMenager->getRekord($this->emaile[0]->id);

	$this->assertEquals(SkrzynkiWiadomosci::ODBIORCZA, $zmieniony->skrzynka);
	$this->assertEquals($emailFolderSprawdzanie, $zmieniony->email_folder->id);
    }

    private function utworzSkrzynkeMailowa() {
	$this->kontoMailowe = TB::create(\Pastmo\Email\Entity\KontoMailowe::class,
			$this)->make();
    }

    private function utworzEmailFolder() {
	$this->emailFolder = TB::create(\Email\Entity\EmailFolder::class, $this)
		->setParameters(array('konto_mailowe' => $this->kontoMailowe))
		->make();
    }

    private function utworzEmail($skrzynka, $folder = null) {
	$this->emaile[] = TB::create(\Email\Entity\Email::class, $this)
		->setParameters(array(
		    'skrzynka' => $skrzynka,
		    'konto_mailowe' => $this->kontoMailowe,
		    'email_folder' => $folder
		))
		->make();
    }

    protected function getEmaileMenagerClass() {
	return \Email\Menager\EmaileMenager::class;
    }

    protected function nowyEmail() {
	return new \Email\Entity\Email($this->sm);
    }

    protected function konwertujZeSkanera($mailSkanera, $kontoMailowe) {
	return \Email\Entity\Email::konwertujZMailSkanera($mailSkanera, $kontoMailowe,$this->sm);
    }

    protected function emailClass() {
	return \Email\Entity\Email::class;
    }

}
