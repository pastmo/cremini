<?php

namespace Testy\FabrykiKonkretnychEncji;

class ProjektowyWatekChatuFactory extends \FabrykaAbstrakcyjna {

    public $uzytkownik;
    public $projektowyWatekChatu;
    public $watekWiadomosci;
    public $watekChatuUzytkownik;

    public function nowaEncja() {
	$encja = new \Konwersacje\Entity\ProjektowyWatekChatu();
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Konwersacje\Menager\ProjektoweWatkiChatuMenager::class;
    }

    public function dowiazInneTabele() {
	$this->encja->watek_wiadomosci = $this->uzyjWlasciwejFabryki(\Konwersacje\Entity\WatekWiadomosci::class);
    }

    public static function create(\Pastmo\Testy\ParametryFabryki\ParametryFabryki $pf) {
	$factory = new ProjektowyWatekChatuFactory($pf);
	return $factory;
    }

    public function utworzPojedynczyWatekChatu() {

	$this->watekWiadomosci = $this->uzyjWlasciwejFabryki(\Konwersacje\Entity\WatekWiadomosci::class,
		array('typ' => \Konwersacje\Enumy\WatkiWiadomosciTypy::PROJEKTOWY));
	$this->projektowyWatekChatu = $this->uzyjWlasciwejFabryki(\Konwersacje\Entity\ProjektowyWatekChatu::class,
		array('watek_wiadomosci' => $this->watekWiadomosci->id));

	$this->uzytkownik = $this->uzyjWlasciwejFabryki(\Logowanie\Model\Uzytkownik::class);
	$this->dodajWatekChatuUzytkownika($this->projektowyWatekChatu, $this->uzytkownik);
    }

    public function dodajWatekChatuUzytkownika($watekChatu, $uzytkownik) {
	$this->watekChatuUzytkownik = $this->uzyjWlasciwejFabryki(\Konwersacje\Entity\ProjektowyWatekChatuUzytkownik::class,
		array('projektowy_watek_chatu' => $watekChatu, 'uzytkownik' => $uzytkownik));
    }

}
