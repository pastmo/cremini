<?php

namespace Testy\FabrykiKonkretnychEncji;

class UzytkownikPowiadomienieKategoriaFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Dashboard\Entity\UzytkownikPowiadomienieKategoria();
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Dashboard\Menager\UzytkownicyPowiadomieniaKategorieMenager::class;
    }

    public function dowiazInneTabele() {
	$this->encja->uzytkownik = $this->uzyjWlasciwejFabryki(\Logowanie\Model\Uzytkownik::class);
	$this->encja->powiadomienie_kategoria = $this->uzyjWlasciwejFabryki(\Dashboard\Entity\PowiadomienieKategoria::class);
    }

}
