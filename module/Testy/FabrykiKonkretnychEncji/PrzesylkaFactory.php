<?php

namespace Testy\FabrykiKonkretnychEncji;

class PrzesylkaFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Wysylka\Entity\Przesylka();
	$encja->zawartosc_przesylki = \Wysylka\Enumy\PrzesylkaZawartosc::KOMPUTERY;
	$encja->forma_platnosci = \Wysylka\Enumy\PrzesylkaPlatnosc::PRZELEW;
	$encja->status = \Wysylka\Enumy\PrzesylkaStatus::NOWA;
	$encja->kod_uslugi = "AH";
	$encja->data_nadania = date("Y-m-d", strtotime("next Friday"));
	$encja->komentarz = "Zwykły komentarz.";
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Wysylka\Menager\PrzesylkiMenager::class;
    }

    public function dowiazInneTabele() {
	$this->encja->projekt = $this->uzyjWlasciwejFabryki(\Projekty\Entity\Projekt::class);
	$this->encja->adres_nadawcy = $this->uzyjWlasciwejFabryki(\Firmy\Entity\Adres::class);
	$this->encja->adres_odbiorcy = $this->uzyjWlasciwejFabryki(\Firmy\Entity\Adres::class,
		['ulica' => 'Myśliwiecka']);
    }

}
