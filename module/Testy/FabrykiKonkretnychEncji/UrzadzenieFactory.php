<?php

namespace Testy\FabrykiKonkretnychEncji;

class UrzadzenieFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Produkcja\Entity\Urzadzenie;
	$encja->nazwa = "Laser urządzenie";
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Produkcja\Menager\UrzadzeniaMenager::class;
    }

}
