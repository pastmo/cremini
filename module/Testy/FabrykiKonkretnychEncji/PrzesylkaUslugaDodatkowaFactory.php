<?php

namespace Testy\FabrykiKonkretnychEncji;

class PrzesylkaUslugaDodatkowaFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Wysylka\Entity\PrzesylkaUslugaDodatkowa();
	$encja->forma_zwrotu = \Wysylka\Enumy\PrzesylkaPlatnosc::GOTOWKA;
	$encja->kod = "KOD";
	$encja->kwota = 123;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Wysylka\Menager\PrzesylkiUslugiDodatkoweMenager::class;
    }

    public function dowiazInneTabele() {
	$this->encja->przesylka = $this->uzyjWlasciwejFabryki(\Wysylka\Entity\Przesylka::class);
    }

}
