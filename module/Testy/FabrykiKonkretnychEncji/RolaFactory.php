<?php

namespace Testy\FabrykiKonkretnychEncji;

class RolaFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Logowanie\Entity\Rola;
	$encja->nazwa = "nowa rola";
	$encja->kod = "nowa_rola";
	$encja->typ = \Logowanie\Model\TypyUzytkownikow::KLIENT;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Logowanie\Menager\RoleMenager::class;
    }

}
