<?php

namespace Testy\FabrykiKonkretnychEncji;

class WycenaProducentaFirmaFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Aukcje\Entity\WycenaProducentaFirma();
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Aukcje\Menager\WycenyProducentowFirmyMenager::class;
    }

    public function dowiazInneTabele() {
	$this->encja->wycena_producentow = $this->uzyjWlasciwejFabryki(\Aukcje\Entity\WycenaProducenta::class);
	$this->encja->firma = $this->uzyjWlasciwejFabryki(\Firmy\Entity\Firma::class);
    }

}
