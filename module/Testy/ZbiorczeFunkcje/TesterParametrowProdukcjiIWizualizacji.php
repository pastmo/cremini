<?php

namespace Testy\ZbiorczeFunkcje;

use Pastmo\Testy\Util\TB;

class TesterParametrowProdukcjiIWizualizacji extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    public $wlasciciel;

    public function __construct($wlasciciel) {
	$this->wlasciciel = $wlasciciel;
    }

    public function zapiszParametryWlasciciela($glownaEncja) {
	$czasPracyPoczatkowy = 60;
	$czasPracyKoncowy = 90;

	$parametryWizualizacji = TB::create($glownaEncja, $this->wlasciciel)
			->setPF_IZP($this->wlasciciel->sm)
			->setParameters(array('czas_pracy' => $czasPracyPoczatkowy))->make();
	$projekt = TB::create(\Projekty\Entity\Projekt::class, $this->wlasciciel)->setParameters(array($this->wlasciciel->menager->getKluczWEncjiProjekt() => $parametryWizualizacji))->make();

	$this->wlasciciel->menager->zapiszParametry(array('project_id' => $projekt->id, 'preview_time' => $czasPracyKoncowy));

	$zapisany = $this->wlasciciel->menager->getRekord($parametryWizualizacji->id);

	$this->assertEquals($czasPracyKoncowy, $zapisany->czas_pracy);
	$this->sprwadzCzasyPracyDlaParametru($zapisany);
    }

    protected function sprwadzCzasyPracyDlaParametru($parametr) {
	$czasyPracyMenager = $this->wlasciciel->sm->get(\Wizualizacje\Menager\CzasyPracyMenager::class);
	$czasPracy = $czasyPracyMenager->getCzasyPracyDlaParametruWlasciciela($parametr);

	$komponent = \Wizualizacje\Entity\CzasyPracyKomponent::create($czasPracy);

	$this->assertEquals($parametr->czas_pracy, $komponent->getCzasTrwania());
    }

}
