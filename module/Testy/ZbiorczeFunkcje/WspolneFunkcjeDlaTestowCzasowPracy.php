<?php

namespace Testy\ZbiorczeFunkcje;

use Pastmo\Testy\Util\TB;

trait WspolneFunkcjeDlaTestowCzasowPracy {

    protected $cp;
    protected $cp_last;
    protected $parametryWlasciciela;
    protected $parametryProdukcji;

    protected function zrobCzasPracyZNowymParametrem($dataOd, $dataDo) {
	$this->zrobParametr();
	$this->usunCzasyPracyParametru();
	return $this->zrobCzasPracy($dataOd, $dataDo);
    }

    protected function zrobParametr() {

	$wszystkieParametry = array_merge($this->parametry, array('grafik' => $this->grafik));

	$this->parametryWlasciciela = TB::create(\Projekty\Entity\ParametryWizualizacji::class, $this)
			->setParameters($wszystkieParametry)->make();
    }

    protected function zrobParametrProdukcji() {

	$wszystkieParametry = array_merge($this->parametry, array('urzadzenie' => $this->urzadzenie));

	$this->parametryProdukcji = TB::create(\Produkcja\Entity\ParametryProdukcji::class, $this)
			->setParameters($wszystkieParametry)->make();
    }

    protected function usunCzasyPracyParametru() {
	$this->czasyPracyMenager->usunCzasyPracyParametruWlasciciela($this->parametryWlasciciela);
    }

    protected function zrobCzasPracy($dataOd, $dataDo) {

	return TB::create(\Wizualizacje\Entity\ CzasPracy::class, $this)->setParameters(array(
			'parametry_wizualizacji' => $this->parametryWlasciciela,
			'typ' => \Wizualizacje\Enumy\TypyCzasowPracy::pobierzTypWlasciciela($this->parametryWlasciciela),
			'data_od' => $dataOd,
			'data_do' => $dataDo))->make();
    }

    protected function zrobTrwajacyProjekt($parametrProdukcji = null, $parametrWizualizacji = null) {
	TB::create(\Projekty\Entity\Projekt::class, $this)
		->setParameters(array(
			'parametry_produkcji' => $parametrProdukcji,
			'parametry_wizualizacji' => $parametrWizualizacji
		))->make();
    }

}
