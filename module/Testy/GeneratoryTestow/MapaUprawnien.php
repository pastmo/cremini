<?php

namespace Testy\GeneratoryTestow;

use Logowanie\Enumy\KodyUprawnien;
use Pastmo\Testy\GeneratoryTestow\TestUprawnienie;

class MapaUprawnien extends \Pastmo\Testy\GeneratoryTestow\MapaUprawnien {

    const NIEOBSLUGIWANA_AKCJA = 'Akcja nieobsługiwana';
    const BRAK_PASUJACEGO_UPRAWNIENIA = 'Brak pasującego uprawnienia';

    public function uprawnienia() {
	return[];
    }

    public $klasyKontrolerow = array(
	    \Email\Controller\EmailController::class,
	    \Logowanie\Controller\LogowanieController::class,
	    \Wspolne\Controller\AjaxController::class,
	    \Zasoby\Controller\ZasobyController::class,
    );
    public $akcjeBezSprawdzanychUprawnien = [
	    '/logowanie/po_rejestracji',
	    '/logowanie/rejestracja',
	    '/logowanie/logowanie',
	    '/logowanie/reset_hasla',
	    '/logowanie/resetuj',
	    '/logowanie/aktywuj',
	    '/logowanie',
	    '/logowanie/logout',
    ];

}
