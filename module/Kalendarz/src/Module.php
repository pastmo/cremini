<?php

namespace Kalendarz;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\MvcEvent;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface {

	public function onBootstrap(MvcEvent $e) {
		$e->getApplication()->getServiceManager()->get('ViewHelperManager')
				->setFactory('cykliczneZdarzeniaButton',
						function($sm) use ($e) {
					$viewHelper = new Helper\WyswietlPrzyciskDodajacyCykliczneZdarzeniaHelper();
					return $viewHelper;
				});
	}

	public function getAutoloaderConfig() {
		return array(
			'Zend\Loader\ClassMapAutoloader' => array(
				__DIR__ . '/../autoload_classmap.php',
			),
			'Zend\Loader\StandardAutoloader' => array(
				'namespaces' => array(
					__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
				),
			),
		);
	}

	public function getConfig() {
		return include __DIR__ . '/../config/module.config.php';
	}

	public function getServiceConfig() {
		return array(
			'factories' => array(
			),
		);
	}

}
