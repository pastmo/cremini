<?php

namespace Kalendarz\Helper;

use Zend\View\Helper\AbstractHelper;

class WyswietlPrzyciskDodajacyCykliczneZdarzeniaHelper extends AbstractHelper {

	public function __construct() {

	}

	public function __invoke($zlecenie, $view) {
		?>
<input class="cykliczne_zdarzenie_input" type="hidden" name="powtarzanie">
<input class="cykliczne_zdarzenie_input2" type="hidden" name="powtarzanie2">
		<a href="#" class="btn btn-danger cykliczne_zdarzenie_btn zapis_zlecen">Cykliczne zdarzenie</a>

		<div id="kalendarz_cykliczne_zdarzenie_wraper">
			<div id="kalendarz_cykliczne_zdarzenie">
				<div>
					<label >Powtarzanie:</label>
					<select class="powtarzenie">
						<option value="codziennie">Codziennie</option>
						<option value="powszednie">W dni powszednie (od poniedziałku do piątku)</option>
						<option value="pn_sr_pt">W każdy poniedziałek, środę i piątek</option>
						<option value="wt_czw">W każdy wtorek i czwartek</option>
						<option value="tydzien">Co tydzień</option>
						<option value="miesiac">Co miesiac</option>
						<option value="rok">Co rok</option>
					</select>
				</div>

				<div class="kroki_cykl k_codziennie k_tydzien k_miesiac k_rok">
					<label >Powtarzaj co:</label>
					<select id="powtarzaj_co">
						<?php for ($i = 1; $i < 31; $i++): ?>
							<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
						<?php endfor; ?>
					</select>
				</div>

				<div class="kroki_cykl k_tydzien" style="display: none">
					<label >Powtarzaj w:</label>

					<input name="powtarzaj_w" type="checkbox" value="1"><label>P</label>
					<input name="powtarzaj_w" type="checkbox" value="2"><label>W</label>
					<input name="powtarzaj_w" type="checkbox" value="3"><label>Ś</label>
					<input name="powtarzaj_w" type="checkbox" value="4"><label>C</label>
					<input name="powtarzaj_w" type="checkbox" value="5"><label>P</label>
					<input name="powtarzaj_w" type="checkbox" value="6"><label>S</label>
					<input name="powtarzaj_w" type="checkbox" value="0"><label>N</label>
				</div>

				<div>
					<label >Koniec:</label>
					<div>
						<input class="koniec" name="koniec" value="nigdy" id="nigdy_checkbox" type="radio" checked="">
						<label>Nigdy</label>
					</div>
					<div>
						<input class="koniec" name="koniec" value="w_dniu" id="w_dniu_checkbox" type="radio">
						<label>W dniu</label>
						<input id="w_dniu" type="date">
					</div>
				</div>



			</div>
		</div><?php
		\Application\Controller\LadowaczSkryptow::add($view->basePath('js/kalendarz/cykliczne_zdarzenia.js'));
	}

}
