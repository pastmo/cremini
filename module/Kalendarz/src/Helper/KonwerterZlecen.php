<?php

namespace Kalendarz\Helper;

use Kalendarz\Entity\RekordZlecenia;
use \Pastmo\Wspolne\Utils\EntityUtil;

class KonwerterZlecen {

    public static function przetworzZlecenia($zlecenia) {
	$wynik = [];
	foreach ($zlecenia as $zlecenie) {

	    $tytul = "".$zlecenie->nazwa;

	    $rekord = RekordZlecenia::create()
		    ->setData_rozpoczecia($zlecenie->data_rozpoczecia)
		    ->setId($zlecenie->id)
		    ->setTytul($tytul);

	    if ($zlecenie->data_rozpoczecia === $zlecenie->data_zakonczenia) {
		$wynik[] = $rekord;
	    } else if ($zlecenie->data_zakonczenia != null) {
		$rekord->setTytul("Start: $tytul");

		$rekord2 = clone $rekord;
		$rekord2->setTytul("Koniec: $tytul")
			->setData_rozpoczecia($zlecenie->data_zakonczenia);

		$wynik[] = $rekord;
		$wynik[] = $rekord2;
	    }
	}

	return $wynik;
    }

}
