<?php

namespace Kalendarz\Entity;


class RekordZlecenia {

    public $id;
    public $tytul;
    public $data_rozpoczecia;

    public static function create() {
	return new RekordZlecenia();
    }
    function setId($id) {
	$this->id = $id;
	return $this;
    }

    function setTytul($tytul) {
	$this->tytul = $tytul;
	return $this;
    }

    function setData_rozpoczecia($data_rozpoczecia) {
	$this->data_rozpoczecia = $data_rozpoczecia;
	return $this;
    }


}
