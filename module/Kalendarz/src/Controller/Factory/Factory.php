<?php

namespace Kalendarz\Controller\Factory;

use Kalendarz\Controller\KalendarzController;

class Factory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

	protected function newController() {
		return new KalendarzController();
	}

}
