<?php

namespace Kalendarz\Controller;

use Zend\View\Model\ViewModel;

class KalendarzController extends \Wspolne\Controller\WspolneController {

	protected $zasobyTable;

	public function indexAction() {
		$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::rol_DOMYSLNE_UPRAWNIENIE,
				true);

		$zlecenia = $this->zleceniaTable->pobierzResultSetZWherem('data_rozpoczecia is not null',
				\Zlecenia\Model\ZleceniaTable::DOMYSLNY_ORDER, false);

		$zleceniaDoWyswietlenia=  \Kalendarz\Helper\KonwerterZlecen::przetworzZlecenia($zlecenia);
		$glownyWidok = new ViewModel(array(
			'zlecenia' => $zleceniaDoWyswietlenia
		));

		$dodawanieZlecenia = $this->dodajEdytujWidok('', 0);

		$glownyWidok->addChild($dodawanieZlecenia, 'dodawanie_zlecenia');

		return $glownyWidok;
	}

	public function testyAction(){}

}
