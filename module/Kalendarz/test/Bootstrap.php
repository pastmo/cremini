<?php
namespace KalendarzTest;

require_once dirname(__FILE__) . '/../../Wspolne/test/WspolneBootstrap.php';

chdir(__DIR__);

class Bootstrap extends \WspolneTest\WspolneBootstrap {

}

Bootstrap::init(array(
	'Aukcje',
));
Bootstrap::chroot();
