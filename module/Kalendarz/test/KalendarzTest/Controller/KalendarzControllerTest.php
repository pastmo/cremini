<?php

namespace KalendarzTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zasoby\Controller\ZasobyController;

class KalendarzControllerTest extends AbstractHttpControllerTestCase {

	protected $traceError = true;

	public function setUp() {
		$this->setApplicationConfig(
				include \Application\Stale::configPath
		);
		parent::setUp();
	}
//TODO: AKtywawać test
	public function t_estIndex(){
		$this->dispatch('/'.$this->getSmallName());
		$this->assertResponseStatusCode(200);

		$this->assertModuleName($this->getBigName());
		$this->assertControllerName($this->getBigName()."\Controller\\".$this->getBigName());
		$this->assertControllerClass($this->getBigName()."Controller");
		$this->assertMatchedRouteName($this->getSmallName());
	}

	public function getBigName() {
		return 'Kalendarz';
	}

	public function getSmallName() {
		return 'kalendarz';
	}

}
