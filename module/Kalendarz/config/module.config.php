<?php

namespace Kalendarz;

return array(
	'controllers' => array(
		'factories' => array(
			'Kalendarz\Controller\Kalendarz' => Controller\Factory\Factory::class,
		),
	),
	'router' => array(
		'routes' => array(
			'kalendarz' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/kalendarz[/:action][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id' => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Kalendarz\Controller\Kalendarz',
						'action' => 'index',
					),
				),
			),
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Kalendarz' => __DIR__ . '/../view',
		),
	),
);
