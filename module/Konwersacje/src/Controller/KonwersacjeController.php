<?php

namespace Konwersacje\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Logowanie\Enumy\KodyUprawnien;
use Konwersacje\Enumy\TrybOdswiezaniaWiadomosci;
use Pastmo\Wspolne\Utils\SearchPole;
use \Pastmo\Wspolne\Utils\ArrayUtil;
use Pastmo\Wspolne\Adnotacje\DomyslnyJsonModel;

/**
 * @DomyslnyJsonModel
 */
class KonwersacjeController extends \Wspolne\Controller\WspolneController {

    private $wiadomoscId;

    public function dodajAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$post = $this->getPost();
	if ($post) {
	    $this->wiadomosciMenager->zapiszZPosta($post);
	    return $this->returnSuccess();
	}
//todorg ten modul ma bledy, nie mam mozliwosci przetestowania czy wstawienie ponizej $this nie wysypie systemu
	return $this->returnFail(array('msg' => "Brak wiadomości do wysłania"));
    }

    public function watkiAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$zalogowany = $this->uzytkownikTable->getZalogowanegoUsera();
	$watki = $this->watkiWiadomosciTable->pobierzNiepusteDlaUzytkownika($zalogowany->id);

	$watkiDoWyslania = array();

	foreach ($watki as $watek) {
	    $najnowszaWiadomosc = $watek->pobierzNajnowszaWiadomosc();
	    $najnowszaWiadomosc->autor->haslo = "";
	    $avatar = $this->zasobyFasada->wyswietlUrlObrazkaZBasePath($najnowszaWiadomosc->autor->avatar);
	    $dodano = $najnowszaWiadomosc->kiedyDodano();

	    $watkiDoWyslania[] = array(
		    'watek' => $watek,
		    'wiadomosc' => $najnowszaWiadomosc,
		    'avatar' => $avatar,
		    'dodano' => $dodano,
		    'nazwa_wlasciciela' => $watek->pobierzNazweWlasciciela());
	}

	return $this->returnSuccess(array('watki_wiadomosci' => $watkiDoWyslania));
    }

    public function wiadomosciAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);



	$watekId = $this->params()->fromQuery('watek_id');
	$watek = $this->watkiWiadomosciTable->getRekord($watekId);

	$tryb = $this->pobierzTryb($watekId);

	if ($tryb === TrybOdswiezaniaWiadomosci::PELNY) {
	    $wiadomosci = $this->wiadomosciMenager->pobierzWiadomosciDlaWatku($watekId);
	} else {
	    $wiadomosci = $this->wiadomosciMenager->pobierzNajnowszeWiadomosciDlaWatku($watekId, $this->wiadomoscId);
	}


	$doWyslania = array();

	foreach ($wiadomosci as $wiadomosc) {

	    $avatar = $this->zasobyFasada->wyswietlUrlObrazkaZBasePath($wiadomosc->autor->avatar);


	    $doWyslania[] = array(
		    'wiadomosc' => $wiadomosc,
		    'avatar' => $avatar,
		    'dodano' => $wiadomosc->kiedyDodano(),
		    'zalogowany_autorem' => $wiadomosc->czyZalogowanyAutorem(),
	    );
	}

	$uzytkownicy = $watek->getUzytkownicyBezZalogowanego();
	$avatary = $watek->getUzytkownicyAvataryBezZalogowanego();


	return $this->returnSuccess(array(
			'tryb' => $tryb,
			'wiadomosci' => $doWyslania,
			'watek' => $watek,
			'uzytkownicy' => $uzytkownicy,
			'avatary' => $avatary,
	));
    }

    public function najnowszeWiadomosciAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$this->wiadomoscId = $this->pobierzNiepustyZGeta('ostatnia_wiadomosc', 0);

	$wiadomosci = $this->wiadomosciMenager->pobierzNajnowszeWiadomosciZalogowanego($this->wiadomoscId, true);

	return $this->returnSuccess(array('wiadomosci' => \Konwersacje\KomunikatyDoJs\WiadomoscKomunikat::listaAdapter($wiadomosci)));
    }

    public function wiadomosciDoNowegoOkienkaAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::WYSWIETLANIE_CHATU, true);

	$typ = $this->params()->fromQuery('typWatku');
	$watekId = $this->params()->fromQuery('watekId');

	$wiadomosci = $this->wiadomosciMenager->pobierzWiadomosciDlaWatku($watekId);

	return $this->returnSuccess(array('wiadomosci' => \Konwersacje\KomunikatyDoJs\WiadomoscKomunikat::listaAdapter($wiadomosci)));


//pobierzOstatnieWiadomosciDlaWatku
    }

    public function searchAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$uzytkownicyArray = $this->pobierzUzytkownikow();
	$wiadomosciArray = $this->pobierzWiadomosciArray();

	$polaczone = array_merge($uzytkownicyArray, $wiadomosciArray);

	$wynik = \Konwersacje\KomunikatyDoJs\WiadomoscKomunikat::listaAdapter($polaczone);

	return $this->returnSuccess(ArrayUtil::zrobTablice(array('wiadomosci' => $wynik)));
    }

    private function pobierzUzytkownikow() {
	$builder = \Pastmo\Wspolne\Builder\PobieranieStronicowaneBuilder::create()
		->setTable($this->uzytkownikTable)
		->setZwrocPaginator(false)
		->setWynikowNaStronie(5)
		->setDomyslneSortowanie("imie ASC")
		->setPola(array(
		SearchPole::createTypuString('imie'),
		SearchPole::createTypuString('nazwisko'),
		SearchPole::createTypuString('mail'),
		SearchPole::createTypuString('telefon'),
		SearchPole::createTypuString('telefon2'),
		)
	);

	$uzytkownicy = $this->pobierzZGetaStronicowanie($builder);

	$uzytkownicyArray = ArrayUtil::konwertujNaArray($uzytkownicy);
	return $uzytkownicyArray;
    }

    private function pobierzWiadomosciArray() {
	$zalogowany = $this->logowanieFasada->getZalogowanegoUsera();

	$builder = \Pastmo\Wspolne\Builder\PobieranieStronicowaneBuilder::create()
		->setTable($this->wiadomosciMenager)
		->setZwrocPaginator(false)
		->setWynikowNaStronie(5)
		->setDomyslneSortowanie("id ASC")
		->setDodatkowyWhere("id IN (select wiadomosc_id from uzytkownicy_wiadomosci where uzytkownik_id={$zalogowany->id})")
		->setPola(array(
		SearchPole::createTypuString('tresc'),
		)
	);

	$wiadomosci = $this->pobierzZGetaStronicowanie($builder);

	$wiadomosciArray = ArrayUtil::konwertujNaArray($wiadomosci);
	return $wiadomosciArray;
    }

    private function pobierzTryb($watekId) {

	$this->wiadomoscId = $this->params()->fromQuery('ostatnia_wiadomosc');

	if ($this->wiadomoscId) {
	    $wiadomosc = $this->wiadomosciMenager->getRekord($this->wiadomoscId);

	    if ($wiadomosc->watek->id == $watekId) {
		return \Konwersacje\Enumy\TrybOdswiezaniaWiadomosci::NOWE;
	    }
	}
	return \Konwersacje\Enumy\TrybOdswiezaniaWiadomosci::PELNY;
    }

}
