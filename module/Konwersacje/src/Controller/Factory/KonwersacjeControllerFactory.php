<?php

namespace Konwersacje\Controller\Factory;

use Konwersacje\Controller\KonwersacjeController;

class KonwersacjeControllerFactory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

    protected function newController() {
	return new KonwersacjeController();
    }

}
