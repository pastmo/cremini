<?php

namespace Konwersacje\Menager;

use Zend\ServiceManager\ServiceManager;
use Konwersacje\Enumy\WatkiWiadomosciTypy;
use \Pastmo\Wspolne\Utils\EntityUtil;

class WiadomosciZWlascicielemMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    private $autorWiadomosci;
    private $wiadomosc;
    private $typWatkuWiadomosci;
    private $menagerWlasciciela;
    private $wlasciciel;

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, \Konwersacje\Module::WIADOMOSCI_GATEWAY);
    }

    public function dodajWiadomoscDoWlascicielaZPosta($post) {

	$tresc = $post['content'];
	$idProjektu = $post['project_id'];
	$typ = $post['typ'];
	$idUzytkownikowDoUdostpenienia = isset($post['visible_to']) ? $post['visible_to'] : array();
	$idPlikow = isset($post['files']) ? $post['files'] : array();

	if (!is_array($idPlikow)) {
	    $idPlikow = $this->pobierzIdZasobow($post, 'files');
	}

	$this->dodajWiadomoscDoWlasciciela($idProjektu, $tresc, $typ, null, $idUzytkownikowDoUdostpenienia, $idPlikow);
    }

    public function dodajWiadomoscDoWlasciciela($idWlasciciela, $tresc, $typ, $uzytkownik = null,
	    $idUzytkownikowDoUdostpenienia = array(), $idPlikow = array()) {

	$this->ustawParametry($idWlasciciela, $typ, $idUzytkownikowDoUdostpenienia);
	$this->pobierzWatekWiadomosci();
	$this->ustawAutora($uzytkownik);
	$this->zapiszWiadomosc($tresc);
	$this->dodajZasobyDoWiadomosci();
	$this->dodajZasobyPoId($idPlikow);
    }

    public function dodajWiadomoscSystemowa($idWlasciciela, $tresc, $typ,
	    $idUzytkownikowDoUdostpenienia = array(), $idPlikow = array()) {
	\Konwersacje\Model\WiadomosciMenager::$ustawZalogowanegoJakoDomyslny = false;
	$this->ustawParametry($idWlasciciela, $typ, $idUzytkownikowDoUdostpenienia);
	$this->pobierzWatekWiadomosci();
//	$this->ustawAutora($uzytkownik);
	$this->zapiszWiadomosc($tresc);
	$this->dodajZasobyDoWiadomosci();
	$this->dodajZasobyPoId($idPlikow);
    }

    private function ustawParametry($idWlasciciela, $typ, $idUzytkownikowDoUdostpenienia) {
	$this->wiadomosc = new \Konwersacje\Entity\Wiadomosc($this->sm);
	$this->typWatkuWiadomosci = $typ;
	$this->menagerWlasciciela = WatkiWiadomosciTypy::getMenagerWlascicielaWatku($typ, $this->sm);
	$this->wlasciciel = $this->menagerWlasciciela->getRekord($idWlasciciela);
	$this->idUzytkownikowDoUdostpenienia = $idUzytkownikowDoUdostpenienia;
    }

    private function ustawAutora($uzytkownik) {
	$this->autorWiadomosci = $uzytkownik;
    }

    private function pobierzWatekWiadomosci() {

	$this->wiadomosc->getBuilder()
		->setTematWatkuWiadomosci("Rozmowa typu {$this->typWatkuWiadomosci} ")
		->setTypWatku($this->typWatkuWiadomosci)
		->setProjektId($this->wlasciciel->id)
		->ustawUserow($this->idUzytkownikowDoUdostpenienia)
	;
	$this->wiadomosc->watek_wiadomosci = $this->wlasciciel->watek_wiadomosci;
    }

    private function zapiszWiadomosc($tresc) {

	$this->wiadomosc->tresc = $tresc;
	$this->wiadomosc->autor = $this->autorWiadomosci;
//	$wiadomosc->watek = $this->watekWiadomosci;

	$wiadomosciTable = $this->get(\Konwersacje\Model\WiadomosciMenager::class);
	$wiadomosciTable->zapisz($this->wiadomosc);

	$this->wiadomosc = $wiadomosciTable->getPoprzednioDodany();
    }

    private function dodajZasobyDoWiadomosci() {
	$zasobyTable = $this->get(\Zasoby\Menager\ZasobyUploadMenager::class);

	$zasoby = $zasobyTable->zaladujPlik();

	$this->dodajZasobyWspolne($zasoby);
    }

    private function dodajZasobyPoId($idPlikow) {
	$this->dodajZasobyWspolne($idPlikow);
    }

    private function dodajZasobyWspolne($zasoby) {
	$wiadomoscZasobMenager = $this->get(\Konwersacje\Menager\WiadomosciZasobyMenager::class);
	foreach ($zasoby as $zasob) {
	    $wiadomoscZasob = new \Konwersacje\Entity\WiadomoscZasob();
	    $wiadomoscZasob->wiadomosc = $this->wiadomosc;
	    $wiadomoscZasob->zasob = $zasob;
	    $wiadomoscZasobMenager->zapisz($wiadomoscZasob);
	}
    }

}
