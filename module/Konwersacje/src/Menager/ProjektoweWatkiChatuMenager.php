<?php

namespace Konwersacje\Menager;

use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;
use Pastmo\Wspolne\Utils\EntityUtil;

class ProjektoweWatkiChatuMenager extends BazowyMenagerBazodanowy {

    private $watkiChatuUzytkownicyMenager;

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, \Konwersacje\Module::PROJEKTOWE_WATKI_CHATU_GATEWAY);

	$this->watkiChatuUzytkownicyMenager = $this->get(ProjektoweWatkiChatuUzytkownicyMenager::class);
    }

    public function pobierzDlaWatkuWiadomosci($watekWiadomosciId) {
	return $this->pobierzZWherem("watek_wiadomosci_id=$watekWiadomosciId");
    }

    public function utworzDlaNiezapisanejWiadomosci(\Konwersacje\Entity\Wiadomosc $model) {
	if (EntityUtil::wydobadzId($model->projektowy_watek_chatu)) {
	    return $model->projektowy_watek_chatu;
	}
	if ($this->czyProjektoweWatkiChatu($model)) {
	    $watek = $model->watek;
	    $uzytkownicy = $model->getBuilder()->pobierzUserow();

	    return $this->pobierzLubUtworzDlaWatkuWiadomosciIListyUzytkownikow(EntityUtil::wydobadzId($watek),
			    $uzytkownicy);
	}
    }

    public function czyProjektoweWatkiChatu(\Konwersacje\Entity\Wiadomosc $model) {
	return
		$model->z_chatu && EntityUtil::wydobadzId($model->watek) && EntityUtil::wydobadzPole($model->watek, 'typ') === \Konwersacje\Enumy\WatkiWiadomosciTypy::PROJEKTOWY;
    }

    public function pobierzLubUtworzDlaWatkuWiadomosciIListyUzytkownikow($watekId, array $uzytkownicy) {

	$istniejacy = $this->pobierzIstniejacy($watekId, $uzytkownicy);
	if ($istniejacy) {
	    return $istniejacy;
	} else {
	    $projektowyWatekChatu = new \Konwersacje\Entity\ProjektowyWatekChatu();

	    $projektowyWatekChatu->watek_wiadomosci = $watekId;
	    $this->zapisz($projektowyWatekChatu);

	    $wynik = $this->getPoprzednioDodany();

	    $this->watkiChatuUzytkownicyMenager->dodajDoWatkuChatu($wynik, $uzytkownicy);

	    return $wynik;
	}
    }

    public function pobierzProjektoweChatoweWatkiWiadomosciUzytkownika() {
	$zalogowany = $this->getLogowanieFasada()->getZalogowanegoUsera();

	$where = " id IN (select projektowy_watek_chatu_id from projektowe_watki_chatu_uzytkownicy where uzytkownik_id={$zalogowany->id})";

	return $this->pobierzZWherem($where);
    }

    private function pobierzIstniejacy($watekId, array $uzytkownicy) {
	if (count($uzytkownicy) > 0) {

	    $zWatku = $this->pobierzZWherem("watek_wiadomosci_id=$watekId");
	} else {
	    $zWatku = array();
	}

	foreach ($zWatku as $kandydat) {

	    $count = $this->watkiChatuUzytkownicyMenager->pobierzDlaWatkuChatuIUzytkownikowCount($kandydat->id,
		    $uzytkownicy);
	    if ($count == count($uzytkownicy)) {
		return $kandydat;
	    }
	}
    }

}
