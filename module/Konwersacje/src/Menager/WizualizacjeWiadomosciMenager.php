<?php

namespace Konwersacje\Menager;

use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;

class WizualizacjeWiadomosciMenager extends BazowyMenagerBazodanowy {

    public function __construct(ServiceManager $sm) {
	$this->sm = $sm;
	$this->tableGateway = $sm->get(\Konwersacje\Module::WIZUALIZACJE_WIADOMOSCI_GATEWAY);
    }

    public function dodajKomentarzDoWizualizacji($post) {
	$projektId = $post['project_id'];
	unset($post['project_id']);

	$projektyWatkiWiadomoscMenager = $this->sm->get(\Konwersacje\Menager\ProjektyWatkiWiadomoscMenager::class);
	$wiadomosciMenager = $this->sm->get(\Konwersacje\Model\WiadomosciMenager::class);

	foreach ($post as $key => $value) {
	    $projektyWatkiWiadomoscMenager->dodajWiadomoscDoProjektu($projektId, $value);
	    $wiadomosc = $wiadomosciMenager->getPoprzednioDodany();

	    $wizualizacjaWiadomosc = new \Konwersacje\Entity\WizualizacjaWiadomosc();
	    $wizualizacjaWiadomosc->wiadomosc = $wiadomosc;
	    $wizualizacjaWiadomosc->wizualizacja = $key;

	    $this->zapisz($wizualizacjaWiadomosc);
	}
    }

    public function pobierzWizualizacjeDlaWiadomosci($wiadomoscId) {
	$wizualizacjeWiadomosci = $this->pobierzZWherem("wiadomosc_id=$wiadomoscId");
	if (count($wizualizacjeWiadomosci) > 0) {
	    return $wizualizacjeWiadomosci[0]->wizualizacja;
	}
	return null;
    }

}
