<?php

namespace Konwersacje\Menager;

use Zend\ServiceManager\ServiceManager;

class WiadomosciTagiMenager extends \Wspolne\Menager\TabelaLaczacaZTagamiMenager {

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, \Konwersacje\Module::WIADOMOSCI_TAGI_GATEWAY);
    }

    public function pobierzPoIdWiadomosci($wiadomoscId) {
	$tagi = $this->pobierzPoIdEncji($wiadomoscId);
	return $tagi;
    }

    public function aktualizujTagiDlaWiadomosci($wiadomoscId, $tagiArray) {
	$this->aktualizujTagiDlaEncji($wiadomoscId, $tagiArray);
    }

    public function dodajTagiDoWiadomosci($wiadomoscId, $tagiArray) {
	$this->dodajTagiDoEncji($wiadomoscId, $tagiArray);
    }

    protected function getKlaseEncjiMenagera() {
	return \Konwersacje\Model\WiadomosciMenager::class;
    }

    protected function kluczEncjiWTabeli() {
	return 'wiadomosc_id';
    }

    public function zapiszEncjaTag($encja, $tag) {
	$wiadomoscTag = new \Konwersacje\Entity\WiadomoscTag();
	$wiadomoscTag->wiadomosc = $encja;
	$wiadomoscTag->tag = $tag;

	$this->zapisz($wiadomoscTag);
    }

    public function getWiadomosciTagiDlaWatku($watekId) {
	$wynik = $this->pobierzZWherem("wiadomosc_id IN(
						SELECT id
						FROM wiadomosci
						WHERE watek_id =$watekId)
							");
	return $wynik;
    }

    public function usunZWiadomosciITaga($wiadomoscId, $tagId) {
	$this->usun("wiadomosc_id=$wiadomoscId AND tag_id=$tagId");
    }

}
