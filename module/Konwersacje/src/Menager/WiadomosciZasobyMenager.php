<?php

namespace Konwersacje\Menager;

use Zend\ServiceManager\ServiceManager;

class WiadomosciZasobyMenager extends \Wspolne\Menager\BazowyMenagerBazodanowy {

    public function __construct(ServiceManager $sm) {
	$this->sm = $sm;
	$this->tableGateway = $sm->get(\Konwersacje\Module::WIADOMOSCI_ZASOBY_GATEWAY);
    }

    public function pobierzDlaWiadomosci($idWiadomosci) {
	$wynik = $this->pobierzZWherem("wiadomosc_id=$idWiadomosci");
	return $wynik;
    }

}
