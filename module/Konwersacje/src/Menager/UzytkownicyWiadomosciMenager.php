<?php

namespace Konwersacje\Menager;

use \Konwersacje\Enumy\WidocznoscWiadomosci;

class UzytkownicyWiadomosciMenager extends \Wspolne\Model\WspolneEventTable {

    private $wiadomosciTable;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm, \Konwersacje\Module::UZYTKOWNIK_WIADOMOSC_GATEWAY);

	$this->wiadomosciTable = $this->get(\Konwersacje\Model\WiadomosciMenager::class);

	$this->ustawDao(\Konwersacje\Dao\UzytkownicyWiadomosciDao::class,
		\Konwersacje\Module::UZYTKOWNIK_WIADOMOSC_GATEWAY);
    }

    public function pobierzDlaWiadomosci($wiadomoscId) {
	return $this->pobierzZWherem("wiadomosc_id=$wiadomoscId");
    }

    public function pobierzDlaZalogowanego() {
	$zalogowany = $this->getLogowanieFasada()->getZalogowanegoUsera();

	return $this->pobierzZWherem("uzytkownik_id={$zalogowany->id}");
    }

    public function aktualizujWidocznoscWiadomosci($post) {
	$wiadomoscId = $post->id;
	$uzytkownicyId = $post->user_ids;
	$wiadomosc = $this->wiadomosciTable->getRekord($wiadomoscId);

	$uzytkownicyId[] = $wiadomosc->autor->id;

	$this->usun("wiadomosc_id=$wiadomoscId");

	foreach ($uzytkownicyId as $uzytkownikId) {
	    $this->dodajUzytkownikWiadomosc($uzytkownikId, $wiadomoscId);
	}
    }

    public function dodajUzytkownikWiadomosc($uzytkownikId, $wiadomoscId, $projektowyWatekChatuId = null) {
	$uzytkownikWiadomosc = new \Konwersacje\Entity\UzytkownikWiadomosc();
	$uzytkownikWiadomosc->uzytkownik = $uzytkownikId;
	$uzytkownikWiadomosc->wiadomosc = $wiadomoscId;
	$uzytkownikWiadomosc->projektowy_watek_chatu = $projektowyWatekChatuId;
	$this->zapisz($uzytkownikWiadomosc);
    }

    public function aktualizujRozwiniecieWiadomosci(array $idUzytkownikWiadomosc, $czyWidoczne) {

	$widocznosc = $czyWidoczne == '1' ? WidocznoscWiadomosci::ROZWINIETA : WidocznoscWiadomosci::ZWINIETA;

	foreach ($idUzytkownikWiadomosc as $id) {
	    $this->dao->aktualizujPojedynczaWidocznosc($id, $widocznosc);
	}
    }

}
