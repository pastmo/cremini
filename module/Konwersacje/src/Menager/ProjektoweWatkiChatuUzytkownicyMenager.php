<?php

namespace Konwersacje\Menager;

use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;

class ProjektoweWatkiChatuUzytkownicyMenager extends BazowyMenagerBazodanowy {

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, \Konwersacje\Module::PROJEKTOWE_WATKI_CHATU_UZYTKOWNICY_GATEWAY);
    }

    public function pobierzDlaWatkuChatuIUzytkownikowCount($watekChatuId, $uzytkownicy) {

	$uzytkownicyIdString = \Pastmo\Wspolne\Utils\ArrayUtil::wydobadzIdDoZapytaniaIn($uzytkownicy);

	return $this->pobierzZWheremCount($this->whereDlaWatkuChatu($watekChatuId) . " AND uzytkownik_id IN($uzytkownicyIdString)");
    }

    public function pobierzDlaWatkuChatu($watekChatuId) {
	return $this->pobierzZWherem($this->whereDlaWatkuChatu($watekChatuId));
    }

    public function dodajDoWatkuChatu($watekChatu, array $uzytkownicy) {

	foreach ($uzytkownicy as $uzytkownik) {
	    $watekChatuUzytkownik = \Konwersacje\Entity\ProjektowyWatekChatuUzytkownik::create();
	    $watekChatuUzytkownik->projektowy_watek_chatu = $watekChatu;
	    $watekChatuUzytkownik->uzytkownik = $uzytkownik;

	    $this->zapisz($watekChatuUzytkownik);
	}
    }

    private function whereDlaWatkuChatu($watekChatuId) {
	return "projektowy_watek_chatu_id=$watekChatuId";
    }

}
