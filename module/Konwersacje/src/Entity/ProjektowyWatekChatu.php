<?php

namespace Konwersacje\Entity;

use Logowanie\Model\Uzytkownik;
use Konwersacje\Entity\WatekWiadomosci;

class ProjektowyWatekChatu extends \Wspolne\Model\WspolneModel {

    public $id;
    public $watek_wiadomosci;

    public function __construct($sm = null) {
	parent::__construct($sm);
    }

    public function dowiazListyTabelObcych() {

    }

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);

	$this->data = $data;
	if ($this->czyTabeleDostepne()) {
	    $this->watek_wiadomosci = $this->pobierzTabeleObca('watek_wiadomosci_id',
		    \Konwersacje\Module::WATKI_WIADOMOSCI_TABLE, new WatekWiadomosci());
	}
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'watek_wiadomosci':
		return 'watek_wiadomosci_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public static function create() {
	$wynik = new ProjektowyWatekChatu();
	$wynik->watek_wiadomosci = WatekWiadomosci::create();
	return $wynik;
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public static function getClass() {
	return __CLASS__;
    }

}
