<?php

namespace Konwersacje\Entity;

class WatekWiadomosci extends \Wspolne\Model\WspolneModel {

    public $id;
    public $temat;
    public $typ;
    private $uzytkownicyWatkiWiadomosci = array();
    private $wiadomosci = array();
    private $najnowszaWiadomosc;

    public function __construct($sm = null) {
	parent::__construct($sm);
	$this->najnowszaWiadomosc = Wiadomosc::create();
    }

    public function dowiazListyTabelObcych() {
	if ($this->czyTabeleDostepne()) {
	    $uzytkownicyWatkiWiadomosciTable = $this->sm->get(\Konwersacje\Module::UZYTKOWNIK_WATKEK_WIADOMOSCI_TABLE);
	    $wiadomosciTable = $this->sm->get(\Konwersacje\Model\WiadomosciMenager::class);

	    $this->uzytkownicyWatkiWiadomosci = $uzytkownicyWatkiWiadomosciTable->pobierzDlaWatku($this->id);
	    $this->wiadomosci = $wiadomosciTable->pobierzWiadomosciDlaWatku($this->id);

	    if (count($this->wiadomosci) > 0) {
		$this->najnowszaWiadomosc = $this->wiadomosci[count($this->wiadomosci) - 1];
	    }
	}
    }

    public function getuzytkownicyWatkiWiadomosci() {
	$this->dowiazListyTabelObcych();

	return $this->uzytkownicyWatkiWiadomosci;
    }

    public function getUzytkownicyBezZalogowanego() {
	$this->getuzytkownicyWatkiWiadomosci();
	$zalogowany = $this->getZalogowanego();

	return \Pastmo\Wspolne\Utils\ArrayUtil::getUzytkownicyBezZalogowanego($this->uzytkownicyWatkiWiadomosci,
			$zalogowany);
    }

    public function getUzytkownicyAvataryBezZalogowanego() {
	$this->getuzytkownicyWatkiWiadomosci();
	$uzytkownikTable = $this->sm->get(\Logowanie\Module::UZYTKOWNIK_TABLE);
	$zalogowany = $uzytkownikTable->getZalogowanegoUsera();

	$wynik = array();

	foreach ($this->uzytkownicyWatkiWiadomosci as $uzytkownikWatek) {

	    if ($uzytkownikWatek->uzytkownik->id != $zalogowany->id) {
		$wynik[] = $uzytkownikWatek->uzytkownik->avatar->wyswietl();
	    }
	}

	return $wynik;
    }

    public function getWiadomosci() {
	$this->dowiazListyTabelObcych();

	return $this->wiadomosci;
    }

    public function pobierzNajnowszaWiadomosc() {
	$this->dowiazListyTabelObcych();

	return $this->najnowszaWiadomosc;
    }

    public function pobierzNazweWlasciciela() {
	if ($this->czyTabeleDostepne()) {
	    $watkiTable = $this->sm->get(\Konwersacje\Model\WatkiWiadomosciTable::class);
	    return $watkiTable->pobierzNazweWlasciciela($this->id);
	}
    }

    public function pobierzWlasciciela() {
	if ($this->czyTabeleDostepne()) {
	    $watkiTable = $this->sm->get(\Konwersacje\Model\WatkiWiadomosciTable::class);
	    return $watkiTable->pobierzWlasciciela($this->id);
	}
    }

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->temat = $this->pobierzLubNull($data, 'temat');
	$this->typ = $this->pobierzLubNull($data, 'typ');
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	return $nazwaWKodzie;
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public static function getClass() {
	return __CLASS__;
    }

    public static function create() {
	$watekWiadomosci = new WatekWiadomosci();


	return $watekWiadomosci;
    }

}
