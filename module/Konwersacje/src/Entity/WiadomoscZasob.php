<?php

namespace Konwersacje\Entity;

class WiadomoscZasob extends \Wspolne\Model\WspolneModel {

    public $id;
    public $wiadomosc;
    public $zasob;

    public function __construct($sm = null) {
	parent::__construct($sm);
    }

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);
	if ($this->czyTabeleDostepne()) {
	    $this->data = $data;
	    $this->wiadomosc = $this->pobierzTabeleObca('wiadomosc_id', \Konwersacje\Model\WiadomosciMenager::class,
		    new Wiadomosc());
	    $this->zasob = $this->pobierzTabeleObca('zasob_id', \Zasoby\Menager\ZasobyUploadMenager::class,
		    new \Zasoby\Model\Zasob());
	}
    }

    public function dowiazListyTabelObcych() {

    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'zasob':
		return 'zasob_id';
	    case 'wiadomosc':
		return 'wiadomosc_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
