<?php

namespace Konwersacje\Entity;

use Pastmo\Wspolne\Utils\ArrayUtil;
use Pastmo\Wspolne\Utils\EntityUtil;

class WiadomoscBuilder {

    private $userzy = array();
    private $projektId;
    private $temat = "";
    private $typWatku = \Konwersacje\Enumy\WatkiWiadomosciTypy::PROJEKTOWY;

    public function dodajUsera($user) {
	if (!ArrayUtil::inEntityArray($user, $this->userzy) && EntityUtil::czyEncjaZId($user)) {
	    $this->userzy[] = $user;
	}
    }

    public function ustawUserow(array $userzy) {
	$this->userzy = $userzy;
    }

    public function getProjektId() {
	return $this->projektId;
    }

    public function getTematWatkuWiadomosci() {
	return $this->temat;
    }

    public function getTypWatku() {
	return $this->typWatku;
    }

    public function setProjektId($projektId) {
	$this->projektId = $projektId;
	return $this;
    }

    public function setTematWatkuWiadomosci($temat) {
	$this->temat = $temat;
	return $this;
    }

    public function setTypWatku($typWatku) {
	$this->typWatku = $typWatku;
	return $this;
    }

    public function pobierzUserow() {
	return $this->userzy;
    }

}
