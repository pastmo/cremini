<?php

namespace Konwersacje\Entity;

class UzytkownikWiadomosc extends \Wspolne\Model\WspolneModel {

    public $id;
    public $uzytkownik;
    public $wiadomosc;
    public $projektowy_watek_chatu;
    public $widocznosc;

    public function dowiazListyTabelObcych() {

    }

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->widocznosc = $this->pobierzLubNull($data, 'widocznosc');
	if ($this->czyTabeleDostepne()) {
	    $this->data = $data;
	    $this->uzytkownik = $this->pobierzTabeleObca('uzytkownik_id', \Logowanie\Menager\UzytkownicyMenager::class,
		    \Logowanie\Model\Uzytkownik::create());
	    $this->wiadomosc = $this->pobierzTabeleObca('wiadomosc_id', \Konwersacje\Model\WiadomosciMenager::class,
		    new Wiadomosc());
	    $this->projektowy_watek_chatu = $this->pobierzTabeleObca('projektowy_watek_chatu_id',
		    \Konwersacje\Menager\ProjektoweWatkiChatuMenager::class, ProjektowyWatekChatu::create());
	}
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'uzytkownik':
		return 'uzytkownik_id';
	    case 'wiadomosc':
		return 'wiadomosc_id';
	    case 'projektowy_watek_chatu':
		return 'projektowy_watek_chatu_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public static function create(){
	$wynik= new UzytkownikWiadomosc();
	$wynik->widocznosc=  \Konwersacje\Enumy\WidocznoscWiadomosci::ROZWINIETA;
	return $wynik;
    }

}
