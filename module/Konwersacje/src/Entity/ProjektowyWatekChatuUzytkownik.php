<?php

namespace Konwersacje\Entity;

use Logowanie\Model\Uzytkownik;
use Konwersacje\Entity\WatekWiadomosci;

class ProjektowyWatekChatuUzytkownik extends \Wspolne\Model\WspolneModel {

    public $id;
    public $projektowy_watek_chatu;
    public $uzytkownik;

    public function __construct($sm = null) {
	parent::__construct($sm);
    }

    public function dowiazListyTabelObcych() {

    }

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);

	$this->data = $data;
	if ($this->czyTabeleDostepne()) {
	    $this->uzytkownik = $this->pobierzTabeleObca('uzytkownik_id', \Logowanie\Menager\UzytkownicyMenager::class,
		    Uzytkownik::create());
	    $this->projektowy_watek_chatu = $this->pobierzTabeleObca('projektowy_watek_chatu_id',
		    \Konwersacje\Menager\ProjektoweWatkiChatuMenager::class, new ProjektowyWatekChatu());
	}
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'projektowy_watek_chatu':
		return 'projektowy_watek_chatu_id';
	    case 'uzytkownik':
		return 'uzytkownik_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public static function create() {
	$wynik = new ProjektowyWatekChatuUzytkownik();
	$wynik->projektowy_watek_chatu = ProjektowyWatekChatu::create();
	$wynik->uzytkownik = Uzytkownik::create();
	return $wynik;
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public static function getClass() {
	return __CLASS__;
    }

}
