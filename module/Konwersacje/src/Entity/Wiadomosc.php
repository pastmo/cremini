<?php

namespace Konwersacje\Entity;

use Logowanie\Model\Uzytkownik;
use Pastmo\Wspolne\Utils\EntityUtil;
use \Pastmo\Wspolne\Utils\StrUtil;

class Wiadomosc extends \Wspolne\Model\WspolneModel {

    public $id;
    public $tresc;
    public $autor;
    public $watek;
    public $z_chatu;
    public $data_dodania;
    public $projektowy_watek_chatu;
    private $wizualizacje;
    private $wiadomosciZasoby;
    public $uzytkownicyWiadomosci;
    private $projekt;
    private $builder;

    public function __construct($sm = null) {
	parent::__construct($sm);

	$this->builder = new WiadomoscBuilder();
    }

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);
	$this->tresc = $this->pobierzLubNull($data, 'tresc');
	$this->z_chatu = $this->pobierzLubNull($data, 'z_chatu', false);
	$this->data_dodania = $this->pobierzLubNull($data, 'data_dodania');

	if (!$this->tresc) {

	    $this->tresc = $this->pobierzLubNull($data, 'editor_textarea');
	}

	if ($this->czyTabeleDostepne()) {
	    $this->data = $data;
	    $this->autor = $this->pobierzTabeleObca('autor_id', \Logowanie\Module::UZYTKOWNIK_TABLE, new Uzytkownik());
	    $this->watek = $this->pobierzTabeleObca('watek_id', \Konwersacje\Module::WATKI_WIADOMOSCI_TABLE, null);
	}
    }

    public function pobierzWizualizacje() {
	$this->dowiazListyTabelObcych();
	return $this->wizualizacje;
    }

    public function pobierzWiadomosciZasoby() {
	$this->dowiazListyTabelObcych();
	return $this->wiadomosciZasoby;
    }

    public function getWiadomosciTagi() {
	if ($this->wiadosciTagi === null) {
	    $this->dowiazListyTabelObcych();
	}
	return $this->wiadosciTagi;
    }

    public function getProjekt() {
	$this->dowiazListyTabelObcych();

	return $this->projekt;
    }

    public function getNazwyTagow() {
	$tagiWiadomosci = $this->getWiadomosciTagi();

	$wynik = array();

	foreach ($tagiWiadomosci as $tagWiadomosc) {
	    $wynik[] = $tagWiadomosc->tag->nazwa;
	}

	$toReturn = json_encode($wynik);

	return str_replace('"', "'", $toReturn);
    }

    public function getUzytkownicyBezZalogowanego() {
	$zalogowany = $this->getZalogowanego();
	$this->getUzytkownicyWiadomosci();

	return \Pastmo\Wspolne\Utils\ArrayUtil::getUzytkownicyBezZalogowanego($this->uzytkownicyWiadomosci,
			$zalogowany);
    }

    public function getTextMessage() {
	$tresc = $this->tresc;

	$tresc = \Pastmo\Wspolne\Utils\DomUtil::konwertujNaWiadomoscTekstowa($tresc);

	return $tresc;
    }

    public function dowiazListyTabelObcych() {
	if ($this->czyTabeleDostepne()) {
	    $wizualizacjeWiadomosciMenager = $this->sm->get(\Konwersacje\Menager\WizualizacjeWiadomosciMenager::class);
	    $wiadomosciZasobyMenager = $this->sm->get(\Konwersacje\Menager\WiadomosciZasobyMenager::class);
//	    $wiadomosciTagiMenager = $this->sm->get(\Konwersacje\Menager\WiadomosciTagiMenager::class);

	    $this->wizualizacje = $wizualizacjeWiadomosciMenager->pobierzWizualizacjeDlaWiadomosci($this->id);
	    $this->wiadomosciZasoby = $wiadomosciZasobyMenager->pobierzDlaWiadomosci($this->id);
//	    $this->wiadosciTagi = $wiadomosciTagiMenager->pobierzPoIdWiadomosci($this->id);
	} else {
	    $this->wizualizacje = array();
	    $this->wiadomosciZasoby = array();
	    $this->wiadosciTagi = array();
	}
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'autor':
		return 'autor_id';
	    case 'watek':
		return 'watek_id';
	    case 'projektowy_watek_chatu':
		return 'projektowy_watek_chatu_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public static function getClass() {
	return __CLASS__;
    }

    public function kiedyDodano() {
	return $this->obliczKiedyDodano($this->data_dodania);
    }

    function obliczKiedyDodano($ts) {
	if (!ctype_digit($ts))
	    $ts = strtotime($ts);

	$diff = time() - $ts;
	if ($diff == 0)
	    return 'now';
	elseif ($diff > 0) {
	    $day_diff = floor($diff / 86400);
	    if ($day_diff == 0) {
		if ($diff < 60)
		    return 'just now';
		if ($diff < 120)
		    return '1 minute ago';
		if ($diff < 3600)
		    return floor($diff / 60) . ' minutes ago';
		if ($diff < 7200)
		    return '1 hour ago';
		if ($diff < 86400)
		    return floor($diff / 3600) . ' hours ago';
	    }
	    if ($day_diff == 1)
		return 'Yesterday';
	    if ($day_diff < 7)
		return $day_diff . ' days ago';
	    if ($day_diff < 31)
		return ceil($day_diff / 7) . ' weeks ago';
	    if ($day_diff < 60)
		return 'last month';
	    return date('F Y', $ts);
	}
	else {
	    $diff = abs($diff);
	    $day_diff = floor($diff / 86400);
	    if ($day_diff == 0) {
		if ($diff < 120)
		    return 'in a minute';
		if ($diff < 3600)
		    return 'in ' . floor($diff / 60) . ' minutes';
		if ($diff < 7200)
		    return 'in an hour';
		if ($diff < 86400)
		    return 'in ' . floor($diff / 3600) . ' hours';
	    }
	    if ($day_diff == 1)
		return 'Tomorrow';
	    if ($day_diff < 4)
		return date('l', $ts);
	    if ($day_diff < 7 + (7 - date('w')))
		return 'next week';
	    if (ceil($day_diff / 7) < 4)
		return 'in ' . ceil($day_diff / 7) . ' weeks';
	    if (date('n', $ts) == date('n') + 1)
		return 'next month';
	    return date('F Y', $ts);
	}
    }

    public function czyZalogowanyAutorem() {

	$zalogowany = $this->getZalogowanego();
	if ($zalogowany) {
	    $czyZalogowanyAutorem = $zalogowany->id == EntityUtil::wydobadzId($this->autor);
	    return $czyZalogowanyAutorem;
	}
	return false;
    }

    public function getUzytkownicyWiadomosci() {
	$this->pobierzListeZTabeliObcej(\Konwersacje\Menager\UzytkownicyWiadomosciMenager::class,
		'pobierzDlaWiadomosci', $this->id, null, 'uzytkownicyWiadomosci');

	return $this->uzytkownicyWiadomosci;
    }

    public function getUzytkownikWiadomoscZalogowanego() {
	$this->getUzytkownicyWiadomosci();

	$zalogowany = $this->getZalogowanego();

	$wynik = \Pastmo\Wspolne\Utils\ArrayUtil::getEncjeZalogowanego($this->uzytkownicyWiadomosci, $zalogowany,
			UzytkownikWiadomosc::create());

	return \Pastmo\Wspolne\Utils\ArrayUtil::pobierzPierwszyElement($wynik);
    }

    public function pobierzIdUzytkownikow() {
	$this->dowiazListyTabelObcych();
	$wynik = array();

	foreach ($this->uzytkownicyWiadomosci as $uzytkownikWiadomosc) {
	    $wynik[] = $uzytkownikWiadomosc->uzytkownik->id;
	}

	return $wynik;
    }

    public function pobierzUzytkownikowBezAutora() {
	$this->getUzytkownicyWiadomosci();
	$wynik = array();

	foreach ($this->uzytkownicyWiadomosci as $uzytkownikWiadomosc) {
	    if ($uzytkownikWiadomosc->uzytkownik->id === $this->autor->id) {
		continue;
	    }
	    $wynik[] = $uzytkownikWiadomosc->uzytkownik;
	}

	return $wynik;
    }

    public function getBuilder() {
	return $this->builder;
    }

    public function dodatkowoIgnorowane() {
	return array('uzytkownicyWiadomosci');
    }

    public static function create() {
	$wynik = new Wiadomosc();
	$wynik->autor = Uzytkownik::create();

	return $wynik;
    }

}
