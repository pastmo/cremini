<?php

namespace Konwersacje\Entity;

class WiadomoscTag extends \Wspolne\Model\WspolneModel {

    public $id;
    public $wiadomosc;
    public $tag;

    public function dowiazListyTabelObcych() {

    }

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);
	if ($this->czyTabeleDostepne()) {
	    $this->data = $data;
	    $this->wiadomosc = $this->pobierzTabeleObca('wiadomosc_id', \Konwersacje\Model\WiadomosciMenager::class,
		    new Wiadomosc());
//	    $this->tag = $this->pobierzTabeleObca('tag_id', \Projekty\Menager\TagiMenager::class,
//		    new \Projekty\Entity\Tag());
	}
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'wiadomosc':
		return 'wiadomosc_id';
	    case 'tag':
		return 'tag_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
