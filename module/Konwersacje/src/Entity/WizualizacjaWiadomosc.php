<?php

namespace Konwersacje\Entity;

class WizualizacjaWiadomosc extends \Wspolne\Model\WspolneModel {

    public $id;
    public $wizualizacja;
    public $wiadomosc;

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);

	if ($this->czyTabeleDostepne()) {
	    $this->data = $data;
	    $this->wiadomosc = $this->pobierzTabeleObca('wiadomosc_id', \Konwersacje\Model\WiadomosciMenager::class,
		    new Wiadomosc());
	    $this->wizualizacja = $this->pobierzTabeleObca('projekt_produkt_wariacja_wizualizacja_id',
		    \Projekty\Menager\ProjektyProduktyWariacjeWizualizacjeMenager::class,
		    new \Projekty\Entity\ProjektProduktWariacjaWizualizacja());
	}
    }

    public function dowiazListyTabelObcych() {

    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'wizualizacja':
		return 'projekt_produkt_wariacja_wizualizacja_id';
	    case 'wiadomosc':
		return 'wiadomosc_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
