<?php

namespace Konwersacje\Entity;

use Logowanie\Model\Uzytkownik;
use Konwersacje\Entity\WatekWiadomosci;

class UzytkownikWatkekWiadomosci extends \Wspolne\Model\WspolneModel {

    public $id;
    public $uzytkownik;
    public $watek_wiadomosci;

    public function __construct($sm = null) {
	parent::__construct($sm);
    }

    public function dowiazListyTabelObcych() {

    }

    public function exchangeArray($data) {
	$this->id = $this->pobierzLubNull($data, self::ID);

	$this->data = $data;
	if ($this->czyTabeleDostepne()) {
	    $this->uzytkownik = $this->pobierzTabeleObca('uzytkownik_id', \Logowanie\Module::UZYTKOWNIK_TABLE,
		    new Uzytkownik());
	    $this->watek_wiadomosci = $this->pobierzTabeleObca('watek_wiadomosci_id',
		    \Konwersacje\Module::WATKI_WIADOMOSCI_TABLE, new WatekWiadomosci());
	}
    }

    public function konwertujNaKolumneDB($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'uzytkownik':
		return 'uzytkownik_id';
	    case 'watek_wiadomosci':
		return 'watek_wiadomosci_id';
	    default :
		return $nazwaWKodzie;
	}
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

    public static function getClass() {
	return __CLASS__;
    }

}
