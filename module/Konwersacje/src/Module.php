<?php

namespace Konwersacje;

use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Mvc\MvcEvent;
use Konwersacje\Entity\Wiadomosc;
use Konwersacje\Model\WiadomosciMenager;
use Konwersacje\Entity\WatekWiadomosci;
use Konwersacje\Model\WatkiWiadomosciTable;
use Konwersacje\Entity\UzytkownikWatkekWiadomosci;
use Konwersacje\Model\UzytkownikWatkekWiadomosciTable;
use Konwersacje\Fasada\KonwersacjeFasada;

class Module implements ConfigProviderInterface {

    const WIADOMOSCI_GATEWAY = "WiadomosciGateway";
    const WATKI_WIADOMOSCI_TABLE = "Konwersacje\Model\WatkiWiadomosciTable";
    const WATKI_WIADOMOSCI_GATEWAY = "WatkiWiadomosciGateway";
    const UZYTKOWNIK_WATKEK_WIADOMOSCI_TABLE = \Konwersacje\Model\UzytkownikWatkekWiadomosciTable::class;
    const UZYTKOWNIK_WATKEK_WIADOMOSCI_GATEWAY = "UzytkownikWatkekWiadomosciGateway";
    const UZYTKOWNIK_WIADOMOSC_GATEWAY = "UzytkownikWiadomoscGateway";
    const PROJEKT_WATKEK_WIADOMOSCI_GATEWAY = "ProjektWatkekWiadomosciGateway";
    const WIZUALIZACJE_WIADOMOSCI_GATEWAY = "WizualizacjeWiadomosciGateway";
    const WIADOMOSCI_ZASOBY_GATEWAY = "WiadomosciZasobyGateway";
    const WIADOMOSCI_TAGI_GATEWAY = "WiadomosciTagiGateway";
    const PROJEKTOWE_WATKI_CHATU_GATEWAY = "projektowe_watki_chatu_gateway";
    const PROJEKTOWE_WATKI_CHATU_UZYTKOWNICY_GATEWAY = "projektowe_watki_chatu_uzytkownicy_gateway";

    public function onBootstrap(MvcEvent $e) {

    }

    public function getConfig() {
	return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig() {
	return array(
		'factories' => array(
			KonwersacjeFasada::class => function($sm) {

			    $table = new KonwersacjeFasada($sm);
			    return $table;
			},
			\Konwersacje\Model\WiadomosciMenager::class => function($sm) {
			    $table = new WiadomosciMenager($sm);
			    return $table;
			},
			\Konwersacje\Menager\WiadomosciZWlascicielemMenager::class => function($sm) {
			    $table = new Menager\WiadomosciZWlascicielemMenager($sm);
			    return $table;
			},
			self::WIADOMOSCI_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new Wiadomosc($sm));
			    return new TableGateway('wiadomosci', $dbAdapter, null, $resultSetPrototype);
			},
			self::WATKI_WIADOMOSCI_TABLE => function($sm) {

			    $table = new WatkiWiadomosciTable($sm);
			    return $table;
			},
			self::WATKI_WIADOMOSCI_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new WatekWiadomosci($sm));
			    return new TableGateway('watki_wiadomosci', $dbAdapter, null, $resultSetPrototype);
			},
			self::UZYTKOWNIK_WATKEK_WIADOMOSCI_TABLE => function($sm) {

			    $table = new UzytkownikWatkekWiadomosciTable($sm);
			    return $table;
			},
//			Menager\ProjektyWatkiWiadomoscMenager::class => function($sm) {
//
//			    $table = new Menager\ProjektyWatkiWiadomoscMenager($sm);
//			    return $table;
//			},
			Menager\WizualizacjeWiadomosciMenager::class => function($sm) {

			    $table = new Menager\WizualizacjeWiadomosciMenager($sm);
			    return $table;
			},
			Menager\WiadomosciZasobyMenager::class => function($sm) {

			    $table = new Menager\WiadomosciZasobyMenager($sm);
			    return $table;
			},
//			Menager\WiadomosciTagiMenager::class => function($sm) {
//
//			    $table = new Menager\WiadomosciTagiMenager($sm);
//			    return $table;
//			},
			Menager\UzytkownicyWiadomosciMenager::class => function($sm) {

			    $table = new Menager\UzytkownicyWiadomosciMenager($sm);
			    return $table;
			},
			Menager\ProjektoweWatkiChatuMenager::class => function($sm) {

			    $table = new Menager\ProjektoweWatkiChatuMenager($sm);
			    return $table;
			},
			Menager\ProjektoweWatkiChatuUzytkownicyMenager::class => function($sm) {

			    $table = new Menager\ProjektoweWatkiChatuUzytkownicyMenager($sm);
			    return $table;
			},
			self::UZYTKOWNIK_WATKEK_WIADOMOSCI_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new UzytkownikWatkekWiadomosci($sm));
			    return new TableGateway('uzytkownicy_watki_wiadomosci', $dbAdapter, null, $resultSetPrototype);
			},
			self::PROJEKT_WATKEK_WIADOMOSCI_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new Entity\ProjektWatkekWiadomosci($sm));
			    return new TableGateway('projekty_watki_wiadomosci', $dbAdapter, null, $resultSetPrototype);
			},
			self::WIZUALIZACJE_WIADOMOSCI_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new Entity\WizualizacjaWiadomosc($sm));
			    return new TableGateway('wizualizacje_wiadomosci', $dbAdapter, null, $resultSetPrototype);
			},
			self::WIADOMOSCI_ZASOBY_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new Entity\WiadomoscZasob($sm));
			    return new TableGateway('wiadomosci_zasoby', $dbAdapter, null, $resultSetPrototype);
			},
			self::WIADOMOSCI_TAGI_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new Entity\WiadomoscTag($sm));
			    return new TableGateway('wiadomosci_tagi', $dbAdapter, null, $resultSetPrototype);
			},
			self::UZYTKOWNIK_WIADOMOSC_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new Entity\UzytkownikWiadomosc($sm));
			    return new TableGateway('uzytkownicy_wiadomosci', $dbAdapter, null, $resultSetPrototype);
			},
			self::PROJEKTOWE_WATKI_CHATU_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new Entity\ProjektowyWatekChatu($sm));
			    return new TableGateway('projektowe_watki_chatu', $dbAdapter, null, $resultSetPrototype);
			},
			self::PROJEKTOWE_WATKI_CHATU_UZYTKOWNICY_GATEWAY => function ($sm) {
			    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			    $resultSetPrototype = new ResultSet();
			    $resultSetPrototype->setArrayObjectPrototype(new Entity\ProjektowyWatekChatuUzytkownik($sm));
			    return new TableGateway('projektowe_watki_chatu_uzytkownicy', $dbAdapter, null, $resultSetPrototype);
			},
		),
	);
    }

}
