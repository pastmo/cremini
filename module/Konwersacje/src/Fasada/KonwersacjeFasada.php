<?php

namespace Konwersacje\Fasada;

use Zend\ServiceManager\ServiceManager;

class KonwersacjeFasada extends \Wspolne\Fasada\WspolneFasada {

    private $projektyWatkiWiadomoscMenager;
    private $wiadomosciZWlascicielemMenager;
    private $wizualizacjeWiadomosciMenager;
    private $wiadomosciTable;
    protected $tagiMenager;
    protected $wiadomosciTagiMenager;
    protected $uzytkownicyWiadomosciMenager;

    public function __construct(\Zend\ServiceManager\ServiceManager $serviceMenager) {
	parent::__construct($serviceMenager);

//	$this->projektyWatkiWiadomoscMenager = $this->serviceMenager->get(\Konwersacje\Menager\ProjektyWatkiWiadomoscMenager::class);
	$this->wiadomosciZWlascicielemMenager = $this->serviceMenager->get(\Konwersacje\Menager\WiadomosciZWlascicielemMenager::class);
	$this->wizualizacjeWiadomosciMenager = $this->serviceMenager->get(\Konwersacje\Menager\WizualizacjeWiadomosciMenager::class);
	$this->wiadomosciTable = $this->serviceMenager->get(\Konwersacje\Model\WiadomosciMenager::class);
//	$this->tagiMenager = $this->serviceMenager->get(\Projekty\Menager\TagiMenager::class);
//	$this->wiadomosciTagiMenager = $this->serviceMenager->get(\Konwersacje\Menager\WiadomosciTagiMenager::class);
	$this->uzytkownicyWiadomosciMenager = $this->serviceMenager->get(\Konwersacje\Menager\UzytkownicyWiadomosciMenager::class);
    }

    public function dodajWiadomoscDoWlascicielaZPosta($post) {
	$this->wiadomosciZWlascicielemMenager->dodajWiadomoscDoWlascicielaZPosta($post);
    }

    public function dodajWiadomoscSystemowa($idWlasciciela, $tresc, $typ,
	    $idUzytkownikowDoUdostpenienia = array(), $idPlikow = array()) {

	$this->wiadomosciZWlascicielemMenager->dodajWiadomoscSystemowa($idWlasciciela, $tresc, $typ,
		$idUzytkownikowDoUdostpenienia, $idPlikow);
    }

    public function dodajWiadomoscDoProjektu($idProjektu, $tresc, $uzytkownik = null) {
//	$this->projektyWatkiWiadomoscMenager->dodajWiadomoscDoProjektu($idProjektu, $tresc, $uzytkownik);
    }

    public function pobierzOstatniaWiadomosc() {
	return $this->wiadomosciTable->getPoprzednioDodany();
    }

    public function dodajKomentarzDoWizualizacji($post) {
	$this->wizualizacjeWiadomosciMenager->dodajKomentarzDoWizualizacji($post);
    }

    /**
     *
     * @return array(String) Tablica tagów
     */
    public function wyszukajTagi($fragmentTagaDowyszukania) {
	return $this->tagiMenager->wyszukajNazwyTagowWiadomosci($fragmentTagaDowyszukania);
    }

    public function dodajTagiDoWiadomosci($wiadomoscId, $tagiArray) {
	return $this->wiadomosciTagiMenager->dodajTagiDoWiadomosci($wiadomoscId, $tagiArray);
    }

    public function getWiadomosciTagiDlaWatku($projektId) {
	return $this->wiadomosciTagiMenager->getWiadomosciTagiDlaWatku($projektId);
    }

    public function usunZWiadomosciITaga($wiadomoscId, $tagId) {
	$this->wiadomosciTagiMenager->usunZWiadomosciITaga($wiadomoscId, $tagId);
    }

    public function aktualizujWidocznoscWiadomosci($post) {
	$this->uzytkownicyWiadomosciMenager->aktualizujWidocznoscWiadomosci($post);
    }

    public function pobierzUzytkownicyWiadomosciDlaZalogowanego() {
	$this->uzytkownicyWiadomosciMenager->pobierzDlaZalogowanego();
    }

    public function aktualizujRozwiniecieWiadomosci(array $idUzytkownikWiadomosc, $czyZwiniete) {
	$this->uzytkownicyWiadomosciMenager->aktualizujRozwiniecieWiadomosci($idUzytkownikWiadomosc, $czyZwiniete);
    }

}
