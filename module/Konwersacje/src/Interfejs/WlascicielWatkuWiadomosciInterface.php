<?php

namespace Konwersacje\Interfejs;

interface WlascicielWatkuWiadomosciInterface {

    public function pobierzIdWatku($encja);

    public function getPoleTworcy();

    public function polaczZWatkiemWiadomosci($id, $watekId);

    public function getPoWatekId($watekId);
}
