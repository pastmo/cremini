<?php

namespace Konwersacje\Dao;

class UzytkownicyWiadomosciDao extends \Pastmo\Wspolne\Dao\WspolnyDao {

    public function aktualizujPojedynczaWidocznosc($id, $widocznosc) {
	$this->update($id, ['widocznosc' => $widocznosc]);
    }

}
