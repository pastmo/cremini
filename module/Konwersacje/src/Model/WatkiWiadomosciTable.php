<?php

namespace Konwersacje\Model;

use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;
use Wspolne\Model\WspolneModel;
use \Konwersacje\Entity\WatekWiadomosci;

class WatkiWiadomosciTable extends BazowyMenagerBazodanowy {

    protected $dbAdapter;

    const WHERE_UZYTKOWNICY_WATKI = "id IN (select watek_wiadomosci_id from uzytkownicy_watki_wiadomosci where uzytkownik_id=%s)";

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, \Konwersacje\Module::WATKI_WIADOMOSCI_GATEWAY);
    }

    public function pobierzNiepusteDlaUzytkownika($userId) {
	$sql = "SELECT DISTINCT ww.*
				FROM uzytkownicy_watki_wiadomosci uww
				LEFT JOIN watki_wiadomosci ww ON ww.id=uww.watek_wiadomosci_id
				JOIN wiadomosci wia ON wia.watek_id=ww.id
				WHERE uww.uzytkownik_id=$userId
				ORDER BY wia.data_dodania DESC;";
	$statement = $this->dbAdapter->query($sql);

	$results = $statement->execute();

	$toReturn = array();

	foreach ($results as $result) {
	    $watek = new WatekWiadomosci($this->sm);
	    $watek->exchangeArray($result);
	    $toReturn[] = $watek;
	}

	return $toReturn;
    }

    public function pobierzPrywatnyWatekWiadomosciMiedzyZalogowanymAUzytkownikiem($uzytkownikId) {
	$watki = $this->pobierzPrywatneWatkiWiadomosciMiedzyZalogowanymAUzytkownikiem($uzytkownikId);

	if (count($watki) > 0) {
	    return $watki[0]; //TODO: Obsłużyć przypadek kiedy jest więcej wątków niż jeden
	} else {
	    return WatekWiadomosci::create();
	}
    }

    public function pobierzPrywatneWatkiWiadomosciMiedzyZalogowanymAUzytkownikiem($uzytkownikId) {
	$zalogowany = $this->getLogowanieFasada()->getZalogowanegoUsera();

	$where = $this->whereUzytkownicyWatki($uzytkownikId)
		. " AND "
		. $this->whereUzytkownicyWatki($zalogowany->id)
		. "AND typ='" . \Konwersacje\Enumy\WatkiWiadomosciTypy::PRYWATNY . "'";

	return $this->pobierzZWherem($where);
    }

    public function pobierzNazweWlasciciela($watekId) {
	$wlasciciel = $this->pobierzWlasciciela($watekId);
	return $wlasciciel->nazwa . '';
    }

    public function pobierzWlasciciela($watekId) {
	$watek = $this->getRekord($watekId);
	$menagerWlasciciela = \Konwersacje\Enumy\WatkiWiadomosciTypy::getMenagerWlascicielaWatku($watek->typ,
			$this->sm);

	$wlasciciel = $menagerWlasciciela->getPoWatekId($watekId);
	return $wlasciciel;
    }

    private function whereUzytkownicyWatki($uzytkownikId) {
	return sprintf(self::WHERE_UZYTKOWNICY_WATKI, $uzytkownikId);
    }

}
