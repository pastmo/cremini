<?php

namespace Konwersacje\Model;

use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;

class UzytkownikWatkekWiadomosciTable extends BazowyMenagerBazodanowy {

    public function __construct(ServiceManager $sm) {
	$this->sm = $sm;
	$this->tableGateway = $sm->get(\Konwersacje\Module::UZYTKOWNIK_WATKEK_WIADOMOSCI_GATEWAY);
    }

    public function pobierzDlaWatku($watekId) {
	return $this->pobierzZWherem("watek_wiadomosci_id=$watekId");
    }

}
