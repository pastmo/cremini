<?php

namespace Konwersacje\Model;

use Zend\ServiceManager\ServiceManager;
use Konwersacje\Entity\WatekWiadomosci;
use Konwersacje\Entity\UzytkownikWatkekWiadomosci;
use \Konwersacje\Entity\Wiadomosc;
use \Pastmo\Wspolne\Entity\WspolnyModel;
use \Pastmo\Wspolne\Utils\EntityUtil;
use \Konwersacje\Interfejs\WlascicielWatkuWiadomosciInterface;
use \Pastmo\Wspolne\Utils\ArrayUtil;

class WiadomosciMenager extends \Wspolne\Model\WspolneEventTable {

    private $tylkoWiadomosciProjektoweZChatu = false;
    private $menagerWlasciciela;
    static $ustawZalogowanegoJakoDomyslny = true;

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, \Konwersacje\Module::WIADOMOSCI_GATEWAY);

//	$this->projektyWatkiWiadomoscMenager = $this->sm->get(\Konwersacje\Menager\ProjektyWatkiWiadomoscMenager::class);
//	$this->projektyMenager = $this->sm->get(\Projekty\Menager\ProjektyMenager::class);
    }

    public function zapisz(WspolnyModel $model) {

	$this->ustawAutora($model);

	if (!isset($model->watek) || $model->watek === null) {

	    $model->watek = $this->zapiszWatekWiadomosci($model);
	    $this->dodajUzytkownicyWatkiWiadomosci($model);
	}

	if ($this->czyNowyModel($model)) {

	    $projektoweWatkiChatuMenager = $this->get(\Konwersacje\Menager\ProjektoweWatkiChatuMenager::class);
	    $model->projektowy_watek_chatu = $projektoweWatkiChatuMenager->utworzDlaNiezapisanejWiadomosci($model);
	}

	parent::zapisz($model);

	$this->dodajUzytkownicyWiadomosci($model);
    }

    public function zrobWiadomosc($post) {

	$wiadomosc = new Wiadomosc($this->sm);
	$wiadomosc->exchangeArray($post);

	$uzytkownikTable = $this->sm->get(\Logowanie\Module::UZYTKOWNIK_TABLE);
	$autor = $uzytkownikTable->getZalogowanegoUsera();
	$wiadomosc->autor = $autor;

	$temat = $this->pobierzTemat($post);
	$wiadomosc->getBuilder()->setTematWatkuWiadomosci($temat);

	$projektId = $this->pobierzProjektId($post);
	$wiadomosc->getBuilder()->setProjektId($projektId);

	$uzytkownicy = $this->pobierzTabliceUzytkownikow($post);
	foreach ($uzytkownicy as $userId) {
	    $uzytkownik = $uzytkownikTable->getRekord($userId);
	    $wiadomosc->getBuilder()->dodajUsera($uzytkownik);
	}

	return $wiadomosc;
    }

    public function zapiszZPosta($post) {
	$wiadomosc = $this->zrobWiadomosc($post);
	$this->zapisz($wiadomosc);
    }

    public function pobierzWiadomosciDlaWatku($watekId) {
	return $this->pobierzZWherem("watek_id=$watekId", "data_dodania ASC");
    }

    public function pobierzWiadomosciTypu($idWlasciciela,
	    WlascicielWatkuWiadomosciInterface $menagerWlasciciela) {

	$this->menagerWlasciciela = $menagerWlasciciela;

	$wlasciciel = $this->menagerWlasciciela->getRekord($idWlasciciela);

	$watekId = $this->menagerWlasciciela->pobierzIdWatku($wlasciciel);
	if ($watekId) {

	    $dodatkowyWhere = $this->zrobDodatkowegoWheraDoUzytkownikaWiadomosci($wlasciciel);
	    $wiadomosci = $this->pobierzZWherem("watek_id={$watekId} " . $dodatkowyWhere, "data_dodania DESC");

	    return $wiadomosci;
	}
	return array();
    }

    private function zrobDodatkowegoWheraDoUzytkownikaWiadomosci($wlasciciel) {
	if (!$this->czyZalogowanyJestTworcaEncji($wlasciciel)) {
	    $zalogowany = $this->getLogowanieFasada()->getZalogowanegoUsera();
	    return "AND  id in (select wiadomosc_id from uzytkownicy_wiadomosci where uzytkownik_id={$zalogowany->id})";
	}
	return "";
    }

    public function czyZalogowanyJestTworcaEncji($wlasciciel) {
	$zalogowany = $this->getLogowanieFasada()->getZalogowanegoUsera();
	$poleTworcy = $this->menagerWlasciciela->getPoleTworcy();
	return $zalogowany->id === $wlasciciel->$poleTworcy->id;
    }

    public function pobierzOstatnieWiadomosciDlaWatku($watekId) {
	$wiadomosci = $this->pobierzZWherem("watek_id=$watekId", "data_dodania DESC", 10);
	$result = array_reverse($wiadomosci);
	return $result;
    }

    public function pobierzNajnowszeWiadomosciDlaWatku($watekId, $ostatniaWiadomoscId) {
	return $this->pobierzZWherem("watek_id=$watekId AND id>$ostatniaWiadomoscId", "data_dodania ASC");
    }

    public function pobierzNajnowszeWiadomosciZalogowanego($ostatniaWiadomoscId,
	    $tylkoWiadomosciProjektoweZChatu = false) {

	$prywatne = $this->pobierzNajnowszeWiadomosciPrywatneZalogowanego($ostatniaWiadomoscId);
	$projektowe = $this->pobierzNajnowszeWiadomosciProjektoweZalogowanego($ostatniaWiadomoscId,
		$tylkoWiadomosciProjektoweZChatu);

	return array_merge($prywatne, $projektowe);
    }

    public function pobierzNajnowszeWiadomosciProjektoweZalogowanego($ostatniaWiadomoscId,
	    $tylkoWiadomosciProjektoweZChatu = false) {

	$this->tylkoWiadomosciProjektoweZChatu = $tylkoWiadomosciProjektoweZChatu;
	$where = "id>$ostatniaWiadomoscId AND";

	return $this->pobierzNajnowszaWiadomoscProjektowaWspolne($where, 'ASC');
    }

    public function pobierzNajnowszeWiadomosciPrywatneZalogowanego($ostatniaWiadomoscId) {
	$where = "id>$ostatniaWiadomoscId AND";

	return $this->pobierzNajnowszaWiadomoscPrywatnaWspolne($where, 'ASC');
    }

    public function pobierzOstatniaWiadomoscZalogowanego($tylkoWiadomosciProjektoweZChatu = false) {

	$this->tylkoWiadomosciProjektoweZChatu = $tylkoWiadomosciProjektoweZChatu;


	$prywatne = $this->pobierzNajnowszaWiadomoscPrywatnaWspolne('', 'DESC', 1);
	$projektowe = $this->pobierzNajnowszaWiadomoscProjektowaWspolne('', 'DESC', 1);

	$prywatneObj = count($prywatne) > 0 ? $prywatne[0] : Wiadomosc::create();
	$projektoweObj = count($projektowe) > 0 ? $projektowe[0] : Wiadomosc::create();

	$wynik = $prywatneObj->id > $projektoweObj->id ? $prywatneObj : $projektoweObj;

	return $wynik;
    }

    public function pobierzOstatnieWiadomosciChatu($idWatkuChatu, $limit) {
	return $this->pobierzZWherem("projektowy_watek_chatu_id=$idWatkuChatu", self::DOMYSLNY_ORDER, $limit);
    }

    public function zapiszWatekWiadomosci($wiadomosc) {
	$wlascicielId = $wiadomosc->getBuilder()->getProjektId();
	$menagerWlasciciela = \Konwersacje\Enumy\WatkiWiadomosciTypy::getMenagerWlascicielaWatku($wiadomosc->getBuilder()->getTypWatku(),
			$this->sm);

	if ($wlascicielId) {
	    $wlasciciel = $menagerWlasciciela->getRekord($wlascicielId);

	    if ($wlasciciel->watek_wiadomosci->id) {
		return $wlasciciel->watek_wiadomosci;
	    }
	}

	$watek = new WatekWiadomosci();
	$watek->temat = $wiadomosc->getBuilder()->getTematWatkuWiadomosci();

	if ($wlascicielId) {
	    $watek->typ = $wiadomosc->getBuilder()->getTypWatku();
	}

	$wynik = $this->zapiszWInnymTable($watek, WatkiWiadomosciTable::class);

	if ($wlascicielId) {
	    $menagerWlasciciela->polaczZWatkiemWiadomosci($wlascicielId, $wynik->id);
	}

	return $wynik;
    }

    private function ustawAutora($model) {

	$zalogowany = $this->getLogowanieFasada()->getZalogowanegoUsera();

	if (self::$ustawZalogowanegoJakoDomyslny) {

	    if (EntityUtil::wydobadzId($model->autor) === null) {
		$model->autor = $zalogowany;
	    }

	    $model->getBuilder()->dodajUsera($zalogowany);
	}

	$model->getBuilder()->dodajUsera($model->autor);
	$model->getBuilder()->dodajUsera($zalogowany);

	self::$ustawZalogowanegoJakoDomyslny = true;
    }

    private function dodajUzytkownicyWatkiWiadomosci($model) {
	foreach ($model->getBuilder()->pobierzUserow() as $uzytkownik) {
	    $uzytkownikWatekWiadomosci = new UzytkownikWatkekWiadomosci();
	    $uzytkownikWatekWiadomosci->uzytkownik = $uzytkownik;
	    $uzytkownikWatekWiadomosci->watek_wiadomosci = $model->watek;
	    $this->zapiszWInnymTable($uzytkownikWatekWiadomosci,
		    \Konwersacje\Module::UZYTKOWNIK_WATKEK_WIADOMOSCI_TABLE);
	}
    }

    private function dodajUzytkownicyWiadomosci($model) {
	if ($this->czyNowyModel($model)) {
	    $wiadomosc = $this->getPoprzednioDodany();

	    $watekChatuId = EntityUtil::wydobadzId($wiadomosc->projektowy_watek_chatu);
	    if ($watekChatuId) {
		$watkiChatuUzytkownicyMenager = $this->get(\Konwersacje\Menager\ProjektoweWatkiChatuUzytkownicyMenager::class);
		$watkiChatuUzytkownicy = $watkiChatuUzytkownicyMenager->pobierzDlaWatkuChatu($watekChatuId);

		foreach ($watkiChatuUzytkownicy as $watekChatuUzytkownik) {
		    $this->dodajUzytkownikWiadomosc($watekChatuUzytkownik->uzytkownik, $wiadomosc->id,
			    $wiadomosc->projektowy_watek_chatu);
		}
	    } else {

		foreach ($model->getBuilder()->pobierzUserow() as $uzytkownik) {
		    $this->dodajUzytkownikWiadomosc($uzytkownik, $wiadomosc->id, $wiadomosc->projektowy_watek_chatu);
		}
	    }
	}
    }

    private function dodajUzytkownikWiadomosc($uzytkownik, $wiadomoscId, $projektowyWatekChatu) {

	$uzytkownicyWiadomosciMenager = $this->get(\Konwersacje\Menager\UzytkownicyWiadomosciMenager::class);
	$uzytkownicyWiadomosciMenager->dodajUzytkownikWiadomosc($uzytkownik, $wiadomoscId,
		EntityUtil::wydobadzId($projektowyWatekChatu));
    }

    private function pobierzTabliceUzytkownikow($post) {
	if (isset($post["uzytkownicy"])) {
	    return \Pastmo\Wspolne\Utils\ArrayUtil::zrobTablice($post["uzytkownicy"]);
	}
	return array();
    }

    private function czyNowyModel($model) {
	return !$model->id;
    }

    private function pobierzNajnowszaWiadomoscProjektowaWspolne($where, $order_type, $limit = false) {
	$zalogowany = $this->getLogowanieFasada()->getZalogowanegoUsera();
	$prywatny = \Konwersacje\Enumy\WatkiWiadomosciTypy::PROJEKTOWY;

	$whereWiadomosci = '';

	if ($this->tylkoWiadomosciProjektoweZChatu) {
	    $whereWiadomosci = " AND z_chatu='1'";
	}

	return $this->pobierzZWherem($where . " id IN (select wiadomosc_id from uzytkownicy_wiadomosci where uzytkownik_id={$zalogowany->id} )"
			. " AND watek_id IN (select id from watki_wiadomosci where typ='{$prywatny}')$whereWiadomosci",
			"id $order_type", $limit);
    }

    private function pobierzNajnowszaWiadomoscPrywatnaWspolne($where, $order_type, $limit = false) {
	$zalogowany = $this->getLogowanieFasada()->getZalogowanegoUsera();
	$prywatny = \Konwersacje\Enumy\WatkiWiadomosciTypy::PRYWATNY;

	return $this->pobierzZWherem($where . " watek_id IN (select watek_wiadomosci_id from uzytkownicy_watki_wiadomosci where uzytkownik_id={$zalogowany->id} )"
			. " AND watek_id IN (select id from watki_wiadomosci where typ='{$prywatny}')", "id $order_type", $limit);
    }

    private function pobierzTemat($post) {
	if (\Pastmo\Wspolne\Utils\ArrayUtil::czyIstniejeNiepustyKlucz($post, 'temat')) {
	    return $post['temat'];
	}
	return '';
    }

    private function pobierzProjektId($post) {
	if (\Pastmo\Wspolne\Utils\ArrayUtil::czyIstniejeNiepustyKlucz($post, 'projekt_id')) {
	    return $post['projekt_id'];
	}
	return false;
    }

}
