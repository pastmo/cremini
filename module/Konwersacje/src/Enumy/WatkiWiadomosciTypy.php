<?php

namespace Konwersacje\Enumy;

class WatkiWiadomosciTypy {

    const PRYWATNY = 'prywatny';
    const PROJEKTOWY = 'projektowy';
    const Z_WYCEN = 'z wycen';

    public static function getMenagerWlascicielaWatku($typ, $sm) {
	switch ($typ) {
	    case \Konwersacje\Enumy\WatkiWiadomosciTypy::Z_WYCEN:
		return $sm->get(\Aukcje\Menager\WycenyProducentowMenager::class);
	    case \Konwersacje\Enumy\WatkiWiadomosciTypy::PRYWATNY:
		return new \Konwersacje\Interfejs\PrywatnyWlasciciel;
	    default:
		throw new \Wspolne\Exception\GgerpException("Typ wątku wiadomości '$typ' jeszcze nieobsługiwany");
	}
    }

}
