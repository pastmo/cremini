<?php

namespace Konwersacje\KomunikatyDoJs;

use Pastmo\Wspolne\Utils\EntityUtil;

class WiadomoscKomunikat {

    public $id;
    public $sender;
    public $projectName;
    public $projectId;
    public $roomId;
    public $projektowyWatekWiadomosciId = "0";
    public $body;
    public $created_at;

    public static function fromWiadomosc(\Konwersacje\Entity\Wiadomosc $wiadomosc) {
	$komunikat = new WiadomoscKomunikat();
	$komunikat->id = $wiadomosc->id;
	$komunikat->body = $wiadomosc->tresc;
	$komunikat->sender = Sender::create(self::getUzytkownikaNaEtykiete($wiadomosc));
	$komunikat->roomId = EntityUtil::wydobadzId($wiadomosc->watek);
	$komunikat->created_at = $wiadomosc->data_dodania;

	if (EntityUtil::wydobadzPole($wiadomosc->watek, 'typ') === \Konwersacje\Enumy\WatkiWiadomosciTypy::PROJEKTOWY) {
	    $projekt = $wiadomosc->getProjekt();
	    $komunikat->projectId = $projekt->id;
	    $komunikat->projectName = $projekt->nazwa;
	    $komunikat->projektowyWatekWiadomosciId = self::getProjektowyWatekChatuId($wiadomosc);
	} else {
	    $komunikat->projectId = 0;
	}

	return $komunikat;
    }

    public static function fromUzytkownik(\Logowanie\Model\Uzytkownik $uzytkownik) {
	$komunikat = new WiadomoscKomunikat();
	$komunikat->sender = Sender::create($uzytkownik);
	return $komunikat;
    }

    private static function getUzytkownikaNaEtykiete(\Konwersacje\Entity\Wiadomosc $wiadomosc) {
	$uzytkownicy = $wiadomosc->getUzytkownicyBezZalogowanego();

	$wynik = \Pastmo\Wspolne\Utils\ArrayUtil::pobierzPierwszyElement($uzytkownicy,
			\Logowanie\Model\Uzytkownik::create());
	return $wynik;
    }

    private static function getProjektowyWatekChatuId($wiadomosc) {
	if ($wiadomosc->z_chatu && \Pastmo\Wspolne\Utils\EntityUtil::wydobadzId($wiadomosc->projektowy_watek_chatu)) {
	    return \Pastmo\Wspolne\Utils\EntityUtil::wydobadzId($wiadomosc->projektowy_watek_chatu);
	}

	return '0';
    }

    public static function listaAdapter(array $lista) {
	$wynik = array();

	foreach ($lista as $element) {

	    if ($element instanceof \Konwersacje\Entity\Wiadomosc) {
		$wynik[] = self::fromWiadomosc($element);
	    } //
	    else if ($element instanceof \Logowanie\Model\Uzytkownik) {
		$wynik[] = self::fromUzytkownik($element);
	    }
	}

	return $wynik;
    }

}

class Sender {

    public $id;
    public $name;
    public $avatar;

    public static function create(\Logowanie\Model\Uzytkownik $uzytkownik) {
	$sender = new Sender();

	$sender->id = $uzytkownik->id;
	$sender->name = $uzytkownik . '';
	$sender->avatar = \Pastmo\Wspolne\Utils\EntityUtil::wydobadzPole($uzytkownik->avatar, 'wyswietlanie_js');

	return $sender;
    }

}
