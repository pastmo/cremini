<?php

return array(
	'controllers' => array(
		'factories' => array(
			'Konwersacje\Controller\Konwersacje' => 'Konwersacje\Controller\Factory\KonwersacjeControllerFactory',
		),
	),
	'router' => array(
		'routes' => array(
			'konwersacje' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/konwersacje[/:action][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id' => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Konwersacje\Controller\Konwersacje',
						'action' => 'najnowsze_wiadomosci',
					),
				),
			),
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Konwersacje' => __DIR__ . '/../view',
		),
		'strategies' => array(
			'ViewJsonStrategy',
		),
	),
);
