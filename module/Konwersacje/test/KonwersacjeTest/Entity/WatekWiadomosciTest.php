<?php

namespace KonwersacjeTest\Entity;

use Konwersacje\Entity\WatekWiadomosci;
use Konwersacje\Entity\UzytkownikWatkekWiadomosci;
use \Logowanie\Model\Uzytkownik;
use \Zasoby\Model\Zasob;

class WatekWiadomosciTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    private $obiect;
    private $user1;
    private $user2;
    private $uzytkownikWatekWiadomosci1;
    private $uzytkownikWatekWiadomosci2;
    private $avatar = 'avatar';

    public function setUp() {
	parent::setUp();
	$this->obiect = new WatekWiadomosci($this->sm);
	$this->obiect->id = 1;

	$this->ustawParametryPoczatkowe();
	$this->obslugaKlasObcych->ustawZalogowanegoUsera($this->user2);
	$this->ustawMockaNaUzytkownikWatekWiadomosciTable();
    }

    public function testgetUzytkownicyBezZalogowanego() {
	$uzytkownicy = $this->obiect->getUzytkownicyBezZalogowanego();

	$this->assertEquals(1, count($uzytkownicy));
    }

    public function testgetUzytkownicyAvataryBezZalogowanego() {
	$this->ustawMockaNaAvatary();

	$avatary = $this->obiect->getUzytkownicyAvataryBezZalogowanego();

	$this->assertEquals(1, count($avatary));
	$this->assertEquals($this->avatar, $avatary[0]);
    }

    protected function nowyObiekt() {
	new WatekWiadomosci();
    }

    private function ustawParametryPoczatkowe() {

	$this->user1 = new Uzytkownik();
	$this->user2 = new Uzytkownik();
	$this->user1->id = 1;
	$this->user2->id = 2;

	$this->user1->avatar = new Zasob($this->sm);
	$this->user2->avatar = new Zasob($this->sm);

	$this->uzytkownikWatekWiadomosci1 = new UzytkownikWatkekWiadomosci();
	$this->uzytkownikWatekWiadomosci2 = new UzytkownikWatkekWiadomosci();

	$this->uzytkownikWatekWiadomosci1->uzytkownik = $this->user1;
	$this->uzytkownikWatekWiadomosci2->uzytkownik = $this->user2;
    }

    private function ustawMockaNaUzytkownikWatekWiadomosciTable() {
	$this->uzytkownikWatekWiadomosciTable
		->expects($this->exactly(1))
		->method('pobierzDlaWatku')
		->will($this->returnValue(array($this->uzytkownikWatekWiadomosci1,
			    $this->uzytkownikWatekWiadomosci2)));
    }

    private function ustawMockaNaAvatary() {
	$this->zasobyUploadMenager
		->expects($this->exactly(1))
		->method('wyswietlUrlObrazka')
		->will($this->returnValue($this->avatar));
    }

}
