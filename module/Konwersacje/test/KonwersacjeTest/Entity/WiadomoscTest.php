<?php

namespace KonwersacjeTest\Entity;

use Konwersacje\Entity\Wiadomosc;
use Pastmo\Testy\Util\TB;

class WiadomoscTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    public function setUp() {
	parent::setUp();
    }

    public function testobliczKiedyDodano_now() {
	$date = new \DateTime();
	$this->obliczKiedyDodano($date, 'now');
    }

    public function testobliczKiedyDodano_yesterday() {
	$date = new \DateTime('yesterday');
	$this->obliczKiedyDodano($date, 'Yesterday');
    }

    public function testobliczKiedyDodano() {
	$date = new \DateTime('5 min ago');
	$this->obliczKiedyDodano($date, '5 minutes ago');
    }

    public function testgetTextMessage() {
	$this->assertGetTextMessage('zwykły tekst', 'zwykły tekst');
    }

    public function testgetTextMessage_html() {
	$this->assertGetTextMessage($this->dluzszyMail, 'A możesz to do zwykłego pliku tekstowego wrzucić?');
    }

    public function testgetUzytkownicyWiadomosci() {
	$expected = "wartość domyślna";
	$this->uzytkownicyWiadomosciMenager->expects($this->once())->method('pobierzDlaWiadomosci')
		->willReturn($expected);

	$wiadomosc = new Wiadomosc($this->sm);
	$wynik = $wiadomosc->getUzytkownicyWiadomosci();

	$this->assertEquals($expected, $wynik);
    }

    public function testgetUzytkownikWiadomoscZalogowanego() {
	$this->uzytkownicyWiadomosciMenager->expects($this->once())->method('pobierzDlaWiadomosci')->willReturn(array());

	$wiadomosc = new Wiadomosc($this->sm);
	$wynik = $wiadomosc->getUzytkownikWiadomoscZalogowanego();

	$this->assertEquals(\Konwersacje\Enumy\WidocznoscWiadomosci::ROZWINIETA, $wynik->widocznosc);
    }

    private function obliczKiedyDodano($date, $expects) {
	$dateString = $date->format("Y-m-d H:i:s");
	$wiadomosc = new Wiadomosc();

	$wynik = $wiadomosc->obliczKiedyDodano($dateString);

	$this->assertEquals($expects, $wynik);
    }

    private function assertGetTextMessage($input, $expected) {
	$wiadomosc = TB::create(Wiadomosc::class, $this)->setParameters(array('tresc' => $input))->make();
	$wynik = $wiadomosc->getTextMessage();
	$this->sprawdzStringZawiera($wynik, $expected);
    }

    protected function nowyObiekt() {
	new Wiadomosc();
    }

    private $dluzszyMail = '<!DOCTYPE html>
<html><head>
    <meta charset="UTF-8">
<style type="text/css">.mceResizeHandle {position: absolute;border: 1px solid black;background: #FFF;width: 5px;height: 5px;z-index: 10000}.mceResizeHandle:hover {background: #000}img[data-mce-selected] {outline: 1px solid black}img.mceClonedResizable, table.mceClonedResizable {position: absolute;outline: 1px dashed black;opacity: .5;z-index: 10000}

<!--
  @font-face  {font-family: "Cambria Math";  }
 @font-face  {font-family: Calibri;  }
  p.MsoNormal, li.MsoNormal, div.MsoNormal  {margin: 0cm;  margin-bottom: .0001pt;  font-size: 11.0pt;  font-family: "Calibri",sans-serif;  }
 a:link, span.MsoHyperlink  {  color: #0563C1;  text-decoration: underline;}
 a:visited, span.MsoHyperlinkFollowed  {  color: #954F72;  text-decoration: underline;}
 span.Stylwiadomocie-mail17  {  font-family: "Calibri",sans-serif;  color: windowtext;}
 .MsoChpDefault  {  font-family: "Calibri",sans-serif;  }
 @page WordSection1  {  margin: 70.85pt 70.85pt 70.85pt 70.85pt;}
 div.WordSection1  {}

-->
</style></head><body style=""><div>A mo&#380;esz to do zwyk&#322;ego pliku tekstowego wrzuci&#263;? Bo mi si&#281; znaki nowej linii pododawa&#322;y...</div>
<blockquote type="cite" style="position: relative; margin-left: 0px; padding-left: 10px; border-left: solid 1px blue;"><!-- [if !mso]><style>v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
w\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style><![endif] --><!-- [if gte mso 9]><xml>
<o:shapedefaults v:ext="edit" spidmax="1026" />
</xml><![endif] --><!-- [if gte mso 9]><xml>
<o:shapelayout v:ext="edit">
<o:idmap v:ext="edit" data="1" />
</o:shapelayout></xml><![endif] -->Dnia 5 grudzie&#324; 2016 o 13:29 &#34;Grzegorz Lachowicz @ GRUPAGLASSO.PL&#34; &#60;g.lachowicz@grupaglasso.pl&#62; napisa&#322;(a):<br><br>
<div class="WordSection1">
<p class="MsoNormal">to jest jaki&#347; inne e-mail, tamtych ju&#380; nie mam na serwerze.</p>
<p class="MsoNormal">&#160;</p>
<p class="MsoNormal"><span style="color: black;">Pozdrawiam,</span></p>
<div align="center">
<table class="MsoNormalTable" style="width: 95.0%;" border="0" cellpadding="0">
<tbody>
<tr style="height: 33.75pt;">
<td style="padding: .75pt .75pt .75pt .75pt; height: 33.75pt;">
<p class="MsoNormal" style="margin-right: 0cm; margin-bottom: 22.5pt; margin-left: 0cm;"><strong><span style="font-size: 10.0pt; font-family: \'Times New Roman\',serif; color: #11aeab; text-transform: uppercase; letter-spacing: 1.5pt;">Grzegorz Lachowicz</span></strong><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif;"><br></span><span style="font-size: 9.0pt; font-family: \'Times New Roman\',serif; color: #555555; text-transform: uppercase; letter-spacing: 1.5pt;">&#160;&#160;&#160;&#160;&#160;&#160;&#160;SALES MANAGER</span><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif;"><br><br></span><strong><span style="font-size: 10.0pt; font-family: \'Times New Roman\',serif; color: #888888;">&#160;&#160;&#160;&#160;&#160;&#160;&#160;tel. kom.:</span></strong><span style="font-size: 10.0pt; font-family: \'Times New Roman\',serif; color: #555555;"> +48 502 268 786</span><span style="font-size: 10.0pt; font-family: \'Times New Roman\',serif;"><br><strong><span style="color: #888888;">e-mail:</span></strong><span style="color: #555555;"> <a href="mailto:g.lachowicz@grupaglasso.pl"><span style="color: #555555; text-decoration: none;">g.lachowicz@grupaglasso.pl</span></a> </span></span><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif;"><br><br></span><span style="font-size: 10.0pt; font-family: \'Times New Roman\',serif; color: #555555;">GLASSO Pawe&#322; Szczerba | tel.: +48 71 344 44 17<br>ul. Kazimierza Wielkiego 67 | 50-077 Wroc&#322;aw</span><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif;"><br></span><span style="font-size: 10.0pt; font-family: \'Times New Roman\',serif; color: #555555;">NIP: 911-170-84-74 | REGON: 020923887 </span><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif;"></span></p>
</td>
</tr>
</tbody>
</table>
</div>
<p class="MsoNormal"><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif; display: none;">&#160;</span></p>
<table class="MsoNormalTable" border="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 120.0pt; padding: .75pt .75pt .75pt .75pt;" valign="top" width="160">
<p class="MsoNormal" style="text-align: center;" align="center"><a href="http://grupaglasso.pl/"><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif; color: blue; text-decoration: none;"><img id="_x0000_i1026" src="http://www.grupaglasso.pl/grupalogo.gif" border="0" alt="glasso" width="160" height="58"></span></a><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif;"><br></span><span style="font-size: 10.0pt; font-family: \'Times New Roman\',serif; color: #555555;"><a href="http://grupaglasso.pl/"><span style="color: #555555; text-decoration: none;">www.grupaglasso.pl</span></a> </span><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif;"><br></span><span style="font-size: 10.0pt; font-family: \'Times New Roman\',serif; color: #555555;"><a href="http://glasso.pl/"><span style="color: #555555; text-decoration: none;">www.glasso.pl</span></a> </span><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif;"><br></span><span style="font-size: 10.0pt; font-family: \'Times New Roman\',serif; color: #555555;"><a href="http://metaleo.pl/"><span style="color: #555555; text-decoration: none;">www.metaleo.pl</span></a> </span><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif;"><br></span><span style="font-size: 10.0pt; font-family: \'Times New Roman\',serif; color: #555555;"><a href="http://woodeo.pl/"><span style="color: #555555; text-decoration: none;">www.woodeo.pl</span></a> </span><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif;"><br></span><span style="font-size: 10.0pt; font-family: \'Times New Roman\',serif; color: #555555;"><a href="http://figro.pl/"><span style="color: #555555; text-decoration: none;">www.figro.pl</span></a> </span><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif;"><br></span><span style="font-size: 10.0pt; font-family: \'Times New Roman\',serif; color: #555555;"><a href="http://statuetkiszklane.org/"><span style="color: #555555; text-decoration: none;">www.statuetkiszklane.org</span></a> </span><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif;"><br></span><span style="font-size: 10.0pt; font-family: \'Times New Roman\',serif; color: #555555;"><a href="http://karykatura3d.pl/"><span style="color: #555555; text-decoration: none;">www.karykatura3d.pl</span></a> </span><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif;"></span></p>
</td>
<td style="width: 120.0pt; padding: .75pt .75pt .75pt .75pt;" valign="top" width="160">
<p class="MsoNormal" style="text-align: center;" align="center"><a href="http://spragia.pl/"><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif; color: blue; text-decoration: none;"><img id="_x0000_i1025" src="http://www.grupaglasso.pl/spragialogo.gif" border="0" alt="spragia" width="160" height="58"></span></a><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif;"><br></span><span style="font-size: 10.0pt; font-family: \'Times New Roman\',serif; color: #555555;"><a href="http://spragia.pl/"><span style="color: #555555; text-decoration: none;">www.spragia.pl</span></a> </span><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif;"><br></span><span style="font-size: 10.0pt; font-family: \'Times New Roman\',serif; color: #555555;"><a href="http://ekskluzywnewizytowki.pl/"><span style="color: #555555; text-decoration: none;">www.ekskluzywnewizytowki.pl</span></a> </span><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif;"></span></p>
</td>
</tr>
</tbody>
</table>
<p class="MsoNormal"><span>&#160;</span></p>
<p class="MsoNormal"><em><span style="font-size: 10.0pt; font-family: \'Times New Roman\',serif; color: #b2b2b2;">Tre&#347;&#263; tej wiadomo&#347;ci zawiera informacje przeznaczone tylko dla adresata. Je&#380;eli nie jeste&#347;cie Pa&#324;stwo jej adresatem, b&#261;d&#378; otrzymali&#347;cie j&#261; przez pomy&#322;k&#281;, prosimy o powiadomienie o tym nadawcy oraz trwa&#322;e jej usuni&#281;cie.</span></em><span style="font-size: 12.0pt; font-family: \'Times New Roman\',serif;"> </span><span></span></p>
<p class="MsoNormal">&#160;</p>
</div>
</blockquote>
<div><br>&#160;</div></body></html>

';

}
