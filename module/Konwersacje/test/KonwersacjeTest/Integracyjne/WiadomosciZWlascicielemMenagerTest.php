<?php

namespace KonwersacjeTest\Integracyjne;

class WiadomosciZWlascicielemMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $wiadomosciZWlascicielemMenager;
    private $wiadomosciMenager;
    private $projektyMenager;
    private $wycenyProducentowMenager;
    private $uzytkownik;
    private $uzytkownik2;
    private $watkiWiadomosciTable;

    public function setUp() {
	parent::setUp();
	$this->wiadomosciZWlascicielemMenager = $this->sm->get(\Konwersacje\Menager\WiadomosciZWlascicielemMenager::class);
	$this->wiadomosciMenager = $this->sm->get(\Konwersacje\Model\WiadomosciMenager::class);
	$this->projektyMenager = $this->sm->get(\Projekty\Menager\ProjektyMenager::class);
	$this->wycenyProducentowMenager = $this->sm->get(\Aukcje\Menager\WycenyProducentowMenager::class);
	$this->watkiWiadomosciTable = $this->sm->get(\Konwersacje\Model\WatkiWiadomosciTable::class);

	$this->uzytkownik = $this->makeEntityBuilder(\Logowanie\Model\Uzytkownik::class)->make();
	$this->uzytkownik2 = $this->makeEntityBuilder(\Logowanie\Model\Uzytkownik::class)->make();
	$this->ustawZalogowanegoUsera($this->uzytkownik->id);
    }

    public function test_dodajWiadomoscDoProjektu() {
	$projekt = $this->makeEntityBuilder(\Projekty\Entity\Projekt::class)->make();

	$this->wiadomosciZWlascicielemMenager->dodajWiadomoscDoWlasciciela($projekt->id, __METHOD__,
		\Konwersacje\Enumy\WatkiWiadomosciTypy::PROJEKTOWY, null, array($this->uzytkownik2->id));

	$wynik = $this->wiadomosciMenager->pobierzWiadomosciTypu($projekt->id, $this->projektyMenager);

	$this->assertEquals(1, count($wynik));
	$this->assertEquals(__METHOD__, $wynik[0]->tresc);
	$this->assertEquals($this->uzytkownik->id, $wynik[0]->autor->id);
	$this->sprawdzWyszukiwanie_pobierzNiepusteDlaUzytkownika($this->uzytkownik->id);

	$this->sprawdzWyszukiwanie_pobierzNiepusteDlaUzytkownika($this->uzytkownik2->id);
    }

    public function test_dodajWiadomoscSystemowaDoProjektu() {
	$projekt = $this->makeEntityBuilder(\Projekty\Entity\Projekt::class)->make();

	$this->wiadomosciZWlascicielemMenager->dodajWiadomoscSystemowa($projekt->id, __METHOD__,
		\Konwersacje\Enumy\WatkiWiadomosciTypy::PROJEKTOWY, array($this->uzytkownik2->id));

	$wynik = $this->wiadomosciMenager->pobierzWiadomosciTypu($projekt->id, $this->projektyMenager);

	$this->assertEquals(1, count($wynik));
	$this->assertEquals(__METHOD__, $wynik[0]->tresc);
	$this->assertEquals(null, $wynik[0]->autor->id);
	$this->sprawdzWyszukiwanie_pobierzNiepusteDlaUzytkownika($this->uzytkownik->id);
	$this->sprawdzWyszukiwanie_pobierzNiepusteDlaUzytkownika($this->uzytkownik2->id);
    }

    public function test_dodajWiadomoscDoWyceny() {
	$wycena = $this->makeEntityBuilder(\Aukcje\Entity\WycenaProducenta::class)->make();

	$this->wiadomosciZWlascicielemMenager->dodajWiadomoscDoWlasciciela($wycena->id, __METHOD__,
		\Konwersacje\Enumy\WatkiWiadomosciTypy::Z_WYCEN);

	$wynik = $this->wiadomosciMenager->pobierzWiadomosciTypu($wycena->id, $this->wycenyProducentowMenager);

	$this->assertEquals(1, count($wynik));
	$this->assertEquals(__METHOD__, $wynik[0]->tresc);
	$this->sprawdzWyszukiwanie_pobierzNiepusteDlaUzytkownika($this->uzytkownik->id);
    }

    private function sprawdzWyszukiwanie_pobierzNiepusteDlaUzytkownika($id) {
	$watki = $this->watkiWiadomosciTable->pobierzNiepusteDlaUzytkownika($id);

	$this->assertEquals(1, count($watki));
    }

}
