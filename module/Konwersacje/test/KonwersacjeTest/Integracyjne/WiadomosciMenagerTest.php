<?php

namespace KontaktyTest\Integracyjne;

use Pastmo\Testy\Util\TB;
use Konwersacje\Entity\WatekWiadomosci;
use Konwersacje\Enumy\WatkiWiadomosciTypy;
use Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane;

class WiadomosciMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    public $zalogowany;
    public $watek;
    public $watekBuilder;
    public $projekt;
    public $wycenaProducenta;
    private $wiadomosciMenager;
    private $projektyMenager;
    private $wycenyProducentowMenager;

    public function setUp() {
	parent::setUp();
	$this->wiadomosciMenager = $this->sm->get(\Konwersacje\Model\WiadomosciMenager::class);
	$this->projektyMenager = $this->sm->get(\Projekty\Menager\ProjektyMenager::class);
	$this->wycenyProducentowMenager = $this->sm->get(\Aukcje\Menager\WycenyProducentowMenager::class);
    }

    public function test_pobierzWiadomosciTypu_projektowego() {

	$this->zrobZalogowanegoUseraIWatekBuildera(WatkiWiadomosciTypy::PROJEKTOWY);
	$this->zrobProjekt();
	$this->podepnijUzytkownicyWiadomosci();

	$wiadomosci = $this->wiadomosciMenager->pobierzWiadomosciTypu($this->projekt->id, $this->projektyMenager);

	$this->assertEquals(1, count($wiadomosci));
    }

    public function test_pobierzWiadomosciTypu_z_wyceny() {

	$this->zrobZalogowanegoUseraIWatekBuildera(WatkiWiadomosciTypy::Z_WYCEN);
	$this->zrobWyceneProducenta();
	$this->podepnijUzytkownicyWiadomosci();

	$wiadomosci = $this->wiadomosciMenager->pobierzWiadomosciTypu($this->wycenaProducenta->id,
		$this->wycenyProducentowMenager);

	$this->assertEquals(1, count($wiadomosci));
    }

    public function test_pobierzOstatnieWiadomosciDlaWatku() {

	$this->zrobZalogowanegoUseraIWatekBuildera(WatkiWiadomosciTypy::Z_WYCEN);
	$this->watekBuilder->make();
	$this->podepnijUzytkownicyWiadomosci();
	$this->podepnijUzytkownicyWiadomosci();

	$wiadomosci = $this->wiadomosciMenager->pobierzOstatnieWiadomosciDlaWatku($this->watek->id);

	$this->assertGreaterThan($wiadomosci[0], $wiadomosci[1]);
    }

    private function zrobZalogowanegoUseraIWatekBuildera($typ) {
	TB::create(\Logowanie\Model\Uzytkownik::class, $this)->setResultField('zalogowany')->make();
	$this->ustawZalogowanegoUsera($this->zalogowany->id);

	$this->watekBuilder = TB::create(WatekWiadomosci::class, $this)
		->setResultField('watek')
		->setParameters(array('typ' => $typ));
    }

    private function podepnijUzytkownicyWiadomosci() {
	TB::create(\Konwersacje\Entity\UzytkownikWiadomosc::class, $this)
		->setParameters(array('uzytkownik' => $this->zalogowany))
		->addOneToMany('wiadomosc',
			TB::create(\Konwersacje\Entity\Wiadomosc::class, $this)
			->setParameters(array('autor' => $this->zalogowany, 'watek' => $this->watek)))->make();
    }

    private function zrobWyceneProducenta() {
	TB::create(\Aukcje\Entity\WycenaProducenta::class, $this)
		->setResultField('wycenaProducenta')
		->addOneToMany('watek_wiadomosci', $this->watekBuilder)->make();
    }

    private function zrobProjekt() {

	TB::create(\Projekty\Entity\Projekt::class, $this)
		->setResultField('projekt')
		->addOneToMany('watek_wiadomosci', $this->watekBuilder)->make();
    }

}
