<?php

namespace KonwersacjeTest\Integracyjne;

use Pastmo\Testy\Util\TB;

class UzytkownicyWiadomosciMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $menager;

    public function setUp() {
	parent::setUp();
	$this->menager = $this->sm->get(\Konwersacje\Menager\UzytkownicyWiadomosciMenager::class);
    }

    public function testaktualizujRozwiniecieWiadomosci() {
	$uzytkownikWiadomosc = TB::create(\Konwersacje\Entity\UzytkownikWiadomosc::class, $this)->make();

	$this->assertEquals(\Konwersacje\Enumy\WidocznoscWiadomosci::ROZWINIETA, $uzytkownikWiadomosc->widocznosc);

	$ids = \Pastmo\Wspolne\Utils\ArrayUtil::zrobTablice($uzytkownikWiadomosc->id);
	$this->menager->aktualizujRozwiniecieWiadomosci($ids, '0');

	$wynik = $this->menager->getRekord($uzytkownikWiadomosc->id);

	$this->assertEquals(\Konwersacje\Enumy\WidocznoscWiadomosci::ZWINIETA, $wynik->widocznosc);
    }

}
