<?php

namespace KonwersacjeTest\Integracyjne;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Konwersacje\Entity\WatekWiadomosci;

class ProjektoweWatkiChatuMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $watekWiadomosci;
    private $uzytkownicy;

    public function setUp() {
	parent::setUp();

	$this->glowneTable = $this->sm->get(\Konwersacje\Menager\ProjektoweWatkiChatuMenager::class);

	$this->watekWiadomosci = \FabrykaRekordow::makeEncje(WatekWiadomosci::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm));

	$this->uzytkownicy = array();
    }

    public function test_dodajRekord() {
	$encja = \FabrykaRekordow::makeEncje(\Konwersacje\Entity\ProjektowyWatekChatu::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneBrakzapisuPowiazane::create($this->sm));

	$this->glowneTable->zapisz($encja);
	$ostatniRekord = $this->glowneTable->getPoprzednioDodany();

	$this->assertNotNull($ostatniRekord->id);
	$this->assertNotNull($ostatniRekord->watek_wiadomosci->id);
    }

    public function test_utworzDlaNiezapisanejWiadomosci() {
	$watek=\FabrykaRekordow::makeEncje(WatekWiadomosci::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm)
				->setparametry(array("typ" => \Konwersacje\Enumy\WatkiWiadomosciTypy::PROJEKTOWY)));
	$wiadomosc = \FabrykaRekordow::makeEncje(\Konwersacje\Entity\Wiadomosc::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneBrakzapisuNiepowiazane::create($this->sm)
				->setparametry(array("z_chatu" => "1",'watek'=>$watek)));

	$projektowyWatekChatu = $this->glowneTable->utworzDlaNiezapisanejWiadomosci($wiadomosc);
	$this->assertNotNull($projektowyWatekChatu);
    }

    public function test_pobierzLubUtworzDlaWatkuWiadomosciIListyUzytkownikow() {

	$this->wypelnijTabliceUzytkownikow();

	$wynik = $this->glowneTable->pobierzLubUtworzDlaWatkuWiadomosciIListyUzytkownikow($this->watekWiadomosci->id,
		$this->uzytkownicy);
	$this->assertNotNull($wynik);

	$wynik2 = $this->glowneTable->pobierzLubUtworzDlaWatkuWiadomosciIListyUzytkownikow($this->watekWiadomosci->id,
		$this->uzytkownicy);

	$this->assertEquals($wynik->id, $wynik2->id);
    }

    public function test_pobierzLubUtworzDlaWatkuWiadomosciIListyUzytkownikow_istniejacy() {

	$this->wypelnijTabliceUzytkownikow();

	$istniejacy = $this->zrobWatekChatuZUzytkownikami($this->watekWiadomosci,
		$this->uzytkownicy);

	$wynik = $this->glowneTable->pobierzLubUtworzDlaWatkuWiadomosciIListyUzytkownikow($this->watekWiadomosci->id,
		$this->uzytkownicy);

	$this->assertNotNull($wynik);
	$this->assertEquals($istniejacy->id, $wynik->id);
    }

    public function test_pobierzLubUtworzDlaWatkuWiadomosciIListyUzytkownikow_istniejacy_z_innymi_userami() {

	$this->wypelnijTabliceUzytkownikow();


	$istniejacy = $this->zrobWatekChatuZUzytkownikami($this->watekWiadomosci,
		$this->uzytkownicy);

	$this->uzytkownicy[] = $this->zrobUzytkownika();

	$wynik = $this->glowneTable->pobierzLubUtworzDlaWatkuWiadomosciIListyUzytkownikow($this->watekWiadomosci->id,
		$this->uzytkownicy);

	$this->assertNotNull($wynik);
	$this->assertNotSame($istniejacy->id, $wynik->id);
    }

    public function test_pobierzProjektoweWatkiWiadomosciUzytkownika_projekty() {
	$fabryka = \Testy\FabrykiKonkretnychEncji\ProjektowyWatekChatuFactory::create(\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm));
	$fabryka->utworzPojedynczyWatekChatu();

	$this->ustawZalogowanegoUsera($fabryka->uzytkownik->id);
	$watkiWiadomosci = $this->glowneTable->pobierzProjektoweChatoweWatkiWiadomosciUzytkownika();
	$this->assertEquals(1, count($watkiWiadomosci));
    }

    protected function zrobUzytkownika() {
	return \FabrykaRekordow::makeEncje(\Logowanie\Model\Uzytkownik::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm));
    }

    private function zrobWatekChatuZUzytkownikami($watekWiadomoci, $uzytkownicy) {
	$encja = \FabrykaRekordow::makeEncje(\Konwersacje\Entity\ProjektowyWatekChatu::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane::create($this->sm)
				->setparametry(array('watek_wiadomosci' => $watekWiadomoci)));

	foreach ($uzytkownicy as $uzytkownik) {
	    \FabrykaRekordow::makeEncje(\Konwersacje\Entity\ProjektowyWatekChatuUzytkownik::class,
		    \Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm)
			    ->setparametry(array('projektowy_watek_chatu' => $encja->id, 'uzytkownik' => $uzytkownik)));
	}
	return $encja;
    }

    private function wypelnijTabliceUzytkownikow() {
	$this->uzytkownicy[] = $this->zrobUzytkownika();
	$this->uzytkownicy[] = $this->zrobUzytkownika();
    }

}
