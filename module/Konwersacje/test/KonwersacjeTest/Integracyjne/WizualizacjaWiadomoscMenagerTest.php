<?php

namespace KonwersacjeTest\Integracyjne;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Konwersacje\Entity\UzytkownikWatkekWiadomosci;

class WizualizacjaWiadomoscMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

	public function setUp() {
		parent::setUp();
		$this->glowneTable = $this->sm->get(\Konwersacje\Menager\WizualizacjeWiadomosciMenager::class);
	}

	public function test_dodajRekord() {
		$this->ustawZalogowanegoUseraId1();
		$wiadomosc = \FabrykaRekordow::utworzEncje($this->sm,
						\Konwersacje\Entity\Wiadomosc::class);
		$wizualizacja = \FabrykaRekordow::utworzEncje($this->sm,
						\Projekty\Entity\ProjektProduktWariacjaWizualizacja::class);

		$data = array(
			'wiadomosc_id' => $wiadomosc->id,
			'projekt_produkt_wariacja_wizualizacja_id' => $wizualizacja->id
		);

		$encja = new \Konwersacje\Entity\WizualizacjaWiadomosc($this->sm);
		$encja->exchangeArray($data);

		$this->glowneTable->zapisz($encja);

		$ostatniRekord = $this->glowneTable->getPoprzednioDodany();

		$this->assertNotNull($ostatniRekord->wiadomosc->id);
		$this->assertNotNull($ostatniRekord->wizualizacja->id);
	}

}
