<?php

namespace KonwersacjeTest\Integracyjne;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Konwersacje\Entity\WatekWiadomosci;

class WatekWiadomosciTableTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $glowneTable;
    private $expectedSize = 2;
    private $wiad1 = 'wiad1';
    private $wiad2 = 'wiad2';
    private $factory;

    public function setUp() {
	parent::setUp();

	$this->glowneTable = $this->sm->get(\Konwersacje\Module::WATKI_WIADOMOSCI_TABLE);
	$this->factory = \WatekWiadomosciFactory::create(\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm));
	$this->ustawZalogowanegoUseraId1();
    }

    public function test_dodajRekord() {
	$nazwa = 'test_dodajRekord' . __CLASS__;

	$encja = new WatekWiadomosci();
	$encja->temat = $nazwa;
	$this->glowneTable->zapisz($encja);

	$ostatniRekord = $this->glowneTable->getPoprzednioDodany();

	$this->assertEquals($ostatniRekord->temat, $nazwa);
    }

    public function testpobierzDlaUzytkownika() {

	$expect = 1;

	$this->factory->utworzWatekZUzytkownikiem();
	\FabrykaRekordow::makeEncje(\Konwersacje\Entity\Wiadomosc::class,
		\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm)->setparametry(array('watek' => $this->factory->watek)));

	$watki = $this->glowneTable->pobierzNiepusteDlaUzytkownika($this->factory->uzytkownik->id);

	$this->assertEquals($expect, count($watki));
    }

    public function testPobierzWiadomosci() {
	$watek = $this->utworzWatekWiadomosciZDwiemaWiadomosciami();

	$wiadomosci = $watek->getWiadomosci();

	$this->assertEquals($this->expectedSize, count($wiadomosci));
	$this->assertEquals($this->wiad1, $wiadomosci[0]->tresc);
	$this->assertEquals($this->wiad2, $wiadomosci[1]->tresc);
    }

    public function testPobierzNajnowszaWiadomosc() {
	$watek = $this->utworzWatekWiadomosciZDwiemaWiadomosciami();

	$wiadomosc = $watek->pobierzNajnowszaWiadomosc();

	$this->assertEquals($this->wiad2, $wiadomosc->tresc);
    }

    private function utworzWatekWiadomosciZDwiemaWiadomosciami() {
	$watek = \FabrykaRekordow::utworzEncje($this->sm,
			\Konwersacje\Entity\WatekWiadomosci::getClass());


	$this->utworzWiadomoscDlaWatku($watek, $this->wiad1);
	$this->utworzWiadomoscDlaWatku($watek, $this->wiad2);

	return $watek;
    }

    private function utworzWiadomoscDlaWatku($watek, $tresc) {
	$parametry2 = array(
	    'watek' => $watek,
	    'tresc' => $tresc
	);

	\FabrykaRekordow::utworzEncje($this->sm,
		\Konwersacje\Entity\Wiadomosc::getClass(), $parametry2);
    }

}
