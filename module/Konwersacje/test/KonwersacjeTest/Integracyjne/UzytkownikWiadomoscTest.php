<?php

namespace KonwersacjeTest\Integracyjne;

use Konwersacje\Entity\UzytkownikWiadomosc;
use Pastmo\Testy\Util\TB;
use Konwersacje\Enumy\WidocznoscWiadomosci;

class UzytkownikWiadomoscTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $uzytkownicyWiadomoscMenager;

    public function setUp() {
	parent::setUp();
	$this->uzytkownicyWiadomoscMenager = $this->sm->get(\Konwersacje\Menager\UzytkownicyWiadomosciMenager::class);
    }

    public function testZapis() {
	$this->ustawZalogowanegoUseraId1();
	$uzytkownikWiadomosc = TB::create(UzytkownikWiadomosc::class, $this)
		->setParameters(array('widocznosc' => WidocznoscWiadomosci::ZWINIETA))
		->setPF_IBP($this->sm)
		->make();

	$this->uzytkownicyWiadomoscMenager->zapisz($uzytkownikWiadomosc);

	$dodany = $this->uzytkownicyWiadomoscMenager->getPoprzednioDodany();

	$this->assertNotNull($dodany->id);
	$this->assertNotNull($dodany->uzytkownik->id);
	$this->assertNotNull($dodany->wiadomosc->id);
	$this->assertEquals(WidocznoscWiadomosci::ZWINIETA, $dodany->widocznosc);
    }

}
