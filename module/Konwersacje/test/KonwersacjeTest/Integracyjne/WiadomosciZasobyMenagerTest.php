<?php

namespace KonwersacjeTest\Integracyjne;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Konwersacje\Entity\UzytkownikWatkekWiadomosci;



class WiadomosciZasobyMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

	public function setUp() {
		parent::setUp();
		$this->glowneTable = $this->sm->get(\Konwersacje\Menager\WiadomosciZasobyMenager::class);
	}

	public function test_dodajRekord() {

		$this->ustawZalogowanegoUseraId1();

		$wiadomosc = \FabrykaRekordow::utworzEncje($this->sm,
						\Konwersacje\Entity\Wiadomosc::class);
		$zasob = \FabrykaRekordow::utworzEncje($this->sm,
						\Zasoby\Model\Zasob::class);

		$data = array(
			'wiadomosc_id' => $wiadomosc->id,
			'zasob_id' => $zasob->id
		);

		$encja = new \Konwersacje\Entity\WiadomoscZasob($this->sm);
		$encja->exchangeArray($data);

		$this->glowneTable->zapisz($encja);

		$ostatniRekord = $this->glowneTable->getPoprzednioDodany();

		$this->assertNotNull($ostatniRekord->wiadomosc->id);
		$this->assertNotNull($ostatniRekord->zasob->id);
	}

}
