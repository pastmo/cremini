<?php

namespace KonwersacjeTest\Integracyjne;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Konwersacje\Entity\WatekWiadomosci;

class ProjektoweWatkiChatuUzytkownicy extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $menager;
    private $watekChatuUzytkownik;

    public function setUp() {
	parent::setUp();

	$this->menager = $this->sm->get(\Konwersacje\Menager\ProjektoweWatkiChatuUzytkownicyMenager::class);

	$this->uzytkownicy = array();
    }

    public function test_dodajRekord() {
	$encja = \FabrykaRekordow::makeEncje(\Konwersacje\Entity\ProjektowyWatekChatuUzytkownik::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneBrakzapisuPowiazane::create($this->sm));

	$this->menager->zapisz($encja);
	$ostatniRekord = $this->menager->getPoprzednioDodany();

	$this->assertNotNull($ostatniRekord->id);
	$this->assertNotNull($ostatniRekord->projektowy_watek_chatu->id);
	$this->assertNotNull($ostatniRekord->uzytkownik->id);
    }

    public function test_pobierzDlaWatkuChatu() {
	$this->zrobWatekChatuUzytkownicy();

	$wynik = $this->menager->pobierzDlaWatkuChatu($this->watekChatuUzytkownik->projektowy_watek_chatu->id);

	$this->assertEquals(1, count($wynik));
	$this->assertEquals($this->watekChatuUzytkownik->id, $wynik[0]->id);
    }

    public function test_dodajDoWatkuChatu() {
	$this->zrobWatekChatuUzytkownicy();

	$user=$this->zrobUzytkownika();
	$dodatkowi = array($user);

	$this->menager->dodajDoWatkuChatu($this->watekChatuUzytkownik->projektowy_watek_chatu->id,
		$dodatkowi);
	$wynik = $this->menager->pobierzDlaWatkuChatu($this->watekChatuUzytkownik->projektowy_watek_chatu->id);

	$this->assertEquals(2, count($wynik));

	$this->assertEquals($user->id, $wynik[0]->uzytkownik->id);
    }

    public function test_pobierzDlaWatkuChatuIUzytkownikowCount() {
	$this->zrobWatekChatuUzytkownicy();

	$user=$this->zrobUzytkownika();
	$dodatkowi = array($user);

	$this->menager->dodajDoWatkuChatu($this->watekChatuUzytkownik->projektowy_watek_chatu->id,
		$dodatkowi);
	$wynik = $this->menager->pobierzDlaWatkuChatuIUzytkownikowCount($this->watekChatuUzytkownik->projektowy_watek_chatu->id,
		array($user));

	$this->assertEquals(1, $wynik);

    }

    private function zrobWatekChatuUzytkownicy() {
	$this->watekChatuUzytkownik = \FabrykaRekordow::makeEncje(\Konwersacje\Entity\ProjektowyWatekChatuUzytkownik::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane::create($this->sm));
    }

}
