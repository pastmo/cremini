<?php

namespace KonwersacjeTest\Table;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Konwersacje\Entity\UzytkownikWatkekWiadomosci;

require_once dirname(__FILE__) . '\..\..\..\..\Wspolne\test\Fabryki\FabrykaRekordow.php';

class UzytkownikWatkekWiadomosciTableTest extends AbstractHttpControllerTestCase {

	public function setUp() {
		$this->setApplicationConfig(
				include \Application\Stale::configTestPath
		);
		parent::setUp();

		$this->sm = $this->getApplicationServiceLocator();
		$this->glowneTable = $this->sm->get(\Konwersacje\Module::UZYTKOWNIK_WATKEK_WIADOMOSCI_TABLE);
	}

	public function test_dodajRekord() {

		$uzytkownik=\FabrykaRekordow::makeEncje( \Logowanie\Model\Uzytkownik::class,						\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm));
		$watekWiadomosci=\FabrykaRekordow::utworzEncje($this->sm, \Konwersacje\Entity\WatekWiadomosci::getClass());

		$data=array(
			'uzytkownik_id'=>$uzytkownik->id,
			'watek_wiadomosci_id'=>$watekWiadomosci->id
		);

		$encja = new UzytkownikWatkekWiadomosci($this->sm);
		$encja->exchangeArray($data);

		$this->glowneTable->zapisz($encja);

		$ostatniRekord = $this->glowneTable->getPoprzednioDodany();

		$this->assertNotNull($ostatniRekord->uzytkownik->login);
		$this->assertNotNull($ostatniRekord->watek_wiadomosci->temat);
	}



}
