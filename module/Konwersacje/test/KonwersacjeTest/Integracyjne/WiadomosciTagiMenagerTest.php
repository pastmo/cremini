<?php

namespace ProjektyTest\Integracyjne;

use \Konwersacje\Entity\Wiadomosc;
use \Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane;

class WiadomosciTagiMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

	protected $nazwaKlienta = "nazwa klienta";
	protected $traceError = true;
	protected $sm;
	private $tagi = array(array('label' =>
			"asdf",
			'value' =>
			"asdf"));
	protected $wiadomosciTagiMenager;
	protected $wiadomosc;
	protected $wartoscTaga;

	public function setUp() {
		parent::setUp();
		$this->ustawZalogowanegoUseraId1();

		$this->wiadomosciTagiMenager = $this->sm->get(\Konwersacje\Menager\WiadomosciTagiMenager::class);

		$this->wiadomosc = \FabrykaRekordow::makeEncje(Wiadomosc::class,
						PFIntegracyjneZapisNiepowiazane::create($this->sm));
		$this->wartoscTaga = $this->tagi[0]['value'];
	}

	public function test_projektyTagi() {
		$this->startTransaction();

		$this->dodajTagiDoWiadomosci();
		$this->sprawdzDodanieTagow();

		$this->dodajTagiDoWiadomosci();
		$this->sprawdzCzySiePowiazaniaZTagamiNiePowielaja();

		$this->rollbackTransaction();
	}

	public function test_getWiadomosciTagiDlaWatku() {
		$factory = \ProjektFactory::create($this->sm);
		$factory->utworzRozbudowanyProjekt(true);

		$tag = \FabrykaRekordow::makeEncje(\Projekty\Entity\Tag::class,
						PFIntegracyjneZapisNiepowiazane::create($this->sm));

		$wiadomoscTag = \FabrykaRekordow::makeEncje(\Konwersacje\Entity\WiadomoscTag::class,
						PFIntegracyjneZapisNiepowiazane::create($this->sm)
								->setparametry(array('wiadomosc' => $factory->wiadomosc, 'tag' => $tag)));

		$wiadomoscTagi = $this->wiadomosciTagiMenager->getWiadomosciTagiDlaWatku($factory->projekt->watek_wiadomosci->id);

		$this->assertEquals(1, count($wiadomoscTagi));
	}

	private function dodajTagiDoWiadomosci() {
		$this->wiadomosciTagiMenager->dodajTagiDoWiadomosci($this->wiadomosc->id,
				$this->tagi);
	}

	private function sprawdzDodanieTagow() {
		$ostatniRekord = $this->wiadomosciTagiMenager->getPoprzednioDodany();

		$this->assertNotNull($ostatniRekord->id);
		$this->assertEquals($this->wartoscTaga, $ostatniRekord->tag->nazwa);
	}

	private function sprawdzCzySiePowiazaniaZTagamiNiePowielaja() {
		$tagiMenager = $this->sm->get(\Konwersacje\Menager\WiadomosciTagiMenager::class);
		$projektyTagi = $tagiMenager->pobierzZWherem("wiadomosc_id=" . $this->wiadomosc->id);

		$this->assertEquals(1, count($projektyTagi));
	}

}
