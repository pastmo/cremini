<?php

namespace KonwersacjeTest\Integracyjne;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Konwersacje\Entity\UzytkownikWatkekWiadomosci;

class ProjektyWatkiWiadomoscMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    protected $projekt;
    private $watekWiadomosci;

    public function setUp() {
	parent::setUp();
	$this->glowneTable = $this->sm->get(\Konwersacje\Menager\ProjektyWatkiWiadomoscMenager::class);

	$this->projekt = \FabrykaRekordow::utworzEncje($this->sm,
			\Projekty\Entity\Projekt::class);
	$this->watekWiadomosci = \FabrykaRekordow::utworzEncje($this->sm,
			\Konwersacje\Entity\WatekWiadomosci::getClass());
    }


    public function test_polaczWatekZProjektem() {
	$this->glowneTable->polaczWatekZProjektem($this->watekWiadomosci->id,
		$this->projekt->id);

	$ostatniRekord = $this->glowneTable->getRekord($this->projekt->id);

	$this->assertEquals($this->watekWiadomosci->id, $ostatniRekord->watek_wiadomosci->id);
    }

}
