<?php

namespace KonwersacjeTest\Controller;

use \Pastmo\Testy\ParametryFabryki\PFMockowePowiazane;
use Pastmo\Testy\Util\TB;

class KonwersacjeControllerTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

    protected $traceError = true;
    private $watekWiadomosci;
    private $wiadomosci = array();

    public function setUp() {
	parent::setUp();

	$this->obslugaKlasObcych->ustawZalogowanegoUsera();
	$this->watekWiadomosci = \FabrykaEncjiMockowych::makeEncje(\Konwersacje\Entity\WatekWiadomosci::class,
			PFMockowePowiazane::create($this->sm));
	$this->obslugaKlasObcych->ustawMetode(\Konwersacje\Menager\UzytkownicyWiadomosciMenager::class,
		'pobierzDlaWiadomosci', array());
    }

    public function testIndex() {

    }

    public function testwatkiAction() {

	$this->watkiWiadomosciTable->expects($this->once())->method('pobierzNiepusteDlaUzytkownika')
		->willReturn(array($this->watekWiadomosci));

	$this->dispatch('/konwersacje/watki');
	$this->sprawdzCzySukces();
	$this->sprawdzOdpowiedzZawiera('"watki_wiadomosci":[{"watek":');
    }

    public function testwiadomosciAction_pustaListaWiadomosci() {
	$this->ustawMocki();
	$this->ustawMockUzytkownikWatekWiadomosci();


	$this->dispatch('/konwersacje/wiadomosci?watek_id=5');
	$this->sprawdzCzySukces();
	$this->sprawdzOdpowiedzZawiera('"wiadomosci":[],"watek":{');
    }

    public function testwiadomosciAction_wszystkieWiadomosci() {
	$this->dodajNowaWiadomosc();
	$this->ustawMocki();
	$this->ustawMockUzytkownikWatekWiadomosci();

	$this->dispatch("/konwersacje/wiadomosci?watek_id=5");
	$this->sprawdzCzySukces();
	$this->sprawdzOdpowiedzZawiera('"wiadomosc":{"id');
	$this->sprawdzOdpowiedzZawiera('"tryb":"' . \Konwersacje\Enumy\TrybOdswiezaniaWiadomosci::PELNY . '"');
    }

    public function testwiadomosciAction_ostatnieWiadomosci() {
	$wiadomosc = \FabrykaEncjiMockowych::makeEncje(\Konwersacje\Entity\Wiadomosc::class,
			PFMockowePowiazane::create($this->sm)
				->setparametry(array('watek' => $this->watekWiadomosci)));

	$this->wiadomosciMenager->expects($this->exactly(1))->method('getRekord')
		->willReturn($wiadomosc);

	$this->wiadomosci[] = $wiadomosc;

	$this->ustawMocki(2);
	$this->ustawMockUzytkownikWatekWiadomosci(2);
	$this->ustawMockPobieraniaNajnowszejWiadomosci();


	$this->dispatch("/konwersacje/wiadomosci?watek_id={$this->watekWiadomosci->id}&ostatnia_wiadomosc=42");
	$this->sprawdzCzySukces();
	$this->sprawdzOdpowiedzZawiera('"wiadomosc":{"id');
	$this->sprawdzOdpowiedzZawiera('"tryb":"' . \Konwersacje\Enumy\TrybOdswiezaniaWiadomosci::NOWE . '"');
    }

    public function testnajnowszeWiadomosciAction() {

	$this->najnowszeWiadomosciSprawdzanie(42, '?ostatnia_wiadomosc=42');

	$this->sprawdzCzySukces();
    }

    public function testnajnowszeWiadomosciAction_pusty_parametr() {
	$this->najnowszeWiadomosciSprawdzanie(0, '?ostatnia_wiadomosc=');
    }

    public function testnajnowszeWiadomosciAction_bez_parametru() {
	$this->najnowszeWiadomosciSprawdzanie(0, '');
    }

    private function najnowszeWiadomosciSprawdzanie($id, $url) {
	$this->dodajNowaWiadomosc();
	$this->wiadomosciMenager->expects($this->exactly(1))->method('pobierzNajnowszeWiadomosciZalogowanego')
		->willReturn($this->wiadomosci)
		->with($this->equalTo($id));

	$this->dispatch("/konwersacje/najnowsze_wiadomosci$url");
	$this->sprawdzOdpowiedzZawiera('"wiadomosci":[{"id":');
    }

    public function testdodajAction() {
	$this->wiadomosciMenager->expects($this->exactly(0))->method('zapiszZPosta');
	$this->dispatch("/konwersacje/dodaj");
	$this->sprawdzCzyFail();
    }

    public function testdodajAction_post() {
	$this->ustawParametryPosta(array('abc' => 'cde'));
	$this->wiadomosciMenager->expects($this->exactly(1))->method('zapiszZPosta');

	$this->dispatch("/konwersacje/dodaj");
	$this->sprawdzCzySukces();
    }

    public function test_searchAction() {


	$this->uzytkownikTable->expects($this->once())
		->method('pobierzResultSetZWherem')
		->willReturn(array())
		->with($this->equalTo('1 AND 1 '));
	$this->wiadomosciMenager->expects($this->once())
		->method('pobierzResultSetZWherem')
		->willReturn(array())
		->with($this->equalTo("1 AND id IN (select wiadomosc_id from uzytkownicy_wiadomosci where uzytkownik_id={$this->obslugaKlasObcych->zalogowany->id}) "));

	$this->dispatch("/konwersacje/search");
	$this->sprawdzCzySukces();
    }

    public function testwiadomosciDoNowegoOkienkaAction_prywatny_brak_wczesniejszej_rozmowy() {
	$this->dispatch("/konwersacje/wiadomosci_do_nowego_okienka?odbiorcaId=6&typWatku=prywatny&projektId=0&watekId=");
	$this->sprawdzCzySukces();
    }

    public function testwiadomosciDoNowegoOkienkaAction_prywatny_ustawiony_watek_wiadomosci() {
	$this->wiadomosciMenager->expects($this->once())->method('pobierzWiadomosciDlaWatku')
		->willReturn([TB::create(\Konwersacje\Entity\Wiadomosc::class, $this)->make()]);
	$this->dispatch("/konwersacje/wiadomosci_do_nowego_okienka?odbiorcaId=6&typWatku=prywatny&projektId=0&watekId=42");

	$this->sprawdzCzyOdpowiedzZawieraConajmniejJednaWiadomosc();
	$this->sprawdzCzySukces();
    }

    private function sprawdzCzyOdpowiedzZawieraConajmniejJednaWiadomosc() {
	$odpowiedz = $this->getResponse()->getBody();
	$odp=  json_decode($odpowiedz);

	$this->assertGreaterThan(0, count($odp->wiadomosci));
    }

    private function ustawMocki($ileRazyPobieranieWiadomosciDlaWatku = 3) {
	$this->watkiWiadomosciTable->expects($this->once())->method('getRekord')
		->willReturn($this->watekWiadomosci);

	$this->wiadomosciMenager->expects($this->exactly($ileRazyPobieranieWiadomosciDlaWatku))->method('pobierzWiadomosciDlaWatku')
		->willReturn($this->wiadomosci);
    }

    private function ustawMockUzytkownikWatekWiadomosci($ileRazy = 2) {
	$this->uzytkownikWatekWiadomosciTable->expects($this->exactly($ileRazy))->method('pobierzDlaWatku')
		->willReturn(array());
    }

    private function ustawMockPobieraniaNajnowszejWiadomosci() {

	$this->wiadomosciMenager->expects($this->exactly(1))->method('pobierzNajnowszeWiadomosciDlaWatku')
		->willReturn($this->wiadomosci);
    }

    private function dodajNowaWiadomosc() {
	$wiadomosc = \FabrykaEncjiMockowych::makeEncje(\Konwersacje\Entity\Wiadomosc::class,
			PFMockowePowiazane::create($this->sm));
	$this->wiadomosci[] = $wiadomosc;
    }

}
