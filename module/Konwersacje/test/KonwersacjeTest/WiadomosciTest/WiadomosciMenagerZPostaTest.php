<?php

namespace KonwersacjeTest\WiadomosciTest;

use Konwersacje\Entity\Wiadomosc;
use Konwersacje\Enumy\WatkiWiadomosciTypy;

require_once dirname(__FILE__) . '\WiadomosciCommonTest.php';

class WiadomosciMenagerZPostaTest extends WiadomosciCommonTest {

    public function setUp() {
	parent::setUp();
    }

    public function testZrobWiadomosc_z_nowym_watkiem() {
	$uzytkownik =$this->userBuilder->make();
	$this->post = array(
	    'tresc' => 'xcvbn',
	    'uzytkownicy' => '' . $uzytkownik->id
	);
	$this->dodajZPosta();

	$this->ustawZalogowanegoUsera($uzytkownik->id);

	$wiadomosci = $this->wiadomosciMenager->pobierzNajnowszeWiadomosciPrywatneZalogowanego(0);

	$this->assertEquals(1, count($wiadomosci));
	$this->assertEquals(2,
		count($wiadomosci[0]->watek->getuzytkownicyWatkiWiadomosci()));
	$this->assertEquals(2, count($wiadomosci[0]->getUzytkownicyWiadomosci()));

	$this->sprawdzTypWatku(WatkiWiadomosciTypy::PRYWATNY);
	$this->sprawdzCzyPustyWatekChatu();
    }

    public function testzapiszZPosta_blad_z_tworzeniem_projektowego_watku_chatu() {
	$this->zrobNowyZwyklyPost();
	$this->post['projekt_id'] = '0';

	$this->dodajZPosta();

	$this->sprawdzTypWatku(WatkiWiadomosciTypy::PRYWATNY);
	$this->sprawdzCzyPustyWatekChatu();
    }

    public function testzapiszZPosta_blad_watku_chatu() {
	$this->zrobNowyProjektowyPost();

	$this->dodajZPosta();

	$this->post['projektowy_watek_chatu_id'] = $this->wyniki[$this->i]->projektowy_watek_chatu->id;
	$this->post['watek_id'] = $this->wyniki[$this->i]->watek->id;
	$this->post['uzytkownicy'] = $this->user1->id;

	$this->dodajZPosta();
	$this->sprawdzCzyWatekChatuNowyJakStary();

	$nowyUser =$this->userBuilder->make();
	$this->post['uzytkownicy'] = $nowyUser->id;
	$this->dodajZPosta();
	$this->sprawdzCzyWatekChatuNowyJakStary();
    }

    public function testzapiszZPosta_blad_watku_chatu_otwieranie_nowej_wiadomosci() {
	$this->zrobNowyProjektowyPost();

	$this->dodajZPosta();

	$this->dodajZPosta();
	$this->sprawdzCzyWatekChatuNowyJakStary();

	$this->dodajZPosta();
	$this->sprawdzCzyWatekChatuNowyJakStary();
    }

    public function testzapiszZPosta_blad_z_dodawaniem_tego_samego_watku_chatu_dla_roznych_userow() {
	$this->zrobNowyProjektowyPost();

	$this->dodajZPosta();

	$nowyUser = $this->userBuilder->make();
	$this->post['uzytkownicy'] = $nowyUser->id;
	$this->dodajZPosta();
	$this->sprawdzCzyWatekChatuNowyRoznyOdStarego();
    }

}
