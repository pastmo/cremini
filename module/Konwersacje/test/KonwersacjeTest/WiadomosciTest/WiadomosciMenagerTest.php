<?php

namespace KonwersacjeTest\WiadomosciTest;

use Konwersacje\Entity\Wiadomosc;
use Konwersacje\Enumy\WatkiWiadomosciTypy;
use Pastmo\Testy\Util\TB;

require_once dirname(__FILE__) . '\WiadomosciCommonTest.php';

class WiadomosciMenagerTest extends WiadomosciCommonTest {

    public function setUp() {
	parent::setUp();
    }

    public function test_dodajRekord() {
	$this->ustawZalogowanegoUseraId1();

	$nazwa = 'test_dodajRekord';

	$this->wiadomosc->tresc = $nazwa;
	$this->wiadomosciMenager->zapisz($this->wiadomosc);

	$ostatniRekord = $this->wiadomosciMenager->getPoprzednioDodany();

	$this->assertEquals($nazwa, $ostatniRekord->tresc);
	$this->assertNotNull($ostatniRekord->data_dodania);
	$this->assertFalse($ostatniRekord->z_chatu);
	$this->assertEquals(1,
		count($ostatniRekord->watek->getuzytkownicyWatkiWiadomosci()));
    }

    public function test_dodajRekord_dodawanie_zalogowanego_do_uzytkownicy_wiadomosci() {
	$nazwa = 'test_dodajRekord';

	$this->wiadomosc->tresc = $nazwa;
	$this->wiadomosciMenager->zapisz($this->wiadomosc);

	$ostatniRekord = $this->wiadomosciMenager->getPoprzednioDodany();

	$uzytkownicyWiadomosciMenager = $this->sm->get(\Konwersacje\Menager\UzytkownicyWiadomosciMenager::class);

	$uzytkownicyWiadomosci = $uzytkownicyWiadomosciMenager->pobierzDlaWiadomosci($ostatniRekord->id);

	$this->assertEquals(1, count($uzytkownicyWiadomosci));
	$this->assertEquals(1, $uzytkownicyWiadomosci[0]->uzytkownik->id);
    }

    public function test_dodajRekord_dodawanie_zalogowanego_do_uzytkownicy_wiadomosci_inny_autor() {

	$this->wiadomosc->tresc = __METHOD__;
	$this->utworzIZalogujUzytkownikow();
	$this->wiadomosc->autor = $this->user2;

	$this->wiadomosciMenager->zapisz($this->wiadomosc);

	$ostatniRekord = $this->wiadomosciMenager->getPoprzednioDodany();

	$uzytkownicyWiadomosciMenager = $this->sm->get(\Konwersacje\Menager\UzytkownicyWiadomosciMenager::class);

	$uzytkownicyWiadomosci = $uzytkownicyWiadomosciMenager->pobierzDlaWiadomosci($ostatniRekord->id);

	$this->assertEquals(2, count($uzytkownicyWiadomosci));
    }

    public function test_dodajRekord_autor() {
	$nazwa = 'test_dodajRekord';

	$user = TB::create(\Logowanie\Model\Uzytkownik::class, $this)->make();

	$encja = new Wiadomosc();
	$encja->tresc = $nazwa;
	$encja->autor = $user;
	$encja->z_chatu = true;
	$this->wiadomosciMenager->zapisz($encja);

	$ostatniRekord = $this->wiadomosciMenager->getPoprzednioDodany();

	$this->assertEquals($ostatniRekord->autor->login, $user->login);
	$this->assertTrue($ostatniRekord->z_chatu > 0);
    }

    public function test_dodajRekord_tworzenie_watku() {

	$this->utworzIZalogujUzytkownikow();

	$this->zrobWiadomsc();

	$this->wiadomosc->autor = $this->user1;

	$this->wiadomosc->getBuilder()->dodajUsera($this->user2);

	$this->wiadomosciMenager->zapisz($this->wiadomosc);

	$ostatniRekord = $this->wiadomosciMenager->getPoprzednioDodany();

	$this->assertEquals($this->temat, $ostatniRekord->watek->temat);
	$this->assertEquals(count($ostatniRekord->watek->getuzytkownicyWatkiWiadomosci()),
		2);
    }

    public function test_dodajRekord_tworzenie_watku_autor_wczesniej_dodany() {
	$this->utworzIZalogujUzytkownikow();

	$this->wiadomosc->autor = $this->user1;
	$this->wiadomosc->getBuilder()->dodajUsera($this->user1);
	$this->wiadomosc->getBuilder()->dodajUsera($this->user2);

	$this->wiadomosciMenager->zapisz($this->wiadomosc);

	$ostatniRekord = $this->wiadomosciMenager->getPoprzednioDodany();

	$this->assertEquals("", $ostatniRekord->watek->temat);
	$this->assertEquals(2,
		count($ostatniRekord->watek->getuzytkownicyWatkiWiadomosci()));
	$this->assertEquals(2, count($ostatniRekord->getUzytkownicyWiadomosci()));
    }

    public function test_dodajRekord_dodawanie_watku() {
	$watek = \FabrykaRekordow::utworzEncje($this->sm,
			\Konwersacje\Entity\WatekWiadomosci::getClass());

	$this->wiadomosc->getBuilder()->setTematWatkuWiadomosci($this->temat);
	$this->wiadomosc->watek = $watek;

	$this->wiadomosciMenager->zapisz($this->wiadomosc);

	$ostatniRekord = $this->wiadomosciMenager->getPoprzednioDodany();

	$this->assertNotEquals($ostatniRekord->watek->temat, $this->temat);
    }

    public function test_kopiowanie_uzytkownikow_z_watkow_chatu_do_uzytkownicy_wiadomosci() {
	$this->utworzWatekChatu();

	$this->wiadomosc->projektowy_watek_chatu = $this->fabrykaWatkuChatu->projektowyWatekChatu;

	$this->wiadomosciMenager->zapisz($this->wiadomosc);

	$ostatniRekord = $this->wiadomosciMenager->getPoprzednioDodany();

	$uzytkownicyWiadomosci = $ostatniRekord->getUzytkownicyWiadomosci();

	$this->assertEquals(2, count($uzytkownicyWiadomosci));
    }

    public function test_pobierzNajnowszeWiadomosciDlaWatku() {
	$this->watekFactory->utworzWatekZWiadomosciamiIPowiazZZalogowanymInowymUzytownikiem(3);

	$wynik = $this->wiadomosciMenager->pobierzNajnowszeWiadomosciDlaWatku($this->watekFactory->watek->id,
		$this->watekFactory->wiadomosci[1]->id);

	$this->assertEquals(1, count($wynik));
	$this->assertEquals($this->watekFactory->wiadomosci[2]->id, $wynik[0]->id);
    }

    public function test_pobierzNajnowszeWiadomosciPrywatneZalogowanego() {
	$this->utworzIZalogujUzytkownikow();
	$this->watekFactory->utworzWatekZWiadomosciamiIPowiazZZalogowanymInowymUzytownikiem(3);

	$this->ustawZalogowanegoUsera($this->user2->id);

	$this->wiadomoscFactory->utworzWiadomoscZNowymUzytkownikiem();

	$this->ustawZalogowanegoUsera($this->user1->id);
	$wynik = $this->wiadomosciMenager->pobierzNajnowszeWiadomosciPrywatneZalogowanego($this->watekFactory->wiadomosci[1]->id);

	$this->assertEquals(1, count($wynik));
	$this->assertEquals($this->watekFactory->wiadomosci[2]->id, $wynik[0]->id);
    }

    public function test_pobierzNajnowszeWiadomosciProjektoweZalogowanego() {
	$this->watekFactory->utworzWatekZWiadomosciamiIPowiazZZalogowanymInowymUzytownikiem(3,
		WatkiWiadomosciTypy::PROJEKTOWY);

	$wynik = $this->wiadomosciMenager->pobierzNajnowszeWiadomosciProjektoweZalogowanego($this->watekFactory->wiadomosci[1]->id);

	$this->assertEquals(1, count($wynik));
	$this->assertEquals($this->watekFactory->wiadomosci[2]->id, $wynik[0]->id);
    }

    public function test_pobierzNajnowszeWiadomosciProjektoweZalogowanego_z_chatu() {
	$this->watekFactory->wiadomosci_z_chatu = array(0, 1);

	$this->watekFactory->utworzWatekZWiadomosciamiIPowiazZZalogowanymInowymUzytownikiem(3,
		WatkiWiadomosciTypy::PROJEKTOWY);

	$wynik = $this->wiadomosciMenager->pobierzNajnowszeWiadomosciProjektoweZalogowanego($this->watekFactory->wiadomosci[0]->id,
		true);

	$this->assertEquals(1, count($wynik));
	$this->assertEquals($this->watekFactory->wiadomosci[1]->id, $wynik[0]->id);
    }

    public function test_pobierzNajnowszePrywatneWiadomosciCzystegoUzytkownika() {

	$uzytkownik = $this->userBuilder->make();
	$this->ustawZalogowanegoUsera($uzytkownik->id);

	$wynik = $this->wiadomosciMenager->pobierzNajnowszeWiadomosciPrywatneZalogowanego(0);

	$this->assertEquals(0, count($wynik));
    }

    public function test_pobierzOstatniaWiadomoscZalogowanego() {
	$this->watekFactory->utworzWatekZWiadomosciamiIPowiazZZalogowanymInowymUzytownikiem(1);

	$this->watekFactory->utworzWatekZWiadomosciamiIPowiazZZalogowanymInowymUzytownikiem(1,
		WatkiWiadomosciTypy::PROJEKTOWY);

	$wynik = $this->wiadomosciMenager->pobierzOstatniaWiadomoscZalogowanego();


	$this->assertEquals($this->watekFactory->wiadomosci[1]->id, $wynik->id);
    }

    public function testzapiszWatekWiadomosci_bez_projektu() {
	$this->zrobWiadomsc();

	$watek = $this->wiadomosciMenager->zapiszWatekWiadomosci($this->wiadomosc);

	$this->assertEquals($this->temat, $watek->temat);
	$this->assertEquals(WatkiWiadomosciTypy::PRYWATNY, $watek->typ);
    }

    public function testzapiszWatekWiadomosci_z_projektem() {
	$this->zrobWiadomsc();
	$this->zrobProjekt();

	$this->wiadomosc->getBuilder()->setProjektId($this->projekt->id);

	$this->wynikowyWatek = $this->wiadomosciMenager->zapiszWatekWiadomosci($this->wiadomosc);

	$this->sprawdzWatekWiadomosciZProjektem();
	$this->sprwadzProjektyWatkiWiadomosci();
    }

    public function testzapiszWatekWiadomosci_z_projektem_blad_z_podwojnyn_tworzeniem_watku() {
	$this->zrobWiadomsc();
	$this->zrobProjekt();

	$this->wiadomosc->getBuilder()->setProjektId($this->projekt->id);

	$watek = $this->wiadomosciMenager->zapiszWatekWiadomosci($this->wiadomosc);

	$this->assertEquals($this->projekt->watek_wiadomosci->id, $watek->id);
    }

}
