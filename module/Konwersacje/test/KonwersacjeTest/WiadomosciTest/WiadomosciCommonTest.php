<?php

namespace KonwersacjeTest\WiadomosciTest;

use Konwersacje\Entity\Wiadomosc;
use Konwersacje\Enumy\WatkiWiadomosciTypy;
use Pastmo\Testy\Util\TB;

class WiadomosciCommonTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    protected $wiadomosciMenager;
    protected $projektyWatkiWiadomoscMenager;
    protected $projektoweWatkiChatuMenager;
    protected $wiadomoscFactory;
    protected $watekFactory;
    protected $temat = 'testowy temat';
    protected $wiadomosc;
    protected $post;
    protected $user1;
    protected $user2;
    protected $wyniki;
    protected $i;
    protected $poprz;
    protected $wynikowyWatek;
    protected $dodanyProjekt;
    protected $fabrykaWatkuChatu;
    protected $projekt;
    protected $userBuilder;

    public function setUp() {
	parent::setUp();

	$this->wiadomosciMenager = $this->sm->get(\Konwersacje\Model\WiadomosciMenager::class);
	$this->projektyWatkiWiadomoscMenager = $this->sm->get(\Konwersacje\Menager\ProjektyWatkiWiadomoscMenager::class);
	$this->projektoweWatkiChatuMenager = $this->sm->get(\Konwersacje\Menager\ProjektoweWatkiChatuMenager::class);

	$this->ustawZalogowanegoUseraId1();
	$this->wiadomoscFactory = \WiadomoscFactory::create(\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm));
	$this->watekFactory = \WatekWiadomosciFactory::create(\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm));

	$this->post = array(
	    'projekt_id' => '',
	    'projektowy_watek_chatu_id' => '',
	    'tresc' => 'Wiadomość z chatu' . __CLASS__,
	    'uzytkownicy' => '',
	    'watek_id' => '',
	    'z_chatu' => '1');

	$this->wyniki = array();
	$this->i = -1;
	$this->wiadomosc = new Wiadomosc();

	$this->userBuilder= TB::create(\Logowanie\Model\Uzytkownik::class, $this);
    }

    protected function utworzWatekChatu() {
	$this->fabrykaWatkuChatu = \Testy\FabrykiKonkretnychEncji\ProjektowyWatekChatuFactory::create(\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->sm));
	$this->fabrykaWatkuChatu->utworzPojedynczyWatekChatu();
	$this->fabrykaWatkuChatu->dodajWatekChatuUzytkownika($this->fabrykaWatkuChatu->projektowyWatekChatu,
		1);
    }

    protected function sprawdzWatekWiadomosciZProjektem() {
	$this->assertEquals($this->projekt->watek_wiadomosci->temat,
		$this->wynikowyWatek->temat);
	$this->assertEquals(WatkiWiadomosciTypy::PROJEKTOWY, $this->wynikowyWatek->typ);
    }

    protected function sprwadzProjektyWatkiWiadomosci() {
	$this->dodanyProjekt = $this->projektyWatkiWiadomoscMenager->getPoprzednioDodany();

	$this->assertNotNull($this->dodanyProjekt);
	$this->assertNotNull($this->dodanyProjekt->watek_wiadomosci->id);
    }

    protected function sprawdzDodanieWatkiChatuDlaDodanegoWatku() {
	$projektoweWatkiChatu = $this->projektoweWatkiChatuMenager->pobierzDlaWatkuWiadomosci($this->dodanyProjekt->watek_wiadomosci->id);

	$this->assertEquals(1, count($projektoweWatkiChatu));
    }

    protected function dodajZPosta() {
	$this->wiadomosciMenager->zapiszZPosta($this->post);
	$this->wyniki[] = $this->wiadomosciMenager->getPoprzednioDodany();

	$this->poprz = $this->i;
	$this->i++;
    }

    protected function sprawdzCzyWatekChatuNowyJakStary() {
	$this->assertEquals($this->wyniki[$this->poprz]->projektowy_watek_chatu->id,
		$this->wyniki[$this->i]->projektowy_watek_chatu->id);
    }

    protected function sprawdzCzyWatekChatuNowyRoznyOdStarego() {
	$this->assertNotEquals($this->wyniki[$this->poprz]->projektowy_watek_chatu->id,
		$this->wyniki[$this->i]->projektowy_watek_chatu->id);
    }

    protected function sprawdzTypWatku($typ) {
	$this->assertEquals($typ, $this->wyniki[$this->i]->watek->typ);
    }

    protected function sprawdzCzyPustyWatekChatu() {
	$this->assertNull(\Pastmo\Wspolne\Utils\EntityUtil::wydobadzId($this->wyniki[$this->i]->projektowy_watek_chatu));
    }

    protected function zrobWiadomsc() {
	$tresc = 'tresc wiadomosci';

	$this->wiadomosc->getBuilder()->setTematWatkuWiadomosci($this->temat);

	$this->wiadomosc->tresc = $tresc;
    }

    protected function zrobProjekt($czy_watek = true) {
	$pf = \Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane::create($this->sm);

	if (!$czy_watek) {
	    $pf->setparametry(array('watek_wiadomosci' => null));
	}

	$this->projekt = \FabrykaRekordow::makeEncje(\Projekty\Entity\Projekt::class,
			$pf)
	;
    }

    protected function utworzIZalogujUzytkownikow() {
	$this->user1 = $this->userBuilder->make();
	$this->user2 = $this->userBuilder->make();

	$this->ustawZalogowanegoUsera($this->user1->id);
    }

    protected function utworzUzytkownika() {
	$uzytkownik = $this->userBuilder->make();
	return $uzytkownik;
    }

    protected function zrobNowyProjektowyPost() {
	$this->zrobNowyZwyklyPost();

	$this->zrobProjekt(false);
	$this->post['projekt_id'] = $this->projekt->id;
    }

    protected function zrobNowyZwyklyPost() {
	$this->utworzIZalogujUzytkownikow();

	$this->post['uzytkownicy'] = $this->user2->id;
    }

}
