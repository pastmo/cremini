<?php

namespace KonwersacjeTest\Model;

use Konwersacje\Entity\Wiadomosc;
use Konwersacje\Model\WiadomosciMenager;
use Pastmo\Testy\Util\TB;

class WiadomosciMenagerTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    private $table;

    const form = array(
	"uzytkownicy" => ["2", "1"],
	"temat" => "Temat",
	"editor_textarea" => "\r\n\t\tTreść<br>",
	"submit" => "Wyślij");

    public function setUp() {
	parent::setUp();

	$this->ustawMockaGateway(\Konwersacje\Module::WIADOMOSCI_GATEWAY);

	$this->table = new WiadomosciMenager($this->sm);
	$uzytkownikBuilder = TB::create(\Logowanie\Model\Uzytkownik::class, $this);

	$this->uzytkownikTable->expects($this->any())->method('getRekord')
		->will($this->onConsecutiveCalls($uzytkownikBuilder->make(),
				$uzytkownikBuilder->make()));

	$this->obslugaKlasObcych->ustawZalogowanegoUsera();
    }

    public function testZrobWiadomosc() {
	$wiadomosc = $this->table->ZrobWiadomosc(self::form);

	$this->assertTrue($wiadomosc instanceof \Konwersacje\Entity\Wiadomosc);
    }

    public function testZrobWiadomosc_temat() {
	$expected = 'Temat';
	$wiadomosc = $this->table->ZrobWiadomosc(self::form);

	$this->assertEquals($expected,
		$wiadomosc->getBuilder()->getTematWatkuWiadomosci());
    }

    public function testZrobWiadomosc_uzytkownicy() {
	$wiadomosc = $this->table->ZrobWiadomosc(self::form);

	$this->assertEquals(2, count($wiadomosc->getBuilder()->pobierzUserow()));
    }

    public function testZrobWiadomosc_uzytkownicy_pojedynczy_id() {

	$form = self::form;
	$form['uzytkownicy'] = '42';
	$wiadomosc = $this->table->ZrobWiadomosc($form);

	$this->assertEquals(1, count($wiadomosc->getBuilder()->pobierzUserow()));
    }

    public function testZrobWiadomosc_ustawionyProjektId() {

	$form = self::form;
	$form['projekt_id'] = '42';
	$wiadomosc = $this->table->ZrobWiadomosc($form);

	$this->assertEquals('42', $wiadomosc->getBuilder()->getProjektId());
    }

    public function testZrobWiadomosc_z_chatu_pusty() {
	$wiadomosc = $this->table->ZrobWiadomosc(self::form);

	$this->assertEquals(0, $wiadomosc->z_chatu);
    }

    public function testZrobWiadomosc_z_chatu_ustawiony_1() {
	$form = self::form;
	$form['z_chatu'] = '1';
	$wiadomosc = $this->table->ZrobWiadomosc($form);

	$this->assertEquals(1, $wiadomosc->z_chatu);
    }

    public function testZrobWiadomosc_z_chatu_ustawiony_0() {
	$form = self::form;
	$form['z_chatu'] = '0';
	$wiadomosc = $this->table->ZrobWiadomosc($form);

	$this->assertEquals(0, $wiadomosc->z_chatu);
    }

    public function testpobierzNajnowszeWiadomosciZalogowanego() {

	$this->obslugaTestowanejKlasy->ustawMockiDoPobierzZWherem(array(), 2);
	$wynik = $this->table->pobierzNajnowszeWiadomosciZalogowanego(0);
	$this->assertEquals(array(), $wynik);
    }

    public function testpobierzOstatniaWiadomoscZalogowanego_projektowa() {
	$this->wykonajSprawdzanieOstatniejWiadomosci(array(),
		array($this->zrobWiadomosc(4)), 4);
    }

    public function testpobierzOstatniaWiadomoscZalogowanego_prywatna() {
	$this->wykonajSprawdzanieOstatniejWiadomosci(array($this->zrobWiadomosc(42)),
		array(), 42);
    }

    public function testpobierzOstatniaWiadomoscZalogowanego_obie() {
	$this->wykonajSprawdzanieOstatniejWiadomosci(array($this->zrobWiadomosc(42)),
		array($this->zrobWiadomosc(23)), 42);
    }

    private function wykonajSprawdzanieOstatniejWiadomosci($prywatne,
	    $projektowe, $expectedId) {
	$builder = $this->obslugaTestowanejKlasy->ustawMockiDoPobierzZWherem(array(), 2);
	$builder->will($this->onConsecutiveCalls($prywatne, $projektowe));

	$wynik = $this->table->pobierzOstatniaWiadomoscZalogowanego(0);
	$this->assertEquals($expectedId, $wynik->id);
    }

    private function zrobWiadomosc($id) {
	$wiadomosc = Wiadomosc::create();
	$wiadomosc->id = $id;
	return $wiadomosc;
    }

    protected function nowyObiekt() {
	return Wiadomosc();
    }

}
