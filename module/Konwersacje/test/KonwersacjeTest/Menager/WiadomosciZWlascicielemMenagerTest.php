<?php

namespace KonwersacjeTest\Table;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Konwersacje\Entity\UzytkownikWatkekWiadomosci;
use \Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane;
use Pastmo\Testy\Util\TB;

class WiadomosciZWlascicielemMenagerTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    private $projekt;
    private $wycena;
    private $trescWiadomosci = "Treść wiadomości";
    private $post;
    private $wiadomoscDodana;

    public function setUp() {
	parent::setUp();

	$this->ustawMockaGateway(\Projekty\Module::PROJEKTY_GATEWAY);

	$this->glowneTable = new \Konwersacje\Menager\WiadomosciZWlascicielemMenager($this->sm);


	$this->obslugaKlasObcych->ustawZalogowanegoUsera();

	$this->ustawProjekt();
	$this->ustawWycene();
	$this->utworzPost();
	$this->wiadomoscDodana = TB::create(\Konwersacje\Entity\Wiadomosc::class, $this)
		->addOneToMany('watek', TB::create(\Konwersacje\Entity\WatekWiadomosci::class,$this))
		->make();
	$this->obslugaKlasObcych->ustawMetode(
		\Konwersacje\Model\WiadomosciMenager::class, 'getPoprzednioDodany',
		$this->wiadomoscDodana);

    }

    public function test_dodajWiadomoscDoWlasciciela() {

	$this->ustawPustyWinikZapisywaniaZasobow();

	$this->sprawdzDodanieWiadomosci();

	$this->glowneTable->dodajWiadomoscDoWlascicielaZPosta($this->post);
    }

    public function test_dodajWiadomoscDoWyceny() {

	$this->ustawTypWatkuWPoscie(\Konwersacje\Enumy\WatkiWiadomosciTypy::Z_WYCEN);
	$this->ustawPustyWinikZapisywaniaZasobow();

	$this->sprawdzDodanieWiadomosci();

	$this->glowneTable->dodajWiadomoscDoWlascicielaZPosta($this->post);
    }

    public function test_dodajWiadomoscDoWlasciciela_zalaczniki_z_mechanizmu_uploadu() {

	$this->ustawPustyWinikZapisywaniaZasobow();

	$this->post['files'] = '["4","42"]';

	$this->sprawdzDodanieDoWiadomosciZasoby();

	$this->glowneTable->dodajWiadomoscDoWlascicielaZPosta($this->post);
    }

    public function test_dodajWiadomoscDoWlasciciela_udostepnianie() {

	$this->ustawPustyWinikZapisywaniaZasobow();
	$this->dodajDodatkowychUzytkownikowDoPosta();


	$this->glowneTable->dodajWiadomoscDoWlascicielaZPosta($this->post);
    }

    public function test_dodajWiadomoscDoWlasciciela_zasoby_w_parametrach_posta() {

	$this->ustawPustyWinikZapisywaniaZasobow();
	$this->dodajZasobyDoPosta();

	$this->sprawdzDodanieDoWiadomosciZasoby();

	$this->glowneTable->dodajWiadomoscDoWlascicielaZPosta($this->post);
    }

    public function test_dodajWiadomoscDoWlasciciela_zasoby() {

	$this->ustawDwaZasobyJakoWynikZapisywaniaZasobow();

	$this->sprawdzDodanieDoWiadomosciZasoby();

	$this->glowneTable->dodajWiadomoscDoWlascicielaZPosta($this->post);
    }

    private function ustawProjekt() {
	$this->projekt = \FabrykaEncjiMockowych::utworzEncje(\Projekty\Entity\Projekt::class);
	$this->obslugaKlasObcych->ustawGetRecord(\Projekty\Menager\ProjektyMenager::class,
		$this->projekt);
    }

    private function ustawWycene() {
	$this->wycena = $this->makeEntityBuilder(\Aukcje\Entity\WycenaProducenta::class)->make();
	$this->obslugaKlasObcych->ustawGetRecord(\Aukcje\Menager\WycenyProducentowMenager::class,
		$this->wycena);
    }

    private function utworzPost() {
	$this->post = array(
	    'project_id' => $this->projekt->id,
	    'content' => $this->trescWiadomosci,
	    'typ' => \Konwersacje\Enumy\WatkiWiadomosciTypy::PROJEKTOWY
	);
    }

    private function ustawTypWatkuWPoscie($typ) {
	$this->post['typ'] = $typ;
    }

    private function dodajDodatkowychUzytkownikowDoPosta() {
	$uzytkownik = \FabrykaEncjiMockowych::makeEncje(\Logowanie\Model\Uzytkownik::class,
			\Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane::create());
	$this->post['visible_to'] = array($uzytkownik->id);
    }

    private function dodajZasobyDoPosta() {
	$uzytkownik = \FabrykaEncjiMockowych::makeEncje(\Logowanie\Model\Uzytkownik::class,
			\Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane::create());
	$this->post['files'] = array(16, '22');
    }

    private function ustawPustyWinikZapisywaniaZasobow() {
	$this->ustawWynikZapisywaniaZasobow(array());
    }

    private function ustawDwaZasobyJakoWynikZapisywaniaZasobow() {
	$zasob1 = \FabrykaEncjiMockowych::utworzEncje(\Zasoby\Model\Zasob::class);
	$zasob2 = \FabrykaEncjiMockowych::utworzEncje(\Zasoby\Model\Zasob::class);

	$this->ustawWynikZapisywaniaZasobow(array($zasob1, $zasob2));
    }

    private function ustawWynikZapisywaniaZasobow($wynik) {
	$this->zasobyUploadMenager->expects($this->once())->method('zaladujPlik')->willReturn($wynik);
    }

    private function sprawdzDodanieWiadomosci() {
	$this->wiadomosciMenager->expects($this->exactly(1))->method('zapisz');
    }

    private function sprawdzDodanieDoWiadomosciZasoby() {
	$this->wiadomosciZasobyMenager->expects($this->exactly(2))->method('zapisz');
    }

    protected function nowyObiekt() {

    }

}
