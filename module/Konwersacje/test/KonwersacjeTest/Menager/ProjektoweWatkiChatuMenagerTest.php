<?php

namespace KonwersacjeTest\Model;

use Konwersacje\Entity\Wiadomosc;
use Konwersacje\Model\WiadomosciMenager;
use \Konwersacje\Enumy\WatkiWiadomosciTypy;

class ProjektoweWatkiChatuMenagerTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    private $wiadomosc;
    private $watkiChatuMenager;

    public function setUp() {
	parent::setUp();
	$this->watkiChatuMenager = new \Konwersacje\Menager\ProjektoweWatkiChatuMenager($this->sm);
	$this->zrobWiadomosc(42);
    }

    public function test_czyProjektoweWatkiChatu() {
	$this->sprwadzCzyProjektoweWatkiChatu('1', FALSE, FALSE);
    }

    public function test_czyProjektoweWatkiChatu_prywatny() {
	$this->sprwadzCzyProjektoweWatkiChatu('1', WatkiWiadomosciTypy::PRYWATNY, FALSE);
    }

    public function test_czyProjektoweWatkiChatu_projektowy() {
	$this->sprwadzCzyProjektoweWatkiChatu('1', WatkiWiadomosciTypy::PROJEKTOWY,
		true);
    }

    private function sprwadzCzyProjektoweWatkiChatu($zChatu, $typ, $czyWynikTrue) {
	$this->wiadomosc->z_chatu = $zChatu;

	if ($typ) {
	    $this->zrobWatek($typ);
	}

	$this->assertEquals($czyWynikTrue,
		$this->watkiChatuMenager->czyProjektoweWatkiChatu($this->wiadomosc));
    }

    private function zrobWiadomosc($id) {
	$this->wiadomosc = Wiadomosc::create();
	$this->wiadomosc->id = $id;
    }

    private function zrobWatek($typ) {
	$this->wiadomosc->watek = \Konwersacje\Entity\WatekWiadomosci::create();
	$this->wiadomosc->watek->id = 55;
	$this->wiadomosc->watek->typ = $typ;
    }

}
