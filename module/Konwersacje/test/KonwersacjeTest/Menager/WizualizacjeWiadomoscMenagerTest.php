<?php

namespace KonwersacjeTest\Table;

class WizualizacjeWiadomoscMenagerTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

	public $gateway;

	public function setUp() {
		parent::setUp();

		$this->ustawMockaGateway(\Konwersacje\Module::WIZUALIZACJE_WIADOMOSCI_GATEWAY);

		$this->glowneTable = new \Konwersacje\Menager\WizualizacjeWiadomosciMenager($this->sm);
	}

	public function test_dodajKomentarzDoWizualizacji() {

		$this->projektyWatkiWiadomoscMenager->expects($this->exactly(2))->method('dodajWiadomoscDoProjektu');


		$wiadomosc = \FabrykaEncjiMockowych::utworzEncje(\Konwersacje\Entity\Wiadomosc::class);
		$this->obslugaKlasObcych->ustawMetode(\Konwersacje\Model\WiadomosciMenager::class,
				'getPoprzednioDodany', $wiadomosc,2);

		$post = array(
			'54' => "komentarz1",
			'42' => "komentarz2",
			'project_id' => '4'
		);

		$this->glowneTable->dodajKomentarzDoWizualizacji($post);
	}

	protected function nowyObiekt() {

	}

}
