<?php

namespace Konwersacje\KomunikatyDoJs;

use Konwersacje\Menager\UzytkownicyWiadomosciMenager;
use Pastmo\Testy\Util\TB;

class WiadomoscKomunikatTest extends \Testy\BazoweKlasyTestow\AbstractMockTest {

    private $wiadomosc;
    private $projektowyWatekChatu;
    private $wiadomoscUzytkownik;
    private $uzytkownik;

    public function setUp() {
	parent::setUp();

	$this->wiadomoscUzytkownik = \FabrykaEncjiMockowych::makeEncje(\Konwersacje\Entity\UzytkownikWiadomosc::class,
			\Pastmo\Testy\ParametryFabryki\PFMockowePowiazane::create());

	$this->uzytkownik = \FabrykaEncjiMockowych::makeEncje(\Logowanie\Model\Uzytkownik::class,
			\Pastmo\Testy\ParametryFabryki\PFMockowePowiazane::create()
				->setparametry(array('imie' => 'Imię', 'nazwisko' => 'Nazwisko')));

	$this->obslugaKlasObcych->ustawZalogowanegoUsera();
    }

    public function test_fromWiadomosc_podstawoweDane() {
	$this->ustawUzytkownicyWiadomosci();
	$this->zrobWiadomosc();
	$komunikat = WiadomoscKomunikat::fromWiadomosc($this->wiadomosc);

	$this->assertEquals($this->wiadomosc->tresc, $komunikat->body);
	$this->assertEquals($this->wiadomoscUzytkownik->uzytkownik->id, $komunikat->sender->id);
	$this->assertEquals(0, $komunikat->projektowyWatekWiadomosciId);
    }

    public function test_fromWiadomosc_typPrywatny() {
	$this->ustawUzytkownicyWiadomosci();
	$this->zrobWiadomosc();

	$komunikat = WiadomoscKomunikat::fromWiadomosc($this->wiadomosc);

	$this->assertEquals(0, $komunikat->projectId);
	$this->assertEquals(0, $komunikat->projektowyWatekWiadomosciId);
    }

    public function test_fromWiadomosc_typPrywatny_bez_watku() {
	$this->ustawUzytkownicyWiadomosci();
	$this->zrobWiadomosc();

	$this->wiadomosc->watek = null;

	$komunikat = WiadomoscKomunikat::fromWiadomosc($this->wiadomosc);

	$this->assertEquals(0, $komunikat->projectId);
	$this->assertEquals(0, $komunikat->projektowyWatekWiadomosciId);
    }

    public function test_fromWiadomosc_typProjektowy() {
	$this->ustawUzytkownicyWiadomosci(1);
	$this->zrobWiadomoscProjektowa(1);
	$komunikat = WiadomoscKomunikat::fromWiadomosc($this->wiadomosc);

	$this->assertEquals($this->projekt->id, $komunikat->projectId);
	$this->assertEquals($this->projekt->nazwa, $komunikat->projectName);
	$this->assertEquals(0, $komunikat->projektowyWatekWiadomosciId);
    }

    public function test_fromWiadomosc_typProjektowy_z_chatu() {
	$this->ustawUzytkownicyWiadomosci(1);
	$this->zrobWiadomoscProjektowaZChatu(1);
	$komunikat = WiadomoscKomunikat::fromWiadomosc($this->wiadomosc);

	$this->assertEquals($this->projekt->id, $komunikat->projectId);
	$this->assertEquals($this->projekt->nazwa, $komunikat->projectName);
	$this->assertEquals($this->projektowyWatekChatu->id, $komunikat->projektowyWatekWiadomosciId);
    }

    public function test_fromUzytkownik() {
	$komunikat = WiadomoscKomunikat::fromUzytkownik($this->uzytkownik);

	$this->assertEquals($this->uzytkownik->id, $komunikat->sender->id);
	$this->assertEquals($this->uzytkownik . '', $komunikat->sender->name);
    }

    public function test_listaAdapter() {
	$this->ustawUzytkownicyWiadomosci();
	$this->zrobWiadomosc();

	$wynik = WiadomoscKomunikat::listaAdapter(array($this->uzytkownik, $this->wiadomosc));

	$this->assertEquals(2, count($wynik));
    }

    private function zrobWiadomosc() {
	$this->wiadomosc = TB::create(\Konwersacje\Entity\Wiadomosc::class, $this)->setPF_MP($this->sm)->make();
	;
    }

    private function zrobWiadomoscProjektowa($ileRazyPobierana = 1) {

	$this->zrobWiadomosc();
	$this->projekt = \FabrykaEncjiMockowych::makeEncje(\Projekty\Entity\Projekt::class,
			\Pastmo\Testy\ParametryFabryki\PFMockowePowiazane::create($this->sm));

	$this->wiadomosc->watek->typ = \Konwersacje\Enumy\WatkiWiadomosciTypy::PROJEKTOWY;

	$this->projektyMenager->expects($this->exactly($ileRazyPobierana))->method('getPoWatekId')->willReturn($this->projekt);
    }

    private function zrobWiadomoscProjektowaZChatu($ileRazyPobierana = 1) {
	$this->zrobWiadomoscProjektowa($ileRazyPobierana);

	$this->projektowyWatekChatu = \FabrykaEncjiMOckowych::makeEncje(\Konwersacje\Entity\ProjektowyWatekChatu::class
			, \Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane::create());
	$this->wiadomosc->projektowy_watek_chatu = $this->projektowyWatekChatu;
	$this->wiadomosc->z_chatu = '1';
    }

    private function ustawUzytkownicyWiadomosci($ileRazy = 1) {
	$this->obslugaKlasObcych->ustawMetode(UzytkownicyWiadomosciMenager::class, 'pobierzDlaWiadomosci',
		array($this->wiadomoscUzytkownik), $ileRazy);
    }

}
