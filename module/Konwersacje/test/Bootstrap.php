<?php
namespace KonwersacjeTest;

require_once dirname(__FILE__) . '/../../Wspolne/test/WspolneBootstrap.php';

chdir(__DIR__);

class Bootstrap extends \WspolneTest\WspolneBootstrap {

}

Bootstrap::init(array(
	'Application','Projekty','Wspolne','Konwersacje','Logowanie','Zasoby','Pastmo'
));
Bootstrap::chroot();
