<?php
return array(
     'controllers' => array(
         'invokables' => array(
             'Menu\Controller\Menu' => 'Menu\Controller\MenuController',
		),
     ),
	  'router' => array(
         'routes' => array(
             'zasoby' => array(
				'type'    => 'segment',
                 'options' => array(
                     'route' => '/zasoby[/:action][/:id]',
					'constraints' => array(
                         'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                         'id'     => '[0-9]+',
                     ),
                     'defaults' => array(
                         'controller' => 'Zasoby\Controller\Zasoby',
						'action'     => 'index',
                     ),
                 ),
             ),
         ),
     ),

     'view_manager' => array(
         'template_path_stack' => array(
             'Menu' => __DIR__ . '/../view',
		),
     ),
 );