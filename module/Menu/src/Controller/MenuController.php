<?php

namespace Menu\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class MenuController extends AbstractActionController {

	protected $zasobyTable;

	public function indexAction() {
		if (!$this->getUzytkownicyMenager()->getZalogowanegoUsera()) {
			return $this->redirect()->toRoute('logowanie', array('action' => 'index'));
		}
		return new ViewModel(array(
		));
	}

	public static function wyswietlMenu() {
		return 'menu/menu/admin_menu.phtml';
	}

}
