<?php

namespace Ustawienia;

return array(
	'controllers' => array(
		'factories' => array(
			'Ustawienia\Controller\Ustawienia' => Controller\Factory\Factory::class,
		),
	),
	'router' => array(
		'routes' => array(
			'ustawienia' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/ustawienia[/:action][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id' => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Ustawienia\Controller\Ustawienia',
						'action' => 'index',
					),
				),
			),

		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Ustawienia' => __DIR__ . '/../view',
		),
	),
);
