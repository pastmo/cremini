<?php

namespace Ustawienia\Controller\Factory;

use Ustawienia\Controller\UstawieniaController;

class Factory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

	protected function newController() {
		return new UstawieniaController();
	}

}
