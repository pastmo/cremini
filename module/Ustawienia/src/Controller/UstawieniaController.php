<?php

namespace Ustawienia\Controller;

use Zend\View\Model\ViewModel;

class UstawieniaController extends \Wspolne\Controller\WspolneController {

    public function indexAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	$wynik = new ViewModel();
	$wynik->addChild($this->faktury(), 'faktury');

	return $wynik;
    }

    private function faktury() {
	$wystawca = $this->wystawcyFakturyMenager->pobierzUtworzWystawce();

	$model = new ViewModel(array(
		'wystawca' => $wystawca
	));


	$model->setTemplate('ustawienia/ustawienia/faktury');

	return $model;
    }

    public function zapiszWystawceFakturAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);
	$post = $this->getPost();
	if ($post) {
	    $this->wystawcyFakturyMenager->zapiszZPosta($post);
	}

	return $this->redirect()->toRoute('ustawienia', array('action' => 'index'));
    }

}
