<?php

namespace Testy\FabrykiKonkretnychEncji;

class EmailFolderFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Email\Entity\EmailFolder();
	$encja->filtr = "filtr";
	$encja->nazwa = __CLASS__;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Email\Menager\EmaileFolderyMenager::class;
    }

    public function dowiazInneTabele() {
	$this->encja->konto_mailowe = $this->uzyjWlasciwejFabryki(\Pastmo\Email\Entity\KontoMailowe::class);
    }

}
