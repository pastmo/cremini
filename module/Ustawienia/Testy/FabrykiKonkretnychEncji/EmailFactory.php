<?php

namespace Testy\FabrykiKonkretnychEncji;

class EmailFactory extends \Pastmo\Testy\FabrykiKonkretnychEncji\EmailFactory {

    public function nowaEncja() {
	$encja = new \Email\Entity\Email();
	$encja->nadawca = "nadawca <email@nadawcy.pl>";
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Email\Menager\EmaileMenager::class;
    }

}
