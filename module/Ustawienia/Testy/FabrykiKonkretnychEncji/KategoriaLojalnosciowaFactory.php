<?php

namespace Testy\FabrykiKonkretnychEncji;

class KategoriaLojalnosciowaFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Produkty\Entity\KategoriaLojalnosciowa();

	$encja->nazwa = "Kategoria ABC";

	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Produkty\Menager\KategorieLojalnoscioweMenager::class;
    }

}
