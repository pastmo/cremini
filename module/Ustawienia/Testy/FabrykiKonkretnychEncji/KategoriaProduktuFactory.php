<?php

namespace Testy\FabrykiKonkretnychEncji;

class KategoriaProduktuFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Produkty\Entity\KategoriaProduktu;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Produkty\Menager\KategorieProduktuMenager::class;
    }

    public static function create(\Zend\ServiceManager\ServiceManager $sm) {
	$factory = new KategoriaProduktuFactory();
	$factory->sm = $sm;
	return $factory;
    }

    public function zrobPrzykladoweDrzewkoKategorii() {
	$wynik = array();
	$nazwy = array('kategoria1', 'kategoria2', 'kategoria3');
	$ids = array('1', '2', '3');

	foreach ($nazwy as $i => $nazwa) {
	    $kategoria = \FabrykaRekordow::makeEncje(\Produkty\Entity\KategoriaProduktu::class,
			    \Pastmo\Testy\ParametryFabryki\PFMockoweNiepowiazane::create()->setparametry(
				    array('nazwa' => $nazwa, 'id' => $ids[$i])));
	    $kategoria->kategorie_podrzedne = $wynik;
	    $wynik[] = $kategoria;
	}

	return $wynik;
    }

}
