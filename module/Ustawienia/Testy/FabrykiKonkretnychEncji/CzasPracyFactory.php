<?php

namespace Testy\FabrykiKonkretnychEncji;

class CzasPracyFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Wizualizacje\Entity\CzasPracy();
	$encja->typ = \Wizualizacje\Enumy\TypyCzasowPracy::GRAFIKA;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Wizualizacje\Menager\CzasyPracyMenager::class;
    }

    public function dowiazInneTabele() {
	$this->encja->parametry_wizualizacji = $this->uzyjWlasciwejFabryki(\Projekty\Entity\ParametryWizualizacji::class);
	$this->encja->parametry_produkcji = $this->uzyjWlasciwejFabryki(\Produkcja\Entity\ParametryProdukcji::class);
    }

}
