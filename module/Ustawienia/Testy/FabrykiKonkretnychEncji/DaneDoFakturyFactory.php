<?php

namespace Testy\FabrykiKonkretnychEncji;

class DaneDoFakturyFactory extends \FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Projekty\Entity\DaneDoFaktury;
	$encja->nip = $this->genrujId();
	$encja->termin_platnosci = date('Y-m-d');
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Projekty\Menager\DaneDoFakturyMenager::class;
    }

    public function dowiazInneTabele() {
	$this->encja->adres = $this->uzyjWlasciwejFabryki(\Firmy\Entity\Adres::class);
    }

}
