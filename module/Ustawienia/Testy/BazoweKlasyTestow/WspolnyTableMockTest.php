<?php

namespace Testy\BazoweKlasyTestow;

abstract class WspolnyTableMockTest extends \Testy\BazoweKlasyTestow\AbstractMockTest {

	public function setUp() {
		\Zasoby\Model\ZasobyTable::$uploadDir="./public_html/test/";
		parent::setUp();
	}

	protected function ustawUzytkownikaPoMailu($wynik, $wynik2 = null) {

		$this->uzytkownikTable->expects($this->any())->method('pobierzUzytkownikaPoMailu')
				->will($this->onConsecutiveCalls($wynik, $wynik2));
	}

	protected function pobierzZarejestrujUzytkownikaPoMailu($wynik, $wynik2 = null) {

		$this->rejestracjaMenager->expects($this->any())->method('pobierzZarejestrujUzytkownikaPoMailu')
				->will($this->onConsecutiveCalls($wynik, $wynik2));
	}

}
