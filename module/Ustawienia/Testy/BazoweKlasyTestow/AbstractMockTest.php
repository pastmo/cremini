<?php

namespace Testy\BazoweKlasyTestow;

abstract class AbstractMockTest extends \Pastmo\Testy\BazoweKlasyTestow\AbstractMockTest {

    public $adresyMenager;
    public $daneDoFakturyMenager;
    public $emaileMenager;
    public $emaileFolderyMenager;
    public $kategoriaDzialalnosciTable;
    public $rodzajePraktykTable;
    public $gateway;
    public $zasobyTable;
    public $aktywacjaTable;
    public $infoDoAdminaMail;
    public $uzytkownikTable;
    public $uzytkownikWatekWiadomosciTable;
    public $projektyMenager;
    public $projektyZEmailiMenager;
    public $parametryWizualizacjiMenager;
    public $uzytkownicyProjektyMenager;
    public $kontaMailoweManager;
    public $wiadomosciMenager;
    public $projektyWatkiWiadomoscMenager;
    public $projektyTagiMenager;
    public $projektyKrokiMiloweMenager;
    public $projektyProduktyWariacjeMenager;
    public $projektyProduktyWariacjeWizualizacjeMenager;
    public $wizualizacjeWiadomosciMenager;
    public $tagiMenager;
    public $wiadomosciZasobyMenager;
    public $rejestracjaMenager;
    public $aktywacjaMenager;
    public $firmyMenager;
    public $uprawnieniaMenager;
    public $przypominanieHaslaMenager;
    public $resetHaslaZapytanieMail;
    public $produktyMenager;
    public $produktyWariacjeMenager;
    public $tworzenieProjektuMenager;
    public $zamowieniaWewnetrzneWariacjeMenager;
    public $przypomnieniaMenager;
    public $wiadomosciTagiMenager;
    public $parametryProdukcjiMenager;
    public $uzytkownicyWiadomosciMenager;
    public $pdfMenager;
    public $zamowieniaWariacjeMenager;
    public $uzytkownicyFirmyMenager;
    public $roleMenager;
    public $produktyZasobyMenager;
    public $produktyKategorieMenager;
    public $krokiMiloweMenager;
    public $watkiWiadomosciTable;
    public $projektoweWatkiChatuMenager;
    public $wycenyProducentowMenager;
    public $wiadomosciZWlascicielemMenager;
    public $powiadomieniaMenager;
    public $statystykiMenager;
    public $logiMenager;
    public $produktyMagazynMenager;
    public $czasyPracyMenager;
    public $czasMenager;
    public $czasyPracyDao;
    public $pobieraczDanychDoOsiCzasuMenager;
    public $projektyOznaczeniaMenager;
    public $zleceniaTable;

    public function setUp() {
	parent::setUp();

	$this->zasobyTable = $this->getMockBuilder('Zasoby\Model\ZasobyTable')
		->disableOriginalConstructor()
		->getMock();
	$this->uzytkownikTable = $this->getMockBuilder('Logowanie\Model\UzytkownikTable')
		->disableOriginalConstructor()
		->getMock();
	$this->uzytkownikWatekWiadomosciTable = $this->getMockBuilder(\Konwersacje\Module::UZYTKOWNIK_WATKEK_WIADOMOSCI_TABLE)
		->disableOriginalConstructor()
		->getMock();
	$this->projektyMenager = $this->getMockBuilder(\Projekty\Menager\ProjektyMenager::class)
		->disableOriginalConstructor()
		->getMock();
	$this->projektyZEmailiMenager = $this->getMockBuilder(\Projekty\Menager\ProjektyZEmailiMenager::class)
		->disableOriginalConstructor()
		->getMock();
	$this->uzytkownicyProjektyMenager = $this->getMockBuilder(\Logowanie\Menager\UzytkownicyProjektyMenager::class)
		->disableOriginalConstructor()
		->getMock();

	$this->wiadomosciMenager = $this->getMockBuilder(\Konwersacje\Model\WiadomosciMenager::class)
		->disableOriginalConstructor()
		->getMock();
	$this->projektyWatkiWiadomoscMenager = $this->getMockBuilder(\Konwersacje\Menager\ProjektyWatkiWiadomoscMenager::class)
		->disableOriginalConstructor()
		->getMock();
	$this->projektyTagiMenager = $this->getMockBuilder(\Projekty\Menager\ProjektyTagiMenager::class)
		->disableOriginalConstructor()
		->getMock();
	$this->projektyKrokiMiloweMenager = $this->getMockBuilder(\Projekty\Menager\ProjektyKrokiMiloweMenager::class)
		->disableOriginalConstructor()
		->getMock();
	$this->tagiMenager = $this->getMockBuilder(\Projekty\Menager\TagiMenager::class)
		->disableOriginalConstructor()
		->getMock();
	$this->projektyProduktyWariacjeMenager = $this->getMockBuilder(\Projekty\Menager\ProjektyProduktyWariacjeMenager::class)
		->disableOriginalConstructor()
		->getMock();
	$this->projektyProduktyWariacjeWizualizacjeMenager = $this->getMockBuilder(\Projekty\Menager\ProjektyProduktyWariacjeWizualizacjeMenager::class)
		->disableOriginalConstructor()
		->getMock();
	$this->wiadomosciZasobyMenager = $this->getMockBuilder(\Konwersacje\Menager\WiadomosciZasobyMenager::class)
		->disableOriginalConstructor()
		->getMock();
	$this->wizualizacjeWiadomosciMenager = $this->getMockBuilder(\Konwersacje\Menager\WizualizacjeWiadomosciMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->rejestracjaMenager = $this->getMockBuilder(\Logowanie\Menager\RejestracjaMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->firmyMenager = $this->getMockBuilder(\Firmy\Menager\FirmyMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->aktywacjaTable = $this->getMockBuilder(\Logowanie\Model\AktywacjaMail::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->infoDoAdminaMail = $this->getMockBuilder(\Logowanie\Model\InfoDoAdminaMail::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->aktywacjaMenager = $this->getMockBuilder(\Logowanie\Menager\AktywacjaMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->uprawnieniaMenager = $this->getMockBuilder(\Logowanie\Menager\UprawnieniaMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->adresyMenager = $this->getMockBuilder(\Firmy\Menager\AdresyMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->przypominanieHaslaMenager = $this->getMockBuilder(\Logowanie\Menager\PrzypominanieHaslaMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->resetHaslaZapytanieMail = $this->getMockBuilder(\Logowanie\Model\ResetHaslaZapytanieMail::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->produktyMenager = $this->getMockBuilder(\Produkty\Menager\ProduktyMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->emaileMenager = $this->getMockBuilder(\Email\Menager\EmaileMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->emaileFolderyMenager = $this->getMockBuilder(\Email\Menager\EmaileFolderyMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->parametryWizualizacjiMenager = $this->getMockBuilder(\Projekty\Menager\ParametryWizualizacjiMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->produktyWariacjeMenager = $this->getMockBuilder(\BazaProduktow\Menager\ProduktyWariacjeMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->tworzenieProjektuMenager = $this->getMockBuilder(\Projekty\Menager\TworzenieProjektuMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->zamowieniaWewnetrzneWariacjeMenager = $this->getMockBuilder(\Magazyn\Menager\ZamowieniaWewnetrzneWariacjeMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->przypomnieniaMenager = $this->getMockBuilder(\Dashboard\Menager\PrzypomnieniaMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->wiadomosciTagiMenager = $this->getMockBuilder(\Konwersacje\Menager\WiadomosciTagiMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->parametryProdukcjiMenager = $this->getMockBuilder(\Produkcja\Menager\ParametryProdukcjiMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->uzytkownicyWiadomosciMenager = $this->getMockBuilder(\Konwersacje\Menager\UzytkownicyWiadomosciMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->daneDoFakturyMenager = $this->getMockBuilder(\Projekty\Menager\DaneDoFakturyMenager::class
		)
		->disableOriginalConstructor()
		->getMock();

	$this->zamowieniaWariacjeMenager = $this->getMockBuilder(\Magazyn\Menager\ZamowieniaWewnetrzneWariacjeMenager::class
		)
		->disableOriginalConstructor()
		->getMock();

	$this->uzytkownicyFirmyMenager = $this->getMockBuilder(\Firmy\Menager\UzytkownicyFirmyMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->roleMenager = $this->getMockBuilder(\Logowanie\Menager\RoleMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->produktyZasobyMenager = $this->getMockBuilder(\Produkty\Menager\ProduktyZasobyMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->produktyKategorieMenager = $this->getMockBuilder(\Produkty\Menager\ProduktyKategorieMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->krokiMiloweMenager = $this->getMockBuilder(\Projekty\Menager\KrokiMiloweMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->watkiWiadomosciTable = $this->getMockBuilder(\Konwersacje\Model\WatkiWiadomosciTable::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->projektoweWatkiChatuMenager = $this->getMockBuilder(\Konwersacje\Menager\ProjektoweWatkiChatuMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->wycenyProducentowMenager = $this->getMockBuilder(\Aukcje\Menager\WycenyProducentowMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->wiadomosciZWlascicielemMenager = $this->getMockBuilder(\Konwersacje\Menager\WiadomosciZWlascicielemMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->powiadomieniaMenager = $this->getMockBuilder(\Dashboard\Menager\PowiadomieniaMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->produktyMagazynMenager = $this->getMockBuilder(\BazaProduktow\Menager\ProduktyMagazynMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->statystykiMenager = $this->getMockBuilder(\Statystyki\Menager\StatystykiMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->czasyPracyMenager = $this->getMockBuilder(\Wizualizacje\Menager\CzasyPracyMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->pobieraczDanychDoOsiCzasuMenager = $this->getMockBuilder(\Wizualizacje\Menager\PobieraczDanychDoOsiCzasuMenager::class
		)
		->disableOriginalConstructor()
		->getMock();
	$this->rowset = $this->getMockBuilder('WspolneTest\Mocki\RowSetMock')
		->disableOriginalConstructor()
		->getMock();
	$this->sqlMock = $this->getMockBuilder('Zend\Db\Sql\Sql')
		->disableOriginalConstructor()
		->getMock();
	$this->selectMock = $this->getMockBuilder('Zend\Db\Sql\Select')
		->disableOriginalConstructor()
		->getMock();

	$this->czasyPracyDao = $this->getMockBuilder(\Wizualizacje\Dao\CzasyPracyDao::class)
		->disableOriginalConstructor()
		->getMock();
	$this->projektyOznaczeniaMenager = $this->getMockBuilder(\Projekty\Menager\ProjektyOznaczeniaMenager::class)
		->disableOriginalConstructor()
		->getMock();
	$this->zleceniaTable = $this->getMockBuilder(\Zlecenia\Model\ZleceniaTable::class)
		->disableOriginalConstructor()
		->getMock();

	$this->sm->setAllowOverride(true);
	$this->sm->setService('Zasoby\Model\ZasobyTable', $this->zasobyTable);
	$this->sm->setService('Logowanie\Model\UzytkownikTable', $this->uzytkownikTable);
	$this->sm->setService(\Konwersacje\Module::UZYTKOWNIK_WATKEK_WIADOMOSCI_TABLE,
		$this->uzytkownikWatekWiadomosciTable);
	$this->sm->setService(\Logowanie\Module::UPRAWNIENIA_GATEWAY, $this->gateway);
	$this->sm->setService(\Projekty\Menager\ProjektyMenager::class, $this->projektyMenager);
	$this->sm->setService(\Projekty\Menager\ProjektyZEmailiMenager::class, $this->projektyZEmailiMenager);
	$this->sm->setService(\Logowanie\Menager\UzytkownicyProjektyMenager::class, $this->uzytkownicyProjektyMenager);

	$this->sm->setService(\Konwersacje\Model\WiadomosciMenager::class, $this->wiadomosciMenager);
	$this->sm->setService(\Konwersacje\Menager\ProjektyWatkiWiadomoscMenager::class,
		$this->projektyWatkiWiadomoscMenager);
	$this->sm->setService(\Projekty\Menager\ProjektyTagiMenager::class, $this->projektyTagiMenager);
	$this->sm->setService(\Projekty\Menager\ProjektyKrokiMiloweMenager::class, $this->projektyKrokiMiloweMenager);
	$this->sm->setService(\Projekty\Menager\TagiMenager::class, $this->tagiMenager);
	$this->sm->setService(\Projekty\Menager\ProjektyProduktyWariacjeWizualizacjeMenager::class,
		$this->projektyProduktyWariacjeWizualizacjeMenager);
	$this->sm->setService(\Konwersacje\Menager\WizualizacjeWiadomosciMenager::class,
		$this->wizualizacjeWiadomosciMenager);
	$this->sm->setService(\Konwersacje\Menager\WiadomosciZasobyMenager::class, $this->wiadomosciZasobyMenager);
	$this->sm->setService(\Logowanie\Menager\RejestracjaMenager::class, $this->rejestracjaMenager);
	$this->sm->setService(\Firmy\Menager\FirmyMenager::class, $this->firmyMenager);
	$this->sm->setService(\Logowanie\Model\AktywacjaMail::class, $this->aktywacjaTable);
	$this->sm->setService(\Logowanie\Model\InfoDoAdminaMail::class, $this->infoDoAdminaMail);
	$this->sm->setService(\Logowanie\Menager\AktywacjaMenager::class, $this->aktywacjaMenager);
	$this->sm->setService(\Projekty\Menager\ProjektyProduktyWariacjeMenager::class,
		$this->projektyProduktyWariacjeMenager);
	$this->sm->setService(\Logowanie\Menager\UprawnieniaMenager::class, $this->uprawnieniaMenager);
	$this->sm->setService(\Firmy\Menager\AdresyMenager::class, $this->adresyMenager);
	$this->sm->setService(\Logowanie\Menager\PrzypominanieHaslaMenager::class, $this->przypominanieHaslaMenager);
	$this->sm->setService(\Logowanie\Model\ResetHaslaZapytanieMail::class, $this->resetHaslaZapytanieMail);
	$this->sm->setService(\Produkty\Menager\ProduktyMenager::class, $this->produktyMenager);
	$this->sm->setService(\Email\Menager\EmaileMenager::class, $this->emaileMenager);
	$this->sm->setService(\Email\Menager\EmaileFolderyMenager::class, $this->emaileFolderyMenager);
	$this->sm->setService(\Projekty\Menager\ParametryWizualizacjiMenager::class,
		$this->parametryWizualizacjiMenager);
	$this->sm->setService(\BazaProduktow\Menager\ProduktyWariacjeMenager::class, $this->produktyWariacjeMenager);
	$this->sm->setService(\Projekty\Menager\TworzenieProjektuMenager::class, $this->tworzenieProjektuMenager);
	$this->sm->setService(\Magazyn\Menager\ZamowieniaWewnetrzneWariacjeMenager::class,
		$this->zamowieniaWewnetrzneWariacjeMenager);
	$this->sm->setService(\Dashboard\Menager\PrzypomnieniaMenager::class, $this->przypomnieniaMenager);
	$this->sm->setService(\Konwersacje\Menager\WiadomosciTagiMenager::class, $this->wiadomosciTagiMenager);
	$this->sm->setService(\Produkcja\Menager\ParametryProdukcjiMenager::class, $this->parametryProdukcjiMenager);
	$this->sm->setService(\Konwersacje\Menager\UzytkownicyWiadomosciMenager::class,
		$this->uzytkownicyWiadomosciMenager);
	$this->sm->setService(\Projekty\Menager\DaneDoFakturyMenager::class, $this->daneDoFakturyMenager);

	$this->sm->setService(\Magazyn\Menager\ZamowieniaWewnetrzneWariacjeMenager::class,
		$this->zamowieniaWariacjeMenager);
	$this->sm->setService(\Firmy\Menager\UzytkownicyFirmyMenager::class, $this->uzytkownicyFirmyMenager);
	$this->sm->setService(\Logowanie\Menager\RoleMenager::class, $this->roleMenager);
	$this->sm->setService(\Produkty\Menager\ProduktyZasobyMenager::class, $this->produktyZasobyMenager);
	$this->sm->setService(\Produkty\Menager\ProduktyKategorieMenager::class, $this->produktyKategorieMenager);
	$this->sm->setService(\Projekty\Menager\KrokiMiloweMenager::class, $this->krokiMiloweMenager);
	$this->sm->setService(\Konwersacje\Model\WatkiWiadomosciTable::class, $this->watkiWiadomosciTable);
	$this->sm->setService(\Konwersacje\Menager\ProjektoweWatkiChatuMenager::class,
		$this->projektoweWatkiChatuMenager);
	$this->sm->setService(\Aukcje\Menager\WycenyProducentowMenager::class, $this->wycenyProducentowMenager);
	$this->sm->setService(\Konwersacje\Menager\WiadomosciZWlascicielemMenager::class,
		$this->wiadomosciZWlascicielemMenager);
	$this->sm->setService(\Dashboard\Menager\PowiadomieniaMenager::class, $this->powiadomieniaMenager);

	$this->sm->setService(\Statystyki\Menager\StatystykiMenager::class, $this->statystykiMenager);
	$this->sm->setService(\BazaProduktow\Menager\ProduktyMagazynMenager::class, $this->produktyMagazynMenager);
	$this->sm->setService(\Wizualizacje\Menager\CzasyPracyMenager::class, $this->czasyPracyMenager);

	$this->sm->setService(\Wizualizacje\Dao\CzasyPracyDao::class, $this->czasyPracyDao);
	$this->sm->setService(\Wizualizacje\Menager\PobieraczDanychDoOsiCzasuMenager::class,
		$this->pobieraczDanychDoOsiCzasuMenager);
	$this->sm->setService(\Projekty\Menager\ProjektyOznaczeniaMenager::class, $this->projektyOznaczeniaMenager);
	$this->sm->setService(\Zlecenia\Model\ZleceniaTable::class, $this->zleceniaTable);
    }

    protected function nowyObiekt() {

    }

    protected function ustawMockaGateway($name) {
	$this->gateway = $this->getMockBuilder('Zend\Db\TableGateway\TableGateway')
		->disableOriginalConstructor()
		->getMock();
	$this->sm->setService($name, $this->gateway);
    }

}
