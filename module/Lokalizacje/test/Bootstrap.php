<?php
namespace LokalizacjeTest;

require_once dirname(__FILE__) . '/../../Wspolne/test/WspolneBootstrap.php';

class Bootstrap extends \WspolneTest\WspolneBootstrap {

}

Bootstrap::init(array(
	'Wspolne', 'Logowanie'
));
Bootstrap::chroot();
