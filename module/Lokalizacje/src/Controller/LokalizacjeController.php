<?php

namespace Lokalizacje\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use \Logowanie\Enumy\KodyUprawnien;

class LokalizacjeController extends \Wspolne\Controller\WspolneController {

    protected $zasobyTable;

    public function indexAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	return new ViewModel(array(
	));
    }

    public function dzialTechnicznyAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	return new ViewModel(array(
	));
    }

    public function obslugaKlientaAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	return new ViewModel(array(
	));
    }

    public function sklepAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	return new ViewModel(array(
	));
    }

    public function portalInformacyjnyAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	return new ViewModel(array(
	));
    }

    public function sprzedazAction() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(KodyUprawnien::DOMYSLNE_UPRAWNIENIE, true);

	return new ViewModel(array(
	));
    }

}
