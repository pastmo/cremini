<?php

namespace Wspolne\Interfejs;

interface UzytkownikBuilderInterfejs {

    function getEmail();

    function getImieINazwisko();

    function getTelefon();
}
