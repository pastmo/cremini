<?php

namespace Wspolne\Menager;

class SqlUpdater extends \Pastmo\Wspolne\Menager\WspolnyMenager {

    private $kategorieProduktuMenager;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm);

	$this->kategorieProduktuMenager = $this->sm->get(\Produkty\Menager\KategorieProduktuMenager::class);
    }

    public function usunNieuzywaneKategorieProdukotw() {
	$nieuzywaneKategorie = $this->kategorieProduktuMenager->pobierzZWherem("id in (select id from kategorie_produktu kp
where
kp.id not in (select pk.kategoria_produktu_id from produkty_kategorie pk  where pk.kategoria_produktu_id is not null)
 AND
 kp.id not in (select distinct kp2.nadrzedna_kategoria_id from kategorie_produktu kp2 where kp2.nadrzedna_kategoria_id is not null))");

	$idKategori = array();

	if (count($nieuzywaneKategorie) > 0) {
	    foreach ($nieuzywaneKategorie as $kategoria) {
		$idKategori[] = $kategoria->id;
	    }

	    $majaZostac = $this->pobierzIdKategoriiKtoreMajaZostac();

	    $ids = implode(",", $idKategori);
	    $nIds = implode(",", $majaZostac);
	    $this->kategorieProduktuMenager->usun("id IN ($ids) AND id not in($nIds)");
	    return true;
	} else {
	    return false;
	}
    }

    public function pobierzIdKategoriiKtoreMajaZostac() {
	$nadrzedne = $this->kategorieProduktuMenager->pobierzZWherem("nazwa='Statuetki Robocze' OR nazwa='Komponenty'");

	$wszystkie = $this->kategorieProduktuMenager->pobierzListaKategoriiZPodkategoriami($nadrzedne);

	$wynik = array();

	foreach ($wszystkie as $kategoria) {
	    $wynik[] = $kategoria->id;
	}
	return $wynik;
    }

}
