<?php

namespace Wspolne\Menager;

use Zend\ServiceManager\ServiceManager;
use Wspolne\Model\FabrykiGetter;
use Pastmo\Wspolne\Utils\EntityUtil;

class BazowyMenagerBazodanowy extends \Pastmo\Wspolne\Menager\WspolnyMenagerBazodanowy implements \Wspolne\Interfejs\UzytkownikFasadInterface {

    use FabrykiGetter;

    protected $wyszukiwanieZKontem = true;

    public function __construct(ServiceManager $sm = null, $gatewayName = '') {
	parent::__construct($sm, $gatewayName);
	$this->fasadyContainer = new \Wspolne\Model\FasadyContainter($sm);
    }

    public function zapisz(\Pastmo\Wspolne\Entity\WspolnyModel $model) {

	$kontoId = $this->getLogowanieFasada()->getKontoIdZalogowanego();

	if ($this->wyszukiwanieZKontem && !EntityUtil::wydobadzId($model) && $kontoId) {

	    $model->konto = $kontoId;
	}

	parent::zapisz($model);
    }

    public function modyfikujSelecta($select) {
	if (!$this->wyszukiwanieZKontem) {
	    return;
	}

	$kontoId = $this->getLogowanieFasada()->getKontoIdZalogowanego();

	if ($kontoId) {
	    $select->where("konto_id=$kontoId");
	}
    }

}
