<?php

namespace Wspolne\Menager;

class SesjaMenager extends \Pastmo\Sesja\Menager\SesjaMenager {

    private $containerUzytkownicy;

    const BAZA_PRODUKTOW_CONTAINER = 'BazaProduktowContainer';
    const KOSZYK_CONTAINER = 'KoszykContainer';
    const UZYTKOWNICY_CONTAINER = 'UzytkownicyContainer';
    const LICZBA_PRODUKTOW_KEY = 'liczbaproduktow';
    const CZY_WYSWIETLANIE_MINIATUR_KEY = 'czywyswietlanieminiatur';
    const KOSZYK_KEY = 'koszyk';
    const UZYTKOWNIK_KEY = 'user_id';

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm);

	$this->containerUzytkownicy = $this->createContainer($this::UZYTKOWNICY_CONTAINER);
    }


    public function ustawIdUzytkownika($id = null) {
	$this->ustawKluczLubUsun($this->containerUzytkownicy, $this::UZYTKOWNIK_KEY, $id);
    }

    public function pobierzIdUzytkownika() {
	$uzytkownikId = $this->zwrocZawartoscKluczaLubFalsz($this->containerUzytkownicy, $this::UZYTKOWNIK_KEY);
	if ($uzytkownikId) {
	    return $uzytkownikId;
	}
    }

}
