<?php

namespace Wspolne\Menager;

class PodpowiadanieMenager extends \Pastmo\Wspolne\Menager\WyszukiwanieMenager {

    private $wszystkieModuly = array(
	    'Użytkownicy' => \Logowanie\Menager\UzytkownicyMenager::class,
	    'Produkty' => \Produkty\Menager\ProduktyMenager::class,
	    'Firmy' => \Firmy\Menager\FirmyMenager::class
    );
    private $aktywneModuly = array();
    private $uprawnieniePrzegladaniaUzytkownika = false;
    private $uprawnienieProjektow = false;
    private $uprawnienieProduktow = false;
    private $uprawnienieFirm = false;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm = null) {
	parent::__construct($sm);
	$this->resetAktywnychModulow();
    }

    public function pobierzPodpowiedzi($string) {
	$podpowiedzi = parent::pobierzPodpowiedzi($string);
	$this->resetAktywnychModulow();
	return $podpowiedzi;
    }

    public function pobierzKlasyModulow() {
	return $this->aktywneModuly;
    }

    public function pobierzWszystkieModuly() {
	return $this->wszystkieModuly;
    }

    public function ustawModuly($aktywneModulyArray) {

	$this->aktywneModuly = array();
	$this->sprawdzUprawnieniaPrzedWyszukaniem();
	if (is_array($aktywneModulyArray)) {
	    foreach ($aktywneModulyArray as $aktywnyModul) {
		$przegladanieProfili = ($aktywnyModul === 'Użytkownicy' && $this->uprawnieniePrzegladaniaUzytkownika);
		$projekty = ($aktywnyModul === 'Projekty' && $this->uprawnienieProjektow);
		$produkty = ($aktywnyModul === 'Produkty' && $this->uprawnienieProduktow);
		$firmy = ($aktywnyModul === 'Firmy' && $this->uprawnienieFirm);
		if ($przegladanieProfili || $projekty || $produkty || $firmy) {
		    $this->aktywneModuly[$aktywnyModul] = $this->wszystkieModuly[$aktywnyModul];
		}
	    }
	}
    }

    private function resetAktywnychModulow() {
	$this->sprawdzUprawnieniaPrzedWyszukaniem();
	foreach ($this->wszystkieModuly as $index => $class) {
	    $przegladanieProfili = ($index === 'Użytkownicy' && $this->uprawnieniePrzegladaniaUzytkownika);
	    $projekty = ($index === 'Projekty' && $this->uprawnienieProjektow);
	    $produkty = ($index === 'Produkty' && $this->uprawnienieProduktow);
	    $firmy = ($index === 'Firmy' && $this->uprawnienieFirm);
	    if ($przegladanieProfili || $projekty || $produkty || $firmy) {
		$this->aktywneModuly[$index] = $class;
	    }
	}
    }

    private function sprawdzUprawnieniaPrzedWyszukaniem() {
	$logowanieFasada = $this->sm->get(\Logowanie\Fasada\LogowanieFasada::class);
	$uzytkownik = $logowanieFasada->getZalogowanegoUsera();
	if (!empty($uzytkownik)) {
	    $uprawnienia = $uzytkownik->rola->getUprawnienia();
	    if (!empty($uprawnienia)) {
		$this->zapiszDostepneUprawnienia($uprawnienia);
	    }
	}
    }

    private function zapiszDostepneUprawnienia(array $uprawnienia) {
	$kodyUprawnien = \Pastmo\Wspolne\Utils\ArrayUtil::przerobNaTabliceWartosci($uprawnienia, 'kod');
	foreach ($kodyUprawnien as $kod) {
	    if ($kod === \Logowanie\Enumy\KodyUprawnien::PRZEGLADANIE_PROFILII_UZYTKOWNIKOW) {
		$this->uprawnieniePrzegladaniaUzytkownika = true;
	    }
	    if ($kod === \Logowanie\Enumy\KodyUprawnien::MENU_PROJEKTY) {
		$this->uprawnienieProjektow = true;
	    }
	    if ($kod === \Logowanie\Enumy\KodyUprawnien::MENU_PRODUKTY) {
		$this->uprawnienieProduktow = true;
	    }
	    if ($kod === \Logowanie\Enumy\KodyUprawnien::MENU_FIRMY) {
		$this->uprawnienieFirm = true;
	    }
	}
    }

}
