<?php

namespace Wspolne\Fasada;

use Zend\ServiceManager\ServiceManager;

class WspolneFasada {

    protected $serviceMenager;

    public function __construct(ServiceManager $serviceMenager) {
	$this->serviceMenager = $serviceMenager;
    }

}
