<?php

namespace Wspolne\Controller;

use \Logowanie\Enumy\KodyUprawnien;

class UpdateController extends \Wspolne\Controller\WspolneController {

    public function indexAction() {
    }

    private function przypiszLosoweKategorieDoWszystkichProduktow() {
	$produkty = $this->produktyMenager->fetchAll();
	$kategorie = $this->kategorieProduktuMenager->pobierzZWherem('1');

	foreach ($produkty as $produkt) {
	    $produktKategoria = new \Produkty\Entity\ProduktKategoria();
	    $produktKategoria->produkt = $produkt;
	    $produktKategoria->kategoria_produktu = $kategorie[rand(0, count($kategorie) - 1)];

	    $this->produktyKategorieMenager->zapisz($produktKategoria);
	}
    }

    private function usunNieuzywaneKategorieProduktow() {
	$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::
		UPRAWNIENIA_ADMINISTRATORA);

	$msg = "";

	for ($i = 0; $i < 5; $i++) {
	    $this->sqlUpdater->usunNieuzywaneKategorieProdukotw();
	    $usuwanie = $this->translate("usuwanie kategori");
	    $msg.="<p>$usuwanie $i</p>";
	}

	return new \Zend\View\Model\ViewModel(array('msg' => $msg));
    }

    public function aktualizujSerieAction() {
	$this->produktyWariacjeMenager->aktualizujSerieIstniejacychWariacji();
	return $this->redirect()->toRoute('dashboard');
    }

}
