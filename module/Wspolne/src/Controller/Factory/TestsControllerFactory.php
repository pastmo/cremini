<?php

namespace Wspolne\Controller\Factory;

class TestsControllerFactory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

    protected function newController() {
	return new \Wspolne\Controller\TestsController();
    }

}
