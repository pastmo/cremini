<?php

namespace Wspolne\Controller\Factory;

use \Zend\ServiceManager\FactoryInterface;
use \Zend\ServiceManager\ServiceLocatorInterface;

abstract class WspolneControllerFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {
	return $this($serviceLocator, '');
    }

    public function __invoke(\Interop\Container\ContainerInterface $container, $requestedName,
	    array $options = NULL) {

	$initObiekt = new \Wspolne\Controller\Factory\InitObiekt();
	$initObiekt->emaileFasada = $container->get(\Email\Fasada\EmailFasada::class);
	$initObiekt->firmyFasada = $container->get(\Firmy\Fasada\FirmyFasada::class);
	$initObiekt->konwersacjeFasada = $container->get(\Konwersacje\Fasada\KonwersacjeFasada::class);
	$initObiekt->logowanieFasada = $container->get(\Logowanie\Fasada\LogowanieFasada::class);
	$initObiekt->zasobyFasada = $container->get(\Zasoby\Fasada\ZasobyFasada::class);

	$initObiekt->uzytkownikTable = $container->get(\Logowanie\Module::UZYTKOWNIK_TABLE);
	$initObiekt->zleceniaTable = $container->get(\Zlecenia\Module::ZLECENIA_TABLE);
	$initObiekt->zleceniaKategorieTable = $container->get(\Zlecenia\Module::KATEGORIE_TABLE);
	$initObiekt->klienciTable = $container->get(\Klienci\Module::KLIENCI_TABLE);
	$initObiekt->notatkiKlienciTable = $container->get(\Klienci\Module::NOTATKI_TABLE);
	$initObiekt->watkiWiadomosciTable = $container->get(\Konwersacje\Module::WATKI_WIADOMOSCI_TABLE);
	$initObiekt->wiadomosciTable = $container->get(\Konwersacje\Model\WiadomosciMenager::class);
	$initObiekt->uzytkownikWatkekWiadomosciTable = $container->get(\Konwersacje\Module::UZYTKOWNIK_WATKEK_WIADOMOSCI_TABLE);
	$initObiekt->roleMenager = $container->get(\Logowanie\Menager\RoleMenager::class);
	$initObiekt->zasobyTable = $container->get(\Zasoby\Menager\ZasobyUploadMenager::class);
	$initObiekt->uprawnieniaKategorieMenager = $container->get(\Logowanie\Menager\UprawnieniaKategorieMenager::class);
	$initObiekt->uprawnieniaRoleMenager = $container->get(\Logowanie\Menager\UprawnieniaRoleMenager::class);
	$initObiekt->uprawnieniaMenager = $container->get(\Logowanie\Menager\UprawnieniaMenager::class);
	$initObiekt->kontaMailoweMenager = $container->get(\Pastmo\Email\Menager\KontaMailoweMenager::class);
	$initObiekt->maileMenager = $container->get(\Email\Menager\EmaileMenager::class);
	$initObiekt->przypominanieHaslaMenager = $container->get(\Logowanie\Menager\PrzypominanieHaslaMenager::class);
	$initObiekt->fakturyMenager = $container->get(\Pastmo\Faktury\Menager\FakturyMenager::class);
	$initObiekt->pdfMenager = $container->get(\Pastmo\Pdf\Menager\PdfMenager::class);
	$initObiekt->wystawcyFakturyMenager = $container->get(\Pastmo\Faktury\Menager\WystawcyFakturMenager::class);

	//$initObiekt->translator = $container->get('MvcTranslator');
	//$initObiekt->constantTranslator = $container->get(\Lokalizacje\Menager\ConstantTranslatorMenager::class);
	//$initObiekt->viewRender = $container->get('ViewRenderer');

	$controller = $this->newController();
	$controller->init($initObiekt);
	return $controller;
    }

    abstract protected function newController();
}
