<?php

namespace Wspolne\Controller;

use \Wspolne\Menager\PodpowiadanieMenager;
use Zend\View\Model\ViewModel;

class WyszukiwanieController extends \Wspolne\Controller\WspolneController {

    public function indexAction() {

	$get = $this->getGet();
	if ($get) {
	    $moduly = $get->moduly;
	    $szukanyTekst = $get->text;

	    $this->podpowiadanieMenager->ustawModuly($moduly);
	    $this->podpowiadanieMenager->setMaxResult(500);
	    $wynik = $this->podpowiadanieMenager->pobierzPodpowiedzi($szukanyTekst);


	    return new ViewModel(array(
		    'wynik_wyszukiwania' => $wynik
	    ));
	}
    }

}
