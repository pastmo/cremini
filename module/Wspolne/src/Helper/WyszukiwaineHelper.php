<?php

namespace Wspolne\Helper;

use Zend\View\Helper\AbstractHelper;

class WyszukiwaineHelper extends AbstractHelper {

    private $podpowiadanieMenager;

    public function __construct($sm) {
	$this->podpowiadanieMenager = $sm->get(\Wspolne\Menager\PodpowiadanieMenager::class);
    }

    public function __invoke() {
	$moduly = $this->podpowiadanieMenager->pobierzKlasyModulow();
	?>
	<div id="wybor_modulow_wraper">

	    <?php
	    foreach ($moduly as $klucz => $wartosc):
		?>
	        <label><?php echo $klucz; ?></label>
	        <input
	    	name = "moduly['<?php echo $klucz; ?>']"
	    	type = 'checkbox'
	    	checked="true"
	    	class="wyszukiwanie_input"
	    	value="<?php echo $klucz; ?>"
	    	><?php
		endforeach;
		?>

	</div>
	<?php
    }

}
