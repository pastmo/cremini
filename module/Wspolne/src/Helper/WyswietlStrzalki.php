<?php

namespace Wspolne\Helper;

use Zend\View\Helper\AbstractHelper;

class WyswietlStrzalki extends AbstractHelper {

    public function __construct() {

    }

    public function __invoke($klucz) {
	?>
	<span class = "strzalki" data-klucz = "<?php echo $klucz; ?>">
	    <span class = "gora glyphicon glyphicon-sort-by-order-alt sort-icon "></span>
	    <span class = "dol glyphicon glyphicon-sort-by-order sort-icon "></span>
	</span>
	<?php
    }

}
