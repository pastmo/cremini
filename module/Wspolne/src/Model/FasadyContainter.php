<?php

namespace Wspolne\Model;

class FasadyContainter extends \Pastmo\Wspolne\Menager\WspolnyMenager {

    private $emailFasada;
    private $firmyFasada;
    private $konwersacjeFasada;
    private $logowanieFasada;
    private $produkcjaFasada;
    private $produktyFasada;
    private $projektyFasada;
    private $zasobyFasada;

    public function initFactory() {
	$this->emailFasada = $this->sm->get(\Email\Fasada\EmailFasada::class);
//	$this->firmyFasada = $this->sm->get(\Firmy\Fasada\FirmyFasada::class);
	$this->konwersacjeFasada = $this->sm->get(\Konwersacje\Fasada\KonwersacjeFasada::class);
	$this->logowanieFasada = $this->sm->get(\Logowanie\Fasada\LogowanieFasada::class);
//	$this->produkcjaFasada = $this->sm->get(\Produkcja\Fasada\ProdukcjaFasada::class);
//	$this->produktyFasada = $this->sm->get(\Produkty\Fasada\ProduktyFasada::class);
//	$this->projektyFasada = $this->sm->get(\Projekty\Fasada\ProjektyFasada::class);
	$this->zasobyFasada = $this->sm->get(\Zasoby\Fasada\ZasobyFasada::class);
    }

//    public function getProdukcjaFasada() {
//	return $this->getFasada('produkcjaFasada');
//    }

    public function getProduktyFasada() {
	return $this->getFasada('produktyFasada');
    }

    public function getProjektyFasada() {
	return $this->getFasada('projektyFasada');
    }

//    public function getFirmyFasada() {
//	return $this->getFasada('firmyFasada');
//    }

    public function getKonwersacjeFasada() {
	return $this->getFasada('konwersacjeFasada');
    }

    public function getLogowanieFasada() {
	return $this->getFasada('logowanieFasada');
    }

    public function getEmaileFasada() {
	return $this->getFasada('emailFasada');
    }

    public function getZasobyFasada() {
	return $this->getFasada('zasobyFasada');
    }

    protected function getFasada($nazwa) {
	if (!$this->$nazwa) {
	    $this->initFactory();
	}
	return $this->$nazwa;
    }

}
