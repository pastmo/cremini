<?php

namespace Wspolne\Model;

abstract class WspolneModel extends \Pastmo\Wspolne\Entity\WspolnyModel {

    public $konto;

    public function exchangeArray($data) {
	$this->data = $data;

	$this->konto = $this->pobierzTabeleObca('konto_id', \Uzytkownicy\Menager\KontaMenager::class,
		new \Uzytkownicy\Entity\Konto());
    }

    protected function konwertujNaKolumneDBWKlasachBazowych($nazwaWKodzie) {
	switch ($nazwaWKodzie) {
	    case 'konto':
		return 'konto_id';
	    default :
		return $nazwaWKodzie;
	}
    }

}
