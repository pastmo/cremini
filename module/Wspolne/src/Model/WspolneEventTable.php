<?php

namespace Wspolne\Model;

use Zend\ServiceManager\ServiceManager;

class WspolneEventTable extends \Pastmo\Wspolne\Menager\WspolnyMenagerBazodanowyEventowy {

    use FabrykiGetter;

    public function __construct(ServiceManager $sm = null, $gatewayName = '') {
	parent::__construct($sm, $gatewayName);
	$this->fasadyContainer = new FasadyContainter($sm);
    }

}
