<?php

namespace WspolneTest\Controller;

class WspolnyControllerTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

    public function setUp() {
	parent::setUp();
    }

    public function testIndex() {

    }

    public function test404() {
	$this->dispatch('nieznanyUrl');

	$this->sprawdzBodyZawiera('Strona nieznaleziona');
    }

    public function testError() {
	$this->dispatch('/tests/nieznany_wyjatek');

	$this->sprawdzBodyZawiera('Wystąpił nienzany wyjątek');
    }

    public function testGgerpException() {
	$this->dispatch('/tests/uprawnienia_wyjatek');

	$this->sprawdzBodyZawiera('<h1>Nie masz uprawnienia</h1>');
    }

    public function testMenu() {
	$this->uprawnieniaMenager->expects($this->any())->method('sprawdzUprawnienie')->willReturn(true);

	$this->dispatch('/tests/menu/1');

	$this->sprawdzBodyZawiera('projekty/szczegoly');
	$this->sprawdzBodyZawiera('Testowe projekty');
	$this->sprawdzBodyZawiera('fa klasa_ikony');

	$this->sprawdzBodyNieZawiera('class="submenu"');
    }

    public function testSubmenu() {
	$this->uprawnieniaMenager->expects($this->any())->method('sprawdzUprawnienie')->willReturn(true);

	$this->dispatch('/tests/submenu');

	$this->sprawdzBodyZawiera('projekty/szczegoly');
	$this->sprawdzBodyZawiera('Testowe projekty');
	$this->sprawdzBodyZawiera('class="submenu"');

	$this->sprawdzBodyNieZawiera('<i class');
    }

    public function testRozwijalneMenu() {
	$this->uprawnieniaMenager->expects($this->any())->method('sprawdzUprawnienie')->willReturn(true);

	$this->dispatch('/tests/rozwijalne_menu');

	$this->sprawdzBodyZawiera('projekty/szczegoly');
	$this->sprawdzBodyZawiera('logowanie/profil');

	$this->sprawdzBodyZawiera('Główna kategoria');
	$this->sprawdzBodyZawiera('Testowe projekty');
	$this->sprawdzBodyZawiera('Testowy profil');
	$this->sprawdzBodyZawiera('class="submenu"');
	$this->sprawdzBodyZawiera('dodatkowy element');

	$this->sprawdzBodyZawiera('<i class');
    }


    public function testMenu_brak_uprawnien() {
	$this->uprawnieniaMenager->expects($this->any())->method('sprawdzUprawnienie')->willReturn(false);

	$this->dispatch('/tests/menu/1');

	$this->sprawdzBodyNieZawiera('<li>');
    }

    public function testRozwijalneMenu_brak_uprawnien() {
	$this->uprawnieniaMenager->expects($this->any())->method('sprawdzUprawnienie')->willReturn(false);

	$this->dispatch('/tests/rozwijalne_menu');

	$this->sprawdzBodyNieZawiera('<li>');
    }

}
