<?php

namespace WspolneTest\Controller;

use Pastmo\Wspolne\Enumy\KluczeGet;

class WyszukiwanieGetTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

    const DOMYSLNE_SORTOWANIE = 'domyslne ASC';
    const DOMYSLNY_WHERE = '1 AND dodatkowy where ';
    const DOMYSLNA_STRONA = '0';
    const DOMYSLNA_LICZBA_WIERSZY = false;
    //
    const WYSZUKAJ_GET = KluczeGet::WYSZUKAJ . "=5";
    const SORTOWANIE_GET = KluczeGet::SORT_KLUCZ . "=klucz&" . KluczeGet::SORT_TYP . "=DESC";
    const NR_STRONY_GET = KluczeGet::STRONA . "=15";
    const LICZBA_WIERSZY = KluczeGet::LICZBA_WIERSZY . "=40";

    private $przypadki = array(
	    array('get' => "", 'where' => self::DOMYSLNY_WHERE, 'order' => self::DOMYSLNE_SORTOWANIE,
		    'strona' => self::DOMYSLNA_STRONA, 'liczba_wierszy' => self::DOMYSLNA_LICZBA_WIERSZY),
	    array('get' => self::WYSZUKAJ_GET, 'where' => '1 AND dodatkowy where  AND (id= 5)', 'order' => self::DOMYSLNE_SORTOWANIE,
		    'strona' => self::DOMYSLNA_STRONA, 'liczba_wierszy' => self::DOMYSLNA_LICZBA_WIERSZY),
	    array('get' => self::SORTOWANIE_GET, 'where' => self::DOMYSLNY_WHERE, 'order' => 'klucz DESC',
		    'strona' => self::DOMYSLNA_STRONA, 'liczba_wierszy' => self::DOMYSLNA_LICZBA_WIERSZY),
	    array('get' => self::NR_STRONY_GET, 'where' => self::DOMYSLNY_WHERE, 'order' => self::DOMYSLNE_SORTOWANIE,
		    'strona' => 15, 'liczba_wierszy' => self::DOMYSLNA_LICZBA_WIERSZY),
	    array('get' => self::LICZBA_WIERSZY, 'where' => self::DOMYSLNY_WHERE, 'order' => self::DOMYSLNE_SORTOWANIE,
		    'strona' => self::DOMYSLNA_STRONA, 'liczba_wierszy' => 40),
    );
    private $aktualnyPrzypadek;

    public function setUp() {
	parent::setUp();
    }

    public function testIndex() {
	$this->obslugaKlasObcych->ustawZalogowanegoUsera(\Logowanie\Model\Uzytkownik::create(), 1);
    }

    public function testWyszukiwaniaStronicowane_puste() {
	$this->aktualnyPrzypadek = $this->przypadki[0];

	$this->sprwadzaniepobierzZGetaStronicowanie();
    }

    public function testWyszukiwaniaStronicowane_wyszukaj() {
	$this->aktualnyPrzypadek = $this->przypadki[1];

	$this->sprwadzaniepobierzZGetaStronicowanie();
    }

    public function testWyszukiwaniaStronicowane_order() {
	$this->aktualnyPrzypadek = $this->przypadki[2];

	$this->sprwadzaniepobierzZGetaStronicowanie();
    }

    public function testWyszukiwaniaStronicowane_nr_strony() {
	$this->aktualnyPrzypadek = $this->przypadki[3];

	$this->sprwadzaniepobierzZGetaStronicowanie();
    }

    public function testWyszukiwaniaStronicowane_wynikow_na_stronie() {
	$this->aktualnyPrzypadek = $this->przypadki[4];

	$this->sprwadzaniepobierzZGetaStronicowanie();
    }

    private function sprwadzaniepobierzZGetaStronicowanie() {
	$this->tagiMenager->expects($this->once())->method('pobierzPaginator')
		->with($this->callback(function($builder) {
			    $this->assertEquals($this->aktualnyPrzypadek['where'], $builder->where);
			    $this->assertEquals($this->aktualnyPrzypadek['strona'], $builder->strona);
			    $this->assertEquals($this->aktualnyPrzypadek['liczba_wierszy'], $builder->wynikowNaStronie);

			    return true;
			}));
	$this->dispatch('/tests/wyszukiwanie_stronicowane?' . $this->aktualnyPrzypadek['get']);
    }

    public function testPustyPrzypadek() {
	$this->sprwadzaniepobierzZGeta($this->przypadki[0]);
    }

    public function testWyszukiwania() {
	$this->sprwadzaniepobierzZGeta($this->przypadki[1]);
    }

    public function testWyszukiwaniaSortowanie() {
	$this->sprwadzaniepobierzZGeta($this->przypadki[2]);
    }

    public function testWyszukiwaniaSortowanieDomyslne() {

	$this->tagiMenager->expects($this->once())->method('pobierzResultSetZWherem')
		->with($this->anything(), $this->equalTo('domyslne ASC'));
	$this->dispatch('/tests/wyszukiwanie_get');
    }

    public function testwyszukiwanieZDodatkowymOrder() {

	$this->tagiMenager->expects($this->once())->method('pobierzResultSetZWherem')
		->with($this->anything(), $this->equalTo(['domyslne' => 'ASC', 'kolumna1' => 'ASC', 'kolumna2' => 'ASC']));
	$this->dispatch('/tests/wyszukiwanie_z_dodatkowym_order');
    }

    public function testwyszukiwanieZDodatkowymOrder_bez_sortowania_domyślnego() {

	$this->tagiMenager->expects($this->once())->method('pobierzResultSetZWherem')
		->with($this->anything(), $this->equalTo(['kolumna1' => 'ASC', 'kolumna2' => 'ASC']));
	$this->dispatch('/tests/wyszukiwanie_z_dodatkowym_order_bez_sortowania');
    }

    private function sprwadzaniepobierzZGeta($przypadek) {
	$this->tagiMenager->expects($this->once())->method('pobierzResultSetZWherem')
		->with(
			$this->equalTo($przypadek['where']), $this->equalTo($przypadek['order'])
	);
	$this->dispatch('/tests/wyszukiwanie_get?' . $przypadek['get']);
    }

}
