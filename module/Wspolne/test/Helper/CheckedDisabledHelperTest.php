<?php

use Wspolne\Helper\CheckedDisabledHelper;



class CheckedDisabledHelperTest extends Testy\BazoweKlasyTestow\WspolnyTableMockTest {

	public function setUp() {
		parent::setUp();
	}

	public function testInvoke() {
		$this->expectOutputString("disabled checked");
		$helper = new CheckedDisabledHelper();
		$helper->__invoke(array(4, 5), 5);
	}

}
