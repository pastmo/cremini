<?php

namespace WspolneTest\Model;

use Konwersacje\Entity\Wiadomosc;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Log\Logger;
use Konwersacje\Model\WiadomosciMenager;

class WspolnyTableEventTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

	private $mock;
	private $wiadomosc;

	public function setUp() {
		parent::setUp();

		$this->wiadomosc = \FabrykaEncjiMockowych::makeEncje(Wiadomosc::class,
						\Pastmo\Testy\ParametryFabryki\PFMockowePowiazane::create());

		$this->ustawMockaGateway(\Konwersacje\Module::WIADOMOSCI_GATEWAY);

		$this->obslugaTestowanejKlasy->ustawWynikGetRekord($this->wiadomosc,
				$this->gateway);

		$this->wiadomosciMenager = new WiadomosciMenager($this->sm);

		$this->sm->setService(WiadomosciMenager::class,
				$this->wiadomosciMenager);
	}

	public function testWysylaniaZdarzenia() {

		$this->sprawdzWywolanieMocka();

		$this->sm->get(WiadomosciMenager::class)->getEventManager()->attach('zapisz',
				function($e) {
			$event = $e->getName();
			$target = get_class($e->getTarget());
			$params = $e->getParams();

			$this->mock->wykonaj();
			$this->assertEquals(WiadomosciMenager::class, $target);
			$this->assertEquals(WiadomosciMenager::ZAPISZ, $event);
			$this->assertTrue($params instanceof Wiadomosc);
		});

		$this->wiadomosciMenager->zapisz($this->wiadomosc);
	}

	public function testWysylaniaZdarzenia_wywolanie_przekazanej_funkcji() {

		$this->sprawdzWywolanieMocka();

		$logEvent = new LogEvents($this->mock);
		$logEvent->attach($this->sm->get(WiadomosciMenager::class)->getEventManager());

		$this->wiadomosciMenager->zapisz($this->wiadomosc);
	}

	private function sprawdzWywolanieMocka() {
		$this->mock = $this->getMockBuilder('WspolneTest\Model\WspolnyTableEventTest')
				->disableOriginalConstructor()
				->getMock();
		$this->mock->expects($this->once())->method('wykonaj');

		$this->sm->get(WiadomosciMenager::class);
	}

	public function wykonaj() {

	}

}

class LogEvents extends \Pastmo\Wspolne\Menager\WspolnyMenager {

	private $mock;

	public function __construct($mock) {
		$this->mock = $mock;
	}

	public function attach(EventManagerInterface $events, $priority = 1) {
		$this->listeners[] = $events->attach('zapisz', [$this, 'log']);
	}

	public function log(EventInterface $e) {
		$event = $e->getName();
		$params = $e->getParams();
		$this->mock->wykonaj();
	}

}
