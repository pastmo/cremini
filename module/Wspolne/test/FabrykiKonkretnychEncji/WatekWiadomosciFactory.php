<?php

use Konwersacje\Enumy\WatkiWiadomosciTypy;

class WatekWiadomosciFactory extends FabrykaAbstrakcyjna {

    public $uzytkownik;
    public $wiadomosci = array();
    public $watek;
    public $uzytkownikWatek;
    public $wiadomosci_z_chatu=array();

    public function nowaEncja() {
	$encja = new Konwersacje\Entity\WatekWiadomosci($this->parametryFabryki->sm);
	$encja->temat = 'temat';
	$encja->typ = \Konwersacje\Enumy\WatkiWiadomosciTypy::PRYWATNY;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return Konwersacje\Model\WatkiWiadomosciTable::class;
    }

    public function dowiazInneTabele() {
	parent::dowiazInneTabele();
    }

    public static function create(Pastmo\Testy\ParametryFabryki\ParametryFabryki $pf) {
	$projektFactory = new WatekWiadomosciFactory($pf);
	return $projektFactory;
    }

    public function utworzWatekZWiadomosciamiIPowiazZZalogowanymInowymUzytownikiem($liczbaWiadomosci = 2,
	    $typ = WatkiWiadomosciTypy::PRYWATNY) {
	$this->watek = FabrykaRekordow::makeEncje(Konwersacje\Entity\WatekWiadomosci::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane::create($this->parametryFabryki->sm)
				->setparametry(array('typ' => $typ)));

	for ($i = 0; $i < $liczbaWiadomosci; $i++) {

	    $z_chatu = \in_array($i, $this->wiadomosci_z_chatu) ? '1' : '0';

	    $this->wiadomosci[] = FabrykaRekordow::makeEncje(Konwersacje\Entity\Wiadomosc::class,
			    \Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->parametryFabryki->sm)
				    ->setparametry(array('watek' => $this->watek->id, 'z_chatu' => $z_chatu)));
	}

	$this->uzytkownik = FabrykaRekordow::makeEncje(Logowanie\Model\Uzytkownik::class,
			\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->parametryFabryki->sm));

	$zalogowany=$this->parametryFabryki->sm->get(\Logowanie\Fasada\LogowanieFasada::class)->getZalogowanegoUsera();
	$uzytkownicy = array($this->uzytkownik, $zalogowany);

	foreach ($uzytkownicy as $user) {
	    $this->dodajUzytkownikWatekWiadomosci($this->watek,$user);
	}
    }

    public function utworzWatekZUzytkownikiem() {
	$this->uzytkownik = $this->uzyjWlasciwejFabryki(\Logowanie\Model\Uzytkownik::class);
	$this->watek = $this->uzyjWlasciwejFabryki(\Konwersacje\Entity\WatekWiadomosci::class);
	$this->uzytkownikWatek = $this->uzyjWlasciwejFabryki(Konwersacje\Entity\UzytkownikWatkekWiadomosci::class,
		array('uzytkownik' => $this->uzytkownik, 'watek_wiadomosci' => $this->watek));
    }

    private function dodajUzytkownikWatekWiadomosci($watek,$user){
	$parametry=array('watek_wiadomosci' => $watek, 'uzytkownik' => $user);

	    FabrykaRekordow::makeEncje(\Konwersacje\Entity\UzytkownikWatkekWiadomosci::class,
		    \Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisNiepowiazane::create($this->parametryFabryki->sm)
			    ->setparametry($parametry));
    }

}
