<?php


class UzytkownikProjektFactory extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new Logowanie\Entity\UzytkownikProjekt();
		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return Logowanie\Menager\UzytkownicyProjektyMenager::class;
	}

	public function dowiazInneTabele() {
		parent::dowiazInneTabele();

		$this->encja->uzytkownik = $this->uzyjWlasciwejFabryki(Logowanie\Model\Uzytkownik::class);
		$this->encja->projekt = $this->uzyjWlasciwejFabryki(Projekty\Entity\Projekt::class);

	}


}