<?php

class WiadomoscTagFactory  extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new \Konwersacje\Entity\WiadomoscTag;
		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return Konwersacje\Menager\WiadomosciTagiMenager::class;
	}

}
