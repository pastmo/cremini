<?php

class FirmaFactory extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new Firmy\Entity\Firma();
		$encja->nazwa = "nazwa firmy";
		$encja->img= new Zasoby\Model\Zasob;
		$encja->nip=  $this->genrujId();
		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return Firmy\Menager\FirmyMenager::class;
	}

	public function dowiazInneTabele() {
		parent::dowiazInneTabele();
		$this->encja->adres_faktury = FabrykaRekordow::utworzEncje(NULL,
						Firmy\Entity\Adres::class, NULL, NULL,
						\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane::create($this->parametryFabryki->sm));
		$this->encja->adres_wysylki = FabrykaRekordow::utworzEncje(NULL,
						Firmy\Entity\Adres::class, NULL, NULL,
						\Pastmo\Testy\ParametryFabryki\PFIntegracyjneZapisPowiazane::create($this->parametryFabryki->sm));
	}

}
