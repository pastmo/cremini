<?php

class KonkurencjaFactory extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new BazaProduktow\Entity\Konkurencja();
		$encja->cena = rand(1, 100);
		$encja->ilosc = rand(1, 100);
		$encja->nazwa = "nowa_nazwa";
		$encja->komentarz = "moj_komentarz";
		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return BazaProduktow\Menager\KonkurencjaMenager::class;
	}

	public function dowiazInneTabele() {
		parent::dowiazInneTabele();

		if ($this->parametryFabryki->czyFabrykaMockow) {
			$this->encja->produkt_wariacja = \FabrykaEncjiMockowych::utworzEncje(\BazaProduktow\Entity\ProduktWariacja::class);
		} else {
			$this->encja->produkt_wariacja = \FabrykaRekordow::makeEncje(\BazaProduktow\Entity\ProduktWariacja::class, $this->parametryFabryki);
		}
	}

}
