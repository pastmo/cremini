<?php

class PomocFactory extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new \Dashboard\Entity\Pomoc();

		$encja->kod = "1234567";
		$encja->opis = "Standardowy opis.";
		$encja->tresc = "To jest standardowa tresc pomocy.";

		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return Dashboard\Menager\PomocMenager::getClass();
	}

	public function dowiazInneTabele() {
		parent::dowiazInneTabele();
	}

}
