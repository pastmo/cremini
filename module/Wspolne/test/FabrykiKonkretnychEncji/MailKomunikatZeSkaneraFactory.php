<?php

class MailKomunikatZeSkaneraFactory  extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new Pastmo\Email\Entity\MailKomunikatZeSkanera(new \Zasoby\Menager\ZasobyUploadMenager());
		$encja->subject="Temat";

		return $encja;
	}

	public function pobierzNazweTableMenager() {
		throw new \Exception(__CLASS__." Nie zapisuje się do bazy danych");
	}

}
