<?php

class ProduktWariacjaProduktWariacjaFactory extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$this->encja = new \BazaProduktow\Entity\ProduktWariacjaProduktWariacja();
		$this->encja->typ = \Produkty\Enumy\TypKategoriaProduktu::PODSTAWKA;
		return $this->encja;
	}

	public function pobierzNazweTableMenager() {
		return \BazaProduktow\Menager\ProduktyWariacjeProduktyWariacjeMenager::class;
	}

	public function dowiazInneTabele() {
		parent::dowiazInneTabele();

		if ($this->parametryFabryki->czyFabrykaMockow) {
			$this->encja->wariacja_glowna = \FabrykaEncjiMockowych::utworzEncje(\BazaProduktow\Entity\ProduktWariacja::class);
			$this->encja->wariacja_dodatkowa = \FabrykaEncjiMockowych::utworzEncje(\BazaProduktow\Entity\ProduktWariacja::class);
		} else {
			$this->encja->wariacja_glowna = \FabrykaRekordow::makeEncje(
					\BazaProduktow\Entity\ProduktWariacja::class, $this->parametryFabryki);
			$this->encja->wariacja_dodatkowa = \FabrykaRekordow::makeEncje(
					\BazaProduktow\Entity\ProduktWariacja::class, $this->parametryFabryki);
		}
	}

}
