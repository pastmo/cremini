<?php


class MagazynFactory extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new Magazyn\Entity\Magazyn();
		$encja->nazwa= "nazwa magazynu";
		$encja->typ=  \Magazyn\Enumy\MagazynyTypy::MAGAZYN;
		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return Magazyn\Menager\MagazynyMenager::class;
	}

	public function dowiazInneTabele() {
		parent::dowiazInneTabele();

		if ($this->parametryFabryki->czyFabrykaMockow) {
		}

	}


}