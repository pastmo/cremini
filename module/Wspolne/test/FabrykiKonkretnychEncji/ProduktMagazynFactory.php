<?php

class ProduktMagazynFactory extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new BazaProduktow\Entity\ProduktMagazyn();
		$encja->ilosc = rand(10, 100);
		$encja->polka = "polka";

		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return BazaProduktow\Menager\ProduktyMagazynMenager::class;
	}

	public function dowiazInneTabele() {
		parent::dowiazInneTabele();

		if ($this->parametryFabryki->czyFabrykaMockow) {
			$this->encja->produkt_wariacja = \FabrykaEncjiMockowych::utworzEncje(\BazaProduktow\Entity\ProduktWariacja::class);
			$this->encja->magazyn = \FabrykaEncjiMockowych::utworzEncje(\Magazyn\Entity\Magazyn::class);
		} else {
			$this->encja->produkt_wariacja = \FabrykaRekordow::makeEncje(\BazaProduktow\Entity\ProduktWariacja::class, $this->parametryFabryki);
			$this->encja->magazyn = \FabrykaRekordow::makeEncje(\Magazyn\Entity\Magazyn::class, $this->parametryFabryki);
		}
	}

}
