<?php

class ProduktMagazynOdpadFactory extends FabrykaAbstrakcyjna {

	public function nowaEncja() {
		$encja = new \BazaProduktow\Entity\ProduktMagazynOdpad();
		$encja->ilosc = rand(1, 100);
		$encja->komentarz = "komentarz";
		$encja->powod = \BazaProduktow\Enumy\ProduktOdpadPowod::TRANSPORT;

		return $encja;
	}

	public function pobierzNazweTableMenager() {
		return BazaProduktow\Menager\ProduktyMagazynOdpadyMenager::class;
	}

	public function dowiazInneTabele() {
		parent::dowiazInneTabele();

		if ($this->parametryFabryki->czyFabrykaMockow) {
			$this->encja->uzytkownik = \FabrykaEncjiMockowych::utworzEncje(\Logowanie\Model\Uzytkownik::class);
			$this->encja->produkt_magazyn = \FabrykaEncjiMockowych::utworzEncje(\BazaProduktow\Entity\ProduktMagazyn::class);
		} else {
			$this->encja->uzytkownik = \FabrykaRekordow::makeEncje(\Logowanie\Model\Uzytkownik::class, $this->parametryFabryki);
			$this->encja->produkt_magazyn = \FabrykaRekordow::makeEncje(\BazaProduktow\Entity\ProduktMagazyn::class, $this->parametryFabryki);
		}
	}

}
