<?php

namespace WspolneTest;

require_once dirname(__FILE__) . '/../../Wspolne/test/WspolneBootstrap.php';

chdir(__DIR__);

class Bootstrap extends \WspolneTest\WspolneBootstrap {

}

Bootstrap::init(array(
	'Application','Wspolne', 'Logowanie','Projekty','Konwersacje','Firmy','Produkty','Zasoby','Email','Pastmo'
));
Bootstrap::chroot();
