<?php

namespace WspolneTest\Menager;

class PodpowiadanieMenagerTest extends \Testy\BazoweKlasyTestow\WspolnyTableMockTest {

    private $podpowiedziMenager;
    private $iloscMenagerow;

    public function setUp() {
	parent::setUp();
	$this->podpowiedziMenager = $this->sm->get(\Wspolne\Menager\PodpowiadanieMenager::class);
    }

    public function test_pobierzPodpowiedzi() {
	$this->ustawMocki();
	$wynik = $this->podpowiedziMenager->pobierzPodpowiedzi("zapytanie");

	$this->assertEquals($this->iloscMenagerow, count($wynik));
    }

    private function ustawMocki() {
	$this->ustawZalogowanegoUzytkownika();
	$this->ustawPobieranieUprawnien();
	$menagery = $this->podpowiedziMenager->pobierzKlasyModulow();
	$this->iloscMenagerow = count($menagery);
	foreach ($menagery as $klasaMenagera) {
	    $this->ustawPojedynczyMock($klasaMenagera);
	}
    }

    private function ustawPojedynczyMock($nazwaKlasy) {
	$this->obslugaKlasObcych->ustawMetode($nazwaKlasy, 'wyszukaj',
		array(new \Pastmo\Wspolne\Entity\RekordWyszukiwania("wysz1" . $nazwaKlasy, "wysz2" . $nazwaKlasy, "wysz3")),
		1);
    }

    private function ustawZalogowanegoUzytkownika() {
	$user = new \Logowanie\Model\Uzytkownik();
	$user->id = 42;
	$user->rola = new \Logowanie\Entity\Rola($this->sm);
	$user->rola->id = 23;
	$this->obslugaKlasObcych->ustawZalogowanegoUsera($user, -1);
    }

    private function ustawPobieranieUprawnien() {
	$uprawnienia[] = \Logowanie\Enumy\KodyUprawnien::PRZEGLADANIE_PROFILII_UZYTKOWNIKOW;
	$uprawnienia[] = \Logowanie\Enumy\KodyUprawnien::MENU_PROJEKTY;
	$uprawnienia[] = \Logowanie\Enumy\KodyUprawnien::MENU_PRODUKTY;
	$uprawnienia[] = \Logowanie\Enumy\KodyUprawnien::MENU_FIRMY;
	$tablicaUprawnienUzytkownika = array();

	foreach ($uprawnienia as $uprawnienie) {
	    $tablicaUprawnienUzytkownika[] = $this->zwrocUprawnienie($uprawnienie);
	}

	$this->obslugaKlasObcych->ustawMetode(\Logowanie\Menager\UprawnieniaMenager::class, 'pobierzUprawnieniaDlaRoli', $tablicaUprawnienUzytkownika);
    }

    private function zwrocUprawnienie($kod) {
	$uprawnienie = new \Logowanie\Entity\Uprawnienie();
	$uprawnienie->opis = 'Opis uprawnienia';
	$uprawnienie->kod = $kod;
	return $uprawnienie;
    }

}
