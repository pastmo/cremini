<?php
return array(
     'controllers' => array(
         'factories' => array(
             'Klienci\Controller\Klienci' => 'Klienci\Controller\Factory\KlienciControllerFactory',
             'Klienci\Controller\KlienciAjax' => 'Klienci\Controller\Factory\KlienciAjaxControllerFactory',
		),
     ),
	  'router' => array(
         'routes' => array(
             'klienci' => array(
				'type'    => 'segment',
                 'options' => array(
                     'route' => '/klienci[/:action][/:id][.:format]',
					'constraints' => array(
                         'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                         'id'     => '[0-9]+',
                     ),
                     'defaults' => array(
                         'controller' => 'Klienci\Controller\Klienci',
						'action'     => 'index',
                     ),
                 ),
             ),
             'klienci_ajax' => array(
				'type'    => 'segment',
                 'options' => array(
                     'route' => '/klienci_ajax[/:action][/:id][.:format]',
					'constraints' => array(
                         'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                         'id'     => '[0-9]+',
                     ),
                     'defaults' => array(
                         'controller' => 'Klienci\Controller\KlienciAjax',
						'action'     => 'index',
                     ),
                 ),
             ),
         ),
     ),

     'view_manager' => array(
         'template_path_stack' => array(
             'Klienci' => __DIR__ . '/../view',
		),
     ),
 );