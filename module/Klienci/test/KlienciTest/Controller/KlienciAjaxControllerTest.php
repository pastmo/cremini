<?php

namespace KlienciTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zasoby\Controller\ZasobyController;
use Klienci\Controller\KlienciController;

class KlienciAjaxControllerTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest {

    public function setUp() {
	parent::setUp();
    }

    public function testIndex() {

    }

    public function test_zapiszNotatkeAction() {
	$this->ustawParametryPosta(['notatka' => __CLASS__]);
	$this->klienciMenager->expects($this->once())->method('zapiszNotatkeKlienta')->with($this->equalTo(42),$this->equalTo(__CLASS__));
	$this->dispatch("/klienci_ajax/zapisz_notatke/42");
	$this->sprawdzCzySukces();
    }

}
