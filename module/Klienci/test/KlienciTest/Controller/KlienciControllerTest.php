<?php

namespace KlienciTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zasoby\Controller\ZasobyController;
use Klienci\Controller\KlienciController;

class KlienciControllerTest extends \Testy\BazoweKlasyTestow\WspolnyControllerCommonTest{

	public function setUp() {
		parent::setUp();
	}

	public function testIndex() {
		$this->ustawZalogowanegoUsera();
		parent::testIndex();
	}

	public function getBigName() {
		return 'Klienci';
	}

	public function getSmallName() {
		return 'klienci';
	}

	public function testWhere() {
		$text = "abc";
		$expect = "uzytkownik_id in(select id from uzytkownicy where imie like '%" . $text . "%' or nazwisko like '%" . $text . "%')";

		$kontroller = new KlienciController();
		$where = $kontroller->getWhere($text);

		$this->assertEquals($expect, $where);
	}
	public function testWhereNull() {
		$text =null;
		$expect = "1";

		$kontroller = new KlienciController();
		$where = $kontroller->getWhere($text);

		$this->assertEquals($expect, $where);
	}

}
