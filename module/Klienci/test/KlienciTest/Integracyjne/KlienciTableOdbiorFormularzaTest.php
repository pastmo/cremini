<?php //
//TODO: Aktywować test
//namespace ZleceniaTest\Integracyjne;
//
//use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
//
//class KlienciTableOdbiorFormularzaTest extends AbstractHttpControllerTestCase {
//
//	protected $formularz = array("nazwa" => "ComputerLand",
//		"status" => "Zrealizowany",
//		"imie" => "Grzegorz",
//		"nazwisko" => "Latał",
//		"ulica" => "ul. Porzeczkowa 22",
//		"kod" => "00-158",
//		"miasto" => "Warszawa",
//		"mail" => "idz_na_grzyby@wp.pl",
//		"telefon" => "+48456789123",
//		"www" => "www.grzybobranie.pl",
//		"nazwa_na_fakturze" => "ComputerLand Grzegorz i przyjaciele",
//		"faktura_ulica" => "ul. Wiśni Słodkiej",
//		"faktura_kod" => "85-555",
//		"faktura_miasto" => "Stare Purchawki",
//		"nip" => "7896541235",
//		"notatka" => "Wspaniały, wiarygodny klient!",
//		"czy_nowe_pole" => "on",
//		"dodatkowe_pole1" => "Dooodatkowe pole1",
//		"dodatkowe_pole2" => "Dooodatkowe pole2",
//		"dodatkowe_pole3" => "Dooodatkowe pole3",
//		"plik" => "hszan.jpg");
//
//	public function setUp() {
//		$this->setApplicationConfig(
//				include \Application\Stale::configTestPath
//		);
//		parent::setUp();
//
//		$this->serviceManager = $this->getApplicationServiceLocator();
//		$this->klienciTable = $this->serviceManager->get('Klienci\Model\KlienciTable');
//	}
//
//	public function testZapisywaniaFormularza() {
//		$this->klienciTable->zapiszKlienta($this->formularz);
//
//		$klient = $this->klienciTable->getPoprzednioDodany();
//
//		$this->assertEquals($this->formularz['nazwa'], $klient->nazwa);
//	}
//
//	public function testZapisywaniaFormularza_uzytkownik() {
//		$this->klienciTable->zapiszKlienta($this->formularz);
//
//		$klient = $this->klienciTable->getPoprzednioDodany();
//
//		$this->assertEquals($this->formularz['imie'], $klient->uzytkownik->imie);
//		$this->assertEquals(\Logowanie\Model\TypyUzytkownikow::KLIENT, $klient->uzytkownik->typ);
//	}
//
//	public function testZapisywaniaFormularza_adres_zwykly() {
//		$this->klienciTable->zapiszKlienta($this->formularz);
//
//		$klient = $this->klienciTable->getPoprzednioDodany();
//
//		$this->assertEquals($this->formularz['ulica'], $klient->adres->ulica);
//		$this->assertEquals($this->formularz['kod'], $klient->adres->kod);
//		$this->assertEquals($this->formularz['miasto'], $klient->adres->miasto);
//		$this->assertEquals('Polska', $klient->adres->kraj);
//	}
//
//	public function testZapisywaniaFormularza_adres_faktury() {
//		$this->klienciTable->zapiszKlienta($this->formularz);
//
//		$klient = $this->klienciTable->getPoprzednioDodany();
//
//		$this->assertEquals($this->formularz['faktura_ulica'], $klient->adres_faktury->ulica);
//		$this->assertEquals($this->formularz['faktura_kod'], $klient->adres_faktury->kod);
//		$this->assertEquals($this->formularz['faktura_miasto'], $klient->adres_faktury->miasto);
//		$this->assertEquals('Polska', $klient->adres->kraj);
//	}
//
//	public function testZapisywaniaFormularza_dodatkowe_pola() {
//		$this->klienciTable->zapiszKlienta($this->formularz);
//
//		$klient = $this->klienciTable->getPoprzednioDodany();
//
//		$dodatkowePola = $klient->getDodatkowePola();
//
//		$this->assertEquals($this->formularz['dodatkowe_pole3'], $dodatkowePola[0]->wartosc);
//		$this->assertEquals($this->formularz['dodatkowe_pole2'], $dodatkowePola[1]->wartosc);
//		$this->assertEquals($this->formularz['dodatkowe_pole1'], $dodatkowePola[2]->wartosc);
//	}
//
//	public function testZapisywaniaFormularza_notatka() {
//		$this->klienciTable->zapiszKlienta($this->formularz);
//
//		$klient = $this->klienciTable->getPoprzednioDodany();
//
//		$notatki = $klient->getNotatki();
//
//		$this->assertEquals($this->formularz['notatka'], $notatki[0]->tekst);
//	}
//
//}
