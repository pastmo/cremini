<?php

namespace ZleceniaTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Klienci\Entity\Adres;

class AdresyTableTest extends AbstractHttpControllerTestCase {

	protected $adresTable;
	protected $ulica_adres = "ulica_adres 2/4";
	protected $kod_adres = "00-000";
	protected $miasto_adres = "Miasto";
	protected $kraj_adres = "KrajAdres";

	public function setUp() {
		$this->setApplicationConfig(
				include \Application\Stale::configTestPath
		);
		parent::setUp();

		$this->serviceManager = $this->getApplicationServiceLocator();
		$this->adresTable = $this->serviceManager->get(\Klienci\Module::ADRESY_TABLE);
	}

	public function test_dodajRekord() {

		$encja = new Adres();
		$encja->ulica = $this->ulica_adres;
		$encja->kod = $this->kod_adres;
		$encja->miasto = $this->miasto_adres;
		$encja->kraj = $this->kraj_adres;
		$this->adresTable->zapisz($encja);

		$ostatniRekord = $this->adresTable->getPoprzednioDodany();

		$this->assertEquals($ostatniRekord->ulica, $this->ulica_adres);
		$this->assertEquals($ostatniRekord->kod, $this->kod_adres);
		$this->assertEquals($ostatniRekord->ulica, $this->ulica_adres);
		$this->assertEquals($ostatniRekord->miasto, $this->miasto_adres);
		$this->assertEquals($ostatniRekord->kraj, $this->kraj_adres);
	}

}
