<?php

namespace ZleceniaTest\Controller;

use Klienci\Entity\Klient;
use Pastmo\Testy\Util\TB;

class KlienciMenagerTest extends \Pastmo\Testy\BazoweKlasyTestow\WspolnyIntegracyjnyTest {

    private $post = ['id' => '',
	    'uzytkownik_id' => "",
	    'adres_id' => "",
	    'nazwa' => "Nazwa klienta",
	    'status' => "Nowy",
	    'imie' => "",
	    'nazwisko' => "",
	    'ulica' => "ulica",
	    'kod' => "kod",
	    'miasto' => "miejscowość",
	    'nip' => "4543",
	    'email' => "asdf@dd.ds",
	    'telefon' => "99999999",
	    'nazwa_na_fakturze' => "",
	    'faktura_ulica' => "",
	    'faktura_kod' => "",
	    'faktura_miasto' => "",
	    'notatka' => __CLASS__,
	    'dodatkowe_nazwa1' => "",
	    'dodatkowe_pole1' => "",
	    'submit' => "Zapisz"];

    public function setUp() {
	parent::setUp();
	$this->utworzIZalogujUzytkownika();

	$this->klienciTable = $this->sm->get('Klienci\Model\KlienciTable');
    }

    public function test_dodajRekord() {
	$nazwa = 'test_dodajRekord';

	$klient = new Klient();
	$klient->nazwa = $nazwa;
	$klient->notatka = __CLASS__;
	$this->klienciTable->zapisz($klient);

	$ostatniRekord = $this->klienciTable->getPoprzednioDodany();

	$this->assertEquals($nazwa, $ostatniRekord->nazwa);
	$this->assertEquals(__CLASS__, $ostatniRekord->nazwa);
    }

    public function test_zapiszKlienta() {

	$post = $this->arrayToParameters($this->post);
	$this->klienciTable->zapiszKlienta($post);

	$ostatniRekord = $this->klienciTable->getPoprzednioDodany();

	$this->assertEquals($this->post['nazwa'], $ostatniRekord->nazwa);
	$this->assertEquals($this->post['notatka'], $ostatniRekord->notatka);
	$this->sprawdzTabelePowiazane($ostatniRekord);
    }

    public function test_zapiszKlienta_istniejacy() {

	$klient = TB::create(Klient::class, $this)->make();
	$this->post['id'] = $klient->id;

	$post = $this->arrayToParameters($this->post);
	$this->klienciTable->zapiszKlienta($post);

	$ostatniRekord = $this->klienciTable->getPoprzednioDodany();

	$this->assertEquals($klient->id, $ostatniRekord->id);
	$this->sprawdzTabelePowiazane($ostatniRekord);
    }

    public function test_zapiszNotatkeKlienta() {
	$klient = TB::create(Klient::class, $this)->make();
	$this->klienciTable->zapiszNotatkeKlienta($klient->id, "nowa notatka");

	$zapisany = $this->klienciTable->getRekord($klient->id);

	$this->assertEquals("nowa notatka", $zapisany->notatka);
    }

    private function sprawdzTabelePowiazane($ostatniRekord) {
	$zapisany = $this->klienciTable->getRekord($ostatniRekord->id);

	$this->assertNotNull($zapisany->uzytkownik->id);
	$this->assertNotNull($zapisany->adres->id);
	$this->assertNotNull($zapisany->konto->id);
    }

}
