<?php

namespace Klienci\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Klienci\Form\UploadForm;
use Wspolne\Utils\SearchPole;

class KlienciController extends \Wspolne\Controller\WspolneController {

	public function indexAction() {
		$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE,
				true);


		$pola = array(
			SearchPole::create()->setDataType('string')->setName('nazwa'),
			SearchPole::create()->setDataType('string')->setName('nazwa_skrocona'),
			SearchPole::create()->setDataType('string')->setName('nip'),
			SearchPole::create()->setDataType('string')->setName('email'),
			SearchPole::create()->setDataType('string')->setName('osoba_kontaktowa'),
			SearchPole::create()->setDataType('string')->setName('telefon'),
					SearchPole::create()->setDataType('query')->setName('adres_id')
					->setQuery('in (select id from adresy where ulica %s)'),
					SearchPole::create()->setDataType('query')->setName('adres_id')
					->setQuery('in (select id from adresy where kod %s)'),
					SearchPole::create()->setDataType('query')->setName('adres_id')
					->setQuery('in (select id from adresy where miasto %s)'),
					SearchPole::create()->setDataType('query')->setName('adres_id')
					->setQuery('in (select id from adresy where kraj %s)'),
		);

		$klienci = $this->pobierzZGeta($this->klienciTable, "1", $pola, "nazwa ASC");
//                $listaPodpowiedzi=  $this->klienciTable->pobierzNazwyKlientow();


		$viewResult = new ViewModel(
				array(
			"klienci" => $klienci,
//			"status" => $status,
//                        'listaPodpowiedzi'=>$listaPodpowiedzi,
			"statusy" => array(null, 'Nowy', 'Stały', 'Zrealizowany')
				)
		);



		$get = $this->getGet();
		$requestId = 0;
		if ($get) {
			$requestId = $get->request_id;
		}

		if ($this->params('format') === 'json') {
			$viewResult->setTemplate('klienci/klienci/index');
			$html = $this->viewRender->render($viewResult);

			return $this->returnSuccess(array(
						'widok' => $html,
						'request_id' => $requestId));
		} else {
			return $viewResult;
		}
	}

	public function getWhere($text, $status) {

		$where = "1";

		if ($text !== null) {
			$where = "uzytkownik_id in(select id from uzytkownicy where imie like '%" . $text . "%' or nazwisko like '%" . $text . "%')";
		} else if ($status !== null && $status !== "") {
			$where = "status='$status'";
		}

		return $where;
	}

	public function dodajAction() {
		return $this->dodajEdytuj("Dodaj klienta");
	}

	public function edytujAction() {
		return $this->dodajEdytuj("Edytuj klienta");
	}

	private function dodajEdytuj($tytul) {
		$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE,
				true);


		$id = $this->params()->fromRoute('id', 0);

		if ($id > 0) {
			$klient = $this->klienciTable->getRekord($id);
		} else {
			$klient = \Klienci\Entity\Klient::create();
		}
		$request = $this->getRequest();

		if ($request->isPost()) {

			$post = $request->getPost();
			$this->klienciTable->zapiszKlienta($post);

			$this->redirect()->toRoute('klienci', array('action' => 'index'));
		}



		$dodajEdytujWidok = new ViewModel(array('klient' => $klient, 'tytul' => $tytul));
		$dodajEdytujWidok->setTemplate('klienci/klienci/dodaj_edytuj');

		$glownyWidok = new ViewModel(array(
		));

		$glownyWidok->addChild($dodajEdytujWidok, 'dodaj_edytuj');

		return $glownyWidok;
	}

	public function szczegolyAction() {
		$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE,
				true);

		$id = (int) $this->params()->fromRoute('id', 0);

		$klient = $this->klienciTable->getRekord($id);

		$this->uzytkownikTable->zapiszZalacznikDlaUzytkownika($klient->uzytkownik->id,
				$this->getRequest());

		$korespondenca = array(); //$this->uzytkownikWatkekWiadomosciTable->pobierzZWherem("uzytkownik_id=".$klient->uzytkownik->id);
		return new ViewModel(array('klient' => $klient,
			'korespondencja' => $korespondenca
		));
	}

	public function usunAction() {
		$this->uprawnieniaMenager->sprawdzUprawnienie(\Logowanie\Enumy\KodyUprawnien::DOMYSLNE_UPRAWNIENIE,
				true);

		$id = (int) $this->params()->fromRoute('id', 0);

		$this->klienciTable->usun("id=$id");

		$this->redirect()->toRoute('klienci', array('action' => 'index'));
	}

	public function nowaListaAction() {

	}

	public function noweDodajAction() {

	}

	public function noweSzczegolyAction() {

	}

}
