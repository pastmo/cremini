<?php

namespace Klienci\Controller\Factory;

use Klienci\Controller\KlienciController;

class KlienciControllerFactory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

	protected function newController() {
		return new KlienciController();
	}

}
