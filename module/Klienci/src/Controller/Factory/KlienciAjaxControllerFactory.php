<?php

namespace Klienci\Controller\Factory;

use Klienci\Controller\KlienciAjaxController;

class KlienciAjaxControllerFactory extends \Wspolne\Controller\Factory\WspolneControllerFactory {

	protected function newController() {
		return new KlienciAjaxController();
	}

}
