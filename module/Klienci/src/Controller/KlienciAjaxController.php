<?php

namespace Klienci\Controller;

use Pastmo\Wspolne\Adnotacje\DomyslnyJsonModel;

/**
 * @DomyslnyJsonModel
 */
class KlienciAjaxController extends \Wspolne\Controller\WspolneController {

    public function zapiszNotatkeAction() {
	$id = $this->params()->fromRoute('id');
	$notatka = $this->params()->fromPost('notatka');

	$this->klienciTable->zapiszNotatkeKlienta($id, $notatka);
	return $this->returnSuccess();
    }

}
