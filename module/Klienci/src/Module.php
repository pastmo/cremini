<?php

namespace Klienci;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Klienci\Entity\Klient;
use Klienci\Entity\Adres;
use Klienci\Entity\DodatkowePole;
use Klienci\Entity\Notatka;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface {

	const KLIENCI_TABLE = 'Klienci\Model\KlienciTable';
	const KLIENCI_GATEWAY='KlienciGateway';
	const ADRESY_TABLE = 'Klienci\Model\AdresyTable';
	const ADRESY_GATEWAY='AdresyGateway';
	const DODATKOWE_POLA_TABLE = 'Klienci\Model\DodatkowePolaTable';
	const DODATKOWE_POLA_GATEWAY='DodatkowePolaGeteway';
	const NOTATKI_TABLE = 'Klienci\Model\NotatkiTable';
	const KLIENCI_NOTATKI_GATEWAY='KlienciNotatkiGeteway';

	public function getAutoloaderConfig() {
		return array(
			'Zend\Loader\ClassMapAutoloader' => array(
				__DIR__ . '/../autoload_classmap.php',
			),
			'Zend\Loader\StandardAutoloader' => array(
				'namespaces' => array(
					__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
				),
			),
		);
	}

	public function getConfig() {
		return include __DIR__ . '/../config/module.config.php';
	}

	public function getServiceConfig() {
		return array(
			'factories' => array(
				self::KLIENCI_TABLE => function($sm) {

					$table = $sm->get(\Pastmo\Faktury\Menager\KlienciMenager::class);
					return $table;
				},
				self::ADRESY_TABLE => function($sm) {
					$table = new Model\AdresyTable($sm);
					return $table;
				},
				self::ADRESY_GATEWAY => function ($sm) {
					$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
					$resultSetPrototype = new ResultSet();
					$resultSetPrototype->setArrayObjectPrototype(new Adres($sm));
					return new TableGateway('adresy', $dbAdapter, null, $resultSetPrototype);
				},
				self::DODATKOWE_POLA_TABLE => function($sm) {
					$table = new Model\DodatkowePolaTable($sm);
					return $table;
				},
				self::DODATKOWE_POLA_GATEWAY => function ($sm) {
					$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
					$resultSetPrototype = new ResultSet();
					$resultSetPrototype->setArrayObjectPrototype(new DodatkowePole($sm));
					return new TableGateway('klienci_dodatkowe_pola', $dbAdapter, null, $resultSetPrototype);
				},
				self::NOTATKI_TABLE => function($sm) {
					$table = new Model\NotatkiTable($sm);
					return $table;
				},
				self::KLIENCI_NOTATKI_GATEWAY => function ($sm) {
					$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
					$resultSetPrototype = new ResultSet();
					$resultSetPrototype->setArrayObjectPrototype(new Notatka($sm));
					return new TableGateway('klienci_notatki', $dbAdapter, null, $resultSetPrototype);
				},
			),
		);
	}

}
