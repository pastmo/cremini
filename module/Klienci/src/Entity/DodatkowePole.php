<?php

namespace Klienci\Entity;

use Logowanie\Model\Uzytkownik;

class DodatkowePole extends \Wspolne\Model\WspolneModel {

	public $id;
	public $nazwa;
	public $wartosc;
	public $klient;

	public function __construct($sm = null) {
		parent::__construct($sm);
	}

	public function getZlecenia() {
		$this->dowiazListyTabelObcych();
		return $this->zlecenia;
	}

	public function exchangeArray($data) {

		$this->id = $this->pobierzLubNull($data, 'id');
		$this->nazwa = $this->pobierzLubNull($data, 'nazwa');
		$this->wartosc = $this->pobierzLubNull($data, 'wartosc');
	}

	public function konwertujNaKolumneDB($nazwaWKodzie) {
		switch ($nazwaWKodzie) {
			case 'klient':
				return 'klient_id';
			default :
				return $nazwaWKodzie;
		}
	}

	public function pobierzKlase() {
		return __CLASS__;
	}

	public function dowiazListyTabelObcych() {

	}

}
