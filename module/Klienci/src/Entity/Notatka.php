<?php

namespace Klienci\Entity;

class Notatka extends \Wspolne\Model\WspolneModel {

	const domyslnyKraj = "Polska";

	public $id;
	public $tekst;
	public $klient_id;
	public $data_dodania;
	private $przedrostekZFormularza = "";

	public function exchangeArray($data) {
		$this->id = $this->pobierzLubNull($data, 'id');
		$this->tekst = $this->pobierzLubNull($data, 'tekst');
		if (!$this->tekst) {
			$this->tekst = $this->pobierzLubNull($data, 'notatka');
		}
		$this->klient_id = $this->pobierzLubNull($data, 'klient_id');
		$this->data_dodania = $this->pobierzLubNull($data, 'data_dodania');
	}

	public function dowiazListyTabelObcych() {

	}

	public function konwertujNaKolumneDB($nazwaWKodzie) {
		return $nazwaWKodzie;
	}

	public function pobierzKlase() {
		return __CLASS__;
	}

	public function pobierzPrzedrostek() {
		return $this->przedrostekZFormularza;
	}

	public function ustawPrzedrostek($przedrostek) {
		$this->przedrostekZFormularza = $przedrostek;
	}

}
