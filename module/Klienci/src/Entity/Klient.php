<?php

namespace Klienci\Entity;

use Logowanie\Model\Uzytkownik;

class Klient extends \Pastmo\Faktury\Entity\Klient {

    public function __construct($sm = null) {
	parent::__construct($sm);
    }

    public function pobierzKlase() {
	return __CLASS__;
    }

}
