<?php

namespace Klienci\Testy;

class FabrykaRekordow {

    public static function makeEncje($klasa, $parametryFabryki) {
	switch ($klasa) {
	    case \Klienci\Entity\Klient::class:
		return (new FabrykiKonkretnychEncji\KlientFactory($parametryFabryki))->makeEncje();
	}
	return false;
    }

}
