<?php

namespace Klienci\Testy\FabrykiKonkretnychEncji;

class KlientFactory extends \Pastmo\Testy\Fabryki\FabrykaAbstrakcyjna {

    public function nowaEncja() {
	$encja = new \Klienci\Entity\Klient($this->parametryFabryki->sm);
	$encja->nazwa = __NAMESPACE__;
	return $encja;
    }

    public function pobierzNazweTableMenager() {
	return \Klienci\Model\KlienciTable::class;
    }

    public function dowiazInneTabele() {

    }

}
