<?php

namespace Klienci\Model;

use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;

class NotatkiTable extends BazowyMenagerBazodanowy {

	protected $sm;

	public function __construct(ServiceManager $sm) {
		$this->sm = $sm;
		$this->tableGateway = $sm->get(\Klienci\Module::KLIENCI_NOTATKI_GATEWAY);
	}


}
