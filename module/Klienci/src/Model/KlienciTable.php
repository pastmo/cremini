<?php

namespace Klienci\Model;

use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;
use Klienci\Entity\Klient;
use Klienci\Entity\Adres;
use Klienci\Entity\Notatka;
use Logowanie\Model\Uzytkownik;
use Klienci\Entity\DodatkowePole;
use Pastmo\Wspolne\Utils\EntityUtil;

class KlienciTable extends BazowyMenagerBazodanowy {

    protected $sm;

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, 'KlienciGateway');
    }

    public function zapiszKlienta($post) {

	$klient = new Klient($this->sm);
	$klient->exchangeArray($post);
	unset($post['id']);

	if (!EntityUtil::wydobadzId($klient->uzytkownik)) {
	    $zalogowany = $this->getLogowanieFasada()->getZalogowanegoUsera();
	    $klient->uzytkownik = $zalogowany->id;
	}

	$this->zapisz($klient);

	if (!$klient->id) {
	    $klient = $this->getPoprzednioDodany();
	}

	$this->aktualizujPowiazanaTabele($klient->id, 'adres', AdresyTable::class, $post);
    }

    public function pobierzNazwyKlientow() {
	$klienci = $this->fetchAll();
	$wynik = array();

	foreach ($klienci as $klient) {
	    $wynik[] = $klient->nazwa;
	}

	return $wynik;
    }

}
