<?php

namespace Klienci\Model;

use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;

class AdresyTable extends BazowyMenagerBazodanowy {

    protected $sm;

    public function __construct(ServiceManager $sm) {
	parent::__construct($sm, \Klienci\Module::ADRESY_GATEWAY);
    }

    public function dodajAdres(\Klienci\Entity\Adres $adres) {
	$this->zapisz($adres);
	return $this->getPoprzednioDodany();
    }

}
