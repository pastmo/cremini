<?php

namespace Klienci\Model;

use Zend\ServiceManager\ServiceManager;
use Wspolne\Menager\BazowyMenagerBazodanowy;

class DodatkowePolaTable extends BazowyMenagerBazodanowy {

	protected $sm;

	public function __construct(ServiceManager $sm) {
		$this->sm = $sm;
		$this->tableGateway = $sm->get(\Klienci\Module::DODATKOWE_POLA_GATEWAY);
	}


}
