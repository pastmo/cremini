ALTER TABLE `uzytkownicy`
	ADD COLUMN `usuniety` CHAR(1) NULL DEFAULT '0' AFTER `czy_aktywny`;

DROP TABLE `ustawienia_systemu`;

CREATE TABLE `ustawienia_systemu` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`uzytkownik_id` INT(11) NULL DEFAULT NULL,
	`kod` VARCHAR(7) NOT NULL COLLATE 'utf8_polish_ci',
	`wartosc` VARCHAR(50) NOT NULL COLLATE 'utf8_polish_ci',
	PRIMARY KEY (`id`),
	INDEX `FK_ustawienia_systemu_uzytkownicy` (`uzytkownik_id`),
	CONSTRAINT `FK_ustawienia_systemu_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`) ON DELETE CASCADE
)
COLLATE='utf8_polish_ci'
ENGINE=InnoDB
;

INSERT INTO `ustawienia_systemu` (`kod`, `wartosc`) VALUES ('js_refr', '30000');

INSERT INTO `logi_kategorie` (`kod`, `nazwa`) VALUES ('email_err', 'Błędy poczty email');

ALTER TABLE `emaile`
	ADD COLUMN `licznik` INT(11) NULL DEFAULT NULL AFTER `zrodlo_id`;
