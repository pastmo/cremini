-- phpMyAdmin SQL Dump
-- version 3.5.8.1
-- http://www.phpmyadmin.net
--
-- Host: sql.bdl.pl
-- Czas wygenerowania: 29 Lip 2016, 11:49
-- Wersja serwera: 5.5.48-log
-- Wersja PHP: 5.3.29

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `pastmo_panel`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `adresy`
--

CREATE TABLE IF NOT EXISTS `adresy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ulica` text COLLATE utf16_polish_ci,
  `kod` text COLLATE utf16_polish_ci,
  `miasto` text COLLATE utf16_polish_ci,
  `kraj` text COLLATE utf16_polish_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=35 ;

--
-- Zrzut danych tabeli `adresy`
--

INSERT INTO `adresy` (`id`, `ulica`, `kod`, `miasto`, `kraj`) VALUES
(1, 'Gliniana 10/28', '30-758', 'Wrocław', 'Polska'),
(2, 'Widok 5', '50-555', 'Nowy Sącz', 'Polska'),
(3, 'Jaśkowa Dolina 49', '80-286', 'Gdańsk', 'Polska'),
(4, 'Jaśkowa Dolina 49', '80-286', 'Gdańsk', 'Polska'),
(5, 'Elektronowa 6', '94-103', 'Łódź', 'Polska'),
(6, 'Elektronowa 6', '94-103', 'Łódź', 'Polska'),
(7, 'Lubelska 22', '71-043', 'Szczecin', 'Polska'),
(8, 'Lubelska 22', '71-043', 'Szczecin', 'Polska'),
(9, 'Łęczycka 65', '45-831', 'Opole', 'Polska'),
(10, 'Łęczycka 65', '85-737', 'Bydgoszcz', 'Polska'),
(11, 'Cementowa 2', '31-983', 'Kraków', 'Polska'),
(12, 'Cementowa 2', '31-983', 'Kraków', 'Polska'),
(13, 'Katowicka 101', '43-346', 'Bielsko-Biała', 'Polska'),
(14, 'Katowicka 101', '43-346', 'Bielsko-Biała', 'Polska'),
(15, NULL, NULL, NULL, 'Polska'),
(16, NULL, NULL, NULL, 'Polska'),
(17, 'Słoneczna 44/197', '60-286', 'Poznań', 'Polska'),
(18, 'ul. Czerniakowska 47', '00-715', 'Warszawa', 'Polska'),
(19, 'ul. Czerniakowska 47', '00-715', 'Warszawa', 'Polska'),
(20, 'Chopina 22/183', '71-450', 'Szczecin', 'Polska'),
(21, '1 Maja 6', '10-118', 'Olsztyn', 'Polska'),
(22, '1 Maja 6', '10-118', 'Olsztyn', 'Polska'),
(23, 'Długa', '35-232', 'Rzeszów', 'Polska'),
(24, 'Sienkiewicza 15/208', '43-300', 'Bielsko-Biała', 'Polska'),
(25, 'Kunickiego 20-24', '20-417', 'Lublin', 'Polska'),
(26, 'Kunickiego 20-24', '20-417', 'Lublin', 'Polska'),
(27, 'Elbląska 54', '80-724', 'Gdańsk', 'Polska'),
(28, 'Elbląska 54', '80-724', 'Gdańsk', 'Polska'),
(29, 'Katowicka 3/177', '45-061', 'Opole', 'Polska'),
(30, 'Libertów, Zgodna 217', '30-444', 'Kraków', 'Polska'),
(31, 'Zgodna 44', '30-444', 'Kraków', 'Polska'),
(32, 'Świdry 122 a', '21-400', 'Łuków', 'Polska'),
(33, 'Taneczna 20', '30-444', 'Warszawa', 'Polska'),
(34, 'Test', 'Test', 'Test', 'Polska');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cennik_zlecen`
--

CREATE TABLE IF NOT EXISTS `cennik_zlecen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oplata_id` int(11) DEFAULT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `co_zrobilem` text COLLATE utf16_polish_ci,
  `koszt` decimal(10,2) DEFAULT NULL,
  `status` enum('nowe','oczekuje na weryfikacje','opłacone') COLLATE utf16_polish_ci DEFAULT 'nowe',
  PRIMARY KEY (`id`),
  KEY `FK_cennik_zlecen_cennik_zlecen_oplaty` (`oplata_id`),
  KEY `FK_cennik_zlecen_uzytkownicy` (`uzytkownik_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=4 ;

--
-- Zrzut danych tabeli `cennik_zlecen`
--

INSERT INTO `cennik_zlecen` (`id`, `oplata_id`, `uzytkownik_id`, `co_zrobilem`, `koszt`, `status`) VALUES
(1, 5, 1, '', '0.00', 'oczekuje na weryfikacje'),
(2, 5, 1, 'dfghnkm', '62.00', 'oczekuje na weryfikacje'),
(3, 5, 1, 'www', '100.00', 'oczekuje na weryfikacje');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cennik_zlecen_oplaty`
--

CREATE TABLE IF NOT EXISTS `cennik_zlecen_oplaty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `kwota` decimal(10,2) NOT NULL DEFAULT '0.00',
  `status` enum('oczekuje na weryfikacje','opłacone') COLLATE utf16_polish_ci NOT NULL DEFAULT 'oczekuje na weryfikacje',
  `data_dodania` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_cennik_zlecen_oplaty_uzytkownicy` (`uzytkownik_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=6 ;

--
-- Zrzut danych tabeli `cennik_zlecen_oplaty`
--

INSERT INTO `cennik_zlecen_oplaty` (`id`, `uzytkownik_id`, `kwota`, `status`, `data_dodania`) VALUES
(1, 1, '62.00', 'oczekuje na weryfikacje', '2016-05-10 17:15:05'),
(2, 1, '162.00', 'oczekuje na weryfikacje', '2016-05-11 17:01:10'),
(3, 1, '162.00', 'oczekuje na weryfikacje', '2016-05-11 17:01:30'),
(4, 1, '162.00', 'oczekuje na weryfikacje', '2016-07-20 10:11:00'),
(5, 1, '162.00', 'oczekuje na weryfikacje', '2016-07-20 10:11:06');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `domeny`
--

CREATE TABLE IF NOT EXISTS `domeny` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `klient_id` int(11) DEFAULT NULL,
  `serwer_id` int(11) DEFAULT NULL,
  `nazwa` text COLLATE utf16_polish_ci,
  `adres` text COLLATE utf16_polish_ci,
  `rodzaj` enum('Katalog','Wordpress','Zaślepka','Inne') COLLATE utf16_polish_ci NOT NULL DEFAULT 'Katalog',
  `serwer` text COLLATE utf16_polish_ci,
  `data_rejestracji` date DEFAULT NULL,
  `data_wygasniecia` date DEFAULT NULL,
  `ip` text COLLATE utf16_polish_ci,
  `dns` text COLLATE utf16_polish_ci,
  `dns2` text COLLATE utf16_polish_ci,
  `seo_tytul` text COLLATE utf16_polish_ci,
  `seo_opis` text COLLATE utf16_polish_ci,
  `seo_keywords` text COLLATE utf16_polish_ci,
  `ostatni_wpis_uzytkownik` text COLLATE utf16_polish_ci,
  `ostatni_wpis_link` text COLLATE utf16_polish_ci,
  `ostatni_wpis_data` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_domeny_klienci` (`klient_id`),
  KEY `FK_domeny_serwery` (`serwer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=4 ;

--
-- Zrzut danych tabeli `domeny`
--

INSERT INTO `domeny` (`id`, `klient_id`, `serwer_id`, `nazwa`, `adres`, `rodzaj`, `serwer`, `data_rejestracji`, `data_wygasniecia`, `ip`, `dns`, `dns2`, `seo_tytul`, `seo_opis`, `seo_keywords`, `ostatni_wpis_uzytkownik`, `ostatni_wpis_link`, `ostatni_wpis_data`) VALUES
(1, 13, NULL, 'Sklep internetowy ARMANDIO', 'www.armandio.pl', 'Inne', NULL, '2016-05-06', '2017-05-11', '25651585815', '51s15AS1C5A1885', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 4, NULL, 'Dj Oles', 'www.djoles.com', 'Wordpress', NULL, '0000-00-00', '0000-00-00', NULL, NULL, NULL, 'dj na wesele - djoles', 'djoles dobry jest', 'Dj oles, wesel, dj na wesele', 'ryjek', NULL, '0000-00-00 00:00:00'),
(3, 13, 1, 'SWISSDENT.pl', 'www.swissdent.pl', 'Wordpress', NULL, '2016-07-20', '2017-07-20', '25651585815', '51s15AS1C5A1885', NULL, 'dentysta kraków, gabinet dentystyczny, dentysta dla dzieci', 'test test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test test', 'test test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testv', 'nero', 'www.swissdent.pl/blog', '2017-07-10 19:12:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `domeny_pracownicy`
--

CREATE TABLE IF NOT EXISTS `domeny_pracownicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domena_id` int(11) DEFAULT NULL,
  `pracownik_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_domeny_pracownicy_domeny` (`domena_id`),
  KEY `FK_domeny_pracownicy_pracownicy` (`pracownik_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci COMMENT='W celu obsłużenia kolumny redaktorzy/blogerzy' AUTO_INCREMENT=7 ;

--
-- Zrzut danych tabeli `domeny_pracownicy`
--

INSERT INTO `domeny_pracownicy` (`id`, `domena_id`, `pracownik_id`) VALUES
(1, 1, 21),
(2, 1, 20),
(3, 1, 19),
(4, 1, 18),
(5, 2, 20),
(6, 3, 21);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `faktury`
--

CREATE TABLE IF NOT EXISTS `faktury` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `klient_id` int(11) DEFAULT NULL,
  `tytul` varchar(50) COLLATE utf16_polish_ci DEFAULT NULL,
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `typ` enum('Faktury zakupowe','WZ-ki','Faktury zewnętrzne','Rozliczenia miesięczne','Rozliczenia roczne','Zwroty/reklamacje','Sprzedaż bezpośrednia') COLLATE utf16_polish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_faktury_klienci` (`klient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=2 ;

--
-- Zrzut danych tabeli `faktury`
--

INSERT INTO `faktury` (`id`, `klient_id`, `tytul`, `data_dodania`, `typ`) VALUES
(1, NULL, 'test', '2016-07-20 10:06:49', 'Faktury zakupowe');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `faktury_zasoby`
--

CREATE TABLE IF NOT EXISTS `faktury_zasoby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `faktura_id` int(11) DEFAULT NULL,
  `zasob_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_faktury_zasoby_faktury` (`faktura_id`),
  KEY `FK_faktury_zasoby_zasoby` (`zasob_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `informacje_seo`
--

CREATE TABLE IF NOT EXISTS `informacje_seo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domena_id` int(11) DEFAULT NULL,
  `tresc` text COLLATE utf16_polish_ci NOT NULL,
  `data_dodania` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_informacje_seo_domeny` (`domena_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klienci`
--

CREATE TABLE IF NOT EXISTS `klienci` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `adres_id` int(11) DEFAULT NULL,
  `adres_faktury_id` int(11) DEFAULT NULL,
  `nazwa` text COLLATE utf16_polish_ci,
  `nazwa_na_fakturze` text COLLATE utf16_polish_ci,
  `nip` tinytext COLLATE utf16_polish_ci,
  `status` enum('Nowy','Stały','Zrealizowany') COLLATE utf16_polish_ci DEFAULT 'Nowy',
  `www` text COLLATE utf16_polish_ci,
  `notatka` text COLLATE utf16_polish_ci,
  PRIMARY KEY (`id`),
  KEY `FK_klienci_uzytkownicy` (`uzytkownik_id`),
  KEY `FK_klienci_adresy` (`adres_id`),
  KEY `FK_klienci_adresy_2` (`adres_faktury_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=15 ;

--
-- Zrzut danych tabeli `klienci`
--

INSERT INTO `klienci` (`id`, `uzytkownik_id`, `adres_id`, `adres_faktury_id`, `nazwa`, `nazwa_na_fakturze`, `nip`, `status`, `www`, `notatka`) VALUES
(1, 3, 1, 2, 'Pastmo', NULL, '1234567891', 'Stały', 'www.pastmo.pl', NULL),
(2, 4, 3, 4, 'Mediafocus sp. z o.o.', 'Mediafocus sp. z o.o.', '585', 'Nowy', 'http://www.mediafocus.pl/', NULL),
(3, 5, 5, 6, 'Zeltech SA', 'Zeltech SA', '727', 'Nowy', 'http://www.zeltech.eu', NULL),
(4, 6, 7, 8, 'Locus Security Duo Sp. z o.o.', 'Locus Security Duo Sp. z o.o.', '852', 'Stały', 'http://www.locus-security.pl', NULL),
(5, 7, 9, 10, 'Galmed Wytwórnia Sprzętu Medycznego', 'Galmed Wytwórnia Sprzętu Medycznego', '5540468264', 'Stały', 'http://www.galmed.com.pl', NULL),
(6, 8, 11, 12, 'Cementownia Kraków Nowa Huta Sp. z o.o.', 'Cementownia Kraków Nowa Huta Sp. z o.o.', '9552249328', 'Zrealizowany', 'http://www.cknh.pl', NULL),
(7, 9, 13, 14, 'Beskid Sp. z o.o.', 'Beskid Sp. z o.o.', '5471876088', 'Zrealizowany', 'http://www.beskid.com.pl', NULL),
(8, 10, 15, 16, 'Nowszy klient', NULL, NULL, 'Nowy', NULL, NULL),
(9, 12, 18, 19, 'Bawaria Motors Sp. z o.o.', 'Bawaria Motors Sp. z o.o.', '5262794848', 'Nowy', 'http://www.bmw-bawariamotors.pl', NULL),
(10, 14, 21, 22, 'Pur Sp. z o.o.', 'Pur Sp. z o.o.', '739-33-53-040', 'Stały', 'http://www.pur-nidzica.pl', NULL),
(11, 17, 25, 26, 'Protektor SA', 'Protektor SA', '712-01-02-959', 'Zrealizowany', 'http://www.protektorsa.pl', NULL),
(12, 18, 27, 28, 'Cetech Sp. z o.o.', 'Cetech Sp. z o.o.', '5830136808', 'Nowy', 'http://www.cetech-gdansk.com.pl', NULL),
(13, 21, 31, 32, 'Nero', 'FHU GAB-ALL Joanna Król-Osińska', '55555555', 'Nowy', 'www.armandio.pl', NULL),
(14, 22, 33, 34, 'Podsumowanie', 'Test', '44654665', 'Nowy', 'www.gab-all.pl', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klienci_dodatkowe_pola`
--

CREATE TABLE IF NOT EXISTS `klienci_dodatkowe_pola` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `klient_id` int(11) DEFAULT NULL,
  `nazwa` text COLLATE utf16_polish_ci,
  `wartosc` text COLLATE utf16_polish_ci,
  PRIMARY KEY (`id`),
  KEY `FK_klienci_dodatkowe_pola_klienci` (`klient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=14 ;

--
-- Zrzut danych tabeli `klienci_dodatkowe_pola`
--

INSERT INTO `klienci_dodatkowe_pola` (`id`, `klient_id`, `nazwa`, `wartosc`) VALUES
(1, 2, NULL, ''),
(2, 3, NULL, ''),
(3, 4, NULL, ''),
(4, 5, NULL, ''),
(5, 6, NULL, ''),
(6, 7, NULL, ''),
(7, 8, NULL, ''),
(8, 9, NULL, ''),
(9, 10, NULL, ''),
(10, 11, NULL, ''),
(11, 12, NULL, ''),
(12, 13, 'GG', '8046451'),
(13, 14, 'nowe pole', 'test');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klienci_notatki`
--

CREATE TABLE IF NOT EXISTS `klienci_notatki` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `klient_id` int(11) DEFAULT NULL,
  `tekst` text COLLATE utf16_polish_ci NOT NULL,
  `data_dodania` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_notatki_klienci` (`klient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=14 ;

--
-- Zrzut danych tabeli `klienci_notatki`
--

INSERT INTO `klienci_notatki` (`id`, `klient_id`, `tekst`, `data_dodania`) VALUES
(1, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sun', '2016-03-23 23:49:25'),
(2, 2, 'firma nr 1', '2016-04-20 19:48:49'),
(3, 3, 'to jest nowa notatka', '2016-04-20 19:53:24'),
(5, 5, 'Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor.', '2016-04-20 21:07:33'),
(6, 6, 'Aliquam erat ac ipsum. Integer aliquam purus. Quisque lorem tortor fringilla sed, vestibulum id.', '2016-04-20 21:18:01'),
(7, 7, 'Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum. Integer aliquam purus. Quisque lorem tortor fringilla sed, vestibulum id, eleifend justo vel bibendum sapien massa ac turpis faucibus orci luctus non, consectetuer lobortis quis, varius in, purus. Integer ultrices posuere cubilia Curae, Nulla ipsum dolor lacus, suscipit adipiscing. Cum sociis natoque penatibus et ultrices volutpat. Nullam wisi ultricies a, gravida vitae, dapibus risus ante sodales lectus blandit eu, tempor diam pede cursus vitae, ultricies eu, faucibus quis, porttitor eros cursus lectus, pellentesque eget, bibendum a, gravida ullamcorper quam. Nullam viverra consectetuer. Quisque cursus et, porttitor risus. Aliquam sem. In hendrerit nulla quam nunc, accumsan congue. Lorem ipsum primis in nibh vel risus. Sed vel lectus. Ut sagittis, ipsum dolor quam.', '2016-04-20 21:23:02'),
(8, 9, 'Etiam sit amet ipsum porta, fermentum dolor sed, bibendum ipsum. Nullam mollis ex vitae dapibus consectetur. Duis ac mattis ligula, nec congue enim. Cras mattis, urna eu finibus hendrerit, tellus ipsum venenatis nibh, quis tempor erat nisi et mauris. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam ultrices nunc ornare massa ultricies tincidunt. Nulla ultricies odio sit amet dolor tristique tempus. Suspendisse convallis neque lacus, eu efficitur mi luctus vitae. Proin arcu nisl, elementum a rhoncus sed, viverra ac eros. Aenean venenatis libero nibh, et dapibus ipsum bibendum a. Mauris mollis consequat est sit amet ultrices. Fusce scelerisque sapien quam, non maximus mi varius id. Mauris id eros non felis pharetra faucibus. Nam ut velit molestie, facilisis massa et, aliquam sem.', '2016-04-24 15:21:49'),
(9, 10, ' Etiam blandit nunc vel est rutrum feugiat. Nam id diam aliquam, mollis arcu vitae, laoreet velit. Integer non finibus eros, eget placerat metus. Duis vehicula eros quis turpis congue tempus. Sed et aliquam metus. Vivamus quis est orci. Vestibulum velit lectus, egestas in finibus in, fringilla nec urna. Nam tempus augue vitae metus rutrum, et maximus mauris finibus. Suspendisse vel lectus placerat, tempor sem eget, eleifend nunc.', '2016-04-24 15:59:23'),
(10, 11, ' Pellentesque sit amet turpis vulputate, sollicitudin diam ac, vehicula ex. Quisque id neque neque. Nam sit amet augue in lacus varius posuere. Sed diam felis, euismod id metus vitae, varius auctor ipsum. Sed tellus leo, pharetra ut ex sodales, iaculis laoreet velit. Cras pretium interdum egestas. Etiam odio dui, elementum eget volutpat nec, pharetra ac ex. Ut non rutrum mi. Donec suscipit odio a augue laoreet, id suscipit leo eleifend. Mauris condimentum risus ac orci gravida imperdiet. Sed tincidunt nisi et lectus tincidunt, sed viverra arcu ullamcorper.', '2016-04-24 17:56:26'),
(11, 12, ' Nam eu arcu facilisis, eleifend nisi sed, interdum lacus. Integer eu consequat risus. Aliquam cursus libero at gravida venenatis. Praesent mollis rhoncus risus at venenatis. Duis condimentum turpis sit amet metus convallis eleifend. Maecenas semper sodales fringilla. Suspendisse consectetur tortor neque, vitae lacinia ligula scelerisque ac. Aenean sit amet nulla vel nulla aliquam laoreet in nec turpis.', '2016-04-24 18:14:01'),
(12, 13, 'KLIENT BIZNESOWY ', '2016-05-10 08:32:34'),
(13, 14, 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test \r\n\r\ntest test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test \r\n\r\ntest test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test v', '2016-07-20 08:54:50');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kontakty`
--

CREATE TABLE IF NOT EXISTS `kontakty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pracownik_id` int(11) DEFAULT NULL,
  `godziny_pracy` text COLLATE utf16_polish_ci,
  `typ` enum('Dział techniczny','Obsługa klienta','Sklep','Portal informacyjny','Sprzedaż') COLLATE utf16_polish_ci DEFAULT 'Dział techniczny',
  PRIMARY KEY (`id`),
  KEY `FK_kontakty_pracownicy` (`pracownik_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=4 ;

--
-- Zrzut danych tabeli `kontakty`
--

INSERT INTO `kontakty` (`id`, `pracownik_id`, `godziny_pracy`, `typ`) VALUES
(1, 7, '8-16', 'Sklep'),
(2, 7, '10:00 - 18:00', 'Dział techniczny'),
(3, 6, '10:00 - 18:00', 'Obsługa klienta');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `magazyn`
--

CREATE TABLE IF NOT EXISTS `magazyn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tytul` varchar(20) COLLATE utf16_polish_ci DEFAULT NULL,
  `opis` varchar(70) COLLATE utf16_polish_ci DEFAULT NULL,
  `data_aktualizacji` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=2 ;

--
-- Zrzut danych tabeli `magazyn`
--

INSERT INTO `magazyn` (`id`, `tytul`, `opis`, `data_aktualizacji`) VALUES
(1, 'test', 'test: 25\r\ntest 2: 35', '2016-07-20 10:01:24');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `magazyn_zasoby`
--

CREATE TABLE IF NOT EXISTS `magazyn_zasoby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `magazyn_id` int(11) NOT NULL,
  `zasob_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_magazyn_zasoby_magazyn` (`magazyn_id`),
  KEY `FK_magazyn_zasoby_zasoby` (`zasob_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `marketing_adwords`
--

CREATE TABLE IF NOT EXISTS `marketing_adwords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `klient_id` int(11) DEFAULT NULL,
  `opiekun_id` int(11) DEFAULT NULL,
  `zlecenie_id` int(11) DEFAULT NULL,
  `tytul_kampanii` text COLLATE utf16_polish_ci,
  `strona_www` text COLLATE utf16_polish_ci,
  `link_do_strony` text COLLATE utf16_polish_ci,
  `budzet_miesieczny` float DEFAULT NULL,
  `slowa_kluczowe` text COLLATE utf16_polish_ci,
  `status` enum('Nowe','Przyjete','W trakcie realizacji','Gotowe do odbioru','Odebrane') COLLATE utf16_polish_ci DEFAULT 'Nowe',
  `priorytet` char(1) COLLATE utf16_polish_ci DEFAULT NULL,
  `uwagi` text COLLATE utf16_polish_ci,
  `zalecenia` text COLLATE utf16_polish_ci,
  PRIMARY KEY (`id`),
  KEY `FK_adwords_klienci` (`klient_id`),
  KEY `FK_adwords_uzytkownicy` (`opiekun_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=7 ;

--
-- Zrzut danych tabeli `marketing_adwords`
--

INSERT INTO `marketing_adwords` (`id`, `klient_id`, `opiekun_id`, `zlecenie_id`, `tytul_kampanii`, `strona_www`, `link_do_strony`, `budzet_miesieczny`, `slowa_kluczowe`, `status`, `priorytet`, `uwagi`, `zalecenia`) VALUES
(1, NULL, NULL, 11, 'Tytuł kampanii', 'www', NULL, NULL, NULL, 'Nowe', NULL, NULL, NULL),
(2, NULL, NULL, 12, 'fffffffffffffffffff', NULL, NULL, NULL, NULL, 'Nowe', NULL, NULL, NULL),
(3, NULL, NULL, 13, 'rrrrrrrr', NULL, NULL, NULL, NULL, 'Nowe', NULL, NULL, NULL),
(4, 1, 2, 14, 'Tytuł kampanii', 'wwwwww', 'link do strony', 555, 'klucz1,kulcz2', 'W trakcie realizacji', 'o', 'Uwagi', 'Zrobić to szybko'),
(5, 1, 2, 15, 'hgfdssdfghj', NULL, NULL, NULL, NULL, 'W trakcie realizacji', NULL, NULL, NULL),
(6, 5, 2, 8, 'Gamma02', 'www.stronafirmy1.eu', 'www.stronafirmy1.eu', 6000, 'medycyna, szpital', 'W trakcie realizacji', NULL, 'Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies.', 'Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum. Integer aliquam purus.');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `marketing_portale`
--

CREATE TABLE IF NOT EXISTS `marketing_portale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa_portalu` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Zrzut danych tabeli `marketing_portale`
--

INSERT INTO `marketing_portale` (`id`, `nazwa_portalu`) VALUES
(1, 'Nowy portal'),
(2, 'FACEBOOK');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `marketing_pozycjonowanie`
--

CREATE TABLE IF NOT EXISTS `marketing_pozycjonowanie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `klient_id` int(11) DEFAULT NULL,
  `opiekun_id` int(11) DEFAULT NULL,
  `zlecenie_id` int(11) DEFAULT NULL,
  `nazwa` text COLLATE utf16_polish_ci,
  `opis_projektu` text COLLATE utf16_polish_ci,
  `strona_www` text COLLATE utf16_polish_ci,
  `link_info` text COLLATE utf16_polish_ci,
  `slowa_kluczowe` text COLLATE utf16_polish_ci,
  `opis_domeny` text COLLATE utf16_polish_ci,
  `link_gplus` text COLLATE utf16_polish_ci,
  `link_fb` text COLLATE utf16_polish_ci,
  `zaplanowano` text COLLATE utf16_polish_ci,
  `status` enum('Nowe','Przyjete','W trakcie realizacji','Gotowe do odbioru','Odebrane') COLLATE utf16_polish_ci DEFAULT NULL,
  `priorytet` char(1) COLLATE utf16_polish_ci DEFAULT '0',
  `uwagi` text COLLATE utf16_polish_ci,
  PRIMARY KEY (`id`),
  KEY `FK_pozycjonowanie_klienci` (`klient_id`),
  KEY `FK_pozycjonowanie_uzytkownicy` (`opiekun_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=5 ;

--
-- Zrzut danych tabeli `marketing_pozycjonowanie`
--

INSERT INTO `marketing_pozycjonowanie` (`id`, `klient_id`, `opiekun_id`, `zlecenie_id`, `nazwa`, `opis_projektu`, `strona_www`, `link_info`, `slowa_kluczowe`, `opis_domeny`, `link_gplus`, `link_fb`, `zaplanowano`, `status`, `priorytet`, `uwagi`) VALUES
(1, NULL, NULL, NULL, 'POzycjonowanie', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Nowe', '0', NULL),
(2, 1, 2, NULL, 'xxxxxx', 'sdfghjk', 'zxcvbm.', '?.,m', 'sdfghjkl;''', 'fghjk', 'rdtfyu', 'rtfyu', 'rtfyui', 'Przyjete', '0', NULL),
(4, 13, 2, 13, 'swissdent', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', 'www.swissdent.pl', 'www.swissdent.pl/info', 'dentysta, kraków, stomatologia', 'www.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.pl', 'www.swissdent.pl', 'www.swissdent.pl', 'www.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.plwww.swissdent.pl', 'Nowe', '0', 'sfwdfsgsfdg');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `marketing_social`
--

CREATE TABLE IF NOT EXISTS `marketing_social` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `klient_id` int(11) DEFAULT NULL,
  `opiekun_id` int(11) DEFAULT NULL,
  `portal_id` int(11) DEFAULT NULL,
  `zlecenie_id` int(11) DEFAULT NULL,
  `nazwa` text COLLATE utf16_polish_ci,
  `link` text COLLATE utf16_polish_ci,
  `facebook_id` text COLLATE utf16_polish_ci,
  `facebook_access_token` text COLLATE utf16_polish_ci,
  `liczba_fanow` int(11) DEFAULT NULL,
  `opis_kampanii` text COLLATE utf16_polish_ci,
  `czas_realizacji` text COLLATE utf16_polish_ci,
  `termin_oddania` date DEFAULT '2016-03-31',
  `status` enum('Kampania w przygotowaniu','Kampania w trakcie','Kampania zakonczona') COLLATE utf16_polish_ci DEFAULT 'Kampania w przygotowaniu',
  `priorytet` char(1) COLLATE utf16_polish_ci DEFAULT NULL,
  `opis_dzialan` text COLLATE utf16_polish_ci,
  PRIMARY KEY (`id`),
  KEY `FK_social_klienci` (`klient_id`),
  KEY `FK_social_uzytkownicy` (`opiekun_id`),
  KEY `FK_social_portale` (`portal_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=8 ;

--
-- Zrzut danych tabeli `marketing_social`
--

INSERT INTO `marketing_social` (`id`, `klient_id`, `opiekun_id`, `portal_id`, `zlecenie_id`, `nazwa`, `link`, `facebook_id`, `facebook_access_token`, `liczba_fanow`, `opis_kampanii`, `czas_realizacji`, `termin_oddania`, `status`, `priorytet`, `opis_dzialan`) VALUES
(1, 1, NULL, NULL, NULL, 'Nowsza nazwac ss', 'ddd', NULL, NULL, 0, 'dd', NULL, '2016-03-31', '', NULL, NULL),
(2, NULL, NULL, NULL, NULL, 'dfghj', NULL, NULL, NULL, NULL, NULL, NULL, '2016-03-31', '', NULL, NULL),
(3, NULL, NULL, 1, 10, 'Nazwwaa', 'lkasdf', NULL, NULL, 3, NULL, NULL, '2016-03-31', '', NULL, NULL),
(4, 6, 1, 1, 7, 'Beta01', 'www.linklink.com.pl', NULL, NULL, 600, 'Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum. Integer aliquam purus. Quisque lorem tortor fringilla sed,', '2017-03-05', '2017-03-06', 'Kampania w trakcie', NULL, 'Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies.'),
(5, 4, 2, 1, 9, 'Delta02', 'www.adresstrony.com.pl', NULL, NULL, 1000, 'Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum. Integer aliquam purus. Quisque lorem tortor fringilla sed,', '2016-04-27', '2016-04-28', 'Kampania zakonczona', 'o', 'Curabitur et ligula.'),
(6, 13, 21, 2, 11, 'Swissdent', 'https://www.facebook.com/swissdentkrk', NULL, NULL, 1553, 'Kampania w tym okresie przeszła bez żadnych niespodzianek. Kontynuowaliśmy promowanie usług dla dzieci po przez posty jak i reklamę lokalną. Wyniki wyszły dobre, natomiast mogłyby być jeszcze lepsze, ale o tym później...  ', NULL, '2016-03-31', 'Kampania zakonczona', 'o', NULL),
(7, 13, 20, 2, 14, 'Swissdent', 'https://www.facebook.com/swissdentkrk/', NULL, NULL, 1865, 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test v\r\n\r\n\r\ntest test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test \r\n\r\ntest test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', NULL, '2016-03-31', 'Kampania zakonczona', 'o', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `marketing_zasoby`
--

CREATE TABLE IF NOT EXISTS `marketing_zasoby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pozycjonowanie_id` int(11) DEFAULT NULL,
  `adwords_id` int(11) DEFAULT NULL,
  `social_id` int(11) DEFAULT NULL,
  `zasob_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_marketing_pozycjonowanie` (`pozycjonowanie_id`),
  KEY `FK_marketing_adwords` (`adwords_id`),
  KEY `FK_marketing_social` (`social_id`),
  KEY `FK_zasoby` (`zasob_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=2 ;

--
-- Zrzut danych tabeli `marketing_zasoby`
--

INSERT INTO `marketing_zasoby` (`id`, `pozycjonowanie_id`, `adwords_id`, `social_id`, `zasob_id`) VALUES
(1, NULL, NULL, 6, 33);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `namioty`
--

CREATE TABLE IF NOT EXISTS `namioty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` text COLLATE utf16_polish_ci,
  `ilosc` int(11) DEFAULT NULL,
  `cena` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `notatki`
--

CREATE TABLE IF NOT EXISTS `notatki` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pracownik_id` int(11) DEFAULT NULL,
  `domena_id` int(11) DEFAULT NULL,
  `serwer_id` int(11) DEFAULT NULL,
  `magazyn_id` int(11) DEFAULT NULL,
  `socialmedia_id` int(11) DEFAULT NULL,
  `faktura_id` int(11) DEFAULT NULL,
  `tresc` text COLLATE utf16_polish_ci,
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_notatki_uzytkownicy` (`pracownik_id`),
  KEY `FK_notatki_domeny` (`domena_id`),
  KEY `FK_notatki_serwery` (`serwer_id`),
  KEY `FK_notatki_marketing_social` (`socialmedia_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=13 ;

--
-- Zrzut danych tabeli `notatki`
--

INSERT INTO `notatki` (`id`, `pracownik_id`, `domena_id`, `serwer_id`, `magazyn_id`, `socialmedia_id`, `faktura_id`, `tresc`, `data_dodania`) VALUES
(1, 2, NULL, NULL, NULL, NULL, NULL, 'Loler ipsum notatkum', '2016-04-18 07:51:38'),
(2, 1, NULL, NULL, NULL, NULL, NULL, 'Lolrm impus administratorus', '2016-04-18 07:52:05'),
(3, 1, NULL, NULL, NULL, NULL, NULL, 'Treść notatki notatki notatki', '2016-04-18 08:40:14'),
(4, 1, NULL, NULL, NULL, NULL, NULL, 'csdfghbmj,nk,', '2016-04-22 22:36:14'),
(5, 1, NULL, NULL, NULL, NULL, NULL, 'test', '2016-05-10 08:29:52'),
(6, 1, 2, NULL, NULL, NULL, NULL, 'WWW do grania i :D', '2016-05-12 07:43:12'),
(7, 1, NULL, NULL, NULL, 6, NULL, 'W tym miesiącu w porównaniu z poprzednim największym wzrostem jest zasięg reklamy, który liczy aż 69 465 osób, które miały styczność z marką SWISSDENT. Pozostałe kategorie jak działania na stronie, filmy, aktywność czy polubienia strony to spadek. Niestety zgłaszałem Państwu, że samymi reklamami nie osiągniemy 120% normy. Tutaj trzeba czegoś więcej niż tylko moja kampania. Na koniec raportu rozpiszę o co chodzi. ', '2016-05-25 08:28:50'),
(8, 1, NULL, NULL, NULL, 7, NULL, 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2016-07-20 09:38:16'),
(9, 1, NULL, NULL, NULL, 7, NULL, 'test', '2016-07-20 09:38:57'),
(10, 1, NULL, 1, NULL, NULL, NULL, 'test', '2016-07-20 09:51:36'),
(11, 1, 3, NULL, NULL, NULL, NULL, 'test test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test testtest test test', '2016-07-20 09:57:38'),
(12, 1, NULL, NULL, NULL, NULL, 1, 'test test ', '2016-07-20 10:06:49');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `planowanie_imprez`
--

CREATE TABLE IF NOT EXISTS `planowanie_imprez` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `klient_id` int(11) DEFAULT NULL,
  `pracownik_id` int(11) DEFAULT NULL,
  `nazwa` varchar(50) COLLATE utf16_polish_ci DEFAULT NULL,
  `opis` text COLLATE utf16_polish_ci,
  `uwagi` text COLLATE utf16_polish_ci,
  `cena` decimal(10,2) DEFAULT NULL,
  `zadatek` decimal(10,2) DEFAULT NULL,
  `kategoria` enum('DJ/Wodzirej','Hale namiotowe','Usługi dodatkowe') COLLATE utf16_polish_ci DEFAULT NULL,
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_realizacji` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_planowanie_imprez_klienci` (`klient_id`),
  KEY `FK_planowanie_imprez_uzytkownicy` (`pracownik_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=3 ;

--
-- Zrzut danych tabeli `planowanie_imprez`
--

INSERT INTO `planowanie_imprez` (`id`, `klient_id`, `pracownik_id`, `nazwa`, `opis`, `uwagi`, `cena`, `zadatek`, `kategoria`, `data_dodania`, `data_realizacji`) VALUES
(1, 1, 2, 'Impreza1', 'Opsi imprezy1', 'Uwagi', '1235.33', '5000.00', 'Hale namiotowe', '2016-04-17 22:28:16', '2016-06-18'),
(2, 13, 21, 'Namiot 5x8', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test v', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '2400.00', '600.00', 'Hale namiotowe', '2016-07-20 09:41:33', '2016-08-06');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `planowanie_imprez_zasoby`
--

CREATE TABLE IF NOT EXISTS `planowanie_imprez_zasoby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `planowanie_imprez_id` int(11) DEFAULT NULL,
  `zasob_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_planowanie_imprez_zasoby_planowanie_imprez` (`planowanie_imprez_id`),
  KEY `FK_planowanie_imprez_zasoby_zasoby` (`zasob_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=2 ;

--
-- Zrzut danych tabeli `planowanie_imprez_zasoby`
--

INSERT INTO `planowanie_imprez_zasoby` (`id`, `planowanie_imprez_id`, `zasob_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `platnosci`
--

CREATE TABLE IF NOT EXISTS `platnosci` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `klient_id` int(11) DEFAULT NULL,
  `kwota` decimal(10,2) DEFAULT NULL,
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pracownicy`
--

CREATE TABLE IF NOT EXISTS `pracownicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `adres_id` int(11) DEFAULT NULL,
  `gg` varchar(20) COLLATE utf16_polish_ci DEFAULT NULL,
  `stanowisko` varchar(50) COLLATE utf16_polish_ci DEFAULT NULL,
  `data_zatrudnienia` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_pracownicy_uzytkownicy` (`uzytkownik_id`),
  KEY `FK_pracownicy_adresy` (`adres_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=8 ;

--
-- Zrzut danych tabeli `pracownicy`
--

INSERT INTO `pracownicy` (`id`, `uzytkownik_id`, `adres_id`, `gg`, `stanowisko`, `data_zatrudnienia`) VALUES
(1, 2, NULL, '4885858', NULL, NULL),
(2, 11, 17, '23932868', 'Pracownik biurowy', '2015-03-22'),
(3, 13, 20, '71622440', 'Szef', '1995-10-12'),
(4, 15, 23, '81564300', 'Dyrektor Handlowy', '2001-08-05'),
(5, 16, 24, '67010942', 'Key Account Manager', '2011-06-01'),
(6, 19, 29, '52971021', 'Pracownik techniczny', '2011-03-10'),
(7, 20, 30, '8046451', 'Kierownik', '1991-05-10');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(15) COLLATE utf16_polish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=5 ;

--
-- Zrzut danych tabeli `role`
--

INSERT INTO `role` (`id`, `nazwa`) VALUES
(1, 'admin'),
(2, 'pracownik'),
(3, 'klient'),
(4, 'test');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `serwery`
--

CREATE TABLE IF NOT EXISTS `serwery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` text COLLATE utf16_polish_ci,
  `typ` enum('Hosting','VPS','Dedykowany') COLLATE utf16_polish_ci DEFAULT NULL,
  `ip` text COLLATE utf16_polish_ci,
  `dns` text COLLATE utf16_polish_ci,
  `dns2` text COLLATE utf16_polish_ci,
  `panel_klienta` text COLLATE utf16_polish_ci,
  `panel_admina` text COLLATE utf16_polish_ci,
  `php_my_admin` text COLLATE utf16_polish_ci,
  `login` text COLLATE utf16_polish_ci,
  `haslo` text COLLATE utf16_polish_ci,
  `data_rejestracji` timestamp NULL DEFAULT NULL,
  `data_wygasniecia` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=2 ;

--
-- Zrzut danych tabeli `serwery`
--

INSERT INTO `serwery` (`id`, `nazwa`, `typ`, `ip`, `dns`, `dns2`, `panel_klienta`, `panel_admina`, `php_my_admin`, `login`, `haslo`, `data_rejestracji`, `data_wygasniecia`) VALUES
(1, 'netdc', 'Hosting', '156.12.15.14.1', 'ns1.netdc.pl', 'ns2.netdc.pl', 'www.netdc.pl/panel', 'www.netdc.pl/adm', 'www.netdc.pl/php', 'admin', 'haslo', '2016-07-19 22:00:00', '2017-07-19 22:00:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `subdomeny`
--

CREATE TABLE IF NOT EXISTS `subdomeny` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domena_id` int(11) DEFAULT NULL,
  `adres` text COLLATE utf16_polish_ci,
  `ip` text COLLATE utf16_polish_ci,
  `rodzaj` enum('Katalog','Wordpress','Zaślepka','Inne') COLLATE utf16_polish_ci DEFAULT 'Katalog',
  PRIMARY KEY (`id`),
  KEY `FK_subdomeny_domeny` (`domena_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=2 ;

--
-- Zrzut danych tabeli `subdomeny`
--

INSERT INTO `subdomeny` (`id`, `domena_id`, `adres`, `ip`, `rodzaj`) VALUES
(1, 3, 'test.swissdent.pl', '156.25.25.241', 'Zaślepka');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uprawnienia`
--

CREATE TABLE IF NOT EXISTS `uprawnienia` (
  `kod` varchar(7) COLLATE utf16_polish_ci NOT NULL,
  `opis` varchar(50) COLLATE utf16_polish_ci DEFAULT NULL,
  PRIMARY KEY (`kod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;

--
-- Zrzut danych tabeli `uprawnienia`
--

INSERT INTO `uprawnienia` (`kod`, `opis`) VALUES
('men_cen', 'Cennik wykonanych zleceń'),
('men_coc', 'Zlenenia czekające na opłacenie'),
('men_cop', 'Zlecenia opłacone'),
('men_hos', 'Hosting'),
('men_kad', 'Kadra'),
('men_kli', 'Klienci'),
('men_kon', 'Kontakty'),
('men_ksi', 'Księgowość'),
('men_mag', 'Magazyn'),
('men_mar', 'Marketing'),
('men_pla', 'Planowanie imprez'),
('men_pli', 'Pliki'),
('men_pul', 'Pulpit'),
('men_rol', 'Role'),
('men_wia', 'Konwersacje'),
('men_zgl', 'Zgłoszenia'),
('men_zle', 'Zlecenia'),
('pas_cha', 'Chat'),
('pas_dod', 'Rozwijalne opcja Dodaj'),
('pas_not', 'Notatki'),
('pas_pow', 'Powiadomienia'),
('pas_pro', 'Profil'),
('pas_ust', 'Ustawienia profilu'),
('pul_cza', 'Czat'),
('pul_kal', 'Kalendarz'),
('pul_kli', 'Ilość klientów'),
('pul_nzl', 'Najnowsze zlecenia'),
('pul_opi', 'Opiekun'),
('pul_pla', 'Płatności (faktury opłacone i nieopłacone)'),
('pul_pow', 'Powiadomienia'),
('pul_rek', 'Box reklamowy z promocjami'),
('pul_typ', 'Typ usługi'),
('pul_wia', 'Wiadomości'),
('pul_zgl', 'Zgłoszenia'),
('sta_zal', 'Aktualnie zalogowani'),
('sta_zle', 'Ilość wykonanych zleceń');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uprawnienia_kategorie`
--

CREATE TABLE IF NOT EXISTS `uprawnienia_kategorie` (
  `kod` varchar(3) COLLATE utf8_polish_ci NOT NULL,
  `opis` varchar(20) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`kod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `uprawnienia_kategorie`
--

INSERT INTO `uprawnienia_kategorie` (`kod`, `opis`) VALUES
('men', 'Pozycje menu'),
('pas', 'Pasek górny'),
('pul', 'Pulpit'),
('sta', 'Statystyki');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uprawnienia_role`
--

CREATE TABLE IF NOT EXISTS `uprawnienia_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rola_id` int(11) NOT NULL,
  `uprawnienie_kod` varchar(7) COLLATE utf16_polish_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_uprawnienia_role_role` (`rola_id`),
  KEY `FK_uprawnienia_role_uprawnienia` (`uprawnienie_kod`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=61 ;

--
-- Zrzut danych tabeli `uprawnienia_role`
--

INSERT INTO `uprawnienia_role` (`id`, `rola_id`, `uprawnienie_kod`) VALUES
(1, 1, 'men_hos'),
(2, 1, 'men_kad'),
(3, 1, 'men_kli'),
(4, 1, 'men_kon'),
(5, 1, 'men_ksi'),
(6, 1, 'men_mag'),
(7, 1, 'men_mar'),
(8, 1, 'men_pla'),
(9, 1, 'men_pli'),
(10, 1, 'men_pul'),
(11, 1, 'men_wia'),
(12, 1, 'men_zgl'),
(13, 1, 'men_zle'),
(14, 2, 'men_hos'),
(15, 2, 'men_kad'),
(16, 2, 'men_kli'),
(17, 2, 'men_kon'),
(18, 2, 'men_mag'),
(19, 2, 'men_pla'),
(20, 2, 'men_pli'),
(21, 2, 'men_pul'),
(22, 2, 'men_wia'),
(23, 2, 'men_zgl'),
(24, 2, 'men_zle'),
(25, 3, 'men_kon'),
(26, 3, 'men_pli'),
(27, 3, 'men_pul'),
(28, 3, 'men_wia'),
(29, 1, 'men_rol'),
(30, 1, 'men_cen'),
(31, 2, 'men_cen'),
(32, 1, 'men_cop'),
(33, 1, 'men_coc'),
(34, 1, 'pul_kli'),
(35, 1, 'pul_wia'),
(36, 1, 'pul_nzl'),
(37, 1, 'pul_zgl'),
(38, 1, 'pul_kal'),
(39, 1, 'pas_pro'),
(40, 1, 'pas_dod'),
(41, 1, 'pas_not'),
(42, 1, 'pas_cha'),
(43, 1, 'pas_pow'),
(44, 1, 'pas_ust'),
(45, 2, 'pul_kli'),
(46, 2, 'pul_wia'),
(47, 2, 'pul_nzl'),
(48, 2, 'pul_zgl'),
(49, 2, 'pul_kal'),
(50, 2, 'pas_pro'),
(51, 2, 'pas_dod'),
(52, 2, 'pas_not'),
(53, 2, 'pas_cha'),
(54, 2, 'pas_pow'),
(55, 2, 'pas_ust'),
(56, 3, 'pul_wia'),
(57, 3, 'pas_cha'),
(58, 3, 'pas_pow'),
(59, 3, 'pas_ust'),
(60, 3, 'pas_pro');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uzytkownicy`
--

CREATE TABLE IF NOT EXISTS `uzytkownicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `avatar_id` int(11) DEFAULT NULL,
  `rola_id` int(11) DEFAULT NULL,
  `login` text COLLATE utf8_polish_ci,
  `imie` text COLLATE utf8_polish_ci,
  `nazwisko` text COLLATE utf8_polish_ci,
  `haslo` text COLLATE utf8_polish_ci,
  `mail` text COLLATE utf8_polish_ci,
  `nr_konta` text COLLATE utf8_polish_ci,
  `telefon` char(12) COLLATE utf8_polish_ci DEFAULT NULL,
  `typ` enum('admin','pracownik','klient') COLLATE utf8_polish_ci DEFAULT 'klient',
  `ostatnie_logowanie` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_uzytkownicy_zasoby` (`avatar_id`),
  KEY `FK_uzytkownicy_role` (`rola_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=23 ;

--
-- Zrzut danych tabeli `uzytkownicy`
--

INSERT INTO `uzytkownicy` (`id`, `avatar_id`, `rola_id`, `login`, `imie`, `nazwisko`, `haslo`, `mail`, `nr_konta`, `telefon`, `typ`, `ostatnie_logowanie`) VALUES
(1, NULL, 1, 'admin', 'kk', 'Admiński', '21232f297a57a5a743894a0e4a801fc3', 'admin', NULL, NULL, 'klient', '2016-07-29 09:35:29'),
(2, NULL, 2, 'pracownik1', 'Pracownik', 'Pracowity', '21232f297a57a5a743894a0e4a801fc3', 'pracownik@pracowity.pl', NULL, NULL, 'pracownik', NULL),
(3, 1, 3, 'pastmo', 'Monika', 'Rymsza', '21232f297a57a5a743894a0e4a801fc3', 'monika.rymsza@pastmo.pl', NULL, '+48111222333', 'klient', NULL),
(4, 2, 3, NULL, 'Marcin', 'Sowiński', NULL, 'office@mediafocus.pl', NULL, '58 340-12-50', 'klient', NULL),
(5, 3, 3, NULL, 'Andrzej', 'Dryjski', NULL, 'zue@zeltech.pl', NULL, '42 254-09-30', 'klient', NULL),
(6, 4, 3, NULL, 'Andrzej', 'Żarnowski', NULL, 'biuroszczecin@locus-security.pl', NULL, '91 483-49-16', 'klient', NULL),
(7, 5, 3, NULL, 'Marian', 'Meger', NULL, 'biuro@galmed.com.pl', NULL, '52 342-03-99', 'klient', NULL),
(8, 6, 3, NULL, 'Jerzy', 'Werbiński', NULL, 'cementownia@kem.com.pl', NULL, '12 681 05 42', 'klient', NULL),
(9, 7, 3, NULL, 'Jan', 'Szybalski', NULL, 'beskid@beskid.com.pl', NULL, '33 822 27 03', 'klient', NULL),
(10, NULL, 3, NULL, 'Adrian', NULL, '4f992a1054b0775672e5e69ad8321c60', 'pawel.k.kazmierczak@gmail.com', NULL, NULL, 'klient', NULL),
(11, 16, 2, NULL, 'Piotr', 'Piotrowski', '0be57ab53c4d988308488ae67937da76', 'biurowy11554@interia.pl', '53 4020 0788 0000 0000 2974 3516', '518 301 799', 'pracownik', NULL),
(12, 17, 3, NULL, 'Stefan', 'Van Herpen', '593ae546e94640ec9e43ecf6af2e310f', 'infokatowice@bmw-bawariamotors.pl', NULL, '22 550-03-00', 'klient', NULL),
(13, 18, 2, NULL, 'Krzysztof', 'Jarzyna', '3fc0a7acf087f549ac2b266baf94b8b1', 'krzysztof.jar@o2.pl', '63 2170 0117 0000 0000 0033 6182', '786-204-902', 'klient', NULL),
(14, 19, 3, NULL, 'Jarosław', 'Cierniewski', 'c21752a284d71ae7d28d082a81c852a0', 'dyrektor@pur-nidzica.pl', NULL, '89 625 67 10', 'klient', NULL),
(15, 20, 2, NULL, 'Janusz', 'Sokół', '0c7ef2652bd9087e14012e9a456d172c', 'janusz270@onet.pl', '49 6055 1100 0000 0000 8222 1114', '664-270-801', 'klient', NULL),
(16, 21, 2, NULL, 'Katarzyna', 'Mikołajczyk-Nowakowska', '2ecbe7830f8fa46df4f4198ea92be21a', 'katarzyna.mik.now@wp.pl', '26 8006 1459 0000 0000 3331 7680', '503 200 949', 'pracownik', NULL),
(17, 22, 3, NULL, 'Piotr', 'Skrzyński', '3f9f0a533918f98498585c42dec0d779', 'info@protektorsa.pl', NULL, '81 532 22 31', 'klient', NULL),
(18, 23, 3, NULL, 'Lech', 'Zapadka', '7186f174cc89f86ff5ea50f116f98039', 'cetech@cetech-gdansk.com.pl', NULL, '58 305-24-77', 'klient', NULL),
(19, 24, 2, NULL, 'Józef', 'Gołąb', '334af3432dd5e5e0ba8707778462d014', 'janusz270@onet.pl', '28 3030 9991 0000 0000 0358 7162', '514 333 195', 'pracownik', NULL),
(20, 25, 2, NULL, 'Konrad', 'Mrowiec', 'e78ebb135b0440421bba57f3cc066842', 'mrowiec.konrad@gmail.com', '48 1111 1111 2222 3333 4444 5555', '537 595 322', 'pracownik', '2016-05-10 08:15:21'),
(21, 29, 3, NULL, 'Konrad', 'Mrowiec', 'd1bb6cc4b808eafec2d5d50542fd19bd', 'mrowiec.konrad@gmail.com', NULL, '727512418', 'klient', NULL),
(22, NULL, 3, NULL, 'Przemek', 'Lolowski', '73ac4bff02d95ad62662dc2ceaaed67b', 'test@test.pl', NULL, '666444555', 'klient', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uzytkownicy_watki_wiadomosci`
--

CREATE TABLE IF NOT EXISTS `uzytkownicy_watki_wiadomosci` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `watek_wiadomosci_id` int(11) DEFAULT NULL,
  `przeczytane` char(1) COLLATE utf16_polish_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_uzytkownicy_wiadomosci_uzytkownicy` (`uzytkownik_id`),
  KEY `FK_uzytkownicy_wiadomosci_wiadomosci` (`watek_wiadomosci_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=8 ;

--
-- Zrzut danych tabeli `uzytkownicy_watki_wiadomosci`
--

INSERT INTO `uzytkownicy_watki_wiadomosci` (`id`, `uzytkownik_id`, `watek_wiadomosci_id`, `przeczytane`) VALUES
(1, 1, 1, '1'),
(2, 1, 2, '1'),
(3, 2, 2, '0'),
(4, 20, 3, '1'),
(5, 1, 3, '1'),
(6, 21, 4, '0'),
(7, 1, 4, '1');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uzytkownicy_zasoby`
--

CREATE TABLE IF NOT EXISTS `uzytkownicy_zasoby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uzytkownik_id` int(11) NOT NULL,
  `zasob_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_uzytkownicy_zasoby_uzytkownicy` (`uzytkownik_id`),
  KEY `FK_uzytkownicy_zasoby_zasoby` (`zasob_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=4 ;

--
-- Zrzut danych tabeli `uzytkownicy_zasoby`
--

INSERT INTO `uzytkownicy_zasoby` (`id`, `uzytkownik_id`, `zasob_id`) VALUES
(1, 20, 26),
(2, 20, 27),
(3, 20, 28);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `watki_wiadomosci`
--

CREATE TABLE IF NOT EXISTS `watki_wiadomosci` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `temat` text COLLATE utf16_polish_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=5 ;

--
-- Zrzut danych tabeli `watki_wiadomosci`
--

INSERT INTO `watki_wiadomosci` (`id`, `temat`) VALUES
(1, 'dfghjk'),
(2, 'NOwa wiadomość testowa'),
(3, 'Test'),
(4, 'Test');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wiadomosci`
--

CREATE TABLE IF NOT EXISTS `wiadomosci` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autor_id` int(11) DEFAULT NULL,
  `watek_id` int(11) DEFAULT NULL,
  `tresc` text COLLATE utf16_polish_ci NOT NULL,
  `data_dodania` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_wiadomosci_watki_wiadomosci` (`watek_id`),
  KEY `FK_wiadomosci_uzytkownicy` (`autor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=7 ;

--
-- Zrzut danych tabeli `wiadomosci`
--

INSERT INTO `wiadomosci` (`id`, `autor_id`, `watek_id`, `tresc`, `data_dodania`) VALUES
(1, NULL, 1, 'fghjk', '2016-04-17 07:02:30'),
(2, 2, 2, 'aassssssssssssssss ssssssssssssssss sssssssssssss\r\n		\r\n    ', '2016-04-17 08:07:41'),
(3, 2, 2, 'sssssssssssssss\n		\n    ', '2016-04-17 08:08:12'),
(4, 1, 3, 'Cześć,&nbsp;<div>właśnie testuje nasze konwersacje :)</div>', '2016-05-10 08:15:01'),
(5, 20, 3, 'Dzięki :)', '2016-05-10 08:17:33'),
(6, 1, 4, 'Cześć, wysyłam do ciebie tego maila żeby sprawdzić jak to działa.<div><br></div><div>test test test test<span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span></div><div>test test test test<span style="line-height: 1.42857;"><br></span></div><div><br></div><div><br></div><div>test test test test<span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test testv</span><br></div><div><span style="line-height: 1.42857;"><br></span></div><div><span style="line-height: 1.42857;"><br></span></div><div>test test test test<span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test <b>test test test</b></span><span style="line-height: 1.42857;"><b>test</b> test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;">test test test test</span><span style="line-height: 1.42857;"><br></span></div>', '2016-07-20 09:00:50');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zasoby`
--

CREATE TABLE IF NOT EXISTS `zasoby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` text COLLATE utf8_polish_ci NOT NULL,
  `typ` enum('plik','obrazek') COLLATE utf8_polish_ci NOT NULL DEFAULT 'plik',
  `kategoria` enum('zwykły plik','rachunki','umowy','dokumenty','inne') COLLATE utf8_polish_ci NOT NULL DEFAULT 'zwykły plik',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=34 ;

--
-- Zrzut danych tabeli `zasoby`
--

INSERT INTO `zasoby` (`id`, `url`, `typ`, `kategoria`) VALUES
(1, '1.jpg', 'obrazek', ''),
(2, '2_ava1.jpg', 'obrazek', ''),
(3, '3_ava2.jpg', 'obrazek', ''),
(4, '4_ava3.jpg', 'obrazek', ''),
(5, '5_ava4.jpg', 'obrazek', ''),
(6, '6_ava5.jpg', 'obrazek', ''),
(7, '7_ava6.jpeg', 'obrazek', ''),
(8, '8_zalacznik.rar', 'plik', ''),
(9, '9_zalacznik.rar', 'plik', ''),
(10, '10_1.txt', 'plik', ''),
(11, '11_1.txt', 'plik', ''),
(12, '12_spakowane.rar', 'plik', ''),
(13, '13_brak.txt', 'plik', ''),
(14, '14_Cover.jpg', 'obrazek', ''),
(15, '15_131012A001.jpg', 'obrazek', ''),
(16, '16_dok1.jpg', 'obrazek', ''),
(17, '17_f11.jpg', 'obrazek', ''),
(18, '18_chop.jpg', 'obrazek', ''),
(19, '19_apl11.jpg', 'obrazek', ''),
(20, '20_asim.jpg', 'obrazek', ''),
(21, '21_apple21.jpg', 'obrazek', ''),
(22, '22_btd.jpg', 'obrazek', ''),
(23, '23_wbd1.jpg', 'obrazek', ''),
(24, '24_bag1.jpg', 'obrazek', ''),
(25, '25_nero.png', 'obrazek', ''),
(26, '26_fb1.doc', 'plik', 'rachunki'),
(27, '27_logo_web.bmp', 'obrazek', 'umowy'),
(28, '28_Raport kampanii reklamowej dla SWISSDENT - MARZEC 2016.docx', 'plik', 'dokumenty'),
(29, '29_942194_590982970925822_516874751_n.jpg', 'obrazek', 'zwykły plik'),
(30, '30_PANEL ADMINISTRACYJNY GABALL (1).docx', 'plik', 'zwykły plik'),
(31, '31_PANEL ADMINISTRACYJNY GABALL.docx', 'plik', 'zwykły plik'),
(32, '32_SZABLON WORDPRESS DLA GAB ALL.docx', 'plik', 'zwykły plik'),
(33, '33_Raport kampanii reklamowej dla SWISSDENT - LUTY 2016.pdf', 'plik', 'zwykły plik');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zgloszenia`
--

CREATE TABLE IF NOT EXISTS `zgloszenia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pracownik_id` int(11) DEFAULT NULL,
  `temat` varchar(50) COLLATE utf16_polish_ci NOT NULL,
  `klient` varchar(50) COLLATE utf16_polish_ci NOT NULL,
  `opiekun` varchar(50) COLLATE utf16_polish_ci NOT NULL,
  `data_dodania` date NOT NULL,
  `status` varchar(10) COLLATE utf16_polish_ci NOT NULL,
  `przeczytane` char(1) COLLATE utf16_polish_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_zgloszenia_uzytkownicy` (`pracownik_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=3 ;

--
-- Zrzut danych tabeli `zgloszenia`
--

INSERT INTO `zgloszenia` (`id`, `pracownik_id`, `temat`, `klient`, `opiekun`, `data_dodania`, `status`, `przeczytane`) VALUES
(2, 1, 'Awaria', 'Krzysztof Krzysztowski', 'Arkadiusz Arkadiuszowski', '2016-03-03', 'Oczekujace', '1');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zlecenia`
--

CREATE TABLE IF NOT EXISTS `zlecenia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `klient_id` int(11) DEFAULT NULL,
  `opiekun_id` int(11) DEFAULT NULL,
  `cennik_zlecenia_id` int(11) DEFAULT NULL,
  `kategoria_id` int(11) DEFAULT NULL,
  `nazwa` text COLLATE utf16_polish_ci,
  `opis_krotki` text COLLATE utf16_polish_ci,
  `opis_dlugi` text COLLATE utf16_polish_ci,
  `kontakt_do_klienta` text COLLATE utf16_polish_ci,
  `czas_realizacji` float DEFAULT NULL,
  `termi_oddania` date DEFAULT NULL,
  `status` enum('Nowe','Przyjete','W trakcie realizacji','Gotowe do odbioru','Odebrane') COLLATE utf16_polish_ci NOT NULL DEFAULT 'Nowe',
  `typ` enum('zlecenie','adwords','pozycjonowanie','social') COLLATE utf16_polish_ci NOT NULL DEFAULT 'zlecenie',
  `priorytet` char(1) COLLATE utf16_polish_ci DEFAULT '0',
  `dane_do_hostingu` text COLLATE utf16_polish_ci,
  `dane_do_domeny` text COLLATE utf16_polish_ci,
  `uwagi` text COLLATE utf16_polish_ci,
  `przeczytane` char(1) COLLATE utf16_polish_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_zlecenia_uzytkownicy_2` (`opiekun_id`),
  KEY `FK_zlecenia_uzytkownicy` (`klient_id`),
  KEY `FK_zlecenia_cennik_zlecen` (`cennik_zlecenia_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=16 ;

--
-- Zrzut danych tabeli `zlecenia`
--

INSERT INTO `zlecenia` (`id`, `klient_id`, `opiekun_id`, `cennik_zlecenia_id`, `kategoria_id`, `nazwa`, `opis_krotki`, `opis_dlugi`, `kontakt_do_klienta`, `czas_realizacji`, `termi_oddania`, `status`, `typ`, `priorytet`, `dane_do_hostingu`, `dane_do_domeny`, `uwagi`, `przeczytane`) VALUES
(1, 1, 1, 3, NULL, 'Zlecenie1', '', NULL, NULL, NULL, '2016-04-16', '', 'zlecenie', '0', NULL, NULL, NULL, '1'),
(2, 1, 1, 1, NULL, 'Zlecenie2', NULL, NULL, NULL, NULL, '2016-06-16', '', 'zlecenie', '0', NULL, NULL, NULL, '1'),
(3, 2, 2, NULL, 4, 'Zakup', 'Lorem ipsum dolor sit amet enim. Etiam ullamcorper.', 'Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum.', NULL, 3, '2016-04-26', 'Nowe', 'zlecenie', '0', NULL, NULL, 'Aliquam erat ac ipsum.', '1'),
(4, 3, 2, NULL, 4, 'Najnowsze', 'Quisque lorem tortor fringilla sed, vestibulum', 'Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum. Integer aliquam purus. Quisque lorem tortor fringilla sed, vestibulum id, eleifend justo vel bibendum sapien massa ac turpis faucibus orci luctus non, consectetuer lobortis quis, varius in, purus.', NULL, 7, '2016-04-24', 'Odebrane', 'zlecenie', 'o', NULL, NULL, 'pilne zadanie', '1'),
(5, 7, 2, NULL, 3, 'Akcja reklamowa', 'Phasellus fermentum in, dolor.', 'Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum. Integer aliquam purus. Quisque lorem tortor fringilla sed, vestibulum id, eleifend justo vel bibendum sapien massa ac turpis faucibus orci luctus non, consectetuer lobortis quis, varius in, purus.', NULL, 12, '2016-04-20', 'Odebrane', 'zlecenie', '0', NULL, NULL, 'zadanie zrealizowane w terminie', '1'),
(7, 6, 1, 2, NULL, 'Beta01', NULL, NULL, NULL, 2017, NULL, '', 'social', '0', NULL, NULL, NULL, '1'),
(8, 5, 2, NULL, NULL, 'Gamma02', NULL, NULL, NULL, NULL, NULL, 'W trakcie realizacji', 'adwords', '0', NULL, NULL, 'Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies.', '1'),
(10, 13, 20, NULL, 1, 'Sklep internetowy', 'Wykonanie sklepu z wiertłami', 'Fabryka produkująca wiertła i działająca na rynku od ponad 100 lat potrzebuje dobrego responsywnego sklepu internetowego aby rozszerzyć swoją działalność na cały świat. ', NULL, 21, '2016-05-31', 'Nowe', 'zlecenie', 'o', 'test test test \r\ntest test test4\r\ntest test test', 'test test test \r\ntest test test4\r\ntest test test', 'test test test \r\ntest test test4\r\ntest test testtest test test \r\ntest test test4\r\ntest test testtest test test \r\ntest test test4\r\ntest test testtest test test \r\ntest test test4\r\ntest test test', '0'),
(11, 13, 21, NULL, NULL, 'Swissdent', NULL, NULL, NULL, NULL, NULL, '', 'social', 'o', NULL, NULL, NULL, '0'),
(12, 13, 20, NULL, NULL, 'Podsumowanie ', 'sprawdzamy ostatecznie dodawanie klienta', 'sprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klientasprawdzamy ostatecznie dodawanie klienta', NULL, 360, '2017-05-10', 'Nowe', 'zlecenie', 'o', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test v', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test v', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', '0'),
(13, 13, 2, NULL, NULL, 'swissdent', NULL, NULL, NULL, NULL, NULL, 'Nowe', 'pozycjonowanie', '0', NULL, NULL, 'sfwdfsgsfdg', '0'),
(14, 13, 20, NULL, NULL, 'Swissdent', NULL, NULL, NULL, NULL, NULL, '', 'social', 'o', NULL, NULL, NULL, '0'),
(15, 13, 20, NULL, NULL, 'Testowe zlecenie', 'test test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test test', 'test test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test test\r\n\r\ntest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test test\r\n\r\ntest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test testtest test test test', NULL, 365, '2016-08-06', 'Nowe', 'zlecenie', 'o', 'test test test testtest test test test\r\ntest test test testtest test test test\r\ntest test test testtest test test test', 'test test test testtest test test test', 'test test test testtest test test testtest test test testtest test test testtest test test test\r\ntest test test testtest test test testtest test test testtest test test testtest test test test\r\ntest test test testtest test test testtest test test testtest test test testtest test test test', '0');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zlecenia_kategorie`
--

CREATE TABLE IF NOT EXISTS `zlecenia_kategorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` text COLLATE utf16_polish_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=7 ;

--
-- Zrzut danych tabeli `zlecenia_kategorie`
--

INSERT INTO `zlecenia_kategorie` (`id`, `nazwa`) VALUES
(1, 'Strony www'),
(2, 'Administracja'),
(3, 'Marketing'),
(4, 'Hosting'),
(5, 'Namioty'),
(6, 'DJ');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zlecenia_zasoby`
--

CREATE TABLE IF NOT EXISTS `zlecenia_zasoby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zlecenie_id` int(11) DEFAULT NULL,
  `zasob_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_zlecenia_zasoby_zlecenia` (`zlecenie_id`),
  KEY `FK_zlecenia_zasoby_zasoby` (`zasob_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci AUTO_INCREMENT=7 ;

--
-- Zrzut danych tabeli `zlecenia_zasoby`
--

INSERT INTO `zlecenia_zasoby` (`id`, `zlecenie_id`, `zasob_id`) VALUES
(1, 3, 12),
(2, 4, 13),
(3, 5, 14),
(4, 10, 30),
(5, 10, 31),
(6, 10, 32);

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `cennik_zlecen`
--
ALTER TABLE `cennik_zlecen`
  ADD CONSTRAINT `FK_cennik_zlecen_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`),
  ADD CONSTRAINT `FK_cennik_zlecen_cennik_zlecen_oplaty` FOREIGN KEY (`oplata_id`) REFERENCES `cennik_zlecen_oplaty` (`id`);

--
-- Ograniczenia dla tabeli `cennik_zlecen_oplaty`
--
ALTER TABLE `cennik_zlecen_oplaty`
  ADD CONSTRAINT `FK_cennik_zlecen_oplaty_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`);

--
-- Ograniczenia dla tabeli `domeny`
--
ALTER TABLE `domeny`
  ADD CONSTRAINT `FK_domeny_serwery` FOREIGN KEY (`serwer_id`) REFERENCES `serwery` (`id`),
  ADD CONSTRAINT `FK_domeny_klienci` FOREIGN KEY (`klient_id`) REFERENCES `klienci` (`id`);

--
-- Ograniczenia dla tabeli `domeny_pracownicy`
--
ALTER TABLE `domeny_pracownicy`
  ADD CONSTRAINT `FK_domeny_pracownicy_domeny` FOREIGN KEY (`domena_id`) REFERENCES `domeny` (`id`),
  ADD CONSTRAINT `FK_domeny_pracownicy_pracownicy` FOREIGN KEY (`pracownik_id`) REFERENCES `uzytkownicy` (`id`);

--
-- Ograniczenia dla tabeli `faktury`
--
ALTER TABLE `faktury`
  ADD CONSTRAINT `FK_faktury_klienci` FOREIGN KEY (`klient_id`) REFERENCES `klienci` (`id`);

--
-- Ograniczenia dla tabeli `faktury_zasoby`
--
ALTER TABLE `faktury_zasoby`
  ADD CONSTRAINT `FK_faktury_zasoby_faktury` FOREIGN KEY (`faktura_id`) REFERENCES `faktury` (`id`),
  ADD CONSTRAINT `FK_faktury_zasoby_zasoby` FOREIGN KEY (`zasob_id`) REFERENCES `zasoby` (`id`);

--
-- Ograniczenia dla tabeli `informacje_seo`
--
ALTER TABLE `informacje_seo`
  ADD CONSTRAINT `FK_informacje_seo_domeny` FOREIGN KEY (`domena_id`) REFERENCES `domeny` (`id`);

--
-- Ograniczenia dla tabeli `klienci`
--
ALTER TABLE `klienci`
  ADD CONSTRAINT `FK_klienci_adresy` FOREIGN KEY (`adres_id`) REFERENCES `adresy` (`id`),
  ADD CONSTRAINT `FK_klienci_adresy_2` FOREIGN KEY (`adres_faktury_id`) REFERENCES `adresy` (`id`),
  ADD CONSTRAINT `FK_klienci_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`);

--
-- Ograniczenia dla tabeli `klienci_dodatkowe_pola`
--
ALTER TABLE `klienci_dodatkowe_pola`
  ADD CONSTRAINT `FK_klienci_dodatkowe_pola_klienci` FOREIGN KEY (`klient_id`) REFERENCES `klienci` (`id`);

--
-- Ograniczenia dla tabeli `klienci_notatki`
--
ALTER TABLE `klienci_notatki`
  ADD CONSTRAINT `FK_notatki_klienci` FOREIGN KEY (`klient_id`) REFERENCES `klienci` (`id`);

--
-- Ograniczenia dla tabeli `kontakty`
--
ALTER TABLE `kontakty`
  ADD CONSTRAINT `FK_kontakty_pracownicy` FOREIGN KEY (`pracownik_id`) REFERENCES `pracownicy` (`id`);

--
-- Ograniczenia dla tabeli `magazyn_zasoby`
--
ALTER TABLE `magazyn_zasoby`
  ADD CONSTRAINT `FK_magazyn_zasoby_magazyn` FOREIGN KEY (`magazyn_id`) REFERENCES `magazyn` (`id`),
  ADD CONSTRAINT `FK_magazyn_zasoby_zasoby` FOREIGN KEY (`zasob_id`) REFERENCES `zasoby` (`id`);

--
-- Ograniczenia dla tabeli `marketing_adwords`
--
ALTER TABLE `marketing_adwords`
  ADD CONSTRAINT `FK_adwords_klienci` FOREIGN KEY (`klient_id`) REFERENCES `klienci` (`id`),
  ADD CONSTRAINT `FK_adwords_uzytkownicy` FOREIGN KEY (`opiekun_id`) REFERENCES `uzytkownicy` (`id`);

--
-- Ograniczenia dla tabeli `marketing_pozycjonowanie`
--
ALTER TABLE `marketing_pozycjonowanie`
  ADD CONSTRAINT `FK_pozycjonowanie_klienci` FOREIGN KEY (`klient_id`) REFERENCES `klienci` (`id`),
  ADD CONSTRAINT `FK_pozycjonowanie_uzytkownicy` FOREIGN KEY (`opiekun_id`) REFERENCES `uzytkownicy` (`id`);

--
-- Ograniczenia dla tabeli `marketing_social`
--
ALTER TABLE `marketing_social`
  ADD CONSTRAINT `FK_social_klienci` FOREIGN KEY (`klient_id`) REFERENCES `klienci` (`id`),
  ADD CONSTRAINT `FK_social_portale` FOREIGN KEY (`portal_id`) REFERENCES `marketing_portale` (`id`),
  ADD CONSTRAINT `FK_social_uzytkownicy` FOREIGN KEY (`opiekun_id`) REFERENCES `uzytkownicy` (`id`);

--
-- Ograniczenia dla tabeli `marketing_zasoby`
--
ALTER TABLE `marketing_zasoby`
  ADD CONSTRAINT `FK_marketing_adwords` FOREIGN KEY (`adwords_id`) REFERENCES `marketing_adwords` (`id`),
  ADD CONSTRAINT `FK_marketing_pozycjonowanie` FOREIGN KEY (`pozycjonowanie_id`) REFERENCES `marketing_pozycjonowanie` (`id`),
  ADD CONSTRAINT `FK_marketing_social` FOREIGN KEY (`social_id`) REFERENCES `marketing_social` (`id`),
  ADD CONSTRAINT `FK_zasoby` FOREIGN KEY (`zasob_id`) REFERENCES `zasoby` (`id`);

--
-- Ograniczenia dla tabeli `notatki`
--
ALTER TABLE `notatki`
  ADD CONSTRAINT `FK_notatki_domeny` FOREIGN KEY (`domena_id`) REFERENCES `domeny` (`id`),
  ADD CONSTRAINT `FK_notatki_marketing_social` FOREIGN KEY (`socialmedia_id`) REFERENCES `marketing_social` (`id`),
  ADD CONSTRAINT `FK_notatki_serwery` FOREIGN KEY (`serwer_id`) REFERENCES `serwery` (`id`),
  ADD CONSTRAINT `FK_notatki_uzytkownicy` FOREIGN KEY (`pracownik_id`) REFERENCES `uzytkownicy` (`id`);

--
-- Ograniczenia dla tabeli `planowanie_imprez`
--
ALTER TABLE `planowanie_imprez`
  ADD CONSTRAINT `FK_planowanie_imprez_klienci` FOREIGN KEY (`klient_id`) REFERENCES `klienci` (`id`),
  ADD CONSTRAINT `FK_planowanie_imprez_uzytkownicy` FOREIGN KEY (`pracownik_id`) REFERENCES `uzytkownicy` (`id`);

--
-- Ograniczenia dla tabeli `planowanie_imprez_zasoby`
--
ALTER TABLE `planowanie_imprez_zasoby`
  ADD CONSTRAINT `FK_planowanie_imprez_zasoby_planowanie_imprez` FOREIGN KEY (`planowanie_imprez_id`) REFERENCES `planowanie_imprez` (`id`),
  ADD CONSTRAINT `FK_planowanie_imprez_zasoby_zasoby` FOREIGN KEY (`zasob_id`) REFERENCES `zasoby` (`id`);

--
-- Ograniczenia dla tabeli `pracownicy`
--
ALTER TABLE `pracownicy`
  ADD CONSTRAINT `FK_pracownicy_adresy` FOREIGN KEY (`adres_id`) REFERENCES `adresy` (`id`),
  ADD CONSTRAINT `FK_pracownicy_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`);

--
-- Ograniczenia dla tabeli `subdomeny`
--
ALTER TABLE `subdomeny`
  ADD CONSTRAINT `FK_subdomeny_domeny` FOREIGN KEY (`domena_id`) REFERENCES `domeny` (`id`);

--
-- Ograniczenia dla tabeli `uprawnienia_role`
--
ALTER TABLE `uprawnienia_role`
  ADD CONSTRAINT `FK_uprawnienia_role_role` FOREIGN KEY (`rola_id`) REFERENCES `role` (`id`),
  ADD CONSTRAINT `FK_uprawnienia_role_uprawnienia` FOREIGN KEY (`uprawnienie_kod`) REFERENCES `uprawnienia` (`kod`);

--
-- Ograniczenia dla tabeli `uzytkownicy`
--
ALTER TABLE `uzytkownicy`
  ADD CONSTRAINT `FK_uzytkownicy_role` FOREIGN KEY (`rola_id`) REFERENCES `role` (`id`),
  ADD CONSTRAINT `FK_uzytkownicy_zasoby` FOREIGN KEY (`avatar_id`) REFERENCES `zasoby` (`id`);

--
-- Ograniczenia dla tabeli `uzytkownicy_watki_wiadomosci`
--
ALTER TABLE `uzytkownicy_watki_wiadomosci`
  ADD CONSTRAINT `FK_uzytkownicy_wiadomosci_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`),
  ADD CONSTRAINT `FK_uzytkownicy_wiadomosci_wiadomosci` FOREIGN KEY (`watek_wiadomosci_id`) REFERENCES `watki_wiadomosci` (`id`);

--
-- Ograniczenia dla tabeli `uzytkownicy_zasoby`
--
ALTER TABLE `uzytkownicy_zasoby`
  ADD CONSTRAINT `FK_uzytkownicy_zasoby_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`),
  ADD CONSTRAINT `FK_uzytkownicy_zasoby_zasoby` FOREIGN KEY (`zasob_id`) REFERENCES `zasoby` (`id`);

--
-- Ograniczenia dla tabeli `wiadomosci`
--
ALTER TABLE `wiadomosci`
  ADD CONSTRAINT `FK_wiadomosci_uzytkownicy` FOREIGN KEY (`autor_id`) REFERENCES `uzytkownicy` (`id`),
  ADD CONSTRAINT `FK_wiadomosci_watki_wiadomosci` FOREIGN KEY (`watek_id`) REFERENCES `watki_wiadomosci` (`id`);

--
-- Ograniczenia dla tabeli `zgloszenia`
--
ALTER TABLE `zgloszenia`
  ADD CONSTRAINT `FK_zgloszenia_uzytkownicy` FOREIGN KEY (`pracownik_id`) REFERENCES `uzytkownicy` (`id`);

--
-- Ograniczenia dla tabeli `zlecenia`
--
ALTER TABLE `zlecenia`
  ADD CONSTRAINT `FK_zlecenia_cennik_zlecen` FOREIGN KEY (`cennik_zlecenia_id`) REFERENCES `cennik_zlecen` (`id`),
  ADD CONSTRAINT `FK_zlecenia_uzytkownicy` FOREIGN KEY (`klient_id`) REFERENCES `klienci` (`id`),
  ADD CONSTRAINT `FK_zlecenia_uzytkownicy_2` FOREIGN KEY (`opiekun_id`) REFERENCES `uzytkownicy` (`id`);

--
-- Ograniczenia dla tabeli `zlecenia_zasoby`
--
ALTER TABLE `zlecenia_zasoby`
  ADD CONSTRAINT `FK_zlecenia_zasoby_zasoby` FOREIGN KEY (`zasob_id`) REFERENCES `zasoby` (`id`),
  ADD CONSTRAINT `FK_zlecenia_zasoby_zlecenia` FOREIGN KEY (`zlecenie_id`) REFERENCES `zlecenia` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
