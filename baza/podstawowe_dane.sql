-- MySQL dump 10.16  Distrib 10.1.13-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: pastmo_panel
-- ------------------------------------------------------
-- Server version	10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `adresy`
--

LOCK TABLES `adresy` WRITE;
/*!40000 ALTER TABLE `adresy` DISABLE KEYS */;
INSERT INTO `adresy` VALUES (1,NULL,'Firma','Osoba kontaktowa','ulica','nr domu','nr lokalu','kod','miasto','kraj','telefon','email','0','książka adresowa'),(2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0','książka adresowa'),(4,NULL,'Firma','Osoba kontaktowa','ulica','nr domu','nr lokalu','kod','miasto','kraj','telefon',NULL,'0','książka adresowa'),(7,NULL,'Glasso','Grzegorz Lachowicz','Kazimierza Wielkiego','67',NULL,'50-077','Wrocław','Polska','+48 71 344 44 17','biuro@grupaglasso.pl','0','adres nadawcy'),(8,NULL,'Pastmo','Paweł Kaźmierczak','Tomaszowska','20','2','50-523','Wrocław','PL','777777','pawel.kazmierczak@pastmo.pl','0','książka adresowa'),(9,1,NULL,NULL,'Tomaszowska, 20, 8',NULL,NULL,'50-523','Wrocław','Polska',NULL,NULL,'0','książka adresowa');
/*!40000 ALTER TABLE `adresy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `dane_do_faktury`
--

LOCK TABLES `dane_do_faktury` WRITE;
/*!40000 ALTER TABLE `dane_do_faktury` DISABLE KEYS */;
INSERT INTO `dane_do_faktury` VALUES (1,NULL,1,'123',NULL);
/*!40000 ALTER TABLE `dane_do_faktury` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `emaile`
--

LOCK TABLES `emaile` WRITE;
/*!40000 ALTER TABLE `emaile` DISABLE KEYS */;
INSERT INTO `emaile` VALUES (1,1,4,NULL,NULL,130,NULL,'Sun, 9 Apr 2017 11:24:28 +0000 (UTC)','nazwa.pl <kontakt@nazwa.pl>','Witamy w poczcie elektronicznej nazwa.pl!','<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\n            \"http://www.w3.org/TR/html4/loose.dtd\">\n<html>\n <head>\n<title>\n      nazwa.pl\n</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n<style type=\"text/css\">\na {outline: none; color: #114170;}\n.na-link{margin: 0 15px;}\n</style>\n\n </head>\n <body bgcolor=\"#ffffff\">\n  <!-- www.nazwa.pl - Czas wybrac dobra nazwe! a45181d6c5084f705d9b -->\n    <table align=\"center\" cellspacing=\"0\" cellpadding=\"0\" width=\"744\" border=\"0\">\n      <tr>\n        <td width=\"37\" height=\"10\"></td>\n        <td width=\"670\"></td>\n        <td width=\"37\"></td>\n      </tr>\n      \n      <tr>\n        <td colspan=\"3\" height=\"95\"><a href=\"https://www.nazwa.pl/domeny-hosting-serwery,4134.html\"><img border=\"0\" src=\"cid:1\" alt=\"\" /></a></td>\n      </tr>\n      \n      <tr>\n        <td colspan=\"3\" height=\"5\"></td>\n      </tr>\n      \n      <tr>\n        \n        <td colspan=\"3\">\n          \n          <font color=\"#000000\" face=\"verdana\" size=\"2\">\n\nZachęcamy Państwa do korzystania z nowej wersji aplikacji <i><b>Active.<font color=\"#0099cc\">office</font></b></i>.<br />\n<br />\n<i><b>Active.<font color=\"#0099cc\">office</font></b></i> to profesjonalny manager poczty elektronicznej, dostępny przez przeglądarkę internetową pod adresem <a href=\"hhtp:/https://www.nazwa.pl/poczta.nazwa.pl\" target=\"_blank\"><font color=\"#005b9a\">http://poczta.nazwa.pl</font></a> Do mobilnej wersji programu <i><b>Active.<font color=\"#0099cc\">office</font></b><b><sup><font color=\"#888888\">lajt</font></sup></b></i> mogą Państwo zalogować się pod adresem <a href =\"http://poczta.nazwa.pl/m\"><font color=\"#005b9a\">http://poczta.nazwa.pl/m</font></a>.<br />\n<br />\nW ramach aplikacji <i><b>Active.<font color=\"#0099cc\">office</font></b></i> mogą Państwo korzystać z następujących udogodnień:\n \n<ul style=\"list-style:none;\">\n <li><img src=\"cid:2\" alt=\"arrow\"/><b>Kompleksowa obsługa poczty e-mail</b> –  szybkie, a przede wszystkim wygodne zarządzanie korespondencją prywatną i firmową.</li>\n <li><img src=\"cid:2\" alt=\"arrow\"/><b>Profesjonalny organizer</b> –  pakiet narzędzi, który pozwoli Państwu sprawnie organizować czas za pomocą kalendarza, planować spotkania i zadania, a także zarządzać kontaktami.</li>\n <li><img src=\"cid:2\" alt=\"arrow\"/><b>Mobilność</b> –  dzięki lżejszej wersji aplikacji Active.office lajt, mogą Państwo korzystać z poczty e-mail w każdym miejscu i o każdej porze.</li>\n <li><img src=\"cid:2\" alt=\"arrow\"/><b>Integracja z serwisami społecznościowymi</b> –  odpowiednia konfiguracja aplikacji Active.office, umożliwi Państwu połączenie konta e-mail z popularnymi serwisami społecznościowymi (np. Facebook czy Twitter) oraz innymi skrzynkami pocztowymi.</li>\n <li><img src=\"cid:2\" alt=\"arrow\"/><b>Możliwość personalizacji widoków</b> –  dzięki tej opcji, mogą Państwo dostosować rozmieszczenie poszczególnych elementów programu do własnych potrzeb.</li>\n <li><img src=\"cid:2\" alt=\"arrow\"/><b>Obsługa zewnętrznych kont e-mail</b> – program pozwala skonfigurować i przyłączyć nieograniczoną liczbę dodatkowych adresów e-mail.</li>\n <li><img src=\"cid:2\" alt=\"arrow\"/><b>Intuicyjna obsługa</b> –  Active.office posiada wiele funkcji, ale jest jednocześnie prosty w obsłudze. Interfejs aplikacji nie sprawi problemu nawet niezaawansowanym użytkownikom.</li>\n <li><img src=\"cid:2\" alt=\"arrow\"/><b>Opcje dodatkowe</b> – Dodatkowe opcje aplikacji pozwalają na m.in.: tworzenie aliasów i list dystrybucyjnych, przekierowanie poczty na inne adresy e-mail, powiadomienia SMS czy ustalenie automatycznej wiadomości zwrotnej (autoresponder).</li>\n</ul>\n\nZe wszystkimi możliwościami aplikacji <i><b>Active.<font color=\"#0099cc\">office</font></b></i> mogą Państwo zapoznać się w naszym <a href =\"https://www.nazwa.pl/pomoc/narzedzia/activeoffice/\"><font color=\"#005b9a\">Centrum Pomocy</font></a>.<br />\n<br />\nMamy nadzieję, że nowa wersja programu Active.office pozwoli Państwu jeszcze efektywniej korzystać z poczty elektronicznej w nazwa.pl.\n\n</tr>\n <tr width=\"744\" height=\"35\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\" border:none;\"><br /></tr>\n\n\n \n <tr cellpadding=\"0\" cellspacing=\"0\" border=\"0\" bgcolor=\"#FAFAFA\" style=\" border:none; background: #FAFAFA;\">\n     <td width=\"51\" height=\"58\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border:none;\">\n            <img src=\"cid:3\" border=\"0\" width=\"51\" height=\"58\" style=\"width:51px; height:58px; border:none; display:block;\" alt=\"nazwa.pl\">\n        </td>\n<td width=\"678\" height=\"34\" style=\"width: 100%; color: #676866; text-align: left; font-family: Arial; font-size: 14px; border-top: 1px solid #ddd; border-bottom: 1px solid #ddd;\">\n <span>Coraz częściej dostajemy sygnały dotyczące prób podszywania się pod nas i oszukania naszych Klientów.\n Zapoznaj się z przykładami najczęstszych nadużyć i bądź bezpieczny. <a href=\"https://blog.nazwa.pl/nieetyczne-praktyki-na-rynku-domen-przestrzegamy-przed-nieuczciwymi-dzialaniami/\" style=\"color: #F37022; text-decoration: none;\">WIĘCEJ →</a></span>\n</td>\n     <td width=\"15\" height=\"58\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" bgcolor=\"#FAFAFA\" style=\" border:none; background: #FAFAFA;\">\n            <img src=\"cid:4\" border=\"0\" width=\"15\" height=\"58\" style=\"width:15px; height:58px; border:none; display:block;\" alt=\"nazwa.pl\">\n        </td>\n </tr>\n</table>\n\n<table align=\"center\" width=\"744\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border:none;\">\n <tr width=\"744\" height=\"0\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\" border:none;\"><br /></tr>\n <tr width=\"744\" height=\"35\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\" border:none;\">\n\n<td width=\"175\" height=\"35\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\" border:none;\" colspan=\"5\">\n \n                <img border=\"0\" src=\"cid:5\" alt=\"\" />\n </a>\n        </td>\n\n<td width=\"184\" height=\"35\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\" border:none;\" colspan=\"5\">\n \n                <img border=\"0\" src=\"cid:6\" alt=\"\" />\n </a>\n        </td>\n\n<td width=\"128\" height=\"35\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\" border:none;\" colspan=\"5\">\n <a href=\"https://blog.nazwa.pl/\">\n                <img border=\"0\" src=\"cid:7\" alt=\"\" />\n </a>\n        </td>\n\n<td width=\"130\" height=\"35\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\" border:none;\" colspan=\"5\">\n <a href=\"http://pomoc.nazwa.pl/baza-wiedzy/\">\n                <img border=\"0\" src=\"cid:8\" alt=\"\" />\n </a>\n        </td>\n\n<td width=\"127\" height=\"35\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\" border:none;\" colspan=\"5\">\n <a href=\"https://www.nazwa.pl/o-firmie/kontakt/\">\n                <img border=\"0\" src=\"cid:9\" alt=\"\" />\n </a>\n        </td>\n </tr>\n</table>\n\n<table align=\"center\" width=\"744\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width:744px; border:none;\">\n    <tr width=\"744\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width:744px; border:none;\">\n        <td width=\"744\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width:744px; border:none;\">\n <span style=\"font-style: normal; color: #a7a7a7; font-family: tahoma;display: inline; width: 100%; height: 100%; font-size: 10px; line-height: 14px;\"><br />\n nazwa.pl sp. z o.o. z siedzibą w Krakowie, ul. Cystersów 20a, 31-553 Kraków, miejsce rejestracji: Sąd Rejonowy dla Krakowa Śródmieścia Wydział XI\nKrajowego Rejestru Sądowego, KRS: 0000594747, wysokość kapitału zakładowego\n114 384 000 PLN (w całości opłacony), REGON: 120805512, NIP: 6751402920. \n </span>\n      </td>\n </tr>\n</table>\n\n</body>\n</html>\n','<<1432254030.577835.1491737068367@netart.pl>>','odbiorcza','nieprzeczytana'),(2,1,4,NULL,NULL,140,NULL,'Tue, 2 May 2017 00:13:04 +0200 (CEST)','pawel.kazmierczak@pastmo.pl <pawel.kazmierczak@pastmo.pl>','Re: czy maile z cremini wychodzą?','<!DOCTYPE html>\n<html><head>\n    <meta charset=\"UTF-8\">\n<style type=\"text/css\">.mceResizeHandle {position: absolute;border: 1px solid black;background: #FFF;width: 5px;height: 5px;z-index: 10000}.mceResizeHandle:hover {background: #000}img[data-mce-selected] {outline: 1px solid black}img.mceClonedResizable, table.mceClonedResizable {position: absolute;outline: 1px dashed black;opacity: .5;z-index: 10000}\n</style></head><body style=\"\"><div>To super!:)</div>\n<div><br>&#62; Dnia 2 maj 2017 o 00:11 &#34;test@pastmo.pl&#34; &#60;test@pastmo.pl&#62; napisa&#322;(a):<br>&#62; <br>&#62; <br>&#62; tre&#347;&#263; wiadomo&#347;ci</div></body></html>\n \n','<<72138818.780386.1493676784197@poczta.nazwa.pl>>','odbiorcza','nieprzeczytana');
/*!40000 ALTER TABLE `emaile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `emaile_foldery`
--

LOCK TABLES `emaile_foldery` WRITE;
/*!40000 ALTER TABLE `emaile_foldery` DISABLE KEYS */;
/*!40000 ALTER TABLE `emaile_foldery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `emaile_zasoby`
--

LOCK TABLES `emaile_zasoby` WRITE;
/*!40000 ALTER TABLE `emaile_zasoby` DISABLE KEYS */;
INSERT INTO `emaile_zasoby` VALUES (1,1,1,131),(2,1,1,132),(3,1,1,133),(4,1,1,134),(5,1,1,135),(6,1,1,136),(7,1,1,137),(8,1,1,138),(9,1,1,139);
/*!40000 ALTER TABLE `emaile_zasoby` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `faktury`
--

LOCK TABLES `faktury` WRITE;
/*!40000 ALTER TABLE `faktury` DISABLE KEYS */;
/*!40000 ALTER TABLE `faktury` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `faktury_zasoby`
--

LOCK TABLES `faktury_zasoby` WRITE;
/*!40000 ALTER TABLE `faktury_zasoby` DISABLE KEYS */;
/*!40000 ALTER TABLE `faktury_zasoby` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `firmy`
--

LOCK TABLES `firmy` WRITE;
/*!40000 ALTER TABLE `firmy` DISABLE KEYS */;
INSERT INTO `firmy` VALUES (1,NULL,NULL,NULL,NULL,3,NULL,'Glasso','2016-04-18 11:05:25',NULL),(2,NULL,NULL,1,2,1,1,'Pastmo','2016-04-18 11:21:08',NULL);
/*!40000 ALTER TABLE `firmy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `jezyki`
--

LOCK TABLES `jezyki` WRITE;
/*!40000 ALTER TABLE `jezyki` DISABLE KEYS */;
INSERT INTO `jezyki` VALUES ('en_US','en','English'),('pl_PL','pl','Polski');
/*!40000 ALTER TABLE `jezyki` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `klienci`
--

LOCK TABLES `klienci` WRITE;
/*!40000 ALTER TABLE `klienci` DISABLE KEYS */;
INSERT INTO `klienci` VALUES (2,1,4,1,4,'Mediafocus sp. z o.o.','Mediafocus sp. z o.o.','585','Nowy','http://www.mediafocus.pl/',NULL),(3,1,5,7,4,'Zeltech SA','Zeltech SA','727','Nowy','http://www.zeltech.eu',NULL),(4,1,6,8,8,'Locus Security Duo Sp. z o.o.','Locus Security Duo Sp. z o.o.','852','Stały','http://www.locus-security.pl',NULL),(5,1,3,4,7,'Galmed Wytwórnia Sprzętu Medycznego','Galmed Wytwórnia Sprzętu Medycznego','5540468264','Stały','http://www.galmed.com.pl',NULL),(6,1,4,1,1,'Cementownia Kraków Nowa Huta Sp. z o.o.','Cementownia Kraków Nowa Huta Sp. z o.o.','9552249328','Zrealizowany','http://www.cknh.pl',NULL),(7,1,3,8,2,'Beskid Sp. z o.o.','Beskid Sp. z o.o.','5471876088','Zrealizowany','http://www.beskid.com.pl',NULL);
/*!40000 ALTER TABLE `klienci` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `klienci_dodatkowe_pola`
--

LOCK TABLES `klienci_dodatkowe_pola` WRITE;
/*!40000 ALTER TABLE `klienci_dodatkowe_pola` DISABLE KEYS */;
/*!40000 ALTER TABLE `klienci_dodatkowe_pola` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `klienci_notatki`
--

LOCK TABLES `klienci_notatki` WRITE;
/*!40000 ALTER TABLE `klienci_notatki` DISABLE KEYS */;
INSERT INTO `klienci_notatki` VALUES (2,NULL,2,'firma nr 1','2016-04-20 19:48:49'),(3,NULL,3,'to jest nowa notatka','2016-04-20 19:53:24'),(4,NULL,4,'brak notatki','2016-04-20 20:24:15'),(5,NULL,5,'Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor.','2016-04-20 21:07:33'),(6,NULL,6,'Aliquam erat ac ipsum. Integer aliquam purus. Quisque lorem tortor fringilla sed, vestibulum id.','2016-04-20 21:18:01'),(7,NULL,7,'Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum. Integer aliquam purus. Quisque lorem tortor fringilla sed, vestibulum id, eleifend justo vel bibendum sapien massa ac turpis faucibus orci luctus non, consectetuer lobortis quis, varius in, purus. Integer ultrices posuere cubilia Curae, Nulla ipsum dolor lacus, suscipit adipiscing. Cum sociis natoque penatibus et ultrices volutpat. Nullam wisi ultricies a, gravida vitae, dapibus risus ante sodales lectus blandit eu, tempor diam pede cursus vitae, ultricies eu, faucibus quis, porttitor eros cursus lectus, pellentesque eget, bibendum a, gravida ullamcorper quam. Nullam viverra consectetuer. Quisque cursus et, porttitor risus. Aliquam sem. In hendrerit nulla quam nunc, accumsan congue. Lorem ipsum primis in nibh vel risus. Sed vel lectus. Ut sagittis, ipsum dolor quam.','2016-04-20 21:23:02');
/*!40000 ALTER TABLE `klienci_notatki` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `konta`
--

LOCK TABLES `konta` WRITE;
/*!40000 ALTER TABLE `konta` DISABLE KEYS */;
INSERT INTO `konta` VALUES (1,1,'2017-04-17 09:39:23'),(2,2,'2017-04-17 09:39:28'),(3,3,'2017-04-17 09:39:31'),(4,4,'2017-04-17 09:39:35'),(5,5,'2017-04-17 09:39:40'),(6,6,'2017-04-17 09:39:47');
/*!40000 ALTER TABLE `konta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `konta_mailowe`
--

LOCK TABLES `konta_mailowe` WRITE;
/*!40000 ALTER TABLE `konta_mailowe` DISABLE KEYS */;
INSERT INTO `konta_mailowe` VALUES (2,NULL,1,'serwer1578643.home.pl',NULL,'testowe1@grupaglasso.pl','7b39106e434fce318cc17903b1daef0bbcb30432d18d74885064ef8e96ada79cMTU0ODc0NTQ1OTg3NDY1NLb4IpGizjZF303PcyzTBCo=',NULL,'587'),(3,NULL,1,'serwer1578643.home.pl',NULL,'testowe2@grupaglasso.pl','7e3f1f3f558094660b36ff07c5218deb048d186180fdefad78f1b89933c21d5bMTU0ODc0NTQ1OTg3NDY1NOHrmpmqd1JRM5T8rwybXu0=',NULL,'587'),(4,1,1,'pastmo.pl','pastmo.pl','test@pastmo.pl','949b097e086f3680255af0845ffa39ca92bc78375004ad3c5c9428775503985fMTU0ODc0NTQ1OTg3NDY1NFWtZ8sjxZLsZbiswX3sEXOhWzeJJysS7LDALG1Ssruu','','test@pastm');
/*!40000 ALTER TABLE `konta_mailowe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `logi`
--

LOCK TABLES `logi` WRITE;
/*!40000 ALTER TABLE `logi` DISABLE KEYS */;
/*!40000 ALTER TABLE `logi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `logi_kategorie`
--

LOCK TABLES `logi_kategorie` WRITE;
/*!40000 ALTER TABLE `logi_kategorie` DISABLE KEYS */;
INSERT INTO `logi_kategorie` VALUES ('baza_produ','Edycja stanów magazynowych'),('pastmo_pan','Błędy poczty email'),('sta_mag','Wartość rynkowa magazynu');
/*!40000 ALTER TABLE `logi_kategorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `pomoc`
--

LOCK TABLES `pomoc` WRITE;
/*!40000 ALTER TABLE `pomoc` DISABLE KEYS */;
INSERT INTO `pomoc` VALUES (1,NULL,'default','default help','To jest przykładowa treść pomocy do wyświetlanego ekranu'),(2,NULL,'log_pro','profil uzytkownika','Tutaj znajdują się podstawowe informacje konta użytkownika.'),(3,NULL,'log_uzy','ust. uzytkownika','Skonfiguruj funkcje systemu ERP.'),(4,NULL,'dsh_dsh','dashboard','Tutaj wyświetlane są bieżące komunikaty. Każda wiadomość posiada hiperłącze do elementów do których się odnosi.  Z prawej strony okna znajduje się lista projektów z bliskim terminem wysyłki.'),(5,NULL,'wia_okn','wiadomosci okno','To okno słuzy do wysyłania wiadomości do użytkowników oraz przeglądania wcześniejszych rozmów.'),(6,NULL,'kos_kln','koszyk klienta','Koszyk zawiera produkty dodane przez użytkownika w trakcie użytkowania systemu. Po wybraniu produktu bądź produktów użytkownik może wskazać jedną z akcji na danych pozycjach.'),(7,NULL,'kos_dst','koszyk dostaw','Koszyk dostaw jest wspólny dla pracowników Glasso. Służy do składania codziennego, wspólnego zamówienia produtków z magazynu GLASSO.');
/*!40000 ALTER TABLE `pomoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `powiadomienia`
--

LOCK TABLES `powiadomienia` WRITE;
/*!40000 ALTER TABLE `powiadomienia` DISABLE KEYS */;
INSERT INTO `powiadomienia` VALUES (2,NULL,NULL,'W projekcie Poboczny zmieniono datę ostatniego kroku milowego na .','3','2017-01-03 12:28:02','2017-01-03 12:28:02','-','powiadomienia'),(3,NULL,NULL,'W projekcie Poboczny zmieniono datę ostatniego kroku milowego na .','3','2017-01-03 12:28:03','2017-01-03 12:28:03','-','powiadomienia'),(4,NULL,NULL,'W projekcie Poboczny zmieniono datę ostatniego kroku milowego na .','3','2017-01-03 12:28:03','2017-01-03 12:28:03','-','powiadomienia'),(5,NULL,'prd_ind','Projekt Grudzień jest zaplanowany dzisiaj do produkcji.',NULL,'2017-02-23 15:03:38','2016-12-26 23:00:00','-','powiadomienia'),(6,NULL,'prd_ind','Projekt Grudzień jest zaplanowany dzisiaj do produkcji.',NULL,'2017-02-23 15:05:16','2016-12-26 23:00:00','-','powiadomienia'),(7,NULL,'prd_ind','Projekt Grudzień jest zaplanowany dzisiaj do produkcji.',NULL,'2017-03-03 21:13:51','2016-12-26 23:00:00','-','powiadomienia'),(8,NULL,'prj_szc','Zostałeś dodany do projektu Grudzień.','6','2017-03-07 08:04:52','2017-03-07 08:04:52','-','powiadomienia'),(9,NULL,'prj_szc','Zostałeś dodany do projektu Grudzień.','6','2017-03-07 08:04:52','2017-03-07 08:04:52','-','powiadomienia'),(10,NULL,'prs_wsp','Dodano nową wiadomość od współpracownika w projekcie Grudzień.','6','2017-03-07 08:07:18','2017-03-07 08:07:18','-','powiadomienia'),(11,NULL,'prs_wsp','Dodano nową wiadomość od współpracownika w projekcie Grudzień.','6','2017-03-07 08:07:18','2017-03-07 08:07:18','-','powiadomienia'),(12,NULL,'prs_wsp','Dodano nową wiadomość od współpracownika w projekcie Grudzień.','6','2017-03-07 08:09:00','2017-03-07 08:09:00','-','powiadomienia'),(13,NULL,'prs_wsp','Dodano nową wiadomość od współpracownika w projekcie Grudzień.','6','2017-03-07 08:13:45','2017-03-07 08:13:45','-','powiadomienia');
/*!40000 ALTER TABLE `powiadomienia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `powiadomienia_kategorie`
--

LOCK TABLES `powiadomienia_kategorie` WRITE;
/*!40000 ALTER TABLE `powiadomienia_kategorie` DISABLE KEYS */;
INSERT INTO `powiadomienia_kategorie` VALUES (1,NULL,'prj_szc','projekty szczegoly','projekty','szczegoly'),(2,NULL,'prj_ind','projekty index','projekty','index'),(3,NULL,'prj_wiz','projekty wizualizacje','projekty','wizualizacje'),(4,NULL,'wia_ind','wiadomosci index','wiadomosci','index'),(5,NULL,'prd_ind','produkcja index','produkcja','index'),(6,NULL,'pro_szc','produkty szczegoly','produkty','szczegoly'),(7,NULL,'fir_szc','firmy szczegoly','firmy','szczegoly'),(8,NULL,'log_pro','logowanie profil','logowanie','profil'),(9,NULL,'prs_kli','wiadomość od klienta','projekty','szczegoly'),(10,NULL,'prs_wsp','wiadomość od współpracownika','projekty','szczegoly');
/*!40000 ALTER TABLE `powiadomienia_kategorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Admin','admin','pracownik'),(2,'Klient','klient','klient'),(3,'Grafik','grafik','pracownik'),(4,'Grafik główny','grafik_gl','pracownik'),(5,'Sprzedawca','sprzedawca','pracownik');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tagi`
--

LOCK TABLES `tagi` WRITE;
/*!40000 ALTER TABLE `tagi` DISABLE KEYS */;
INSERT INTO `tagi` VALUES (11,NULL,1,'Tag_projekt_grudzień_GL','rgb(251, 76, 47)'),(12,NULL,1,'poboczny_GL','rgb(73, 134, 231)'),(13,NULL,1,'wspolny_tag','rgb(179, 239, 211)'),(14,NULL,2,'wspolny_tag',NULL);
/*!40000 ALTER TABLE `tagi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `udostepnione_zasoby`
--

LOCK TABLES `udostepnione_zasoby` WRITE;
/*!40000 ALTER TABLE `udostepnione_zasoby` DISABLE KEYS */;
/*!40000 ALTER TABLE `udostepnione_zasoby` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `udostepnione_zasoby_foldery`
--

LOCK TABLES `udostepnione_zasoby_foldery` WRITE;
/*!40000 ALTER TABLE `udostepnione_zasoby_foldery` DISABLE KEYS */;
/*!40000 ALTER TABLE `udostepnione_zasoby_foldery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `udostepnione_zasoby_uzytkownicy`
--

LOCK TABLES `udostepnione_zasoby_uzytkownicy` WRITE;
/*!40000 ALTER TABLE `udostepnione_zasoby_uzytkownicy` DISABLE KEYS */;
/*!40000 ALTER TABLE `udostepnione_zasoby_uzytkownicy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `uprawnienia`
--

LOCK TABLES `uprawnienia` WRITE;
/*!40000 ALTER TABLE `uprawnienia` DISABLE KEYS */;
INSERT INTO `uprawnienia` VALUES ('auk_pup','Przypisanie innych userów'),('auk_wdo','Dodawania ofert'),('auk_wep','Edycja pytań'),('auk_wop','Odpowiedzi na pytania'),('auk_wpl','Dodawanie plików do wyceny'),('auk_wpy','Możliwość zadawania pytań'),('auk_wwi','Widoczność użytkowników w wycenach'),('auk_wwo','Wybór oferty'),('auk_wza','Dodawanie zapytań o wycenę- projekt kafelka'),('auk_zpa','Pliki projektu akceptacja'),('auk_zpd','Pliki projektu dodawanie'),('auk_zpk','Pliki projektu komentarz'),('auk_zsa','Szczegóły zamówienia akceptacja'),('auk_zsd','Szczegóły zamówienia dodawanie'),('auk_zse','Szczegóły zamówienia edycja- aukcje'),('auk_zwz','Wiadomości i załączniki'),('bpd_kce','Cennik'),('bpd_kip','Informacje podstawowe'),('bpd_kka','Kategorie'),('bpd_kko','Konkurencja'),('bpd_kkp','Komponenty dodatkowe'),('bpd_kma','Magazyn'),('bpd_kod','Odpady'),('bpd_kpl','Pliki'),('bpd_kpo','Podstawki'),('bpd_kpr','Karta produktu'),('bpd_kpu','Pudełka'),('bpd_kre','Rezerwacje'),('bpd_kta','Tagi'),('bpd_kwa','Wariacje'),('bpd_kwp','Wyceny producentów'),('bpd_kwy','Komponenty wymienne'),('bpd_kzd','Zdjęcia'),('bpd_kzn','Zniżki'),('bpd_m3d','Moduł 3D'),('cha_all','Komunikacja z dowolnymi pracownikami Glasso'),('cha_wys','Wyświetlanie chatu'),('kos_ekp','Edycja koszyka produktów'),('kos_tpk','Tworzenie projektu z koszyka'),('men_auk','Menu Moduł aukcyjny'),('men_awy','Menu Moduł aukcyjny wyceny'),('men_aza','Menu Moduł aukcyjny zamówienia'),('men_azk','Menu Moduł aukcyjny zakończone'),('men_bka','Menu Baza produktów kategorie'),('men_bpd','Menu Baza produktów'),('men_bpr','Menu Baza produktów produkty'),('men_bus','Menu Baza produktów ustawienia'),('men_bzm','Menu Baza produktów zmiany magazynowe'),('men_bzn','Menu Baza produktów zniżki'),('men_das','Menu dashboard'),('men_edi','Menu Magento EDI'),('men_ema','Menu Email'),('men_fir','Menu Firmy'),('men_kal','Menu Kalendarz'),('men_kli','Menu Koszyk klienta'),('men_kos','Menu Koszyk'),('men_ksp','Menu Koszyk dostaw'),('men_mag','Menu Magazyn'),('men_mdo','Menu Magazyn dostawa'),('men_mpl','Menu Manager plików'),('men_pdy','Menu Produkty'),('men_prd','Menu Produkcja'),('men_pro','Menu Projekty'),('men_rol','Menu Role'),('men_skl','Menu Statystyki klientów'),('men_sog','Menu Statystyki ogólne'),('men_spr','Menu Statystyki sprzedawców'),('men_sta','Menu Statystyki'),('men_swm','Menu Statystyki wartość magazynu'),('men_urz','Menu Urządzenia'),('men_uzp','Menu Użytkownicy podmenu'),('men_uzy','Menu Użytkownicy'),('men_wia','Menu Wiadomości'),('men_wik','Menu Wiki'),('men_wiz','Menu Wizualizacje'),('men_wka','Menu Wysyłka książka adresowa'),('men_wmp','Menu Wysyłka moje przesyłki'),('men_wnp','Menu Wysyłka nowa przesyłka'),('men_wpn','Menu Wysyłka potwierdzenie nadania'),('men_wpr','Menu Wysyłka projekty'),('men_wys','Menu Wysyłka'),('prj_fak','Szczegóły firmy akceptacja'),('prj_fde','Szczegoly firmy dodawanie edycja'),('prj_ind','Dodawanie indywizualizacji'),('prj_kow','Kończenie wizualizacji'),('prj_kwi','Komentarz do wizualizacji'),('prj_sak','Akcjeptacja szczegółów zamówienia'),('prj_sdo','Dodawanie szczegółów zamówienia'),('prj_sed','Szczegóły zamówienia edycja- projekty'),('prj_sta','Edycja statusów'),('prj_tap','Tagi projektu'),('prj_taw','Tagi wiadomości'),('prj_udo','Użytkownicy w projekcie przypisywanie'),('prj_wak','Akcjaptacja wizualizacji i indywidualizacji'),('prj_wia','Wiadomości w projektach'),('prj_wid','Dodawanie wizualizacji'),('pro_ace','Akceptacja cen w projekcie'),('pro_ain','Akceptowanie indywidualizacji'),('pro_app','Aktualizacja produktów w projekcie'),('pro_awi','Akceptowanie wizualizacji'),('pro_etp','Edycja transportu produktów'),('pro_eup','Edycja użytkowników w projekcie'),('pro_ppr','Przeglądanie projektów'),('pro_wce','Widoczność cen przed akceptacją'),('pty_dig','Dostęp do projektów innych grafików'),('pty_pgb','Przekazanie projektu innemy grafikowi bez zgody'),('pty_pgz','Przekazanie projektu innemy grafikowi'),('pty_psb','Przekazywanie projektu innemu sprzedawcy bez zgody'),('pty_psz','Przekazywanie projektu innemu sprzedawcy za zgodą'),('rol_adm','Uprawnienia administratora'),('rol_def','Domyślne uprawnienie'),('rol_eup','Edycja uprawnień'),('rol_ggr','Uprawnienia głównego grafika'),('rol_gra','Uprawnienia grafika'),('rol_kli','Uprawnienia klienta'),('rol_pra','Uprawnienia pracownika'),('rol_spr','Uprawnienia sprzedawcy'),('rol_tup','Tworzenie nowych uprawnień'),('skl_psl','Przeglądanie sklepu'),('sta_kli','Przeglądanie statystyk klientów'),('sta_ogo','Przeglądanie statystyk ogólnych'),('sta_psg','Przeglądanie statystyk grafików'),('sta_psp','Przeglądanie statystyk produków'),('sta_pss','Przeglądanie statystyk sprzedawców'),('ust_pus','Przeglądanie ustawień ERP'),('uzy_euz','Edycja użytkowników'),('uzy_ppu','Przeglądanie profilii użytkowników'),('uzy_tuz','Tworzenie nowych użytkowników'),('zam_pwy','Przeglądanie wycen');
/*!40000 ALTER TABLE `uprawnienia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `uprawnienia_kategorie`
--

LOCK TABLES `uprawnienia_kategorie` WRITE;
/*!40000 ALTER TABLE `uprawnienia_kategorie` DISABLE KEYS */;
INSERT INTO `uprawnienia_kategorie` VALUES ('auk','Moduł aukcyjny'),('bpd','Baza produktów'),('cha','Chat'),('kos','Koszyk'),('men','Menu'),('prj','Projekt'),('pro','Produkty'),('pty','Projekty'),('rol','Uprawnienia'),('skl','Sklep'),('sta','Statystyki'),('ust','Ustawienia ERP'),('uzy','Użytkownicy'),('wys','Wysyłka'),('zam','Moduł zamówień');
/*!40000 ALTER TABLE `uprawnienia_kategorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `uprawnienia_role`
--

LOCK TABLES `uprawnienia_role` WRITE;
/*!40000 ALTER TABLE `uprawnienia_role` DISABLE KEYS */;
INSERT INTO `uprawnienia_role` VALUES (16,2,'rol_def'),(20,2,'rol_kli'),(144,3,'rol_def'),(145,3,'rol_gra'),(146,4,'rol_def'),(147,4,'rol_ggr'),(148,4,'rol_pra'),(149,5,'rol_def'),(150,5,'rol_pra'),(151,5,'rol_spr'),(240,1,'auk_wdo'),(241,1,'auk_wpl'),(242,1,'auk_wza'),(243,1,'auk_wep'),(244,1,'auk_wpy'),(245,1,'auk_wop'),(246,1,'auk_zpa'),(247,1,'auk_zpd'),(248,1,'auk_zpk'),(249,1,'auk_pup'),(250,1,'auk_zsa'),(251,1,'auk_zsd'),(252,1,'auk_zse'),(253,1,'auk_zwz'),(254,1,'auk_wwi'),(255,1,'auk_wwo'),(256,1,'bpd_kce'),(257,1,'bpd_kip'),(258,1,'bpd_kpr'),(259,1,'bpd_kka'),(260,1,'bpd_kkp'),(261,1,'bpd_kwy'),(262,1,'bpd_kko'),(263,1,'bpd_kma'),(264,1,'bpd_m3d'),(265,1,'bpd_kod'),(266,1,'bpd_kpl'),(267,1,'bpd_kpo'),(268,1,'bpd_kpu'),(269,1,'bpd_kre'),(270,1,'bpd_kta'),(271,1,'bpd_kwa'),(272,1,'bpd_kwp'),(273,1,'bpd_kzd'),(274,1,'bpd_kzn'),(275,1,'cha_all'),(276,1,'cha_wys'),(277,1,'kos_ekp'),(278,1,'kos_tpk'),(279,1,'men_bpd'),(280,1,'men_bka'),(281,1,'men_bpr'),(282,1,'men_bus'),(283,1,'men_bzm'),(284,1,'men_bzn'),(285,1,'men_das'),(286,1,'men_ema'),(287,1,'men_fir'),(288,1,'men_kal'),(289,1,'men_kos'),(290,1,'men_ksp'),(291,1,'men_kli'),(292,1,'men_mag'),(293,1,'men_mdo'),(294,1,'men_edi'),(295,1,'men_mpl'),(296,1,'men_auk'),(297,1,'men_awy'),(298,1,'men_azk'),(299,1,'men_aza'),(300,1,'men_prd'),(301,1,'men_pdy'),(302,1,'men_pro'),(303,1,'men_rol'),(304,1,'men_sta'),(305,1,'men_skl'),(306,1,'men_sog'),(307,1,'men_spr'),(308,1,'men_swm'),(309,1,'men_urz'),(310,1,'men_uzy'),(311,1,'men_uzp'),(312,1,'men_wia'),(313,1,'men_wik'),(314,1,'men_wiz'),(315,1,'men_wys'),(316,1,'men_wka'),(317,1,'men_wmp'),(318,1,'men_wnp'),(319,1,'men_wpn'),(320,1,'men_wpr'),(321,1,'prj_wak'),(322,1,'prj_sak'),(323,1,'prj_ind'),(324,1,'prj_sdo'),(325,1,'prj_wid'),(326,1,'prj_sta'),(327,1,'prj_kwi'),(328,1,'prj_kow'),(329,1,'prj_fde'),(330,1,'prj_fak'),(331,1,'prj_sed'),(332,1,'prj_tap'),(333,1,'prj_taw'),(334,1,'prj_udo'),(335,1,'prj_wia'),(336,1,'pro_ace'),(337,1,'pro_ain'),(338,1,'pro_awi'),(339,1,'pro_app'),(340,1,'pro_etp'),(341,1,'pro_eup'),(342,1,'pro_ppr'),(343,1,'pro_wce'),(344,1,'pty_dig'),(345,1,'pty_pgz'),(346,1,'pty_pgb'),(347,1,'pty_psb'),(348,1,'pty_psz'),(349,1,'rol_def'),(350,1,'rol_eup'),(351,1,'rol_tup'),(352,1,'rol_adm'),(353,1,'rol_ggr'),(354,1,'rol_gra'),(355,1,'rol_kli'),(356,1,'rol_pra'),(357,1,'rol_spr'),(358,1,'skl_psl'),(359,1,'sta_psg'),(360,1,'sta_kli'),(361,1,'sta_ogo'),(362,1,'sta_psp'),(363,1,'sta_pss'),(364,1,'ust_pus'),(365,1,'uzy_euz'),(366,1,'uzy_ppu'),(367,1,'uzy_tuz'),(368,1,'zam_pwy');
/*!40000 ALTER TABLE `uprawnienia_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ustawienia_systemu`
--

LOCK TABLES `ustawienia_systemu` WRITE;
/*!40000 ALTER TABLE `ustawienia_systemu` DISABLE KEYS */;
INSERT INTO `ustawienia_systemu` VALUES (1,NULL,'js_refr','30000');
/*!40000 ALTER TABLE `ustawienia_systemu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `uzytkownicy`
--

LOCK TABLES `uzytkownicy` WRITE;
/*!40000 ALTER TABLE `uzytkownicy` DISABLE KEYS */;
INSERT INTO `uzytkownicy` VALUES (1,NULL,1,1,'pl_PL','monika.rymsza@pastmo.pl','Monika','Rymsza','f74c7eb07b757543a51e7614f0c9a8c7','monika.rymsza@pastmo.pl','',NULL,'pracownik',NULL,'1','0','2016-09-24 10:00:43'),(2,NULL,1,2,'en_US','admin','Admin','Admiński','f74c7eb07b757543a51e7614f0c9a8c7','admin',NULL,NULL,'pracownik',NULL,'1','0','2016-09-24 10:00:43'),(3,NULL,2,3,'en_US','leszczu@gmail.com','Edward','Leszczyński','f74c7eb07b757543a51e7614f0c9a8c7','leszczu@gmail.com',NULL,NULL,'klient',NULL,'1','0','2016-09-24 10:00:43'),(4,NULL,3,4,'en_US','jerzygrafik','Jerzy','Grafik','f74c7eb07b757543a51e7614f0c9a8c7','jerzygrafik@asdf.dd',NULL,NULL,'pracownik',NULL,'1','0','2016-09-24 10:00:43'),(5,NULL,5,5,'en_US','sprzedawca','Drugi','Sprzedawca','f74c7eb07b757543a51e7614f0c9a8c7','drugi',NULL,NULL,'klient',NULL,'1','0','2016-10-12 06:22:20'),(6,NULL,4,6,'en_US','grafikszef','Szef','Grafików','f74c7eb07b757543a51e7614f0c9a8c7','grafikszef',NULL,NULL,'pracownik',NULL,'1','0','2016-10-12 06:23:27');
/*!40000 ALTER TABLE `uzytkownicy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `uzytkownicy_firmy`
--

LOCK TABLES `uzytkownicy_firmy` WRITE;
/*!40000 ALTER TABLE `uzytkownicy_firmy` DISABLE KEYS */;
INSERT INTO `uzytkownicy_firmy` VALUES (1,NULL,1,1),(2,NULL,3,2);
/*!40000 ALTER TABLE `uzytkownicy_firmy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `uzytkownicy_konta_mailowe`
--

LOCK TABLES `uzytkownicy_konta_mailowe` WRITE;
/*!40000 ALTER TABLE `uzytkownicy_konta_mailowe` DISABLE KEYS */;
INSERT INTO `uzytkownicy_konta_mailowe` VALUES (1,NULL,2,1,'7b39106e434fce318cc17903b1daef0bbcb30432d18d74885064ef8e96ada79cMTU0ODc0NTQ1OTg3NDY1NLb4IpGizjZF303PcyzTBCo='),(2,NULL,3,1,'7e3f1f3f558094660b36ff07c5218deb048d186180fdefad78f1b89933c21d5bMTU0ODc0NTQ1OTg3NDY1NOHrmpmqd1JRM5T8rwybXu0='),(3,1,4,1,NULL);
/*!40000 ALTER TABLE `uzytkownicy_konta_mailowe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `uzytkownicy_powiadomienia`
--

LOCK TABLES `uzytkownicy_powiadomienia` WRITE;
/*!40000 ALTER TABLE `uzytkownicy_powiadomienia` DISABLE KEYS */;
INSERT INTO `uzytkownicy_powiadomienia` VALUES (1,NULL,1,5,'0',NULL),(2,NULL,1,6,'0',NULL),(3,NULL,1,7,'0',NULL),(4,NULL,4,8,'0',NULL),(5,NULL,2,9,'0',NULL),(6,NULL,2,10,'0',NULL),(7,NULL,4,11,'0',NULL),(8,NULL,2,12,'0',NULL),(9,NULL,1,13,'0',NULL);
/*!40000 ALTER TABLE `uzytkownicy_powiadomienia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `uzytkownicy_powiadomienia_kategorie`
--

LOCK TABLES `uzytkownicy_powiadomienia_kategorie` WRITE;
/*!40000 ALTER TABLE `uzytkownicy_powiadomienia_kategorie` DISABLE KEYS */;
/*!40000 ALTER TABLE `uzytkownicy_powiadomienia_kategorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `uzytkownicy_watki_wiadomosci`
--

LOCK TABLES `uzytkownicy_watki_wiadomosci` WRITE;
/*!40000 ALTER TABLE `uzytkownicy_watki_wiadomosci` DISABLE KEYS */;
INSERT INTO `uzytkownicy_watki_wiadomosci` VALUES (1,NULL,2,1),(2,NULL,1,1),(3,NULL,1,3),(4,NULL,5,3),(5,NULL,6,3),(6,NULL,1,3),(7,NULL,1,4);
/*!40000 ALTER TABLE `uzytkownicy_watki_wiadomosci` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `uzytkownicy_wiadomosci`
--

LOCK TABLES `uzytkownicy_wiadomosci` WRITE;
/*!40000 ALTER TABLE `uzytkownicy_wiadomosci` DISABLE KEYS */;
INSERT INTO `uzytkownicy_wiadomosci` VALUES (1,NULL,1,NULL,4,'rozwinięta'),(2,NULL,5,NULL,4,'rozwinięta'),(3,NULL,6,NULL,5,'rozwinięta'),(4,NULL,1,NULL,5,'rozwinięta'),(6,NULL,2,NULL,6,'rozwinięta'),(7,NULL,4,NULL,6,'rozwinięta'),(8,NULL,1,NULL,6,'rozwinięta'),(9,NULL,1,1,7,'rozwinięta'),(10,NULL,2,1,7,'rozwinięta'),(11,NULL,1,1,8,'rozwinięta'),(12,NULL,2,1,8,'rozwinięta');
/*!40000 ALTER TABLE `uzytkownicy_wiadomosci` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `uzytkownicy_zasoby`
--

LOCK TABLES `uzytkownicy_zasoby` WRITE;
/*!40000 ALTER TABLE `uzytkownicy_zasoby` DISABLE KEYS */;
/*!40000 ALTER TABLE `uzytkownicy_zasoby` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `watki_wiadomosci`
--

LOCK TABLES `watki_wiadomosci` WRITE;
/*!40000 ALTER TABLE `watki_wiadomosci` DISABLE KEYS */;
INSERT INTO `watki_wiadomosci` VALUES (1,NULL,'Watek1','prywatny'),(2,NULL,'Rozmowa w projekcie 1','projektowy'),(3,NULL,'','projektowy'),(4,NULL,'Rozmowa typu projektowy ','projektowy');
/*!40000 ALTER TABLE `watki_wiadomosci` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `wiadomosci`
--

LOCK TABLES `wiadomosci` WRITE;
/*!40000 ALTER TABLE `wiadomosci` DISABLE KEYS */;
INSERT INTO `wiadomosci` VALUES (1,NULL,2,1,NULL,'0','Wiadomość testowa','2016-04-14 08:34:47'),(2,NULL,1,2,NULL,'0','hhhhhhhhhhhhhh','2016-09-05 11:01:11'),(3,NULL,1,2,NULL,'0','Kolejna wiadomość','2016-09-05 14:50:49'),(4,NULL,5,3,NULL,'1','Cześć','2016-11-11 11:23:12'),(5,NULL,1,3,NULL,'1','Hm, sprzedawca się odezwał','2016-11-11 11:24:44'),(6,NULL,1,4,NULL,'0','Wiadomość w projekcie Grudzień, od G.Lachowicz, do Admin Admiński i Jerzy Grafik','2017-03-07 08:06:31'),(7,NULL,1,4,1,'1','Wiadomość chatu do Admina Admińskiego z projektu Grudzień','2017-03-07 08:08:59'),(8,NULL,2,4,1,'1','Wiadomość z chatu projektu Grudzień od Admin Admiński do G. Lachowicz','2017-03-07 08:13:45');
/*!40000 ALTER TABLE `wiadomosci` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `wiadomosci_tagi`
--

LOCK TABLES `wiadomosci_tagi` WRITE;
/*!40000 ALTER TABLE `wiadomosci_tagi` DISABLE KEYS */;
/*!40000 ALTER TABLE `wiadomosci_tagi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `wiadomosci_zasoby`
--

LOCK TABLES `wiadomosci_zasoby` WRITE;
/*!40000 ALTER TABLE `wiadomosci_zasoby` DISABLE KEYS */;
/*!40000 ALTER TABLE `wiadomosci_zasoby` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `wiki_artykuly`
--

LOCK TABLES `wiki_artykuly` WRITE;
/*!40000 ALTER TABLE `wiki_artykuly` DISABLE KEYS */;
INSERT INTO `wiki_artykuly` VALUES (1,NULL,1,'pl_PL',NULL,NULL,NULL,NULL,'Artykuł1','Treść artykułu 1'),(2,NULL,1,'en_US',NULL,NULL,NULL,NULL,'Artykuł2','Treść artykułu2');
/*!40000 ALTER TABLE `wiki_artykuly` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `wiki_artykuly_historia`
--

LOCK TABLES `wiki_artykuly_historia` WRITE;
/*!40000 ALTER TABLE `wiki_artykuly_historia` DISABLE KEYS */;
/*!40000 ALTER TABLE `wiki_artykuly_historia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `wystawca_faktur`
--

LOCK TABLES `wystawca_faktur` WRITE;
/*!40000 ALTER TABLE `wystawca_faktur` DISABLE KEYS */;
INSERT INTO `wystawca_faktur` VALUES (1,NULL,NULL,'nazwa','0','0','0','firma'),(2,NULL,NULL,'nazwa','0','0','0','firma'),(3,NULL,NULL,'nazwa','0','0','0','firma'),(4,NULL,NULL,'nazwa','0','0','0','firma'),(5,NULL,NULL,'nazwa','0','0','0','firma'),(6,NULL,NULL,'nazwa','0','0','0','firma'),(7,NULL,NULL,'nazwa','0','0','0','firma'),(8,1,9,'Pastmo, Paweł Kaźmierczak','8992616144','Paweł Kaźmierczak','Wrocław','firma'),(9,2,NULL,'nazwa','0','0','0','firma');
/*!40000 ALTER TABLE `wystawca_faktur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `zasoby`
--

LOCK TABLES `zasoby` WRITE;
/*!40000 ALTER TABLE `zasoby` DISABLE KEYS */;
INSERT INTO `zasoby` VALUES (1,NULL,NULL,'1.jpg','1.jpg',1,'obrazek','','2016-06-28 23:39:28'),(2,NULL,NULL,'9.jpg','9.jpg',1,'obrazek','','2016-06-28 23:39:28'),(3,NULL,NULL,'13.jpg','obrazek13.jpg',1,'obrazek','','2016-06-28 23:39:28'),(4,NULL,NULL,'4_131012A001.jpg','131012A001.jpg',1,'obrazek','','2016-06-28 23:39:28'),(6,NULL,NULL,'5_13.jpg','13.jpg',1,'obrazek','','2016-06-28 23:39:28'),(10,NULL,NULL,'TbaBiCEkwzpAvsTN3dJLbN12IFr8sHQ0xJkatd1JjSqmSnC5.png','TbaBiCEkwzpAvsTN3dJLbN12IFr8sHQ0xJkatd1JjSqmSnC5.p',1,'obrazek','','2016-06-28 23:39:28'),(11,NULL,NULL,'P3odZSr9Fd4OYvnwOt7GJEAoXaFZRR2CLcMYZKybpeiuDCWT.png','P3odZSr9Fd4OYvnwOt7GJEAoXaFZRR2CLcMYZKybpeiuDCWT.p',1,'obrazek','','2016-06-28 23:39:28'),(12,NULL,NULL,'obrazek_zastepczy.jpg','logo.jpg',4313,'obrazek','image/jpeg','2016-09-03 19:18:18'),(13,NULL,NULL,'obrazek_zastepczy.jpg','hszan.jpg',110930,'obrazek','image/jpeg','2016-09-03 19:24:41'),(14,NULL,NULL,'obrazek_zastepczy.jpg','Bez tytułu2.png',35060,'obrazek','image/png','2016-09-03 19:34:16'),(15,NULL,NULL,'obrazek_zastepczy.jpg','Bez tytułu.png',98538,'obrazek','image/png','2016-09-03 19:50:10'),(16,NULL,NULL,'obrazek_zastepczy.jpg','rozmiary produktu.png',5558,'obrazek','image/png','2016-09-08 12:01:32'),(17,NULL,NULL,'obrazek_zastepczy.jpg','logo.jpg',4313,'obrazek','image/jpeg','2016-09-08 12:03:09'),(18,NULL,NULL,'obrazek_zastepczy.jpg','logo.jpg',4313,'obrazek','image/jpeg','2016-09-08 13:01:18'),(19,NULL,NULL,'obrazek_zastepczy.jpg','logo.jpg',4313,'obrazek','image/jpeg','2016-09-08 13:10:10'),(20,NULL,NULL,'obrazek_zastepczy.jpg','logo.jpg',4313,'obrazek','image/jpeg','2016-09-08 13:12:01'),(21,NULL,NULL,'obrazek_zastepczy.jpg','DSC00177 (kopia).JPG',498435,'obrazek','image/jpeg','2016-09-08 13:17:47'),(22,NULL,NULL,'obrazek_zastepczy.jpg','logo.jpg',4313,'obrazek','image/jpeg','2016-09-08 13:28:03'),(23,NULL,NULL,'obrazek_zastepczy.jpg','logo.jpg',4313,'obrazek','image/jpeg','2016-09-08 13:31:09'),(24,NULL,NULL,'obrazek_zastepczy.jpg','kwiatek dla mamy i trochę rekina.jpg',475645,'obrazek','image/jpeg','2016-09-08 13:31:46'),(25,NULL,NULL,'obrazek_zastepczy.jpg','hszan.jpg',110930,'obrazek','image/jpeg','2016-09-08 13:40:39'),(26,NULL,NULL,'obrazek_zastepczy.jpg','logo.jpg',4313,'obrazek','image/jpeg','2016-09-08 13:41:08'),(27,NULL,NULL,'obrazek_zastepczy.jpg','hszan.jpg',110930,'obrazek','image/jpeg','2016-09-08 13:43:05'),(28,NULL,NULL,'obrazek_zastepczy.jpg','obrazek2.jpg',55586,'obrazek','image/jpeg','2016-09-20 11:01:08'),(29,NULL,NULL,'0e9531860fd85029275d06f3b83e5d571.png','inn001.png',507231,'obrazek','image/png','2016-10-11 11:25:41'),(30,NULL,NULL,'0d458dd4f3eb97760d02fc0c867dacae0.png','inn004.png',251350,'obrazek','image/png','2016-10-11 11:25:45'),(31,NULL,NULL,'0ccd5e77611c84fb271ad93dbaef3ecc8.png','inn006.png',397704,'obrazek','image/png','2016-10-11 11:25:47'),(32,NULL,NULL,'05956c80a2a126aca6e4f1f02767ccdd3.png','inn007.png',431245,'obrazek','image/png','2016-10-11 11:25:49'),(33,NULL,NULL,'083df65a4e261d3b956790398416a1dfe.png','inn008.png',377990,'obrazek','image/png','2016-10-11 11:25:50'),(34,NULL,NULL,'0433dd7b5faf67907d525ec103da2e5fa.png','inn009.png',439104,'obrazek','image/png','2016-10-11 11:25:51'),(35,NULL,NULL,'09bb2f9e846fbf891d7c1d709080c45ac.png','inn005_1.png',333130,'obrazek','image/png','2016-10-11 11:25:52'),(36,NULL,NULL,'02d862009bca2e207b601316a4846c2bb.png','inn010.png',389771,'obrazek','image/png','2016-10-11 11:25:53'),(37,NULL,NULL,'0b8571fc0db00a073cdcea3c907a0c103.png','inn011.png',440436,'obrazek','image/png','2016-10-11 11:25:54'),(38,NULL,NULL,'0b1336ef208aa54067f93128e49a5dcfb.png','inn012.png',483865,'obrazek','image/png','2016-10-11 11:25:55'),(39,NULL,NULL,'071d4e895d7c5d455dc31fb5a01c855e3.png','inn013.png',444820,'obrazek','image/png','2016-10-11 11:25:56'),(40,NULL,NULL,'01e85d5a6e6955ab031f51c5c99f8d08b.png','inn014.png',485926,'obrazek','image/png','2016-10-11 11:25:57'),(41,NULL,NULL,'0c21d7c0464a713fad19dce0614999c57.png','inn015.png',423404,'obrazek','image/png','2016-10-11 11:25:58'),(42,NULL,NULL,'0e9b218028efcfd7bba5075876a6c129b.png','inn016.png',509875,'obrazek','image/png','2016-10-11 11:25:59'),(43,NULL,NULL,'097d3e50abf07197f72ef008cfa6c1389.png','inn017.png',597745,'obrazek','image/png','2016-10-11 11:26:01'),(44,NULL,NULL,'00c466c8548dcd749fc9537c56e95e5b7.png','inn018.png',464139,'obrazek','image/png','2016-10-11 11:26:02'),(45,NULL,NULL,'034f1b520a00b6445233f46eb9277da2f.png','inn019.png',624825,'obrazek','image/png','2016-10-11 11:26:03'),(46,NULL,NULL,'06040354cc836d551230192872b34fd14.png','inn020.png',486949,'obrazek','image/png','2016-10-11 11:26:04'),(47,NULL,NULL,'0a8b510f3d32e2bafac8552e24c6b40ae.png','inn021.png',514442,'obrazek','image/png','2016-10-11 11:26:05'),(48,NULL,NULL,'086207a879c4d607637a8e2cd98744b63.png','inn022.png',509388,'obrazek','image/png','2016-10-11 11:26:06'),(49,NULL,NULL,'0cc9c1fcac26932e3533dc342204c8f30.png','inn023.png',392807,'obrazek','image/png','2016-10-11 11:26:07'),(50,NULL,NULL,'0415e05416ed4c38db994056cc95570c5.png','inn024.png',461254,'obrazek','image/png','2016-10-11 11:26:08'),(51,NULL,NULL,'04dcaab29bbc1cb54b002ef95c1c37d9e.png','inn025.png',320466,'obrazek','image/png','2016-10-11 11:26:09'),(52,NULL,NULL,'090c3a05865ba3cb599df909a1b1fb81b.png','inn026.png',439215,'obrazek','image/png','2016-10-11 11:26:10'),(53,NULL,NULL,'001ad7af9145b4e11aa73671b0fd93352.png','inn027.png',523191,'obrazek','image/png','2016-10-11 11:26:11'),(54,NULL,NULL,'091de8b3840aa5dec008aba77cf6de6fa.png','inn028.png',483968,'obrazek','image/png','2016-10-11 11:26:12'),(55,NULL,NULL,'0443bc084c4e93c1da08b240cedc5001a.png','inn029.png',339590,'obrazek','image/png','2016-10-11 11:26:12'),(56,NULL,NULL,'09573149273579e2dd6bb9c30b7671363.png','inn030.png',381991,'obrazek','image/png','2016-10-11 11:26:14'),(57,NULL,NULL,'08547d0b701441fb187cd16617a4b9170.png','inn031.png',636914,'obrazek','image/png','2016-10-11 11:26:15'),(58,NULL,NULL,'03221c142ad03eeeebd13b188ffba3238.png','inn032.png',625611,'obrazek','image/png','2016-10-11 11:26:15'),(59,NULL,NULL,'080c449c793117e154d0f22904fdd1636.png','inn033.png',707827,'obrazek','image/png','2016-10-11 11:26:16'),(60,NULL,NULL,'02ed4f5533e4d4e1668608fb1b96adc4e.png','inn034.png',616112,'obrazek','image/png','2016-10-11 11:26:18'),(61,NULL,NULL,'0edec5857d06fe904fd5284e5ba7e0cd8.png','inn035.png',736699,'obrazek','image/png','2016-10-11 11:26:19'),(62,NULL,NULL,'0ba1c6d4d7ea75d468c9b9ebc415e8c66.png','inn036.png',519960,'obrazek','image/png','2016-10-11 11:26:20'),(63,NULL,NULL,'07352888ec9f910c4bf8f0e57b622c38c.png','inn037.png',660534,'obrazek','image/png','2016-10-11 11:26:21'),(64,NULL,NULL,'0dfc4a91b59053738bc85c6b3f8386b69.png','inn038.png',656804,'obrazek','image/png','2016-10-11 11:26:22'),(65,NULL,NULL,'0d02fc8f7181fe8f27c4fa35005342f41.png','inn039.png',307460,'obrazek','image/png','2016-10-11 11:26:23'),(66,NULL,NULL,'0d309a1b87c9a1996988cd9fb3db26970.png','inn040.png',536194,'obrazek','image/png','2016-10-11 11:26:23'),(67,NULL,NULL,'0fe2f6d065abecfc1cff032e4a4ae3ebb.png','lka001.png',444692,'obrazek','image/png','2016-10-11 11:26:24'),(68,NULL,NULL,'0b88b9293495ff0d54357ec4d06f994ef.png','lka002.png',419564,'obrazek','image/png','2016-10-11 11:26:26'),(69,NULL,NULL,'0cd7d905174d26c477b3f5e1aeab4a3a4.png','lka003.png',492061,'obrazek','image/png','2016-10-11 11:26:27'),(70,NULL,NULL,'0cf8c79875ab7763bb257602e0057d4d6.png','lka004.png',356726,'obrazek','image/png','2016-10-11 11:26:28'),(71,NULL,NULL,'02d2f9cda9c4c8ce9adc7dc560b8441d1.png','lka005.png',411307,'obrazek','image/png','2016-10-11 11:26:28'),(72,NULL,NULL,'07da3d28da154501542fe8ddf325a5b43.png','lka006.png',319886,'obrazek','image/png','2016-10-11 11:26:29'),(73,NULL,NULL,'055c026697c62b3cf79a3419b53e95460.png','lka007.png',460469,'obrazek','image/png','2016-10-11 11:26:31'),(74,NULL,NULL,'0423c9e08df78ad36d7e8d2893876b94a.png','lka008.png',447546,'obrazek','image/png','2016-10-11 11:26:31'),(75,NULL,NULL,'063ed6f70ec3776de2461e247a74ccb8e.png','lka009.png',512620,'obrazek','image/png','2016-10-11 11:26:32'),(76,NULL,NULL,'09e7e9718ce9feb67e664358e84b6bbb8.png','lka010.png',468759,'obrazek','image/png','2016-10-11 11:26:33'),(77,NULL,NULL,'06ab6dc340139805c6f04325a1dcf8076.png','lka011.png',384673,'obrazek','image/png','2016-10-11 11:26:34'),(78,NULL,NULL,'0fb818f483801fcf7d705395e905d6970.png','lka012.png',433248,'obrazek','image/png','2016-10-11 11:26:35'),(79,NULL,NULL,'0ec9dcc11ff21e999869c4a1ea8b9a2a5.png','lka013.png',718661,'obrazek','image/png','2016-10-11 11:26:36'),(80,NULL,NULL,'0ef3581bcc1976d331e74212b37c0f4f6.png','lka014.png',690246,'obrazek','image/png','2016-10-11 11:26:37'),(81,NULL,NULL,'0739e216b6e749d6bd2dfadbaef87ab09.png','lka015.png',435776,'obrazek','image/png','2016-10-11 11:26:38'),(82,NULL,NULL,'0e1637388d43f92c11a6df9b024aaacf5.png','lka016.png',499727,'obrazek','image/png','2016-10-11 11:26:38'),(83,NULL,NULL,'04b7b350bb3134f7f318b97582b79d2d1.png','mts001.png',465037,'obrazek','image/png','2016-10-11 11:26:39'),(84,NULL,NULL,'09ba06d77a1603a153bc269796c6fe27e.png','mts002.png',610070,'obrazek','image/png','2016-10-11 11:26:40'),(85,NULL,NULL,'08f826c49b51ff22cfc0877b109e4bc22.png','mts003.png',452726,'obrazek','image/png','2016-10-11 11:26:41'),(86,NULL,NULL,'04c9151aa8cceb026db546cf690157582.png','mts004.png',442735,'obrazek','image/png','2016-10-11 11:26:42'),(87,NULL,NULL,'075b875761a58a2fa74c4582d1ac5b9f7.png','mts005.png',580927,'obrazek','image/png','2016-10-11 11:26:44'),(88,NULL,NULL,'0d76d97e6171291152d2b01272a073849.png','mts006.png',450269,'obrazek','image/png','2016-10-11 11:26:45'),(89,NULL,NULL,'0fa6294985901d234fdca316b6af7b47d.png','mts007.png',459441,'obrazek','image/png','2016-10-11 11:26:46'),(90,NULL,NULL,'0a90cefea6ff70c6559824c181bb598cc.png','pns001_1.png',407740,'obrazek','image/png','2016-10-11 11:26:48'),(91,NULL,NULL,'015a1d8ca950e917864dbae8c77800e5b.png','mts008.png',499123,'obrazek','image/png','2016-10-11 11:26:49'),(92,NULL,NULL,'02e15387f752f8e5fd6930b5947dffeb4.png','pns002.png',449609,'obrazek','image/png','2016-10-11 11:26:50'),(93,NULL,NULL,'0a226a8a7c1d4d56ba18eb8fb33f2ad76.png','mts009.png',764493,'obrazek','image/png','2016-10-11 11:26:50'),(94,NULL,NULL,'0e5890ae4b1699196cad44f30562f74d1.png','pns003.png',549954,'obrazek','image/png','2016-10-11 11:26:52'),(95,NULL,NULL,'07c9a9a721661e9ace5f720c5c0805ebc.png','mts010.png',608761,'obrazek','image/png','2016-10-11 11:26:53'),(96,NULL,NULL,'0fbd23b7e8f1851eda27847676abd6c2a.png','mts011.png',726050,'obrazek','image/png','2016-10-11 11:26:54'),(97,NULL,NULL,'0c592cd0c46458736c6f4fa702f13e2e6.png','mts012.png',491759,'obrazek','image/png','2016-10-11 11:26:55'),(98,NULL,NULL,'0b3baa9ad949af8e5d11c7e608712b34e.png','pns004.png',379807,'obrazek','image/png','2016-10-11 11:26:55'),(99,NULL,NULL,'0ff2cbda88dc4b09909e3fe339021e88a.png','pns005.png',437689,'obrazek','image/png','2016-10-11 11:26:56'),(100,NULL,NULL,'96a9cca64a2c00825f63e7bf522372ac31.png','skan15568585378.png',23788,'obrazek','image/png','2016-10-14 07:17:06'),(101,NULL,NULL,'974a2b3ea34bf01e4767626d1c902b076b.pdf','etykieta15568585378.pdf',77468,'plik','application/pdf','2016-10-14 07:17:19'),(102,NULL,NULL,'98d78367430a9eaf4632c36413f0a0116c.pdf','pnp2016-10-17.pdf',70666,'plik','application/pdf','2016-10-14 07:18:22'),(103,NULL,NULL,'99ca55a1a812a1fe842c4b5d02c457b006.obj','komponent.obj',6246,'plik','application/octet-st','2016-10-21 22:18:54'),(104,NULL,NULL,'10074ff5785051144df07cb3b6ed4742b42.obj','podstawka.obj',3623,'plik','application/octet-st','2016-10-21 22:21:52'),(105,NULL,NULL,'1017e51c779c0466f6f62e9417b2e5b123b.obj','produkt.obj',6201,'plik','application/octet-st','2016-10-21 22:23:08'),(106,NULL,NULL,'102bbc25ea264fd0330585a97b3f923c239.obj','produkt2.obj',6997,'plik','application/octet-st','2016-10-21 22:31:02'),(107,NULL,NULL,'10373d07dde23868d99445b8727bb09e1cd.obj','podstawka2.obj',4463,'plik','application/octet-st','2016-10-21 22:33:29'),(108,NULL,NULL,'104b8d52a74a42f9a1f5abcdd912163009d.txt','<<564F0042.3020001@tidio.net>>.txt',2672,'plik','message/rfc822','2017-03-17 05:16:12'),(109,NULL,NULL,'1058322cd81ecf9fcfd0717fb7491643138.txt','<<148559360-fa2baed1f62224fda6d9403029eaba89@pmq1v.m5r2.onet>>.txt',1409,'plik','message/rfc822','2017-03-17 05:16:12'),(110,NULL,NULL,'106b281c8739c1380a28c22555bc8965b4a.txt','Fri, 12 Feb 2016 03:55:45 +0100home.pl <info@mailing.home.pl>e27be3d90cc47c380a7cd629ce79120b.txt',42174,'plik','message/rfc822','2017-03-17 05:16:12'),(111,NULL,NULL,'1076b31fb99d6968831455c9c99486946f8.txt','<<c10ee3b504401c4f1e8aa4450e5821a3@46.4.190.214>>.txt',20474,'plik','message/rfc822','2017-03-17 05:16:12'),(112,NULL,NULL,'108fde188985c556faf227b18b4a3edbf68.txt','<<861dedbbb33eb1f90777691a74afa0bc@46.4.190.214>>.txt',20474,'plik','message/rfc822','2017-03-17 05:16:12'),(113,NULL,NULL,'109fc06d00ba49560b16a51d89a68a80fdc.txt','<<d2ba9f6015202f424624532e810fb92b@pastmo.pl>>.txt',1012,'plik','message/rfc822','2017-03-17 05:16:13'),(114,NULL,NULL,'1101dc14ac0c403f33dcb3d457cd2fb06aa.txt','<<774958940.59564073@emlgrid.com>>.txt',57467,'plik','message/rfc822','2017-03-17 05:16:13'),(115,NULL,NULL,'11120a1261db1d36d8212dc55b25f1ae2f3.txt','<<01da01d1e101$656611e0$303235a0$@grupaglasso.pl>>.txt',1697,'plik','message/rfc822','2017-03-17 05:16:13'),(116,NULL,NULL,'112094d5d8964f11c22e89519d1a6bd9498.txt','<<368878a62af1126f7944c62549120955@pastmo.pl>>.txt',25561,'plik','message/rfc822','2017-03-17 05:16:13'),(117,NULL,NULL,'1133f2813e7f22b0896ec1cd68b0a2fb8b5.jpeg','324485fe.jpeg',12295,'obrazek','image/jpeg','2017-03-17 05:16:13'),(118,NULL,NULL,'114f121d135f39f03e48da5fe5e8ced5b0a.jpg','logo.jpg',4313,'obrazek','image/jpeg','2017-03-17 05:16:13'),(119,NULL,NULL,'1157b141d9e00c553a599175faa170fee3c.txt','<<201608310755.u7V7tvQO024174@glasso.nazwa.pl>>.txt',1222,'plik','message/rfc822','2017-03-17 05:16:13'),(121,1,NULL,'1/11627d4ed0c20d10d407f34c631549b249e.zo.o..pdf','FVS_6_2017 Beskid Sp. z o.o..pdf',1587555,'plik','application/pdf','2017-05-01 04:24:23'),(122,1,NULL,'1/117d44a25377399714e6e6efcf89295ae48.png','min.png',155,'obrazek','image/png','2017-05-01 07:51:18'),(123,1,NULL,'1/118d44a25377399714e6e6efcf89295ae48.png','min.png',155,'obrazek','image/png','2017-05-01 11:39:23'),(124,1,NULL,'1/119d44a25377399714e6e6efcf89295ae48.png','min.png',155,'obrazek','image/png','2017-05-01 12:33:38'),(125,1,NULL,'1/1207baffa827ffed52161ca1d7b4d24299c.jpg','DSC_0350 (1).jpg',709639,'obrazek','image/jpeg','2017-05-01 12:58:23'),(126,1,NULL,'1/1215ee10ea808ef5bd66adabd1dbe5c865f.jpg','DSC_0350.jpg',709639,'obrazek','image/jpeg','2017-05-01 12:58:24'),(127,1,NULL,'1/122bc0a38bdd29a3fcb4edbf963afda4a12.jpg','DSC_0659.jpg',875166,'obrazek','image/jpeg','2017-05-01 12:58:26'),(128,1,NULL,'1/1231f944133b3c434c243e1801181ce1524.jpg','DSC_1136.jpg',520578,'obrazek','image/jpeg','2017-05-01 12:58:27'),(129,1,NULL,'1/12465fab719f757b159f9414216bae3afa7.png','asdfg.png',1733,'obrazek','image/png','2017-05-01 14:34:06'),(130,1,NULL,'1/125edc2983e14bbfcf124fe5f7768efc070.txt','<<1432254030.577835.1491737068367@netart.pl>>.txt',65855,'plik','message/rfc822','2017-05-03 15:28:55'),(131,1,NULL,'1/12603fdad155b7548884584c7c39b0c5cd2.','inline',13441,'obrazek','image/jpeg','2017-05-03 15:28:55'),(132,1,NULL,'1/12703fdad155b7548884584c7c39b0c5cd2.','inline',57,'obrazek','image/gif','2017-05-03 15:28:55'),(133,1,NULL,'1/12803fdad155b7548884584c7c39b0c5cd2.','inline',2677,'obrazek','image/jpeg','2017-05-03 15:28:55'),(134,1,NULL,'1/12903fdad155b7548884584c7c39b0c5cd2.','inline',496,'obrazek','image/jpeg','2017-05-03 15:28:55'),(135,1,NULL,'1/13003fdad155b7548884584c7c39b0c5cd2.','inline',4738,'obrazek','image/jpeg','2017-05-03 15:28:55'),(136,1,NULL,'1/13103fdad155b7548884584c7c39b0c5cd2.','inline',5201,'obrazek','image/jpeg','2017-05-03 15:28:55'),(137,1,NULL,'1/13203fdad155b7548884584c7c39b0c5cd2.','inline',3473,'obrazek','image/jpeg','2017-05-03 15:28:55'),(138,1,NULL,'1/13303fdad155b7548884584c7c39b0c5cd2.','inline',3905,'obrazek','image/jpeg','2017-05-03 15:28:55'),(139,1,NULL,'1/13403fdad155b7548884584c7c39b0c5cd2.','inline',3635,'obrazek','image/jpeg','2017-05-03 15:28:55'),(140,1,NULL,'1/1355d3a37b9cc92167e325eb8fd8588f3a2.txt','<<72138818.780386.1493676784197@poczta.nazwa.pl>>.txt',2230,'plik','text/html','2017-05-03 15:28:55');
/*!40000 ALTER TABLE `zasoby` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `zasoby_kategorie`
--

LOCK TABLES `zasoby_kategorie` WRITE;
/*!40000 ALTER TABLE `zasoby_kategorie` DISABLE KEYS */;
/*!40000 ALTER TABLE `zasoby_kategorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `zlecenia`
--

LOCK TABLES `zlecenia` WRITE;
/*!40000 ALTER TABLE `zlecenia` DISABLE KEYS */;
INSERT INTO `zlecenia` VALUES (26,1,7,5,'Wymiana żarówki','Opis','Nowe','0','2017-05-07 22:00:00','2017-05-23 22:00:00'),(27,1,7,5,'Konfiguracja serwera','Opis','Nowe','0','2017-05-09 22:00:00','2017-05-11 22:00:00'),(28,1,NULL,5,'Analiza wysyłania wiadomości',NULL,'Nowe','0','2017-05-02 22:00:00','2017-05-02 22:00:00'),(29,1,3,5,'Nowe zlecenie','Opis','Nowe','0','2017-05-02 22:00:00','2017-05-02 22:00:00');
/*!40000 ALTER TABLE `zlecenia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `zlecenia_kategorie`
--

LOCK TABLES `zlecenia_kategorie` WRITE;
/*!40000 ALTER TABLE `zlecenia_kategorie` DISABLE KEYS */;
INSERT INTO `zlecenia_kategorie` VALUES (1,NULL,'Strony www'),(2,NULL,'Administracja'),(3,NULL,'Marketing'),(4,NULL,'Hosting'),(5,NULL,'Namioty'),(6,NULL,'DJ');
/*!40000 ALTER TABLE `zlecenia_kategorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `zlecenia_zasoby`
--

LOCK TABLES `zlecenia_zasoby` WRITE;
/*!40000 ALTER TABLE `zlecenia_zasoby` DISABLE KEYS */;
INSERT INTO `zlecenia_zasoby` VALUES (8,1,27,124),(9,1,28,125),(10,1,28,126),(11,1,28,127),(12,1,28,128);
/*!40000 ALTER TABLE `zlecenia_zasoby` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-17  6:19:30
