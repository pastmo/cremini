-- MySQL dump 10.16  Distrib 10.1.13-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: pastmo_panel
-- ------------------------------------------------------
-- Server version	10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `adresy`
--

DROP TABLE IF EXISTS `adresy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adresy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `nazwa` text COLLATE utf16_polish_ci,
  `osoba_kontaktowa` text COLLATE utf16_polish_ci,
  `ulica` text COLLATE utf16_polish_ci,
  `nr_domu` text COLLATE utf16_polish_ci,
  `nr_lokalu` text COLLATE utf16_polish_ci,
  `kod` text COLLATE utf16_polish_ci,
  `miasto` text COLLATE utf16_polish_ci,
  `kraj` text COLLATE utf16_polish_ci,
  `telefon` text COLLATE utf16_polish_ci,
  `email` text COLLATE utf16_polish_ci,
  `akceptacja` char(1) COLLATE utf16_polish_ci DEFAULT '0',
  `typ` enum('książka adresowa','pomocniczy','adres nadawcy') COLLATE utf16_polish_ci DEFAULT 'książka adresowa',
  PRIMARY KEY (`id`),
  KEY `FK_adresy_konta` (`konto_id`),
  CONSTRAINT `FK_adresy_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dane_do_faktury`
--

DROP TABLE IF EXISTS `dane_do_faktury`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dane_do_faktury` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `adres_id` int(11) DEFAULT NULL,
  `nip` varchar(15) COLLATE utf8_polish_ci DEFAULT NULL,
  `termin_platnosci` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_dane_do_faktury_adresy` (`adres_id`),
  KEY `FK_dane_do_faktury_konta` (`konto_id`),
  CONSTRAINT `FK_dane_do_faktury_adresy` FOREIGN KEY (`adres_id`) REFERENCES `adresy` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_dane_do_faktury_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emaile`
--

DROP TABLE IF EXISTS `emaile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emaile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `konto_mailowe_id` int(11) DEFAULT NULL,
  `wiadomosc_id` int(11) DEFAULT NULL,
  `email_folder_id` int(11) DEFAULT NULL,
  `zrodlo_id` int(11) DEFAULT NULL,
  `licznik` int(11) DEFAULT NULL,
  `data_naglowka` varchar(40) COLLATE utf8_polish_ci DEFAULT NULL,
  `nadawca` text COLLATE utf8_polish_ci,
  `tytul` text COLLATE utf8_polish_ci,
  `tresc` text COLLATE utf8_polish_ci,
  `message_id` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL,
  `skrzynka` enum('odbiorcza','wyslane','spam','kosz','usuniete') COLLATE utf8_polish_ci DEFAULT 'odbiorcza',
  `status` enum('nieprzeczytana','przeczytana','usunieta','ukryta') COLLATE utf8_polish_ci DEFAULT 'nieprzeczytana',
  PRIMARY KEY (`id`),
  KEY `FK_maile_konta_mailowe` (`konto_mailowe_id`),
  KEY `FK_maile_wiadomosci` (`wiadomosc_id`),
  KEY `FK_emaile_emaile_foldery` (`email_folder_id`),
  KEY `FK_emaile_zasoby` (`zrodlo_id`),
  KEY `FK_emaile_konta` (`konto_id`),
  CONSTRAINT `FK_emaile_emaile_foldery` FOREIGN KEY (`email_folder_id`) REFERENCES `emaile_foldery` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_emaile_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_emaile_zasoby` FOREIGN KEY (`zrodlo_id`) REFERENCES `zasoby` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_maile_konta_mailowe` FOREIGN KEY (`konto_mailowe_id`) REFERENCES `konta_mailowe` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_maile_wiadomosci` FOREIGN KEY (`wiadomosc_id`) REFERENCES `wiadomosci` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emaile_foldery`
--

DROP TABLE IF EXISTS `emaile_foldery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emaile_foldery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `konto_mailowe_id` int(11) DEFAULT NULL,
  `nazwa` varchar(20) COLLATE utf8_polish_ci DEFAULT NULL,
  `filtr` text COLLATE utf8_polish_ci,
  PRIMARY KEY (`id`),
  KEY `FK_emaile_foldery_konta_mailowe` (`konto_mailowe_id`),
  KEY `FK_emaile_foldery_konta` (`konto_id`),
  CONSTRAINT `FK_emaile_foldery_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_emaile_foldery_konta_mailowe` FOREIGN KEY (`konto_mailowe_id`) REFERENCES `konta_mailowe` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emaile_zasoby`
--

DROP TABLE IF EXISTS `emaile_zasoby`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emaile_zasoby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `email_id` int(11) DEFAULT NULL,
  `zasob_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_emaile_zasoby_emaile` (`email_id`),
  KEY `FK_emaile_zasoby_zasoby` (`zasob_id`),
  KEY `FK_emaile_zasoby_konta` (`konto_id`),
  CONSTRAINT `FK_emaile_zasoby_emaile` FOREIGN KEY (`email_id`) REFERENCES `emaile` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_emaile_zasoby_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_emaile_zasoby_zasoby` FOREIGN KEY (`zasob_id`) REFERENCES `zasoby` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `faktury`
--

DROP TABLE IF EXISTS `faktury`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faktury` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `klient_id` int(11) DEFAULT NULL,
  `wystawca_id` int(11) DEFAULT NULL,
  `nr_faktury` varchar(30) COLLATE utf8_polish_ci DEFAULT NULL,
  `rok` year(4) DEFAULT NULL,
  `do_zaplaty` decimal(10,2) DEFAULT NULL,
  `kwota_netto` decimal(10,2) DEFAULT NULL,
  `zaplacono` decimal(10,2) DEFAULT NULL,
  `vat` decimal(4,2) DEFAULT NULL,
  `data_wystawienia` date DEFAULT NULL,
  `data_wykonania` date DEFAULT NULL,
  `termin_platnosci` date DEFAULT NULL,
  `miejsce_wystawienia` varchar(30) COLLATE utf8_polish_ci DEFAULT NULL,
  `slownie` varchar(300) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_faktury_klienci` (`klient_id`),
  KEY `FK_faktury_wystawca_faktur` (`wystawca_id`),
  KEY `FK_faktury_konta` (`konto_id`),
  CONSTRAINT `FK_faktury_klienci` FOREIGN KEY (`klient_id`) REFERENCES `klienci` (`id`),
  CONSTRAINT `FK_faktury_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_faktury_wystawca_faktur` FOREIGN KEY (`wystawca_id`) REFERENCES `wystawca_faktur` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `faktury_zasoby`
--

DROP TABLE IF EXISTS `faktury_zasoby`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faktury_zasoby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `faktura_id` int(11) DEFAULT NULL,
  `zasob_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_faktury_zasoby_faktury` (`faktura_id`),
  KEY `FK_faktury_zasoby_zasoby` (`zasob_id`),
  KEY `FK_faktury_zasoby_konta` (`konto_id`),
  CONSTRAINT `FK_faktury_zasoby_faktury` FOREIGN KEY (`faktura_id`) REFERENCES `faktury` (`id`),
  CONSTRAINT `FK_faktury_zasoby_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_faktury_zasoby_zasoby` FOREIGN KEY (`zasob_id`) REFERENCES `zasoby` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `firmy`
--

DROP TABLE IF EXISTS `firmy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `firmy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `img_id` int(11) DEFAULT NULL,
  `adres_wysylki_id` int(11) DEFAULT NULL,
  `adres_faktury_id` int(11) DEFAULT NULL,
  `kategoria_lojalnosciowa_id` int(11) DEFAULT NULL,
  `domyslny_sprzedawca_id` int(11) DEFAULT NULL,
  `nazwa` text COLLATE utf8_polish_ci,
  `data_dodania` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nip` varchar(15) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_firmy_zasoby` (`img_id`),
  KEY `FK_firmy_adresy` (`adres_wysylki_id`),
  KEY `FK_firmy_adresy_2` (`adres_faktury_id`),
  KEY `FK_firmy_kategorie_lojalnosciowe` (`kategoria_lojalnosciowa_id`),
  KEY `FK_firmy_uzytkownicy` (`domyslny_sprzedawca_id`),
  KEY `FK_firmy_konta` (`konto_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jezyki`
--

DROP TABLE IF EXISTS `jezyki`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jezyki` (
  `kod` char(5) COLLATE utf8_polish_ci NOT NULL,
  `skrot` char(5) COLLATE utf8_polish_ci NOT NULL,
  `nazwa` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`kod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `klienci`
--

DROP TABLE IF EXISTS `klienci`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `klienci` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `adres_id` int(11) DEFAULT NULL,
  `adres_faktury_id` int(11) DEFAULT NULL,
  `nazwa` text COLLATE utf16_polish_ci,
  `nazwa_na_fakturze` text COLLATE utf16_polish_ci,
  `nip` tinytext COLLATE utf16_polish_ci,
  `status` enum('Nowy','Stały','Zrealizowany') COLLATE utf16_polish_ci DEFAULT 'Nowy',
  `www` text COLLATE utf16_polish_ci,
  `notatka` text COLLATE utf16_polish_ci,
  PRIMARY KEY (`id`),
  KEY `FK_klienci_uzytkownicy` (`uzytkownik_id`),
  KEY `FK_klienci_adresy` (`adres_id`),
  KEY `FK_klienci_adresy_2` (`adres_faktury_id`),
  KEY `FK_klienci_konta` (`konto_id`),
  CONSTRAINT `FK_klienci_adresy` FOREIGN KEY (`adres_id`) REFERENCES `adresy` (`id`),
  CONSTRAINT `FK_klienci_adresy_2` FOREIGN KEY (`adres_faktury_id`) REFERENCES `adresy` (`id`),
  CONSTRAINT `FK_klienci_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_klienci_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `klienci_dodatkowe_pola`
--

DROP TABLE IF EXISTS `klienci_dodatkowe_pola`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `klienci_dodatkowe_pola` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `klient_id` int(11) DEFAULT NULL,
  `nazwa` text COLLATE utf16_polish_ci,
  `wartosc` text COLLATE utf16_polish_ci,
  PRIMARY KEY (`id`),
  KEY `FK_klienci_dodatkowe_pola_klienci` (`klient_id`),
  KEY `FK_klienci_dodatkowe_pola_konta` (`konto_id`),
  CONSTRAINT `FK_klienci_dodatkowe_pola_klienci` FOREIGN KEY (`klient_id`) REFERENCES `klienci` (`id`),
  CONSTRAINT `FK_klienci_dodatkowe_pola_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `klienci_notatki`
--

DROP TABLE IF EXISTS `klienci_notatki`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `klienci_notatki` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `klient_id` int(11) DEFAULT NULL,
  `tekst` text COLLATE utf16_polish_ci NOT NULL,
  `data_dodania` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_notatki_klienci` (`klient_id`),
  KEY `FK_klienci_notatki_konta` (`konto_id`),
  CONSTRAINT `FK_klienci_notatki_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_notatki_klienci` FOREIGN KEY (`klient_id`) REFERENCES `klienci` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `konta`
--

DROP TABLE IF EXISTS `konta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `konta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rejestrujacy_id` int(11) DEFAULT NULL,
  `data_dodania` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_konto_uzytkownicy` (`rejestrujacy_id`),
  CONSTRAINT `FK_konto_uzytkownicy` FOREIGN KEY (`rejestrujacy_id`) REFERENCES `uzytkownicy` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `konta_mailowe`
--

DROP TABLE IF EXISTS `konta_mailowe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `konta_mailowe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `host` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL,
  `host_wychodzacy` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL,
  `user` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL,
  `password` text COLLATE utf8_polish_ci,
  `port_imap` varchar(10) COLLATE utf8_polish_ci DEFAULT NULL,
  `port_stmp` varchar(10) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_konta_mailowe_konta` (`konto_id`),
  CONSTRAINT `FK_konta_mailowe_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `logi`
--

DROP TABLE IF EXISTS `logi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `kod_kategorii` varchar(10) COLLATE utf8_polish_ci DEFAULT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `tresc` varchar(500) COLLATE utf8_polish_ci DEFAULT NULL,
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_logi_uzytkownicy` (`uzytkownik_id`),
  KEY `FK_logi_logi_kategorie` (`kod_kategorii`),
  KEY `FK_logi_konta` (`konto_id`),
  CONSTRAINT `FK_logi_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_logi_logi_kategorie` FOREIGN KEY (`kod_kategorii`) REFERENCES `logi_kategorie` (`kod`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_logi_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `logi_kategorie`
--

DROP TABLE IF EXISTS `logi_kategorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logi_kategorie` (
  `kod` varchar(10) COLLATE utf8_polish_ci NOT NULL,
  `nazwa` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`kod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pomoc`
--

DROP TABLE IF EXISTS `pomoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pomoc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jezyk_id` int(11) DEFAULT NULL,
  `kod` varchar(7) COLLATE utf16_polish_ci NOT NULL,
  `opis` varchar(20) COLLATE utf16_polish_ci NOT NULL,
  `tresc` varchar(1000) COLLATE utf16_polish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `powiadomienia`
--

DROP TABLE IF EXISTS `powiadomienia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `powiadomienia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `kategoria_kod` varchar(7) COLLATE utf8_polish_ci DEFAULT NULL,
  `tresc` varchar(500) COLLATE utf8_polish_ci DEFAULT NULL,
  `url_id` varchar(500) COLLATE utf8_polish_ci DEFAULT NULL,
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_przypomnienia` timestamp NULL DEFAULT NULL,
  `cykliczne` enum('-','roczne') COLLATE utf8_polish_ci NOT NULL DEFAULT '-',
  `typ` enum('powiadomienia','przypomnienia') COLLATE utf8_polish_ci DEFAULT 'powiadomienia',
  PRIMARY KEY (`id`),
  KEY `FK_powiadomienia_powiadomienia_kategorie_2` (`kategoria_kod`),
  KEY `FK_powiadomienia_konta` (`konto_id`),
  CONSTRAINT `FK_powiadomienia_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_powiadomienia_powiadomienia_kategorie_2` FOREIGN KEY (`kategoria_kod`) REFERENCES `powiadomienia_kategorie` (`kod`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `powiadomienia_kategorie`
--

DROP TABLE IF EXISTS `powiadomienia_kategorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `powiadomienia_kategorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `kod` varchar(7) COLLATE utf8_polish_ci DEFAULT NULL,
  `nazwa` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  `url_modul` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  `url_akcja` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kod` (`kod`),
  KEY `FK_powiadomienia_kategorie_konta` (`konto_id`),
  CONSTRAINT `FK_powiadomienia_kategorie_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(15) COLLATE utf16_polish_ci NOT NULL,
  `kod` varchar(10) COLLATE utf16_polish_ci NOT NULL,
  `typ` enum('pracownik','klient') COLLATE utf16_polish_ci DEFAULT 'klient',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tagi`
--

DROP TABLE IF EXISTS `tagi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tagi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `nazwa` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `kolor` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tagi_uzytkownicy` (`uzytkownik_id`),
  KEY `FK_tagi_konta` (`konto_id`),
  CONSTRAINT `FK_tagi_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_tagi_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `udostepnione_zasoby`
--

DROP TABLE IF EXISTS `udostepnione_zasoby`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `udostepnione_zasoby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `wlasciciel_id` int(11) DEFAULT NULL,
  `udostepniony_zasob_folder_id` int(11) DEFAULT NULL,
  `zasob_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_role_zasoby_zasoby2` (`zasob_id`),
  KEY `FK_udostepnione_zasoby_uzytkownicy2` (`wlasciciel_id`),
  KEY `FK_udostepnione_zasoby_udostepnione_zasoby_foldery2` (`udostepniony_zasob_folder_id`),
  KEY `FK_udostepnione_zasoby_konta` (`konto_id`),
  CONSTRAINT `FK_role_zasoby_zasoby2` FOREIGN KEY (`zasob_id`) REFERENCES `zasoby` (`id`),
  CONSTRAINT `FK_udostepnione_zasoby_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_udostepnione_zasoby_udostepnione_zasoby_foldery2` FOREIGN KEY (`udostepniony_zasob_folder_id`) REFERENCES `udostepnione_zasoby_foldery` (`id`),
  CONSTRAINT `FK_udostepnione_zasoby_uzytkownicy2` FOREIGN KEY (`wlasciciel_id`) REFERENCES `uzytkownicy` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `udostepnione_zasoby_foldery`
--

DROP TABLE IF EXISTS `udostepnione_zasoby_foldery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `udostepnione_zasoby_foldery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `folder_nadrzedny_id` int(11) DEFAULT NULL,
  `nazwa` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_udostepnione_zasoby_foldery_udostepnione_zasoby_foldery` (`folder_nadrzedny_id`),
  KEY `FK_udostepnione_zasoby_foldery_uzytkownicy` (`uzytkownik_id`),
  KEY `FK_udostepnione_zasoby_foldery_konta` (`konto_id`),
  CONSTRAINT `FK_udostepnione_zasoby_foldery_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_udostepnione_zasoby_foldery_udostepnione_zasoby_foldery` FOREIGN KEY (`folder_nadrzedny_id`) REFERENCES `udostepnione_zasoby_foldery` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_udostepnione_zasoby_foldery_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `udostepnione_zasoby_uzytkownicy`
--

DROP TABLE IF EXISTS `udostepnione_zasoby_uzytkownicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `udostepnione_zasoby_uzytkownicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `rola_id` int(11) DEFAULT NULL,
  `udostepniony_zasob_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_udostepnione_zasoby_uzytkownicy_uzytkownicy` (`uzytkownik_id`),
  KEY `FK_udostepnione_zasoby_uzytkownicy_role` (`rola_id`),
  KEY `FK_udostepnione_zasoby_uzytkownicy_udostepnione_zasoby` (`udostepniony_zasob_id`),
  KEY `FK_udostepnione_zasoby_uzytkownicy_konta` (`konto_id`),
  CONSTRAINT `FK_udostepnione_zasoby_uzytkownicy_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_udostepnione_zasoby_uzytkownicy_role` FOREIGN KEY (`rola_id`) REFERENCES `role` (`id`),
  CONSTRAINT `FK_udostepnione_zasoby_uzytkownicy_udostepnione_zasoby` FOREIGN KEY (`udostepniony_zasob_id`) REFERENCES `udostepnione_zasoby` (`id`),
  CONSTRAINT `FK_udostepnione_zasoby_uzytkownicy_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uprawnienia`
--

DROP TABLE IF EXISTS `uprawnienia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uprawnienia` (
  `kod` varchar(7) COLLATE utf16_polish_ci NOT NULL,
  `opis` varchar(50) COLLATE utf16_polish_ci DEFAULT NULL,
  PRIMARY KEY (`kod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uprawnienia_kategorie`
--

DROP TABLE IF EXISTS `uprawnienia_kategorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uprawnienia_kategorie` (
  `kod` varchar(3) COLLATE utf8_polish_ci NOT NULL,
  `opis` varchar(20) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`kod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uprawnienia_role`
--

DROP TABLE IF EXISTS `uprawnienia_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uprawnienia_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rola_id` int(11) NOT NULL,
  `uprawnienie_kod` varchar(7) COLLATE utf16_polish_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_uprawnienia_role_role` (`rola_id`),
  KEY `FK_uprawnienia_role_uprawnienia` (`uprawnienie_kod`),
  CONSTRAINT `FK_uprawnienia_role_role` FOREIGN KEY (`rola_id`) REFERENCES `role` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_uprawnienia_role_uprawnienia` FOREIGN KEY (`uprawnienie_kod`) REFERENCES `uprawnienia` (`kod`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ustawienia_systemu`
--

DROP TABLE IF EXISTS `ustawienia_systemu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ustawienia_systemu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `kod` varchar(7) COLLATE utf8_polish_ci NOT NULL,
  `wartosc` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ustawienia_systemu_uzytkownicy` (`uzytkownik_id`),
  CONSTRAINT `FK_ustawienia_systemu_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uzytkownicy`
--

DROP TABLE IF EXISTS `uzytkownicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uzytkownicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `avatar_id` int(11) DEFAULT NULL,
  `rola_id` int(11) DEFAULT NULL,
  `konto_id` int(11) DEFAULT NULL,
  `jezyk_kod` char(5) COLLATE utf8_polish_ci DEFAULT 'en_US',
  `login` text COLLATE utf8_polish_ci,
  `imie` text COLLATE utf8_polish_ci,
  `nazwisko` text COLLATE utf8_polish_ci,
  `haslo` text COLLATE utf8_polish_ci,
  `mail` text COLLATE utf8_polish_ci,
  `telefon` text COLLATE utf8_polish_ci,
  `telefon2` text COLLATE utf8_polish_ci,
  `typ` enum('pracownik','klient') COLLATE utf8_polish_ci DEFAULT 'klient',
  `deklarowana_firma` char(70) COLLATE utf8_polish_ci DEFAULT NULL,
  `czy_aktywny` char(1) COLLATE utf8_polish_ci DEFAULT '0',
  `usuniety` char(1) COLLATE utf8_polish_ci DEFAULT '0',
  `data_dodania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_uzytkownicy_zasoby` (`avatar_id`),
  KEY `FK_uzytkownicy_role` (`rola_id`),
  KEY `FK_uzytkownicy_jezyki` (`jezyk_kod`),
  KEY `FK_uzytkownicy_konto` (`konto_id`),
  CONSTRAINT `FK_uzytkownicy_jezyki` FOREIGN KEY (`jezyk_kod`) REFERENCES `jezyki` (`kod`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_uzytkownicy_konto` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`),
  CONSTRAINT `FK_uzytkownicy_role` FOREIGN KEY (`rola_id`) REFERENCES `role` (`id`),
  CONSTRAINT `FK_uzytkownicy_zasoby` FOREIGN KEY (`avatar_id`) REFERENCES `zasoby` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uzytkownicy_firmy`
--

DROP TABLE IF EXISTS `uzytkownicy_firmy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uzytkownicy_firmy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `firma_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_uzytkownicy_firmy_uzytkownicy` (`uzytkownik_id`),
  KEY `FK_uzytkownicy_firmy_firmy` (`firma_id`),
  KEY `FK_uzytkownicy_firmy_konta` (`konto_id`),
  CONSTRAINT `FK_uzytkownicy_firmy_firmy` FOREIGN KEY (`firma_id`) REFERENCES `firmy` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_uzytkownicy_firmy_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_uzytkownicy_firmy_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uzytkownicy_konta_mailowe`
--

DROP TABLE IF EXISTS `uzytkownicy_konta_mailowe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uzytkownicy_konta_mailowe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `konto_mailowe_id` int(11) DEFAULT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `password` text COLLATE utf8_polish_ci,
  PRIMARY KEY (`id`),
  KEY `FK_uzytkownicy_konta_mailowe_konta_mailowe` (`konto_mailowe_id`),
  KEY `FK_uzytkownicy_konta_mailowe_uzytkownicy` (`uzytkownik_id`),
  KEY `FK_uzytkownicy_konta_mailowe_konta` (`konto_id`),
  CONSTRAINT `FK_uzytkownicy_konta_mailowe_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_uzytkownicy_konta_mailowe_konta_mailowe` FOREIGN KEY (`konto_mailowe_id`) REFERENCES `konta_mailowe` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_uzytkownicy_konta_mailowe_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uzytkownicy_powiadomienia`
--

DROP TABLE IF EXISTS `uzytkownicy_powiadomienia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uzytkownicy_powiadomienia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `powiadomienie_id` int(11) DEFAULT NULL,
  `przeczytane` char(1) COLLATE utf8_polish_ci DEFAULT '0',
  `kolor` varchar(24) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_uzytkownicy_powiadomienia_uzytkownicy` (`uzytkownik_id`),
  KEY `FK_uzytkownicy_powiadomienia_powiadomienia` (`powiadomienie_id`),
  KEY `FK_uzytkownicy_powiadomienia_konta` (`konto_id`),
  CONSTRAINT `FK_uzytkownicy_powiadomienia_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_uzytkownicy_powiadomienia_powiadomienia` FOREIGN KEY (`powiadomienie_id`) REFERENCES `powiadomienia` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_uzytkownicy_powiadomienia_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uzytkownicy_powiadomienia_kategorie`
--

DROP TABLE IF EXISTS `uzytkownicy_powiadomienia_kategorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uzytkownicy_powiadomienia_kategorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `powiadomienie_kategoria_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_uzytkownicy_powiadomienia_kategorie_uzytkownicy` (`uzytkownik_id`),
  KEY `FK_uzytkownicy_powiadomienia_kategorie_powiadomienia_kategorie` (`powiadomienie_kategoria_id`),
  KEY `FK_uzytkownicy_powiadomienia_kategorie_konta` (`konto_id`),
  CONSTRAINT `FK_uzytkownicy_powiadomienia_kategorie_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_uzytkownicy_powiadomienia_kategorie_powiadomienia_kategorie` FOREIGN KEY (`powiadomienie_kategoria_id`) REFERENCES `powiadomienia_kategorie` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_uzytkownicy_powiadomienia_kategorie_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uzytkownicy_watki_wiadomosci`
--

DROP TABLE IF EXISTS `uzytkownicy_watki_wiadomosci`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uzytkownicy_watki_wiadomosci` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `watek_wiadomosci_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_uzytkownicy_wiadomosci_uzytkownicy` (`uzytkownik_id`),
  KEY `FK_uzytkownicy_wiadomosci_wiadomosci` (`watek_wiadomosci_id`),
  KEY `FK_uzytkownicy_watki_wiadomosci_konta` (`konto_id`),
  CONSTRAINT `FK_uzytkownicy_watki_wiadomosci_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_uzytkownicy_wiadomosci_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`),
  CONSTRAINT `FK_uzytkownicy_wiadomosci_wiadomosci` FOREIGN KEY (`watek_wiadomosci_id`) REFERENCES `watki_wiadomosci` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uzytkownicy_wiadomosci`
--

DROP TABLE IF EXISTS `uzytkownicy_wiadomosci`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uzytkownicy_wiadomosci` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `uzytkownik_id` int(11) DEFAULT NULL,
  `projektowy_watek_chatu_id` int(11) DEFAULT NULL,
  `wiadomosc_id` int(11) DEFAULT NULL,
  `widocznosc` enum('zwinięta','rozwinięta') COLLATE utf8_polish_ci DEFAULT 'rozwinięta',
  PRIMARY KEY (`id`),
  KEY `FK_uzytkownicy_wiadomosci_wiadomosci_bezposrednio` (`wiadomosc_id`),
  KEY `FK_uzytkownicy_wiadomosci_uzytkownicy_user` (`uzytkownik_id`),
  KEY `FK_uzytkownicy_wiadomosci_projektowe_watki_chatu` (`projektowy_watek_chatu_id`),
  KEY `FK_uzytkownicy_wiadomosci_konta` (`konto_id`),
  CONSTRAINT `FK_uzytkownicy_wiadomosci_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_uzytkownicy_wiadomosci_projektowe_watki_chatu` FOREIGN KEY (`projektowy_watek_chatu_id`) REFERENCES `projektowe_watki_chatu` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_uzytkownicy_wiadomosci_uzytkownicy_user` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_uzytkownicy_wiadomosci_wiadomosci_bezposrednio` FOREIGN KEY (`wiadomosc_id`) REFERENCES `wiadomosci` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `uzytkownicy_zasoby`
--

DROP TABLE IF EXISTS `uzytkownicy_zasoby`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uzytkownicy_zasoby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `uzytkownik_id` int(11) NOT NULL,
  `zasob_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_uzytkownicy_zasoby_uzytkownicy` (`uzytkownik_id`),
  KEY `FK_uzytkownicy_zasoby_zasoby` (`zasob_id`),
  KEY `FK_uzytkownicy_zasoby_konta` (`konto_id`),
  CONSTRAINT `FK_uzytkownicy_zasoby_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_uzytkownicy_zasoby_uzytkownicy` FOREIGN KEY (`uzytkownik_id`) REFERENCES `uzytkownicy` (`id`),
  CONSTRAINT `FK_uzytkownicy_zasoby_zasoby` FOREIGN KEY (`zasob_id`) REFERENCES `zasoby` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `watki_wiadomosci`
--

DROP TABLE IF EXISTS `watki_wiadomosci`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `watki_wiadomosci` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `temat` text COLLATE utf16_polish_ci,
  `typ` enum('prywatny','projektowy','z wycen') COLLATE utf16_polish_ci NOT NULL DEFAULT 'prywatny',
  PRIMARY KEY (`id`),
  KEY `FK_watki_wiadomosci_konta` (`konto_id`),
  CONSTRAINT `FK_watki_wiadomosci_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wiadomosci`
--

DROP TABLE IF EXISTS `wiadomosci`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiadomosci` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `autor_id` int(11) DEFAULT NULL,
  `watek_id` int(11) DEFAULT NULL,
  `projektowy_watek_chatu_id` int(11) DEFAULT NULL,
  `z_chatu` char(1) COLLATE utf16_polish_ci NOT NULL DEFAULT '0',
  `tresc` text COLLATE utf16_polish_ci NOT NULL,
  `data_dodania` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_wiadomosci_watki_wiadomosci` (`watek_id`),
  KEY `FK_wiadomosci_uzytkownicy` (`autor_id`),
  KEY `FK_wiadomosci_projektowe_watki_chatu` (`projektowy_watek_chatu_id`),
  KEY `FK_wiadomosci_konta` (`konto_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wiadomosci_tagi`
--

DROP TABLE IF EXISTS `wiadomosci_tagi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiadomosci_tagi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `wiadomosc_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_wiadomosci_tagi_wiadomosci` (`wiadomosc_id`),
  KEY `FK_wiadomosci_tagi_tagi` (`tag_id`),
  KEY `FK_wiadomosci_tagi_konta` (`konto_id`),
  CONSTRAINT `FK_wiadomosci_tagi_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_wiadomosci_tagi_tagi` FOREIGN KEY (`tag_id`) REFERENCES `tagi` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_wiadomosci_tagi_wiadomosci` FOREIGN KEY (`wiadomosc_id`) REFERENCES `wiadomosci` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wiadomosci_zasoby`
--

DROP TABLE IF EXISTS `wiadomosci_zasoby`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiadomosci_zasoby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `wiadomosc_id` int(11) NOT NULL DEFAULT '0',
  `zasob_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_wiadomosci_zasoby_wiadomosci` (`wiadomosc_id`),
  KEY `FK_wiadomosci_zasoby_zasoby` (`zasob_id`),
  KEY `FK_wiadomosci_zasoby_konta` (`konto_id`),
  CONSTRAINT `FK_wiadomosci_zasoby_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_wiadomosci_zasoby_wiadomosci` FOREIGN KEY (`wiadomosc_id`) REFERENCES `wiadomosci` (`id`),
  CONSTRAINT `FK_wiadomosci_zasoby_zasoby` FOREIGN KEY (`zasob_id`) REFERENCES `zasoby` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wiki_artykuly`
--

DROP TABLE IF EXISTS `wiki_artykuly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_artykuly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `id_tematu` int(11) NOT NULL,
  `jezyk_kod` char(5) COLLATE utf8_polish_ci DEFAULT NULL,
  `nadrzedny_id_tematu` int(11) DEFAULT NULL,
  `avatar_id` int(11) DEFAULT NULL,
  `autor_id` int(11) DEFAULT NULL,
  `uprawnienie_kod` varchar(7) CHARACTER SET utf16 COLLATE utf16_polish_ci DEFAULT NULL,
  `tytul` varchar(200) COLLATE utf8_polish_ci DEFAULT NULL,
  `tresc` text COLLATE utf8_polish_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_tematu_jezyk_kod` (`id_tematu`,`jezyk_kod`),
  KEY `FK_wiki_artykuly_jezyki` (`jezyk_kod`),
  KEY `FK_wiki_artykuly_wiki_artykuly` (`nadrzedny_id_tematu`),
  KEY `FK_wiki_artykuly_zasoby` (`avatar_id`),
  KEY `FK_wiki_artykuly_uprawnienia` (`uprawnienie_kod`),
  KEY `FK_wiki_artykuly_uzytkownicy` (`autor_id`),
  KEY `FK_wiki_artykuly_konta` (`konto_id`),
  CONSTRAINT `FK_wiki_artykuly_jezyki` FOREIGN KEY (`jezyk_kod`) REFERENCES `jezyki` (`kod`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_wiki_artykuly_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_wiki_artykuly_uprawnienia` FOREIGN KEY (`uprawnienie_kod`) REFERENCES `uprawnienia` (`kod`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_wiki_artykuly_uzytkownicy` FOREIGN KEY (`autor_id`) REFERENCES `uzytkownicy` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_wiki_artykuly_wiki_artykuly` FOREIGN KEY (`nadrzedny_id_tematu`) REFERENCES `wiki_artykuly` (`id_tematu`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_wiki_artykuly_zasoby` FOREIGN KEY (`avatar_id`) REFERENCES `zasoby` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wiki_artykuly_historia`
--

DROP TABLE IF EXISTS `wiki_artykuly_historia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_artykuly_historia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `wiki_artykul_id` int(11) NOT NULL,
  `id_tematu` int(11) NOT NULL,
  `jezyk_kod` char(5) COLLATE utf8_polish_ci DEFAULT NULL,
  `nadrzedny_id_tematu` int(11) DEFAULT NULL,
  `avatar_id` int(11) DEFAULT NULL,
  `autor_id` int(11) DEFAULT NULL,
  `uprawnienie_kod` varchar(7) CHARACTER SET utf16 COLLATE utf16_polish_ci DEFAULT NULL,
  `tytul` varchar(200) COLLATE utf8_polish_ci DEFAULT NULL,
  `tresc` text COLLATE utf8_polish_ci,
  PRIMARY KEY (`id`),
  KEY `FK_wiki_artykuly_jezykih` (`jezyk_kod`),
  KEY `FK_wiki_artykuly_wiki_artykulyh` (`nadrzedny_id_tematu`),
  KEY `FK_wiki_artykuly_zasobyh` (`avatar_id`),
  KEY `FK_wiki_artykuly_uprawnieniah` (`uprawnienie_kod`),
  KEY `FK_wiki_artykuly_uzytkownicyh` (`autor_id`),
  KEY `FK_wiki_artykuly_historia_wiki_artykuly` (`wiki_artykul_id`),
  KEY `FK_wiki_artykuly_historia_konta` (`konto_id`),
  CONSTRAINT `FK_wiki_artykuly_historia_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_wiki_artykuly_historia_wiki_artykuly` FOREIGN KEY (`wiki_artykul_id`) REFERENCES `wiki_artykuly` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_wiki_artykuly_jezykih` FOREIGN KEY (`jezyk_kod`) REFERENCES `jezyki` (`kod`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_wiki_artykuly_uprawnieniah` FOREIGN KEY (`uprawnienie_kod`) REFERENCES `uprawnienia` (`kod`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_wiki_artykuly_uzytkownicyh` FOREIGN KEY (`autor_id`) REFERENCES `uzytkownicy` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_wiki_artykuly_wiki_artykulyh` FOREIGN KEY (`nadrzedny_id_tematu`) REFERENCES `wiki_artykuly` (`id_tematu`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_wiki_artykuly_zasobyh` FOREIGN KEY (`avatar_id`) REFERENCES `zasoby` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wystawca_faktur`
--

DROP TABLE IF EXISTS `wystawca_faktur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wystawca_faktur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `adres_id` int(11) DEFAULT NULL,
  `nazwa` varchar(200) COLLATE utf8_polish_ci NOT NULL DEFAULT '0',
  `nip` varchar(10) COLLATE utf8_polish_ci NOT NULL DEFAULT '0',
  `imie_nazwisko` varchar(50) COLLATE utf8_polish_ci NOT NULL DEFAULT '0',
  `miejsce_wystawienia` varchar(30) COLLATE utf8_polish_ci NOT NULL DEFAULT '0',
  `typ` enum('firma','osoba prywatna') COLLATE utf8_polish_ci DEFAULT 'firma',
  PRIMARY KEY (`id`),
  KEY `FK_wystawca_faktur_adresy` (`adres_id`),
  KEY `FK_wystawca_faktur_konta` (`konto_id`),
  CONSTRAINT `FK_wystawca_faktur_adresy` FOREIGN KEY (`adres_id`) REFERENCES `adresy` (`id`),
  CONSTRAINT `FK_wystawca_faktur_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zasoby`
--

DROP TABLE IF EXISTS `zasoby`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zasoby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `kategoria_id` int(11) DEFAULT NULL,
  `url` text COLLATE utf8_polish_ci NOT NULL,
  `nazwa` varchar(150) COLLATE utf8_polish_ci NOT NULL,
  `rozmiar` int(10) unsigned NOT NULL,
  `typ` enum('plik','obrazek') COLLATE utf8_polish_ci NOT NULL,
  `typ_fizyczny` varchar(20) COLLATE utf8_polish_ci NOT NULL,
  `data_dodania` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_zasoby_zasoby_kategorie` (`kategoria_id`),
  KEY `FK_zasoby_konta` (`konto_id`),
  CONSTRAINT `FK_zasoby_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_zasoby_zasoby_kategorie` FOREIGN KEY (`kategoria_id`) REFERENCES `zasoby_kategorie` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zasoby_kategorie`
--

DROP TABLE IF EXISTS `zasoby_kategorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zasoby_kategorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `opis` varchar(20) COLLATE utf8_polish_ci NOT NULL DEFAULT '''''',
  PRIMARY KEY (`id`),
  KEY `FK_zasoby_kategorie_konta` (`konto_id`),
  CONSTRAINT `FK_zasoby_kategorie_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zlecenia`
--

DROP TABLE IF EXISTS `zlecenia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zlecenia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `klient_id` int(11) DEFAULT NULL,
  `nr_zlecenia` int(11) DEFAULT NULL,
  `nazwa` text COLLATE utf16_polish_ci,
  `opis` text COLLATE utf16_polish_ci,
  `status` enum('Nowe','Przyjete','W trakcie realizacji','Gotowe do odbioru','Odebrane') COLLATE utf16_polish_ci NOT NULL DEFAULT 'Nowe',
  `zakonczone` char(1) COLLATE utf16_polish_ci NOT NULL DEFAULT '0',
  `data_rozpoczecia` timestamp NULL DEFAULT NULL,
  `data_zakonczenia` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_zlecenia_uzytkownicy` (`klient_id`),
  KEY `FK_zlecenia_konta` (`konto_id`),
  CONSTRAINT `FK_zlecenia_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_zlecenia_uzytkownicy` FOREIGN KEY (`klient_id`) REFERENCES `klienci` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zlecenia_kategorie`
--

DROP TABLE IF EXISTS `zlecenia_kategorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zlecenia_kategorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `nazwa` text COLLATE utf16_polish_ci,
  PRIMARY KEY (`id`),
  KEY `FK_zlecenia_kategorie_konta` (`konto_id`),
  CONSTRAINT `FK_zlecenia_kategorie_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zlecenia_zasoby`
--

DROP TABLE IF EXISTS `zlecenia_zasoby`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zlecenia_zasoby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `konto_id` int(11) DEFAULT NULL,
  `zlecenie_id` int(11) DEFAULT NULL,
  `zasob_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_zlecenia_zasoby_zlecenia` (`zlecenie_id`),
  KEY `FK_zlecenia_zasoby_zasoby` (`zasob_id`),
  KEY `FK_zlecenia_zasoby_konta` (`konto_id`),
  CONSTRAINT `FK_zlecenia_zasoby_konta` FOREIGN KEY (`konto_id`) REFERENCES `konta` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_zlecenia_zasoby_zasoby` FOREIGN KEY (`zasob_id`) REFERENCES `zasoby` (`id`),
  CONSTRAINT `FK_zlecenia_zasoby_zlecenia` FOREIGN KEY (`zlecenie_id`) REFERENCES `zlecenia` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


-- Dump completed on 2017-08-17  6:19:45

