CalendarFunckje = {
    porownajTygodnie: function (interval, sprawdzanaData) {
	if (interval.endDate() === undefined) {
	    interval.endDate("01/01/2017");
	}

	var daty = interval.all("L");
	for (var i = 0; i < daty.length; i++) {
	    var data = moment(daty[i]);
	    var wynik = data.isSame(sprawdzanaData, "week");
	    if (wynik)
		return true;
	}

	return false;
    },
    getParameterByName: function (name, url) {
	if (!url)
	    url = window.location.href;
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		results = regex.exec(url);
	if (!results)
	    return null;
	if (!results[2])
	    return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
    updateQueryStringParameter: function (uri, key, value) {
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
	    return uri.replace(re, '$1' + key + "=" + value + '$2');
	} else {
	    return uri + separator + key + "=" + value;
	}
    },
    zapiszZmienna: function (klucz, wartosc) {
	var newHREF = CalendarFunckje.updateQueryStringParameter(location.href, klucz, wartosc);
	history.pushState('', location.title, newHREF);
    },
    pobierzStart: function () {
	var start = CalendarFunckje.getParameterByName('start');

	if (start === null) {
	    var start = new Date();
	    start.toISOString().substring(0, 10);
	}
	return start;
    },
    korygujDateZakonczenia: function (startDate, endDate) {
	var swichedDate = startDate.clone();
	swichedDate.add(31, 'minutes');

	if (swichedDate.isAfter(endDate)) {
	    var wynik = startDate.clone().add(1, 'hour');
	    return wynik;
	}

	return endDate;
    },
    bootboxDatePicker: function (frm_str) {

	var object = $('<div/>').html(frm_str).contents();

	object.find('.datapicker_data').datepicker({
	    format: 'yyyy-mm-dd',
	    autoclose: true}).on('changeDate', function (ev) {
	    $(this).blur();
	    $(this).datepicker('hide');
	});

	return object;


    }

};


function BootboxContent() {
     $('#kalendarz_dodaj_zecenie_wraper .hasDatepicker').removeAttr('id');
    $('#kalendarz_dodaj_zecenie_wraper .hasDatepicker').datepicker("destroy");

    $('#kalendarz_dodaj_zecenie_wraper .hasDatepicker').removeClass('hasDatepicker');

    var form = $('#kalendarz_dodaj_zecenie_wraper').html();
    var frm_str = '<div id="display_form">' + form + '</div>';

    var object = $('<div/>').html(frm_str).contents();

    object.find('.date').datepicker({
	  autoclose: true,
		showOtherMonths: true,
		altFormat: "yy-mm-dd",
		dateFormat: "yy-mm-dd",
		monthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
		dayNamesMin: ["Nie", "Pn", "Wt", "Śr", "Cz", "Pt", "So"],
		monthNamesShort: ["Sty", "Lut", "Mar", "Kwi", "Maj", "Cze", "Lip", "Sie", "Wrz", "Paź", "Lis", "Gru"],
		selectOtherMonths: true,
		showMonthAfterYear: true,
		weekHeader: "W",
		yearRange: "2016:2100",
		changeMonth: true,
		changeYear: true}).on('changeDate', function (ev) {
	$(this).blur();
	$(this).datepicker('hide');
    });

    return object;
}

function BootboxContent2() {
      var frm_str = '<form id="some-form">'
                           + '<div class="form-group">'
                              + '<label for="date">Date</label>'
                              + '<input id="date" class="date span2 form-control input-sm" size="16" placeholder="dd-mm-yy" type="text">'
                              + '</div>'
                           + '</form>';

            var object = $('<div/>').html(frm_str).contents();

            object.find('.date').datepicker({
                autoclose: true,
		showOtherMonths: true,
		altFormat: "yy-mm-dd",
		dateFormat: "yy-mm-dd",
		monthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
		dayNamesMin: ["Nie", "Pn", "Wt", "Śr", "Cz", "Pt", "So"],
		monthNamesShort: ["Sty", "Lut", "Mar", "Kwi", "Maj", "Cze", "Lip", "Sie", "Wrz", "Paź", "Lis", "Gru"],
		selectOtherMonths: true,
		showMonthAfterYear: true,
		weekHeader: "W",
		yearRange: "2016:2100",
		changeMonth: true,
		changeYear: true,}).on('changeDate', function (ev) {
                   $(this).blur();
                   $(this).datepicker('hide');
            });

             return object

}

function BootboxContentCommon(frm_str) {
    var object = $('<div/>').html(frm_str).contents();

    object.find('.date').datepicker({
	format: 'yyyy-mm-dd',
	autoclose: true}).on('changeDate', function (ev) {
	$(this).blur();
	$(this).datepicker('hide');
    });

    return object;
}