function pobierzDateWStrefie(start_end) {
    var dateObj = new Date(start_end);
    var dateIntNTZ = dateObj.getTime() + dateObj.getTimezoneOffset() * 60 * 1000;
    var dateObjNTZ = new Date(dateIntNTZ);
    return dateObjNTZ;
}


$(document).ready(function () {


    function renderCalendar() {
	var today = new Date();
	today.toISOString().substring(0, 10);

	var calendar = $('#calendar').fullCalendar({
	    header: {
		left: 'prev,next',
		center: 'title',
		right: 'year,month,agendaWeek'
	    },
	    defaultDate: today,
	    lang: 'pl',
	    buttonIcons: false, // show the prev/next text
	    defaultView: 'month',
	    events: document.events,
	    defaultTimedEventDuration: '01:00:00',
	    views: {
		year: {
		    buttonText: 'Rok'
		}
	    },
	    height: 'auto',

	    eventDrop: function (event, delay) {

		var ajaxUrl = home_url + '/ajax/zmien_czas';

		var data = {id: event.id,
		    miesiace: delay._months,
		    dni: delay._days,
		    milisekundy: delay._milliseconds};
		$.post(ajaxUrl, data);


	    },
	    selectable: true,
	    selectHelper: true,
	    select: function (start, end, allDay) {
		var ajaxUrl = home_url + '/ajax/dodaj_zlecenie';

		var param = {start: start, end: end, allDay: allDay};



		bootbox.dialog({
		    title: "Dodaj zlecenie",
		    message: '<div id="display_form">' + $('#kalendarz_dodaj_zecenie_wraper').html() + "</div>",
//		    buttons: {
//			success: {
//			    label: "Zapisz",
//			    className: "btn-success",
//			    callback: function () {
////				var name = $('#name').val();
////				var answer = $("input[name='awesomeness']:checked").val()
//				var form = $('#display_form form').serialize();
//				;
//
//				jQuery.post(
//					ajaxUrl // your url
//					, form
//				).success(function () {
//				    location.reload();
//				});
//
//			    }
//			}
//		    }
		}
		);

		var mom = moment(pobierzDateWStrefie(start));
		var mom2 = moment(pobierzDateWStrefie(end));

		var data = mom.format("YYYY-MM-DD");
		var godzinaOd = mom.format("hh:mm");
		var godzinaDo = mom2.format("hh:mm");

		$("input[name='data_wykonania']").val(data);
		$("input[name='godzina_wykonania_od']").val(godzinaOd);
		$("input[name='godzina_wykonania_do']").val(godzinaDo);


		calendar.fullCalendar('unselect');
	    }
	});
    }
    addEvents();
    renderCalendar();

    $('.fc-button').addClass('btn btn-danger');
    $('.fc-button').removeClass('fc-next-button fc-button fc-state-default fc-corner-left fc-corner-right');
});