$(document).ready(function () {
    renderMiniCalendar();
    ukrywanieMiniKalendara();

});

function renderMiniCalendar() {
    var start = CalendarFunckje.pobierzStart();

    $('#mini_calendar').fullCalendar({
	defaultView: 'month',
	lang: 'pl',
	height: 360,
	defaultDate: start,
	dayClick: function (date, jsEvent, view) {

	    CalendarFunckje.zapiszZmienna('start', date.format("YYYY-MM-DD"));
	    location.reload();
	}
    });

}
;

function ukrywanieMiniKalendara() {
    chowajMiniKalendarz();

    $('#mini_label').hover(
	    function () {
		pokazMiniKalendarz();
	    });

    $('#mini_calendar').hover(
	    function () {},
	    function () {
		chowajMiniKalendarz()
	    });
}

function pokazMiniKalendarz() {
    $('#mini_calendar').show();
}

function chowajMiniKalendarz() {
    $('#mini_calendar').hide();
}