$(document).ready(function () {
    dodajClickDoCyklicznychZdarzen();
});
var prefix = "#cykliczne_zdarzenie_dialog ";
function dodajClickDoCyklicznychZdarzen() {
    $('.cykliczne_zdarzenie_btn').click(function () {
	bootbox.dialog({
	    title: "Cykliczne zdarzenie",
	    message: '<div id="cykliczne_zdarzenie_dialog">' + $('#kalendarz_cykliczne_zdarzenie_wraper').html() + "</div>",
	    buttons: {
		close: {
		    label: "Zamknij",
		    className: "btn btn-danger"
		},
		success: {
		    label: "Zapisz",
		    className: "btn btn-danger",
		    callback: function () {

			var wynik = '';
			var wynik2 = '';
			var data_wykonania = $('#data_wykonania').val();

			var koniec = $(prefix + 'input[name=koniec]:checked').val();
			var co_ile = $(prefix + '#powtarzaj_co').val();
			var dzien_zakonczenia = $(prefix + '#w_dniu').val();

			var powtarzaj_w = '';
			$('input[name=powtarzaj_w]:checked').map(function () {
			    powtarzaj_w += $(this).val() + ",";
			});

			var powtarzanie = $(prefix + '.powtarzenie').val();

			var appendInt1 = "";

			switch (powtarzanie) {
			    case 'codziennie':
				wynik = 'recur().every(' + co_ile + ').day()';
				break;
			    case 'powszednie':
				wynik = 'recur().every([1,2,3,4,5]).daysOfWeek()';
				break;
			    case 'pn_sr_pt':
				wynik = 'recur().every([1,3,5]).daysOfWeek()';
				break;
			    case 'wt_czw':
				wynik = 'recur().every([2,4]).daysOfWeek()';
				break;
			    case 'tydzien':
				wynik = 'recur().every(' + co_ile + ').weeks()';
				appendInt1 = ',tydz:true';
				wynik2 = 'recur().every([' + powtarzaj_w + ']).daysOfWeek()';

				break;
			    case 'miesiac':
				wynik = 'recur().every(' + co_ile + ').months()';
				break;
			    case 'rok':
				wynik = 'recur().every(' + co_ile + ').years()';
				break;

			    default:

			}

			switch (koniec) {
			    case 'w_dniu':
				wynik += '.endDate("' + dzien_zakonczenia + '")';
				if (wynik2 !== '') {
				    wynik2 += '.endDate("' + dzien_zakonczenia + '")';
				}
				break;
			}

			wynik += appendInt1;

			$('.cykliczne_zdarzenie_input').val(wynik);
			$('.cykliczne_zdarzenie_input2').val(wynik2);
		    }
		}
	    }
	}
	);


	$(prefix + ".powtarzenie").change(function () {
	    var wartosc = $(this).val();

	    $(prefix + '.kroki_cykl').hide();
	    $(prefix + '.k_' + wartosc).show();
	});

    });


}
