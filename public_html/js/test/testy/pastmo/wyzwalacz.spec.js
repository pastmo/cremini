describe('wyzwalacz.spec.js >', function () {
    beforeEach(function () {
    });

    it('tworzenie i wywolywanie wyzwalacza', function () {
	var thisFormularza = "formularz";
	var mock = {run: PastmoMoc.utworzMocka('run')};

	var wyzwalacz = new Wyzwalacz(mock.run);

	wyzwalacz.ustawParametr(thisFormularza);
	wyzwalacz._wywolaj();

	expect(mock.run).toHaveBeenCalledWith(thisFormularza);

    });


});