describe('Odswiezacz Email ', function () {

    beforeEach(function () {
	jasmine.getFixtures().fixturesPath = 'base/js/test/testy/helpery';
	loadFixtures('odswiezacz_email.dom.spec.html');
    });

    it('obsluzPowiadomienieWMenu ', function () {
	sprawdzObslugePowiadomien(5, 42, true);

    });

    it('obsluzPowiadomienieWMenu brak nowych', function () {
	$('#gadzet_email .item-quantity').show();
	sprawdzObslugePowiadomien(0, 0, false);
    });

    it('wyswietlNoweMaile', function () {
	OdswiezaczEmail.wyswietlNoweMaile(emailOdpowiedziAjax.odswiez_maile_ze_wszystkich_skrzynek_jeden_nowy_mail);
	var wynik = $('#mail-list').children();
	expect(wynik.length).toBe(3);

    });
    it('zmiana_odswiezania_email ', function () {
	spyOn(Pastmo, 'wyslijPost');
	OdswiezaczEmail.timer={zmienInterwal:function(){}};
	OdswiezaczEmail.initZmianeCzasuODswiezania();

	$('#zmiana_odswiezania_email select').val('42');
	$('#zmiana_odswiezania_email select').change();

	expect(Pastmo.wyslijPost).toHaveBeenCalledWith('email_ajax/zmien_czestotliwosc_odswiezania', {wartosc: '42'}, jasmine.any(Function));


    });

    var sprawdzObslugePowiadomien = function (ileStarych, ileNowych, czyWidoczne) {
	$('#gadzet_email .item-quantity').html(ileStarych);
	spyOn(OdswiezaczEmail, 'getIleMailiWOdpowiedzi').and.returnValues(ileNowych);

	OdswiezaczEmail.obsluzPowiadomienieWMenu([]);

	var wynik = $('#gadzet_email .item-quantity').html();
	var widocznosc = $('#gadzet_email .item-quantity').is(":visible");

	expect(wynik).toEqual(ileStarych + ileNowych + '');
	expect(widocznosc).toBe(czyWidoczne);
    }

});