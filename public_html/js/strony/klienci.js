var Klienci = {
    init: function () {
	this.initNotatkiKlientow();
    },
    initNotatkiKlientow: function () {
	var wyzwalacz = new Wyzwalacz(this.zapiszNotatke);
	$('#page-wrapper').on("keyup", ".notatka_klienta", function () {
	    wyzwalacz.wywolaj(this);
	});
    },
    zapiszNotatke: function (self) {
	var url = $(self).data('url');
	var notatka = $(self).val();

	Pastmo.wyslijPost(url, {notatka: notatka}, function (data) {
	    Pastmo.oznaczPoleJakoZapisane(self);
	});
    }


}

if (typeof wTrakcieTestow === 'undefined') {
    Klienci.init();
}