jQuery(document).ready(function ($) {
    //uruchomOdswiezaniePrzypomnien();
   // przypomnieniaWyslijAjaxa();

});

function uruchomOdswiezaniePrzypomnien() {
//    interval = setInterval(przypomnieniaTimer, 3000);
}
;

function przypomnieniaTimer() {
    przypomnieniaWyslijAjaxa();
}

function przypomnieniaWyslijAjaxa() {
    $ajaxUrl = $('body').data('ajax');
    $.ajax({
        url: $ajaxUrl,
        type: "POST",
        success: function (msg) {
            refreshPrzypomnienia(msg);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }});
}

function refreshPrzypomnienia(msg) {

    var liczba_zlecen = msg.dane.zlecenia.length;
    $('#przypomnienia_zlecenia').empty();
    $('#przypomnienia_zlecenia').append(liczba_zlecen);


    var liczba_zgloszen = msg.dane.zgloszenia.length;
    $('#przypomnienia_zgloszenia').empty();
    $('#przypomnienia_zgloszenia').append(liczba_zgloszen);


    var liczba_konwersacji = msg.dane.konwersacje.length;
    $('#przypomnienia_konwersacje').empty();
    $('#przypomnienia_konwersacje').append(liczba_konwersacji);

    var suma = liczba_zlecen + liczba_zgloszen + liczba_konwersacji;
    $('#przypomnienia_suma').empty();
    $('#przypomnienia_suma').append(suma);


}