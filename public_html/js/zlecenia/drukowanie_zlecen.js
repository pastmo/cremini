DrukowanieZlecen = {
    zaznaczenia: [],
    mapaKolumn: [
	{kolumna: 2, tekst: "Firma:"},
	{kolumna: 3, tekst: "Ulica:"},
	{kolumna: 7, tekst: "Godzina:"},
	{kolumna: 5, tekst: "Data:"},
	{kolumna: 8, tekst: "Technik:"},
	{kolumna: 9, tekst: "Tel."}
    ],
    pozostaleKolkumny: {id: 0, opis: 4},
    drukuj: function () {
	this.drukujZaznaczone();
    },
    drukujZaznaczone: function () {
	$('#drukDiv').empty();
	for (i = 0; i < DrukowanieZlecen.zaznaczenia.length; i++) {
	    var kolejnosc = DrukowanieZlecen.zaznaczenia[i];

	    var element = $(kolejnosc);

	    DrukowanieZlecen.dodajDoWydruku(element);
	}

	this.printContent("drukDiv");
    },
    dodajDoWydruku: function (element, kolumny_do_wydruku) {
	var jElement = $(element);

	var wlasciweKolumny = jElement.parent().siblings();

	var tekst = "";

	tekst += '<div>Nr zlecenia : ' + $(wlasciweKolumny[DrukowanieZlecen.pozostaleKolkumny.id]).html() + "</div>";

	tekst += '<table class="drukowanie_row"><tr class="row">';
	$.each(this.mapaKolumn, function (index, value) {
	    var nazwa = $($('th .nazwa')[value.kolumna]).html();
	    tekst += DrukowanieZlecen.opakujSmallDiv(value.tekst + "<br>" + $(wlasciweKolumny[value.kolumna]).html());
	});
	tekst += "</tr></table>";

	tekst += this.opakuj("Opis :<br>" + $(wlasciweKolumny[DrukowanieZlecen.pozostaleKolkumny.opis]).html());

	tekst += "<hr>";

	$('#drukDiv').append(tekst);
    },
    opakujSmallDiv: function (tekst) {
	return '<td class="col-sm-2">' + Drukowanie.usunDivy(tekst) + "</td>";
    },
    opakuj: function (tekst) {
	return '<div class="drukowanie_row"><pre>' + Drukowanie.usunDivy(tekst) + "</pre></div>";
    },
    initZaznaczanie: function () {
	$('.druk').change(function () {
	    if (this.checked) {
		DrukowanieZlecen.zaznaczenia.push(this);
	    } else {
		var i = DrukowanieZlecen.zaznaczenia.indexOf(this);
		if (i != -1) {
		    DrukowanieZlecen.zaznaczenia.splice(i, 1);
		}
	    }
	});
    },
    init: function () {
	this.__proto__ = Drukowanie;
	this.initZaznaczanie();
    }

}

DrukowanieZlecen.init();
jQuery(document).ready(function ($) {
    $('#drukuj').click(function () {

	DrukowanieZlecen.drukuj();
    })
});