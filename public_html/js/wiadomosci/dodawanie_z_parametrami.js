function aktywujAkcjeWatku() {

    $('#odpowiedz_button').click(function () {
	wyslijZadanieDodawania('zapisz_odpowiedz');
    });

    $('#przekaz_button').click(function () {
	wyslijZadanieDodawania('zapisz_przekaz');
    });

    $('#jako_wazny_button').click(function () {
	wyslijOznaczanieJakoWazny();
    });
    $('#usun_button').click(function () {
	wyslijUsun();
    });


}
;

function wyslijZadanieDodawania(komenda) {
    $('#akcja_w_dodaj').val(komenda);
    $('#polecenie_dodawania').submit();

}

function wyslijOznaczanieJakoWazny() {
    $('#polecenie_oznaczania_jako_wazny').submit();

}

function wyslijUsun() {
    $('#polecenie_usun').submit();

}