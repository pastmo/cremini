function ustawAktualnyWatek(idWatku) {
    $('#wiadomosci_wyslij').data('watek_id', idWatku);
}

function pobierzAktualnyWatek() {
    return $('#wiadomosci_wyslij').data('watek_id');
}

function pobierzWszystkieWatki() {
    var skrzynka = $('#skrzynka').data('skrzynka');
    var url = zrobUrl("konwersacje/watki.json");

    $.ajax({
	url: url,
	type: "POST",
	data: "action=get_watki&skrzynka=" + skrzynka,
	success: function (msg) {
	    refreshWatki(msg);
	},
	error: function (jqXHR, textStatus, errorThrown) {
	    console.log(jqXHR);
	    console.log(textStatus);
	    console.log(errorThrown);
	}});

}
;

function podepinijWyszukiwarke() {
    $('#szukaj_btn').click(function () {
	wyszukajWatki();
    });

    $('#szukaj').keypress(function (e) {
	if (e.which == 13) {
	    wyszukajWatki();

	}
    });

}

function wyszukajWatki() {
    var tekst = $('#szukaj').val();

    var url = zrobUrl("konwersacje/wyszukaj_watki.json");

    $.ajax({
	url: url,
	type: "POST",
	data: "tekst=" + tekst,
	success: function (msg) {
	    refreshWatki(msg);
	},
	error: function (jqXHR, textStatus, errorThrown) {
	    console.log(jqXHR);
	    console.log(textStatus);
	    console.log(errorThrown);
	}});
}


function refreshWatki(msg) {

    $('.selector_watku').remove();
    $('.rozmowa').empty();
    msg.watki_wiadomosci.forEach(drukujElement);

    var poczatkowy = $('#poczatkowy').data('watekdomyslny');

    if (poczatkowy > 0) {
	$('#watek_' + poczatkowy).click();
    } else {
	$('.selector_watku').first().click();
    }


}

function drukujElement(element, index, array) {

    var imie = element.wiadomosc.autor.imie;
    var nazwisko = element.wiadomosc.autor.nazwisko;
    var status = element.status;
    var dodatkowe = "";


    if (imie == undefined) {
	dodatkowe = 'style="background-color: #eee;"';
	imie = "-";
	nazwisko = "";
    }



    var nowy = ' <a id="watek_' + element.watek.id + '" ' + dodatkowe + ' href="#" class="nadawca list-group-item selector_watku ' + status + '">' +
	    '<span class="chat-img pull-left">' +
	    '<img src="' + element.avatar + '" alt="User Avatar" class="obraz_nad img-circle" />' +
	    '</span> <div class="uzytkownik">' + imie + ' ' + nazwisko + '</div>' +
	    '<span class="pull-right text-muted small"><em>' + element.dodano + '</em>' +
	    '</span>' +
	    '</a>';

    $('#konwersacje_watki').append(nowy);

    var last = $('.selector_watku').last();
    last.data("watek_id", element.watek.id);
    last.click(function () {
	var idWatku = $(this).data("watek_id");
	ustawAktualnyWatek(idWatku);
	aktywujPrzyciskWyslij();
	pobierzWszystkieWiadomosci();

	if ($(this).hasClass('nieprzeczytana')) {
	    $(this).removeClass('nieprzeczytana');
	    $(this).addClass('przeczytana');
	}
    });
}