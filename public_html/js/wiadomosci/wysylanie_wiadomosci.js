

function aktywujPrzyciskWyslij() {

    $('#wiadomosci_wyslij').off();
    $('#wiadomosci_wyslij').on('click', function () {
	var watekId = $('#wiadomosci_wyslij').data('watek_id');
	var tresc = $('#editor_textarea').val();
	var zalaczniki = $('.pliki').val();

	$("#editor").empty();
	Dropzone.forElement(".zasoby_upload").removeAllFiles(true);

	var url = zrobUrl("konwersacje/zapisz_dodaj.json");
	$.ajax({
	    url: url,
	    type: "POST",
	    data: "watek_id=" + watekId + "&tresc=" + tresc + '&pliki=' + zalaczniki,
	    success: function (msg) {
		console.log(msg);
	    },
	    error: function (jqXHR, textStatus, errorThrown) {
		console.log(jqXHR);
		console.log(textStatus);
		console.log(errorThrown);
	    }});
    });
}
