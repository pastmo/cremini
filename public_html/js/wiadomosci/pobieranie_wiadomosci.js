function pobierzWszystkieWiadomosci() {
    zatrzymajCykliczneOdswiezanie();
    var url = zrobUrl("konwersacje/wiadomosci.json");
    $.ajax({
	url: url,
	type: "POST",
	data: "watek_id=" + pobierzAktualnyWatek(),
	success: function (msg) {
	    odbierzWszystkieWiadomosci(msg);
	},
	error: function (jqXHR, textStatus, errorThrown) {
	    console.log(jqXHR);
	    console.log(textStatus);
	    console.log(errorThrown);
	}});
}
;
function ObslugaWiadomosci(msg) {
    this.mgs = msg;
    this.init = function () {
	this.wyczyscWiadomosci();
	this.ustawTemat();
	this.wyswietlWszystkie();
    }

    this.ustawTemat = function () {
	var temat = this.mgs.watek.temat;
	var uzytkownicy = this.mgs.uzytkownicy;
	$('.tylul_maila').empty();
	$('.tylul_maila').append(temat);
	$('#wiadomosci_osoby').empty();
	uzytkownicy.forEach(this.dodajUzytkownikow);
	$('.id_watku_odpowiedzi').val(this.mgs.watek.id);
    }

    this.dodajUzytkownikow = function (element, index, array) {

	var uzytkownik = '<a href="' + element.uzytkownik.url + '" class="czarny_link">' + element.uzytkownik.imie + " " + element.uzytkownik.nazwisko
		+ '</a>(' + element.uzytkownik.mail + '), ';
	$('#wiadomosci_osoby').append(uzytkownik);
    }


    this.wyswietlWszystkie = function () {
	this.mgs.wiadomosci.forEach(this.wyswietl);
    }

    this.wyswietl = function (element, index, array) {
	var dodano = element.dodano;
	var trescClass = 'odp_nad';
	if (element.zalogowany_autorem) {
	    trescClass = "odp_adr";
	}

	var html_zalacznikow = "";
	for (i = 0; i < element.zalaczniki.length; i++) {
	    var zalacznik = element.zalaczniki[i];
	    html_zalacznikow += '<a href="' + zalacznik.link + '"> ' +
		    '<div class="zalacznik_wiadomosci" style="background-image: url(\'' + zalacznik.wyswietlanie + '\')" ></div>' +
		    '</a>';
	}

	$html = '<div class="rozmowa_teraz list-group-item">' +
		'<span class="chat-img pull-left">' +
		'<img src="' + element.avatar + '" alt="User Avatar" class="obraz_nad img-circle" />' +
		'</span><div class="' + trescClass + '"><div class="odp_srodek">' + element.wiadomosc.tresc + '</div></div>' +
		'<span class="pull-right text-muted small"><em>' + dodano + '</em>' +
		'</span>' + html_zalacznikow
	'</div>'

	$('#wiadomosci_kontener').append($html);
    };
    this.wyczyscWiadomosci = function () {
	$('#wiadomosci_kontener').empty();
    };
    this.init();
}


function odbierzWszystkieWiadomosci($msg) {
    new ObslugaWiadomosci($msg);
    uruchomCykliczneOdswiezanie();
    aktywujAkcjeWatku();
}
;