var WyswietlanieModali = {
    parametry: {},
    wyswietlModala: function (url, parametryGeta, parametryDoInita) {

	var kontener = new KontenerOdbioruModala(parametryDoInita);
	Pastmo.wyslijGet(url, parametryGeta, kontener.odbior, true);
    },
    odbiorModala: function (dane, kontener) {
	var dodawaneOkienko = $("<div>" + dane + "</div>");
	var skrypty = dodawaneOkienko.find('#dolaczone_skrypty').data('skrypty');
	var tytul = dodawaneOkienko.find("#parametry_modala").data('title');
	bootbox.dialog({
	    message: dane,
	    title: tytul,
	    buttons: {
		close: {
		    label: "Zamknij",
		    className: "btn btn-primary"
		},
		success: {
		    label: "Zapisz",
		    className: "btn btn-primary zapis_modala",
		}
	    }
	});
	ObslugaInputow.podepnijDatepickerPodWszystkieDateInputy();
	$.each(skrypty, kontener.dolaczLubAktywujSkrypt);
    },
    dolaczLubAktywujSkrypt: function (index, element, kontener) {
	var url = element.url;
	var nazwaFunkcji = element.funkcja_js;

	WyswietlanieModali.parametry[nazwaFunkcji] = kontener.parametryInita;

	if (typeof window[nazwaFunkcji] === 'undefined') {
	    var jsElm = document.createElement("script");
	    jsElm.type = "application/javascript";
	    jsElm.src = url;
	    document.body.appendChild(jsElm);
	} else {
	    window[nazwaFunkcji].init();
	}

    },
    dodajParametry: function (klucz, parametry) {
	this.parametry[klucz] = parametry;
    },
    getParametry: function (klucz) {
	return this.parametry[klucz];
    }
}

var KontenerOdbioruModala = function (parametryDoInita) {
    var self = this;
    this.parametryInita = parametryDoInita;

    this.odbior = function (dane) {
	WyswietlanieModali.odbiorModala(dane, self);
    }
    this.dolaczLubAktywujSkrypt = function (index, element) {
	WyswietlanieModali.dolaczLubAktywujSkrypt(index, element, self);
    }

}