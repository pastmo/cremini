var ObslugaInputow = {
    obsluzPojedynczyDataInput: function (selektorLubObiektJquery) {
	var elementy = $(selektorLubObiektJquery);

	elementy.removeClass("hasDatepicker");

	ObslugaInputow.obsluzDataInput(elementy);
    },
    podepnijDatepickerPodWszystkieDateInputy: function () {

	var elementy = $('input[type="date"]');

	ObslugaInputow.obsluzDataInput(elementy);
    },
    obsluzDataInput: function (elementy) {
	elementy.attr('type', 'text');

	$.each(elementy, function () {
	    element = $(this);
	    minDate=element.data('min_date');

	    element.datepicker(
		    {
			inline: true,
			showOtherMonths: true,
			altFormat: "yy-mm-dd",
			dateFormat: "yy-mm-dd",
			monthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
			dayNamesMin: ["Nie", "Pn", "Wt", "Śr", "Cz", "Pt", "So"],
			monthNamesShort: ["Sty", "Lut", "Mar", "Kwi", "Maj", "Cze", "Lip", "Sie", "Wrz", "Paź", "Lis", "Gru"],
			selectOtherMonths: true,
			showMonthAfterYear: true,
			weekHeader: "W",
			yearRange: "2016:2100",
			changeMonth: true,
			changeYear: true,
			firstDay: 1,
			minDate: minDate
		    });
	});

	elementy.data('mask', "____-__-__");
    }

}
jQuery(document).ready(function ($) {
    ObslugaInputow.podepnijDatepickerPodWszystkieDateInputy();
});