Dymki = function (selektor) {
    this.selektor = selektor;
    this.selektorFlagiAtywnosci = "aktywne_pobieranie";
    this.selektorKonteneraTresci = '.tmp_wynikow';

    var self = this;

    $(selektor).hover(function () {

	var e = $(this);
	if (e.data(self.selektorFlagiAtywnosci)) {
	    return;
	}
	e.data(self.selektorFlagiAtywnosci, true);
	self.wyswietlTooltip(e);

    },
	    function () {
		var e = $(this);
		e.data(self.selektorFlagiAtywnosci, false);
		e.siblings('.popover').remove();
	    });
    this.wyswietlTooltip = function (e) {
	var kontenerTresci = e.siblings(self.selektorKonteneraTresci).children();

	if (kontenerTresci.length > 0) {
	    if (e.data(self.selektorFlagiAtywnosci)) {
		e.popover({
		    placement: 'auto',
		    html: true,
		    content: function () {
			var tresc=kontenerTresci.html();
			return tresc;
		    }
		}).popover('show');
	    }
	} else {
	    $.get(e.data('url'), function (d) {
		e.siblings(self.selektorKonteneraTresci).append(d);
		self.wyswietlTooltip(e);
	    });
	}
    }

}
