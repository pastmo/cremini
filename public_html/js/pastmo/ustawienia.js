var Ustawienia = {
    getCzasOdswiezania: function () {
	return odswiezanieJs;
    },
    initJezyki: function () {
	var jezyk = Pastmo.valueUndefined('pl');

	bootbox.setDefaults({
	    locale: jezyk
	});

	moment.locale(jezyk);
    }

}

jQuery(document).ready(function ($) {
    Ustawienia.initJezyki();
});