

function aktywujPrzyciskWyslij() {

    $('#wiadomosci_wyslij').off();
    $('#wiadomosci_wyslij').on('click', function () {
        var watekId = $('#wiadomosci_wyslij').data('watek_id');
        var tresc = $('#editor_textarea').val();

        $("#editor").empty();

        $.ajax({
            url: "email_ajax/dodaj.json",
            type: "POST",
            data: "watek_id=" + watekId + "&tresc=" + tresc,
            success: function (msg) {
                console.log(msg);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }});
    });
}
