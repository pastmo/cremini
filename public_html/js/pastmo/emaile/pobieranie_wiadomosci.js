function pobierzWszystkieWiadomosci() {
    zatrzymajCykliczneOdswiezanie();

    $.ajax({
	url: "email_ajax/wiadomosci.json",
	type: "POST",
	data: "watek_id=" + pobierzAktualnyWatek(),
	success: function (msg) {
	    odbierzWszystkieWiadomosci(msg);
	},
	error: function (jqXHR, textStatus, errorThrown) {
	    console.log(jqXHR);
	    console.log(textStatus);
	    console.log(errorThrown);
	}});
}
;


function ObslugaWiadomosci(msg) {
    this.mgs = msg;

    this.init = function () {
	this.wyczyscWiadomosci();
	this.ustawTemat();
	this.wyswietlWszystkie();
	this.wyswietlZalaczniki();
	this.uzupelnijWysywig();
    }

    this.wyczyscWiadomosci = function () {
	$('#wiadomosci_kontener').empty();
    };

    this.ustawTemat = function () {
	var temat = this.mgs.wiadomosc.tytul;
	var nadawca = this.mgs.wiadomosc.nadawca;

	$('.tylul_maila').empty();
	$('.tylul_maila').append(temat);

	$('#wiadomosci_osoby').empty();
	$('#wiadomosci_osoby').append(nadawca);

    }

    this.wyswietlWszystkie = function () {

	$html = '<div class="rozmowa_teraz list-group-item">' +
		'<div><div class="odp_srodek"><pre>' + this.mgs.wiadomosc.tresc + '</pre></div></div>' +
		'<span class="pull-right text-muted small"><em>' + this.mgs.wiadomosc.data_naglowka + '</em>' +
		'</span>' +
		'</div>'

	$('#wiadomosci_kontener').append($html);
    };



    this.wyswietlZalaczniki = function () {
	$('#zalaczniki').empty();
	var html = "";
	var wyswietlanieUrl=$('#zalaczniki').data('wyswietlanie');
	var downloadUrl=$('#zalaczniki').data('download');

	for (i = 0; i < this.mgs.zasoby.length; i++) {
	    var zasob = this.mgs.zasoby[i];


	    html += '<div class="zasob"><a href="' + downloadUrl+'/'+zasob.zasob.id + '">' +
		    '<img src="' + wyswietlanieUrl+zasob.obrazek + '"></a>' +
		    '</div>'
	}


	$('#zalaczniki').append(html);
    };



    this.uzupelnijWysywig = function () {
	$('#odbiorca_wysiwyg').val(this.mgs.nadawca);
	$('#tytul_wysiwyg').val('Re: '+this.mgs.wiadomosc.tytul);
    }

    this.init();

}


function odbierzWszystkieWiadomosci($msg) {
    new ObslugaWiadomosci($msg);
    uruchomCykliczneOdswiezanie();


}
;