function ustawAktualnyWatek(idWatku) {
    $('#wiadomosci_wyslij').data('watek_id', idWatku);
}

function pobierzAktualnyWatek() {
    return $('#wiadomosci_wyslij').data('watek_id');
}

function pobierzWszystkieWatki() {
    dodajLoader('#ustawienia_maili');
    Pastmo.wyslijPost("email_ajax/watki.json", "action=get_watki", function (msg) {
	refreshWatki(msg);
	usunLoader();
    });

}
;


function refreshWatki(msg) {

    msg.watki_wiadomosci.forEach(drukujElement);

    var poczatkowy = $('#poczatkowy').data('watekdomyslny');

    if (poczatkowy > 0) {
	$('#watek_' + poczatkowy).click();
    } else {
	$('.selector_watku').first().click();
    }


}

function drukujElement(element, index, array) {

    var nadawca = element.watek.nadawca;

    var nowy = ' <a id="watek_' + element.watek.id + '" href="#" class="nadawca list-group-item selector_watku">' +
	    '<div class="uzytkownik">' + nadawca + '</div>' +
	    '<span class=" text-muted small"><em>' + element.dodano + '</em>' +
	    '</span>' +
	    '</a>';

    $('#konwersacje_watki').append(nowy);

    var last = $('.selector_watku').last();
    last.data("watek_id", element.watek.id);
    last.click(function () {
	var idWatku = $(this).data("watek_id");
	ustawAktualnyWatek(idWatku);
	aktywujPrzyciskWyslij();
	pobierzWszystkieWiadomosci();
    });
}