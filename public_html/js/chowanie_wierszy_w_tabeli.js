jQuery(document).ready(function ($) {

    $('.wiersz_naglowkowy').click(function () {
        var aktualny = $(this);
        aktualny.next().toggle();
    });

    $('.przycisk-rozwijania').click(function() {
	var aktualny = $(this).closest('.wiersz_naglowek');
	var i=$(this).find('i');
	var napis=$(this).find('span');
	if (i.hasClass('fa-angle-down')) {
	    i.removeClass('fa-angle-down').addClass('fa-angle-up');
	} else {
	    i.removeClass('fa-angle-up').addClass('fa-angle-down');
	}
	napis.toggle();
	aktualny.next().toggle();
    });
});