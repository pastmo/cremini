var zwinietyClass="zwiniety";
var zwinietySidebarClass="zwiniety_sidebar";

jQuery(document).ready(function ($) {
    $('#chowanie_akcja').click(function () {


        var page_wrapper=$('#page-wrapper');
        var sidebar=$(".sidebar");

        if (page_wrapper.hasClass(zwinietyClass)) {
            page_wrapper.removeClass(zwinietyClass);
            sidebar.removeClass(zwinietySidebarClass);
        } else {
            page_wrapper.addClass(zwinietyClass);
            sidebar.addClass(zwinietySidebarClass);
        }
    });
});
