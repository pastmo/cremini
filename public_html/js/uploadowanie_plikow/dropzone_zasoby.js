var submitAktywny = true;

$("form").submit(function () {
    return submitAktywny;
});
var Profile = {
    init: function () {

	Dropzone.autoDiscover = false;
	var inputElementSelector = $(".zasoby_upload").data('desc');
	$(inputElementSelector).val("[]");
	$(".zasoby_upload").dropzone({
	    url: $(".zasoby_upload").data('url'),
	    autoDiscover: false,
	    success: function (file, response) {
		submitAktywny = false;
		for (var i = 0; i < response.zasoby.length; i++) {
		    var zasob = response.zasoby[i];
		    var inputVal = $(inputElementSelector).val();
		    var inputArray = JSON.parse(inputVal);
		    inputArray.push(zasob.id);
		    $(inputElementSelector).val(JSON.stringify(inputArray));
		}
		submitAktywny = true;
	    },
	    error: function (file, response) {
		file.previewElement.classList.add("dz-error");
	    },
	    sending: function (file, xhr, formData) {
		var select = $('select[name=kategoria]');
		if (select.length > 0) {
		    formData.append("kategoria", select.val());
		}
	    }
	});
    }};

Profile.init();