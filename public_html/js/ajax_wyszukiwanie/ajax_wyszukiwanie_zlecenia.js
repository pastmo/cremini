var request_id = 0;
var przetworzone_id = 0;


jQuery(document).ready(function ($) {
    $('#tekst_wyszukiwania').keyup(function () {
	wyszukiwanie();
    })
    $('#kategoria').change(function () {
	wyszukiwanie();
    })
});

function wyszukiwanie() {

    var url = $('#wyszukiwanie').data('adres');
    var value = $('#tekst_wyszukiwania').val();
    var kategoria = $('#kategoria').val();
    request_id++;

    $.ajax({
	url: url,
	type: "get",
	data: {wyszukaj: value, kategoria_id: kategoria, request_id: request_id},
	success: function (msg) {
	    if (msg.request_id >= przetworzone_id) {
		przetworzone_id=msg.request_id;
		
		$('#tabela_zlecen').empty();
		msg.zlecenia.forEach(drukujWierszeTabeli)
	    }
	}});
}

function drukujWierszeTabeli(element, index, array) {

    var szczegolyZlecenia = $('#wyszukiwanie').data('szczegoly_zlecenia');
    var szczegolyKlienta = $('#wyszukiwanie').data('szczegoly_klienta');

    var wiersz =
	    '<tr>' +
	    '<td>' + (index + 1) + '</td>' +
	    '<td><a href="' + szczegolyZlecenia + '/' + element.id + '">' + element.nazwa + '</a></td>' +
	    '<td ' + element.stylKlient + '><a href="' + szczegolyKlienta + '/' + element.klientId + '">' + element.klient + '</a></td>' +
	    '<td>' + element.opiekun + '</td>' +
	    '<td>' + element.dataOddania + '</td>' +
	    '<td class="' + element.dniKolor + '">' + element.iloscDniDoKonca + '</td>' +
	    '<td>' + element.status + '</td>' +
	    '</tr>';
    $('#tabela_zlecen').append(wiersz);
}